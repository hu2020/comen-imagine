#include <comen_config.h>

#include <QSettings>
#include <QApplication>

#define COMEN_CFG_GRP_UI           "UI"
#define COMEN_CFG_GRP_EMAIL        "Email"

#define COMEN_CFG_KEY_MW_GEOMETRY  "MainWindowGeometry"
#define COMEN_CFG_KEY_MW_STATE     "MainWindowState"

#define COMEN_CFG_KEY_HOST         "Host"

Config::Config(const QString& path):
  m_path(path)
{
}

Config::Config() :
  Config("")
{
}

Config::~Config()
{
}

bool Config::load()
{
  bool      result { true };
  QSettings *cfg   { };

  if (m_path.isEmpty())
  {

    cfg  = new QSettings(QSettings::IniFormat, QSettings::UserScope,
                        QApplication::organizationName(),
                        QApplication::applicationName());
  }
  else
  {
    cfg  = new QSettings(m_path, QSettings::IniFormat);
  }

  cfg->beginGroup(COMEN_CFG_GRP_UI);
  m_mwGeometry        = cfg->value(COMEN_CFG_KEY_MW_GEOMETRY).toByteArray();
  m_mwState           = cfg->value(COMEN_CFG_KEY_MW_STATE).toByteArray();
  cfg->endGroup();

  cfg->beginGroup(COMEN_CFG_GRP_EMAIL);
  m_emailHost = cfg->value(COMEN_CFG_KEY_HOST).toString();
  cfg->endGroup();

  delete cfg;

  return result;
}

bool Config::save()
{
  bool        result = true;
  QSettings   *cfg   = nullptr;

  if (m_path.isEmpty())
  {
    cfg  = new QSettings(QSettings::IniFormat, QSettings::UserScope,
                        QApplication::organizationName(),
                        QApplication::applicationName());
  }
  else
  {
    cfg  = new QSettings(m_path, QSettings::IniFormat);
  }

  cfg->beginGroup(COMEN_CFG_GRP_UI);
  cfg->setValue(COMEN_CFG_KEY_MW_GEOMETRY, m_mwGeometry);
  cfg->setValue(COMEN_CFG_KEY_MW_STATE, m_mwState);
  cfg->endGroup();

  cfg->beginGroup(COMEN_CFG_GRP_EMAIL);
  cfg->setValue(COMEN_CFG_KEY_HOST, m_emailHost);
  cfg->endGroup();

  delete cfg;

  return result;
}
