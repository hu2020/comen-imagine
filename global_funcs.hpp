#ifndef GLOBAL_FUNCS_HPP
#define GLOBAL_FUNCS_HPP

#include "lib/utils/cutils.h"
#include "lib/machine/machine_handler.h"
#include "tests/unit_tests/ccrypto/ccryptotests.h"
#include "tests/unit_tests/merkle/merkle.h"
#include "tests/unit_tests/tests_premutation.h"
#include "tests/unit_tests/ccrypto/tests_pgp.h"

class MainWindow;



void onAboutToQuit(MainWindow* w);
int initDb();
void initDirMonitoring();

void dummyTestsHandler();
void init_logging();

#endif // GLOBAL_FUNCS_HPP
