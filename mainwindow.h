#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "stable.h"

#include "lib/machine/machine_handler.h"
#include "lib/services/initialize_node.h"
#include "lib/threads_handler.h"
#include "gui/c_gui.h"

#include "lib/wallet/wallet.h"
#include "lib/transactions/basic_transactions/utxo/coin.h"

#include "global_vars.hpp"
#include "global_funcs.hpp"



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class Wallet;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  void loadConfigurationParameters();
  void saveConfigurationParameters();

  int initMachineEnvironment();
  int sendEmail();

  //  -  -  -  -  GUI part
  void informationMessage(const QString& msg, const QString& title = "");
  void warningMessage(const QString& msg, const QString& title = "");
  void connectGUIDataModels();


  void refreshGUI();
  bool futureRefresDAGManually();

  // -  -  -  -  -  -  -  settings tab
  bool emailSettingsValidator();

  // -  -  -  -  -  -  -  -  neighbors tab
//  void loadNeighborsInfo();

private slots:

  void on_pushButton_dummyTrigger_clicked();

  // - - - - - - Menu % Toolbar QActions
  void on_actionBackup_Machine_triggered();
  void on_actionExit_triggered();
  void on_pushButton_restoreMachineFromBackup_clicked();

  void on_pushButton_saveEmailsSettings_clicked();



  // - - - - - - Neighbors
  void connectNeighborsToModel();
  void handshakeDblClicked(const QModelIndex& index);
  bool on_pushButton_addANewNeighbor_clicked();
//  void neighborDoubleClicked(int nRow, int nCol);
  void on_pushButton_readHDManually_clicked();



  // -  -  -  Monitor
  void connectInboxFilesToModel();
  void connectOutboxFilesToModel();
  void connectDAGHistoryToModel();
  void connectParsingQToModel();
  void connectSendingQToModel();
  void connectMissedBlocksToModel();
  void connectDAGLeavesModel();
  void connectMachineBalances();

//  void refreshParsingQ();
//  void refresDAG();
  bool on_pushButton_sendDAGSnapshot_clicked();
  bool on_pushButton_downloadDAGSnapshot_clicked();
  bool on_pushButton_refresDAGManually_clicked();


  void setLabelCPacketsInParsingQ();

  void refreshLeavesInfo();

  void on_pushButton_invokeDescendents_clicked();

  void on_pushButton_broadcastManually_clicked();

  void on_pushButton_invokeBlock_clicked();

  void on_pushButton_fullDAGRequest_clicked();

  void on_pushButton_pullFromParsingQManually_clicked();

  void on_pushButton_controlMissedBlocks_clicked();

  void on_pushButton_invokeAllMissedBlocks_clicked();

  void on_pushButton_refreshAccountsBalance_clicked();

  // -  -  -  Wallet
  void on_pushButton_createBasic1of1Address_clicked();
  void on_pushButton_createBasic2of3Address_clicked();
  void on_pushButton_createStrict2of3Address_clicked();
  void on_pushButton_signTransaction_clicked();
  void on_pushButton_refreshWallet_clicked();
  void on_pushButton_resetSpendingForm_clicked();
  void connectWalletCoinsToModel();
  void connectWalletAccountsToModel();
  void on_lineEdit_trx_amount_textEdited(const QString& amount);
  void on_lineEdit_trx_fee_textEdited(const QString& amount);
  void on_pushButton_unmark_not_really_used_coins_clicked();

  // -  -  -  wiki
  void connectWikiTitlesToModel();
  void on_pushButton_submit_edited_wiki_page_clicked();
  void on_pushButton_submit_wiki_page_clicked();
  void on_pushButton_edit_wiki_page_clicked();
  void on_pushButton_toggle_wiki_form_clicked();

  // -  -  -  misc
  void connectBlockBufferToModel();
  void on_pushButton_flushBlockBuffer_clicked();
  void on_pushButton_refreshBlockBuffer_clicked();
  void removeFromBuffer(const QModelIndex& index);
  void on_pushButton_generate_transactions_list_clicked();

  // -  -  -  society
  void connectSocietyPollingsToModel();
  void connectAdmPollingsToModel();
  void dspAdmPollings(const QModelIndex& index);
  void on_pushButton_pushAdmPolling_clicked();
  void on_pushButton_refreshSocietyPollings_clicked();
  void on_horizontalSlider_adm_polling_valueChanged(int value);
  void on_pushButton_vote_adm_polling_clicked();


  // -  -  -  Demos
  void connectAgoraTitlesToModel();
  void clickedOnDemosLinks(const QUrl& link);
  void on_lineEdit_agora_title_textChanged(const QString& amount);
  void on_comboBox_agora_category_currentIndexChanged(const QString& arg1);
  void on_horizontalSlider_agora_reply_valueChanged(int value);
  void on_pushButton_create_agora_clicked();
  void on_comboBox_agora_domain_currentIndexChanged(const QString&);
  void on_pushButton_toggle_agora_creation_form_clicked();
  void on_pushButton_reply_agora_post_clicked();
  void on_pushButton_new_post_on_agora_clicked();

  // -  -  -  Contributes
  void connectContributesToModel();
  void connectDraftProposalsToModel();
  void connectDraftPledgesToModel();
  void connectSendLoanRequestToModel();
  void refreshProposalCost();
  void on_pushButton_refreshContributesInfo_clicked();
  void on_pushButton_voteProposal_clicked();
  void on_pushButton_prepareProposal_clicked();
  void on_pushButton_resetProposalForm_clicked();
  void on_comboBox_contributeHours_currentIndexChanged(int index);
  void on_comboBox_contributeUsefulness_currentIndexChanged(int index);
  void on_pushButton_closeLoanRequestForm_clicked();
  void on_lineEdit_loanRequestIntrestRate_textChanged(const QString& amount);
  void on_lineEdit_loanRepaymentAmount_textChanged(const QString& amount);
  void on_pushButton_signLoanRequest_clicked();
  void on_horizontalSlider_vote_valueChanged(int value);


  // -  -  -  Contracts
  void on_pushButton_contractsInfo_clicked();
  void connectReceivedLoanRequestsToModel();
  void connectProposalPledgeTransactionsToModel();
  void connectMachineContractsToModel();


  // -  -  -  inames
  void connectRegisteredINamesToModel();
  void connectMachineControlledINamesToModel();
  void on_lineEdit_iname_to_reg_textChanged(const QString& iname);
  void on_pushButton_registerIName_clicked();
  void on_pushButton_update_registered_inames_clicked();
  void on_pushButton_create_and_bind_iPGP_clicked();


  // -  -  -  DAG state
  void connectCoinsVisbilityModel();
  void connectSnapshotModel();
  void connectSnapComapreModel();
  void on_pushButton_updateCoinsVisibility_clicked();
  void on_pushButton_controllDAGHealth_clicked();
  void on_pushButton_loadSnapshot_clicked();





private:
  Ui::MainWindow *ui;

  Wallet *m_wallet { };
};
#endif // MAINWINDOW_H
