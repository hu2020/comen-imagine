#include "stable.h"

#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"
#include "lib/transactions/basic_transactions/utxo/suspect_trx_handler.h"
#include "tests/unit_tests/transactions/importing_validations/prepare_data/prepare_data_util.h"




/**
 * TrxA and TrxB using same coin1. they been propagate to network by a cheater in order to missleading etwork
 * and some nodes consider TrxA as a valid transaction and the others voting for TrxB and split/crush the network
 *
 * the spOrder of trx (by spendDate stated by block):
 *      spenderBlock1.TrxA.spendDate(t0) -> GT1 -> spenderBlock2.TrxB.spendDate
 *
 * the rOrder of trx (by POV of Voter which is provided by "vote date" field):     GT=Gap Time
 *      spenderBlock2.TrxB.spendDate(t0) -> GTPropagation ->
 *      spenderBlock2.TrxB.rOrder -> GT2 -> spenderBlock1.TrxA.rOrder
 *
 */

bool testDoubleSpend_spend_ab_receive_ba()
{
  TimeBySecT full_cycle = CUtils::getCycleBySeconds();
  QString dataset_str = KVHandler::getValue("rawVotes_spend_ab_receive_ba");
  if (dataset_str == "")
  {
    PrepareDataUtil::doPrepareRawVotes_spend_ab_receive_ba();
    dataset_str = KVHandler::getValue("rawVotes_spend_ab_receive_ba");
  }
  QJsonObject votes_datasets = CUtils::parseToJsonObj(dataset_str);

  QString manual_key = "";
  if (manual_key != "")
  {
    QJsonObject the_dataset = votes_datasets[manual_key].toObject();

    UTXOImportDataContainer* block_inspect_result = new UTXOImportDataContainer();
    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_result, the_dataset);

    SuspectTrxHandler::checkDocValidity(
      block_inspect_result,
      "TrxA");

    SuspectTrxHandler::checkDocValidity(
      block_inspect_result,
      "TrxB");

    delete block_inspect_result;
    return true;
  }

  uint32_t start_index = 0;
  QStringList cases_keys = votes_datasets.keys();
  cases_keys.sort();
  uint32_t end_index = cases_keys.size();

  // start_index = 100;
  // end_index = 300;

  UTXOImportDataContainer* block_inspect_container = new UTXOImportDataContainer();
//  UTXOImportDataContainer* block_inspect_container = new UTXOImportDataContainer();

  for (uint32_t inx = start_index; inx < end_index; inx++)
  {
    delete block_inspect_container;
    block_inspect_container = new UTXOImportDataContainer();

    QString the_case_key = cases_keys[inx];
    QJsonObject the_dataset = votes_datasets[the_case_key].toObject();

    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_container, the_dataset);
    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_container, the_dataset);

    SuspectTrxHandler::checkDocValidity(block_inspect_container, "TrxB");
    SuspectTrxHandler::checkDocValidity(block_inspect_container, "TrxA");


    if (QStringList{"0", "<6"}.contains(the_dataset["GT1"].toString()))
    {
      if (QStringList{"0", "<6", "6", "6-12"}.contains(the_dataset["GTPropagation"].toString()))
      {
        /**
        * both diff blocks which using same coin are created with less than 6 hours
        * and propagated and received to our machine in less than 6 hours
        * so donate coins
        */
        if (PrepareDataUtil::addGapTimes(the_dataset["GTPropagation"].toString(), the_dataset["GT2"].toString()) < full_cycle)
        {
          /**
           * the final diff-time between spending first coin spending(TrxB) and second spending(TrxA)
           * for machine POV is adding propagation delay to time diff between first doc and second one (GT2)
           * only if this number is smaller than a cycle, machine can cease the transaction.
           * it means after 12 hours machine is not eligible to cease a transaction
           */
          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check ab-ba 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "donate")
            CUtils::exiter("failed on double spending check ab-ba 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check ab-ba 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check ab-ba 4 key: " + the_case_key, 23);


          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check ab-ba 5 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "donate")
            CUtils::exiter("failed on double spending check ab-ba 6 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check ab-ba 7 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check ab-ba 8 key: " + the_case_key, 23);

          continue;

        } else {
          /**
           * the first rarrived trx(TrxB) is valid and TrxA will be rejected
           *
           */

          if (!block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check ab-ba 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
            CUtils::exiter("failed on double spending check ab-ba 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check ab-ba 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check ab-ba 4 key: " + the_case_key, 23);


          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check ab-ba 5 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
            CUtils::exiter("failed on double spending check ab-ba 6 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check ab-ba 7 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check ab-ba 8 key: " + the_case_key, 23);

          continue;

        }

      } else {
        /**
         * both diff blocks which using same coin are created with less than 6 hours
         * and propagated and received to our machine in equal or more than 12 hours
         * so the first one machine visited(TrxB) is valid and second must be rejected
         */

        if (!block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
          CUtils::exiter("failed on double spending check ab-ba 1 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
          CUtils::exiter("failed on double spending check ab-ba 2 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
          CUtils::exiter("failed on double spending check ab-ba 3 key: " + the_case_key, 23);

        if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
          CUtils::exiter("failed on double spending check ab-ba 4 key: " + the_case_key, 23);


        if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
          CUtils::exiter("failed on double spending check ab-ba 5 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
          CUtils::exiter("failed on double spending check ab-ba 6 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
          CUtils::exiter("failed on double spending check ab-ba 7 key: " + the_case_key, 23);

        if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
          CUtils::exiter("failed on double spending check ab-ba 8 key: " + the_case_key, 23);

        continue;
      }

    }

    if (QStringList{"6", "6-12", "12", "12<", "24", "24<"}.contains(the_dataset["GT1"].toString())) {
      /**
       * between first and second doc, by cheater statements there is equal or
       * more than 6 hours
       * and TrxB is after TrxA, but in machine"s POV it is vice versa
       * it is, machine first received the TrxB then TrxA and since the creation Date gap
       * is bigger than 6 hour
       * for machine the TrxB is Valid, and TrxA Invalid and the node simply reject it
       *
       * Note: when machine receive TrxB first of all controls if creation Date is not in future!
       * otherwise machine drops the lock whit future creation date
       *
       */

      if (!block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
        CUtils::exiter("failed on double spending check ab-ba 1 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
        CUtils::exiter("failed on double spending check ab-ba 2 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
        CUtils::exiter("failed on double spending check ab-ba 3 key: " + the_case_key, 23);

      if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
        CUtils::exiter("failed on double spending check ab-ba 4 key: " + the_case_key, 23);


      if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
        CUtils::exiter("failed on double spending check ab-ba 5 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
        CUtils::exiter("failed on double spending check ab-ba 6 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
        CUtils::exiter("failed on double spending check ab-ba 7 key: " + the_case_key, 23);

      if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
        CUtils::exiter("failed on double spending check ab-ba 8 key: " + the_case_key, 23);

      continue;

    }


    CUtils::exiter("Mustn't reach here ab-ba!", 11);
    break;
  }

  return true;
}
