#include "stable.h"

#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"
#include "lib/transactions/basic_transactions/utxo/suspect_trx_handler.h"
#include "tests/unit_tests/transactions/importing_validations/prepare_data/prepare_data_util.h"


/**
 * TrxA, TrxB and TrxC using same coin1. they been propagate to network by a cheater in order to missleading network
 * and some nodes consider TrxA as a valid transaction and the others voting for TrxB Or TrxC
 * and split/crush the network
 *
 * the spOrder of trx (by spendDate stated by block):
 *      spenderBlock1.TrxA.spendDate(t0) -> GT1 -> spenderBlock2.TrxB.spendDate -> GT2 -> spenderBlock3.TrxC.spendDate
 *
 * the rOrder of trx (by POV of Voter which is provided by "vote date" field):     GT=Gap Time
 *      spenderBlock1.TrxA.spendDate(t0) -> GTPropagation ->
 *          spenderBlock1.TrxA.rOrder -> GT3 -> spenderBlock2.TrxB.rOrder -> GT4 -> spenderBlock3.TrxC.rOrder
 *
 */

bool testDoubleSpend_spend_abc_receive_acb()
{
  TimeBySecT full_cycle = CUtils::getCycleBySeconds();
  QString dataset_str = KVHandler::getValue("rawVotes_spend_abc_receive_acb");
  if (dataset_str == "")
  {
    PrepareDataUtil::doPreparerawVotes_spend_abc_receive_acb();
    dataset_str = KVHandler::getValue("rawVotes_spend_abc_receive_acb");
  }
  QJsonObject votes_datasets = CUtils::parseToJsonObj(dataset_str);

  QString manual_key = "";
  if (manual_key != "")
  {
    QJsonObject the_dataset = votes_datasets[manual_key].toObject();
    UTXOImportDataContainer* block_inspect_result = new UTXOImportDataContainer();
    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_result, the_dataset);

    SuspectTrxHandler::checkDocValidity(
      block_inspect_result,
      "TrxA");

    SuspectTrxHandler::checkDocValidity(
      block_inspect_result,
      "TrxB");

    SuspectTrxHandler::checkDocValidity(
      block_inspect_result,
      "TrxC");

    delete block_inspect_result;
    return true;
  }



  uint32_t start_index = 0;
  QStringList cases_keys = votes_datasets.keys();
  cases_keys.sort();
  uint32_t end_index = cases_keys.size();

  // start_index = 25000;
  // end_index = 28000;

  UTXOImportDataContainer* block_inspect_container = new UTXOImportDataContainer();

  for (uint32_t inx = start_index; inx < end_index; inx++)
  {
    delete block_inspect_container;
    block_inspect_container = new UTXOImportDataContainer();

    QString the_case_key = cases_keys[inx];
    QJsonObject the_dataset = votes_datasets[the_case_key].toObject();

    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_container, the_dataset);
    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_container, the_dataset);
    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_container, the_dataset);

    SuspectTrxHandler::checkDocValidity(block_inspect_container, "TrxA");
    SuspectTrxHandler::checkDocValidity(block_inspect_container, "TrxB");
    SuspectTrxHandler::checkDocValidity(block_inspect_container, "TrxC");


    if (QStringList{"0", "<6"}.contains(the_dataset.value("GT1").toString()))
    {
      if (QStringList{"0", "<6", "6", "6-12"}.contains(the_dataset.value("GTPropagation").toString()))
      {
        /**
        * two TrxA & TrxB which are received before TrxC are spended same coin1 as well as TrxC
        * in less than 6 hours
        * so donate coins
        */

        if (PrepareDataUtil::addGapTimes(the_dataset.value("GTPropagation").toString(), the_dataset.value("GT3").toString()) < full_cycle)
        {
          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "donate")
            CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "donate")
            CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


          if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "donate")
            CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);

          continue;

        } else {
          /**
           * the first rarrived trx(TrxA) is valid and TrxB & TrxC will be rejected
           *
           */
          if (!block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
            CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
            CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


          if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
            CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);

          continue;

        }

      } else {
        /**
         * both diff blocks which using same coin are created with less than 6 hours
         * and propagated and received to our machine in equal or more than 12 hours
         * so the first one machine visited(TrxA) is valid and second must be rejected
         */
        if (!block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
          CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
          CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
          CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

        if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
          CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


        if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
          CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
          CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
          CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

        if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
          CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


        if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
          CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
          CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
          CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

        if(block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
          CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


        continue;

      }

    }

    if (QStringList{"6", "12", "6-12", "12<", "24", "24<"}.contains(the_dataset.value("GT1").toString()))
    {
      /**
       * between first and second doc, by cheater statements there is equal or more than 6 hours and second one is after first one
       * so the TrxA must be considered as a valid transaction and TrxB & TrxC Invalid and the node simply reject second transaction
       * in all of these cases it is not important what Time Gap occures for rOrder, in other word wile the order of
       * receiving transactions in machine is firsts TrxA and then TrxB & TrxC, machine considers first as valid and second invalid,
       * despite the how long gap was between these two block
       */

      if (!block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
        CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
        CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
        CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

      if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
        CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


      if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
        CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
        CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
        CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

      if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
        CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);


      if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
        CUtils::exiter("failed on double spending check abc-abc 1 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
        CUtils::exiter("failed on double spending check abc-abc 2 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
        CUtils::exiter("failed on double spending check abc-abc 3 key: " + the_case_key, 23);

      if(block_inspect_container->m_transactions_validity_check["TrxC"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
        CUtils::exiter("failed on double spending check abc-abc 4 key: " + the_case_key, 23);

      continue;

    }

    CUtils::exiter("Mustn't reach here abc-abc!", 11);
    break;
  }

  return true;
}
