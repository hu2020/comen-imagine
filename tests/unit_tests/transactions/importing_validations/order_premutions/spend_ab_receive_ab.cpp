#include "stable.h"

#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"
#include "lib/transactions/basic_transactions/utxo/suspect_trx_handler.h"
#include "tests/unit_tests/transactions/importing_validations/prepare_data/prepare_data_util.h"

/**
 * TrxA and TrxB using same coin1. they been propagate to network by a cheater in order to missleading etwork
 * and some nodes consider TrxA as a valid transaction and the others voting for TrxB and split/crush the network
 *
 * the spOrder of trx (by spendDate stated by block):
 *      spenderBlock1.TrxA.spendDate(t0) -> GT1 -> spenderBlock2.TrxB.spendDate
 *
 * the rOrder of trx (by POV of Voter which is provided by "vote date" field):     GT=Gap Time
 *      spenderBlock1.TrxA.spendDate(t0) -> GTPropagation ->
 *      spenderBlock1.TrxA.rOrder -> GT2 -> spenderBlock1.TrxB.rOrder
 *
 */

bool testDoubleSpend_spend_ab_receive_ab()
{
  TimeBySecT full_cycle = CUtils::getCycleBySeconds();
  QString dataset_str = KVHandler::getValue("rawVotes_spend_ab_receive_ab");
  if (dataset_str == "")
  {
    PrepareDataUtil::doPrepareRawVotes_spend_ab_receive_ab();
    dataset_str = KVHandler::getValue("rawVotes_spend_ab_receive_ab");
  }
  QJsonObject votes_datasets = CUtils::parseToJsonObj(dataset_str);

  QString manual_key = "";
  if (manual_key != "")
  {
    QJsonObject the_dataset = votes_datasets[manual_key].toObject();
    UTXOImportDataContainer* block_inspect_result = new UTXOImportDataContainer();
    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_result, the_dataset);
    SuspectTrxHandler::checkDocValidity(
      block_inspect_result,
      "TrxA");

    SuspectTrxHandler::checkDocValidity(
      block_inspect_result,
      "TrxB");

    delete block_inspect_result;
    return true;
  }

  uint32_t start_index = 0;
  QStringList cases_keys = votes_datasets.keys();
  cases_keys.sort();
  uint32_t end_index = cases_keys.size();

  // start_index = 100;
  // end_index = 300;

  UTXOImportDataContainer* block_inspect_container = new UTXOImportDataContainer();

  for (uint32_t inx = start_index; inx < end_index; inx++)
  {
    delete block_inspect_container;
    block_inspect_container = new UTXOImportDataContainer();

    QString the_case_key = cases_keys[inx];
    QJsonObject the_dataset = votes_datasets[the_case_key].toObject();

    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_container, the_dataset);
    SuspectTrxHandler::checkDocValidity(block_inspect_container, "TrxA"); // TrxAValidity

    PrepareDataUtil::assignVotestoInspectContainer(block_inspect_container, the_dataset);
    SuspectTrxHandler::checkDocValidity(block_inspect_container, "TrxB"); // TrxBValidity


    if (QStringList{"0", "<6"}.contains(the_dataset["GT1"].toString()))
    {

      if (QStringList{"0", "<6", "6", "6-12"}.contains(the_dataset["GTPropagation"].toString()))
      {
        /**
         * both diff blocks which using same coin are created with less than 6 hours
         * and propagated and received to our machine in less than 6 hours
         * so donate coins
         */
        if (PrepareDataUtil::addGapTimes(the_dataset["GTPropagation"].toString(), the_dataset["GT2"].toString()) < full_cycle)
        {
          /**
           * the final diff-time between spending first coin spending(TrxA) and second spending(TrxB)
           * for machine POV is adding propagation delay to time diff between first doc and second one (GT2)
           * only if this number is smaller than a cycle, machine can cease the transaction.
           * it means after 12 hours machine is not eligible to cease a transaction
           */
          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check ab-ab 1 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "donate")
            CUtils::exiter("failed on double spending check ab-ab 2 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check ab-ab 3 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check ab-ab 4 key: " + the_case_key, 23);


          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check ab-ab 5 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "donate")
            CUtils::exiter("failed on double spending check ab-ab 6 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check ab-ab 7 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check ab-ab 8 key: " + the_case_key, 23);

          continue;

        }else{
          if (!block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check ab-ab 9 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
            CUtils::exiter("failed on double spending check ab-ab 10 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check ab-ab 11 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check ab-ab 12 key: " + the_case_key, 23);


          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
            CUtils::exiter("failed on double spending check ab-ab 13 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
            CUtils::exiter("failed on double spending check ab-ab 14 key: " + the_case_key, 23);

          if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
            CUtils::exiter("failed on double spending check ab-ab 15 key: " + the_case_key, 23);

          if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
            CUtils::exiter("failed on double spending check ab-ab 16 key: " + the_case_key, 23);

          continue;

        }

      } else {
        /**
         * both diff blocks which using same coin are created with less than 6 hours
         * and propagated and received to our machine in more or eaul than 12 hours
         * so the first one machine visited(TrxA) is valid and second must be rejected
         */

        if (!block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
          CUtils::exiter("failed on double spending check ab-ab 17 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
          CUtils::exiter("failed on double spending check ab-ab 18 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
          CUtils::exiter("failed on double spending check ab-ab 19 key: " + the_case_key, 23);

        if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
          CUtils::exiter("failed on double spending check ab-ab 20 key: " + the_case_key, 23);


        if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
          CUtils::exiter("failed on double spending check ab-ab 21 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
          CUtils::exiter("failed on double spending check ab-ab 22 key: " + the_case_key, 23);

        if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
          CUtils::exiter("failed on double spending check ab-ab 23 key: " + the_case_key, 23);

        if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
          CUtils::exiter("failed on double spending check ab-ab 24 key: " + the_case_key, 23);

        continue;

      }
    }

    if (QStringList{"6", "6-12", "12", "12<", "24", "24<"}.contains(the_dataset["GT1"].toString()))
    {
      /**
       * between first and second doc, by cheater statements there is equal or more
       * than 6 hours and second one is after first one
       * so the TrxA must be considered as a valid transaction and TrxB Invalid
       * and the node simply reject second transaction
       * in all of these cases it is not important what Time Gap occures for rOrder,
       * in other word wile the order of
       * receiving transactions in machine is firts TrxA and then TrxB,
       * machine considers first as valid and second invalid,
       * despite the how long gap was between these two block
       */

      if (!block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
        CUtils::exiter("failed on double spending check ab-ab 25 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "-")
        CUtils::exiter("failed on double spending check ab-ab 26 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
        CUtils::exiter("failed on double spending check ab-ab 27 key: " + the_case_key, 23);

      if(block_inspect_container->m_transactions_validity_check["TrxA"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
        CUtils::exiter("failed on double spending check ab-ab 28 key: " + the_case_key, 23);


      if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_valid)
        CUtils::exiter("failed on double spending check ab-ab 29 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_action != "reject")
        CUtils::exiter("failed on double spending check ab-ab 30 key: " + the_case_key, 23);

      if (block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_voters != 1)
        CUtils::exiter("failed on double spending check ab-ab 31 key: " + the_case_key, 23);

      if(block_inspect_container->m_transactions_validity_check["TrxB"].m_susVoteRes[PrepareDataUtil::s_coin1].m_votes != 100)
        CUtils::exiter("failed on double spending check ab-ab 32 key: " + the_case_key, 23);

      continue;
    }

    CUtils::exiter("Mustn't reach here ab-ab!", 11);
    break;
  }
  return true;
}
