#ifndef PREPAREDATAUTIL_H
#define PREPAREDATAUTIL_H


class PrepareDataUtil
{
public:
  PrepareDataUtil();

  const static QStringList s_gap_time_types;// GapTimeTypes
  const static CDateT s_t0;
  const static QHash<QString, uint64_t> s_map_gap_type_to_seconds;
  const static CCoinCodeT s_coin1;

  static CDateT addGapTime(
    const QString& gap_type,
    const CDateT& cDate);

  static uint64_t addGapTimes(
    const QString& g1,
    const QString& g2);

  static void loadDatasets(
    const QString& file_name,
    const bool doChunk = false,
    const int chunkSize = 1000);

  static void doPrepareRawVotes_spend_ab_receive_ab();
  static void doPrepareRawVotes_spend_ab_receive_ba();
  static void doPrepareRawVotes_spend_abc_receive_abc();
  static void doPreparerawVotes_spend_abc_receive_acb();
  static void doPreparerawVotes_spend_abc_receive_bac();

  static bool makeSense2(
    const QString& key,
    const QString& GT1,
    const QString& GTPropagation,
    const QString& GT2);

  static bool makeSense3(
    const QString& key,
    const QString& GT1,
    const QString& GT2,
    const QString& GTPropagation,
    const QString& GT3,
    const QString& GT4);

  static void assignVotestoInspectContainer(
    UTXOImportDataContainer* block_inspect_container,
    const QJsonObject& the_dataset);



};

#endif // PREPAREDATAUTIL_H
