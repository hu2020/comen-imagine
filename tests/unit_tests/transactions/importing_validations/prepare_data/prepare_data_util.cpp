#include "stable.h"

#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"
#include "lib/transactions/basic_transactions/utxo/suspect_trx_handler.h"

#include "prepare_data_util.h"

const QStringList PrepareDataUtil::s_gap_time_types = {"0", "<6", "6", "6-12", "12", "12<", "24", "24<"}; //
const CDateT PrepareDataUtil::s_t0 = "2019-01-01 00:00:00";
const CCoinCodeT PrepareDataUtil::s_coin1 = "coin1___________________________________________________________:__";

const QHash<QString, uint64_t> PrepareDataUtil::s_map_gap_type_to_seconds = {
  {"0", 0},
  {"<6", 60},   // 1 minute
  {"6", 21600},   // 6 hour
  {"6-12", 21660},    // 6 hour + 1 minute
  {"12", 43200},// 12 hour
  {"12<", 43260},// 12 hour + 1 minute
  {"24", 86400},// 24 hour
  {"24<", 86400},// 24 hour + 1 minute
};

PrepareDataUtil::PrepareDataUtil()
{

}

CDateT PrepareDataUtil::addGapTime(
  const QString& gap_type,
  const CDateT& cDate)
{
  CDateT out;
  if (gap_type == "0")
  {
    out = cDate;
  }
  else if (gap_type == "<6")
  {
    out = CUtils::secondsAfter(60, cDate);
  }
  else if (gap_type == "6")
  {
    out = CUtils::secondsAfter(CUtils::getCycleBySeconds() / 2, cDate);
  }
  else if (gap_type == "6-12")
  {
    out = CUtils::secondsAfter((CUtils::getCycleBySeconds() / 2) + 60, cDate);
  }
  else if (gap_type == "12")
  {
    out = CUtils::secondsAfter(CUtils::getCycleBySeconds(), cDate);
  }
  else if (gap_type == "12<")
  {
    out = CUtils::secondsAfter(CUtils::getCycleBySeconds() + 60, cDate);
  }
  else if (gap_type == "24")
  {
    out = CUtils::secondsAfter(CUtils::getCycleBySeconds() * 2, cDate);
  }
  else if (gap_type == "24<")
  {
    out = CUtils::secondsAfter(CUtils::getCycleBySeconds() * 2 + 60, cDate);
  }

  return out;
}


uint64_t PrepareDataUtil::addGapTimes(
  const QString& g1,
  const QString& g2)
{
  uint64_t asSeconds = s_map_gap_type_to_seconds[g1] + s_map_gap_type_to_seconds[g2];
  return asSeconds;
}

void PrepareDataUtil::doPrepareRawVotes_spend_ab_receive_ab()
{
  QString datasetInfo = KVHandler::getValue("rawVotes_spend_ab_receive_ab");
  if (datasetInfo != "")
    return;

  QJsonObject votes_datasets = {};
  int inx = 0;
  for (QString GT1: s_gap_time_types)
  {
    for (QString GTPropagation: s_gap_time_types)
    {
      for (QString GT2: s_gap_time_types)
      {
        QString key = QStringList {GT1, GTPropagation, GT2}.join(",");

        // some case combinations does not make sense
        if (!makeSense2(key, GT1, GTPropagation, GT2))
            continue;

        QJsonArray raw_votes = {
          QJsonObject {
            {"st_spender_doc", "TrxA"},
            {"st_spend_date", s_t0},
            {"st_receive_order", 0},
            {"st_vote_date", s_t0},

            {"st_spender_block", "spenderBlock1"},
            {"st_voter", "voter1"},
            {"st_coin", s_coin1},
            {"st_logger_block", "loggerBlock1"},
            {"voterPercentage", 100}
          },
          QJsonObject {
            {"st_spender_doc", "TrxB"},
            {"st_spend_date", s_t0},
            {"st_receive_order", 1},
            {"st_vote_date", s_t0},

            {"st_spender_block", "spenderBlock2"},
            {"st_voter", "voter1"},
            {"st_coin", s_coin1},
            {"st_logger_block", "loggerBlock1"},
            {"voterPercentage", 100}
          }
        };

        inx++;
        QJsonObject tmp_obj0 = raw_votes[0].toObject();
        QJsonObject tmp_obj1 = raw_votes[1].toObject();

        tmp_obj0["st_spend_date"] = s_t0;
        tmp_obj0["st_vote_date"] = addGapTime(GTPropagation, s_t0);
        tmp_obj1["st_spend_date"] = addGapTime(GT1, tmp_obj0.value("st_spend_date").toString());
        tmp_obj1["st_vote_date"] = addGapTime(GT2, tmp_obj0.value("st_vote_date").toString());

        raw_votes[0] = tmp_obj0;
        raw_votes[1] = tmp_obj1;


        votes_datasets[key] = QJsonObject {
          {"GT1", GT1},
          {"GT2", GT2},
          {"GTPropagation", GTPropagation},
          {"inx", inx},
          {"rawVotes", raw_votes}};
      }
    }
  }

  // save in db
  KVHandler::upsertKValue("rawVotes_spend_ab_receive_ab", CUtils::serializeJson(votes_datasets));
}

void PrepareDataUtil::doPrepareRawVotes_spend_ab_receive_ba()
{
  QString datasetInfo = KVHandler::getValue("rawVotes_spend_ab_receive_ba");
  if (datasetInfo != "")
    return;

  QJsonObject votes_datasets = {};
  int inx = 0;
  for (QString GT1: s_gap_time_types)
  {
    for (QString GTPropagation: s_gap_time_types)
    {
      for (QString GT2: s_gap_time_types)
      {
        QString key = QStringList {GT1, GTPropagation, GT2}.join(",");

        // some case combinations does not make sense
        if (!makeSense2(key, GT1, GTPropagation, GT2))
            continue;

        QJsonArray raw_votes = {
          QJsonObject {
            {"st_spender_doc", "TrxA"},
            {"st_spend_date", s_t0},
            {"st_receive_order", 1},
            {"st_vote_date", s_t0},

            {"st_spender_block", "spenderBlock1"},
            {"st_voter", "voter1"},
            {"st_coin", s_coin1},
            {"st_logger_block", "loggerBlock1"},
            {"voterPercentage", 100}
          },
          QJsonObject {
            {"st_spender_doc", "TrxB"},
            {"st_spend_date", s_t0},
            {"st_receive_order", 0},
            {"st_vote_date", s_t0},

            {"st_spender_block", "spenderBlock2"},
            {"st_voter", "voter1"},
            {"st_coin", s_coin1},
            {"st_logger_block", "loggerBlock1"},
            {"voterPercentage", 100}
          }
        };

        inx++;
        QJsonObject tmp_obj0 = raw_votes[0].toObject();
        QJsonObject tmp_obj1 = raw_votes[1].toObject();

        tmp_obj0["st_spend_date"] = s_t0;
        tmp_obj1["st_spend_date"] = addGapTime(GT1, tmp_obj0.value("st_spend_date").toString());

        tmp_obj1["st_vote_date"] = addGapTime(GTPropagation, s_t0);
        tmp_obj0["st_vote_date"] = addGapTime(GT2, tmp_obj1.value("st_vote_date").toString());

        raw_votes[0] = tmp_obj0;
        raw_votes[1] = tmp_obj1;


        votes_datasets[key] = QJsonObject {
          {"GT1", GT1},
          {"GT2", GT2},
          {"GTPropagation", GTPropagation},
          {"inx", inx},
          {"rawVotes", raw_votes}};
      }
    }
  }

  // save in db
  KVHandler::upsertKValue("rawVotes_spend_ab_receive_ba", CUtils::serializeJson(votes_datasets));
}

void PrepareDataUtil::doPrepareRawVotes_spend_abc_receive_abc()
{
  QString datasetInfo = KVHandler::getValue("rawVotes_spend_abc_receive_abc");
  if (datasetInfo != "")
    return;

  QJsonObject votes_datasets = {};
  int inx = 0;

  for (QString GT1: s_gap_time_types)
  {
    for (QString GT2: s_gap_time_types)
    {
      for (QString GTPropagation: s_gap_time_types)
      {
        for (QString GT3: s_gap_time_types)
        {
          for (QString GT4: s_gap_time_types)
          {
            QString key = QStringList {GT1, GT2, GTPropagation, GT3, GT4}.join(",");

            // some case combinations does not make sense
            if (!makeSense3(key, GT1, GT2, GTPropagation, GT3, GT4))
              continue;

            QJsonArray raw_votes = {
              QJsonObject {
                {"st_spender_doc", "TrxA"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 0},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock1"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              },
              QJsonObject {
                {"st_spender_doc", "TrxB"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 1},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock2"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              },
              QJsonObject {
                {"st_spender_doc", "TrxC"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 2},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock3"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              }
            };

            inx++;
            QJsonObject tmp_obj0 = raw_votes[0].toObject();
            QJsonObject tmp_obj1 = raw_votes[1].toObject();
            QJsonObject tmp_obj2 = raw_votes[2].toObject();

            tmp_obj0["st_spend_date"] = s_t0;
            tmp_obj1["st_spend_date"] = addGapTime(GT1, tmp_obj0["st_spend_date"].toString());
            tmp_obj2["st_spend_date"] = addGapTime(GT2, tmp_obj1["st_spend_date"].toString());

            tmp_obj0["st_vote_date"] = addGapTime(GTPropagation, s_t0);
            tmp_obj1["st_vote_date"] = addGapTime(GT3, tmp_obj0["st_vote_date"].toString());
            tmp_obj2["st_vote_date"] = addGapTime(GT4, tmp_obj1["st_vote_date"].toString());

            raw_votes[0] = tmp_obj0;
            raw_votes[1] = tmp_obj1;
            raw_votes[2] = tmp_obj2;


            votes_datasets[key] = QJsonObject {
            {"GT1", GT1},
            {"GT2", GT2},
            {"GTPropagation", GTPropagation},
            {"GT3", GT3},
            {"GT4", GT4},
            {"inx", inx},
            {"rawVotes", raw_votes}};

          }
        }
      }
    }
  }


  // save in db
  KVHandler::upsertKValue("rawVotes_spend_abc_receive_abc", CUtils::serializeJson(votes_datasets));
}

void PrepareDataUtil::doPreparerawVotes_spend_abc_receive_acb()
{
  QString datasetInfo = KVHandler::getValue("rawVotes_spend_abc_receive_abc");
  if (datasetInfo != "")
    return;

  QJsonObject votes_datasets = {};
  int inx = 0;

  for (QString GT1: s_gap_time_types)
  {
    for (QString GT2: s_gap_time_types)
    {
      for (QString GTPropagation: s_gap_time_types)
      {
        for (QString GT3: s_gap_time_types)
        {
          for (QString GT4: s_gap_time_types)
          {
            QString key = QStringList {GT1, GT2, GTPropagation, GT3, GT4}.join(",");

            // some case combinations does not make sense
            if (!makeSense3(key, GT1, GT2, GTPropagation, GT3, GT4))
              continue;

            QJsonArray raw_votes = {
              QJsonObject {
                {"st_spender_doc", "TrxA"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 0},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock1"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              },
              QJsonObject {
                {"st_spender_doc", "TrxB"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 2},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock2"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              },
              QJsonObject {
                {"st_spender_doc", "TrxC"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 1},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock3"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              }
            };

            inx++;
            QJsonObject tmp_obj0 = raw_votes[0].toObject();
            QJsonObject tmp_obj1 = raw_votes[1].toObject();
            QJsonObject tmp_obj2 = raw_votes[2].toObject();

            tmp_obj0["st_spend_date"] = s_t0;
            tmp_obj1["st_spend_date"] = addGapTime(GT1, tmp_obj0["st_spend_date"].toString());
            tmp_obj2["st_spend_date"] = addGapTime(GT2, tmp_obj1["st_spend_date"].toString());

            tmp_obj0["st_vote_date"] = addGapTime(GTPropagation, s_t0);
            tmp_obj2["st_vote_date"] = addGapTime(GT3, tmp_obj0["st_vote_date"].toString());
            tmp_obj1["st_vote_date"] = addGapTime(GT4, tmp_obj2["st_vote_date"].toString());

            raw_votes[0] = tmp_obj0;
            raw_votes[1] = tmp_obj1;
            raw_votes[2] = tmp_obj2;


            votes_datasets[key] = QJsonObject {
            {"GT1", GT1},
            {"GT2", GT2},
            {"GTPropagation", GTPropagation},
            {"GT3", GT3},
            {"GT4", GT4},
            {"inx", inx},
            {"rawVotes", raw_votes}};

          }
        }
      }
    }
  }


  // save in db
  KVHandler::upsertKValue("rawVotes_spend_abc_receive_abc", CUtils::serializeJson(votes_datasets));
}

void PrepareDataUtil::doPreparerawVotes_spend_abc_receive_bac()
{
  QString datasetInfo = KVHandler::getValue("rawVotes_spend_abc_receive_bac");
  if (datasetInfo != "")
    return;

  QJsonObject votes_datasets = {};
  int inx = 0;

  for (QString GT1: s_gap_time_types)
  {
    for (QString GT2: s_gap_time_types)
    {
      for (QString GTPropagation: s_gap_time_types)
      {
        for (QString GT3: s_gap_time_types)
        {
          for (QString GT4: s_gap_time_types)
          {
            QString key = QStringList {GT1, GT2, GTPropagation, GT3, GT4}.join(",");

            // some case combinations does not make sense
            if (!makeSense3(key, GT1, GT2, GTPropagation, GT3, GT4))
              continue;

            QJsonArray raw_votes = {
              QJsonObject {
                {"st_spender_doc", "TrxA"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 1},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock1"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              },
              QJsonObject {
                {"st_spender_doc", "TrxB"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 0},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock2"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              },
              QJsonObject {
                {"st_spender_doc", "TrxC"},
                {"st_spend_date", s_t0},
                {"st_receive_order", 2},
                {"st_vote_date", s_t0},

                {"st_spender_block", "spenderBlock3"},
                {"st_voter", "voter1"},
                {"st_coin", s_coin1},
                {"st_logger_block", "loggerBlock1"},
                {"voterPercentage", 100}
              }
            };

            inx++;
            QJsonObject tmp_obj0 = raw_votes[0].toObject();
            QJsonObject tmp_obj1 = raw_votes[1].toObject();
            QJsonObject tmp_obj2 = raw_votes[2].toObject();

            tmp_obj0["st_spend_date"] = s_t0;
            tmp_obj1["st_spend_date"] = addGapTime(GT1, tmp_obj0["st_spend_date"].toString());
            tmp_obj2["st_spend_date"] = addGapTime(GT2, tmp_obj1["st_spend_date"].toString());

            tmp_obj1["st_vote_date"] = addGapTime(GTPropagation, s_t0);
            tmp_obj0["st_vote_date"] = addGapTime(GT3, tmp_obj1["st_vote_date"].toString());
            tmp_obj2["st_vote_date"] = addGapTime(GT4, tmp_obj0["st_vote_date"].toString());


            raw_votes[0] = tmp_obj0;
            raw_votes[1] = tmp_obj1;
            raw_votes[2] = tmp_obj2;

            votes_datasets[key] = QJsonObject {
            {"GT1", GT1},
            {"GT2", GT2},
            {"GTPropagation", GTPropagation},
            {"GT3", GT3},
            {"GT4", GT4},
            {"inx", inx},
            {"rawVotes", raw_votes}};

          }
        }
      }
    }
  }


  // save in db
  KVHandler::upsertKValue("rawVotes_spend_abc_receive_bac", CUtils::serializeJson(votes_datasets));
}

bool PrepareDataUtil::makeSense2(
  const QString& key,
  const QString& GT1,
  const QString& GTPropagation,
  const QString& GT2)
{

  if (key == "0,0,0,0,0")
    return false; // never will happend

  // in real world most of time will be gap between spending and propagation to neighbors
  if (GTPropagation == "0")
    return false;

  return true;
}

bool PrepareDataUtil::makeSense3(
  const QString& key,
  const QString& GT1,
  const QString& GT2,
  const QString& GTPropagation,
  const QString& GT3,
  const QString& GT4)
{

  if (key == "0,0,0,0,0")
    return false; // never will happend

  // in real world most of time will be gap between spending and propagation to neighbors
  if (GTPropagation == "0")
    return false;

  return true;
}

void PrepareDataUtil::assignVotestoInspectContainer(
  UTXOImportDataContainer* block_inspect_result,
  const QJsonObject& the_dataset)
{

  block_inspect_result->m_raw_votes = {};
  for(auto a_vote_: the_dataset.value("rawVotes").toArray())
  {
    QJsonObject  a_vote = a_vote_.toObject();
    QVDicT a_vote_dict {
      {"the_coin", PrepareDataUtil::s_coin1},
      {"st_voter", a_vote.value("st_voter")},
      {"st_vote_date", a_vote.value("st_vote_date")},
      {"st_logger_block", a_vote.value("st_logger_block")},
      {"st_spender_block", a_vote.value("st_spender_block")},
      {"st_spender_doc", a_vote.value("st_spender_doc")},
      {"st_receive_order", a_vote.value("st_receive_order")},
      {"st_spend_date", a_vote.value("st_spend_date")},
      {"voterPercentage", a_vote.value("voterPercentage")}};
    RawVote a_vote_obj(a_vote_dict);
    block_inspect_result->m_raw_votes.push_back(a_vote_obj);
  }
}

void PrepareDataUtil::loadDatasets(
  const QString& file_name,
  const bool doChunk,
  const int chunkSize)
{
//  let contentRes = fileHandler.packetFileReadSync({
//    file_name,
//    appCloneId: 0,
//    dirName: iConsts.HD_PATHES.hd_backup_dag,
//  });
//  let datasets = utils.parse(contentRes.content);
//  let caseKeys = utils.objKeys(datasets).sort();

//  let chunks = [];
//  if (doChunk) {

//  }
//  return { datasets, caseKeys, chunks }
}
