#include "stable.h"

#include "order_premutions/spend_ab_receive_ab.cpp"
#include "order_premutions/spend_ab_receive_ba.cpp"
#include "order_premutions/spend_abc_receive_abc.cpp"
#include "order_premutions/spend_abc_receive_acb.cpp"
#include "order_premutions/spend_abc_receive_bac.cpp"

#include "tests_imports_validation.h"

TestsImportsValidation::TestsImportsValidation()
{

}

bool TestsImportsValidation::doTests()
{
  testDoubleSpend_spend_abc_receive_bac();
  testDoubleSpend_spend_abc_receive_acb();
  testDoubleSpend_spend_abc_receive_abc();
  testDoubleSpend_spend_ab_receive_ba();
  testDoubleSpend_spend_ab_receive_ab();

  return true;
}
