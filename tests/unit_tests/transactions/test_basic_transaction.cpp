#include "stable.h"

#include "lib/block/document_types/document.h"
#include "lib/block/document_types/basic_tx_document.h"
#include "lib/transactions/basic_transactions/basic_transaction_handler.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_set.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_document.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/individual_signature.h"

#include "test_basic_transaction.h"

TestBasicTransaction::TestBasicTransaction()
{

}

bool TestBasicTransaction::doTests()
{
  testTransaction1();
  return true;
}

/**
        aome usefull replace map
  m_private_keys            =>  the_private_keys
  m_unlock_sets             =>  uSets
  m_left_hash               =>  lHash
  m_salt                    =>  salt
  m_signature_sets          =>  sSets
  m_merkle_proof            =>  mProof
  m_input_time_lock_strict  =>  iTLockSt
  m_input_time_lock         =>  iTLock
  m_output_time_lock        =>  oTLock
  m_permitted_to_pledge     =>  pPledge
  m_permitted_to_delegate   =>  pDelegate
  m_signature_key           =>  sKey
  m_signature_type          =>  sType
  m_signature_ver           =>  sVer


 */

bool TestBasicTransaction::testTransaction1()
{
  QStringList envolved_addresses = {};
  CMPAIValueT spendable_amount = 0;
  QHash<CCoinCodeT, TInput> inputs {};
  std::vector<TOutput> outputs {};
  bool validate_res;
  QV2DicT used_coins_dict {};
  QV2DicT invalid_coins_dict {};

  QString description = "";
  CSigIndexT unlocker_index = 0;
  CCoinCodeT coine_code_1 = "4c8caf434d338255235439637424794d8657806422dcc47f69198398aeb634de:1";
  CAddressT coin_1_owner = "im1xqekywtpvenxzwp58yekywfsx4jnyctzxymrjwtx8qerqe3hv56qe09ed3";
  CMPAIValueT coin_1_value = CConsts::ONE_MILLION * CConsts::ONE_MILLION;
  CMPAIValueT max_data_process_cost = 10000 * CConsts::ONE_MILLION;
  QStringList a_coin_segments = coine_code_1.split(":");
  inputs[coine_code_1] = TInput {
    a_coin_segments[0],
    static_cast<CInputIndexT>(QVariant::fromValue(a_coin_segments[1]).toUInt()),
    coin_1_owner,
    coin_1_value};

  outputs.emplace_back(TOutput {
    "im1xq6kxcnyx5urzdrxvesn2v33xvmnswpkv3jxvvfnxgcrserxxdskvskywkh",
    0,
    CConsts::OUTPUT_CHANGEBACK}); // change back

  outputs.emplace_back(TOutput {
    "im1xqerwc3jvyen2ctzvyux2efcxuukvcn98qergcesxyer2en98q6nz0rqnx3",
    5000000,
    CConsts::OUTPUT_NORMAL}); // recipient

  QString address_details = R"(
     {"m_account_address":"im1xqekywtpvenxzwp58yekywfsx4jnyctzxymrjwtx8qerqe3hv56qe09ed3","m_merkle_root":"e5a94f6fad085ff6c5e923f1ce2d85e74d1b6b6e161281c6425ddcb342f9b748","m_merkle_version":"0.1.0","the_private_keys":{"1055db05858b3894":["4d2fd57178c99fec70903d00f8528f781b0bfc3728cc81f4fc686732095ae62e","328351f0feb1331b217adf1bfe1fd325faec1a93aa2a54f4f8ca72a15749f336"],"1bdb153dc3e8bfb2":["f85554ebabf9f4a6ca5a7eca2f117ce1dd152143d67c2d7bef5b896ac727e91d","328351f0feb1331b217adf1bfe1fd325faec1a93aa2a54f4f8ca72a15749f336"],"409d9c0740873895":["4d2fd57178c99fec70903d00f8528f781b0bfc3728cc81f4fc686732095ae62e","f85554ebabf9f4a6ca5a7eca2f117ce1dd152143d67c2d7bef5b896ac727e91d"]},"uSets":[{"lHash":"","mProof":["r.7a5d9f2e7645dd0d6ee45240db585f486be907ef689a8c37bb9b9d1e1394971b","r.a9974a8dacf2ab5ceb5409f93a62e814206a7b4bf874aaa188034aea7cf454bc"],"salt":"409d9c0740873895","sSets":[{"iTLock":0,"iTLockSt":0,"oTLock":0,"pDelegate":"Y","pPledge":"Y","sKey":"02bec6b044062a2ba8498095dd12234cebf0cb51969c1186d6e9c9cbeeae3175e1"},{"iTLock":0,"iTLockSt":0,"oTLock":0,"pDelegate":"N","pPledge":"N","sKey":"02414efcf8f403c583a58b5fc762e48c8ce121f8a42574eb60d1c3ba562c5b7c8e"}],"sType":"Strict","sVer":"0.0.1"},{"lHash":"","mProof":["r.leave_4","l.abfe9c460dac1b1798568a848755555965710c7986d30273786bffa6b84e9432"],"salt":"1bdb153dc3e8bfb2","sSets":[{"iTLock":0,"iTLockSt":0,"oTLock":0,"pDelegate":"N","pPledge":"N","sKey":"02414efcf8f403c583a58b5fc762e48c8ce121f8a42574eb60d1c3ba562c5b7c8e"},{"iTLock":0,"iTLockSt":0,"oTLock":0,"pDelegate":"N","pPledge":"N","sKey":"02a3706ea6fb55639891b20df233d8c725cd011594b0f5ea78cf1d1d601b407b65"}],"sType":"Strict","sVer":"0.0.1"},{"lHash":"41ad366adb674ae2c6bb2b6367438484217e6f0eb4ebc70528a496ff95eb6c91","mProof":["r.a9974a8dacf2ab5ceb5409f93a62e814206a7b4bf874aaa188034aea7cf454bc"],"salt":"1055db05858b3894","sSets":[{"iTLock":0,"iTLockSt":0,"oTLock":0,"pDelegate":"Y","pPledge":"Y","sKey":"02bec6b044062a2ba8498095dd12234cebf0cb51969c1186d6e9c9cbeeae3175e1"},{"iTLock":0,"iTLockSt":0,"oTLock":0,"pDelegate":"N","pPledge":"N","sKey":"02a3706ea6fb55639891b20df233d8c725cd011594b0f5ea78cf1d1d601b407b65"}],"sType":"Strict","sVer":"0.0.1"}]}
  )";
  QJsonObject Jaddress_details = CUtils::parseToJsonObj(address_details);
  inputs[coine_code_1].m_unlock_set = Jaddress_details.value("uSets").toArray()[unlocker_index].toObject();;
  QString salt = inputs[coine_code_1].m_unlock_set.value("salt").toString();
  QJsonArray private_keys = Jaddress_details.value("the_private_keys").toObject()[salt].toArray();
  inputs[coine_code_1].m_private_keys = CUtils::convertJSonArrayToQStringList(private_keys);
  CLog::log("\nTest Coin to be sepnt: " + inputs[coine_code_1].dumpMe(), "trx", "info");

  used_coins_dict[coine_code_1] = QVDicT {
    {"ut_coin", coine_code_1},
    {"ut_o_address", coin_1_owner},
    {"ut_o_value", QVariant::fromValue(coin_1_value)},
    {"ut_ref_creation_date", CUtils::getNow()}};

  auto trx_template = BasicTransactionTemplate {
    inputs,
    outputs,
    max_data_process_cost,
    0,  // dDPCost
    description};
  trx_template.m_tpl_backers_addresses = QStringList {"im1xq6kxcnyx5urzdrxvesn2v33xvmnswpkv3jxvvfnxgcrserxxdskvskywkh"};

  auto[res_status, res_msg, trx, dp_cost] = BasicTransactionHandler::makeATransaction(trx_template);
  if (!res_status)
    CUtils::exiter("failed in test Transaction 1 cost" + CUtils::microPAIToPAI6(dp_cost) + res_msg, 78);

  CLog::log("\nTest transaction 1: " + trx->safeStringifyDoc(), "trx", "info");





  // signatures control
  validate_res = trx->validateSignatures(
    used_coins_dict,
    {},
    "dummy_block_hash");
  if (!validate_res)
    CUtils::exiter("failed in test Transaction 1 validate Signatures", 78);

  // control transaction hash
  validate_res = trx->validateGeneralRulsForTransaction();
  if (!validate_res)
    CUtils::exiter("failed in test Transaction 1 validate General Ruls For Transaction", 78);

  auto[equation_check_res, msg, total_inputs_amounts, total_outputs_amounts] = trx->equationCheck(
    used_coins_dict,
    invalid_coins_dict);
  if (!equation_check_res)
    CUtils::exiter("failed in test Transaction 1 equation Check", 78);

  if (total_inputs_amounts != total_outputs_amounts)
    CUtils::exiter("failed in test Transaction 1 equation Check L != R ", 78);

  return true;
}
