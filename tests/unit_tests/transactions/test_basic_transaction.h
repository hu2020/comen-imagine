#ifndef TESTBASICTRANSACTION_H
#define TESTBASICTRANSACTION_H


class TestBasicTransaction
{
public:
  TestBasicTransaction();

  static bool testTransaction1();
  static bool doTests();

};

#endif // TESTBASICTRANSACTION_H
