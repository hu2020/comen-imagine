#include "stable.h"
#include "lib/ccrypto.h"
#include "lib/pgp/cpgp.h"
#include "tests_pgp.h"

TestsPGP::TestsPGP()
{

}

void TestsPGP::doTests()
{
  test2_pgp();
  test1_aes();
  test1_pgp();
}

bool TestsPGP::test1_aes()
{
  QString secret_key = "12345678901234567890123456789012";
  QString initialization_vector = "1234567890123416";
  QString aes_version = "0.2.0";
  QString clear_text = "hello world";
  {
    auto [status_aes_enc, encrypted] = CCrypto::AESencrypt(
      clear_text,
      secret_key,
      initialization_vector,
      aes_version);

    if (!status_aes_enc)
    {
      CLog::log("AESencrypt failed 1: ", "app", "fatal");
      exit(8491);
    }

    auto[status_aes_dec, recovered] = CCrypto::AESdecrypt(encrypted, secret_key, initialization_vector, aes_version);
    Q_UNUSED(status_aes_dec);
    if (recovered != clear_text)
    {
      CLog::log("AES encrypt/decrypt failed 1: ", "app", "fatal");
      exit(8491);
    }
  }

  {
    aes_version = "0.0.0";
    auto [status_aes_enc, encrypted] = CCrypto::AESencrypt(clear_text, secret_key, initialization_vector, aes_version);
    if (!status_aes_enc)
    {
      CLog::log("AESencrypt failed 2: ", "app", "fatal");
      exit(8491);
    }

    auto[status_aes_dec, recovered] = CCrypto::AESdecrypt(encrypted, secret_key, initialization_vector, aes_version);
    if (!status_aes_dec)
      CUtils::exiter("AES encrypt/decrypt failed 12: ", 8491);

    if (recovered != clear_text)
      CUtils::exiter("AES encrypt/decrypt failed 2: ", 18491);
  }
  return true;
}

bool TestsPGP::test1_pgp()
  {
  if (CConsts::CURRENT_AES_VERSION != "0.2.0")
    return true;

      //    cipher = oo.value("encrypted").toString().toStdString();
      //    cipher = CCrypto::base64Decode(cccipher).toStdString();


  QString senderPrvKey = "-----BEGIN PRIVATE KEY-----\nMIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQDWrsk6SSj2GV4n\noFs4KpcFJ/Sd8bXMXfWWLN20yQXjP94FBENLErvFf5/jT7SKsO5Pq1owYTvunzk6\nwgbmzx0GkPRyYwlVOhgBHwmCO6nK9cGLYukuO8N18CD/YTHbxVlYRMiBFFKMsDZ7\n8q6Hy8BYwgpRDbQVYdpET/eEsZxUvOt3dD1PglsNYU0mfb1Om4ZpOUknW/qli/PK\n++kJ1WwztN9bi4kDrN1tYev0AI4gNxteOsBPAx4h5NHbhmLBNcoM8JDt0QA+e2p8\nrnpYNrjqDTu226uYfkc08bjHwW4IjQ1GOj7kiUnr7hzZuobqzMPtFNqfYmVD6vsh\nCsgx5rnArxSFGCJ5FDlgBqVietnAhT40Ymm1wizZwYYuntb3BshHQh672puvDbAR\nxwJQLX5Pw/Yij88yRkllFWZ36a5moecxzR6rC24sc8YUlIJHcUysswlaOU0JvKMq\ngZuuFzrszkzllwlgymXeuxpO+P2uAXOL+iep/vEfup+dRBBvt+d7SQBNjL1J/Z3o\nncBxlLiK/fHJ4MRiaBMAIx5iZGs8HdfdQ0iP0sAr2ODjIaUMMbzwfj0KxSfMNF+u\ne/XAGqWqyMRHKozSU8pyh/OZxOUrzJqHojeqk+vCtS9Qh3eL9HI5HiYDJS7F7eCx\nj26QlmXtPeN8VmNygK25iamK7t4AmQIDAQABAoICAQCuVdoLfwXIc+lF/K6W+d8R\nzNSUvoiHoXWhsmWIPgaN+HtvT3rmKBxb0bwhd8SBLBUalWf0CcIFkT+EO7QnEuHb\nRzN7AHm/wCFmJ7ItFA5aRyAY9QWvCdE8oPfq6x71/VkEmTbJu5sdrKtSFE0u8YMN\nkOWX1Kz9AC2jc9zo4OK23YPJyZdQTedrAi0IqOtmPl30bRS+5a6xaOldWRxxY+Mu\nSh9n0/Q8C9D01qQpzZmdB6Mt7j/k6f86Pvs8bfEcPXYK9AB9X0/3bXmYLogjwqx/\nRKfNYq4Cc+mXUQRhEu8r+n9hHEOeiu6ArSi4lwShAA/LIzg2Xk0lzliJCJxyCY+l\nMDmhlXnZOJnC/bWz5nUgJ2jXHQ6bOvrHpyx8w2zQrDdiMRfDuYGQqSBZagpK6mgQ\nOtrod3ewZ6UKA64xFF/oVMqjxbf5i9hUOxJf6UxDqvePWB7+ESj/GSIB92SDa+YE\nORsJRduBBBgoMqZyZYBHbydtKgMmqlbc+tmyHWIDhZ6buKzU5UkYUs43waxXKSYg\n/Y8x7hwr8taYy/s/HuR5lPPEbmjorHy292t22SJ6TsjN3RPwm7m7kPG5dJO1zTKc\nneZ+UNsgwfax1kStR1+Fl4bl7dPPSXG3guLOqJYtzGMm9maK+8HbyExP9owg/3Yu\naoOhv/3uZkv6yx8OI8ZmQQKCAQEA922wuMcsNazQDzjeqfdyKOi70UUSZUlkSJ6M\nAMim7ctGjR7DJP080Uw5KnRm+qZpWjlFmjNVVTpcm30BQPHfdqlwdRABA1XbcsfU\nWitKq6tZfjsY4CkSvA877259cn2fiiKe8sVrd5nI4zLAZtMlSrlM3lYlp0/baJ/X\nUTbN46MyN2N4AyR2V6dh/JhEZ0Wilrn58rdIeBCGD0CS7OGlR661oN5hEqmfRCZt\nPMtmuy6OwyGUALwNp1l3LpbhzaJvgdRlMoGYrXOfpPA7hXmzpV1tkr6v2uw8k/ru\nluEWH7vLMTLmDdFyqGXpy6hdp1F/yUBASCWffhGYZ99kCUM2RQKCAQEA3h6xDO5x\nDLZ9f28zbT/wXcqY68dWcwtaEFljPQQ67XwAEd3B3ec0cMJEqChy+q8imo5XiBuz\nOwhx2ud/TiBYqQsIl1xrjq0MURmFJnt7krj2AURxBsFpauXOZLyi857EFT18dEF2\nX72dYcWhVMFvkZQvv11MepVSUcZLyuy2+ga/ImNVtcih8xZlTgks5qfKbWmmCURU\n/hnbNmJnWB55EGV80pZnD8vgvW4twclyvPGQvx61IOt4ADuFXuS3PMpin0ukNXQt\nLSmlqoKDnQDti+eWmmwBc3AAZxOM2ZJUzRuezyhTyDQYIj/EbBy/JOt0ZfJtxqDi\nUyj3rBWR9XTgRQKCAQAyAFzCOp3Yiv/Q07FLr9HGKqIp/EdnaJl3g3LPhb0WOQoc\n4qaVBwgPU9QJnFRjOtX3gbNrIgyQmKa2VWC3eymplTxAq+dIgUVDwNZNEoqYfpwr\nQk4YnrRGhN2b2TxIstJW3AA7F2JMXSuXnl37aZgTrba3B7m8QgsY7ZRZuTWKVySC\nxPsvx2KS/CR1I2BPbmcZBqRYRGnEubIdyvLmJAN+pPPXepkraFPjNdDFhGVSVmpL\nTLHYJCiuNQo8xAkFwVm/Zui8eCwegUs1zcZ5hsqjFGpcSWTIXQ0w6njkzL/n0c+S\nNbL//9+n4FCK849ZjMLCLNXQvqmolG20MBTLOmXxAoIBAEH/mpn+C2c8niOMVhqD\nIhesSC3eqAg8+kDbXopVxHCmJJsqah1WQpWyk5Xnlup3uW91FfsiR+olgzcJDCGZ\nqN5pod7fnjHhp0Q9sfuSO1CIVshfnrnkJD+YEvCJwF1k8lxhTaZ5gQPHoojXoS0+\n4Li/8EQLI9tUnQYhRooANgoqkQZVv7To7X3+TJGYVlgeyR4jgytDu4FZC3KoYI6l\nV1GQ5jroZ60g5iFDKKNpCQetO1YeeLFUZe6jwOhGS0/rzswIPL2JTJcbxdbZOuQe\nz3Z/lJdDulajk+sMuxA2062yJEjncaWOHRGoIuQA0qbUs1xLYWTgjv8osBbYverS\nFIUCggEBAK/vSY3VOo+L3LJZOhrY66jWTMvP85QFoA/I6Fn7jI5zMa+s3wmFtVeF\n4Gu3omIWxFl1hubpZSElF+ST3jUnjlUDP5aStDapRxLWQjLnBqTTSgH2fxrLIPAq\n0E6LFhIsReGep4UautAwAFs5gbU7E7jVCochnhxeISgLWlBgZSMxiFJmBErxLfrJ\nPBWaStN1IN5qBRFx0985c1PSC1idCB9+0yb6b+aGs0JOA47tChMMquiQLSru3uPd\nvRGkXCblGXUPyrKAojYfynPFm11Xn+04XFM5ya+iI6gl+FnQnFgkvbqtg4KMiFa0\nh7pHev7nX0AT1oNFVxayTQdjOmyEDfo=\n-----END PRIVATE KEY-----";
  QString senderPubKey = "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA1q7JOkko9hleJ6BbOCqX\nBSf0nfG1zF31lizdtMkF4z/eBQRDSxK7xX+f40+0irDuT6taMGE77p85OsIG5s8d\nBpD0cmMJVToYAR8JgjupyvXBi2LpLjvDdfAg/2Ex28VZWETIgRRSjLA2e/Kuh8vA\nWMIKUQ20FWHaRE/3hLGcVLzrd3Q9T4JbDWFNJn29TpuGaTlJJ1v6pYvzyvvpCdVs\nM7TfW4uJA6zdbWHr9ACOIDcbXjrATwMeIeTR24ZiwTXKDPCQ7dEAPntqfK56WDa4\n6g07tturmH5HNPG4x8FuCI0NRjo+5IlJ6+4c2bqG6szD7RTan2JlQ+r7IQrIMea5\nwK8UhRgieRQ5YAalYnrZwIU+NGJptcIs2cGGLp7W9wbIR0Ieu9qbrw2wEccCUC1+\nT8P2Io/PMkZJZRVmd+muZqHnMc0eqwtuLHPGFJSCR3FMrLMJWjlNCbyjKoGbrhc6\n7M5M5ZcJYMpl3rsaTvj9rgFzi/onqf7xH7qfnUQQb7fne0kATYy9Sf2d6J3AcZS4\niv3xyeDEYmgTACMeYmRrPB3X3UNIj9LAK9jg4yGlDDG88H49CsUnzDRfrnv1wBql\nqsjERyqM0lPKcofzmcTlK8yah6I3qpPrwrUvUId3i/RyOR4mAyUuxe3gsY9ukJZl\n7T3jfFZjcoCtuYmpiu7eAJkCAwEAAQ==\n-----END PUBLIC KEY-----";
  QString receiverPrvKey = "-----BEGIN PRIVATE KEY-----\nMIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQDWrsk6SSj2GV4n\noFs4KpcFJ/Sd8bXMXfWWLN20yQXjP94FBENLErvFf5/jT7SKsO5Pq1owYTvunzk6\nwgbmzx0GkPRyYwlVOhgBHwmCO6nK9cGLYukuO8N18CD/YTHbxVlYRMiBFFKMsDZ7\n8q6Hy8BYwgpRDbQVYdpET/eEsZxUvOt3dD1PglsNYU0mfb1Om4ZpOUknW/qli/PK\n++kJ1WwztN9bi4kDrN1tYev0AI4gNxteOsBPAx4h5NHbhmLBNcoM8JDt0QA+e2p8\nrnpYNrjqDTu226uYfkc08bjHwW4IjQ1GOj7kiUnr7hzZuobqzMPtFNqfYmVD6vsh\nCsgx5rnArxSFGCJ5FDlgBqVietnAhT40Ymm1wizZwYYuntb3BshHQh672puvDbAR\nxwJQLX5Pw/Yij88yRkllFWZ36a5moecxzR6rC24sc8YUlIJHcUysswlaOU0JvKMq\ngZuuFzrszkzllwlgymXeuxpO+P2uAXOL+iep/vEfup+dRBBvt+d7SQBNjL1J/Z3o\nncBxlLiK/fHJ4MRiaBMAIx5iZGs8HdfdQ0iP0sAr2ODjIaUMMbzwfj0KxSfMNF+u\ne/XAGqWqyMRHKozSU8pyh/OZxOUrzJqHojeqk+vCtS9Qh3eL9HI5HiYDJS7F7eCx\nj26QlmXtPeN8VmNygK25iamK7t4AmQIDAQABAoICAQCuVdoLfwXIc+lF/K6W+d8R\nzNSUvoiHoXWhsmWIPgaN+HtvT3rmKBxb0bwhd8SBLBUalWf0CcIFkT+EO7QnEuHb\nRzN7AHm/wCFmJ7ItFA5aRyAY9QWvCdE8oPfq6x71/VkEmTbJu5sdrKtSFE0u8YMN\nkOWX1Kz9AC2jc9zo4OK23YPJyZdQTedrAi0IqOtmPl30bRS+5a6xaOldWRxxY+Mu\nSh9n0/Q8C9D01qQpzZmdB6Mt7j/k6f86Pvs8bfEcPXYK9AB9X0/3bXmYLogjwqx/\nRKfNYq4Cc+mXUQRhEu8r+n9hHEOeiu6ArSi4lwShAA/LIzg2Xk0lzliJCJxyCY+l\nMDmhlXnZOJnC/bWz5nUgJ2jXHQ6bOvrHpyx8w2zQrDdiMRfDuYGQqSBZagpK6mgQ\nOtrod3ewZ6UKA64xFF/oVMqjxbf5i9hUOxJf6UxDqvePWB7+ESj/GSIB92SDa+YE\nORsJRduBBBgoMqZyZYBHbydtKgMmqlbc+tmyHWIDhZ6buKzU5UkYUs43waxXKSYg\n/Y8x7hwr8taYy/s/HuR5lPPEbmjorHy292t22SJ6TsjN3RPwm7m7kPG5dJO1zTKc\nneZ+UNsgwfax1kStR1+Fl4bl7dPPSXG3guLOqJYtzGMm9maK+8HbyExP9owg/3Yu\naoOhv/3uZkv6yx8OI8ZmQQKCAQEA922wuMcsNazQDzjeqfdyKOi70UUSZUlkSJ6M\nAMim7ctGjR7DJP080Uw5KnRm+qZpWjlFmjNVVTpcm30BQPHfdqlwdRABA1XbcsfU\nWitKq6tZfjsY4CkSvA877259cn2fiiKe8sVrd5nI4zLAZtMlSrlM3lYlp0/baJ/X\nUTbN46MyN2N4AyR2V6dh/JhEZ0Wilrn58rdIeBCGD0CS7OGlR661oN5hEqmfRCZt\nPMtmuy6OwyGUALwNp1l3LpbhzaJvgdRlMoGYrXOfpPA7hXmzpV1tkr6v2uw8k/ru\nluEWH7vLMTLmDdFyqGXpy6hdp1F/yUBASCWffhGYZ99kCUM2RQKCAQEA3h6xDO5x\nDLZ9f28zbT/wXcqY68dWcwtaEFljPQQ67XwAEd3B3ec0cMJEqChy+q8imo5XiBuz\nOwhx2ud/TiBYqQsIl1xrjq0MURmFJnt7krj2AURxBsFpauXOZLyi857EFT18dEF2\nX72dYcWhVMFvkZQvv11MepVSUcZLyuy2+ga/ImNVtcih8xZlTgks5qfKbWmmCURU\n/hnbNmJnWB55EGV80pZnD8vgvW4twclyvPGQvx61IOt4ADuFXuS3PMpin0ukNXQt\nLSmlqoKDnQDti+eWmmwBc3AAZxOM2ZJUzRuezyhTyDQYIj/EbBy/JOt0ZfJtxqDi\nUyj3rBWR9XTgRQKCAQAyAFzCOp3Yiv/Q07FLr9HGKqIp/EdnaJl3g3LPhb0WOQoc\n4qaVBwgPU9QJnFRjOtX3gbNrIgyQmKa2VWC3eymplTxAq+dIgUVDwNZNEoqYfpwr\nQk4YnrRGhN2b2TxIstJW3AA7F2JMXSuXnl37aZgTrba3B7m8QgsY7ZRZuTWKVySC\nxPsvx2KS/CR1I2BPbmcZBqRYRGnEubIdyvLmJAN+pPPXepkraFPjNdDFhGVSVmpL\nTLHYJCiuNQo8xAkFwVm/Zui8eCwegUs1zcZ5hsqjFGpcSWTIXQ0w6njkzL/n0c+S\nNbL//9+n4FCK849ZjMLCLNXQvqmolG20MBTLOmXxAoIBAEH/mpn+C2c8niOMVhqD\nIhesSC3eqAg8+kDbXopVxHCmJJsqah1WQpWyk5Xnlup3uW91FfsiR+olgzcJDCGZ\nqN5pod7fnjHhp0Q9sfuSO1CIVshfnrnkJD+YEvCJwF1k8lxhTaZ5gQPHoojXoS0+\n4Li/8EQLI9tUnQYhRooANgoqkQZVv7To7X3+TJGYVlgeyR4jgytDu4FZC3KoYI6l\nV1GQ5jroZ60g5iFDKKNpCQetO1YeeLFUZe6jwOhGS0/rzswIPL2JTJcbxdbZOuQe\nz3Z/lJdDulajk+sMuxA2062yJEjncaWOHRGoIuQA0qbUs1xLYWTgjv8osBbYverS\nFIUCggEBAK/vSY3VOo+L3LJZOhrY66jWTMvP85QFoA/I6Fn7jI5zMa+s3wmFtVeF\n4Gu3omIWxFl1hubpZSElF+ST3jUnjlUDP5aStDapRxLWQjLnBqTTSgH2fxrLIPAq\n0E6LFhIsReGep4UautAwAFs5gbU7E7jVCochnhxeISgLWlBgZSMxiFJmBErxLfrJ\nPBWaStN1IN5qBRFx0985c1PSC1idCB9+0yb6b+aGs0JOA47tChMMquiQLSru3uPd\nvRGkXCblGXUPyrKAojYfynPFm11Xn+04XFM5ya+iI6gl+FnQnFgkvbqtg4KMiFa0\nh7pHev7nX0AT1oNFVxayTQdjOmyEDfo=\n-----END PRIVATE KEY-----";
  QString receiverPubKey = "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA1q7JOkko9hleJ6BbOCqX\nBSf0nfG1zF31lizdtMkF4z/eBQRDSxK7xX+f40+0irDuT6taMGE77p85OsIG5s8d\nBpD0cmMJVToYAR8JgjupyvXBi2LpLjvDdfAg/2Ex28VZWETIgRRSjLA2e/Kuh8vA\nWMIKUQ20FWHaRE/3hLGcVLzrd3Q9T4JbDWFNJn29TpuGaTlJJ1v6pYvzyvvpCdVs\nM7TfW4uJA6zdbWHr9ACOIDcbXjrATwMeIeTR24ZiwTXKDPCQ7dEAPntqfK56WDa4\n6g07tturmH5HNPG4x8FuCI0NRjo+5IlJ6+4c2bqG6szD7RTan2JlQ+r7IQrIMea5\nwK8UhRgieRQ5YAalYnrZwIU+NGJptcIs2cGGLp7W9wbIR0Ieu9qbrw2wEccCUC1+\nT8P2Io/PMkZJZRVmd+muZqHnMc0eqwtuLHPGFJSCR3FMrLMJWjlNCbyjKoGbrhc6\n7M5M5ZcJYMpl3rsaTvj9rgFzi/onqf7xH7qfnUQQb7fne0kATYy9Sf2d6J3AcZS4\niv3xyeDEYmgTACMeYmRrPB3X3UNIj9LAK9jg4yGlDDG88H49CsUnzDRfrnv1wBql\nqsjERyqM0lPKcofzmcTlK8yah6I3qpPrwrUvUId3i/RyOR4mAyUuxe3gsY9ukJZl\n7T3jfFZjcoCtuYmpiu7eAJkCAwEAAQ==\n-----END PUBLIC KEY-----";

  QString message = "hello world";
  auto [status_pgp, pgpEncrypted] = CPGP::encryptPGP(
    message,
    senderPrvKey,
    receiverPubKey,
    "12345678901234567890123456789012",
    "1234567890123456",
    true,
    true
  );
  if (!status_pgp)
    CUtils::exiter("Failed in CPGP::encryptPGP 1: ", 23);

  pgpEncrypted = CUtils::breakByBR(pgpEncrypted);
  pgpEncrypted = CPGP::wrapPGPEnvelope(pgpEncrypted);

  // decrypting pgp
  {
    pgpEncrypted = CPGP::stripPGPEnvelope(pgpEncrypted);
    pgpEncrypted = CUtils::stripBR(pgpEncrypted);
    CPGPMessage decryptedMessage = CPGP::decryptPGP(
      pgpEncrypted,
      receiverPrvKey,
      senderPubKey
    );

    if (!decryptedMessage.m_decryption_status)
    {
      CLog::log("ERROR in CPGP::decryptPGP 1: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_message != message)
    {
      CLog::log("ERROR in CPGP::decryptPGP message1: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_is_signed != true)
    {
      CLog::log("ERROR in CPGP::decryptPGP m_is_verified: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_is_verified != true)
    {
      CLog::log("ERROR in CPGP::decryptPGP m_is_verified: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_is_authenticated != true)
    {
      CLog::log("ERROR in CPGP::decryptPGP m_aes_version1: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_aes_version != "0.2.0")
    {
      CLog::log("ERROR in CPGP::decryptPGP m_aes_version2: " , "app", "fatal");
      exit(891);
    }

  }

  return true;
}

bool TestsPGP::test2_pgp()
  {
  QString message = "hello world";
  auto [status_pgp, pgpEncrypted] = CPGP::encryptPGP(
    message,
    "",
    "",
    "",
    "",
    true,
    false
  );
  if (!status_pgp)
    CUtils::exiter("Failed in CPGP::encryptPGP 2: ", 23);

  pgpEncrypted = CUtils::breakByBR(pgpEncrypted);
  pgpEncrypted = CPGP::wrapPGPEnvelope(pgpEncrypted);

  // decrypting pgp
  {
    pgpEncrypted = CPGP::stripPGPEnvelope(pgpEncrypted);
    pgpEncrypted = CUtils::stripBR(pgpEncrypted);
    CPGPMessage decryptedMessage = CPGP::decryptPGP(
      pgpEncrypted,
      "",
      ""
    );

    if (!decryptedMessage.m_decryption_status)
    {
      CLog::log("ERROR in CPGP::decryptPGP 1: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_message != message)
    {
      CLog::log("ERROR in CPGP::decryptPGP message2: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_is_signed != false)
    {
      CLog::log("ERROR in CPGP::decryptPGP m_is_verified: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_is_verified != false)
    {
      CLog::log("ERROR in CPGP::decryptPGP m_is_verified: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_is_authenticated != false)
    {
      CLog::log("ERROR in CPGP::decryptPGP m_aes_version3: " , "app", "fatal");
      exit(891);
    }
    if (decryptedMessage.m_aes_version != "")
    {
      CLog::log("ERROR in CPGP::decryptPGP m_aes_version4: " , "app", "fatal");
      exit(891);
    }

  }

  return true;
}
