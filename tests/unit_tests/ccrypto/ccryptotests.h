#ifndef CCRYPTOTESTS_H
#define CCRYPTOTESTS_H




class TestsCCrypto
{
public:
  TestsCCrypto();
  static void doTests();

  static bool testsBech32();
  static bool test_b64();
  static void autoGenNativeKeyPairTests();

  static void autoGenECDSAKeyPairTests();

  static void genCryptoTests();
};

#endif // CCRYPTOTESTS_H
