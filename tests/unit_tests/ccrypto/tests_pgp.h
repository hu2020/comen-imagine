#ifndef TESTSPGP_H
#define TESTSPGP_H



class TestsPGP
{
public:
  TestsPGP();
  static void doTests();
  static bool test1_aes();
  static bool test1_pgp();
  static bool test2_pgp();
};

#endif // TESTSPGP_H
