#include <math.h>
#include <vector>

#include <QList>
#include <QVariant>

#include "constants.h"

#include "lib/clog.h"
#include "lib/ccrypto.h"

#include "lib/utils/cmerkle.h"
#include "merkle2.h"

CMerkleTests2::CMerkleTests2()
{
}

void CMerkleTests2::doTests()
{
  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2"}, "hashed", "noHash");
    Q_UNUSED(version);
    if (root != "12")
    {
      CLog::log("ERROR in CMerkle::generate 2: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof != QStringList{"r.2"})
    {
      CLog::log("ERROR in CMerkle::generate 2: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_left_hash != "")
    {
      CLog::log("ERROR in CMerkle::generate 2: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_merkle_proof != QStringList{})
    {
      CLog::log("ERROR in CMerkle::generate 2: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_left_hash != "1")
    {
      CLog::log("ERROR in CMerkle::generate 2: " , "app", "fatal");
      exit(1098);
    }
    if (leaves != 2)
    {
      CLog::log("ERROR in CMerkle::generate 2: " , "app", "fatal");
      exit(1098);
    }
    if (levels != 2)
    {
      CLog::log("ERROR in CMerkle::generate 2: " , "app", "fatal");
      exit(1098);
    }
  }

  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2"}, "hashed", "aliasHash");
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "h(12)")
    {
      CLog::log("ERROR in CMerkle::generate 2: " , "app", "fatal");
      exit(1098);
    }

    if (root != CMerkle::getRootByAProve("1", verifies["1"].m_merkle_proof, verifies["1"].m_left_hash, "hashed", "aliasHash")
       )
    {
      CLog::log("ERROR in CMerkle::generate getRootByAProve 2: " , "app", "fatal");
      exit(1098);
    }
    if (root !=
       CMerkle::getRootByAProve("2", verifies["2"].m_merkle_proof, verifies["2"].m_left_hash, "hashed", "aliasHash")
       )
    {
      CLog::log("ERROR in CMerkle::generate getRootByAProve 2: " , "app", "fatal");
      exit(1098);
    }
  }



  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2"}, "string", "aliasHash");
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "h(h(1)h(2))")
    {
      CLog::log("ERROR in CMerkle::merkle tree root for 1,2 aliasHash 2: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["h(1)"].m_merkle_proof != QStringList{"r.h(2)"})
    {
      CLog::log("ERROR in CMerkle::merkle tree root for 1,2 aliasHash 2: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["h(2)"].m_merkle_proof != QStringList{})
    {
      CLog::log("ERROR in CMerkle::merkle tree root for 1,2 aliasHash 2: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["h(2)"].m_left_hash != "h(1)")
    {
      CLog::log("ERROR in CMerkle::merkle tree root for 1,2 aliasHash 2: " , "app", "fatal");
      exit(1098);
    }
  }


  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2"}, "string");
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != CCrypto::keccak256(CCrypto::keccak256("1") + CCrypto::keccak256("2")))
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
    if (verifies[CCrypto::keccak256("1")].m_merkle_proof != QStringList{"r." + CCrypto::keccak256("2")})
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
    if (verifies[CCrypto::keccak256("2")].m_merkle_proof != QStringList{})
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
  }


  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2"});
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != CCrypto::keccak256("12"))
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
    if (root != "7f8b6b088b6d74c2852fc86c796dca07b44eed6fb3daf5e6b59f7c364db14528")
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
  }


  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2"}, "hashed", "aliasHash");
    Q_UNUSED(root);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "h(12)")
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("1", verifies["1"].m_merkle_proof, verifies["1"].m_left_hash, "hashed", "aliasHash"))
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("2", verifies["2"].m_merkle_proof, verifies["2"].m_left_hash,"hashed", "aliasHash"))
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
  }



  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2"}, "string", "aliasHash");
    Q_UNUSED(root);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (verifies["h(1)"].m_merkle_proof != QStringList{"r.h(2)"})
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["h(2)"].m_merkle_proof != QStringList{})
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }

    if (verifies["h(2)"].m_left_hash != "h(1)")
    {
      CLog::log("ERROR in CMerkle::merkle root for 1,2 direct CCrypto::keccak256: " , "app", "fatal");
      exit(1098);
    }
  }


}

