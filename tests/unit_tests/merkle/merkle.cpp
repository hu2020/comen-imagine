#include "stable.h"

#include "merkle1.h"
#include "merkle2.h"
#include "merkle3.h"  // FIXME: implement ALL merkle tests
#include "test_cmerkle_5.h"  // FIXME: implement ALL merkle tests


#include "merkle.h"

CMerkleTests::CMerkleTests()
{
}

void CMerkleTests::doTests()
{
  TestCMerkle5::doTests();
  CMerkleTests1::doTests();
  CMerkleTests2::doTests();
  CMerkleTests3::doTests();
  // FIXME: implement ALL merkle tests


}

