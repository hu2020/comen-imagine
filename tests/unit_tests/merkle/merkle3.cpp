#include <math.h>
#include <vector>

#include <QList>
#include <QVariant>

//#include "constants.h"
#include "stable.h"
#include "lib/ccrypto.h"

#include "lib/utils/cmerkle.h"

#include "merkle3.h"

CMerkleTests3::CMerkleTests3()
{
}

void CMerkleTests3::doTests()
{
  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "hashed", "noHash");
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);

    if (root != "123leave_4")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 1 root:" + root, "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_left_hash != "")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 2" , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof[0].midRef(0, 1) != "r")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 3" , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof[0].midRef(2) != "2")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 4" , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof[1].midRef(0, 1) != "r")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 5" , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof[1].midRef(2) != "3leave_4")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 6" , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_left_hash != "1")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 7" , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_left_hash != "")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 8a" , "app", "fatal");
      exit(1098);
    }
    if (verifies["leave_4"].m_left_hash != "3")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 8b" , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_merkle_proof[0].midRef(2) != "3leave_4")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 9" , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_merkle_proof[0].midRef(0, 1) != "r")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 10" , "app", "fatal");
      exit(1098);
    }
    if (verifies["leave_4"].m_merkle_proof[0].midRef(0, 1) != "l")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 11a" , "app", "fatal");
      exit(1098);
    }
    if (verifies["leave_4"].m_merkle_proof[0].midRef(2) != "12")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 12a" , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_merkle_proof[0].midRef(0, 1) != "r")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 11b" , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_merkle_proof[0].midRef(2) != "leave_4")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 12b" , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_merkle_proof[1].midRef(0, 1) != "l")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 11c" , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_merkle_proof[1].midRef(2) != "12")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 12c" , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("1", verifies["1"].m_merkle_proof, verifies["1"].m_left_hash, "hashed", "noHash"))
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 13" , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("2", verifies["2"].m_merkle_proof, verifies["2"].m_left_hash, "hashed", "noHash"))
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 14" , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("3", verifies["3"].m_merkle_proof, verifies["3"].m_left_hash, "hashed", "noHash"))
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 15" , "app", "fatal");
      exit(1098);
    }
  }



  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "hashed", "aliasHash");
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "h(h(12)h(3leave_4))")
    {
      CLog::log("ERROR in CMerkle::generate 3 a: " , "app", "fatal");
      exit(1098);
    }
  }

  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "string", "aliasHash");
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "h(h(h(1)h(2))h(h(3)h(leave_4)))")
    {
      CLog::log("ERROR in CMerkle::generate 3 b: " , "app", "fatal");
      exit(1098);
    }
  }

  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "string");
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != CCrypto::keccak256(CCrypto::keccak256(CCrypto::keccak256("1") + CCrypto::keccak256("2")) + CCrypto::keccak256(CCrypto::keccak256("3") + CCrypto::keccak256("leave_4"))) )
    {
      CLog::log("ERROR in CMerkle::generate 3 c: " , "app", "fatal");
      exit(1098);
    }
    if (verifies[CCrypto::keccak256("1")].m_merkle_proof != QStringList{"r." + CCrypto::keccak256("2"), "r." + CCrypto::keccak256(CCrypto::keccak256("3") + CCrypto::keccak256("leave_4"))} )
    {
      CLog::log("ERROR in CMerkle::generate 3 d: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("1", verifies[CCrypto::keccak256("1")].m_merkle_proof, verifies[CCrypto::keccak256("1")].m_left_hash, "string", "keccak256"))
    {
      CLog::log("ERROR in CMerkle::generate 3 e: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("2", verifies[CCrypto::keccak256("2")].m_merkle_proof, verifies[CCrypto::keccak256("2")].m_left_hash, "string", "keccak256"))
    {
      CLog::log("ERROR in CMerkle::generate 3 e: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("3", verifies[CCrypto::keccak256("3")].m_merkle_proof, verifies[CCrypto::keccak256("3")].m_left_hash, "string", "keccak256"))
    {
      CLog::log("ERROR in CMerkle::generate 3 f: " , "app", "fatal");
      exit(1098);
    }
  }


  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"});
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "6c4915f1849b0171846ef4d6d2abab6eeb3548a6c89d63c660b49ed738d4736a" )
    {
      CLog::log("ERROR in CMerkle::generate 3 g: root: " + root, "app", "fatal");
      exit(1098);
    }
  }


  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "hashed", "noHash");
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "123leave_4" )
    {
      CLog::log("ERROR in CMerkle::generate 3 h: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_left_hash != "" )
    {
      CLog::log("ERROR in CMerkle::generate 3 i: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof[0].midRef(0, 1).toString() != "r" )
    {
      CLog::log("ERROR in CMerkle::generate 3 j: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof[0].midRef(2).toString() != "2" )
    {
      CLog::log("ERROR in CMerkle::generate 3 k: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof[1].midRef(0, 1).toString() != "r" )
    {
      CLog::log("ERROR in CMerkle::generate 3 l: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_merkle_proof[1].midRef(2).toString() != "3leave_4" )
    {
      CLog::log("ERROR in CMerkle::generate 3 m: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_left_hash != "1" )
    {
      CLog::log("ERROR in CMerkle::generate 3 n: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_merkle_proof[0].midRef(2).toString() != "3leave_4" )
    {
      CLog::log("ERROR in CMerkle::generate 3 o: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_merkle_proof[0].midRef(0,1).toString() != "r" )
    {
      CLog::log("ERROR in CMerkle::generate 3 p: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_left_hash != "" )
    {
      CLog::log("ERROR in CMerkle::generate 3 q: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_merkle_proof[0].midRef(0, 1).toString() != "r" )
    {
      CLog::log("ERROR in CMerkle::generate 3 r: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_merkle_proof[0].midRef(2).toString() != "leave_4" )
    {
      CLog::log("ERROR in CMerkle::generate 3 s: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("1", verifies["1"].m_merkle_proof, verifies["1"].m_left_hash, "hashed", "noHash")
       )
    {
      CLog::log("ERROR in CMerkle::generate 3 t: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("2", verifies["2"].m_merkle_proof, verifies["2"].m_left_hash, "hashed", "noHash")
       )
    {
      CLog::log("ERROR in CMerkle::generate 3 u: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("3", verifies["3"].m_merkle_proof, verifies["3"].m_left_hash, "hashed", "noHash")
       )
    {
      CLog::log("ERROR in CMerkle::generate 3 v: " , "app", "fatal");
      exit(1098);
    }
  }

  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate(
    {"98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0",
      "ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f",
      "3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9d"});
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
   if (root != CCrypto::keccak256(
         CCrypto::keccak256("98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f") +
         CCrypto::keccak256("3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9dleave_4"))
       )
    {
      CLog::log("ERROR in CMerkle::generate 3 w: " , "app", "fatal");
      exit(1098);
    }
  }

}

