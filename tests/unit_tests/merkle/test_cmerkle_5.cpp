#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/utils/cmerkle.h"

#include "test_cmerkle_5.h"

TestCMerkle5::TestCMerkle5()
{

}

bool TestCMerkle5::doTests()
{
  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"a", "b", "c", "d", "e"}, "hashed", "noHash");
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    CLog::log("dumpProofs for root " + root);
    CLog::log("dumpProofs for abcde 1: " + CMerkle::dumpProofs(verifies));

    if (root != "abcdeleave_6leave_7leave_8")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 1 root: " + root, 111);

    if (verifies["a"].m_left_hash != "")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 2 root: " + root, 111);

    if (verifies["a"].m_merkle_proof[0].midRef(0, 1) != "r")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 3 root: " + root, 111);

    if (verifies["a"].m_merkle_proof[0].midRef(2) != "b")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 4 root: " + root, 111);

    if (verifies["a"].m_merkle_proof[1].midRef(0, 1) != "r")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 5 root: " + root, 111);

    if (verifies["a"].m_merkle_proof[1].midRef(2) != "cd")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 6 root: " + root, 111);

    if (verifies["b"].m_left_hash != "a")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 7 root: " + root, 111);

    if (verifies["c"].m_left_hash != "")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 8 root: " + root, 111);

    if (verifies["d"].m_left_hash != "c")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 9 root: " + root, 111);

    if (verifies["b"].m_merkle_proof[0].midRef(2) != "cd")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 10 root: " + root, 111);

    if (verifies["e"].m_merkle_proof[0] != "r.leave_6")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 11 root: " + root, 111);

    if (verifies["leave_6"].m_merkle_proof[0] != "r.leave_7leave_8")
      CUtils::exiter("ERROR in CMerkle::generate tree root for abcde 12 root: " + root, 111);

//    if (verifies["_3"].m_merkle_proof[0].midRef(2) != "12")
//    {
//      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 12a" , "app", "fatal");
//      exit(1098);
//    }
//    if (verifies["3"].m_merkle_proof[0].midRef(0, 1) != "r")
//    {
//      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 11b" , "app", "fatal");
//      exit(1098);
//    }
//    if (verifies["3"].m_merkle_proof[0].midRef(2) != "_3")
//    {
//      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 12b" , "app", "fatal");
//      exit(1098);
//    }
//    if (verifies["3"].m_merkle_proof[1].midRef(0, 1) != "l")
//    {
//      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 11c" , "app", "fatal");
//      exit(1098);
//    }
//    if (verifies["3"].m_merkle_proof[1].midRef(2) != "12")
//    {
//      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 12c" , "app", "fatal");
//      exit(1098);
//    }
//    if (root != CMerkle::getRootByAProve("1", verifies["1"].m_merkle_proof, verifies["1"].m_left_hash, "hashed", "noHash"))
//    {
//      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 13" , "app", "fatal");
//      exit(1098);
//    }
//    if (root != CMerkle::getRootByAProve("2", verifies["2"].m_merkle_proof, verifies["2"].m_left_hash, "hashed", "noHash"))
//    {
//      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 14" , "app", "fatal");
//      exit(1098);
//    }
//    if (root != CMerkle::getRootByAProve("3", verifies["3"].m_merkle_proof, verifies["3"].m_left_hash, "hashed", "noHash"))
//    {
//      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 15" , "app", "fatal");
//      exit(1098);
//    }
  }

}
