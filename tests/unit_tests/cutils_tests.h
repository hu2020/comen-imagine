#ifndef CUTILSTESTS_H
#define CUTILSTESTS_H

#include <math.h>
#include <vector>

#include <QList>
#include <QVariant>

#include "constants.h"

class CUtilsTests
{
public:
    CUtilsTests();
    static void doChunksTests();
    static void doChunkTests2();
    static void doTests();
    static bool doTest_isAValidEmailFormat();

    static bool doTest_microPAIToPAI6();
};

#endif // CUTILSTESTS_H
