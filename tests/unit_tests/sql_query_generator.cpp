#include "stable.h"

#include "sql_query_generator.h"

#include "lib/database/db_model.h"
#include "lib/clog.h"

// FIXME: organize all ttests in an standard unittest

SQLQueryGenerator::SQLQueryGenerator()
{

}

bool SQLQueryGenerator::doTests()
{
  if (CConsts::DATABASAE_AGENT == "sqlite")
    return doSqliteTests();

  if (CConsts::DATABASAE_AGENT == "psql")
    return doPSqllTests();
  return false;
}

bool SQLQueryGenerator::doPSqllTests()
{
  PTRRes qRes;

  // test 1
  qRes = DbModel::prepareToSelect(
    "tableX",
    QStringList {"field1"}  // fields
  );
  if (qRes.complete_query != "SELECT field1 FROM tableX")
  {
    CLog::log(QString("ERROR in prepareToSelect: ") + qRes.complete_query, "sql", "fatal");
    exit(183);
  }


  // test 2
  qRes = DbModel::prepareToSelect(
      "tableX",
      QStringList {"field1", "field2"}
  );
  if (qRes.complete_query != "SELECT field1, field2 FROM tableX")
  {
      CLog::log(QString("ERROR in prepareToSelect: ") + qRes.complete_query, "sql", "fatal");
      exit(183);
  }


  // test 3
  qRes = DbModel::prepareToSelect(
      "tableX",
      QStringList {"field1", "field2", "field3"},
      {ModelClause("field1", "value1")}       // caluses
  );
  if (qRes.complete_query != "SELECT field1, field2, field3 FROM tableX WHERE (field1 = ? )")
  {
      CLog::log(QString("ERROR in prepareToSelect34: ") + qRes.complete_query, "sql", "fatal");
      exit(283);
  }
  if (qRes.qElms.m_values.keys().size() != 1)
  {
      CLog::log(QString("ERROR in prepareToSelect qElms.m_values.size()!=1: 1") + qRes.complete_query, "sql", "fatal");
      exit(283);
  }
  if (qRes.qElms.m_values[qRes.qElms.m_values.keys()[0]] != "value1")
  {
      CLog::log(QString("ERROR in prepareToSelect qElms.m_values[0]!=value11: 2") + qRes.complete_query, "sql", "fatal");
      exit(283);
  }


  // test 4
  qRes = DbModel::prepareToSelect(
      "tableX",
      QStringList {"field1", "field2", "field3"},
      {ModelClause("field1", "value1")},
      OrderT {{"field3", "ASC"}, {"field1", "DESC"}}   // order
  );
  if (qRes.complete_query != "SELECT field1, field2, field3 FROM tableX WHERE (field1 = ? ) ORDER BY field3 ASC, field1 DESC")
  {
      CLog::log(QString("ERROR in prepareToSelect5t: 3") + qRes.complete_query, "sql", "fatal");
      exit(383);
  }
  if (qRes.qElms.m_values.keys().size() != 1)
  {
      CLog::log(QString("ERROR in prepareToSelect qElms.m_values.size()!=1: 4") + qRes.complete_query, "sql", "fatal");
      exit(383);
  }
  if (qRes.qElms.m_values[qRes.qElms.m_values.keys()[0]] != "value1")
  {
      CLog::log(QString("ERROR in prepareToSelect qElms.m_values[0]!=value12: 5") + qRes.complete_query, "sql", "fatal");
      exit(383);
  }


  // test 5
  qRes = DbModel::prepareToSelect(
    "tableX",
    QStringList {"field1", "field2", "field3"},
    {ModelClause("field1", "value1")},
    OrderT {{"field3", "ASC"}, {"field1", "DESC"}},   // order
    1       // limit
  );
  if (qRes.complete_query != "SELECT field1, field2, field3 FROM tableX WHERE (field1 = ? ) ORDER BY field3 ASC, field1 DESC LIMIT 1")
  {
    CLog::log(QString("ERROR in prepareToSelect4r: 6") + qRes.complete_query, "sql", "fatal");
    exit(483);
  }
  if (qRes.qElms.m_values.keys().size() != 1)
  {
    CLog::log(QString("ERROR in prepareToSelect qElms.m_values.size()!=1: 7") + qRes.complete_query, "sql", "fatal");
    exit(483);
  }
  if (qRes.qElms.m_values[qRes.qElms.m_values.keys()[0]] != "value1")
  {
    CLog::log(QString("ERROR in prepareToSelect qElms.m_values[0]!=value13: ") + qRes.complete_query, "sql", "fatal");
    exit(483);
  }


  // test select 6
  qRes = DbModel::prepareToSelect(
    "tableX",
    QStringList {"field1", "field2", "field3"},
    {
      ModelClause("field1", "value1"),
      ModelClause("field2", QStringList{"val2.0", "val2.1"}, "IN")
    },
    OrderT {{"field3", "ASC"}, {"field1", "DESC"}},   // order
    1       // limit
  );
  if (qRes.complete_query != "SELECT field1, field2, field3 FROM tableX WHERE (field1 = ? ) AND (field2 IN (?, ?)) ORDER BY field3 ASC, field1 DESC LIMIT 1")
  {
    CLog::log(QString("ERROR in prepareToSelect56: ") + qRes.complete_query, "sql", "fatal");
    exit(783);
  }
  if (qRes.qElms.m_values.keys().size() != 3)
  {
    CLog::log(QString("ERROR in prepareToSelect qElms.m_values.size()!=1: 8") + qRes.complete_query, "sql", "fatal");
    exit(783);
  }
  QStringList keys = qRes.qElms.m_values.keys();
  keys.sort();
  if (qRes.qElms.m_values[keys[0]] != "value1")
  {
    CLog::log(QString("ERROR in prepareToSelect qElms.m_values[0]!=value14: 9") + qRes.complete_query, "sql", "fatal");
    exit(783);
  }

  // test select 6.1
  qRes = DbModel::prepareToSelect(
    "c_kvalue",
    QStringList {"kv_key", "kv_value", "kv_last_modified"},
    {
      ModelClause("kv_last_modified", "2021-08-15 18:37:31", "<="),
      ModelClause("kv_key", QStringList{"DAG_LEAVE_BLOCKS", "k4"}, "IN")
    },
    OrderT {{"field3", "ASC"}, {"field1", "DESC"}},   // order
    1       // limit
  );
  if (qRes.complete_query != "SELECT kv_key, kv_value, kv_last_modified FROM c_kvalue WHERE (kv_last_modified <= ? ) AND (kv_key IN (?, ?)) ORDER BY field3 ASC, field1 DESC LIMIT 1")
  {
    CLog::log(QString("ERROR in prepareToSelect 6.1: ") + qRes.complete_query, "sql", "fatal");
    exit(783);
  }


  // test update
  GenRes res;
  auto[updCompleteQuery, updFields] = DbModel::prepareToUpdate(
    "tableX",
    QVDicT {
      {QString("field1"), QString("value1")}
    },          //upd_values
    ClausesT {
      ModelClause("caluseField1", "caluseValue1")
    }
  );

  if (updCompleteQuery != "UPDATE tableX SET field1= ? WHERE (caluseField1 = ? )")
  {
    CLog::log(QString("ERROR in prepareToUpdate4d: ") + updCompleteQuery, "sql", "fatal");
    exit(783);
  }
  updFields.sort();
  if (updFields[0] != "field1")
  {
    CLog::log(QString("ERROR in prepareToUpdate45t: ") + updCompleteQuery, "sql", "fatal");
    exit(783);
  }

  return true;
}

bool SQLQueryGenerator::doSqliteTests()
{
  PTRRes qRes;

  // test 1
  qRes = DbModel::prepareToSelect(
      "tableX",
      QStringList {"field1"}  // fields
  );
  if (qRes.complete_query != "SELECT field1 FROM tableX")
  {
      CLog::log(QString("ERROR in prepareToSelect: ") + qRes.complete_query, "sql", "fatal");
      exit(183);
  }


  // test 2
  qRes = DbModel::prepareToSelect(
      "tableX",
      QStringList {"field1", "field2"}
  );
  if (qRes.complete_query != "SELECT field1, field2 FROM tableX")
  {
      CLog::log(QString("ERROR in prepareToSelect: ") + qRes.complete_query, "sql", "fatal");
      exit(183);
  }


  // test 3
  qRes = DbModel::prepareToSelect(
      "tableX",
      QStringList {"field1", "field2", "field3"},
      {ModelClause("field1", "value1")}       // caluses
  );
  if (qRes.complete_query != "SELECT field1, field2, field3 FROM tableX WHERE (field1 = :field1)")
  {
      CLog::log(QString("ERROR in prepareToSelect34: ") + qRes.complete_query, "sql", "fatal");
      exit(283);
  }
  if (qRes.qElms.m_values.keys().size() != 1)
  {
      CLog::log(QString("ERROR in prepareToSelect qElms.m_values.size()!=1: 1") + qRes.complete_query, "sql", "fatal");
      exit(283);
  }
  if (qRes.qElms.m_values[qRes.qElms.m_values.keys()[0]] != "value1")
  {
      CLog::log(QString("ERROR in prepareToSelect qElms.m_values[0]!=value11: 2") + qRes.complete_query, "sql", "fatal");
      exit(283);
  }


  // test 4
  qRes = DbModel::prepareToSelect(
      "tableX",
      QStringList {"field1", "field2", "field3"},
      {ModelClause("field1", "value1")},
      OrderT {{"field3", "ASC"}, {"field1", "DESC"}}   // order
  );
  if (qRes.complete_query != "SELECT field1, field2, field3 FROM tableX WHERE (field1 = :field1) ORDER BY field3 ASC, field1 DESC")
  {
      CLog::log(QString("ERROR in prepareToSelect5t: 3") + qRes.complete_query, "sql", "fatal");
      exit(383);
  }
  if (qRes.qElms.m_values.keys().size() != 1)
  {
      CLog::log(QString("ERROR in prepareToSelect qElms.m_values.size()!=1: 4") + qRes.complete_query, "sql", "fatal");
      exit(383);
  }
  if (qRes.qElms.m_values[qRes.qElms.m_values.keys()[0]] != "value1")
  {
      CLog::log(QString("ERROR in prepareToSelect qElms.m_values[0]!=value12: 5") + qRes.complete_query, "sql", "fatal");
      exit(383);
  }


  // test 5
  qRes = DbModel::prepareToSelect(
    "tableX",
    QStringList {"field1", "field2", "field3"},
    {ModelClause("field1", "value1")},
    OrderT {{"field3", "ASC"}, {"field1", "DESC"}},   // order
    1       // limit
  );
  if (qRes.complete_query != "SELECT field1, field2, field3 FROM tableX WHERE (field1 = :field1) ORDER BY field3 ASC, field1 DESC LIMIT 1")
  {
    CLog::log(QString("ERROR in prepareToSelect4r: 6") + qRes.complete_query, "sql", "fatal");
    exit(483);
  }
  if (qRes.qElms.m_values.keys().size() != 1)
  {
    CLog::log(QString("ERROR in prepareToSelect qElms.m_values.size()!=1: 7") + qRes.complete_query, "sql", "fatal");
    exit(483);
  }
  if (qRes.qElms.m_values[qRes.qElms.m_values.keys()[0]] != "value1")
  {
    CLog::log(QString("ERROR in prepareToSelect qElms.m_values[0]!=value13: ") + qRes.complete_query, "sql", "fatal");
    exit(483);
  }


  // test select 6
  qRes = DbModel::prepareToSelect(
    "tableX",
    QStringList {"field1", "field2", "field3"},
    {
      ModelClause("field1", "value1"),
      ModelClause("field2", QStringList{"val2.0", "val2.1"}, "IN")
    },
    OrderT {{"field3", "ASC"}, {"field1", "DESC"}},   // order
    1       // limit
  );
  if (qRes.complete_query != "SELECT field1, field2, field3 FROM tableX WHERE (field1 = :field1) AND (field2 IN (:field20, :field21)) ORDER BY field3 ASC, field1 DESC LIMIT 1")
  {
    CLog::log(QString("ERROR in prepareToSelect56: ") + qRes.complete_query, "sql", "fatal");
    exit(783);
  }
  if (qRes.qElms.m_values.keys().size() != 3)
  {
    CLog::log(QString("ERROR in prepareToSelect qElms.m_values.size()!=1: 8") + qRes.complete_query, "sql", "fatal");
    exit(783);
  }
  QStringList keys = qRes.qElms.m_values.keys();
  keys.sort();
  if (qRes.qElms.m_values[keys[0]] != "value1")
  {
    CLog::log(QString("ERROR in prepareToSelect qElms.m_values[0]!=value14: 9") + qRes.complete_query, "sql", "fatal");
    exit(783);
  }

  // test select 6.1
  qRes = DbModel::prepareToSelect(
    "c_kvalue",
    QStringList {"kv_key", "kv_value", "kv_last_modified"},
    {
      ModelClause("kv_last_modified", "2021-08-15 18:37:31", "<="),
      ModelClause("kv_key", QStringList{"DAG_LEAVE_BLOCKS", "k4"}, "IN")
    },
    OrderT {{"field3", "ASC"}, {"field1", "DESC"}},   // order
    1       // limit
  );
  if (qRes.complete_query != "SELECT kv_key, kv_value, kv_last_modified FROM c_kvalue WHERE (kv_last_modified <= :kv_last_modified) AND (kv_key IN (:kv_key0, :kv_key1)) ORDER BY field3 ASC, field1 DESC LIMIT 1")
  {
    CLog::log(QString("ERROR in prepareToSelect 6.1: ") + qRes.complete_query, "sql", "fatal");
    exit(783);
  }



//  QueryRes tmpres = DbModel::select(
//    "c_kvalue",
//    {"kv_key", "kv_value", "kv_last_modified"},
//    {
//      ModelClause("kv_last_modified", "2021-08-15 18:37:31", "<="),
//      ModelClause("kv_key", QStringList{"DAG_LEAVE_BLOCKS", "k4"}, "IN")
//    }
//  );




  // test update
  GenRes res;
  auto[updCompleteQuery, updFields] = DbModel::prepareToUpdate(
    "tableX",
    QVDicT {
      {QString("field1"), QString("value1")}
    },          //upd_values
    ClausesT {
      ModelClause("caluseField1", "caluseValue1")
    }
  );

  if (updCompleteQuery != "UPDATE tableX SET field1=:field1 WHERE (caluseField1 = :caluseField1)")
  {
    CLog::log(QString("ERROR in prepareToUpdate4d: ") + updCompleteQuery, "sql", "fatal");
    exit(783);
  }
  updFields.sort();
  if (updFields[0] != "field1")
  {
    CLog::log(QString("ERROR in prepareToUpdate45t: ") + updCompleteQuery, "sql", "fatal");
    exit(783);
  }

  return true;
}
