#ifndef SQL_QUERY_GENERATOR_H
#define SQL_QUERY_GENERATOR_H


class SQLQueryGenerator
{

public:
    SQLQueryGenerator();
    static bool doTests();
    static bool doSqliteTests();
    static bool doPSqllTests();
};

#endif // SQL_QUERY_GENERATOR_H
