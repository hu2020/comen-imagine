#include "stable.h"

#include "cutils_tests.h"


typedef double longDouble;

CUtilsTests::CUtilsTests() {
}

void CUtilsTests::doChunksTests()
{
  QStringList values;
  QVector<QStringList> chunks;





  values = QStringList {"a"};
  chunks = CUtils::chunkStringList(values, 1);
  if ((chunks.size()!=1) || (chunks[0].size()!=1) || (chunks[0][0]!="a"))
  {
    CLog::log("ERROR in chunkStringList1.1: " , "app", "fatal");
    exit(1098);
  }

  values = QStringList {"a", "b", "c", "d"}; //, "e", "f", "g", "h", "i", "j", "k", "l"};
  chunks = CUtils::chunkStringList(values, 1);
  if ((chunks.size()!=4) || (chunks[0].size()!=1) || (chunks[1].size()!=1) || (chunks[2].size()!=1) || (chunks[3].size()!=1) ||
     (chunks[0][0]!="a") || (chunks[1][0]!="b") || (chunks[2][0]!="c") || (chunks[3][0]!="d"))
  {
    CLog::log("ERROR in chunkStringList4.1: " , "app", "fatal");
    exit(1098);
  }

  values = QStringList {"a", "b", "c", "d"}; //, "e", "f", "g", "h", "i", "j", "k", "l"};
  chunks = CUtils::chunkStringList(values, 2);
  if ((chunks.size()!=2) || (chunks[0].size()!=2) || (chunks[1].size()!=2) ||
     (chunks[0][0]!="a") || (chunks[0][1]!="b") || (chunks[1][0]!="c") || (chunks[1][1]!="d"))
  {
    CLog::log("ERROR in chunkStringList4: " , "app", "fatal");
    exit(1098);
  }

  values = QStringList {"a", "b", "c"}; //, "d", "e", "f", "g", "h", "i", "j", "k", "l"};
  chunks = CUtils::chunkStringList(values, 2);
  if ((chunks.size()!=2) || (chunks[0].size()!=2) || (chunks[1].size()!=1) ||
     (chunks[0][0]!="a") || (chunks[0][1]!="b") || (chunks[1][0]!="c"))
  {
    CLog::log("ERROR in chunkStringList3: " , "app", "fatal");
    exit(1098);
  }

  values = QStringList {"a", "b"}; //, "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"};
  chunks = CUtils::chunkStringList(values, 2);
  if ((chunks.size()!=1) || (chunks[0].size()!=2) || (chunks[0][0]!="a") || (chunks[0][1]!="b"))
  {
    CLog::log("ERROR in chunkStringList2: " , "app", "fatal");
    exit(1098);
  }


  values = QStringList {"a"};
  chunks = CUtils::chunkStringList(values, 2);
  if ((chunks.size()!=1) || (chunks[0].size()!=1) || (chunks[0][0]!="a"))
  {
    CLog::log("ERROR in chunkStringList1: " , "app", "fatal");
    exit(1098);
  }

  values = QStringList {};
  chunks = CUtils::chunkStringList(values, 2);
  if (chunks.size()!=0)
  {
    CLog::log("ERROR in chunkStringList0: " , "app", "fatal");
    exit(1098);
  }


}

bool CUtilsTests::doTest_microPAIToPAI6()
{
  if(CUtils::microPAIToPAI6(1) != "0.000001")
    CUtils::exiter("micro PAI To PAI6 failed! 1" + CUtils::microPAIToPAI6(1), 45);

  if(CUtils::microPAIToPAI6(-123456) != "-0.123456")
    CUtils::exiter("micro PAI To PAI6 failed! -1", 45);

  if(CUtils::microPAIToPAI6(123456) != "0.123456")
    CUtils::exiter("micro PAI To PAI6 failed!", 45);

  if(CUtils::microPAIToPAI6(90123456) != "90.123456")
    CUtils::exiter("micro PAI To PAI6 failed!", 45);

  if(CUtils::microPAIToPAI6(890123456) != "890.123456")
    CUtils::exiter("micro PAI To PAI6 failed!", 45);

  if(CUtils::microPAIToPAI6(234567890123456) != "234,567,890.123456")
    CUtils::exiter("micro PAI To PAI6 failed!", 45);

  if(CUtils::microPAIToPAI6(1234567890123456) != "1,234,567,890.123456")
    CUtils::exiter("micro PAI To PAI6 failed!", 45);

  return true;
}

bool CUtilsTests::doTest_isAValidEmailFormat()
{
  // TODO: add more tests
  if (!CUtils::isAValidEmailFormat("abc@def.gh"))
    CUtils::exiter("Failed on is A Valid Email Format 1", 11);

  if (CUtils::isAValidEmailFormat("abc@defgh"))
    CUtils::exiter("Failed on is A Valid Email Format 2", 11);

  return true;
}

void CUtilsTests::doChunkTests2()
{
  {
    QStringList chunks = CUtils::chunkString("a", 1);
    if (chunks.size() != 1) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "a") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }

  {
    QStringList chunks = CUtils::chunkString("ab", 1);
    if (chunks.size() != 2) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "a") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[1] != "b") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }

  {
    QStringList chunks = CUtils::chunkString("abc", 1);
    if (chunks.size() != 3) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "a") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[1] != "b") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[2] != "c") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }

  {
    QStringList chunks = CUtils::chunkString("abcd", 1);
    if (chunks.size() != 4) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "a") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[1] != "b") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[2] != "c") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[3] != "d") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }

  {
    QStringList chunks = CUtils::chunkString("abcdef", 1);
    if (chunks.size() != 6) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "a") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[5] != "f") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }

  {
    QStringList chunks = CUtils::chunkString("a", 2);
    if (chunks.size() != 1) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "a") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }
  {
    QStringList chunks = CUtils::chunkString("ab", 2);
    if (chunks.size() != 1) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "ab") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }
  {
    QStringList chunks = CUtils::chunkString("abc", 2);
    if (chunks.size() != 2) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "ab") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[1] != "c") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }
  {
    QStringList chunks = CUtils::chunkString("abcd", 2);
    if (chunks.size() != 2) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "ab") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[1] != "cd") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }
  {
    QStringList chunks = CUtils::chunkString("abcde", 2);
    if (chunks.size() != 3) {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[0] != "ab") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[1] != "cd") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
    if (chunks[2] != "e") {
      CLog::log("ERROR in chunkString 1: " , "app", "fatal");
      exit(391);
    }
  }

  return;
}

void CUtilsTests::doTests()
{

  doTest_microPAIToPAI6();

  doChunkTests2();
  doChunksTests();
  doTest_isAValidEmailFormat();


  // test 1
  if (CUtils::hash8c("123456789") != "12345678") {
    CLog::log("ERROR in hash8c: " , "app", "fatal");
    exit(1098);
  }

  // test 1
  if (CUtils::removeDblSpaces("1   2 345  6   78                      9 1   ") != "1 2 345 6 78 9 1 ") {
    CLog::log("ERROR in removeDblSpaces: " , "app", "fatal");
    exit(1098);
  }

  // test 1
  if (CUtils::isGreaterThanNow("")) {
      CLog::log(QString("ERROR in isGreaterThanNow: null") , "app", "fatal");
  }

  // test 2
  if (CUtils::isGreaterThanNow("2000-01-01 00:00:00")) {
      CLog::log(QString("ERROR in isGreaterThanNow: 2000-01-01 00:00:00") , "app", "fatal");
  }



  // test 2
  if (CUtils::minutesBefore(1, "2000-01-01 12:00:00") != "2000-01-01 11:59:00")
  {
      CLog::log(QString("ERROR in minutesBefore: 2000-01-01 11:59:00") , "app", "fatal");
      exit(1098);
  }
  if (CUtils::minutesBefore(1, "2000-02-02 00:00:00") != "2000-02-01 23:59:00")
  {
      CLog::log(QString("ERROR in minutesBefore: 2000-01-01 11:59:00") , "app", "fatal");
      exit(1098);
  }
  if (CUtils::minutesBefore(1, "2000-02-01 00:00:00") != "2000-01-31 23:59:00")
  {
      CLog::log(QString("ERROR in minutesBefore: 2000-01-01 11:59:00") , "app", "fatal");
      exit(1098);
  }
  if (CUtils::minutesBefore(1, "2000-01-01 00:00:00") != "1999-12-31 23:59:00")
  {
      CLog::log(QString("ERROR in minutesBefore: 2000-01-01 11:59:00") , "app", "fatal");
      exit(1098);
  }
  if (CUtils::minutesBefore(12 * 60, "2000-01-01 12:00:01") != "2000-01-01 00:00:01")
  {
      CLog::log(QString("ERROR in minutesBefore: 2000-01-01 11:59:00") , "app", "fatal");
      exit(1098);
  }
  if (CUtils::minutesBefore(12 * 60, "2000-02-02 00:00:00") != "2000-02-01 12:00:00")
  {
      CLog::log(QString("ERROR in minutesBefore: 2000-01-01 11:59:00") , "app", "fatal");
      exit(1098);
  }
  if (CUtils::minutesBefore(12 * 60, "2000-02-01 11:00:00") != "2000-01-31 23:00:00")
  {
      CLog::log(QString("ERROR in minutesBefore: 2000-01-01 11:59:00") , "app", "fatal");
      exit(1098);
  }
  if (CUtils::minutesBefore(12 * 60, "2000-01-01 11:00:00") != "1999-12-31 23:00:00")
  {
      CLog::log(QString("ERROR in minutesBefore: 2000-01-01 11:59:00") , "app", "fatal");
      exit(1098);
  }
  if (CUtils::minutesBefore(5 * 720, "2020-02-02 00:00:00") != "2020-01-30 12:00:00")
  {
      CLog::log(QString("ERROR in minutesBefore: 2020-01-30 12:00:00") , "app", "fatal");
      exit(1098);
  }

  if (CUtils::yearsBefore(1, "2020-02-02 00:00:00") != "2019-02-02 00:00:00")
  {
      CLog::log(QString("ERROR in yearsBefore: 2020-01-30 12:00:00") , "app", "fatal");
      exit(1198);
  }












  if (CUtils::iFloorFloat(1) != 1)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(134217728) != 134217728)
  {
      CLog::log(QString("ERROR in iFloorFloat: 134217728") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(9896665478913551) != 9896665478913551)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(134217728.9896665478913551) != 134217728.9896665478)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(0.989666547891355) != CUtils::iFloorFloat(0.9896665478913551))
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::customFloorFloat(0.9896665478913551, 10) != 0.9896665478)
  {
      CLog::log(QString("ERROR in customFloorFloat: 0.9896665478913551") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(1000000.002) != 1000000.002)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::customFloorFloat(1000000.002, 3) != 1000000.002)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::customFloorFloat(1000000.002, 4) != 1000000.002)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(1000000.002) != 1000000.0020)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(1000000.002) != 1000000.00200)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(1.0000000000001) != 1)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(1.000000000001) != 1)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(1.00000000001) != 1.00000000001)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(1.00000000001234) != 1.00000000001)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(0.00000000001) != 0)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::iFloorFloat(0.0000000001) != 0.0000000001)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::customFloorFloat(10.001, 0) != 10)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::customFloorFloat(0.001, 0) != 0)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }
  if (CUtils::customFloorFloat(0.00001, 2) != 0)
  {
      CLog::log(QString("ERROR in iFloorFloat: 1") , "app", "fatal");
      exit(398);
  }











  {
    auto[x, y, gain, revGain] = CUtils::calcLog(0, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 100)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(50, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 84.9485002168)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(95, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 34.9485002168)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(96, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 30.10299956639)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(97, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 23.85606273598)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(98, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 15.05149978319)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(99, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 0)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(100, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 0)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }

  uint64_t electionTime = 2 * 24 * 60; // for example for election in 2 day
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(0, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 100)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime / 4, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 96.3884197283)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime / 2, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 91.2981832293)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 3 / 4, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 82.59636645861)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 40 / 48, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 77.5061299596)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 44 / 48, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 68.80431318891)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 46 / 48, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 60.10249641821)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 47 / 48, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 51.40067964752)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 47.5 / 48, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 42.69886287682)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 47.75 / 48, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 33.99704610613)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 47.84 / 48, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 28.39432751603)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(electionTime * 47.917/ 48, electionTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 20.15467586366)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(100, 100, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 0)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }

  electionTime = 2 * 24 * 60; // for example for election in 2 day
  uint64_t negativeVoteTime = electionTime * 1.5;

  {
    auto[x, y, gain, revGain] = CUtils::calcLog(0, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 100)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime / 4, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 96.56335317912)
    {
      CLog::log(QString("ERROR in calcLog: 1/4 ") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime / 2, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 91.71967153125)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 54 / 72, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 83.43934306251)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 60 / 72, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 78.59566141464)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 64 / 72, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 73.75197976678)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 70 / 72, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
    if (gain != 57.19132282929)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 71 / 72, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 48.91099436055)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 71.50 / 72, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 40.63066589181)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    // at the latest 15 minutes
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 71.75 / 72, negativeVoteTime, 17);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(revGain);
        if (gain != 32.35033742307)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    // at the latest 10 minutes
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 71.834/ 72, negativeVoteTime, 17);
    Q_UNUSED(x);
    Q_UNUSED(y);
    Q_UNUSED(revGain);
    if (gain != 27.4587759964)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(negativeVoteTime * 71.917/ 72, negativeVoteTime, 17);
    Q_UNUSED(x);
    Q_UNUSED(y);
    Q_UNUSED(revGain);
    if (gain != 19.17844752766)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }
  {
    auto[x, y, gain, revGain] = CUtils::calcLog(100, 100, 17);
    Q_UNUSED(x);
    Q_UNUSED(y);
    Q_UNUSED(revGain);
    if (gain != 0)
    {
      CLog::log(QString("ERROR in calcLog: 100") , "app", "fatal");
      exit(398);
    }
  }

}
