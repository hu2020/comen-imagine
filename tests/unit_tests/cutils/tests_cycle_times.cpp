#include "stable.h"
#include "tests_cycle_times.h"
#include "lib/utils/version_handler.h"

TestsCycleTimes::TestsCycleTimes()
{

}

bool TestsCycleTimes::doTests()
{

//  double me = 1000000;
//  double he = 6;
//  CMPAIValueT cycle_coins = 45036577016206;

//  CMPAIValueT dividend_me = CUtils::CFloor((CUtils::iFloorFloat(me/(me+he)) * cycle_coins));
//  CMPAIValueT dividend_he = CUtils::CFloor((CUtils::iFloorFloat(he/(me+he)) * cycle_coins));

//  double he_div = CUtils::iFloorFloat(6.0/(me+he));

  if (CUtils::convertFloatToString(99.999999999989996) != "99.99999999999")
    CUtils::exiter("convert Float To Striong Failed 1", 78);

  if (CUtils::convertFloatToString(0.0035996328299999999) != "0.00359963283")
    CUtils::exiter("convert Float To Striong Failed 2", 78);

  if (CUtils::convertFloatToString(0.0) != "0")
    CUtils::exiter("convert Float To Striong Failed 3", 78);

  if (CUtils::convertFloatToString(0.00) != "0")
    CUtils::exiter("convert Float To Striong Failed 4", 78);

  if (VersionHandler::isOlderThan("0.0.8", "0.0.8") == 1)
    CUtils::exiter("is Older Than Failed 1", 78);

  testVector1();

  bool status;
  status = test1();
  if (!status)
    return status;

  status = test2();
  if (!status)
    return status;

  status = test3();
  if (!status)
    return status;

  status = test4();
  if (!status)
    return status;

  return true;
}

bool TestsCycleTimes::testVector1()
{
  std::vector<QString> vec {"a", "b", "c"};

  if(!CUtils::contains_(vec, "a"))
    CUtils::exiter("contains_ failed1", 0);
  if(CUtils::contains_(vec, "d"))
    CUtils::exiter("contains_ failed2", 0);

  return true;
}

bool TestsCycleTimes::test1()
{
 //Should control coinbase date range is valid (12 hour per cycle)
  if (CConsts::TIME_GAIN == 1)
  {

    if (CUtils::getCoinbaseCycleNumber("2012-11-05 00:00:01") != "00:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed1", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 01:08:00") != "00:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed2", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 07:08:00") != "00:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed3", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 11:59:59") != "00:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed4", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 12:00:00") != "12:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed5", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 12:00:01") != "12:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed6", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 13:08:00") != "12:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed7", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 23:59:59") != "12:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed8", 0);

    if (CUtils::getCycleElapsedByMinutes("2012-11-05 00:00:00") != 0)
      CUtils::exiter("get Cycle Elapsed By Minutes failed1", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 00:01:01") != 1)
      CUtils::exiter("get Cycle Elapsed By Minutes failed2", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 07:00:01") != 420)
      CUtils::exiter("get Cycle Elapsed By Minutes failed3", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 12:00:01") != 0)
      CUtils::exiter("get Cycle Elapsed By Minutes failed4", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 13:00:01") != 60)
      CUtils::exiter("get Cycle Elapsed By Minutes failed5", 0);

    if (CUtils::getCycleElapsedByMinutes("2012-11-05 11:59:59") != 719)
      CUtils::exiter("get Cycle Elapsed By Minutes failed6", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 23:59:59") != 719)
      CUtils::exiter("get Cycle Elapsed By Minutes failed7", 0);

    {
      auto the_range = CUtils::getCoinbaseRange("2012-11-05 00:00:01");
      if (the_range.from != "2012-11-05 00:00:00")
        CUtils::exiter("get Coinbase Range failed1", 0);
    }

    {
      auto the_range = CUtils::getCoinbaseRange("2012-11-05 00:00:01");
      if (the_range.to != "2012-11-05 11:59:59")
        CUtils::exiter("get Coinbase Range failed2", 0);
    }

    {
      auto the_range = CUtils::getCoinbaseRange("2012-11-05 11:59:59");
      if (the_range.from != "2012-11-05 00:00:00")
        CUtils::exiter("get Coinbase Range failed3", 0);
    }

    {
      auto the_range = CUtils::getCoinbaseRange("2012-11-05 11:59:59");
      if (the_range.to != "2012-11-05 11:59:59")
        CUtils::exiter("get Coinbase Range failed4", 0);
    }

    {
      auto the_range = CUtils::getCoinbaseRange("2012-11-05 12:00:00");
      if (the_range.from != "2012-11-05 12:00:00")
        CUtils::exiter("get Coinbase Range failed5", 0);
    }

    {
      auto the_range = CUtils::getCoinbaseRange("2012-11-05 12:00:00");
      if (the_range.to != "2012-11-05 23:59:59")
        CUtils::exiter("get Coinbase Range failed6", 0);
    }

    {
      auto the_range = CUtils::getCoinbaseRange("2012-11-05 23:59:59");
      if (the_range.from != "2012-11-05 12:00:00")
        CUtils::exiter("get Coinbase Range failed7", 0);
    }

    {
      auto the_range = CUtils::getCoinbaseRange("2012-11-05 23:59:59");
      if (the_range.to != "2012-11-05 23:59:59")
        CUtils::exiter("get Coinbase Range failed8", 0);
    }



    if (CUtils::getCoinbaseCycleStamp("2012-11-05 00:00:00") != "2012-11-05 00:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed1 " + CUtils::getCoinbaseCycleStamp("2012-11-05 00:00:00"), 0);
    if (CUtils::getCoinbaseCycleStamp("2012-11-05 00:00:01") != "2012-11-05 00:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed2", 0);
    if (CUtils::getCoinbaseCycleStamp("2012-11-05 11:59:59") != "2012-11-05 00:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed3", 0);
    if (CUtils::getCoinbaseCycleStamp("2012-11-05 12:00:00") != "2012-11-05 12:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed4", 0);
    if (CUtils::getCoinbaseCycleStamp("2012-11-05 12:00:01") != "2012-11-05 12:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed5", 0);
    if (CUtils::getCoinbaseCycleStamp("2012-11-05 23:59:59") != "2012-11-05 12:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed6", 0);

  }
  return true;
}


bool TestsCycleTimes::test2()
{

  // coinbase_plan
  // Should encode then decod and return same text
  if (CUtils::getCycleByMinutes() == 5)
  {
    // if (CoinbaseUTXOHandler.calcCoinbasedOutputMaturationDate("2019-05-10 00:00:00") != "2019-05-10 00:15:00");
    // if (CoinbaseUTXOHandler.calcCoinbasedOutputMaturationDate("2019-05-10 00:02:00") != "2019-05-10 00:15:00");
    // if (CoinbaseUTXOHandler.calcCoinbasedOutputMaturationDate("2019-05-10 00:04:59") != "2019-05-10 00:15:00");
  }


  if (CUtils::getCycleByMinutes() == 720)
  {
    if (CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 00:00:00") != "2019-05-11 00:00:00")
      CUtils::exiter("calc Coinbased Output Maturation Date failed1 " + CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 00:00:00"), 0);
    if (CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 11:59:59") != "2019-05-11 00:00:00")
      CUtils::exiter("calc Coinbased Output Maturation Date failed2 " + CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 11:59:59"), 0);

    if (CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 12:00:00") != "2019-05-11 12:00:00")
      CUtils::exiter("calc Coinbased Output Maturation Date failed3", 0);
    if (CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 23:59:59") != "2019-05-11 12:00:00")
      CUtils::exiter("calc Coinbased Output Maturation Date failed4", 0);
  }
  return true;
}

bool TestsCycleTimes::test3()
{
  if (CConsts::TIME_GAIN == 1)
  {
    {
     auto the_range = CUtils::getCbUTXOsDateRange("2017-07-22 00:00:00");
      if (the_range.from != "2017-07-21 00:00:00")
        CUtils::exiter("get Cb UTXOs Date Range failed1 " + the_range.from, 0);
      if (the_range.to != "2017-07-21 11:59:59")
        CUtils::exiter("get Cb UTXOs Date Range failed2 " + the_range.to, 0);
    }
    {
     auto the_range = CUtils::getCbUTXOsDateRange("2017-07-22 11:59:59");
      if (the_range.from != "2017-07-21 00:00:00")
        CUtils::exiter("get Cb UTXOs Date Range failed3", 0);
      if (the_range.to != "2017-07-21 11:59:59")
        CUtils::exiter("get Cb UTXOs Date Range failed4", 0);
    }
    {
     auto the_range = CUtils::getCbUTXOsDateRange("2017-07-22 12:00:00");
      if (the_range.from != "2017-07-21 12:00:00")
        CUtils::exiter("get Cb UTXOs Date Range failed5", 0);
      if (the_range.to != "2017-07-21 23:59:59")
          CUtils::exiter("get Cb UTXOs Date Range failed6", 0);
    }
    {
     auto the_range = CUtils::getCbUTXOsDateRange("2017-07-22 23:59:00");
      if (the_range.from != "2017-07-21 12:00:00")
        CUtils::exiter("get Cb UTXOs Date Range failed7", 0);
      if (the_range.to != "2017-07-21 23:59:59")
          CUtils::exiter("get Cb UTXOs Date Range failed8", 0);
    }

  }
  return true;
}

bool TestsCycleTimes::test4()
{

  // getCoinbaseInfo
  // Should getCoinbaseInfo
  {
    auto[cycleStamp, from_, to_, fromHour, toHour] = CUtils::getCoinbaseInfo("", "2016-01-01 00:00:00");
    if (cycleStamp != "2016-01-01 00:00:00")
      CUtils::exiter("get Coinbase Info failed1", 0);
    if (from_ != "2016-01-01 00:00:00")
      CUtils::exiter("get Coinbase Info failed2", 0);
    if (fromHour != "00:00:00")
      CUtils::exiter("get Coinbase Info failed3", 0);
    if (to_ != "2016-01-01 11:59:59")
      CUtils::exiter("get Coinbase Info failed4", 0);
    if (toHour != "11:59:59")
      CUtils::exiter("get Coinbase Info failed5", 0);
  }

  {
    auto[cycleStamp, from_, to_, fromHour, toHour] = CUtils::getCoinbaseInfo("", "2016-01-01 12:00:00");
    if (cycleStamp != "2016-01-01 12:00:00")
      CUtils::exiter("get Coinbase Info failed11", 0);
    if (from_ != "2016-01-01 12:00:00")
      CUtils::exiter("get Coinbase Info failed12", 0);
    if (fromHour != "12:00:00")
      CUtils::exiter("get Coinbase Info failed13", 0);
    if (to_ != "2016-01-01 23:59:59")
      CUtils::exiter("get Coinbase Info failed14", 0);
    if (toHour != "23:59:59")
      CUtils::exiter("get Coinbase Info failed15", 0);
  }

  return true;
}
