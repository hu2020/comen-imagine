#ifndef TESTSSTRINGMANIPULATIONS_H
#define TESTSSTRINGMANIPULATIONS_H


class TestsStringmanipulations
{
public:
  TestsStringmanipulations();
  static bool doTests();
  static bool test1();
  static bool testSepNum();
  static bool testStripNonAlphaNumeric();
};

#endif // TESTSSTRINGMANIPULATIONS_H
