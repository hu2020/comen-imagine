#include "stable.h"
#include "tests_string_manipulations.h"

TestsStringmanipulations::TestsStringmanipulations()
{

}

bool TestsStringmanipulations::doTests()
{
  testStripNonAlphaNumeric();
  test1();
  testSepNum();

  return true;
}

bool TestsStringmanipulations::test1()
{
  // strip no hex chars
  if (CUtils::stripNonHex("12dj8in 0e8") != "12d80e8")
    CUtils::exiter("strip Non Hex failed 1", 0);

  if (CUtils::stripNonHex("Z12dj8-/in 0'kkk\"=+e8") != "12d80e8")
    CUtils::exiter("strip Non Hex failed 2", 0);

  if (CUtils::isValidHash("z3e4073b44c2ce6218f816d25c2e4ab28c8a003d1e6182ba9c15176dad236321"))
    CUtils::exiter("is Valid Hash failed 3", 0);

  if (!CUtils::isValidHash("53e4073b44c2ce6218f816d25c2e4ab28c8a003d1e6182ba9c15176dad236321"))
    CUtils::exiter("is Valid Hash failed 4", 0);

  if (CUtils::isValidHash("12d80e8"))
    CUtils::exiter("is Valid Hash failed 5", 0);

  if (CUtils::isValidHash("Z1\2dj8-/in 0'kkk\"=+e8"))
    CUtils::exiter("is Valid Hash failed 6", 0);

  return true;
}

bool TestsStringmanipulations::testStripNonAlphaNumeric()
{

  if (CUtils::stripNonAlphaNumeric("1111") != "1111")
    CUtils::exiter("strip Non Alpha Numeric failed 1", 0);

  if (CUtils::stripNonAlphaNumeric("1234") != "1234")
    CUtils::exiter("strip Non Alpha Numeric failed 1", 0);

  if (CUtils::stripNonAlphaNumeric("aaaa") != "aaaa")
    CUtils::exiter("strip Non Alpha Numeric failed 1", 0);

  if (CUtils::stripNonAlphaNumeric("AAAA") != "AAAA")
    CUtils::exiter("strip Non Alpha Numeric failed 1", 0);

  if (CUtils::stripNonAlphaNumeric("aa11") != "aa11")
    CUtils::exiter("strip Non Alpha Numeric failed 1", 0);


}

bool TestsStringmanipulations::testSepNum()
{
  if (CUtils::sepNum(1) != "1")
    CUtils::exiter("sep Num failed 1 =" + CUtils::sepNum(1), 10);

  if (CUtils::sepNum(21) != "21")
    CUtils::exiter("sep Num failed 21", 10);

  if (CUtils::sepNum(321) != "321")
    CUtils::exiter("sep Num failed 321", 10);

  if (CUtils::sepNum(4321) != "4,321")
    CUtils::exiter("sep Num failed 4321 =" + CUtils::sepNum(4321), 10);

  if (CUtils::sepNum(7654321) != "7,654,321")
    CUtils::exiter("sep Num failed 7654321", 10);

  if (CUtils::sepNum(-1) != "-1")
    CUtils::exiter("sep Num failed -1="+CUtils::sepNum(-1), 10);

  if (CUtils::sepNum(-21) != "-21")
    CUtils::exiter("sep Num failed -21", 10);

  if (CUtils::sepNum(-321) != "-321")
    CUtils::exiter("sep Num failed -321", 10);

  if (CUtils::sepNum(-4321) != "-4,321")
    CUtils::exiter("sep Num failed -4321", 10);

  if (CUtils::sepNum(-7654321) != "-7,654,321")
    CUtils::exiter("sep Num failed -7654321", 10);

  return true;
}
