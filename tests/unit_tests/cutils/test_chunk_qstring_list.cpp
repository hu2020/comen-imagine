#include "stable.h"

#include "test_chunk_qstring_list.h"

Test_chunkQStringList::Test_chunkQStringList()
{

}

bool Test_chunkQStringList::doTests()
{
  test1();

  return true;
}

bool Test_chunkQStringList::test1()
{
  QVector<QStringList> res = {};

  QStringList values = {"a"};
  res = CUtils::chunkQStringList({}, 1);
  if (res.size()!=0)
    CUtils::exiter("Failed in chunkQStringList 1", 22);

  res = CUtils::chunkQStringList(values, 1);
  if ((res.size()!=1) || (res[0] != QStringList{"a"}))
    CUtils::exiter("Failed in chunkQStringList 2", 22);

  res = CUtils::chunkQStringList(values, 2);
  if ((res.size()!=1) || (res[0] != QStringList{"a"}))
    CUtils::exiter("Failed in chunkQStringList 3", 22);


  values = QStringList {"a", "b"};
  res = CUtils::chunkQStringList(values, 1);
  if ((res.size()!=2) || (res[0] != QStringList{"a"}) || (res[1] != QStringList{"b"}))
    CUtils::exiter("Failed in chunkQStringList 14", 22);

  res = CUtils::chunkQStringList(values, 2);
  if ((res.size()!=1) || (res[0] != QStringList{"a", "b"}))
    CUtils::exiter("Failed in chunkQStringList 15", 22);

  res = CUtils::chunkQStringList(values, 3);
  if ((res.size()!=1) || (res[0] != QStringList{"a", "b"}))
    CUtils::exiter("Failed in chunkQStringList 15", 22);

  res = CUtils::chunkQStringList(values, 4);
  if ((res.size()!=1) || (res[0] != QStringList{"a", "b"}))
    CUtils::exiter("Failed in chunkQStringList 16", 22);


  values = QStringList {"a", "b", "c"};
  res = CUtils::chunkQStringList(values, 1);
  if ((res.size()!=3) || (res[0] != QStringList{"a"}) || (res[1] != QStringList{"b"}) || (res[2] != QStringList{"c"}))
    CUtils::exiter("Failed in chunkQStringList 24", 22);

  res = CUtils::chunkQStringList(values, 2);
  if ((res.size()!=2) || (res[0] != QStringList{"a", "b"}) || (res[1] != QStringList{"c"}))
    CUtils::exiter("Failed in chunkQStringList 25", 22);

  res = CUtils::chunkQStringList(values, 3);
  if ((res.size()!=1) || (res[0] != QStringList{"a", "b", "c"}))
    CUtils::exiter("Failed in chunkQStringList 26", 22);

  res = CUtils::chunkQStringList(values, 4);
  if ((res.size()!=1) || (res[0] != QStringList{"a", "b", "c"}))
    CUtils::exiter("Failed in chunkQStringList 27", 22);


  return true;
}
