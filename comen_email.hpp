#ifndef COMEN_EMAIL_HPP
#define COMEN_EMAIL_HPP

#include <string>
#include <Poco/Net/MailMessage.h>
#include <Poco/Net/SMTPClientSession.h>

int sendEmail(const Poco::Net::MailMessage& mailMsg,
              const std::string& username, const std::string& password,
              const std::string& host,
              Poco::UInt16 port = Poco::Net::SMTPClientSession::SMTP_PORT);

#endif // COMEN_EMAIL_HPP
