#include "stable.h"

#include "mainwindow.h"
#include "lib/ccrypto.h"
#include "ui_mainwindow.h"

#include "lib/file_buffer_handler/file_buffer_handler.h"


//#include <Poco/Net/MailMessage.h>
//#include <Poco/Net/MailRecipient.h>

//#include <string>

#include "comen_email.hpp"



MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  CGUI::initGUI(ui);
}


MainWindow::~MainWindow()
{
  delete ui;
}

int MainWindow::initMachineEnvironment()
{
  CMachine::createFolders();

  CLog::initLog();

  bool res = CMachine::bootMachine();
  if (!res)
    CUtils::exiter("Fail in booting node", 111);

  loadConfigurationParameters();

  InitializeNode::maybeInitDAG();

  ThreadsHandler::launchThreads();

  connectGUIDataModels();
  CGUI::connectSignalSlots();

  // doRegKeys();
  return 1;
}

void MainWindow::loadConfigurationParameters()
{
  restoreGeometry(CMachine::get().m_global_configs->m_mwGeometry);
  restoreState(CMachine::get().m_global_configs->m_mwState);
}

void MainWindow::saveConfigurationParameters()
{
  CMachine::get().m_global_configs->m_mwGeometry = saveGeometry();
  CMachine::get().m_global_configs->m_mwState	= saveState();
}

int MainWindow::sendEmail()
{
  using Poco::Net::MailMessage;
  using Poco::Net::MailRecipient;

  std::string host      { ui->hostLineEdit->text().toStdString() },
         sender    { ui->senderLineEdit->text().toStdString() },
         recipient { ui->recipientLineEdit->text().toStdString() },
         subject   { ui->subjectLineEdit->text().toStdString() },
         content   { ui->contentTextEdit->toPlainText().toStdString() },
         username  { ui->usernameLineEdit->text().toStdString() },
         password  { ui->passwordLineEdit->text().toStdString() };
  Poco::UInt16 port { static_cast<Poco::UInt16>(ui->portSpinBox->value()) };

  MailMessage msg;

  msg.setSender(sender);
  msg.addRecipient(MailRecipient(MailRecipient::PRIMARY_RECIPIENT, recipient));
  msg.setSubject(subject);
  msg.setContent(content);

  return ::sendEmail(msg, username, password, host, port);
}




void MainWindow::on_pushButton_dummyTrigger_clicked()
{
  FileBufferHandler::doReadAndParseHardDiskInbox();
}


