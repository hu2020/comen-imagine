#include <iostream>
#include <vector>
//#include <initializer_list>

#include <QtSql>
#include <QDir>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <qlogging.h>
#include <qtranslator.h>

#include "constants.h"
#include "global_funcs.hpp"
#include "mainwindow.h"
#include "global_vars.hpp"
#include "lib/bech32.h"
#include "lib/clog.h"
#include "lib/ccrypto.h"
#include "lib/utils/cutils.h"
#include "lib/wallet/signature_unlcok_set.h"
#include "tests/unit_tests/sql_query_generator.h"
#include "tests/unit_tests/cutils/tests_cycle_times.h"
#include "tests/unit_tests/cutils/tests_string_manipulations.h"
#include "tests/unit_tests/cutils_tests.h"
#include "tests/unit_tests/cutils/test_chunk_qstring_list.h"
#include "tests/unit_tests/transactions/test_basic_transaction.h"
#include "tests/unit_tests/transactions/importing_validations/tests_imports_validation.h"


#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

//namespace logging = boost::log;
//namespace keywords = boost::log::keywords;


//void onDirectoryChaned(const QString& path)
//{
//  qDebug() << path;
//  QDir dir(g_monDir);

//  if (dir.exists())
//  {
//    QFileInfoList fiList = dir.entryInfoList(QDir::Files, QDir::Name);

//    if (fiList.size() > g_monFiList.size())
//    {
//      for (const auto& fi: fiList)
//      {
//        if (g_monFiList.contains(fi))
//        {
//          continue;
//        }

//        // Do something to the file.

//        dir.remove(fi.fileName());
//      }
//    }
//  }
//}

//void initDirMonitoring()
//{
//  g_fsWatch = new QFileSystemWatcher(qApp);

//  g_monDir = QApplication::applicationDirPath() + QDir::separator() + "MonDir";

//  QDir dir(g_monDir);

//  if (dir.exists())
//  {
//    g_monFiList = dir.entryInfoList(QDir::Files, QDir::Name);

//    g_fsWatch->addPath(g_monDir);
//    QObject::connect(g_fsWatch, &QFileSystemWatcher::directoryChanged,
//                     [&](const QString& path)
//    {
//      onDirectoryChaned(path);
//    });
//  }
//}

void dummyTestsHandler()
{
  CLog::initLog();

  // Bech32 tests

  //    const std::string& str = "CConsts::HU_DNA_SHARE_ADDRESS"; do js contorl on new hu bech32 address
  //    auto ret = bech32::Decode(str);
  //    std::cout << std::endl << "ret.first: " << ret.first << std::endl;
  //    std::cout << std::endl << "ret.second: " << static_cast<int>(ret.second[0]) << std::endl;
  //    std::string recode = bech32::Encode(ret.first, ret.second);
  //    std::cout << std::endl << "recode: " << recode << std::endl;

//  QString testBech32 = bech32::ComenEncode("eb9591dd8cf44f88479bcbfdaa634e94be6d08df6fbafcb2620e63e9f6a1462b");
//  if (testBech32!="im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl"){
//      CLog::log("Error in -------Bech32 ERRORRRRRR" + testBech32);
//      exit(3);
//  }


  bool do_tests = true;
  if (do_tests)
  {

    TestBasicTransaction::doTests();

    // TestsImportsValidation::doTests(); // note: heavy resouce consumer test

    Test_chunkQStringList::doTests();

    TestsStringmanipulations::doTests();

    TestsCycleTimes::doTests();

    TestsPremutation::doTests();

    CMerkleTests::doTests();

    TestsCCrypto::doTests();

    //SQLQueryGenerator::doTests();

    CUtilsTests::doTests();

    TestsPGP::doTests();

  }



  if (false)
  {

    // JSON tests unserialize and parse
    string serializedStr = {R"({"sType":"Strict","sVer":"0.0.0","sSets":[{"sKey":"022968b10e02e2af51a5965b9735ac2c75c51c71207f85bec0bd49fa61902f8619","pPledge":"Y","pDelegate":"Y"},{"sKey":"0339129227adebcb49c89fdcbf036249b1e277727895b6803378a0364c33bc0b46","pPledge":"N","pDelegate":"N"},{"sKey":"03a797608e14ee87a93c0bf7d7d121593c5985030e9053e4d062bf081d59da956b","pPledge":"N","pDelegate":"N"},{"sKey":"03f9e4a46c160246e518c41c661b6eeae89aee2188e9dd454274bcca3414a2ed54","pPledge":"N","pDelegate":"N"},{"sKey":"03c146c6e882a1be14606d4a56a72905620064d30aaefa61bf99c1b4dcd10412ad","pPledge":"Y","pDelegate":"Y"},{"sKey":"02bcf7558f443691819af3d1ab6661f379efb8bbda9791f81156749f211ad2501a","pPledge":"N","pDelegate":"N"}],"mProof":["r.87afd722ed6535327ca5eae69f26a9effa17c7b993f506062c595015cdb8a2e6","r.8f82ac108fb11a7f2d5cabf4a25675c85328f07416e433698e7f3d0e1d32529c","r.4f928483e496d9778c5fa005457120b1fa9742a2ef0f69f1bfd1c7a9040a7ce0"],"lHash":null,"salt":"a9de676dd54b672c"}    )"};
    QString strTmp = QString::fromStdString(serializedStr);
    std::cout << std::endl << "serializedStr: " << serializedStr;

    qWarning() << strTmp;
    QJsonDocument d = QJsonDocument::fromJson(strTmp.toUtf8());
    QJsonObject unserObj = d.object();
    QJsonValue sSetsList = unserObj.value(QString("sSets"));
    qWarning() << sSetsList;
    QJsonObject item = sSetsList.toObject();
    qWarning() << ("QJsonObject of description: ") << item;

    /* in case of string value get value and convert into string*/
    qWarning() << ("QJsonObject[appName] of description: ") << item["description"];
    QJsonValue subobj = item["description"];
    qWarning() << subobj.toString();

    /* in case of array get array and convert into string*/
    qWarning() << ("QJsonObject[appName] of value: ") << item["imp"];
    QJsonArray test = item["imp"].toArray();
    qWarning() << test[1].toString();

  }


  // ccrypto tests

  if (CCrypto::base64Decode(CCrypto::base64Encode("")) != "")
  {
    CLog::log(QString::fromStdString("Error in base64Decode/base64Encode"));
    exit(3);
  }
  if (CCrypto::base64Decode(CCrypto::base64Encode("a")) != "a")
  {
    CLog::log(QString::fromStdString("Error in base64Decode/base64Encode"));
    exit(3);
  }


  // time funcs tests

  TimeDiff td;
  td = CUtils::timeDiff("2000-01-01 00:00:00", "2000-01-01 00:00:00");
  if (td != TimeDiff{})
  {
    CLog::log(QString::fromStdString("Error in timeDiff 1"));
    exit(3);
  }

  td = CUtils::timeDiff("2000-01-01 00:00:00", "2000-01-01 00:00:01");
  if (td != TimeDiff {
     0,
     0,
     0,
     0,
     0,
     0,
     0,
     0,
     0,
     0,
     1,
     1
  })
  {
    CLog::log(QString::fromStdString("Error in timeDiff 2"));
    exit(3);
  }

  td = CUtils::timeDiff("2000-01-01 00:00:00", "2000-01-01 00:01:01");
  if (td != TimeDiff {
     0,
     0,
     0,
     0,
     0,
     0,
     0,
     0,
     1,
     1,
     61,
     1
  })
  {
    CLog::log(QString::fromStdString("Error in timeDiff 3"));
    exit(3);
  }

  td = CUtils::timeDiff("2000-01-01 00:00:00", "2000-01-01 01:01:01");
  if (td != TimeDiff {
     0,
     0,
     0,
     0,
     0,
     0,
     1,
     1,
     61,
     1,
     3661,
     1
  })
  {
    CLog::log(QString::fromStdString("Error in timeDiff 4"));
    exit(3);
  }

  td = CUtils::timeDiff("2000-01-01 00:00:00", "2000-01-02 01:01:01");
  if (td != TimeDiff {
     0,
     0,
     0,
     0,
     1,
     1,
     25,
     1,
     1501,
     1,
     90061,
     1
  })
  {
    CLog::log(QString::fromStdString("Error in timeDiff 5"));
    exit(3);
  }





  if (false)
  {


  // JSON serilization test
  QJsonObject jObj;

  jObj["sType"] = "Strict";
  jObj["sVer"] = "0.0.0";

  QJsonObject sSet1;
  sSet1["sKey"] = "022968b10e02e2af51a5965b9735ac2c75c51c71207f85bec0bd49fa61902f8619";
  sSet1["pPledge"] = "Y";
  sSet1["pDelegate"] = "Y";
  QJsonObject sSet2;
  sSet2["sKey"] = "0339129227adebcb49c89fdcbf036249b1e277727895b6803378a0364c33bc0b46";
  sSet2["pPledge"] = "N";
  sSet2["pDelegate"] = "N";

  QJsonArray sSets = { sSet1, sSet2, QString() };
  jObj["sSets"] = sSets;
  QString serJ = QJsonDocument(jObj).toJson();
  qWarning() << serJ;
  std::cout << std::endl << "C++ Serialized: " << QString(serJ).toStdString();

  std::cout << std::endl << "Done";

  }

}
