CoMEn Core integration/staging tree
=====================================

What is CoMEn?
----------------

Comen, stands for Community Maker Engine, is an experimental standalone online community maker software. Each community have its own Rules and money.
people can send money of different community to each other. There is no central authority. Managing transactions and issuing money are carried out
collectively by the community decision. Comen is the name of open source software which enables establish communities and their mony.

For more information, as well as an immediately usable, binary version of the Comen software (core or mobile Wallet), ask your friends!
There is no central server at all.

License
-------

Comen Core is released under the terms of the GPLV2 license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/gpl-2.0.php.





Dependencies:
sudo apt-get install libboost-all-dev

sudo apt-get install libpoco-dev
sudo apt-get install libcrypto++6 libcrypto++6-dbg libcrypto++-dev
sudo apt-get install libcrypto++-utils



While CoMEn can be installed using, Sqlite Postgres is preferred for any significant use.
later should connect to sudo apt-get install odb




coding style:
https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md
