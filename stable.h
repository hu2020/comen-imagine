
#if defined __cplusplus

// Add C++ includes here
#include <iostream>
#include <vector>
#include <string.h>
#include <random>
#include <stdio.h>
#include <assert.h>
#include <tuple>
#include <thread>
#include <future>
#include <math.h>

// -  -  -  -  Qt includes
#include <QApplication>
#include "QDebug"
#include <QDir>
#include <QHash>
#include <QLabel>
#include "QJsonDocument"
#include <QJsonObject>
#include <QMetaType>
#include <QObject>
#include <QPushButton>
#include <QRegularExpression>
#include <QSet>
#include <QString>
#include <QStringList>
#include <QtSql>
#include <QVariant>
#include <QVector>

// -  -  -  -  Qt QUI includes
#include <QMessageBox>
#include <QTableWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QSpinBox>
#include <QTextEdit>



//#include "thirdparty/include/libmain.h"




#include "constants.h"
#include "comen_config.h"
#include "lib/clog.h"
#include "lib/database/db_handler.h"
#include "lib/database/db_model.h"
#include "lib/utils/cmerkle.h"
#include "lib/utils/compressor.h"
#include "lib/utils/cutils.h"
#include "lib/utils/permutation_handler.h"
#include "lib/i18n/multi_language.h"








#endif
