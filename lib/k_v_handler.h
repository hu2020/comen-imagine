#ifndef KVHANDLER_H
#define KVHANDLER_H



class KVHandler
{
public:
  static QString stbl_kvalue;
  KVHandler();

  static QString getValue(const QString &kvKey);

  static bool deleteKey(const QString &kvKey);

  static bool updateKValue(const QString &key, const QString &value);

  static bool upsertKValue(
    const QString &key,
    const QString &value,
    const bool &log = true);

  static bool setValue(
    const QString &key,
    const QString &value,
    const bool &log = true);

  static QVDRecordsT serach(
    const ClausesT& clauses,
    const QStringList& fields,
    const OrderT& order = {},
    const int& limit = 0);

};

#endif // KVHANDLER_H
