#include "stable.h"

#include "lib/ccrypto.h"

#include "trx_utils.h"

TrxUtils::TrxUtils()
{

}

std::vector<TInput*> TrxUtils::normalizeInputs(
  const std::vector<TInput*> inputs)
{
  // BIP69
  std::vector<TInput*> normalized_inputs {};
  if (inputs.size() > 1)
  {
    QHash<QString, TInput*> dict {};
    for (TInput* an_input: inputs)
    {
      QString key = QStringList {
          an_input->m_transaction_hash,
          CUtils::paddingLengthValue(QString::number(an_input->m_output_index))}.join("");
      dict[key] = an_input;
    }
    QStringList keys = dict.keys();
    keys.sort();
    for (QString k: keys)
    {
      normalized_inputs.push_back(dict[k]);
    }
  } else{
    return inputs;
  }
  return normalized_inputs;
}


std::tuple<bool, std::vector<TOutput*> > TrxUtils::normalizeOutputs(
  const std::vector<TOutput*> outputs)
{
  for (TOutput* an_output: outputs)
  {
    if (!CConsts::TREASURY_PAYMENTS.contains(an_output->m_address) &&
        !CCrypto::isValidBech32(an_output->m_address))
    {
      CLog::log("invalid trx output " + CUtils::dumpIt(an_output), "trx", "error");
      return {false, {}};
    }

    // TODO: implement some control like no-floatingpoint-part etc ...

  }

  std::vector<TOutput*> normalized_outputs {};

  if (outputs.size() > 1)
  {
    QHash<QString, TOutput*> dict {};
    for (COutputIndexT inx = 0; inx < outputs.size(); inx++)
    {
      TOutput* an_output = outputs[inx];
      QString key = QStringList {
        an_output->m_address,
        QString::number(an_output->m_amount).rightJustified(20, '0'),
        QString::number(inx).rightJustified(5, '0')}.join(",");
      dict[key] = an_output;
    }


    QStringList keys = dict.keys();
    keys.sort();
    for (QString key: keys)
      normalized_outputs.push_back(dict[key]);
    return {true, normalized_outputs};

  }else{
    return {true, outputs};


  }
}

std::tuple<bool, QJsonArray> TrxUtils::normalizeOutputsJ(
  const QJsonArray& outputs,
  const bool& sortIt)
{
  // BIP69
  JARecordsT normalized_outputs = {};
  for (auto an_output_: outputs)
  {

    // entry control
    QJsonArray an_output = an_output_.toArray();
    if (an_output.size() != 2)
    {
      CLog::log("invalid trx output " + CUtils::dumpIt(an_output), "trx", "error");
      return {false, {}};
    }

    // address control
    if (!CConsts::TREASURY_PAYMENTS.contains(an_output[0].toString()) &&
        !CCrypto::isValidBech32(an_output[0].toString()))
    {
      CLog::log("invalid trx output " + CUtils::dumpIt(an_output), "trx", "error");
      return {false, {}};
    }

    // value controll
    // TODO: implement some control like no-floatingpoint-part etc ...

    normalized_outputs.push_back(an_output);

  }

  QJsonArray final_outputs {};

  if (sortIt && (normalized_outputs.size() > 1))
  {
    QJADicT dict {};
    for (COutputIndexT inx = 0; inx < normalized_outputs.size(); inx++)
    {
      QJsonArray an_output = normalized_outputs[inx];
      QString key = QStringList {
        an_output[0].toString(),
        QString::number(an_output[1].toDouble()).rightJustified(20, '0'),
        QString::number(inx).rightJustified(5, '0')}.join(",");
      dict[key] = an_output;
    }


    QStringList keys = dict.keys();
    keys.sort();
    for (QString key: keys)
      final_outputs.push_back(dict[key]);
  }

  return {true, final_outputs};
}








