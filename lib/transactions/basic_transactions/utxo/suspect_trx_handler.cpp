#include "stable.h"

#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"
#include "lib/transactions/basic_transactions/utxo/votes_arranger.h"

#include "suspect_trx_handler.h"

const QString SuspectTrxHandler::stbl_trx_suspect_transactions = "c_trx_suspect_transactions";
const QStringList SuspectTrxHandler::stbl_trx_suspect_transactions_fields = {"st_id", "st_vote_date", "st_coin", "st_logger_block", "st_spender_block", "st_spender_doc", "st_receive_order", "st_spend_date"};

SuspectTrxHandler::SuspectTrxHandler()
{

}

/**
 * @brief SuspectTrxHandler::getSusInfoByBlockHash
 * @param blockHash
 * return {has_sus_records, votes_dict}
 */
std::tuple<bool, QHash<CBlockHashT, TheVotes> > SuspectTrxHandler::getSusInfoByBlockHash(
  const CDocHashT block_hash)
{
  QHash<CBlockHashT, TheVotes> votesDict {};
  // retrieve votes about ALL votes about all coins which are used in given block and are spent in other blocks too
  QString complete_query = "SELECT st_voter, st_coin, st_spender_block, st_spender_doc, st_vote_date FROM " + stbl_trx_suspect_transactions;
  complete_query += " WHERE st_coin IN (SELECT st_coin FROM " + stbl_trx_suspect_transactions + " WHERE st_spender_block=:st_spender_block) ";
  complete_query += " ORDER BY st_voter, st_coin";
  QueryRes votes = DbModel::customQuery(
    "db_comen_general",
    complete_query,
    {"st_voter", "st_coin", "st_spender_block", "st_spender_doc", "st_vote_date"},
    0,
    {{"st_spender_block", block_hash}});
  if (votes.records.size() == 0)
  {
    CLog::log("Transaction Didn't recognized as an suspicious case. block(" + CUtils::hash8c(block_hash) + ")", "trx", "trace");
    return {false, votesDict};
  }
  CLog::log("Found sus trxes(" + QString::number(votes.records.size()) + " records) for block(" + CUtils::hash8c(block_hash) + ")", "trx", "warning");


  // control if they are cloned trx?


  // calculate already votes

  for (QVDicT vote: votes.records)
  {
    QString spender_block = vote.value("st_spender_block").toString();
    if (!votesDict.contains(spender_block))
      votesDict[spender_block] = TheVotes {{}, 0.0};

    auto[share_count_, sharesPercent] = DNAHandler::getAnAddressShares(vote.value("st_voter").toString(), vote.value("st_vote_date").toString()); // dna handler to calculate
    Q_UNUSED(share_count_);
    if (sharesPercent < CConsts::MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER)
        sharesPercent = CUtils::iFloorFloat(CConsts::MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER);

    votesDict[spender_block].m_votes.push_back(TheVote{vote.value("st_voter").toString(), sharesPercent});

  }

  for (CBlockHashT blkHsh: votesDict.keys())
  {
    QHash<CAddressT, DNASharePercentT> uniqVoters {};
    for (TheVote aVote: votesDict[blkHsh].m_votes)
    {
      uniqVoters[aVote.m_voter] = aVote.m_sharesPercent;
    };
    votesDict[blkHsh].m_sumPercent = 0.0;
    for (QString voter: uniqVoters.keys())
    {
      votesDict[blkHsh].m_sumPercent += uniqVoters[voter];
    };
    votesDict[blkHsh].m_sumPercent = CUtils::iFloorFloat(votesDict[blkHsh].m_sumPercent);
  };
  return {true, votesDict}; //cloneStatus
}

/**
 * @brief SuspectTrxHandler::getSusInfoByDocHash
 * @param invokedDocHash
 * return {all_Coins_Are_Valid, raw_votes}
 */
std::tuple<bool, std::vector<RawVote> > SuspectTrxHandler::getSusInfoByDocHash(
  const CDocHashT& invokedDocHash)
{

  // retrieve all trx which using same inputs of given trx
  QString complete_query = "SELECT " + stbl_trx_suspect_transactions_fields.join(", ") + " FROM " + stbl_trx_suspect_transactions + " WHERE st_coin IN ";
  complete_query += " (SELECT st_coin FROM " + stbl_trx_suspect_transactions + " WHERE st_spender_doc=:st_spender_doc) ";
  complete_query += " ORDER BY st_voter, st_receive_order";
  QueryRes rawVotes = DbModel::customQuery(
    "db_comen_general",
    complete_query,
    stbl_trx_suspect_transactions_fields,
    0,
    {{":st_spender_doc", invokedDocHash}});

  if (rawVotes.records.size() == 0)
  {
    CLog::log("invokedDocHash(" + CUtils::hash8c(invokedDocHash) + ") didn't recognized as an suspicious doc!", "trx", "error");
    return {
      true,
      {}};
      // rawVotes: [],
      //  coinsAndVotersDict: {},
      //  coinsAndOrderedSpendersDict: {},
      //  coinsAndPositionsDict: {},
      //  susVoteRes: {}
  }

  std::vector<RawVote> row_votes {};
  for(QVDicT a_row: rawVotes.records)
  {
    row_votes.emplace_back(RawVote(a_row));
  }
  return {
    false,
    row_votes};
//  ,
//    coinsAndVotersDict: {},
//    coinsAndOrderedSpendersDict: {},
//    coinsAndPositionsDict: {},
//    susVoteRes: {}
//  };

}

/**
 * @brief SuspectTrxHandler::retrieveVoterPercentages
 * @param raw_votes
 * return {status, raw_votes}
 */
std::tuple<bool, std::vector<RawVote> > SuspectTrxHandler::retrieveVoterPercentages(
  std::vector<RawVote> raw_votes)
{

  // retrieve voter shares
  QHash<QString, double> tmpVoterDateSharesDIct = {};
  for (uint32_t inx=0; inx< raw_votes.size(); inx++)
  {
    QString key = raw_votes[inx].m_voter + ":" + raw_votes[inx].m_vote_date;  // TODO: optimaized it to use start date f period instead of analog date in range, in  order to reduce map table
    if (!tmpVoterDateSharesDIct.keys().contains(key))
    {
      auto[shares_, percentage] = DNAHandler::getAnAddressShares(raw_votes[inx].m_voter, raw_votes[inx].m_vote_date);
      Q_UNUSED(shares_);
      if (percentage < CConsts::MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER)
      {
        CLog::log("shares == 0 for (" + CUtils::shortBech16(raw_votes[inx].m_voter) + "): percentage(" + QString::number(percentage) + ") ", "trx", "info");
        percentage = CConsts::MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER;
        CLog::log("shares == 0 for (" + CUtils::shortBech16(raw_votes[inx].m_voter) + "): percentage(" + QString::number(percentage) + ") ", "trx", "info");
      }
      tmpVoterDateSharesDIct[key] = percentage;
    }
    raw_votes[inx].m_voterPercentage = tmpVoterDateSharesDIct[key];
  }

  return {false, raw_votes};
}


void SuspectTrxHandler::checkDocValidity(
  UTXOImportDataContainer* block_inspect_container,
  const CDocHashT& invokedDocHash,
  const bool doDBLog,
  const bool doConsoleLog)
{
  CLog::log("trx suspect transactions for doc(" + CUtils::hash8c(invokedDocHash) + ") ", "trx", "info");

//  if (doDBLog)
//    this.logSus({
//      lkey: '1.suspect transactions',
//      blockHash: '-',
//      docHash: invokedDocHash,
//      refLocs: '-',
//      logBody: utils.stringify(rawVotes)});


  VotesArranger::doGroupByCoinAndVoter(
    block_inspect_container,
    invokedDocHash,
    doDBLog);
  if (block_inspect_container->m_transactions_validity_check[invokedDocHash].m_valid)
    return;

  if (doConsoleLog)
    CLog::log("coinsAndVotersDict: " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndVotersDict), "trx", "trace");




  // preparing coinsAndOrderedSpendersDict
  VotesArranger::doGroupByCoinAndSpender(
    block_inspect_container,
    invokedDocHash,
    doDBLog);
  if (doConsoleLog)
    CLog::log("coinsAndOrderedSpendersDict: " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndOrderedSpendersDict), "trx", "trace");



  // preparing coinsAndPositionsDict
  VotesArranger::doGroupByCoinAndPosition(
    block_inspect_container,
    invokedDocHash,
    doDBLog);
  if (doConsoleLog)
    CLog::log("coins And Positions Dict: " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndPositionsDict), "trx", "trace");





  VotesArranger::doSusVoteRes(
    block_inspect_container,
    invokedDocHash,
    doDBLog);
  if (doConsoleLog)
    CLog::log("sus Vote final Res: " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_transactions_validity_check[invokedDocHash].m_susVoteRes), "trx", "trace");

  return;

//  return { , cvRes.m_coinsAndVotersDict, coinsAndPositionsDict, };
}


QVDRecordsT SuspectTrxHandler::searchInSusTransactions(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int limit)
{
  QueryRes posts = DbModel::select(
    stbl_trx_suspect_transactions,
    fields,
    clauses,
    order,
    limit);

  return posts.records;
}
