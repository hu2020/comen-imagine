#ifndef SUSPECTTRXHANDLER_H
#define SUSPECTTRXHANDLER_H

class TheVotes;
class RawVote;
class ValidityCheck;

class SuspectTrxHandler
{
public:
  SuspectTrxHandler();

  const static QString stbl_trx_suspect_transactions;
  const static QStringList stbl_trx_suspect_transactions_fields;

  static std::tuple<bool, QHash<CBlockHashT, TheVotes> > getSusInfoByBlockHash(const QString block_hash);

  static std::tuple<bool, std::vector<RawVote> > getSusInfoByDocHash(
    const CDocHashT& invokedDocHash);

  static std::tuple<bool, std::vector<RawVote> > retrieveVoterPercentages(
    std::vector<RawVote> raw_votes);

  static void checkDocValidity(
    UTXOImportDataContainer* block_inspect_container,
    const CDocHashT& invokedDocHash,
    const bool doDBLog = true,
    const bool doConsoleLog = true);

  static QVDRecordsT searchInSusTransactions(
    const ClausesT& clauses = {},
    const QStringList& fields = stbl_trx_suspect_transactions_fields,
    const OrderT& order = {},
    const int limit = 0);

};

#endif // SUSPECTTRXHANDLER_H
