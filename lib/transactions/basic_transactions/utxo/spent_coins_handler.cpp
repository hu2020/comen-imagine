#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/block/document_types/document.h"
#include "lib/transactions/basic_transactions/utxo/coin.h"

#include "spent_coins_handler.h"

const QString SpentCoinsHandler::stbl_trx_spend = "c_trx_spend";
const QStringList SpentCoinsHandler::stbl_trx_spend_filed = {"sp_coin", "sp_spend_loc", "sp_spend_date"};


SpentCoinsHandler::SpentCoinsHandler()
{

}

std::tuple<bool, GRecordsT> SpentCoinsHandler::findCoinsSpendLocations(const QStringList& coins)
{
  // finding the block(s) which are used these coins and already are registerg in DAG(if they did)
  // this function just writes some logs and have not effect on block validation accept/denay
  GRecordsT recorded_spend_in_DAG {};
  for (CCoinCodeT a_coin: coins)
  {
    CLog::log("SCUDS: looking for a Coin(" + CUtils::shortCoinRef(a_coin) + ")", "trx", "trace");
    auto[doc_hash, inx_] = CUtils::unpackCoinCode(a_coin);
    Q_UNUSED(inx_);

    // find broadly already recorded block(s) which used(or referenced) this input-doc_hash
    auto[wBlocks, map_] = DAG::getWBlocksByDocHash(QStringList {doc_hash});
    Q_UNUSED(map_);

    CLog::log("SCUDS: looking for doc(" + CUtils::hash8c(doc_hash) + ") returned " + QString::number(wBlocks.size()) + " potentially blocks", "trx", "trace");
    if (wBlocks.size()> 0)
    {
      for (QVDicT wBlock: wBlocks)
      {
        QJsonObject refBlock = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(wBlock.value("b_body").toString()).content);

        CBlockHashT block_hash = refBlock.value("bHash").toString();
        if (block_hash == "")
          continue;

        CLog::log("SCUDS: controlling block(" + CUtils::hash8c(block_hash) + ") ", "trx", "trace");
        if (refBlock.keys().contains("docs") && (refBlock.value("docs").toArray().size() > 0))
        {
          CLog::log("SCUDS: block(" + CUtils::hash8c(block_hash) + " has " + QString::number(refBlock.value("docs").toArray().size())+ " docs", "trx", "info");
          for (auto doc_: refBlock.value("docs").toArray())
          {
            QJsonObject doc = doc_.toObject();
            if (doc.keys().contains("inputs"))
            {
              QJsonArray the_inputs = doc.value("inputs").toArray();
              CLog::log("SCUDS: doc has " + QString::number(the_inputs.size()) + " inputs", "trx", "info");
              for (int input_index = 0; input_index < the_inputs.size(); input_index++)
              {
                QJsonArray trx_input = the_inputs[input_index].toArray();
                // if the doc_hash is referenced as an input index, select id
                if (trx_input[0].toString() == doc_hash)
                {
                  QString tmp_coin = CUtils::packCoinCode(trx_input[0].toString(), trx_input[1].toInt());
                  CLog::log("SCUDS: controlling input(" + CUtils::shortCoinRef(tmp_coin) + ")", "trx", "info");
                  if (coins.contains(tmp_coin))
                  {
                    if (!recorded_spend_in_DAG.keys().contains(tmp_coin))
                      recorded_spend_in_DAG[tmp_coin] = QVDRecordsT {};
                    recorded_spend_in_DAG[tmp_coin].push_back(QVDicT {
                      {"block_hash", refBlock.value("bHash")},
                      {"doc_hash", doc_hash},
                      {"input_index", input_index}
                    });
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return {true, recorded_spend_in_DAG};
}

QVDRecordsT SpentCoinsHandler::searchInSpentCoins(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_trx_spend,
    fields,
    clauses,
    order,
    limit,
    false,
    false);
  return res.records;
}


/**
* accepts given coins and prepares an ordered history of coins spent
*/
SpendCoinsList* SpentCoinsHandler::makeSpentCoinsDict(const QStringList& coins)
{
  // TODO: maybe optimize it via bloom filters for daily(cyclic) spent coins
  SpendCoinsList* spend_coins = new SpendCoinsList();
//  GRecordsT coinsInSpentTable = {};
//  let spendsOrder = {};
  QVDRecordsT inDAGRecordedCoins = searchInSpentCoins(
    {{"sp_coin", coins, "IN"}},
    stbl_trx_spend_filed,
    {{"sp_spend_date", "ASC"}});
  if (inDAGRecordedCoins.size()> 0)
  {
    for (QVDicT sp: inDAGRecordedCoins)
    {
      QString the_coin = sp.value("sp_coin").toString();
      if (!spend_coins->m_coins_dict.keys().contains(the_coin))
        spend_coins->m_coins_dict[the_coin] = std::vector<SpendCoinInfo*> {};
      auto[block_hash_, doc_hash_] = CUtils::unpackCoinSpendLoc(sp.value("sp_spend_loc").toString());

      spend_coins->m_coins_dict[the_coin].emplace_back(
       new SpendCoinInfo {
         sp.value("sp_spend_date").toString(),
         block_hash_,
         doc_hash_});

      /**
      * making a dictionary of history of spending each unique coin,
      * and ordering by spent time in machine's point of view.
      * later it will be used to vote about transactions priority.
      * it is totally possible in this step machine can not retrieve very old spending
      * (because the spent table periodicly truncated), in this case machine will vote a doublespended doc as a first document
      * later in "doGroupByCoinAndVoter" method we add second index(vote date) to securing the spend order
      *
      */
      // if (!_.has(spendsOrder, the_coin))
      //     spendsOrder[the_coin] = [];
      // spendsOrder[the_coin].push({
      //     date: sp.spSpendDate,
      //     blockHash: spendInfo.blockHash,
      //     docHash: spendInfo.docHash,
      // });
    }
//    CLog::log("already Spent And Recorded Inputs Dict: " + CUtils::dumpIt(spend_coins), "trx", "error");
  }
  return spend_coins;
}

bool SpentCoinsHandler::markSpentAnInput(
  const QString& the_coin,
  const QString& spend_loc,
  const QString& spend_date,
  const QString& cDate)
{
  /**
   *
   * remove old records
   * TODO: infact we should not remove history(at least for some resonably percent of nodes)
   * they have to keep ALL data, specially for long-term loans, to un-pledge the account, nodes needed this information.
   * wherase repayments took place in RpBlocks.RpDocs and most of time are sufficient to close a pledge,
   * but sometimes pledger can pay all the loan in one transaction to get ride of loan
   * or pays a big part of loan to reduce interest rate.
   * in all of these case, the nodes which are engaged in loan(e.g. the arbiters nodes) must have these information.
   * although all trx info are reachable via blocks, but this table provides a faster & easier crawling
   * so in pledge time, the pledgee (can or must) add a bunch of long-term-data-backers(LTDB or LoTeDB)
   * as evidence of repayments.
   * in such a way, to unpledge an account either pledgee signature or these LoTeDB signatures will be sufficient,
   * so, there is not obligue to mantain all data by all nodes
   * obviously these LoteDbs will be payed by pledge contract based on repayments longth
   * TODO: must be implemented
   */
  DbModel::dDelete(
    stbl_trx_spend,
    {{"sp_spend_date", CUtils::minutesBefore(CMachine::getCycleByMinutes() * CConsts::KEEP_SPENT_COINS_BY_CYCLE, cDate),"<"}},
    true,
    false);

  DbModel::insert(
    stbl_trx_spend,
    {{"sp_coin", the_coin},
    {"sp_spend_loc", spend_loc},
    {"sp_spend_date", spend_date}},
    true,
    false);

  return true;
}

bool SpentCoinsHandler::markAsSpentAllBlockInputs(
  const Block* block,
  const QString& cDate)
{
  for (Document* doc: block->getDocuments())
  {
    // TODO FIXME: discover cloned transactions and mark them too
    for (TInput* input: doc->getInputs())
    {
      markSpentAnInput(
        input->getCoinCode(),  // the spent coin
        CUtils::packCoinSpendLoc(block->getBlockHash(), doc->getDocHash()),  // spending location
        block->m_block_creation_date,
        cDate);
    }
  }

  return true;
}

QJsonObject SpentCoinsHandler::convertSpendsToJsonObject(const SpendCoinsList* sp)
{
  QJsonObject Jout {};
  for (QString a_group_key: sp->m_coins_dict.keys())
  {
    QJsonArray Jgroup {};
    for (SpendCoinInfo* a_row: sp->m_coins_dict[a_group_key])
    {
      QJsonObject Jrow{
        {"spendDate", a_row->m_spend_date},
        {"spendBlockHash", a_row->m_spend_block},
        {"spendDocHash", a_row->m_spend_document},
        {"spendOrder", QVariant::fromValue(a_row->m_spend_order).toDouble()},
      };
      Jgroup.push_back(Jrow);
    }
    Jout.insert(a_group_key, Jgroup);
  }
  return Jout;
}
