#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "votes_arranger.h"

void VotesArranger::doGroupByCoinAndPosition(
  UTXOImportDataContainer* block_inspect_container,
  const CDocHashT& invokedDocHash,
  bool doDBLog)
{
  QHash<CCoinCodeT, QHash<uint32_t, QHash<CDocHashT, CoinOrderedSpender> > > coinsAndOrderedSpendersDict =
  block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndOrderedSpendersDict;
  QHash<CCoinCodeT, QHash<uint32_t, CoinAndPosition> > coinsAndPositionsDict {};
  QStringList coins = coinsAndOrderedSpendersDict.keys();
  for (CCoinCodeT aCoin: coins)
  {
    if (!coinsAndPositionsDict.keys().contains(aCoin))
        coinsAndPositionsDict[aCoin] = QHash<uint32_t, CoinAndPosition> {};

    for (int64_t position = 0; position < coinsAndOrderedSpendersDict[aCoin].size(); position++)
    {
      if (!coinsAndPositionsDict[aCoin].contains(position))
        coinsAndPositionsDict[aCoin][position] = CoinAndPosition {
          {},
          {},
          0,
          0};

      // integrating votes of all documents in this position
      QHash<CDocHashT, CoinOrderedSpender> documentsInThisPosition = coinsAndOrderedSpendersDict[aCoin][position];
      for (CDocHashT docHash: documentsInThisPosition.keys())
      {
        VoteData voteData = coinsAndOrderedSpendersDict[aCoin][position][docHash].m_voteData;
        if (voteData.m_outside6VotesGain > voteData.m_inside6VotesGain)
        {
          coinsAndPositionsDict[aCoin][position].m_outsideTotal += voteData.m_outside6VotesGain;
          coinsAndPositionsDict[aCoin][position].m_outside6h.emplace_back(InOutside6hElm {
            docHash,
            voteData.m_outside6VotesGain,
            voteData.m_outside6VotersCount});

        } else if (voteData.m_outside6VotesGain < voteData.m_inside6VotesGain) {
            coinsAndPositionsDict[aCoin][position].m_insideTotal += voteData.m_inside6VotesGain;
            coinsAndPositionsDict[aCoin][position].m_inside6h.emplace_back(InOutside6hElm {
              docHash,
              voteData.m_inside6VotesGain,
              voteData.m_inside6VotersCount});

        } else if (voteData.m_outside6VotesGain == voteData.m_inside6VotesGain) {
          if (voteData.m_outside6VotersCount > voteData.m_inside6VotersCount)
          {
            coinsAndPositionsDict[aCoin][position].m_outsideTotal += voteData.m_outside6VotesGain;
            coinsAndPositionsDict[aCoin][position].m_outside6h.emplace_back(InOutside6hElm {
              docHash,
              voteData.m_outside6VotesGain,
              voteData.m_outside6VotersCount});

          } else if (voteData.m_outside6VotersCount < voteData.m_inside6VotersCount) {
              coinsAndPositionsDict[aCoin][position].m_insideTotal += voteData.m_inside6VotesGain;
              coinsAndPositionsDict[aCoin][position].m_inside6h.emplace_back(InOutside6hElm {
                docHash,
                voteData.m_inside6VotesGain,
                voteData.m_inside6VotersCount});

          } else if (voteData.m_outside6VotersCount == voteData.m_inside6VotersCount) {
            QString outHash = CCrypto::keccak256(
              QString::number(voteData.m_outside6VotersCount) +
              QString::number(voteData.m_outside6VotesGain));

            QString inHash = CCrypto::keccak256(
              QString::number(voteData.m_inside6VotersCount) +
              QString::number(voteData.m_inside6VotesGain));

            if (outHash > inHash)
            {
              coinsAndPositionsDict[aCoin][position].m_outsideTotal += voteData.m_outside6VotesGain;
              coinsAndPositionsDict[aCoin][position].m_outside6h.emplace_back(InOutside6hElm {
                docHash,
                voteData.m_outside6VotesGain,
                voteData.m_outside6VotersCount});

            } else {
              coinsAndPositionsDict[aCoin][position].m_insideTotal += voteData.m_inside6VotesGain;
              coinsAndPositionsDict[aCoin][position].m_inside6h.emplace_back(InOutside6hElm {
                docHash,
                voteData.m_inside6VotesGain,
                voteData.m_inside6VotersCount});

            }
          }
        }
      }
    }
  }

  CLog::log("coinsAndPositionsDict for doc(" + CUtils::hash8c(invokedDocHash) + "): " + UTXOImportDataContainer::dumpMe(coinsAndPositionsDict), "trx", "trace");

//  if (doDBLog)
//    this._super.logSus({
//        lkey: '4.coinsAndPositionsDict',
//        blockHash: '-',
//        docHash: invokedDocHash,
//        coins,
//        logBody: utils.stringify(coinsAndPositionsDict)
//    });

  block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndPositionsDict = coinsAndPositionsDict;

  return;
}




/**
 *

coinsAndPositionsDict:
{
    "e45630b91c0df1323e8a861464e9fdb8deb705c523cb977bca09d58c5409ef5c:0": {
        "0": {
            "inside6h": [],
            "outside6h": [
                {
                    "docHash": "ba35317697b2836c890faf5dc8b2046d46b9d637ebcd3c37144babb501b117dd",
                    "votes": 99.99999999999,
                    "voters": 2
                }
            ],
            "outsideTotal": 99.99999999999,
            "insideTotal": 0
        },
        "1": {
            "inside6h": [],
            "outside6h": [
                {
                    "docHash": "8830e21bfa191b477847587adcc2d4ba1ed1c9339205b6afe0b693c38f611acb",
                    "votes": 99.99999999999,
                    "voters": 2
                }
            ],
            "outsideTotal": 99.99999999999,
            "insideTotal": 0
        }
    }
}

it means for the coin e45630:0 in position zero there is only one spender which is out of 6 hours
and this spender is valid, while there is another spender in position 2 and difinetaly must be rejected

 */
