#ifndef COIN_H
#define COIN_H


/**
 * @brief The CCoin class
 * information about the block/doc in which the coin is created
 */
class CCoin
{
public:
  CCoin();
  CCoin(
    const CCoinCodeT& coin,
    const CAddressT& owner,
    const CMPAIValueT& amount,

    const CDateT& creation_date = "",

    const CBlockHashT& block_hash = "",
    const CDocIndexT& doc_index = 0,
    const CDocHashT& doc_hash = "",
    const COutputIndexT& output_index = 0,
    const QString& cycle = "");

  CCoinCodeT m_coin = "";
  CAddressT m_owner = "";
  CMPAIValueT m_amount = 0;

  CDateT m_creation_date = "";	// coin creation date

  CBlockHashT m_block_hash = "";
  CDocIndexT m_doc_index = 0;
  CDocHashT m_doc_hash = "";
  COutputIndexT m_output_index = 0;
  QString m_cycle = "";

};



#endif // COIN_H


