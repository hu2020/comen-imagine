#include "stable.h"

#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "votes_arranger.h"

/**
 * @brief VotesArranger::doGroupByCoinAndSpender
 * @param block_inspect_container
 * @param invokedDocHash
 * @param doDBLog
 *
 * here we re-orginizing votes based on transactions.
 * that is, making 2 dimensional array of coin and transaction-order and put in this member the transaction shole scores
 */
void VotesArranger::doGroupByCoinAndSpender(
  UTXOImportDataContainer* block_inspect_container,
  const QString& invokedDocHash,
  const bool doDBLog)
{
  QHash<CCoinCodeT, QHash<CAddressT, CoinVoterInfo> > coinsAndVotersDict = block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndVotersDict;

  QHash<CCoinCodeT, QHash<uint32_t, QHash<CDocHashT, CoinOrderedSpender> > > coinsAndOrderedSpendersDict {};
  QStringList coins = coinsAndVotersDict.keys();
  for (CCoinCodeT aCoin: coins)
  {
    if (!coinsAndOrderedSpendersDict.keys().contains(aCoin))
      coinsAndOrderedSpendersDict[aCoin] = QHash<uint32_t, QHash<CDocHashT, CoinOrderedSpender> > {};

    for (CAddressT voter: coinsAndVotersDict[aCoin].keys())
    {
      CoinVoterInfo aCoinVoter = coinsAndVotersDict[aCoin][voter];

      QStringList orderedKeys = aCoinVoter.m_rOrders.keys();
      orderedKeys.sort();

      QStringList consideredDocs {};
      for (int64_t ordIndex = 0; ordIndex < orderedKeys.size(); ordIndex++)
      {
        CDocHashT docHash = aCoinVoter.m_rOrders[orderedKeys[ordIndex]];

        if (consideredDocs.contains(docHash))
          continue;

        consideredDocs.append(docHash);

        CoinVoterDocInfo theDoc = aCoinVoter.m_docsInfo[docHash];

        if (!coinsAndOrderedSpendersDict[aCoin].contains(ordIndex))
          coinsAndOrderedSpendersDict[aCoin][ordIndex] = QHash<CDocHashT, CoinOrderedSpender> {};

        if (!coinsAndOrderedSpendersDict[aCoin][ordIndex].contains(docHash))
        {
          coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash] = CoinOrderedSpender {
            VoteData {
                docHash,
                0,
                0,
                0,
                0,
                0,
                {}
            },
            {}};
        }
        // let votePower = utils.calcLog(theDoc.gapTime, (iConsts.getCycleBySeconds() * 2)).gain;
        // if (votePower == 0)
        //     votePower = utils.iFloorFloat(iConsts.MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER);
        float votePower = 1.0;  // TODO improve votePower
        float voteGain = CUtils::iFloorFloat(theDoc.m_voterPercentage * votePower);

        coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].m_voteData.m_voteGain += voteGain;

        if (aCoinVoter.m_spendsLessThan6H)
        { //spendsLessThan6HNew
          coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].m_voteData.m_inside6VotersCount += 1;
          coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].m_voteData.m_inside6VotesGain += voteGain;
        } else {
          coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].m_voteData.m_outside6VotersCount += 1;
          coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].m_voteData.m_outside6VotesGain += voteGain;
        }

        coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].m_voteData.m_details[voter] = theDoc;
        coinsAndOrderedSpendersDict[aCoin][ordIndex][docHash].m_docs.push_back(theDoc);

      }
    }
  }

  CLog::log("coins And Spenders Dict for doc(" + CUtils::hash8c(invokedDocHash) + ") coins And Ordered Spenders Dict: " + UTXOImportDataContainer::dumpMe(coinsAndOrderedSpendersDict), "trx", "trace");
//  if (doDBLog)
//    this._super.logSus({
//        lkey: '3.coinsAndOrderedSpendersDict',
//        blockHash: '-',
//        docHash: invokedDocHash,
//        coins,
//        logBody: utils.stringify(coinsAndOrderedSpendersDict)
//    });

  block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndOrderedSpendersDict = coinsAndOrderedSpendersDict;
  return;
}












/**
 *
coinsAndOrderedSpendersDict sample: {
    "e45630b91c0df1323e8a861464e9fdb8deb705c523cb977bca09d58c5409ef5c:0": [

        first position
        {
            "ba35317697b2836c890faf5dc8b2046d46b9d637ebcd3c37144babb501b117dd": {
                "voteData": {
                    "docHash": "ba35317697b2836c890faf5dc8b2046d46b9d637ebcd3c37144babb501b117dd",
                    "inside6VotersCount": 0,
                    "inside6VotesGain": 0,
                    "outside6VotersCount": 2,
                    "outside6VotesGain": 99.99999999999,
                    "voteGain": 99.99999999999,
                    "details": {
                        "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl": {
                            "creationDate": "2019-04-01 22:22:36",
                            "rOrder": 0,
                            "shares": 99.98680174217,
                            "gapTime": 1723
                        },
                        "im1xpsnyd3k8q6kvvrzxq6xyctyxp3xxc3cxdnryep5xg6rgvehvgcx2fu6vs2": {
                            "creationDate": "2019-04-01 22:22:36",
                            "rOrder": 0,
                            "shares": 0.01319825782,
                            "gapTime": 1730
                        }
                    }
                },
                "docs": [
                    {
                        "creationDate": "2019-04-01 22:22:36",
                        "rOrder": 0,
                        "shares": 99.98680174217,
                        "gapTime": 1723
                    },
                    {
                        "creationDate": "2019-04-01 22:22:36",
                        "rOrder": 0,
                        "shares": 0.01319825782,
                        "gapTime": 1730
                    }
                ]
            }
        },

        second position
        {
            "8830e21bfa191b477847587adcc2d4ba1ed1c9339205b6afe0b693c38f611acb": {
                "voteData": {
                    "docHash": "8830e21bfa191b477847587adcc2d4ba1ed1c9339205b6afe0b693c38f611acb",
                    "inside6VotersCount": 0,
                    "inside6VotesGain": 0,
                    "outside6VotersCount": 2,
                    "outside6VotesGain": 99.99999999999,
                    "voteGain": 99.99999999999,
                    "details": {
                        "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl": {
                            "creationDate": "2019-04-01 22:50:58",
                            "rOrder": 1,
                            "shares": 99.98680174217,
                            "gapTime": 21
                        },
                        "im1xpsnyd3k8q6kvvrzxq6xyctyxp3xxc3cxdnryep5xg6rgvehvgcx2fu6vs2": {
                            "creationDate": "2019-04-01 22:50:58",
                            "rOrder": 1,
                            "shares": 0.01319825782,
                            "gapTime": 28
                        }
                    }
                },
                "docs": [
                    {
                        "creationDate": "2019-04-01 22:50:58",
                        "rOrder": 1,
                        "shares": 99.98680174217,
                        "gapTime": 21
                    },
                    {
                        "creationDate": "2019-04-01 22:50:58",
                        "rOrder": 1,
                        "shares": 0.01319825782,
                        "gapTime": 28
                    }
                ]
            }
        }
    ]
}
 *
 */
