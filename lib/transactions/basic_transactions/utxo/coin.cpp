#include "stable.h"
#include "coin.h"

CCoin::CCoin(
  const CCoinCodeT& coin,
  const CAddressT& owner,
  const CMPAIValueT& value,

  const CDateT& creation_date,

  const CBlockHashT& block_hash,
  const CDocIndexT& doc_index,
  const CDocHashT& doc_hash,
  const COutputIndexT& output_index,
  const QString& cycle)
{
  m_coin = coin;
  m_owner = owner;
  m_amount = value;

  m_creation_date = creation_date;

  m_block_hash = block_hash;
  m_doc_index = doc_index;
  m_doc_hash = doc_hash;
  m_output_index = output_index;
  m_cycle = cycle;
}
