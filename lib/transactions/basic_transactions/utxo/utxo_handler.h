#ifndef UTXOHANDLER_H
#define UTXOHANDLER_H

class Block;


class UTXOHandler
{
public:
  UTXOHandler();

  const static QString stbl_trx_utxos;
  const static QStringList stbl_trx_utxos_fields;

  static void loopCoinCleaner(CDateT cDate = "");
  static void doCoinClean(CDateT cDate = "");
  static bool refreshVisibility(CDateT cDate = "");

  static ClausesT prepareUTXOQuery(
    const QStringList& coins = {},
    const QStringList& visible_by = {});

  static QVDRecordsT searchInSpendableCoins(
    const ClausesT& clauses,
    const QStringList& fields,
    const OrderT& order = {},
    const uint64_t limit = 0);

  static QVDRecordsT searchInSpendableCoinsCache(const QStringList& coins);

  static void inheritAncestorsVisbility(
    const QStringList& blockHashes,
    const QString& creation_date,
    const QString& newBlockHash);

  static bool addNewUTXO(
    const QString& creation_date,
    const QString& the_coin,
    const QString visibleBy,
    const QString& address,
    const CMPAISValueT& coin_value,
    const QString& ref_creation_date);

  static bool removeCoin(const CCoinCodeT&);
  static bool removeUsedCoinsByBlock(const Block*);
  static bool removeVisibleOutputsByBlocks(const QStringList& blockHashes, const bool do_control = true);

  static std::tuple<CMPAIValueT, QVDRecordsT, QV2DicT> getSpendablesInfo();

  static QVDRecordsT extractUTXOsBYAddresses(const QStringList& addresses);

  static QVDRecordsT generateCoinsVisibilityReport();

  static void assignCacheCoinsVisibility();

  static void removeCoinFromCachedSpendableCoins(
    const CBlockHashT& the_block = "",
    const CCoinCodeT& the_coin = "");

};

#endif // UTXOHANDLER_H
