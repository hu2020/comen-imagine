#ifndef SPENTCOINSHANDLER_H
#define SPENTCOINSHANDLER_H

class SpendCoinInfo
{
public:
  QString m_spend_date = "";
  QString m_spend_block = "";
  QString m_spend_document = "";
  uint32_t m_spend_order = 9999;
};

class SpendCoinsList
{
  public:
  SpendCoinsList(){
    m_coins_dict = {};
  };
  ~SpendCoinsList()
  {
    for (QString a_key: m_coins_dict.keys())
    {
      for (SpendCoinInfo* sp: m_coins_dict[a_key])
        delete sp;
    }
  };

  QHash<QString, std::vector<SpendCoinInfo*> > m_coins_dict {};
};

class SpentCoinsHandler
{
public:
  SpentCoinsHandler();

  static const QString stbl_trx_spend;
  static const QStringList stbl_trx_spend_filed;

  static QVDRecordsT searchInSpentCoins(
    const ClausesT& clauses,
    const QStringList& fields = stbl_trx_spend_filed,
    const OrderT order = {},
    const uint64_t limit = 0);

  static SpendCoinsList* makeSpentCoinsDict(const QStringList& coins);

  static std::tuple<bool, GRecordsT> findCoinsSpendLocations(const QStringList& coins);

  static bool markAsSpentAllBlockInputs(
    const Block* block,
    const QString& cDate = CUtils::getNow());

  static bool markSpentAnInput(
    const QString& the_coin,
    const QString& spend_loc,
    const QString& spend_date,
    const QString& cDate = CUtils::getNow());

  static QJsonObject convertSpendsToJsonObject(const SpendCoinsList*);
};

#endif // SPENTCOINSHANDLER_H
