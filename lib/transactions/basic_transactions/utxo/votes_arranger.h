#ifndef VOTESARRANGER_H
#define VOTESARRANGER_H

class UTXOImportDataContainer;
class ValidityCheck;
class RawVote;
class CoinOrderedSpender;
class CoinVoterInfo;
class CoinAndPosition;
class SusVote;

class VotesArranger
{
public:
  VotesArranger();

  static void doGroupByCoinAndVoter(
    UTXOImportDataContainer* block_inspect_container,
    const CDocHashT& invokedDocHash,
    const bool doDBLog);

  static void doGroupByCoinAndSpender(
    UTXOImportDataContainer* block_inspect_container,
    const QString& invokedDocHash,
    const bool doDBLog = true);

  static void doGroupByCoinAndPosition(
    UTXOImportDataContainer* block_inspect_container,
    const CDocHashT& invokedDocHash,
    bool doDBLog = false);

  static void doSusVoteRes(
    UTXOImportDataContainer* block_inspect_container,
    const QString& invokedDocHash,
    const bool doDBLog = false);

};

#endif // VOTESARRANGER_H
