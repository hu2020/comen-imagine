#include "stable.h"

#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "votes_arranger.h"

VotesArranger::VotesArranger()
{

}

void VotesArranger::doGroupByCoinAndVoter(
  UTXOImportDataContainer* block_inspect_container,
  const CDocHashT& invokedDocHash,
  const bool doDBLog)
{
  QHash<CCoinCodeT, QHash<CAddressT, CoinVoterInfo> > coinsAndVotersDict = {};

  // preparing coinsAndVotersDict
  QStringList coinSpenderDocs {}; // will be used to recognize colned transactions(if exist)
  for (RawVote aRawVote: block_inspect_container->m_raw_votes)
  {

    CCoinCodeT aCoin = aRawVote.m_voting_coin;

    // integrate coin spends
    coinSpenderDocs.append(aRawVote.m_spender_doc);

    // group by coins
    if (!coinsAndVotersDict.keys().contains(aCoin))
      coinsAndVotersDict[aCoin] = QHash<CAddressT, CoinVoterInfo> {};

    // group by voters
    CAddressT voter = aRawVote.m_voter;
    if (!coinsAndVotersDict[aCoin].keys().contains(voter))
        coinsAndVotersDict[aCoin][voter] = CoinVoterInfo {
          {}, // spend Orders Info By Spender Block's Stated Creation Date (spendOIBSBSCD)
          {}, //
          {}, // reeive order
        }; // will be used to recognize if the 2 usage of coins have less than 6 hours different or not


    QString spend_key = aRawVote.m_spend_date + "," + aRawVote.m_spender_doc;

    coinsAndVotersDict[aCoin][voter].m_spendOIBSBSCD[spend_key] = SBSCDS {
      aRawVote.m_spend_date,
      aRawVote.m_spender_doc}; // Spender Block's Stated Creation Date Structure

    /**
     * the time between voting time and now, more old more trusty
     * older than 12 hours defenitely approved vote
     */
    coinsAndVotersDict[aCoin][voter].m_docsInfo[aRawVote.m_spender_doc] = CoinVoterDocInfo {
      aRawVote.m_spend_date,
      aRawVote.m_receive_order, // order of receiving spends for same coin
      aRawVote.m_voterPercentage,
      aRawVote.m_vote_date};

    QString gapInxSeg1 = QString::number(aRawVote.m_receive_order).rightJustified(5, '0');

    /**
     * it is totaly possible when the node creates votes(because of spending same coin in different times),
     * hasn't the total spends in history, so creates a partially ordered votes.
     * and did not send the vote about early usages of coin and only votes for 2 recently usages.
     * say one years a go the coin C was spent in Trx1, and 11 month ago cheater again tried to spend it in Trx2
     * and now again want to spend in Trx3.
     * since machine has not Trx1 in history will say Trx2 is valid while it is not.
     * to cover this issue here we add a second index (vote date).
     * since the vote table NEVERE truncated, we have entire spends(if they are case of doublespend)
     * and related doublespends attempttion's history
     */
    QString gapInxSeg2 = aRawVote.m_vote_date;
    QString gapInx = QStringList {gapInxSeg1, gapInxSeg2}.join(",");
    coinsAndVotersDict[aCoin][voter].m_rOrders[gapInx] = aRawVote.m_spender_doc;
  }
  QString coins = coinsAndVotersDict.keys().join("");
  CLog::log("coinsAndVotersDict for doc(" + CUtils::hash8c(invokedDocHash) + "): ${utils.stringify(coinsAndVotersDict)}", "trx", "info");
//  if (doDBLog)
//    this._super.logSus({
//        lkey: '2.coinsAndVotersDict',
//        blockHash: '-',
//        docHash: invokedDocHash,
//        coins,
//        logBody: utils.stringify(coinsAndVotersDict)
//    });

  coinSpenderDocs = CUtils::arrayUnique(coinSpenderDocs);
  if ((coinSpenderDocs.size() == 1) && (coinSpenderDocs[0] == invokedDocHash))
  {
    /**
     * all voters for all coins are agree, that all coins are used in same document with same hash
     */
    // so it is a cloned transaction
    block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndVotersDict = coinsAndVotersDict;
    block_inspect_container->m_transactions_validity_check[invokedDocHash].m_cloned = "cloned";
    block_inspect_container->m_transactions_validity_check[invokedDocHash].m_valid = true;


//    if (doDBLog)
//        this._super.logSus({
//            lkey: '3.cloned response',
//            blockHash: '-',
//            docHash: invokedDocHash,
//            coins,
//            logBody: utils.stringify(response)
//        });
    return;

  }

  // control if voter belives the coins are spended in less than 6 hours
  TimeBySecT fullCycle = CMachine::getCycleByMinutes() * 60;
  TimeBySecT halfCycle = fullCycle / 2;

  for (CCoinCodeT aCoin: coinsAndVotersDict.keys())
  {
    for (CAddressT aVoter: coinsAndVotersDict[aCoin].keys())
    {
      QStringList spend_keys = coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD.keys();
      spend_keys.sort();

      // controll if spend_keys are for one uniq doc or diffferent docs
      QHash<CDocHashT, uint64_t> keyGroup {};
      for (QString an_spend_key: spend_keys)
      {
        CDocHashT spender_doc = coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD[an_spend_key].m_spdDoc;
        if (!keyGroup.keys().contains(spender_doc))
          keyGroup[spender_doc] = 0;
        keyGroup[spender_doc]++;
      }

//      coinsAndVotersDict[aCoin][aVoter].m_spendTimes = CUtils::arrayUnique(coinsAndVotersDict[aCoin][aVoter].m_spendTimes);
//      coinsAndVotersDict[aCoin][aVoter].m_spendTimes.sort();

//      QStringList spendTimes = coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD.keys();
//      spendTimes = CUtils::arrayUnique(spendTimes);
//      spendTimes.sort();

      coinsAndVotersDict[aCoin][aVoter].m_spendsLessThan6HNew = false;
      coinsAndVotersDict[aCoin][aVoter].m_spendsLessThan6H = false;
      if (keyGroup.keys().size() == 1)
      {
        // there is only on spender document, so it is not the case of double-spending
      } else if (keyGroup.keys().size() > 1) {
        bool canControlLess6Condition = true;
        for (CDocHashT aKey: keyGroup.keys())
        {
            if (keyGroup[aKey] > 1)
            {
              /**
               * it means there is at least one group of docs(maybe cloned) in combine with the another group(s)
               * the conflicts can be simple double-spend doc or a clone of double-spend
               */
              canControlLess6Condition = false;
            }
        }
        /**
         * it is possible existing 2 different groups of documents
         * 1. a document group with magiority of occurrence which are cloned docs
         * 2. and another group of doc which are cheating double spend docs
         * defeniately both groups are issued by one entity who can sign the coin(s)
         *
         * cloned       cloned      double
         * cloned       cloned      cloned
         *    |         double      double
         *    |            |           |
         *    v            |           |
         * double          v           v
         * cloned       cloned      cloned
         *
         */
        coinsAndVotersDict[aCoin][aVoter].m_canControlLess6Condition = canControlLess6Condition;
        coinsAndVotersDict[aCoin][aVoter].m_spendsLessThan6HNew = false;
        if (canControlLess6Condition)
        {

          SBSCDS first_spend_by_signer_claim = coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD[spend_keys[0]];
          SBSCDS second_spend_by_signer_claim = coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD[spend_keys[1]];
          QStringList receive_order_by_machine_POV = coinsAndVotersDict[aCoin][aVoter].m_rOrders.keys();
          receive_order_by_machine_POV.sort();
          CDocHashT second_spend_by_machine_POV = coinsAndVotersDict[aCoin][aVoter].m_rOrders[receive_order_by_machine_POV[1]]; // it is second doc by machine's POV
          if (CUtils::timeDiff(
              first_spend_by_signer_claim.m_spdDate,
              coinsAndVotersDict[aCoin][aVoter].m_docsInfo[second_spend_by_machine_POV].m_voteDate
          ).asSeconds < fullCycle
          ) {
            /**
             * if voter received a Trx before of 12 hours after stated spend time,
             * she can vote on it.
             * after 12 hours of spending a coin any claim is inacceptable
             */

            coinsAndVotersDict[aCoin][aVoter].m_spendsLessThan6H =
              (CUtils::timeDiff(first_spend_by_signer_claim.m_spdDate, second_spend_by_signer_claim.m_spdDate).asSeconds < halfCycle);
          }


          for (int64_t inx = 1; inx < spend_keys.size(); inx++)
          {
            if (coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD[spend_keys[inx - 1]].m_spdDoc !=
                coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD[spend_keys[inx]].m_spdDoc) {
                /**
                 * they are not 2 cloned doc in sequence!
                 * so must controll time diff
                 */
                if ((CUtils::timeDiff(
                    coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD[spend_keys[inx - 1]].m_spdDate,
                    coinsAndVotersDict[aCoin][aVoter].m_spendOIBSBSCD[spend_keys[inx]].m_spdDate
                ).asSeconds < halfCycle)) {
                    /**
                     * two different docs are created in less than half a cylce time diff
                     * so
                     */
                    coinsAndVotersDict[aCoin][aVoter].m_spendsLessThan6HNew = true;
                }
            }
          }
        }
      } else {
        //
      }

    }
  }

  block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndVotersDict = coinsAndVotersDict;

  return;
}
























/**
 *
sample coinsAndVotersDict:

{
    "ac36d47b7244bd6a6928686ad37b87c24d2f10329ea7eb430615cafc23976a55:0": {
        "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl": {
            "spendOIBSBSCD": {
                "2019-04-06 18:36:33:305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61": {
                    "spdDate": "2019-04-06 18:36:33",
                    "spdDoc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61"
                },
                "2019-04-06 19:16:58:0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1": {
                    "spdDate": "2019-04-06 19:16:58",
                    "spdDoc": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1"
                }
            },
            "docsInfo": {
                "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61": {
                    "coinSpendDate": "2019-04-06 18:36:33",
                    "rOrder": 0,
                    "voterPercentage": 99.9700089973
                },
                "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1": {
                    "coinSpendDate": "2019-04-06 19:16:58",
                    "rOrder": 1,
                    "voterPercentage": 99.9700089973
                }
            },
            "rOrders": {
                "0000:2019-04-06 19:17:15": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
                "0001:2019-04-06 19:17:15": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1"
            },
            "spendTimes": [
                "2019-04-06 18:36:33",
                "2019-04-06 19:16:58"
            ],
            "firstSpenderInfo": {
                "spDoc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
                "spTime": "2019-04-06 18:36:33"
            },
            "spendsLessThan6HNew": false,
            "spendsLessThan6H": false
        },

        "im1xqukxefe8p3xxwpk89jx2wfjvdjkycfhvscrsde4xymnxvrrxy6xxjljrvp": {
            "spendOIBSBSCD": {
                "2019-04-06 18:36:33:305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61": {
                    "spdDate": "2019-04-06 18:36:33",
                    "spdDoc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61"
                },
                "2019-04-06 19:16:58:0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1": {
                    "spdDate": "2019-04-06 19:16:58",
                    "spdDoc": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1"
                }
            },
            "docsInfo": {
                "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61": {
                    "coinSpendDate": "2019-04-06 18:36:33",
                    "rOrder": 0,
                    "voterPercentage": 0.02999100269
                },
                "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1": {
                    "coinSpendDate": "2019-04-06 19:16:58",
                    "rOrder": 1,
                    "voterPercentage": 0.02999100269
                }
            },
            "rOrders": {
                "0000:2019-04-06 19:17:09": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
                "0001:2019-04-06 19:17:09": "0c8a0f651a80a5e06d44ff5e2c7a71fe4990997e4d2fb23bce68af94446a8fd1"
            },
            "spendTimes": [
                "2019-04-06 18:36:33",
                "2019-04-06 19:16:58"
            ],
            "firstSpenderInfo": {
                "spDoc": "305e32fda515fa1e49f06fb00b1df26703e621c18e8fa60e71e65258486bca61",
                "spTime": "2019-04-06 18:36:33"
            },
            "spendsLessThan6HNew": false,
            "spendsLessThan6H": false
        }
    }
}

*/
