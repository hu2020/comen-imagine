#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "votes_arranger.h"


void VotesArranger::doSusVoteRes(
  UTXOImportDataContainer* block_inspect_container,
  const QString& invokedDocHash,
  const bool doDBLog)
{
  QHash<CCoinCodeT, QHash<uint32_t, CoinAndPosition> > coinsAndPositionsDict = block_inspect_container->m_transactions_validity_check[invokedDocHash].m_coinsAndPositionsDict;

  QHash<CCoinCodeT, SusVote> susVoteRes {};

  // preparing final response for invoked document, based on position 0 (the fist spender)
  // responses for susvote
  QStringList coins = coinsAndPositionsDict.keys();
  for (CCoinCodeT aCoin: coins)
  {
    if (coinsAndPositionsDict[aCoin][0].m_outsideTotal <= coinsAndPositionsDict[aCoin][0].m_insideTotal)
    {
      susVoteRes[aCoin] = SusVote {
        false,
        "donate",
        coinsAndPositionsDict[aCoin][0].m_inside6h[0].m_voters,
        coinsAndPositionsDict[aCoin][0].m_inside6h[0].m_votes};
    }


    if (coinsAndPositionsDict[aCoin][0].m_outside6h.size()== 1)
    {
        if (coinsAndPositionsDict[aCoin][0].m_outside6h[0].m_docHash == invokedDocHash)
        {
            susVoteRes[aCoin] = SusVote {
              true,
              "-",
              coinsAndPositionsDict[aCoin][0].m_outside6h[0].m_voters,
              coinsAndPositionsDict[aCoin][0].m_outside6h[0].m_votes};

        } else {
            susVoteRes[aCoin] = SusVote {
              false,
              "reject",
              coinsAndPositionsDict[aCoin][0].m_outside6h[0].m_voters,
              coinsAndPositionsDict[aCoin][0].m_outside6h[0].m_votes};

        }
    }


    if (coinsAndPositionsDict[aCoin][0].m_outside6h.size() > 1)
    {
      // in firt position, there are more than one docs with more than 6h gap time
      QHash<QString, InOutside6hElm> voteAndHashDict = {};
      for (InOutside6hElm anOutside: coinsAndPositionsDict[aCoin][0].m_outside6h)
      {
        QString theKey = QString::number(anOutside.m_votes).rightJustified(10, '0') + QString::number(anOutside.m_voters).rightJustified(10, '0') + anOutside.m_docHash;
        voteAndHashDict[theKey] = anOutside;
      }
      QStringList votes = voteAndHashDict.keys();
      if (votes.size() > 0)
        std::sort (votes.begin(), votes.begin());
      std::reverse(votes.begin(), votes.end());
      QString maxVote = votes[0];
      InOutside6hElm max_vote_info = voteAndHashDict[maxVote];

      if (max_vote_info.m_docHash == invokedDocHash)
      {
        susVoteRes[aCoin] = SusVote {
          true,
          "",
          max_vote_info.m_voters,
          max_vote_info.m_votes};

      } else {
        susVoteRes[aCoin] = SusVote {
          false,
          "reject",
          max_vote_info.m_voters,
          max_vote_info.m_votes};

      }
    }
  }

//  if (doDBLog)
//    this._super.logSus({
//        lkey: '5.susVoteRes',
//        blockHash: '-',
//        docHash: invokedDocHash,
//        coins,
//        logBody: utils.stringify({ susVoteRes })
//    });

  block_inspect_container->m_transactions_validity_check[invokedDocHash].m_susVoteRes = susVoteRes;

  return;
}
