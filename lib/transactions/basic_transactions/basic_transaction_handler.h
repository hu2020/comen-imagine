#ifndef BASICTRANSACTIONHANDLER_H
#define BASICTRANSACTIONHANDLER_H

class BasicTxDocument;

class BasicTransactionTemplate
{
public:
  QHash<CCoinCodeT, TInput> m_tpl_inputs;
  std::vector<TOutput> m_tpl_outputs;

  CMPAIValueT m_tpl_max_DP_cost;
  CMPAIValueT m_tpl_pre_calculated_dp_cost = 0;

  QString m_tpl_comment = "";
  CDocHashT m_tpl_doc_ref = "";

  int64_t m_tpl_change_back_output_index = 0;
  CAddressT m_tpl_change_back_address = "";

  QStringList m_tpl_backers_addresses = {};

  QString m_tpl_sig_hash = CConsts::SIGHASH::ALL;

  CDateT m_tpl_creation_date = CUtils::getNow();
  QString m_tpl_doc_type = CConsts::DOC_TYPES::BasicTx;
  QString m_tpl_doc_class = CConsts::TRX_CLASSES::SimpleTx;
  QString m_tpl_doc_ver = "0.0.2";
  QJsonArray m_tpl_doc_ext_info {};
  CDocHashT m_tpl_doc_ext_hash = "";

  // for test purpose
  bool m_tpl_do_unequal = false;

  static const CMPAIValueT TEMP_DPCOST_AMOUNT;

  bool setChangabackOutputIndex();
  bool setOutputIndexes();
  QStringList getInputCoinsCodes();
  QJsonArray generateInputTuples();
  QJsonArray generateOutputTuples();
  std::tuple<bool, QString, CMPAIValueT> canAffordCosts(CMPAIValueT cost);
  void appendTmpDPCostOutPut(CAddressT address);
  QVector<COutputIndexT> getDPCostIndexes();
  bool signAndCreateDExtInfo();
  bool setDPcostAmount(CMPAIValueT amount);
  bool setChangebackOutputAmount(CMPAIValueT amount);
  bool removeChangebackOutput();
  std::vector<TOutput*> generateDocOutputTuples();

  QString dumpMe();

};

class BasicTransactionHandler
{
public:
  BasicTransactionHandler();

  static std::tuple<bool, QString, CMPAIValueT, CMPAIValueT> preValidateTransactionParams(BasicTransactionTemplate tpl);

  static std::tuple<bool, QString, BasicTxDocument*, CMPAIValueT> makeATransaction(
    BasicTransactionTemplate tpl);


};

#endif // BASICTRANSACTIONHANDLER_H
