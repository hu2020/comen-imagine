#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/transactions/trx_utils.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/basic_tx_document.h"

#include "basic_transaction_handler.h"

const CMPAIValueT BasicTransactionTemplate::TEMP_DPCOST_AMOUNT = 999999999999;

bool BasicTransactionTemplate::setChangabackOutputIndex()
{
  for (auto an_out: m_tpl_outputs)
    if (an_out.m_output_charachter == CConsts::OUTPUT_CHANGEBACK)
    {
      m_tpl_change_back_output_index = an_out.m_output_index;
      return true;
    }

  return false;
}

bool BasicTransactionTemplate::setOutputIndexes()
{
  // BIP 69 for outputs
  std::vector<TOutput> normalized_outputs {};

  if (m_tpl_outputs.size() < 1)
    return false;

  QHash<QString, TOutput> dict {};
  uint64_t dummy_inx = 0;
  for (TOutput an_output: m_tpl_outputs)
  {
    QString key = QStringList {
      QString::number(an_output.m_amount).rightJustified(24, '0'),
      an_output.m_address,
      QString::number(dummy_inx).rightJustified(5, '0')}.join(",");
    dict[key] = an_output;
    dummy_inx++;
  }

  std::vector<QString> keys {};
  for(QString a_key: dict.keys())
    keys.push_back(a_key);
  std::sort(keys.begin(), keys.end());
  std::reverse(keys.begin(), keys.end());
  COutputIndexT real_output_inx = 0;
  for (QString a_key: keys)
  {
    dict[a_key].m_output_index = real_output_inx;
    normalized_outputs.push_back(dict[a_key]);
    real_output_inx++;
  }
  m_tpl_outputs = normalized_outputs;

  return true;
}

QJsonArray BasicTransactionTemplate::generateInputTuples()
{
  QJsonArray input_tuples {};

  QStringList the_coins_codes = m_tpl_inputs.keys();
  the_coins_codes.sort(); //BIP69  inputs

  for (CCoinCodeT a_coin: the_coins_codes)
  {
    QJsonArray elm {m_tpl_inputs[a_coin].m_transaction_hash, m_tpl_inputs[a_coin].m_output_index};
    input_tuples.push_back(elm);
  }
  return input_tuples;
}

QJsonArray BasicTransactionTemplate::generateOutputTuples()
{
  setOutputIndexes();

  std::vector<COutputIndexT> indexes {};
  for(auto an_out: m_tpl_outputs)
  {
    indexes.push_back(an_out.m_output_index);
  }
  std::sort(indexes.begin(), indexes.end());

  QJsonArray output_tuples {};
  for (COutputIndexT an_index: indexes)
  {
    for(auto an_out: m_tpl_outputs)
    {
      if (an_out.m_output_index == an_index)
      {
        QJsonArray elm {
          an_out.m_address,
          QVariant::fromValue(an_out.m_amount).toDouble()};
        output_tuples.push_back(elm);
      }
    }
  }
  return output_tuples;
}

std::vector<TOutput*> BasicTransactionTemplate::generateDocOutputTuples()
{
  std::vector<TOutput*> final_out {};
  QJsonArray outputs = generateOutputTuples();
  for(auto an_output: outputs)
  {
    TOutput* output = new TOutput {
        an_output.toArray()[0].toString(),
        static_cast<CMPAIValueT>(an_output.toArray()[1].toDouble())};
    final_out.push_back(output);
  }
  return final_out;
}

QStringList BasicTransactionTemplate::getInputCoinsCodes()
{
  QStringList the_coins_codes = m_tpl_inputs.keys();
  the_coins_codes.sort(); //BIP69  inputs
  return the_coins_codes;
}

std::tuple<bool, QString, CMPAIValueT> BasicTransactionTemplate::canAffordCosts(
  CMPAIValueT calculate_dp_cost)
{
  QString msg;
  if (m_tpl_pre_calculated_dp_cost > 0)
  {
    if (calculate_dp_cost > m_tpl_pre_calculated_dp_cost)
    {
      msg = "Transaction calculated cost(" + CUtils::microPAIToPAI6(calculate_dp_cost) + " PAIs) is heigher than your desire(" + CUtils::microPAIToPAI6(m_tpl_pre_calculated_dp_cost) + "PAIs)";
      CLog::log(msg, "trx", "error");
      return {false, msg, 0};

    } else {
      return {true, msg, m_tpl_pre_calculated_dp_cost};
    }

  }


  if ((m_tpl_max_DP_cost > 0) && (calculate_dp_cost > m_tpl_max_DP_cost))
  {
    msg = "Transaction locally recalculate trx dp cost(" + CUtils::microPAIToPAI6(calculate_dp_cost) + " PAIs) is heigher than your max(" + CUtils::microPAIToPAI6(m_tpl_max_DP_cost) + " PAIs)";
    CLog::log(msg, "trx", "error");
    return {false, msg, 0};

  }

  return {true, msg, calculate_dp_cost};
}

void BasicTransactionTemplate::appendTmpDPCostOutPut(CAddressT address)
{
  m_tpl_outputs.push_back(TOutput {address, TEMP_DPCOST_AMOUNT, CConsts::OUTPUT_DPCOST});
}

QVector<COutputIndexT> BasicTransactionTemplate::getDPCostIndexes()
{
  QVector<COutputIndexT> indexes {};
  for (auto an_out: m_tpl_outputs)
    if (an_out.m_output_charachter == CConsts::OUTPUT_DPCOST)
      indexes.push_back(an_out.m_output_index);
  return indexes;
}

bool BasicTransactionTemplate::signAndCreateDExtInfo()
{
  QJsonArray dExtInfo {};
  QJsonArray input_tuples = generateInputTuples();
  QJsonArray output_tuples = generateOutputTuples();
  for (CCoinCodeT a_coin: getInputCoinsCodes())
  {
    QJsonArray signatures {};
    for (QString a_private_key: m_tpl_inputs[a_coin].m_private_keys)
    {
      QString signature = BasicTxDocument::signingInputOutputs(
        a_private_key,
        input_tuples,
        output_tuples,
        m_tpl_sig_hash,
        m_tpl_doc_ref,
        m_tpl_creation_date);
      signatures.push_back(QJsonArray {signature, m_tpl_sig_hash});
    }
    dExtInfo.push_back(
      QJsonObject {
        {"uSet", m_tpl_inputs[a_coin].m_unlock_set},
        {"signatures", signatures}});
  }
  m_tpl_doc_ext_info = dExtInfo;

  return true;
}

bool BasicTransactionTemplate::setDPcostAmount(CMPAIValueT amount)
{
  for (uint64_t inx = 0; inx < m_tpl_outputs.size(); inx++)
    if (m_tpl_outputs[inx].m_output_charachter == CConsts::OUTPUT_DPCOST)
      m_tpl_outputs[inx].m_amount = amount;
  return true;
}

bool BasicTransactionTemplate::removeChangebackOutput()
{
  std::vector<TOutput> new_outputs {};
  for (auto an_out: m_tpl_outputs)
    if (an_out.m_output_charachter != CConsts::OUTPUT_CHANGEBACK)
      new_outputs.push_back(an_out);

  m_tpl_outputs = new_outputs;
  return true;
}

bool BasicTransactionTemplate::setChangebackOutputAmount(CMPAIValueT amount)
{
  bool changeback_existed = false;
  for (uint64_t inx = 0; inx < m_tpl_outputs.size(); inx++)
  {
    if (m_tpl_outputs[inx].m_output_charachter == CConsts::OUTPUT_CHANGEBACK)
    {
      m_tpl_outputs[inx].m_amount = amount;
      changeback_existed = true;
    }
  }
  if (changeback_existed)
    return true;

  // creat a new output
  if (m_tpl_change_back_address == "")
    m_tpl_change_back_address = CMachine::getBackerAddress();

  m_tpl_outputs.push_back(TOutput {m_tpl_change_back_address, amount, CConsts::OUTPUT_CHANGEBACK});

  return true;
}

QString BasicTransactionTemplate::dumpMe()
{
  QString out = "\n";
  for (auto an_input: m_tpl_inputs)
    out += "\n" + an_input.dumpMe();

  out += "\nOutputs: ";
  for (auto an_output: m_tpl_outputs)
    out += "\n" + an_output.m_address + "(" + an_output.m_output_charachter + " / " + QString::number(an_output.m_output_index) + ") \t " + CUtils::microPAIToPAI6(an_output.m_amount);

  out += "\nmax DP cost: " + CUtils::microPAIToPAI6(m_tpl_max_DP_cost);
  out += "\nPre calculated cost: " + CUtils::microPAIToPAI6(m_tpl_pre_calculated_dp_cost);

  out += "\nComment: " + m_tpl_comment;
  out += "\ndoc ref: " + m_tpl_doc_ref;

  out += "\nchange back outputindex: " + QString::number(m_tpl_change_back_output_index);
  out += "\nChange back address: " + m_tpl_change_back_address;

  out += "\nBackers addresses: " + m_tpl_backers_addresses.join(", ");
  out += "\nSig hash: " + m_tpl_sig_hash;
  out += "\nCreation date: " + m_tpl_creation_date;
  out += "\nDoc type: " + m_tpl_doc_type;
  out += "\nDoc class: " + m_tpl_doc_class;
  out += "\nDoc Version: " + m_tpl_doc_ver;
  out += "\nDo Unequal: " + CUtils::dumpIt(m_tpl_do_unequal);
  out += "\n";

  return out;
}





//  -  -  -  Basic transaction handler part
BasicTransactionHandler::BasicTransactionHandler()
{

}

std::tuple<bool, QString, CMPAIValueT, CMPAIValueT> BasicTransactionHandler::preValidateTransactionParams(BasicTransactionTemplate tpl)
{
  QString msg;

  if (tpl.m_tpl_inputs.keys().size() == 0)
  {
    msg = "Try to make transaction without inputs!";
    CLog::log(msg, "trx", "error");
    return {false, msg, 0, 0};
  }

  CMPAIValueT total_inputs_amount = 0;
  for (CCoinCodeT a_coin: tpl.m_tpl_inputs.keys())
    total_inputs_amount += tpl.m_tpl_inputs[a_coin].m_amount;

  if (total_inputs_amount == 0)
  {
    msg = "Try to make transaction with zero inputs!";
    CLog::log(msg, "trx", "error");
    return {false, msg, 0, 0};
  }


  if (tpl.m_tpl_outputs.size() == 0)
  {
    msg = "Try to make transaction without output accounts!";
    CLog::log(msg, "trx", "error");
    return {false, msg, 0, 0};
  }

  CMPAIValueT total_outputs_amount_except_dp_costs = 0;
  for(auto an_out: tpl.m_tpl_outputs)
    if (!QStringList{CConsts::OUTPUT_CHANGEBACK, CConsts::OUTPUT_DPCOST}.contains(an_out.m_output_charachter))
      total_outputs_amount_except_dp_costs += static_cast<CMPAIValueT>(an_out.m_amount);
  if (total_outputs_amount_except_dp_costs == 0)
  {
    msg = "Try to make transaction without outputs!";
    CLog::log(msg, "trx", "error");
    return {false, msg, 0, 0};
  }

  return {true, "", total_inputs_amount, total_outputs_amount_except_dp_costs};
}

/**
 * @brief BasicTransactionHandler::makeATransaction
 * @param tpl
 * @return {status, err_msg, transaction, transaction_dp_cost}
 */
std::tuple<bool, QString, BasicTxDocument*, CMPAIValueT> BasicTransactionHandler::makeATransaction(
  BasicTransactionTemplate tpl)
{
  QString msg;
  BasicTxDocument* transaction;

  // supportedP4PTrxLength = _.has(args, 'supportedP4PTrxLength') ? (args.supportedP4PTrxLength) : 0;

  if ( (tpl.m_tpl_pre_calculated_dp_cost > 0) && (tpl.m_tpl_max_DP_cost == 0))
    tpl.m_tpl_max_DP_cost = tpl.m_tpl_pre_calculated_dp_cost; // user has some idea about the cost and want to pay exactly the desired price (pre_calculated_DP_cost)

  if (tpl.m_tpl_backers_addresses.size() == 0)
    tpl.m_tpl_backers_addresses.push_back(CMachine::getBackerAddress());  // in case of clone, a transaction can have more than one backer

  // adding DP Costs payment outputs
  for(CAddressT a_backer: tpl.m_tpl_backers_addresses)
    tpl.appendTmpDPCostOutPut(a_backer);

  CLog::log("Make a transaction, input params:" + tpl.dumpMe(), "trx", "trace");

  auto[pre_validate_status, pre_validate_res_msg, total_inputs_amount, total_outputs_amount_except_dp_costs] = preValidateTransactionParams(tpl);
  if(!pre_validate_status)
    return {false, pre_validate_res_msg, nullptr, 0};



  // pre-signing in order to estimate actual outcomed transaction length
  tpl.signAndCreateDExtInfo();
  QJsonObject trx_json {
    {"dHash", "0000000000000000000000000000000000000000000000000000000000000000"},
    {"dLen", "0000000"},
    {"dType", tpl.m_tpl_doc_type},
    {"dClass", tpl.m_tpl_doc_class},
    {"dVer", tpl.m_tpl_doc_ver},
    {"dRef", tpl.m_tpl_doc_ref},
    {"dPIs", QJsonArray {0}},  // data & process cost, which are payed by one output(or more output for cloned transactions)
    {"dComment", tpl.m_tpl_comment},
    {"dCDate", tpl.m_tpl_creation_date},
    {"inputs", tpl.generateInputTuples()},
    {"outputs", tpl.generateOutputTuples()},
    {"dExtInfo", tpl.m_tpl_doc_ext_info},
    {"dExtHash", "0000000000000000000000000000000000000000000000000000000000000000"},
  };
  CLog::log("trx_json in pre-transaction: " + CUtils::serializeJson(trx_json), "trx", "trace");
  transaction = new BasicTxDocument(trx_json);

  DocLenT offset_extra_chars = 100; // just an offset length to cover potentially extra chars of percise DPCost
  CLog::log("offset_extra_chars: " + CUtils::sepNum(offset_extra_chars), "trx", "trace");

  transaction->setDocLength();

  CLog::log("pre calculation transaction 1: " + transaction->safeStringifyDoc(true), "trx", "trace");

  auto[status, locally_recalculate_trx_dp_cost] = transaction->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    CUtils::getNow(),
    offset_extra_chars);
  CLog::log("The transaction  cost is (" + CUtils::microPAIToPAI6(locally_recalculate_trx_dp_cost) + ")", "trx", "info");
  if (!status)
  {
    msg = "Fail in calculate transacton cost!";
    CLog::log(msg, "trx", "error");
    return {false, msg, nullptr, 0};
  }
  CLog::log("pre calculation transaction 2: " + transaction->safeStringifyDoc(true), "trx", "trace");

  auto[afford_status, afford_msg, the_DP_cost] = tpl.canAffordCosts(locally_recalculate_trx_dp_cost);
  if (!afford_status)
    return {false, afford_msg, nullptr, 0};
  tpl.setDPcostAmount(the_DP_cost);

  CLog::log("sum total_inputs_amount: " + CUtils::microPAIToPAI6(total_inputs_amount) + " PAI", "trx", "info");
  CLog::log("sum outputs Value(except dp costs): " + CUtils::microPAIToPAI6(total_outputs_amount_except_dp_costs) + " PAI", "trx", "info");
  CLog::log("sum DPCost(" + QString::number(tpl.m_tpl_backers_addresses.size()) + " Backer): " + CUtils::microPAIToPAI6(the_DP_cost * tpl.m_tpl_backers_addresses.size()) + " PAI", "trx", "info");

  CMPAISValueT change_back_amount = total_inputs_amount - total_outputs_amount_except_dp_costs - (the_DP_cost * tpl.m_tpl_backers_addresses.size()); // TODO: add possibilitiy for user to pay diffrent DPCost to different backers(in case of cloning trx)
  CLog::log("change back amount (" + CUtils::microPAIToPAI6(change_back_amount) + " PAI) because of DPCosts", "trx", "info");
//  CMPAISValueT finalChange = transaction->m_outputs[change_back_output_index]->m_amount + change_back_amount;

  CLog::log("Dump tpl befor setChangebackOutputAmount" + tpl.dumpMe() + "\n", "trx", "trace");

  if (change_back_amount > 0)
  {
    tpl.setChangebackOutputAmount(change_back_amount);
  } else {
    // there is no change back, so remove the change back output
    tpl.removeChangebackOutput();
  }

  // test for unequality, by adding 1 unit to one out put and making a dummy unequality
  //if (tpl.m_tpl_do_unequal)
  //  transaction->m_outputs[tpl.m_tpl_change_back_output_index]->m_amount++;

  CLog::log("Dump tpl after setChangebackOutputAmount and befor final assignment to document outputs" + tpl.dumpMe() + "\n", "trx", "trace");
  transaction->m_outputs = tpl.generateDocOutputTuples();

  transaction->m_data_and_process_payment_indexes = tpl.getDPCostIndexes();
  if (transaction->m_data_and_process_payment_indexes.size() == 0)
  {
    msg = "Failed in find output by (address_plus_value)";
    CLog::log(msg, "trx", "error");
    return {false, msg, nullptr, 0};
  }
  std::sort(transaction->m_data_and_process_payment_indexes.begin(), transaction->m_data_and_process_payment_indexes.end());
  auto last = std::unique(transaction->m_data_and_process_payment_indexes.begin(), transaction->m_data_and_process_payment_indexes.end());
  transaction->m_data_and_process_payment_indexes.erase(last, transaction->m_data_and_process_payment_indexes.end());



  // re-signing, because of output re-ordering
  tpl.signAndCreateDExtInfo();
  transaction->m_doc_ext_info = tpl.m_tpl_doc_ext_info;
  transaction->setDExtHash();
  transaction->setDocLength();
  transaction->setDocHash();
  Block* tmp_block = new Block(QJsonObject {
    {"bCDate", CUtils::getNow()},
    {"bType", "future_block"},
    {"bHash", "future_hash"}});
  GenRes final_validate = transaction->fullValidate(tmp_block);
  delete tmp_block;
  if (!final_validate.status)
  {
    msg = "Failed in transaction full Validate, " + final_validate.msg;
    CLog::log(msg + transaction->safeStringifyDoc(true), "trx", "error");
    return {false, msg, nullptr, 0};
  }

  // double-check outputs
  for (TOutput* an_out: transaction->getOutputs())
  {
    if (an_out->m_amount <= 0)
    {
      msg = "at least one negative/zero output exist";
      CLog::log(msg, "trx", "error");
      return {false, msg, nullptr, 0};
    }
  }

  // signatures control
  QV2DicT used_coins_dict {};
  for (auto an_inp: tpl.m_tpl_inputs)
    used_coins_dict[an_inp.getCoinCode()] = QVDicT {
    {"ut_o_address", an_inp.m_owner},
    {"ut_o_value", QVariant::fromValue(an_inp.m_amount)}};
  bool signature_validate_res = transaction->validateSignatures(
    used_coins_dict,
    {},
    "11111111111111111111111111111111");
  if (!signature_validate_res)
  {
    msg = "Failed in transaction validate signature";
    CLog::log(msg + transaction->safeStringifyDoc(true), "trx", "error");
    return {false, msg, nullptr, 0};
  }

  // control transaction hash
  bool general_transaction_validate_res = transaction->validateGeneralRulsForTransaction();
  if (!general_transaction_validate_res)
  {
    msg = "Failed in transaction general validate rules";
    CLog::log(msg + transaction->safeStringifyDoc(true), "trx", "error");
    return {false, msg, nullptr, 0};
  }

  // equation control
  auto[equation_check_res, equation_msg, total_inputs_amounts, total_outputs_amounts] = transaction->equationCheck(
    used_coins_dict,
    {},
    "2222222222222222222222222222222222");
  if(!equation_check_res)
  {
    msg = "Failed in transaction equation contol";
    CLog::log(msg + transaction->safeStringifyDoc(true), "trx", "error");
    return {false, msg, nullptr, 0};
  }

  CLog::log("pre-Successfully created transaction: " + transaction->safeStringifyDoc(true), "trx", "trace");

  auto[calc_status2, trx_dp_cost2] = transaction->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    CUtils::getNow(),
    0);

  if (the_DP_cost < trx_dp_cost2)
  {
    if (transaction->getDPIs().size() > 1)
      CUtils::exiter("Multiple DPCost payment(cloned trx) is not supported yet", 101);

    CLog::log("Failed in DPCost calc: trx_dp_cost2(" + CUtils::microPAIToPAI6(trx_dp_cost2)+ ")" + transaction->safeStringifyDoc(true), "trx", "error");
    CUtils::exiter("Failed in transaction cost calculation!!!!!", 101);
    //transaction->m_outputs[transaction->getDPIs()[0]]->m_amount = trx_dp_cost2;

  }

  return {true, "", transaction, the_DP_cost};
}
