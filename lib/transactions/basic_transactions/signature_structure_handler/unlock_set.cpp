#include "stable.h"
#include "unlock_set.h"

UnlockSet::UnlockSet(
    QVector<IndividualSignature> signature_sets,
    QString salt,
    QString signature_type,
    QString signature_ver,
    QString left_hash,
    QStringList mProof)
{
  m_signature_sets = signature_sets;
  m_salt = salt;
  m_signature_ver = signature_ver;
  m_signature_type = signature_type;
  m_left_hash = left_hash;
  m_merkle_proof = mProof;
}

QJsonObject UnlockSet::exportToJson()
{
  QJsonArray signature_sets{};
  for (IndividualSignature aSig: m_signature_sets)
  {
    signature_sets.push_back(aSig.exportJson());
  }
  return QJsonObject {
    {"sType", m_signature_type},
    {"sVer", m_signature_ver},
    {"mProof", CUtils::convertQStringListToJSonArray(m_merkle_proof)},
    {"sSets", signature_sets},
    {"lHash", m_left_hash},
    {"salt", m_salt}
  };
}

void UnlockSet::importJson(const QJsonObject& obj)
{
  m_signature_type = obj.value("sType").toString();
  m_signature_ver = obj.value("sVer").toString();
  m_merkle_proof = CUtils::convertJSonArrayToQStringList(obj.value("mProof").toArray());
  m_left_hash = obj.value("lHash").toString();
  m_salt = obj.value("salt").toString();
  m_signature_sets = {};
  for (auto a_signature_set: obj.value("sSets").toArray())
  {
    IndividualSignature sig_obj;
    sig_obj.importJson(a_signature_set.toObject());
    m_signature_sets.push_back(sig_obj);
  }

}
