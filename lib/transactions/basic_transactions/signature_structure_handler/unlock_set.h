#ifndef UNLOCKSET_H
#define UNLOCKSET_H

#include "QVector"
#include "QString"
#include "QStringList"

#include "constants.h"

#include "individual_signature.h"

class UnlockSet
{
public:
  QString m_signature_type;
  QString m_signature_ver;
  QVector<IndividualSignature> m_signature_sets;
  QStringList m_merkle_proof;
  QString m_left_hash = "";
  QString m_salt ="";

  UnlockSet(
    QVector<IndividualSignature> signature_sets = {},
    QString salt = "",
    QString signature_type = CConsts::SIGNATURE_TYPES::Basic,
    QString signature_ver = "0.0.0",
    QString left_hash = "",
    QStringList mProof = {});

  QJsonObject exportToJson();
  void importJson(const QJsonObject &obj);

};


#endif // UNLOCKSET_H
