#ifndef INDIVIDUALSIGNATURE_H
#define INDIVIDUALSIGNATURE_H

#include <QString>
#include <QJsonObject>

#include "stable.h"

class IndividualSignature
{
public:
  QString m_signer_id = ""; // a dummy handler id
  QString m_signature_key = ""; // sKey
  QString m_permitted_to_pledge = ""; // pPledge
  QString m_permitted_to_delegate = ""; // pDelegate

  TimeByHoursT m_input_time_lock = 0;
  TimeByHoursT m_output_time_lock = 0;

  // a date for which, user can not spend this money before stated date
  TimeByHoursT m_input_time_lock_strict = 0;
//    note for implementation. ther will be 2 differnt input lock time signatures.
//    free input lock time: either the user with lock time or other users with or without lock time, can escrow money
//    strict input lock time: ONLY and ONLY one signature is valid and the owner ONLY can scrow money after time expiration.


  IndividualSignature(){};

  IndividualSignature(
    const QString& signature_key,
    const QString& permitted_to_pledge = "",
    const QString& permitted_to_delegate = "",
    const TimeByHoursT input_time_lock_strict = 0);

  QJsonObject exportJson() const
  {
    return QJsonObject {
      {"sKey", m_signature_key},
      {"pPledge", m_permitted_to_pledge},
      {"pDelegate", m_permitted_to_delegate},
      {"iTLock", QVariant::fromValue(m_input_time_lock).toDouble()},
      {"oTLock", QVariant::fromValue(m_output_time_lock).toDouble()},
      {"iTLockSt", QVariant::fromValue(m_input_time_lock_strict).toDouble()}
    };
  }

  void importJson(const QJsonObject& obj)
  {
    m_signature_key = obj.value("sKey").toString();
    m_permitted_to_pledge = obj.value("pPledge").toString();
    m_permitted_to_delegate = obj.value("pDelegate").toString();
    m_input_time_lock = obj.value("iTLock").toDouble();
    m_output_time_lock = obj.value("oTLock").toDouble();
    m_input_time_lock_strict = obj.value("iTLockSt").toDouble();
  }
};


#endif // INDIVIDUALSIGNATURE_H
