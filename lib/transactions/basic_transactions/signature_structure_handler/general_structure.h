#ifndef GSSHANDLER_H
#define GSSHANDLER_H

#include "individual_signature.h"
#include "unlock_set.h"
#include "unlock_document.h"


class TInput
{
public:
  CDocHashT m_transaction_hash = "";  // the reffered transaction hash
  COutputIndexT m_output_index = 0;

  CAddressT m_owner = "";
  CMPAIValueT m_amount = 0;
  QStringList m_private_keys {};  // they are need to sign the coin in order to spend it
  QJsonObject m_unlock_set {};

  CCoinCodeT getCoinCode();
  QString dumpMe() const;

};

class TOutput
{
public:
  CAddressT m_address = "";
  CMPAIValueT m_amount = 0;
  QString m_output_charachter = CConsts::OUTPUT_NORMAL;
  COutputIndexT m_output_index = -1;
};


namespace SignatureStructureHandler
{


QString calcUnlockHash(const UnlockSet& unlock_set, QString hashAlgorithm = "keccak256");

UnlockDocument createCompleteUnlockSets(
    const QHash<QString, IndividualSignature>& individuals_signing_sets,
    uint16_t neccessarySignaturesCount,
    QVDicT options = {});

UnlockDocument createMOfNMerkle(
    QVector<UnlockSet> unlock_sets,
    QV2DicT customTypes,
    QVDicT customSalts,
    QVDicT options = {});

bool validateSigStruct(const UnlockSet& uSet, const QString& address, const QVDicT& options = {});
bool validateSigStruct(const QJsonObject& uSet, const QString& address, const QVDicT& options = {});
UnlockSet convertJsonUSetToStruct(const QJsonObject& unlockSet);

bool validateStructureStrictions(const UnlockSet& unlock, const QVDicT& options = {});
QString customStringifySignatureSets(const QVector<IndividualSignature>& signature_sets);

QString safeStringifySigntureSets(const QJsonArray& signture_sets);
QString safeStringifyUnlockSet(const QJsonObject& unlockSet);

QString stringifyInputs(const QJsonArray inputs);
QString stringifyInputs(const std::vector<TInput*> inputs);

QString stringifyOutputs(const QJsonArray outputs);
QString stringifyOutputs(const std::vector<TOutput*> outputs);

QJsonObject compactUnlocker(const QJsonObject& unlock_doc);
QJsonArray compactUnlockersArray(const QJsonArray& dExtInfo);

};

#endif // GSSHANDLER_H
