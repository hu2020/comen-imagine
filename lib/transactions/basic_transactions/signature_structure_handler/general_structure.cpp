#include "stable.h"

#include "lib/ccrypto.h"

#include "general_structure.h"


QString TInput::dumpMe() const
{
  QString out = "\nCoin Code: " + m_transaction_hash + ":" + QString::number(m_output_index);
  out += "\nOwner: " + m_owner;
  out += "\nAmount: " + QString::number(m_amount);
  out += "\nUnlockset: " + CUtils::serializeJson(m_unlock_set);
//  out += "\nPrivate keys: " + m_private_keys.join(", ");
  return out;
}

CCoinCodeT TInput::getCoinCode()
{
  return CUtils::packCoinCode(m_transaction_hash, m_output_index);
}

// -  -  -  -  -  -  SignatureStructureHandler -  -  -  -

namespace SignatureStructureHandler
{


UnlockDocument createCompleteUnlockSets(
  const QHash<QString, IndividualSignature>& individuals_signing_sets,
  uint16_t neccessary_signatures_count,
  QVDicT options)
{
  QString signature_type = options.value("signature_type", "").toString();
  QString signature_version = options.value("signature_version", "").toString();
  QString customSalt = options.value("customSalt", "PURE_LEAVE").toString();

  QStringList signers_ids;
  QVDicT signersByKey;

  // generate permutation of signatures. later will be used as tree leaves
  QStringList leave_ids = individuals_signing_sets.keys();
  leave_ids.sort();
  PermutationHandler signPermutations(leave_ids, neccessary_signatures_count);

  QV2DicT custom_types;
  QVector<UnlockSet> unlock_sets;
  QVDicT custom_salts;
  for (QStringList an_unlock_individuals_combination: signPermutations.m_permutations)
  {
    QVector<IndividualSignature> a_signature_combination;
    for (QString an_individual_id: an_unlock_individuals_combination) {
      a_signature_combination.push_back(individuals_signing_sets.value(an_individual_id));
    }
    unlock_sets.push_back(a_signature_combination);


    QString custom_key = CUtils::hash16c(CCrypto::keccak256(customStringifySignatureSets(a_signature_combination)));
    if ((signature_type != "") && (signature_version != ""))
    {
        custom_types[custom_key] = QVDicT
        {
          {"signature_type", signature_type},
          {"signature_version", signature_version}
        };
    }

    if (customSalt != "")
    {
      if (customSalt == "PURE_LEAVE") {
        custom_salts[custom_key] = custom_key;
      } else {
        custom_salts[custom_key] = customSalt;
      }
    }
  }

  return createMOfNMerkle(
    unlock_sets,
    custom_types,
    custom_salts,
    options
  );

}


UnlockDocument createMOfNMerkle(
    QVector<UnlockSet> unlock_sets,
    QV2DicT custom_types,
    QVDicT custom_salts,
    QVDicT options)
{
//  CLog::log("createMOfNMerkle creating unlock_sets" + CUtils::dumpIt(unlock_sets), "app", "trace");

  QString hash_algorithm = options.value("hash_algorithm", "keccak256").toString();
  QString input_type = options.value("input_type", "hashed").toString();

  QStringList hashed_unlocks;
  QHash<QString, UnlockSet> tmp_unlockers;
  QString custom_key, leave_hash;
  for (UnlockSet an_unlock_set: unlock_sets)
  {
    custom_key = CUtils::hash16c(CCrypto::keccak256(customStringifySignatureSets(an_unlock_set.m_signature_sets)));
    if (custom_types.keys().contains(custom_key))
    {
      an_unlock_set.m_signature_type = custom_types[custom_key]["signature_type"].toString();
      an_unlock_set.m_signature_ver = custom_types[custom_key]["signature_version"].toString();
    }
    else
    {
      an_unlock_set.m_signature_type = CConsts::SIGNATURE_TYPES::Basic;
      an_unlock_set.m_signature_ver = "0.0.0";
    }

    // adding random salt to obsecure/unpredictable final address
    // TODO: maybe use crypto secure random generator
    if (custom_salts.keys().contains(custom_key))
    {
      an_unlock_set.m_salt = custom_salts[custom_key].toString();
    } else {
      an_unlock_set.m_salt = CUtils::hash16c(CCrypto::keccak256(CUtils::dumpIt(an_unlock_set) + CUtils::getNow() + QString::number(random())));
    }
    leave_hash = calcUnlockHash(an_unlock_set, hash_algorithm);
    hashed_unlocks.push_back(leave_hash);
    tmp_unlockers[leave_hash] = an_unlock_set;

  }

  auto[merkle_root, mVerifies, mVersion, _levels, _leaves] = CMerkle::generate(
    hashed_unlocks,
    input_type,
    hash_algorithm);
  Q_UNUSED(_levels);
  Q_UNUSED(_leaves);
  merkle_root = CCrypto::keccak256Dbl(merkle_root); // because of securiy, MUST use double hash

  // FIXME: m_signature_type is aplyable for each uSet in a m of n shema, wherase for merkle root we can apply only one signature_type.
  // for now we just use the signature_type of fist uSet.
  // BTW for now all signature_types in an address are same
  if (tmp_unlockers[tmp_unlockers.keys()[0]].m_signature_type == CConsts::SIGNATURE_TYPES::Mix23)
    merkle_root = CCrypto::sha256Dbl(merkle_root);  // Extra securiy level

  UnlockDocument unlock_document(
    merkle_root,
    CCrypto::bech32Encode(merkle_root),
    {},
    mVersion
  );

  // asign merkle proofs to sign_set itself
  for (QString key: tmp_unlockers.keys())
  {
    UnlockSet tmp(
      tmp_unlockers[key].m_signature_sets,
      tmp_unlockers[key].m_salt,
      tmp_unlockers[key].m_signature_type,
      tmp_unlockers[key].m_signature_ver,
      mVerifies.value(key).m_left_hash,
      mVerifies.value(key).m_merkle_proof
    );
    unlock_document.m_unlock_sets.push_back(tmp);
  }
  return unlock_document;
}

QString calcUnlockHash(const UnlockSet& unlock_set, QString hash_algorithm)
{
//  std::string potential_strings = R"(
//  Basic:0.0.0:[{"sKey":"0353444374647c47d52534fb9e0c1d7767d1a143ac5511c1ea45f98bc321732a0c"}]:a1ca0290308ed6a3
//  Basic:0.0.0:[{"sKey":"0353444374647c47d52534fb9e0c1d7768d1a143ac5511c1ee45f98bc321732a0c"},{"sKey":"0257040e1bf22ab8785c56d21615b140b766dee38ec8f1a7db49a6ebf98977a6bc"}]:b84100a46012d51a
//  Strict:0.0.0:[{"sKey":"022968b10e02e2af51a5965b9735ac2c75c51c71207f87bec0bd49fa61902f8619","pPledge":"Y","pDelegate":"Y"},{"sKey":"0339129227adebcb49c89fdcbf036249b1e277727895b6803378a0364c33bc0b46","pPledge":"N","pDelegate":"N"},{"sKey":"03a797608e14ee87a93c0bf7d7d121593c5985030e9053e7d062bf081d59da956b","pPledge":"N","pDelegate":"N"},{"sKey":"03f4e4a46c160246e518c41c661b6eeae89aee2188e9dd454274bcca3414a2ed54","pPledge":"N","pDelegate":"N"},{"sKey":"03c146c6e887a1be14606d4a56a72905620064d30a0efa61bf99c1b4dcd10412ad","pPledge":"Y","pDelegate":"Y"},{"sKey":"02bcf7558f443691819af3d1ab6661f379efb8bbda9791f81156749f218ad2101a","pPledge":"N","pDelegate":"N"}]:a9de676dd54b672c
//  )";


  QString to_be_hashed = unlock_set.m_signature_type + ":" +unlock_set.m_signature_ver +":" + customStringifySignatureSets(unlock_set.m_signature_sets) + ":" + unlock_set.m_salt;//  hash_algorithm(${sType}:${sVer}:${JSON.stringify(sSet)}:${salt})
  CLog::log("Custom stringyfied unlock_struct: " + to_be_hashed, "app", "trace");
  if (hash_algorithm == "keccak256")
  {
    return CCrypto::keccak256(to_be_hashed);
  }

  CLog::log("Invalid hash algorythm!", "app", "fatal");
  exit(918);
}

UnlockSet convertJsonUSetToStruct(const QJsonObject& unlockSet)
{
  QVector<IndividualSignature> sSets;
  for(QJsonValueRef an_s_set: unlockSet.value("sSets").toArray())
  {
    QJsonObject an_s_setJ = an_s_set.toObject();
    IndividualSignature an_s_setDoc {
      an_s_setJ.value("sKey").toString(),
      an_s_setJ.value("pPledge").toString(),
      an_s_setJ.value("pDelegate").toString()};

    if (an_s_setJ.keys().contains("iTLock"))
      an_s_setDoc.m_input_time_lock = an_s_setJ.value("iTLock").toDouble();

    if (an_s_setJ.keys().contains("oTLock"))
      an_s_setDoc.m_output_time_lock = an_s_setJ.value("oTLock").toDouble();

    if (an_s_setJ.keys().contains("iTLockSt"))
      an_s_setDoc.m_input_time_lock_strict = an_s_setJ.value("iTLockSt").toDouble();

    sSets.push_back(an_s_setDoc);
  }

  UnlockSet out {
    sSets,
    unlockSet.value("salt").toString(),
    unlockSet.value("sType").toString(),
    unlockSet.value("sVer").toString(),
    unlockSet.value("lHash").toString(),
    CUtils::convertJSonArrayToQStringList(unlockSet.value("mProof").toArray())
  };

  return out;
}

QString stringifyInputs(const QJsonArray inputs)
{
  QStringList inputs_list = {};
  for(auto an_input: inputs)
    inputs_list.append("[\"" + an_input[0].toString() + "\"," + QString::number(an_input[1].toDouble()) + "]");
  QString inputs_string = "[" + inputs_list.join(",") + "]";
  return inputs_string;
}

QString stringifyInputs(const std::vector<TInput*> inputs)
{
  QStringList inputs_list = {};
  for(TInput* an_input: inputs)
    inputs_list.append("[\"" + an_input->m_transaction_hash+ "\"," + QString::number(an_input->m_output_index) + "]");
  QString inputs_string = "[" + inputs_list.join(",") + "]";
  return inputs_string;
}

QString stringifyOutputs(const QJsonArray outputs)
{
  QStringList outputs_list = {};
  for(auto an_input: outputs)
    outputs_list.append("[\"" + an_input[0].toString() + "\"," + QString::number(an_input[1].toDouble()) + "]");
  QString outputs_string = "[" + outputs_list.join(",") + "]";
  return outputs_string;
}

QString stringifyOutputs(const std::vector<TOutput*> outputs)
{
  QStringList outputs_list = {};
  for(TOutput* an_output: outputs)
    outputs_list.append("[\"" + an_output->m_address + "\"," + QString::number(an_output->m_amount) + "]");
  QString outputs_string = "[" + outputs_list.join(",") + "]";
  return outputs_string;
}

QJsonObject compactUnlocker(const QJsonObject& u_set)
{
  QStringList optional_attributes_string = {"pPledge", "pDelegate"};
  QStringList optional_attributes_int = {"iTLockSt", "iTLock", "oTLock"};

  QJsonArray new_sign_sets {};

  for (auto a_sign_set_: u_set["sSets"].toArray())
  {
    QJsonObject a_sign_set = a_sign_set_.toObject();
    QStringList a_sign_set_keys = a_sign_set.keys();
    QJsonObject a_new_sign_set {
      {"sKey", a_sign_set["sKey"]}
    };

    for (QString a_key: optional_attributes_string)
      if (a_sign_set_keys.contains(a_key) && (a_sign_set[a_key] != ""))
        a_new_sign_set[a_key] = a_sign_set[a_key];

    for (QString a_key: optional_attributes_int)
      if (a_sign_set_keys.contains(a_key) && (a_sign_set[a_key] != 0))
        a_new_sign_set[a_key] = a_sign_set[a_key];

    new_sign_sets.push_back(a_new_sign_set);
  }
  QJsonObject new_u_set {
    {"lHash", u_set["lHash"]},
    {"mProof", u_set["mProof"]},
    {"sSets", new_sign_sets},
    {"sType", u_set["sType"]},
    {"sVer", u_set["sVer"]},
    {"salt", u_set["salt"]}};

  return new_u_set;
}

QJsonArray compactUnlockersArray(const QJsonArray& dExtInfo)
{
  QJsonArray new_doc_ext_info {};
  for (auto an_ext: dExtInfo)
  {
    QJsonObject unlock_doc = an_ext.toObject();
    QJsonObject new_unlock_doc {};
    for (QString a_key: unlock_doc.keys())
    {
      if (a_key == "uSet")
      {
        QJsonObject u_set = unlock_doc["uSet"].toObject();
        QJsonObject new_u_set = compactUnlocker(u_set);
        new_unlock_doc[a_key] = new_u_set;
      }else{
        new_unlock_doc[a_key] = unlock_doc[a_key];
      }
    }
    new_doc_ext_info.push_back(new_unlock_doc);
  }
  return new_doc_ext_info;
}

QString safeStringifySigntureSets(const QJsonArray& signture_sets)
{
  QStringList sets_str;
  for(QJsonValue an_s_set: signture_sets)
  {
    QJsonObject an_s_setJ = an_s_set.toObject();

    QString a_set = "{";
    a_set += "\"sKey\":\"" + an_s_setJ.value("sKey").toString() + "\"";

    if (an_s_setJ.keys().contains("iTLock") && (an_s_setJ["iTLock"].toDouble() > 0))
      a_set += ",\"iTLock\":" + QString::number(an_s_setJ.value("iTLock").toDouble());

    if (an_s_setJ.keys().contains("iTLockSt") && (an_s_setJ["iTLockSt"].toDouble() > 0))
      a_set += ",\"iTLockSt\":" + QString::number(an_s_setJ.value("iTLockSt").toDouble()) ;

    if (an_s_setJ.keys().contains("oTLock") && (an_s_setJ["oTLock"].toDouble() > 0))
      a_set += ",\"oTLock\":" + QString::number(an_s_setJ.value("oTLock").toDouble());

    if(an_s_setJ.value("pDelegate").toString() != "")
      a_set += ",\"pDelegate\":\"" + an_s_setJ.value("pDelegate").toString() + "\"";

    if (an_s_setJ.value("pPledge").toString() != "")
      a_set += ",\"pPledge\":\"" + an_s_setJ.value("pPledge").toString() + "\"";

    a_set += "}";

    sets_str.append(a_set);
  }
  QString out = "[" + sets_str.join(",") + "]";
  return out;
}

QString safeStringifyUnlockSet(const QJsonObject& unlockSet)
{
  QString out = "{";
  if (unlockSet.value("lHash").toString() == "")
  {
    out += "\"lHash\":\"\",";
  }else{
    out += "\"lHash\":\"" + unlockSet.value("lHash").toString() + "\",";
  }
  if (unlockSet.value("mProof").toArray().size() > 0)
  {
    out += "\"mProof\":" + CUtils::serializeJson(unlockSet.value("mProof").toArray()) + ",";
  }else{
    out += "\"mProof\":[],";
  }
  out += "\"salt\":\"" + unlockSet.value("salt").toString() + "\",";
  out += "\"sSets\":" + safeStringifySigntureSets(unlockSet.value("sSets").toArray()) + ",";
  out += "\"sType\":\"" + unlockSet.value("sType").toString() + "\",";
  out += "\"sVer\":\"" + unlockSet.value("sVer").toString() + "\"";
  out += "}";
  return out;
}

bool validateSigStruct(
  const QJsonObject& unlockSet,
  const QString& address,
  const QVDicT& options)
{
  return validateSigStruct(convertJsonUSetToStruct(unlockSet), address, options);
}

bool validateSigStruct(
  const UnlockSet& unlock_set,
  const QString& address,
  const QVDicT& options)
{
  CLog::log("validate Sig Struct: " + CUtils::dumpIt(unlock_set), "app", "debug");
  QString input_type = options.value("input_type", "hashed").toString();
  QString hash_algorithm = options.value("hash_algorithm", "keccak256").toString();
  bool do_permutation = options.value("do_permutation", false).toBool();

  // console.log(validate StructureOfAnUnlockMOfN.args: ${utils.stringify(args)});
  if (unlock_set.m_signature_type == CConsts::SIGNATURE_TYPES::Strict){
    if (!validateStructureStrictions(unlock_set, options))
    {
      CLog::log("Invalid strict address! " + CUtils::dumpIt(unlock_set), "app", "error");
      return false;
    }
  }

  try {

     // normally the wallets SHOULD send the saved order of a sSets, so we do not need to Permutation
     QString leaveHash = calcUnlockHash(unlock_set);

     QString merkle_root = CMerkle::getRootByAProve(leaveHash, unlock_set.m_merkle_proof, unlock_set.m_left_hash, input_type, hash_algorithm);
     merkle_root = CCrypto::keccak256Dbl(merkle_root);  // because of securiy, MUST use double hash

     if ((QStringList{CConsts::HU_DNA_SHARE_ADDRESS, CConsts::HU_INAME_OWNER_ADDRESS}.contains(address)) &&
         (unlock_set.m_signature_type == CConsts::SIGNATURE_TYPES::Mix23))
       merkle_root = CCrypto::sha256Dbl(merkle_root);  // Mixed extra securiy level

     QString bech32 = CCrypto::bech32Encode(merkle_root);
     if (address == bech32)
       return true;


    if (!do_permutation)
    {
      CLog::log("Invalid unlock structure! " + CUtils::dumpIt(unlock_set), "app", "error");
      return false;
    }

      // FIXME: implement it ASAP
     // in case of disordinating inside sSets
//     let hp = new utils.heapPermutation();
//     hp.heapP(unlock_set.sSets)
//     for (let premu of hp.premutions) {
//         leaveHash = hash_algorithm(${unlock_set.sType}:${unlock_set.sVer}:${JSON.stringify(premu)}:${unlock_set.salt});
//         merkle_root = crypto.merkleGetRootByAProve(leaveHash, unlock_set.proofs, unlock_set.lHash, input_type, hash_algorithm);
//         bech32 = crypto.bech32_encodePub(crypto.keccak256Dbl(merkle_root)).encoded;  // because of securiy, MUST use double hash
//         if (_.has(unlock_set, 'address')) {
//             if (unlock_set.address == bech32) {
//                 return true;
//             }
//         } else if (_.has(unlock_set, 'merkle_root')) {
//             if (unlock_set.merkle_root == merkle_root) {
//                 return true;
//             }
//         }
//     };
//     return false;
    CLog::log("Invalid unlock structure, even after premutation1! " + CUtils::dumpIt(unlock_set), "app", "error");
    return false;
  } catch (std::exception) {
    CLog::log("Invalid unlock structure, even after premutation2! " + CUtils::dumpIt(unlock_set), "app", "error");
    return false;
  }
}

QString customStringifySignatureSets(const QVector<IndividualSignature>& signature_sets)
{
  QStringList sSets_serial;
  for (IndividualSignature a_sig: signature_sets)
  {
    QString tmp = "{\"sKey\":\"" + a_sig.m_signature_key + "\"";

    if (a_sig.m_permitted_to_pledge != "")
    {
      tmp += ",\"pPledge\":\"" + a_sig.m_permitted_to_pledge + "\"";
    }

    if (a_sig.m_permitted_to_delegate != "")
    {
      tmp += ",\"pDelegate\":\"" + a_sig.m_permitted_to_delegate + "\"";
    }

    tmp += "}";

    sSets_serial.append(tmp);
  }
  QString custom_stringify = "[" + sSets_serial.join(",") + "]";  //  JSON.stringify(sSet)
  return custom_stringify;
}

bool validateStructureStrictions(const UnlockSet& unlock_set, const QVDicT& options)
{
    // console.log(validate StructureStrictions.args: ${utils.stringify(args)});
  QString hash_algorithm = options.value("hash_algorithm", "keccak256").toString();

  if (unlock_set.m_signature_type == CConsts::SIGNATURE_TYPES::Strict)
  {

    /**
     * this strict type of signature MUST have and ONLY have these 3 features
     * sKey: can be a public key(and later can be also another bech32 address, after implementing nested signature feature)
     * pPledge: means the signer Permitted to Pleadge this account
     * pDelegate: means the signer Permited to Delegate some rights (binded to this address) to others
     */

    if (CUtils::hash16c(CCrypto::keccak256(customStringifySignatureSets(unlock_set.m_signature_sets))) != unlock_set.m_salt)
    {
      CLog::log("invalid strict structure of signature of salt(" + unlock_set.m_salt + ") ");
      return false;
    }

    for (IndividualSignature aSignSet: unlock_set.m_signature_sets)
    {
        if ( (aSignSet.m_signature_key == "") || (aSignSet.m_permitted_to_pledge == "") ||
            (aSignSet.m_permitted_to_delegate == "")
            )
        {
            CLog::log("invalid strict structure of signature:" + CUtils::dumpIt(unlock_set));
            return false;
        }
    }

    return true;
  }

  return true;

}


};
