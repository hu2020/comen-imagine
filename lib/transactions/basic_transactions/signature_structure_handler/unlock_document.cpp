#include "stable.h"
#include "unlock_document.h"

// -  -  -  -  -  -  UnlockDocument -  -  -  -

UnlockDocument::UnlockDocument()
{

}

UnlockDocument::UnlockDocument(
  QString merkle_root,
  QString account_address,
  QVector<UnlockSet> unlock_sets,
  QString merkle_version)
{
  m_merkle_root = merkle_root;
  m_account_address = account_address;
  m_unlock_sets = unlock_sets;
  m_merkle_version = merkle_version;
}

void UnlockDocument::importJson(const QJsonObject &obj)
{
  m_merkle_root = obj.value("m_merkle_root").toString();
  m_account_address = obj.value("m_account_address").toString();
  m_merkle_version = obj.value("m_merkle_version").toString();
  m_unlock_sets = {};
  for (auto a_u_set: obj.value("uSets").toArray())
  {
    UnlockSet uO;
    uO.importJson(a_u_set.toObject());
    m_unlock_sets.push_back(uO);
  }
  m_private_keys = {};
  QJsonObject private_keys = obj.value("the_private_keys").toObject();
  for (QString a_salt: private_keys.keys())
  {
    QStringList priv_keys{};
    for (QJsonValueRef a_ky: private_keys[a_salt].toArray())
    {
      priv_keys.push_back(a_ky.toString());
    }
    m_private_keys.insert(a_salt, priv_keys);
  }
}

QJsonObject UnlockDocument::exportJson() const
{
  QJsonArray unlock_sets{};
  for (UnlockSet a_u_set: m_unlock_sets)
  {
    unlock_sets.push_back(a_u_set.exportToJson());
  }

  QJsonObject private_keys;
  for (QString a_salt: m_private_keys.keys())
  {
    QJsonArray priv_keys{};
    for (QString a_ky: m_private_keys[a_salt])
    {
      priv_keys.push_back(a_ky);
    }
    private_keys.insert(a_salt, priv_keys);
  }

  return QJsonObject {
    {"m_merkle_root", m_merkle_root},
    {"m_account_address", m_account_address},
    {"m_merkle_version", m_merkle_version},
    {"uSets", unlock_sets},
    {"the_private_keys", private_keys},
  };
}

std::tuple<bool, QJsonObject> UnlockDocument::exportUnlockSet(const uint32_t unlock_index) const
{
  if (unlock_index >= static_cast<uint32_t>(m_unlock_sets.size()))
    return {false, {}};
  auto unlock_set = m_unlock_sets[unlock_index];
  return {true, unlock_set.exportToJson()};
}
