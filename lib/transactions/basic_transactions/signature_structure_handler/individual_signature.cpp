#include "stable.h"

#include "individual_signature.h"

IndividualSignature::IndividualSignature(
  const QString& signature_key,
  const QString& permitted_to_pledge,
  const QString& permitted_to_delegate,
  const TimeByHoursT input_time_lock_strict)
{
  m_signer_id = "";
  m_signature_key = signature_key;
  m_permitted_to_pledge = permitted_to_pledge;
  m_permitted_to_delegate = permitted_to_delegate;
  m_input_time_lock_strict = input_time_lock_strict;
}
