#ifndef TRXUTILS_H
#define TRXUTILS_H


class TrxUtils
{
public:
  TrxUtils();

  static std::vector<TInput*> normalizeInputs(
    const std::vector<TInput*> inputs);

  static std::tuple<bool, QJsonArray> normalizeOutputsJ(
    const QJsonArray& outputs = {},
    const bool& sortIt = true);

  static std::tuple<bool, std::vector<TOutput*> > normalizeOutputs(
    const std::vector<TOutput*> outputs);
};

#endif // TRXUTILS_H
