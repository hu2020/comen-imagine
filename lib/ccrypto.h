#ifndef CCRYPTO_H
#define CCRYPTO_H

#include <random>
#include <vector>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include <cryptopp/aes.h>
#include "cryptopp/eccrypto.h"
#include "cryptopp/integer.h"
#include "cryptopp/oids.h"
#include "cryptopp/osrng.h"
#include "cryptopp/pssr.h"
#include <cryptopp/rsa.h>
#include <cryptopp/sha.h>
#include "cryptopp/cryptlib.h"
#include <cryptopp/sha3.h>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>
#include <cryptopp/filters.h>
#include <cryptopp/base64.h>
#include <cryptopp/modes.h>

#include <QString>
#include <string.h>

#include "constants.h"
#include "lib/bech32.h"
#include "lib/utils/cutils.h"
#include "lib/file_handler/file_handler.h"

#include "lib/clog.h"



#include "cryptopp/keccak.h"


class InitCCrypto
{
public:
  InitCCrypto(const InitCCrypto&) = delete;
  static InitCCrypto& get(){return s_instance;};
  void static init(){get().Iinit();};

private:
  InitCCrypto(){Iinit();};
  static InitCCrypto s_instance;
  void Iinit();
};


class CCrypto
{
public:
  static const std::string PUBLIC_BEGIN;
  static const std::string PUBLIC_END;
  static const std::string PRIVATE_BEGIN;
  static const std::string PRIVATE_END;
  static const std::string RSA_PRIVATE_BEGIN;
  static const std::string RSA_PRIVATE_END;

  static QString convertTitleToHash(const QString& title);

  static QString keccak256(const uint64_t num);
  static QString keccak256(const uint32_t num);
  static QString keccak256(const int64_t num);
  static QString keccak256(const int32_t num);
  static QString keccak256(const QString& msg);
  static QString keccak256Dbl(const QString& msg);

  static QString sha256(const QString& msg);
  static QString sha256Dbl(const QString& msg);

  static QString base64Encode(const QString& msg);
  static QString base64Decode(const QString& msg);

  static QString bech32Encode(const QString& str);
  static std::pair< std::string, std::vector<uint8_t> > bech32Decode(const QString& str);
  static bool isValidBech32(const QString& str);

  static std::tuple<bool, std::string> stripHeaderAndFooter(const std::string&  s, const std::string& header, const std::string& footer);

  // ECDSA part
  static std::tuple<bool, QString, QString> ECDSAGenerateKeyPair();
  static bool VerifyMessage(const CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey& key, const std::string& message, const std::string& signature);
  static bool ECDSAValidatePrivateKey(const std::string& hex_private_key);
  static bool ECDSAValidatePrivateKey(const QString& hex_private_key);
  static bool ECDSAValidatePublicKey(const std::string& hex_public_key);
  static bool ECDSAValidatePublicKey(const QString& hex_public_key);
  static std::tuple<bool, CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::ECDSA::PrivateKey, std::string> ECDSAgeneratePrivateKey();
  static std::tuple<bool, CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::ECDSA::PublicKey, std::string> ECDSAgeneratePublicKey(CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::ECDSA::PrivateKey& key);
  static std::tuple<bool, CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::ECDSA::PrivateKey> ECDSAimportHexPrivateKey(const std::string& key);
  static std::tuple<bool, CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::ECDSA::PublicKey> ECDSAimportHexPublicKey(const std::string& key);
  static std::tuple<bool, std::string, std::string> ECDSAsignMessage(const std::string& private_key, const std::string& message);
  static std::tuple<bool, std::string, std::string> ECDSAsignMessage(const QString& private_key, const QString&  message);
  static bool ECDSAVerifysignature(const std::string& public_key, const std::string& message, const std::string& signature);
  static bool ECDSAVerifysignature(const QString& public_key, const QString& message, const QString& signature);

//  QString signMsg(const QString& message, const QString& private_key);
//  bool verifySignature(const QString& signed_message, const QString& signature, const QString&  public_key);


  // general
  static QString getRandomNumber(int len = 8);
  static std::tuple<bool, QString> AESencrypt(const QString& clear_text, const QString& secret_key , const QString& initialization_vector, const QString& aes_version = CConsts::CURRENT_AES_VERSION);
  static std::tuple<bool, QString> AESencrypt020(const QString& clear_text, const QString& secret_key , const QString& initialization_vector, const QString& aes_version = CConsts::CURRENT_AES_VERSION);
  static std::tuple<bool, QString>  AESdecrypt(const QString& cipher, const QString& secret_key, const QString& initialization_vector, const QString& aes_version);
  static std::tuple<bool, QString>  AESdecrypt020(const QString& cipher, const QString& secret_key, const QString& initialization_vector, const QString& aes_version);

  // native iPGP compatible
  static std::tuple<bool, QString, QString> nativeGenerateKeyPair();
  static QString nativeSign(const QString& prvKey, const QString& message);
  static bool nativeVerifySignature(const QString& pubKey, const QString& message, const QString& signature);
  static QString encryptStringWithPublicKey(const QString& publicKey, const QString& toEncrypt);
  static QString decryptStringWithPrivateKey(const QString& privateKey, const QString& toDecrypt);

  static void CPEM_Base64Decode(CryptoPP::BufferedTransformation& source, CryptoPP::BufferedTransformation& dest);
  static void CPEM_Base64Encode(CryptoPP::BufferedTransformation& source, CryptoPP::BufferedTransformation& dest);
  static void CPEM_LoadPublicKey(CryptoPP::BufferedTransformation& src, CryptoPP::X509PublicKey& key, bool subjectInfo);
  static void CPEM_LoadPrivateKey(CryptoPP::BufferedTransformation& src, CryptoPP::PKCS8PrivateKey& key, bool subjectInfo);
  static bool isValidRSAPrivateKey(const QString&  prvKey);
  static bool isValidRSAPublicKey(const QString&  prvKey);
  static std::tuple<bool, CryptoPP::RSA::PrivateKey> read_PEM_private_key(std::string RSA_PRIV_KEY, const std::string& header = PRIVATE_BEGIN, const std::string& footer = PRIVATE_END);
  static std::tuple<bool, CryptoPP::RSA::PublicKey> read_PEM_public_key(std::string RSA_PUB_KEY, const std::string& header = PUBLIC_BEGIN, const std::string& footer = PUBLIC_END);

};

#endif // CCRYPTO_H
