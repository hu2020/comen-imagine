#ifndef CLOG_H
#define CLOG_H

#include <QString>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>


class CLog
{
public:

  CLog(const CLog&) = delete;
  static CLog& get(){return s_instance;};

  void static initLog(){get().IinitLog();};
  void static log(
      const std::string& msg,
      const std::string& module="app",
      const std::string& level = "info"
      ){Ilog(msg, module, level);};
  void static log(
      const QString& msg,
      const std::string& module="app",
      const std::string& level = "info"
  ){Ilog(msg, module, level);};
  void static log(
      const char *msg,
      const char *module="app",
      const char *level="info"
  ){Ilog(msg, module, level);};
  std::string static version();

private:
  CLog(){};
  static CLog s_instance;
  bool m_is_inited = false;

  void IinitLog();

  void static Ilog(
      const std::string& msg,
      const std::string& module="app",
      const std::string& level = "info"
      );
  void static Ilog(
      const QString& msg,
      const std::string& module="app",
      const std::string& level = "info"
  );
  void static Ilog(
      const char *msg,
      const char *module="app",
      const char *level="info"
  );

};

#endif // CLOG_H
