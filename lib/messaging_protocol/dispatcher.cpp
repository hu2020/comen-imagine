#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/dag/dag.h"
#include "lib/dag/missed_blocks_handler.h"
#include "lib/messaging_protocol/graphql_handler.h"
#include "lib/parsing_q_handler/parsing_q_handler.h"
#include "lib/messaging_protocol/dag_message_handler.h"

#include "dispatcher.h"


void invokeDescendents_()
{
  std::this_thread::sleep_for (std::chrono::seconds(50));
  DAGMessageHandler::invokeDescendents();
}

Dispatcher::Dispatcher()
{

}



std::tuple<bool, bool> Dispatcher::dispatchMessage(
  const QString& sender,
  const QJsonObject& message,
  const QString& connection_type)
{
  QStringList keys = message.keys();
  CLog::log(
    "dispatching Message sender(" + sender + ") connection_type(" +
    connection_type + ") keys: " + keys.join(", ") +
    "\nbType(" + message.value("bType").toString() + ") " +
    "bVer(" + message.value("bVer").toString() + ") ",
    "app", "trace");

  if ((sender == "") || (connection_type == ""))
  {
    CLog::log("no sender or connection_type to dispach sender(" + sender + ") ", "app", "error");
    return {false, true};
  }

  if (keys.size() == 0)
  {
    CLog::log("Empty message to dispach ", "app", "error");
    return {false, true};
  }

  QString creation_date = keys.contains("bCDate") ? message.value("bCDate").toString() : "";

  /**
  * pType (Packet type) is more recent than old one bType(Block type) which was created to support block exchanging,
  * wherease pType is proposed to support cpacket exchange
  * which is more comprehensive(and expanded concept) than block.
  * each package can contain one or more blocks and misc requests
  * it is a-kind-of graphGL implementation.
  * each packet contains one or more cards, and each card represents a single query or single query result
  */
  QString pType = keys.contains("pType") ? message.value("pType").toString() : "";
  if (pType != "")
  {
    // it is a graphQL packet which includes some cards, each card represents a block or a page of differnt comunication
    QString pVer = keys.contains("pVer") ? message.value("pVer").toString() : "";
    if (pVer == "")
    {
      CLog::log("missed pVer value! for pType(" + pType + ")", "app", "error");
      return {false, true};
    }

    bool status = true;
    bool should_purge_file = false;
    for (auto aCard_: message.value("cards").toArray())
    {
      QJsonObject aCard = aCard_.toObject();
      auto[dispatch_status, should_purge_file_] = innerDispatchMessage(
        sender,
        connection_type,
        creation_date,
        aCard, // message
        aCard.value("cdType").toString(),
        aCard.value("cdVer").toString());

      if (!dispatch_status)
        status = false;
      should_purge_file |= should_purge_file_;
    }
    return { status, should_purge_file };

  }
  else
  {
    // it is old packet style which is one block per cpacket
    QString type_ = keys.contains("bType") ? message.value("bType").toString() : "";
    if (type_ == "")
      type_ = keys.contains("type") ? message.value("type").toString() : "";
    if (type_ == "")
        return {false, true};

    QString bVer = keys.contains("bVer") ? message.value("bVer").toString() : "";

    /**
     * @brief mVer
     * message version.
     * indeed messages are messages between nodes, only in order to synch nodes togethere.
     * they are some kind of internal commands, and have no effect on DAG it self.
     * messages are about asking a particular block information, or the leave information or handshake with other nodes, etc
     */
    QString mVer = keys.contains("mVer") ? message.value("mVer").toString() : "";

    if ((bVer == "") && (mVer == ""))
    {
      CLog::log("No bVer or mVer stated", "app" "error");
      return { false, true};
    }

    return innerDispatchMessage(
        sender,
        connection_type,
        creation_date,
        message,
        type_,
        bVer,
        mVer);
  }
}



std::tuple<bool, bool> Dispatcher::innerDispatchMessage(
  const QString& sender,
  const QString& connection_type,
  const QString& creation_date,
  const QJsonObject& message,
  const QString& type,
  const QString& ver,
  const QString& mVer)
{


  CLog::log("\n@@@@@ inner Dispatch Message @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", "app", "trace");
  CLog::log("--- dispatching(" + type + ") message from(" + sender + ")", "app", "trace");


  // FIXME: security issue. what happend if adversary creates million of blocks in minute and send the final descendente?
  // in this case all nodes have to download entire blocks all the way back to find ancestor
  // and start to validate from the oldest one and add it to DAG(if is VALID)
  // in this process nodes can not control if the blocks in between are valid or not?
  // so the bandwidth&  machine harddisk will be vasted
  // and network will be blocked!
  // here we need implement a system to control creation date of eache received block(profiled for each neighbor or backer address)
  // and limit creating block(e.g 10 bloocks per minute) in proportion to neighbor's reputation.

  QStringList bloc_types = {
    CConsts::BLOCK_TYPES::Normal,
    CConsts::BLOCK_TYPES::Coinbase,
    CConsts::BLOCK_TYPES::FSign,
    CConsts::BLOCK_TYPES::FVote,
    CConsts::BLOCK_TYPES::POW,
    CConsts::BLOCK_TYPES::RpBlock};

  QStringList message_types = {
    CConsts::MESSAGE_TYPES::DAG_INVOKE_BLOCK,
    CConsts::MESSAGE_TYPES::DAG_INVOKE_DESCENDENTS,
    CConsts::MESSAGE_TYPES::DAG_INVOKE_LEAVES,
    CConsts::MESSAGE_TYPES::DAG_LEAVES_INFO,
    CConsts::MESSAGE_TYPES::HANDSHAKE,
    CConsts::MESSAGE_TYPES::NICETOMEETYOU,
    CConsts::MESSAGE_TYPES::HEREISNEWNEIGHBOR};


  QStringList gql_types = {
    GQL_CARD_TYPES::ProposalLoanRequest,
    GQL_CARD_TYPES::FullDAGDownloadRequest,
    GQL_CARD_TYPES::pleaseRemoveMeFromYourNeighbors,
    GQL_CARD_TYPES::FullDAGDownloadResponse,
    GQL_CARD_TYPES::BallotsReceiveDates,
    GQL_CARD_TYPES::NodeStatusSnapshot,
    GQL_CARD_TYPES::NodeStatusScreenshot,
    GQL_CARD_TYPES::directMsgToNeighbor};


  if (bloc_types.contains(type))
  {
    // the essage is a whole block, so push it to table c_parsing_q
    QString code = CUtils::hash16c(message.value("bHash").toString());
    if (!CUtils::isValidVersionNumber(message.value("bVer").toString()))
    {
      CLog::log("invalid bVer(" + message.value("bVer").toString() + ") for block(" + code + ") in dispatcher! type(" + type + ")", "sec", "error");
      return {false, true};
    }
    CLog::log("--- pushing block(" + code + ") type(" + type + ") from(" + sender + ") to 'c_parsing_q'");

    QVDRecordsT alreadyRecordedInDAG = DAG::searchInDAG(
      {{"b_hash", message.value("bHash")}},
      {"b_hash"});

    if (alreadyRecordedInDAG.size() > 0)
    {
      CLog::log("Duplicated packet received block(" + code + ") type(" + type + ") from(" + sender + ") ", "app", "trace");
      return { true, true};

    } else {

      auto[push_status, should_purge_file] = ParsingQHandler::pushToParsingQ(
        message,
        message.value("bCDate").toString(),
        type,
        message.value("bHash").toString(),
        sender,
        connection_type);
        Q_UNUSED(should_purge_file);

      // if it is a valid block, update last received block info
      if (push_status)
        DAGMessageHandler::setLastReceivedBlockTimestamp(type, message.value("bHash").toString());
    }

    // remove from missed blocks (if exist)
    MissedBlocksHandler::removeFromMissedBlocks(message.value("bHash").toString());

    return { true, true};

  }
  else if (message_types.contains(type))
  {
    return handleSingleMessages(
      sender,
      connection_type,
      creation_date,
      message,
      type,
      ver,
      mVer);

  }
  else if (gql_types.contains(type))
  {
    return handleGQLMessages(
      sender,
      connection_type,
      creation_date,
      message,
      type,
      ver);

  }
  else if (type == CConsts::BLOCK_TYPES::Genesis)
  {
    return {true, true};
  }
  else
  {
    QString code_ = message.keys().contains("bHash") ? message.value("bHash").toString() : "";
    CLog::log("Unknown Message type(" + type + ") was received from (" + sender + ") HD in inbox (" + code_ + ")", "sec", "error");
    return {true, true};
  }
}

std::tuple<bool, bool> Dispatcher::handleSingleMessages(
  const QString& sender,
  const QString& connection_type,
  const QString& creation_date,
  const QJsonObject& message,
  const QString& type,
  const QString& ver,
  QString mVer)
{
  QString code_ = message.keys().contains("bHash") ? message.value("bHash").toString() : "";
  if (message.keys().contains("mVer") && (message.value("mVer").toString() != ""))
    mVer = message.value("mVer").toString();

  if (!CUtils::isValidVersionNumber(mVer))
  {
    CLog::log("invalid mVer for in dispatcher! type(" + type + ") mVer(" + mVer + ")", "sec", "error");
    return {false, true};
  }

  // DAG comunications
  if (type == CConsts::MESSAGE_TYPES::DAG_INVOKE_BLOCK)
  {
    CLog::log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@ CConsts::MESSAGE_TYPES::DAG_INVOKE_BLOCK @@@@@@@@@@@@@@@@@@@@@@@@@@@@@", "app", "trace");
    auto[push_status, should_purge_file] = ParsingQHandler::pushToParsingQ(
      message,
      creation_date,
      type,
      code_,
      sender,
      connection_type);
    return {push_status, should_purge_file};

  }
  else if (type == CConsts::MESSAGE_TYPES::DAG_INVOKE_DESCENDENTS)
  {

    CLog::log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ CConsts::MESSAGE_TYPES::DAG_INVOKE_DESCENDENTS @@@@@@@@@@@@@@@@@@@@@@@@@@@@", "app", "trace");
    auto[push_status, should_purge_file] = ParsingQHandler::pushToParsingQ(
      message,
      creation_date,
      type,
      code_,
      sender,
      connection_type);
    return {push_status, should_purge_file};

  }
  else if (type == CConsts::MESSAGE_TYPES::DAG_INVOKE_LEAVES)
  {

//    if (!iutils.isValidVersionNumber(args.mVer)) {
//        msg = `invalid mVer for  in dispatcher! ${type}`
//        clog.sec.error(msg);
//        return { err: true, msg }
//    }
//    clog.app.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ CConsts::MESSAGE_TYPES::DAG_INVOKE_LEAVES @@@@@@@@@@@@@@@@@@@@@@@@@@@');
//    clog.app.info(`@@@@@@@@@@@@@@@@@@@@ sender: ${sender} @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`);
//    dspchRes = require("./dag/dag-msg-handler").extractLeavesAndPushInSendingQ({
//        sq_type: type,
//        sq_code: utils.getNow(),
//        sender,
//        connection_type
//    });

  }
  else if (type == CConsts::MESSAGE_TYPES::DAG_LEAVES_INFO)
  {

//    if (!iutils.isValidVersionNumber(args.mVer)) {
//        msg = `invalid mVer for  in dispatcher! ${type}`
//        clog.sec.error(msg);
//        return { err: true, msg }
//    }
//    dagMsgHandler.handleReceivedLeaveInfo(message.leaves)
//    dspchRes = { err: false, shouldPurgeMessage: true }

  }
  else if (type == CConsts::MESSAGE_TYPES::HANDSHAKE)
  {
    // handshake
    if (!CUtils::isValidVersionNumber(mVer))
    {
      CLog::log("invalid mVer in dispatcher! type(" + type + ")", "sec", "error");
      return {false, true};
    }
    // TODO: implement a switch to set off/on for no more new neighbor
    CLog::log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ CConsts::MESSAGE_TYPES::HANDSHAKE @@@@@@@@@@@@@@@@@@@@@@@@@@@", "app", "trace");
    auto[parse_status, should_purge_file] = CMachine::parseHandshake(
      sender,
      message,
      connection_type);
    CLog::log("greeting Parsers parse Handshake res: parse_status(" + CUtils::dumpIt(parse_status) + ") should_purge_file(" + CUtils::dumpIt(should_purge_file) + ") ", "app", "trace");
    std::thread(invokeDescendents_).detach();

    CGUI::signalUpdateNeighbors();

    return {parse_status, should_purge_file};

  }
  else if (type == CConsts::MESSAGE_TYPES::NICETOMEETYOU)
  {
    if (!CUtils::isValidVersionNumber(mVer))
    {
      CLog::log("invalid mVer for in dispatcher! type(" + type + ") mVer(" + mVer + ") ", "sec", "error");
      return {false, true};
    }

    auto[parse_status, should_purge_file] = CMachine::parseNiceToMeetYou(
      sender,
      message,
      connection_type);

    std::thread(invokeDescendents_).detach();

    CGUI::signalUpdateNeighbors();

    return {parse_status, should_purge_file};

  }
  else if (type == CConsts::MESSAGE_TYPES::HEREISNEWNEIGHBOR)
  {
//    if (!iutils.isValidVersionNumber(args.mVer)) {
//        msg = `invalid mVer for in dispatcher! ${type}`
//        clog.sec.error(msg);
//        return { err: true, msg }
//    }
//    dspchRes = greetingParsers.parseHereIsNewNeighbor({
//        sender,
//        message,
//        connection_type
//    })

    CGUI::signalUpdateNeighbors();

  }
  else
  {

  }

  return {false, false};
}

std::tuple<bool, bool> Dispatcher::handleGQLMessages(
  const QString& sender,
  const QString& connection_type,
  const QString& creation_date,
  const QJsonObject& message,
  const QString& type,
  const QString& ver)
{

  if (QStringList {
      GQL_CARD_TYPES::ProposalLoanRequest,
      GQL_CARD_TYPES::FullDAGDownloadRequest,
      GQL_CARD_TYPES::pleaseRemoveMeFromYourNeighbors}.contains(type))
  {
    QString dummy_hash = CCrypto::keccak256(CUtils::getNowSSS());
    if (!CUtils::isValidVersionNumber(message.value("cdVer").toString()))
    {
      CLog::log("invalid cdVer for GQL(" + dummy_hash + ") in dispatcher! type(" + type + ")", "sec", "error");
      return {false, true};
    }
    auto[status, should_purge_file] = ParsingQHandler::pushToParsingQ(
      message,
      CUtils::getNow(),
      type,
      dummy_hash,
      sender,
      connection_type);
    return {status, should_purge_file};

  }
  else if (type == GQL_CARD_TYPES::FullDAGDownloadResponse)
  {
    QJsonObject block = message.value("block").toObject();
    QString block_hash = block.value("bHash").toString();
    if (!CUtils::isValidVersionNumber(message.value("cdVer").toString()))
    {
      CLog::log("invalid cdVer for GQL(" + block_hash + ") in dispatcher! type(" + type + ")", "sec", "error");
      return {false, true};
    }

    // update flag LastFullDAGDownloadResponse
    KVHandler::upsertKValue("LastFullDAGDownloadResponse", CUtils::getNow());

    // control if already exist in DAG
    QVDRecordsT alreadyRecordedInDAG = DAG::searchInDAG({{"b_hash", block_hash}});
    if (alreadyRecordedInDAG.size()> 0)
    {
      CLog::log("Duplicated packet received " + type + "-" + block_hash, "app", "trace");
      return {true, true};

    } else {
      // push to table _parsing_q
      auto[status, should_purge_file] = ParsingQHandler::pushToParsingQ(
        block,
        block.value("creationDate").toString(),
        block.value("bType").toString(),
        block_hash,
        sender,
        connection_type);
      Q_UNUSED(should_purge_file);

      // if it was a valid message
      if (status)
      {
        DAGMessageHandler::setLastReceivedBlockTimestamp(block.value("bType").toString(), block_hash);
        if (!CMachine::isInSyncProcess())
          CGUI::signalUpdateParsingQ();
      }
    }
    return {true, true};

//  }
//  else if (type == GQL_CARD_TYPES::BallotsReceiveDates)
//  {
//    // recceived all ballotes received date via QGL
//          clog.app.info(`Ballots Receive date message: ${utils.stringify(message.ballotsReceiveDates)}`);
//          if (!iutils.isValidVersionNumber(message.cdVer)) {
//              msg = `invalid cdVer for GQL(${block.blockHash}) Ballots Receive Dates in dispatcher! ${type}`
//              clog.sec.error(msg);
//              return { err: true, msg }
//          }
//          try {
//              // normalizing/sanitize Ballots Receive Dates and upsert into kv
//              let sanBallots = {};
//              for (let aBlt of utils.objKeys(message.ballotsReceiveDates)) {
//                  sanBallots[utils.stripNonAlphaNumeric(aBlt)] = {
//                      baReceiveDate: utils.stripNonInDateString(message.ballotsReceiveDates[aBlt].baReceiveDate.toString()),
//                      baVoteRDiff: utils.stripNonNumerics(message.ballotsReceiveDates[aBlt].baVoteRDiff.toString()),
//                  }
//              }
//              kvHandler.upsertKValueSync('ballotsReceiveDates', utils.stringify(sanBallots));
//              dspchRes = { err: false, shouldPurgeMessage: true }
//          } catch (e) {
//              clog.sec.error(e);
//              return { err: true, msg: e }
//          }
//    }
//  }
//  else if (type == GQL_CARD_TYPES::NodeStatusScreenshot)
//  {
//    // recceived an screenshot of neighbor's Machine status
//    console.log(`screenshott message: ${message.creationDate}-${message.sender}`);
//    clog.app.info(`screenshott message: ${message.creationDate}-${message.sender}`);
//    // "cdType":"NodeStatusScreenshot","cdVer":"0.0.1","creationDate":"2020-03-08 13:34:10","":"abc@def.gh",

//    if (!iutils.isValidVersionNumber(message.cdVer)) {
//        msg = `invalid cdVer for GQL(${block.blockHash}) Node Status Screenshot in dispatcher! ${type}`
//        clog.sec.error(msg);
//        return { err: true, msg }
//    }
//    const nodeScHandler = require('../services/node-screen-shot/node-screen-shot-handler');
//    let saveRes = nodeScHandler.pushReportToDB({
//        scOwner: `${message.sender}:${message.creationDate}`,
//        content: message.finalReport
//    });
//    saveRes.shouldPurgeMessage = true;
//    return saveRes;

//  }
//  else if (type == GQL_CARD_TYPES::directMsgToNeighbor)
//  {
//    // recceived an screenshot of neighbor's Machine status
//    console.log(`direct Msg To Neighbor: ${utils.stringify(message)}`);
//    clog.app.info(`direct Msg To Neighbor: ${message.creationDate}-${message.sender}`);
//    // "cdType":"NodeStatusScreenshot","cdVer":"0.0.1","creationDate":"2020-03-08 13:34:10","":"abc@def.gh",

//    if (!iutils.isValidVersionNumber(message.cdVer)) {
//      msg = `invalid cdVer for GQL(${block.blockHash}) direct Msg To Neighbor in dispatcher! ${type}`
//      clog.sec.error(msg);
//      return { err: true, msg }
//    }
//    const directmsgHandler = require('./direct-msg-handler');
//    let msgRes = directmsgHandler.recordReceivedMsg({
//      sender: message.sender,
//      receiver: message.receiver,
//      directMsgBody: message.directMsgBody
//    });
//    return msgRes;

  }
  return {false, false};
}
