#ifndef GRAPHQLHANDLER_H
#define GRAPHQLHANDLER_H


namespace GQL_CARD_TYPES
{
  const QString ProposalLoanRequest = "ProposalLoanRequest";
  // FUNC_ PROPOSAL_LOAN_REQUEST = "ProposalLoanRequest";

  const QString FullDAGDownloadRequest = "FullDAGDownloadRequest";
  const QString FullDAGDownloadResponse = "FullDAGDownloadResponse";
  const QString BallotsReceiveDates = "BallotsReceiveDates";
  const QString NodeStatusScreenshot = "NodeStatusScreenshot";
  const QString NodeStatusSnapshot = "NodeStatusSnapshot";
  const QString pleaseRemoveMeFromYourNeighbors = "pleaseRemoveMeFromYourNeighbors";
  const QString directMsgToNeighbor = "directMsgToNeighbor";
};

class GraphQLHandler
{
public:
  GraphQLHandler();

  static std::tuple<QString , QString > makeAPacket(
    const QJsonArray &cards,
    const QString &pType = CConsts::GQL,
    const QString &pVer = "0.0.3",
    const QString &creation_date = CUtils::getNow());

};

#endif // GRAPHQLHANDLER_H
