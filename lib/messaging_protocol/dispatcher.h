#ifndef DISPATCHER_H
#define DISPATCHER_H


class Dispatcher
{
public:
  Dispatcher();

  static std::tuple<bool, bool> dispatchMessage(
    const QString& sender,
    const QJsonObject& message,
    const QString& connection_type);

  static std::tuple<bool, bool> innerDispatchMessage(
    const QString& sender,
    const QString& connection_type,
    const QString& creation_date,
    const QJsonObject& message,
    const QString& type,
    const QString& ver,
    const QString& mVer = "");

  static std::tuple<bool, bool> handleSingleMessages(
    const QString& sender,
    const QString& connection_type,
    const QString& creation_date,
    const QJsonObject& message,
    const QString& type,
    const QString& ver,
    QString mVer);

  static std::tuple<bool, bool> handleGQLMessages(
    const QString& sender,
    const QString& connection_type,
    const QString& creation_date,
    const QJsonObject& message,
    const QString& type,
    const QString& ver);



};

#endif // DISPATCHER_H
