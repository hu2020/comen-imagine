#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/pgp/cpgp.h"
#include "message_handler.h"



//  -  -  -  -  -  -  -  greeting codes

std::tuple<bool, QString, QString, QString, QString> MessageHandler::createHandshakeRequest(
  const QString &connection_type,
  const QString &receiver_id)
{
  CLog::log("generate Handshake packet for: " + connection_type + " " + receiver_id);

  if (connection_type == "")
  {
    CLog::log("The connection_type can not be empty", "app", "error");
    return { false, "", "", "", "" };
  }

  if (receiver_id == "")
  {
    CLog::log("The receiver_id can not be empty!", "app", "error");
    return { false, "", "", "", "" };
  }

  QVDRecordsT receivers_info = CMachine::getNeighbors("", "", "", receiver_id);
  if (receivers_info.size() != 1)
  {
    CLog::log("Wrong receiver id! " + CUtils::dumpIt(receivers_info), "app", "error");
    return { false, "", "", "", "" };
  }

  QVDicT receiver_info = receivers_info[0];

  auto machine_settings = CMachine::getProfile();
  QString email = (connection_type == CConsts::PRIVATE) ? machine_settings.m_mp_settings.m_private_email.m_address : machine_settings.m_mp_settings.m_public_email.m_address;
  QString PGP_public_key = (connection_type == CConsts::PRIVATE) ? machine_settings.m_mp_settings.m_private_email.m_PGP_public_key : machine_settings.m_mp_settings.m_public_email.m_PGP_public_key;

  QString email_body = CUtils::serializeJson({
     {"type", CConsts::MESSAGE_TYPES::HANDSHAKE},
     {"mVer", "0.0.0"},
     {"connectionType", connection_type},
     {"email", email}, // sender email
           // the node public key is used to secure comiunication between nodes
     {"PGPPubKey", CCrypto::base64Encode(PGP_public_key)} //sender's iPGP public key
   });



  CLog::log("write Handshake email_body: " + email_body);

  auto[pgp_status, pgp_msg] = CPGP::encryptPGP(
    email_body,
    "", // sender private key
    "", // receiver Public Key
    "", // secert key
    "", // iv
    false, // should compress
    false // should sign
  );
  if (!pgp_status)
  {
    CLog::log("PGP encryption failed", "app", "fatal");
    return {false, "", "", "", ""};
  }

  QString final_message = CUtils::breakByBR(pgp_msg);
  final_message = CPGP::wrapPGPEnvelope(final_message);

  return {
    true,
    "Handshake from a new neighbor",
    email,  //sender
    receiver_info.value("n_email").toString(), //target
    final_message
  };
}


std::tuple<bool, QString, QString, QString, QString> MessageHandler::createNiceToMeetYou(
  const QString &connection_type,
  const QString &receiver_email,
  const QString &receiver_PGP_public_key)
{
  CLog::log("packetGenerators.write NiceToMeetYou args: connection_type(" + connection_type + ") receiver_email(" + receiver_email + ")", "app", "trace");

  if ((receiver_email == "") || (receiver_PGP_public_key == ""))
  {
    CLog::log("niceToMeetYou missed receiver_email receiver_email(" + receiver_email + ") or PGP key", "app", "error");
    return {false, "", "", "", ""};
  }

  if (connection_type == "")
  {
    CLog::log("In niceToMeetYou, the connection_type can not be (" + connection_type + ")", "app", "error");
    return {false, "", "", "", ""};
  }
  QString default_message_version = "0.0.0";
  QJsonObject Jemail_body {
    {"type", CConsts::MESSAGE_TYPES::NICETOMEETYOU},
    {"connectionType", connection_type},
    {"mVer", default_message_version},
    {"email", CMachine::getPubEmailInfo().m_address},
    {"PGPPubKey", CCrypto::base64Encode(CMachine::getPubEmailInfo().m_PGP_public_key)}};

  QString email_body = CUtils::serializeJson(Jemail_body);

  CLog::log("write NiceToMeetYou email_body1: " + email_body, "app", "trace");
//  auto[secret_key, vi] = CPGP::generateSecretKeyVI();
  auto[pgp_status, encrypted_message] = CPGP::encryptPGP(
    email_body,
    "",
    receiver_PGP_public_key,
    "",
    "",
    true,
    false);
  Q_UNUSED(pgp_status);

  email_body = CUtils::breakByBR(encrypted_message);
  email_body = CPGP::wrapPGPEnvelope(email_body);
  CLog::log("packetGenerators.write NiceToMeetYou email_body2: " + email_body, "app", "trace");

  return {
    true,
    "niceToMeetYou", // title
    CMachine::getPubEmailInfo().m_address, // sender
    receiver_email,
    email_body};

  //TODO after successfull sending must save some part the result and change the email to confirmed
}


std::tuple<bool, QString, QString, QString, QString> MessageHandler::createHereIsNewNeighbor(
  const QString &connection_type,
  const QString &machine_email,
  const QString &machine_PGP_private_key,
  const QString &receiver_email,
  const QString &receiver_PGP_public_key,
  const QString &new_neighbor_email,
  const QString &new_neighbor_PGP_public_key)
{
  CLog::log("create Here Is New Neighbor args: connection_type(" + connection_type + ") ", "app", "trace");

  // clog.app.info('------------ inside hereIsNewNeighbor generator');
  // clog.app.info(args);
  if (
    (new_neighbor_email == "") ||
    (new_neighbor_PGP_public_key == "") ||
    (receiver_email == "") ||
    (receiver_PGP_public_key == "")
   )
    return {false, "", "", "", ""};

  QString default_mVer = "0.0.0";
  QJsonObject Jemail_body {
    {"type", CConsts::MESSAGE_TYPES::HEREISNEWNEIGHBOR},
    {"mVer", default_mVer},
    {"connectionType", connection_type},
    {"newNeighborEmail", new_neighbor_email},
    {"newNeighborPGPPubKey", CCrypto::base64Encode(new_neighbor_PGP_public_key)}};  //sender's iPGP public key

  QString email_body = CUtils::serializeJson(Jemail_body);


//  let params = {
//   shouldSign: true, // in real world you can not sign an email in negotiation step in which the receiver has not your pgp public key
//   shouldCompress: true,
//   message: email_body,
//   sendererPrvKey: machine_PGP_private_key,
//   receiverPubKey: receiver_PGP_public_key
//  }

  CLog::log("write Here Is New Neighbor going to pgp mVer(" + default_mVer + ") connection_type(" + connection_type + ") new_neighbor_email(" + new_neighbor_email + ")", "app", "trace");

  auto[pgp_status, message] = CPGP::encryptPGP(
    email_body,
    machine_PGP_private_key,
    receiver_PGP_public_key,
    "",
    "",
    true,
    true);
  Q_UNUSED(pgp_status);
  message = CUtils::breakByBR(message);
  message = CPGP::wrapPGPEnvelope(message);

  return {
    true,
    "hereIsNewNeighbor",  // title
    machine_email,  // sender
    receiver_email,
    email_body  // message
  };


  // TODO after successfull sending must save some part the result and change the email
  // to confirmed
}
