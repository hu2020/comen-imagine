#include "stable.h"

#include "lib/ccrypto.h"
#include "graphql_handler.h"

GraphQLHandler::GraphQLHandler()
{

}

std::tuple<QString , QString> GraphQLHandler::makeAPacket(
  const QJsonArray &cards,
  const QString &pType,
  const QString &pVer,
  const QString &creation_date)
{
  QJsonObject body_json {
    {"cards", cards},
    {"pType", pType},
    {"pVer", pVer},
    {"creationDate", creation_date}
  };
  QString body = CUtils::serializeJson(body_json);
  QString code = CCrypto::keccak256(body);
  return { code, body };
}

