
#include "ccrypto.h"


InitCCrypto InitCCrypto::s_instance;
void InitCCrypto::Iinit()
{

  //  /* ... Do some crypto stuff here ... */

  //  /* Clean up */

  //  /* Removes all digests and ciphers */

  CLog::log("InitCCrypto::IinitLog");
  return;
}


const std::string CCrypto::PUBLIC_BEGIN = "-----BEGIN PUBLIC KEY-----";
const std::string CCrypto::PUBLIC_END = "-----END PUBLIC KEY-----";
const std::string CCrypto::PRIVATE_BEGIN = "-----BEGIN PRIVATE KEY-----";
const std::string CCrypto::PRIVATE_END = "-----END PRIVATE KEY-----";
const std::string CCrypto::RSA_PRIVATE_BEGIN = "-----BEGIN RSA PRIVATE KEY-----";
const std::string CCrypto::RSA_PRIVATE_END = "-----END RSA PRIVATE KEY-----";



QString CCrypto::convertTitleToHash(const QString& title)
{
  return keccak256(title);
}


using std::cout, std::endl;

//  -  -  -  general

std::tuple<bool, QString> CCrypto::AESencrypt(
  const QString& clear_text,
  const QString& secret_key,
  const QString& initialization_vector,
  const QString& aes_version)
{
  if (aes_version == "0.0.0")
  {
    // no encryption
//    QString out = "{";
//    out += "\"aesVersion\":\"" + aes_version + "\",";
//    out += "\"encrypted\":\"" + base64Encode(clear_text) + "\"}";
    return {true, base64Encode(clear_text)};

  }
  else if (aes_version == "0.1.0")
  {
    // compatible version with legacy javascript clients. FIXME: implement it ASAP
    CLog::log("not suppoerted AES version", "app", "error");
    return {false, ""};
  }
  else if (aes_version == "0.2.0")
  {
    return AESencrypt020(clear_text, secret_key, initialization_vector, aes_version);
  }

  return {false, ""};
}

std::tuple<bool, QString> CCrypto::AESencrypt020(
  const QString& clear_text,
  const QString& secret_key,
  const QString& initialization_vector,
  const QString& aes_version)
{
  Q_UNUSED(aes_version);
  using namespace CryptoPP;
  AutoSeededRandomPool prng;

  std::string cipher, encoded, recovered;

  try
  {
    SecByteBlock aes_key(NULL, 32);
    SecByteBlock ivF(NULL, 16);

    // stub for how you really get it, e.g. reading it from a file, off of a
    //   network socket encrypted with an asymmetric cipher, or whatever
  //  read_key(aes_key, aes_key.size());
    std::string secret_key_ = secret_key.toStdString();
    for(uint i = 0; i < secret_key_.length(); i++)
    {
      aes_key[i] = secret_key_[i];
    }

    // stub for how you really get it, e.g. filling it with random bytes or
    //   reading it from the other side of the socket since both sides have
    //   to use the same IV as well as the same key
  //  read_initialization_vector(iv);
    std::string initialization_vector_ = initialization_vector.toStdString();
    for(uint i = 0; i < initialization_vector_.length(); i++)
    {
      ivF[i] = initialization_vector_[i];
    }

    // the final argument is specific to CFB mode, and specifies the refeeding
    //   size in bytes. This invocation corresponds to Java's
    //   Cipher.getInstance("AES/CFB8/NoPadding")
  //  auto enc1 = new CBC_Mode<AES>::Encryption(aes_key, sizeof(aes_key), ivF, 1);
    CBC_Mode<AES>::Encryption enc1(aes_key, sizeof(aes_key), ivF);
    StringSource ss2( clear_text.toStdString(), true,
      new StreamTransformationFilter( enc1,
        new Base64Encoder(
          new StringSink( cipher ), true, 64
        ) // Base64Encoder
      ) // StreamTransformationFilter
    ); // StringSource
    return {true, QString::fromStdString(cipher)};

  }
  catch( const CryptoPP::Exception& ex )
  {
    std::cerr << ex.what() << endl;
    CUtils::exiter("failed in AESencrypt 0.2.0", 908);
    return {false, ""};
  }

}


std::tuple<bool, QString> CCrypto::AESdecrypt(
    const QString& cipher,
    const QString& secret_key,
    const QString& initialization_vector,
    const QString& aes_version
    )
{
  if (aes_version == "0.0.0")
  {
    // only base 64 decryption
    return {true, base64Decode(cipher)};

  }
  else if (aes_version == "0.1.0")
  {
    // compatible version with legacy javascript clients. FIXME: implement it ASAP
    CLog::log("not suppoerted AES version", "app", "error");
    return {false, ""};
  }
  else if (aes_version == "0.2.0")
  {
    return AESdecrypt020(cipher, secret_key, initialization_vector, aes_version);
  }

  return {false, ""};
}

std::tuple<bool, QString> CCrypto::AESdecrypt020(
  const QString& cipher,
  const QString& secret_key,
  const QString& initialization_vector,
  const QString& aes_version
  )
{
  using namespace CryptoPP;
  AutoSeededRandomPool prng;

  std::string recovered;
  Q_UNUSED(aes_version);

  try
  {
    SecByteBlock aes_key(NULL, 32);
    SecByteBlock ivF(NULL, 16);

    std::string secret_key_ = secret_key.toStdString();
    for(uint i = 0; i < secret_key_.length(); i++)
    {
      aes_key[i] = secret_key_[i];
    }

    std::string initialization_vector_ = initialization_vector.toStdString();
    for(uint i = 0; i < initialization_vector_.length(); i++)
    {
      ivF[i] = initialization_vector_[i];
    }

    CBC_Mode<AES>::Decryption dec1(aes_key, sizeof(aes_key), ivF);

    StringSource ss( cipher.toStdString(), true,
      new Base64Decoder(
        new StreamTransformationFilter( dec1,
          new StringSink( recovered )
        ) // StreamTransformationFilter
      ) // base64
    ); // StringSource

    return {true, QString::fromStdString(recovered)};
  }
  catch( const CryptoPP::Exception& e )
  {
    std::cerr << e.what() << endl;
    return {false, ""};
  }
}


/**
 * @brief getRandomNumber
 * @return
 */
QString CCrypto::getRandomNumber(int len)
{
  // FIXME: maybe use more random source (e.g. open ssl)
  srand (time(NULL));
  QString res = QString::number(rand());
  while (res.length() < len)
    res += QString::number(rand());

  return res.midRef(0, len).toString();
}

QString CCrypto::keccak256(const uint64_t num)
{
  return keccak256(QString::number(num));
}

QString CCrypto::keccak256(const uint32_t num)
{
  return keccak256(QString::number(num));
}

QString CCrypto::keccak256(const int64_t num)
{
  return keccak256(QString::number(num));
}

QString CCrypto::keccak256(const int32_t num)
{
  return keccak256(QString::number(num));
}

QString CCrypto::keccak256(const QString& msg)
{
  std::string m = msg.toStdString();
//  using namespace CryptoPP;
  CryptoPP::Keccak_256 hash;
  std::string hashed;
  CryptoPP::HexEncoder encoder(new CryptoPP::StringSink(hashed));
  std::string digest;
  hash.Update((const byte*)m.data(), m.size());
  digest.resize(hash.DigestSize());
  hash.Final((byte*)&digest[0]);
  CryptoPP::StringSource(digest, true, new CryptoPP::Redirector(encoder));
  std::transform(hashed.begin(), hashed.end(), hashed.begin(), [](unsigned char c){ return std::tolower(c); });
  return QString::fromStdString(hashed);
}

QString CCrypto::keccak256Dbl(const QString& msg){
  return keccak256(keccak256(msg));
}

QString CCrypto::sha256Dbl(const QString& msg)
{
  return sha256(sha256(msg));
}

QString CCrypto::sha256(const QString& msg)
{
  std::string m = msg.toStdString();
  CryptoPP::SHA256 hash;
  std::string hashed;
  CryptoPP::HexEncoder encoder(new CryptoPP::StringSink(hashed));
  std::string digest;
  hash.Update((const byte*)m.data(), m.size());
  digest.resize(hash.DigestSize());
  hash.Final((byte*)&digest[0]);
  CryptoPP::StringSource(digest, true, new CryptoPP::Redirector(encoder));
  std::transform(hashed.begin(), hashed.end(), hashed.begin(), [](unsigned char c){ return std::tolower(c); });
  return QString::fromStdString(hashed);
}

QString CCrypto::base64Encode(const QString& decoded)
{
  QByteArray ba;
  ba.append(decoded);
  return QString(ba.toBase64());
}

QString CCrypto::base64Decode(const QString& encoded)
{
  QByteArray ba;
  ba.append(encoded);
  return QString(QByteArray::fromBase64(ba));
}

QString CCrypto::bech32Encode(const QString& str)
{
  return bech32::ComenEncode(str, "im");
}

bool CCrypto::isValidBech32(const QString& str)
{
  return bech32::ComenIsValid(str.toStdString());
}

std::pair< std::string, std::vector<uint8_t> > CCrypto::bech32Decode(const QString& str)
{
  return bech32::Decode(str.toStdString());
}














std::tuple<bool, std::string> CCrypto::stripHeaderAndFooter(const std::string&  s, const std::string& header, const std::string& footer)
{

  size_t pos1, pos2;
  pos1 = s.find(header);
  if (pos1 == std::string::npos)
    return {false, ""};

  pos2 = s.find(footer, pos1+1);
  if (pos2 == std::string::npos)
      return {false, ""};

  // Start position and length
  pos1 = pos1 + header.length();
  pos2 = pos2 - pos1;
  return {true, s.substr(pos1, pos2)};
}







// - - - - - - - - ECDSA key managements - - - - - - -


std::tuple<bool, CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PrivateKey, std::string> CCrypto::ECDSAgeneratePrivateKey()
{
  using namespace CryptoPP;
  AutoSeededRandomPool prng;
  ECDSA<ECP, SHA256>::PrivateKey privateKey;


  privateKey.Initialize(prng, CryptoPP::ASN1::secp256k1());
  if (!privateKey.Validate( prng, 3 ))
    return {false, privateKey, ""};

  const Integer& x = privateKey.GetPrivateExponent();
  std::stringstream ss;
  ss << std::hex << x;
  std::string private_key_hex = ss.str();//.substr(1, 64);
  private_key_hex = private_key_hex.substr(0, 64);
//  std::cout << "K1: " << std::hex << x << std::endl;
  return {true, privateKey, private_key_hex};
}


std::tuple<bool, CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey, std::string> CCrypto::ECDSAgeneratePublicKey(CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::ECDSA::PrivateKey& privateKey)
{
  using namespace CryptoPP;
  CryptoPP::AutoSeededRandomPool prng;
  ECDSA<ECP, SHA256>::PublicKey publicKey;

  // Sanity check
  if ( !privateKey.Validate(prng, 3) )
    return {false, publicKey, ""} ;

  privateKey.MakePublicKey(publicKey);

  bool result = publicKey.Validate(prng, 3);
  if ( !result )
    return {false, publicKey, ""};

  const ECP::Point& q = publicKey.GetPublicElement();

  const Integer& qx = q.x;
  const Integer& qy = q.y;

  std::stringstream ss_x;
  ss_x << std::hex << qx;
  std::string public_key_x = ss_x.str();
  public_key_x = public_key_x.substr(0, 64);
  CLog::log("public_key_x: " + public_key_x);

  std::stringstream ss_y;
  ss_y << std::hex << qy;
  std::string public_key_y = ss_y.str();
  public_key_y = public_key_y.substr(0, 64);
  CLog::log("public_key_y: " + public_key_y);



//    if (qy.IsNegative() == true)
//    {
//      public_key_x = "03" + public_key_x;
//    }else{
//      public_key_x = "02" + public_key_x;
//    }

//    std::string public_key_hex = ss.str().substr(0, 64);

  return { true, publicKey, public_key_x};
}



std::tuple<bool, CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PrivateKey> CCrypto::ECDSAimportHexPrivateKey(const std::string& exp_)
{
  using namespace CryptoPP;
  AutoSeededRandomPool prng;
  ECDSA<ECP, SHA256>::PrivateKey privateKey;

  std::string exp = exp_;
  exp.insert(0, "0x");
  Integer x(exp.c_str());
  privateKey.Initialize(ASN1::secp256k1(), x);
  bool result = privateKey.Validate(prng, 3);
  return {result, privateKey};
}


std::tuple<bool, CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey> CCrypto::ECDSAimportHexPublicKey(const std::string& compact_point)
{
  using namespace CryptoPP;
  AutoSeededRandomPool prng;

  // ----- reconstruct point from compressed point/value.
  ECDSA<ECP, SHA256>::PublicKey pubKey;
  pubKey.AccessGroupParameters().Initialize(ASN1::secp256k1());
  //  std::string compact_point = "0287ac934f3c8f2e9dacd5dd2a41cacaf33c5da5913e8a710b8adcc1fcb762c04fh";
  StringSource ss (compact_point, true, new CryptoPP::HexDecoder);
  ECP::Point point;

  pubKey.GetGroupParameters().GetCurve().DecodePoint (point, ss, ss.MaxRetrievable());

//  std::cout << "Result after decompression X: " << std::hex << point.x << std::endl;
//  std::cout << "Result after decompression Y: " << std::hex << point.y << std::endl;

  ECDSA<ECP, SHA256>::PublicKey publicKeyRegen;
  publicKeyRegen.Initialize(ASN1::secp256k1(), point);

  bool result = publicKeyRegen.Validate(prng, 3);
  if (result)
  {
    return {true, publicKeyRegen};
  }
  else
  {
    return {false, publicKeyRegen};
  }


//  std::string y_sign_prefix = "02";
//  std::string pt = "87ac934f3c8f2e9dacd5dd2a41cacaf33c5da5913e8a710b8adcc1fcb762c04fh";
////              "87ac934f3c8f2e9dacd5dd2a41cacaf33c5da5913e8a710b8adcc1fcb762c04fh";

//  HexDecoder decoder;
//  decoder.Put((byte*)pt.data(), pt.size());
//  decoder.MessageEnd();

//  ECP::Point q;
//  size_t len = decoder.MaxRetrievable();
//  // len should be GetField().MaxElementByteLength()

//  q.identity = false;
//  q.x.Decode(decoder, len);
////  q.y.Decode(decoder, len/2);
//  if (y_sign_prefix == "02")
//  {
//    q.y.SetPositive();
//  }else{
//    q.y.SetNegative();
//  }
////  q.y.

//  ECDSA<ECP, SHA256>::PublicKey publicKey;
//  publicKey.Initialize(ASN1::secp256k1(), q);

//  bool resultp = publicKey.Validate( prng, 3 );
//  if ( resultp )
//  {
//      cout << "Validated public key" << endl;
//  }
//  else
//  {
//      std::cerr << "Failed to validate public key" << endl;
//      return {false, publicKey};
//  }

//  const ECP::Point& qq = publicKey.GetPublicElement();
//  cout << "Q.x: " << std::hex << qq.x << endl;
//  cout << "Q.y: " << std::hex << qq.y << endl;
//  return {true, publicKey};
}

std::tuple<bool, std::string, std::string> CCrypto::ECDSAsignMessage(const std::string& private_key, const std::string& message)
{
  using namespace CryptoPP;
  AutoSeededRandomPool prng;

  auto [private_status, regenerated_private_key] = ECDSAimportHexPrivateKey(private_key);
  if (!private_status)
    return {false, "", ""};

  ECDSA<ECP, SHA256>::Signer signer(regenerated_private_key);
  size_t siglen = signer.MaxSignatureLength();
  std::string signature(siglen, 0x00);
  siglen = signer.SignMessage( prng, (const byte*)&message[0], message.size(), (byte*)&signature[0] );
  signature.resize(siglen);
  std::string signature_hex;
  StringSource s( signature, true /*pump all*/,
    new HexEncoder(new StringSink( signature_hex ), false)
  ); // StringSource

  return {true, signature_hex, signature};
}

std::tuple<bool, std::string, std::string> CCrypto::ECDSAsignMessage(
  const QString& private_key,
  const QString& message)
{
  return ECDSAsignMessage(private_key.toStdString(), message.toStdString());
}

bool CCrypto::ECDSAVerifysignature(
  const std::string& public_key,
  const std::string& message,
  const std::string& signature_hex)
{
  using namespace CryptoPP;
  AutoSeededRandomPool prng;
  auto[pub_status, regenerated_public_key] = ECDSAimportHexPublicKey(public_key);
  if (!pub_status)
      return false;

  std::string signature;
  StringSource s2(signature_hex, true /*pump all*/,
    new HexDecoder(new StringSink(signature))
  ); // StringSource

  ECDSA<ECP, SHA256>::Verifier verifier(regenerated_public_key);
  bool result = verifier.VerifyMessage( (const byte*)&message[0], message.size(), (const byte*)&signature[0], signature.size() );

  // Verification failure?
  return true; // FIXME: ASAP
  return result;
}


bool CCrypto::ECDSAVerifysignature(
  const QString& public_key,
  const QString& message,
  const QString& signature_hex)
{
  return ECDSAVerifysignature(public_key.toStdString(), message.toStdString(), signature_hex.toStdString());
}

// - - - - - - - - native RSA key managements - - - - - - -


bool CCrypto::VerifyMessage(const CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey& key, const std::string& message, const std::string& signature)
{
  using namespace CryptoPP;
  bool result = false;

  StringSource( signature+message, true,
    new SignatureVerificationFilter(
      ECDSA<ECP,SHA1>::Verifier(key),
      new ArraySink( (byte*)&result, sizeof(result) )
    ) // SignatureVerificationFilter
  );

  return result;
}

bool CCrypto::ECDSAValidatePrivateKey(const std::string& hex_private_key)
{
  auto[status, regenerated_private_key] = ECDSAimportHexPrivateKey(hex_private_key);
  Q_UNUSED(regenerated_private_key);
  if (!status)
  {
    CLog::log("failed in ECDSA import Hex Private Key", "app", "fatal");
    return false;
  }
  return true;
}

bool CCrypto::ECDSAValidatePrivateKey(const QString& hex_private_key)
{
  return ECDSAValidatePrivateKey(hex_private_key.toStdString());
}

bool CCrypto::ECDSAValidatePublicKey(const std::string& hex_public_key)
{
  auto[status, regenerated_public_key] = ECDSAimportHexPublicKey(hex_public_key);
  Q_UNUSED(regenerated_public_key);
  if (!status)
  {
    CLog::log("failed in ECDSA import Hex Public Key", "app", "fatal");
    return false;
  }
  return true;
}

bool CCrypto::ECDSAValidatePublicKey(const QString& hex_public_key)
{
  return ECDSAValidatePublicKey(hex_public_key.toStdString());
}

std::tuple<bool, QString, QString> CCrypto::ECDSAGenerateKeyPair()
{
  using namespace CryptoPP;
  using CryptoPP::AES;
  using CryptoPP::ECDSA;
  using CryptoPP::ECP;
  using CryptoPP::DL_GroupParameters_EC;
  using CryptoPP::OID;

  AutoSeededRandomPool prng;

  auto[privStatus, privKey, privStr] = ECDSAgeneratePrivateKey();
  if (!privStatus)
  {
    CLog::log("failed in ECDSAgeneratePrivateKey", "app", "fatal");
    return {false, "", ""};
  }

  auto[pubStatus, pubKey_, pubStr] = ECDSAgeneratePublicKey(privKey);
  Q_UNUSED(pubKey_);
  if (!pubStatus)
  {
    CLog::log("failed in ECDSAgeneratePublicKey", "app", "fatal");
    return {false, "", ""};
  }

  // validate

  // convert hex string private key to ecdsa object
  auto[prive_status, regenerated_private_key] = ECDSAimportHexPrivateKey(privStr);
  Q_UNUSED(regenerated_private_key);
  if (!prive_status)
  {
    CLog::log("failed in ECDSAimportHexPrivateKey", "app", "fatal");
    return {false, "", ""};
  }

  // convert hex string public key to ecdsa object

  // sign a message & verify
  std::string message = "Imagine all the people living life in peace";
  auto[signatureStatus, signature_hex, signatureAsc] = ECDSAsignMessage(privStr, message);
  Q_UNUSED(signatureAsc);
  if (!signatureStatus)
  {
    CLog::log("failed in ECDSAsignMessage", "app", "fatal");
    return {false, "", ""};
  }

  bool signatureVerifyStatus = ECDSAVerifysignature("02" + pubStr, message, signature_hex);
  if (signatureVerifyStatus)
  {
    pubStr = "02"+pubStr; //FIXME ASAP
  }else{
    CLog::log("failed in ECDSAVerifysignature 02", "app", "fatal");
    signatureVerifyStatus = ECDSAVerifysignature("03" + pubStr, message, signature_hex);
    if (signatureVerifyStatus)
    {
      pubStr = "03"+pubStr;
    }else{
      CLog::log("Even failed in ECDSAVerifysignature 03", "app", "fatal");
      return {false, "", ""};
    }
  }
  auto[pub_status, regenerated_public_key] = ECDSAimportHexPublicKey(pubStr);
  Q_UNUSED(regenerated_public_key);
  if (!pub_status)
  {
    CLog::log("failed in ECDSAimportHexPublicKey", "app", "fatal");
    return {false, "", ""};
  }

  return {
      true,
      QString::fromStdString(privStr),
      QString::fromStdString(pubStr)
    };
}

//QString signMsg(const QString& message, const QString& private_key)
//{
//  // FIXME: implement it
//  return "";

//}

//bool verifySignature(const QString& signed_message, const QString& signature, const QString&  public_key)
//{
//  // FIXME: implement it
//  return true;
//}


//  -  -  -  Native RSA part codes -  -  -  -  -  -  -  -

std::tuple<bool, QString, QString> CCrypto::nativeGenerateKeyPair()
{
  using namespace CryptoPP;

  // Private Key
  CryptoPP::AutoSeededRandomPool rng;
  CryptoPP::RSA::PrivateKey private_key_RSA;
  private_key_RSA.Initialize( rng, 4096 /*, e=17*/);

  // Save as PKCS #8 (using ASN.1 DER Encoding )
  std::string private_key_to_save, private_key_RSA_string;
  StringSink ss1(private_key_to_save);
  private_key_RSA.Save( ss1 );
  StringSource sx1(private_key_to_save, true,
      new Base64Encoder(
          new StringSink(private_key_RSA_string), true, 64
      ) // Base64Encoder
  ); // StringSource
  private_key_RSA_string = PRIVATE_BEGIN + "\n" + private_key_RSA_string + PRIVATE_END;

  {
    auto[isValid, tmp_] = read_PEM_private_key(private_key_RSA_string, PRIVATE_BEGIN, PRIVATE_END);
    Q_UNUSED(tmp_);
    if (!isValid)
        return {isValid, "", ""};
  }

  // Public Key
  std::string public_key_to_save, public_key_RSA_string;
  CryptoPP::RSA::PublicKey publicKey( private_key_RSA );
  // Save as X.509 (using ASN.1 DER Encoding )
  StringSink ss2(public_key_to_save);
  publicKey.Save( ss2 );

  StringSource sx2(public_key_to_save, true,
      new Base64Encoder(
          new StringSink(public_key_RSA_string), true, 64
      ) // Base64Encoder
  ); // StringSource
  public_key_RSA_string = PUBLIC_BEGIN + "\n" + public_key_RSA_string + PUBLIC_END;

  {
    auto[isValid, tmp_] = read_PEM_public_key(public_key_RSA_string, PUBLIC_BEGIN, PUBLIC_END);
    Q_UNUSED(tmp_);
    if (!isValid)
        return {isValid, "", ""};
  }


  return {
    true,
    QString::fromStdString(private_key_RSA_string),
    QString::fromStdString(public_key_RSA_string)
  };

};


std::tuple<bool, CryptoPP::RSA::PublicKey> CCrypto::read_PEM_public_key(std::string RSA_PUB_KEY, const std::string& header, const std::string& footer)
{
  using namespace CryptoPP;
  CryptoPP::RSA::PublicKey rsa;
  try {
    auto[status, strPrivKey] = stripHeaderAndFooter(RSA_PUB_KEY, header, footer);
    if (!status)
        return {false, rsa};

    ByteQueue t1, t2;
    StringSource ss(strPrivKey, true,
        new CryptoPP::Redirector(t1)
    );
    CPEM_Base64Decode(t1, t2);
    CPEM_LoadPublicKey(t2, rsa, true);
    return {true, rsa};
  } catch (const CryptoPP::Exception& ex) {
    return {false, rsa};
  }
}

void CCrypto::CPEM_LoadPublicKey(CryptoPP::BufferedTransformation& src, CryptoPP::X509PublicKey& key, bool subjectInfo)
{
   using namespace CryptoPP;
    X509PublicKey& pk = dynamic_cast<X509PublicKey&>(key);

    if (subjectInfo)
        pk.Load(src);
    else
        pk.BERDecode(src);

#define PEM_KEY_OR_PARAMETER_VALIDATION
#if defined(PEM_KEY_OR_PARAMETER_VALIDATION) && !defined(NO_OS_DEPENDENCE)
    AutoSeededRandomPool prng;
    if (!pk.Validate(prng, 2))
        throw Exception(Exception::OTHER_ERROR, "PEM_LoadPublicKey: key validation failed");
#endif


}


void CCrypto::CPEM_Base64Decode(CryptoPP::BufferedTransformation& source, CryptoPP::BufferedTransformation& dest)
{
  using namespace CryptoPP;
    Base64Decoder decoder(new Redirector(dest));
    source.TransferTo(decoder);
    decoder.MessageEnd();
}

void CCrypto::CPEM_Base64Encode(CryptoPP::BufferedTransformation& source, CryptoPP::BufferedTransformation& dest)
{
  using namespace CryptoPP;
  Base64Encoder encoder(new Redirector(dest), true, 64);
  source.TransferTo(encoder);
  encoder.MessageEnd();
}

bool CCrypto::isValidRSAPublicKey(const QString&  pubKey)
{
  auto[isValid, publicKey] = read_PEM_public_key(pubKey.toStdString());
  Q_UNUSED(publicKey);
  return isValid;
}

bool CCrypto::isValidRSAPrivateKey(const QString&  prvKey)
{
  auto[isValid, prv] = read_PEM_private_key(prvKey.toStdString());
  Q_UNUSED(prv);
  return isValid;
}

void CCrypto::CPEM_LoadPrivateKey(CryptoPP::BufferedTransformation& src, CryptoPP::PKCS8PrivateKey& key, bool subjectInfo)
{
  using namespace CryptoPP;
  if (subjectInfo)
    key.Load(src);
  else
    key.BERDecodePrivateKey(src, 0, src.MaxRetrievable());

#define PEM_KEY_OR_PARAMETER_VALIDATION
#if defined(PEM_KEY_OR_PARAMETER_VALIDATION) && !defined(NO_OS_DEPENDENCE)
  AutoSeededRandomPool prng;
  if (!key.Validate(prng, 2))
    throw Exception(Exception::OTHER_ERROR, "PEM_LoadPrivateKey: key validation failed");
#endif
}

std::tuple<bool, CryptoPP::RSA::PrivateKey> CCrypto::read_PEM_private_key(std::string rsa_priv_key, const std::string& header, const std::string& footer)
{
  using std::string, std::runtime_error;
  using namespace CryptoPP;
  CryptoPP::RSA::PrivateKey rsa;
  try
  {
    auto[status, strPrivKey] = stripHeaderAndFooter(rsa_priv_key, header, footer);
    if (!status)
        return {false, rsa};

    CryptoPP::ByteQueue t1, t2;
    StringSource ss(strPrivKey, true,
        new CryptoPP::Redirector(t1)
    );
    CPEM_Base64Decode(t1, t2);
    CPEM_LoadPrivateKey(t2, rsa, true);
    return {true, rsa};
  } catch (const CryptoPP::Exception& ex) {
    std::cerr << ex.what() << endl;
    return {false, rsa};
  }
}

QString CCrypto::nativeSign(const QString& prvKey, const QString& msg)
{
  using namespace CryptoPP;
  auto[isValid, privateKey] = read_PEM_private_key(prvKey.toStdString());
  Q_UNUSED(isValid);
  //  privateKey.BEREncode();

  //  PKCS8PrivateKey
  std::string message, signature;
  message = msg.toStdString();
  // Sign and Encode
  //  RSASSA_PKCS1v15_SHA_Signer signer(privateKey);
  RSASS<PKCS1v15,SHA256>::Signer signer(privateKey);
  CryptoPP::AutoSeededRandomPool rng;


  // Create signature space
  size_t length = signer.MaxSignatureLength();
  SecByteBlock signature2(length);

  // Sign message
  length = signer.SignMessage(
    rng, (const byte*)message.c_str(),
    message.length(), signature2);

  // Resize now we know the true size of the signature
  signature2.resize(length);

  // base64-encode signature to pass to forge
  std::string encoded;
  CryptoPP::StringSource ss(
    signature2.data(), signature2.size(), true,
    new CryptoPP::Base64Encoder(
      new CryptoPP::StringSink(encoded)
    )
  );

  return CUtils::removeNewLine(QString::fromStdString(encoded));

};

bool CCrypto::nativeVerifySignature(const QString& publicKeyStr, const QString& msg, const QString& sig) {

  return true;  // FIXME: implement it ASAP

  //  https://github.com/digitalbazaar/forge/issues/117
  using namespace CryptoPP;
  std::string message = msg.toStdString();
  std::string signature = sig.toStdString();
  auto[isValid, publicKey] = read_PEM_public_key(publicKeyStr.toStdString());
  if (!isValid)
    return isValid;

  RSASS<PKCS1v15,SHA256>::Verifier verifier(publicKey);
  auto sss = (const byte*)signature.c_str();
  bool result = verifier.VerifyMessage((const byte*)message.c_str(),
    message.length(), sss, signature.size());
  return result;
}


QString CCrypto::decryptStringWithPrivateKey(const QString& privateKeyStr, const QString& toDecrypt)
{
  using namespace CryptoPP;
  auto[isValid, privateKey] = read_PEM_private_key(privateKeyStr.toStdString());
  Q_UNUSED(isValid);
  std::string cipher = (toDecrypt).toStdString();
  std::string recovered;

  // Decryption
//  RSAES_OAEP_SHA_Decryptor d(privateKey);
  RSAES<OAEP<SHA1> >::Decryptor d(privateKey);
  CryptoPP::AutoSeededRandomPool rng;

  StringSource ss2(cipher, true,
    new CryptoPP::Base64Decoder(
      new PK_DecryptorFilter(rng, d,
          new StringSink(recovered)
     ) // PK_DecryptorFilter
    )
  ); // StringSource

  return QString::fromStdString(recovered);
};


QString CCrypto::encryptStringWithPublicKey(const QString& publicKeyStr, const QString& toEncrypt)
{
  std::string plain = toEncrypt.toStdString();
  std::string cipher = "";

  using namespace CryptoPP;
  auto[isValid, publicKey] = read_PEM_public_key(publicKeyStr.toStdString());
  Q_UNUSED(isValid);
  CryptoPP::AutoSeededRandomPool rng;


  // Encryption
  RSAES<OAEP<SHA1> >::Encryptor e(publicKey);

  StringSource ss1(plain, true,
      new PK_EncryptorFilter(rng, e,
        new CryptoPP::Base64Encoder(
          new StringSink(cipher)
        )
     ) // PK_EncryptorFilter
  ); // StringSource

  return CUtils::removeNewLine(QString::fromStdString(cipher));
};
