#include "stable.h"

#include "dag_state_handler.h"




QString doSerial_blocks(const QVDicT& item)
{
 QString out =
   item.value("b_hash").toString() +
   item.value("b_creation_date").toString() +
   item.value("b_ancestors").toString() +
   item.value("b_type").toString() +
//   item.value("b_confidence").toString() +
   item.value("b_utxo_imported").toString();
// CLog::log("doSerial_blocks: " + out);
 return out;
}

QString doSerial_docs_blocks_map(const QVDicT& item)
{
 QString out =
   item.value("dbm_block_hash").toString() +
   item.value("dbm_doc_index").toString() +
   item.value("dbm_doc_hash").toString();
 CLog::log("doSerial_docs_blocks_map: " + out);
 return out;
}

QString doSerial_block_extinfos(const QVDicT& item)
{
 QString out =
   item.value("x_block_hash").toString() +
   item.value("x_creation_date").toString() +
   item.value("_detail").toString();
 CLog::log("doSerial_block_extinfos: " + out);
 return out;
}

QString doSerial_trx_utxos(const QVDicT& item)
{
 QString out =
   item.value("ut_coin").toString() +
   item.value("ut_ref_creation_date").toString() +
   item.value("ut_o_address").toString() +
   QString::number(item.value("ut_o_value").toDouble());
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_proposals(const QVDicT& item)
{
 QString out =
   item.value("pr_hash").toString() +
   item.value("pr_type").toString() +
   item.value("pr_class").toString() +
   item.value("pr_version").toString() +
   item.value("pr_title").toString() +
   item.value("pr_description").toString() +
   item.value("pr_tags").toString() +
   item.value("pr_project_id").toString() +
   QString::number(item.value("pr_help_hours").toInt()) +
   QString::number(item.value("pr_help_level").toInt()) +
   QString::number(item.value("pr_voting_timeframe").toInt()) +
   item.value("pr_polling_profile").toString() +
   item.value("pr_cotributer_account").toString() +
   item.value("pr_start_voting_date").toString() +
   item.value("pr_conclude_date").toString() +
   item.value("pr_approved").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_polling_profiles(const QVDicT& item)
{
 QString out =
   item.value("ppr_name").toString() +
   item.value("ppr_activated").toString() +
   item.value("ppr_perform_type").toString() +
   item.value("ppr_amendment_allowed").toString() +
   item.value("ppr_votes_counting_method").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_pollings(const QVDicT& item)
{
 QString out =
   item.value("pll_hash").toString() +
   item.value("pll_start_date").toString() +
   item.value("pll_creator").toString() +
   item.value("pll_type").toString() +
   item.value("pll_class").toString() +
   item.value("pll_ref").toString() +
   item.value("pll_ref_type").toString() +
   item.value("pll_ref_class").toString() +
   QString::number(item.value("pll_timeframe").toInt()) +
   item.value("pll_version").toString() +
   item.value("pll_comment").toString() +
   QString::number(item.value("pll_y_count").toDouble()) +
   QString::number(item.value("pll_y_shares").toDouble()) +
   QString::number(item.value("pll_y_gain").toDouble()) +
//   QString::number(item.value("pll_y_value").toDouble()) +
   QString::number(item.value("pll_n_count").toDouble()) +
   QString::number(item.value("pll_n_shares").toDouble()) +
   QString::number(item.value("pll_n_gain").toDouble()) +
   QString::number(item.value("pll_n_value").toDouble()) +
   QString::number(item.value("pll_a_count").toDouble()) +
   QString::number(item.value("pll_a_shares").toDouble()) +
   QString::number(item.value("pll_a_gain").toDouble()) +
   QString::number(item.value("pll_a_value").toDouble()) +
   item.value("pll_status").toString() +
   item.value("pll_ct_done").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_ballots_essentials(const QVDicT& item)
{
 QString out =
   item.value("ba_hash").toString() +
   item.value("ba_creation_date").toString() +
   item.value("ba_pll_hash").toString() +
   item.value("ba_voter").toString() +
   QString::number(item.value("ba_voter_shares").toDouble()) +
   QString::number(item.value("ba_vote").toInt()) +
   QString::number(item.value("ba_vote_c_diff").toDouble());
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_ballots_reception_time(const QVDicT& item)
{
 QString out =
   item.value("ba_hash").toString() +
   item.value("ba_creation_date").toString() +
   item.value("ba_pll_hash").toString() +
   item.value("ba_voter").toString() +
   QString::number(item.value("ba_voter_shares").toDouble()) +
   QString::number(item.value("ba_vote").toInt()) +
   QString::number(item.value("ba_vote_c_diff").toDouble()) +
   item.value("ba_receive_date").toString() +
   QString::number(item.value("ba_vote_r_diff").toDouble());
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_dna_shares(const QVDicT& item)
{
 QString out =
   item.value("dn_doc_hash").toString() +
   item.value("dn_creation_date").toString() +
   item.value("dn_project_hash").toString() +
   item.value("dn_shareholder").toString() +
   QString::number(item.value("dn_help_hours").toInt()) +
   QString::number(item.value("dn_help_level").toInt()) +
   QString::number(item.value("dn_votes_y").toDouble()) +
   QString::number(item.value("dn_votes_a").toDouble()) +
   QString::number(item.value("dn_votes_n").toDouble()) +
   item.value("ba_title").toString() +
   item.value("ba_descriptions").toString() +
   item.value("ba_tags").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_treasury(const QVDicT& item)
{
 QString out =
   item.value("tr_coin").toString() +
   item.value("tr_block_hash").toString() +
   item.value("tr_creation_date").toString() +
   item.value("tr_cat").toString() +
   QString::number(item.value("tr_value").toDouble()) +
   item.value("tr_title").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_trx_spend(const QVDicT& item)
{
 QString out =
   item.value("sp_coin").toString() +
   item.value("sp_spend_loc").toString() +
   item.value("sp_spend_date").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_pledged_accounts(const QVDicT& item)
{
 QString out =
   item.value("pgd_hash").toString() +
   item.value("pgd_type").toString() +
   item.value("pgd_class").toString() +
   item.value("pgd_version").toString() +
   item.value("pgd_pledger_sign_date").toString() +
   item.value("pgd_pledgee_sign_date").toString() +
   item.value("pgd_arbiter_sign_date").toString() +
   item.value("pgd_activate_date").toString() +
   item.value("pgd_close_date").toString() +
   item.value("pgd_pledger").toString() +
   item.value("pgd_pledgee").toString() +
   item.value("pgd_arbiter").toString() +
   QString::number(item.value("pgd_principal").toDouble()) +
   QString::number(item.value("pgd_annual_interest").toDouble()) +
   QString::number(item.value("pgd_repayment_offset").toInt()) +
   QString::number(item.value("pgd_repayment_amount").toInt()) +
   QString::number(item.value("pgd_repayment_schedule").toInt()) +
   item.value("pgd_status").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_signals(const QVDicT& item)
{
 QString out =
   item.value("sig_block_hash").toString() +
   item.value("sig_creation_date").toString() +
   item.value("sig_signaler").toString() +
   item.value("sig_key").toString() +
   item.value("sig_value").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_agoras(const QVDicT& item)
{
 QString out =
   item.value("ag_hash").toString() +
   item.value("ag_parent").toString() +
   item.value("ag_in_hash").toString() +
   item.value("ag_lang").toString();
   item.value("ag_title").toString();
   item.value("ag_doc_hash").toString();
   item.value("ag_creation_date").toString();
   item.value("ag_creator").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_agoras_posts(const QVDicT& item)
{
 QString out =
   item.value("ap_ag_hash").toString() +
   item.value("ap_creation_date").toString() +
   item.value("ap_doc_hash").toString() +
   item.value("ap_creator").toString() +
   item.value("ap_reply").toString() +
   QString::number(item.value("ap_reply_point").toInt()) +
   item.value("ap_format_version").toString() +
   item.value("ag_opinion").toString() +
   item.value("ag_attrs").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_wiki_pages(const QVDicT& item)
{
 QString out =
   item.value("wkp_hash").toString() +
   item.value("wkp_creation_date").toString() +
   item.value("wkp_iname").toString() +
   item.value("wkp_hash").toString() +
   item.value("wkp_doc_hash").toString() +
   item.value("wkp_lang").toString() +
   item.value("wkp_format_version").toString() +
   item.value("wkp_creator").toString() +
   item.value("wkp_title").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_wiki_contents(const QVDicT& item)
{
 QString out =
   item.value("wkc_wkp_hash").toString() +
   item.value("wkp_content").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_iname_records(const QVDicT& item)
{
 QString out =
   item.value("in_hash").toString() +
   item.value("in_register_date").toString() +
   item.value("in_name").toString() +
   item.value("in_owner").toString() +
   item.value("in_doc_hash").toString() +
   item.value("in_is_settled").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_iname_bindings(const QVDicT& item)
{
 QString out =
   item.value("nb_doc_hash").toString() +
   item.value("nb_creation_date").toString() +
   item.value("nb_in_hash").toString() +
   item.value("nb_bind_type").toString() +
   item.value("nb_conf_info").toString() +
   item.value("nb_title").toString() +
   item.value("nb_comment").toString() +
   item.value("nb_status").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_administrative_pollings(const QVDicT& item)
{
 QString out =
   item.value("apr_hash").toString() +
   item.value("apr_creation_date").toString() +
   item.value("apr_conclude_date").toString() +
   item.value("apr_approved").toString() +
   item.value("apr_creator").toString() +
   item.value("apr_subject").toString() +
   item.value("apr_values").toString() +
   item.value("apr_comment").toString() +
   item.value("nb_comment").toString() +
   item.value("nb_conclude_info").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_administrative_refines_history(const QVDicT& item)
{
 QString out =
   item.value("arh_hash").toString() +
   item.value("arh_apply_date").toString() +
   item.value("arh_subject").toString() +
   QString::number(item.value("arh_value").toDouble());
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_administrativeCurrentValues(const QVDicT& item)
{
 QString out =
   item.value("cycleStartDate").toString() +
   item.value("getTransactionMinimumFee").toString() +
   item.value("getBasePricePerChar").toString() +
   item.value("getBlockFixCost").toString() +
   item.value("getMinShareToAllowedIssueFVote").toString() +
   item.value("getMinShareToAllowedVoting").toString() +
   item.value("getMinShareToAllowedSignCoinbase").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_trx_suspect_transactions(const QVDicT& item)
{
 QString out =
   item.value("_uniq_hash").toString() +
   item.value("st_voter").toString() +
   item.value("st_vote_date").toString() +
   item.value("st_coin").toString() +
   item.value("st_logger_block").toString() +
   item.value("st_spender_block").toString() +
   item.value("st_spender_doc").toString() +
   item.value("st_receive_order").toString() +
   item.value("st_spend_date").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}

QString doSerial_trx_rejected_transactions(const QVDicT& item)
{
 QString out =
   item.value("_uniq_hash").toString() +
   item.value("rt_doc_hash").toString() +
   item.value("rt_coin").toString();
 CLog::log("doSerial_trx_utxos: " + out);
 return out;
}




