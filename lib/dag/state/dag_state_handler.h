#ifndef DAGSTATEHANDLER_H
#define DAGSTATEHANDLER_H


class CompareState
{
public:
  bool m_have_same_keys = false;
  QStringList m_addition_keys_in_local = {};
  QStringList m_addition_keys_in_remote = {};
  QStringList m_nodes_discrepancies = {};
  QStringList m_compared_tables = {};

  QHash <QString, int32_t> m_are_same_in_records_count;
  QStringList m_discrepancy_in_records_count = {};

  QHash <QString, bool> m_are_same_in_content;
  QStringList m_discrepancy_in_records_contents = {};

  QHash <QString, QStringList> m_local_missed_records;
  QHash <QString, QStringList> m_local_has_more_records;
  QHash <QString, QStringList> m_discrepancy_in_content;
};


class DAGStateHandler
{
public:
  DAGStateHandler();

  static const QString stbl_nodes_snapshots;
  static const QStringList stbl_nodes_snapshots_fields;

  enum VALIDITY_CONTROL_LEVEL {
    STRICT,
    ACCEPTABLE,
    ROUGH
  };


  static GRecordsT generateNodeSnapshot();

  static bool sendNodeSnapshot(
    const QString& target,
    const QString& neighbor = "");

  static QJsonObject convertToJSon(
    const GRecordsT& node_snapshot);

  static void doGeneralCompares(
    CompareState& the_compare_state,
    std::function<QString(const QVDicT& values)> doSerial,
    const QString& key_ref,
    const QVDRecordsT& local_data,
    const QJsonArray& remote_data,
    const QString& hash_field,
    const QString& test_step_number,
    const int compare_level,
    const bool shortening_refs = true);

//  static void compare_blocks(
//    CompareState& the_compare_state,
//    const QVDRecordsT& local_data,
//    const QJsonArray& remote_data,
//    int compare_level);

  static std::tuple<bool, CompareState> compareSnapshots(
    const int snapshot_id,
    int compare_level = VALIDITY_CONTROL_LEVEL::STRICT);

  static bool RecordNodeSnapshotInDb(
    const QString& content,
    const QString& name = CUtils::getNowSSS());

  static QVDRecordsT listSnapshots();

  static QString dumpCompareState(
    const CompareState& report);

};

#endif // DAGSTATEHANDLER_H
