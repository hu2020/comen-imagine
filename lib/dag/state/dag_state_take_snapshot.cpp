#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/file_handler/file_handler.h"
#include "lib/block/node_signals_handler.h"
#include "lib/block/document_types/document.h"
#include "lib/services/polling/ballot_handler.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/messaging_protocol/graphql_handler.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/free_docs/wiki/wiki_handler.h"
#include "lib/services/free_docs/demos/demos_handler.h"
#include "lib/services/contracts/flens/iname_handler.h"
#include "lib/dag/normal_block/rejected_transactions_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/transactions/basic_transactions/utxo/spent_coins_handler.h"
#include "lib/transactions/basic_transactions/utxo/suspect_trx_handler.h"

#include "dag_state_handler.h"




GRecordsT DAGStateHandler::generateNodeSnapshot()
{

  // refresh pollings status
  PollingHandler::maybeUpdateOpenPollingsStat();

  GRecordsT final_report {};
//  let report;

  final_report["_blocks"] = DAG::searchInDAG(
    {},
    {"b_creation_date", "b_backer", "b_hash", "b_type", "b_confidence",
    "b_ancestors", "b_utxo_imported"},
    {{"b_creation_date", "ASC"},
      {"b_backer", "ASC"},
      {"b_hash", "ASC"},
      {"b_type", "ASC"},
      {"b_confidence", "ASC"},
      {"b_ancestors", "ASC"},
      {"b_utxo_imported", "ASC"}});


  final_report["_docs_blocks_map"] = Document::searchInDocBlockMap(
    {},
    {"dbm_block_hash", "dbm_doc_index", "dbm_doc_hash"},
    {{"dbm_block_hash", "ASC"},
    {"dbm_doc_index", "ASC"},
    {"dbm_doc_hash", "ASC"}});


  QVDRecordsT block_extinfos = Block::searchInBlockExtInfo(
    {},
    {"x_creation_date", "x_block_hash", "x_detail"},
    {{"x_creation_date", "ASC"}, {"x_block_hash", "ASC"}, {"x_detail", "ASC"}});
  QVDRecordsT final_block_extinfos {};
  for(QVDicT a_row: block_extinfos)
  {
    a_row["x_detail"] = CCrypto::keccak256(a_row.value("x_detail").toString());
    final_block_extinfos.push_back(a_row);
  }
  final_report["_block_extinfos"] = final_block_extinfos;


  final_report["_proposals"] = ProposalHandler::searchInProposals(
    {},
    {"pr_hash", "pr_type", "pr_class", "pr_version", "pr_title", "pr_descriptions",
    "pr_tags", "pr_project_id", "pr_help_hours", "pr_help_level", "pr_voting_timeframe",
    "pr_polling_profile", "pr_contributor_account", "pr_start_voting_date", "pr_conclude_date", "pr_approved"},
    {{"pr_hash", "ASC"},
    {"pr_type", "ASC"},
    {"pr_class", "ASC"},
    {"pr_version", "ASC"},
    {"pr_title", "ASC"},
    {"pr_descriptions", "ASC"},
    {"pr_tags", "ASC"},
    {"pr_project_id", "ASC"},
    {"pr_help_hours", "ASC"},
    {"pr_help_level", "ASC"},
    {"pr_voting_timeframe", "ASC"},
    {"pr_polling_profile", "ASC"},
    {"pr_contributor_account", "ASC"},
    {"pr_start_voting_date", "ASC"},
    {"pr_conclude_date", "ASC"},
    {"pr_approved", "ASC"}});


  final_report["_polling_profiles"] = PollingHandler::searchInPollingProfiles(
    {},
    {"ppr_name", "ppr_activated", "ppr_perform_type", "ppr_amendment_allowed", "ppr_votes_counting_method", "ppr_version"},
    {{"ppr_name", "ASC"},
    {"ppr_activated", "ASC"},
    {"ppr_perform_type", "ASC"},
    {"ppr_amendment_allowed", "ASC"},
    {"ppr_votes_counting_method", "ASC"},
    {"ppr_version", "ASC"}});


  final_report["_pollings"] = PollingHandler::searchInPollings(
    {},
    {"pll_start_date", "pll_hash", "pll_creator", "pll_type", "pll_class", "pll_ref",
    "pll_ref_type", "pll_ref_class", "pll_timeframe", "pll_version", "pll_comment",
    "pll_y_count", "pll_y_shares", "pll_y_gain", // "pll_y_value",
    "pll_n_count", "pll_n_shares", "pll_n_gain", "pll_n_value",
    "pll_a_count", "pll_a_shares", "pll_a_gain", "pll_a_value",
    "pll_status", "pll_ct_done"},
    {{"pll_start_date", "ASC"},
    {"pll_hash", "ASC"},
    {"pll_creator", "ASC"},
    {"pll_type", "ASC"},
    {"pll_class", "ASC"},
    {"pll_ref", "ASC"},
    {"pll_ref_type", "ASC"},
    {"pll_ref_class", "ASC"},
    {"pll_timeframe", "ASC"},
    {"pll_version", "ASC"},
    {"pll_comment", "ASC"},
    {"pll_y_count", "ASC"},
    {"pll_y_shares", "ASC"},
    {"pll_y_gain", "ASC"},
    // {"pll_y_value", "ASC"},
    {"pll_n_count", "ASC"},
    {"pll_n_shares", "ASC"},
    {"pll_n_gain", "ASC"},
    {"pll_n_value", "ASC"},
    {"pll_a_count", "ASC"},
    {"pll_a_shares", "ASC"},
    {"pll_a_gain", "ASC"},
    {"pll_a_value", "ASC"},
    {"pll_status", "ASC"},
    {"pll_ct_done", "ASC"}});


    final_report["_ballots"] = BallotHandler::searchInBallots(
      {},
      {"ba_hash", "ba_pll_hash", "ba_voter", "ba_voter_shares", "ba_vote", "ba_creation_date",
      "ba_vote_c_diff", "ba_receive_date", "ba_vote_r_diff"},
      {{"ba_hash", "ASC"},
      {"ba_pll_hash", "ASC"},
      {"ba_voter", "ASC"},
      {"ba_voter_shares", "ASC"},
      {"ba_vote", "ASC"},
      {"ba_creation_date", "ASC"},
      {"ba_vote_c_diff", "ASC"},
      {"ba_receive_date", "ASC"},
      {"ba_vote_r_diff", "ASC"}});


    final_report["_dna_shares"] = DNAHandler::searchInDNA(
      {},
      {"dn_creation_date", "dn_project_hash", "dn_shareholder", "dn_doc_hash", "dn_help_hours",
      "dn_help_level", "dn_votes_y", "dn_votes_a", "dn_votes_n", "dn_title","dn_descriptions", "dn_tags"},
      {{"dn_creation_date", "ASC"},
      {"dn_project_hash", "ASC"},
      {"dn_shareholder", "ASC"},
      {"dn_doc_hash", "ASC"},
      {"dn_help_hours", "ASC"},
      {"dn_help_level", "ASC"},
      {"dn_votes_y", "ASC"},
      {"dn_votes_a", "ASC"},
      {"dn_votes_n", "ASC"},
      {"dn_title", "ASC"},
      {"dn_descriptions", "ASC"},
      {"dn_tags", "ASC"}});


    final_report["_treasury"] = TreasuryHandler::searchInTreasury(
      {},
      {"tr_coin", "tr_block_hash", "tr_creation_date", "tr_cat", "tr_value", "tr_title"},
      {{"tr_coin", "ASC"},
      {"tr_block_hash", "ASC"},
      {"tr_creation_date", "ASC"},
      {"tr_cat", "ASC"},
      {"tr_value", "ASC"},
      {"tr_title", "ASC"}});


    final_report["_trx_spend"] = SpentCoinsHandler::searchInSpentCoins(
      {},
      {"sp_coin", "sp_spend_loc", "sp_spend_date"},
      {{"sp_coin", "ASC"},
      {"sp_spend_loc", "ASC"},
      {"sp_spend_date", "ASC"}});


    final_report["_trx_utxos"] = UTXOHandler::searchInSpendableCoins(
      {},
      {"ut_coin", "ut_ref_creation_date", "ut_o_address", "ut_o_value"},
      {{"ut_ref_creation_date", "ASC"},
      {"ut_coin", "ASC"},
      {"ut_o_address", "ASC"},
      {"ut_o_value", "ASC"}});


    final_report["_pledged_accounts"] = GeneralPledgeHandler::searchInPledgedAccounts(
      {},
      {"pgd_hash", "pgd_type", "pgd_class", "pgd_version",
      "pgd_pledger_sign_date", "pgd_pledgee_sign_date", "pgd_arbiter_sign_date",
      "pgd_activate_date", "pgd_close_date",
      "pgd_pledger", "pgd_pledgee", "pgd_arbiter",
      "pgd_principal", "pgd_annual_interest", "pgd_repayment_offset",
      "pgd_repayment_amount", "pgd_repayment_schedule", "pgd_status"},
      {{"pgd_hash", "ASC"},
      {"pgd_type", "ASC"},
      {"pgd_class", "ASC"},
      {"pgd_version", "ASC"},
      {"pgd_pledger_sign_date", "ASC"},
      {"pgd_pledgee_sign_date", "ASC"},
      {"pgd_arbiter_sign_date", "ASC"},
      {"pgd_activate_date", "ASC"},
      {"pgd_close_date", "ASC"},
      {"pgd_pledger", "ASC"},
      {"pgd_pledgee", "ASC"},
      {"pgd_arbiter", "ASC"},
      {"pgd_principal", "ASC"},
      {"pgd_annual_interest", "ASC"},
      {"pgd_repayment_offset", "ASC"},
      {"pgd_repayment_amount", "ASC"},
      {"pgd_repayment_schedule", "ASC"},
      {"pgd_status", "ASC"}});


  final_report["_signals"] = NodeSignalsHandler::searchInSignals(
    {},
    {"sig_creation_date", "sig_block_hash", "sig_signaler", "sig_key", "sig_value"},
    {{"sig_creation_date", "ASC"},
    {"sig_block_hash", "ASC"},
    {"sig_signaler", "ASC"},
    {"sig_key", "ASC"},
    {"sig_value", "ASC"}});


  final_report["_agoras"] = DemosHandler::searchInAgoras(
    {},
    {"ag_parent", "ag_iname", "ag_hash", "ag_language", "ag_title", "ag_description", "ag_tags", "ag_content_format_version", "ag_creator", "ag_creation_date", "ag_last_modified",  "ag_doc_hash"},
    {{"ag_parent", "ASC"},
    {"ag_iname", "ASC"},
    {"ag_hash", "ASC"},
    {"ag_language", "ASC"},
    {"ag_title", "ASC"},
    {"ag_description", "ASC"},
    {"ag_tags", "ASC"},
    {"ag_content_format_version", "ASC"},
    {"ag_creator", "ASC"},
    {"ag_creation_date", "ASC"},
    {"ag_last_modified", "ASC"},
    {"ag_doc_hash", "ASC"}});


  QVDRecordsT demos_posts = DemosHandler::searchInPosts(
    {},
    {"ap_creation_date", "ap_ag_hash", "ap_doc_hash", "ap_creator", "ap_reply", "ap_reply_point",
    "ap_format_version", "ap_opinion", "ap_attrs"},
    {{"ap_creation_date", "ASC"},
    {"ap_ag_hash", "ASC"},
    {"ap_doc_hash", "ASC"},
    {"ap_creator", "ASC"},
    {"ap_reply", "ASC"},
    {"ap_reply_point", "ASC"},
    {"ap_format_version", "ASC"},
    {"ap_opinion", "ASC"},
    {"ap_attrs", "ASC"}});
  QVDRecordsT final_agoras_posts = {};
  for(QVDicT a_record: demos_posts)
  {
    a_record["ap_opinion"] = CCrypto::keccak256(a_record["ap_opinion"].toString());
    final_agoras_posts.push_back(a_record);
  }
  final_report["_agoras_posts"] = final_agoras_posts;


  final_report["_wiki_pages"] = WikiHandler::searchInWikiPages(
    false,
    {},
    {"wkp_creation_date", "wkp_hash", "wkp_iname", "wkp_doc_hash", "wkp_language",
    "wkp_format_version", "wkp_creator", "wkp_title"},
    {{"wkp_creation_date", "ASC"},
    {"wkp_hash", "ASC"},
    {"wkp_iname", "ASC"},
    {"wkp_doc_hash", "ASC"},
    {"wkp_language", "ASC"},
    {"wkp_format_version", "ASC"},
    {"wkp_creator", "ASC"},
    {"wkp_title", "ASC"}});


  QVDRecordsT wiki_contents = WikiHandler::searchInWikiContents(
    {},
    {"wkc_wkp_hash", "wkc_content"},
    {{"wkc_wkp_hash", "ASC"},
    {"wkc_content", "ASC"}});
  QVDRecordsT final_wiki_contents = {};
  for(QVDicT a_record: wiki_contents)
  {
    a_record["wkc_content"] = CCrypto::keccak256(a_record["wkc_content"].toString());
    final_wiki_contents.push_back(a_record);
  }
  final_report["_wiki_contents"] = final_wiki_contents;

  BindingInfo b_info = INameHandler::searchRegisteredINames(
    {},
    {"in_register_date", "in_hash", "in_name", "in_owner", "in_doc_hash", "in_is_settled"},
    {{"in_register_date", "ASC"},
    {"in_hash", "ASC"},
    {"in_name", "ASC"},
    {"in_owner", "ASC"},
    {"in_doc_hash", "ASC"},
    {"in_is_settled", "ASC"}});
  final_report["_iname_records"] = b_info.m_records;


  final_report["_iname_bindings"] = INameHandler::searchINameBindings(
    {},
    {"nb_creation_date", "nb_in_hash", "nb_doc_hash", "nb_bind_type", "nb_conf_info",
    "nb_title", "nb_comment", "nb_status"},
    {{"nb_creation_date", "ASC"},
    {"nb_in_hash", "ASC"},
    {"nb_doc_hash", "ASC"},
    {"nb_bind_type", "ASC"},
    {"nb_conf_info", "ASC"},
    {"nb_title", "ASC"},
    {"nb_comment", "ASC"},
    {"nb_status", "ASC"}});


  final_report["_administrative_pollings"] = SocietyRules::searchInAdmPollings(
    {},
    {"apr_creation_date", "apr_conclude_date", "apr_approved", "apr_hash", "apr_creator", "apr_subject",
    "apr_values", "apr_comment", "apr_conclude_info"},
    {{"apr_creation_date", "ASC"},
    {"apr_conclude_date", "ASC"},
    {"apr_approved", "ASC"},
    {"apr_hash", "ASC"},
    {"apr_creator", "ASC"},
    {"apr_subject", "ASC"},
    {"apr_values", "ASC"},
    {"apr_comment", "ASC"},
    {"apr_conclude_info", "ASC"}});


  final_report["_administrative_refines_history"] = SocietyRules::searchInAdmRefineHistory(
    {},
    {"arh_apply_date", "arh_hash", "arh_subject", "arh_value"},
    {{"arh_apply_date", "ASC"},
    {"arh_hash", "ASC"},
    {"arh_subject", "ASC"},
    {"arh_value", "ASC"}});


  final_report["administrativeCurrentValues"] = QVDRecordsT {SocietyRules::readAdministrativeCurrentValues()};


  final_report["_trx_suspect_transactions"] = SuspectTrxHandler::searchInSusTransactions(
    {},
    {"st_voter", "st_vote_date", "st_coin", "st_logger_block", "st_spender_block",
    "st_spender_doc", "st_receive_order", "st_spend_date"},
    {{"st_voter", "ASC"},
    {"st_vote_date", "ASC"},
    {"st_coin", "ASC"},
    {"st_logger_block", "ASC"},
    {"st_spender_block", "ASC"},
    {"st_spender_doc", "ASC"},
    {"st_receive_order", "ASC"},
    {"st_spend_date", "ASC"}});


  final_report["_trx_rejected_transactions"] = RejectedTransactionsHandler::searchInRejectedTrx(
    {},
    {"rt_block_hash", "rt_doc_hash", "rt_coin"},
    {{"rt_block_hash", "ASC"},
    {"rt_doc_hash", "ASC"},
    {"rt_coin", "ASC"}});

  return final_report;
}




