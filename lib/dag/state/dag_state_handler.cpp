#include "stable.h"

#include "dag_state_do_serial.cpp"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/file_handler/file_handler.h"
#include "lib/block/node_signals_handler.h"
#include "lib/block/document_types/document.h"
#include "lib/services/polling/ballot_handler.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/messaging_protocol/graphql_handler.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/free_docs/wiki/wiki_handler.h"
#include "lib/services/free_docs/demos/demos_handler.h"
#include "lib/services/contracts/flens/iname_handler.h"
#include "lib/dag/normal_block/rejected_transactions_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/transactions/basic_transactions/utxo/spent_coins_handler.h"
#include "lib/transactions/basic_transactions/utxo/suspect_trx_handler.h"

#include "dag_state_handler.h"

const QString DAGStateHandler::stbl_nodes_snapshots = "c_nodes_snapshots";
const QStringList DAGStateHandler::stbl_nodes_snapshots_fields = {"nss_id", "nss_label", "nss_content", "nss_creation_date"};

DAGStateHandler::DAGStateHandler()
{

}

QJsonObject DAGStateHandler::convertToJSon(
  const GRecordsT& node_snapshot)
{
  QJsonObject out {};
  for(QString a_group: node_snapshot.keys())
    out[a_group] = BlockUtils::convertToJSonArray(node_snapshot["a_group"]);

  return out;
}


// js name was dlNodeScreenShot
bool DAGStateHandler::sendNodeSnapshot(
  const QString& target,
  const QString& neighbor)
{
  GRecordsT node_snapshot = generateNodeSnapshot();

  QString string_snapshot = CUtils::serializeJson(convertToJSon(node_snapshot));

  if (target == "toHard")
  {
    // write on Hard Disk(backup-dag folder)
    return FileHandler::write(
      CMachine::getDAGBackup(),
      CUtils::getNow() + "_node_snapshot.txt",
      string_snapshot);

  } else if (target == "toNeighbor") {
    if (neighbor == "")
      return false;

    auto[code, body] = GraphQLHandler::makeAPacket(
      QJsonArray{ QJsonObject {
        {"cdType", CConsts::CARD_TYPES::NodeStatusSnapshot},
        {"cdVer", "0.0.1"},
        {"sender", CMachine::getPubEmailInfo().m_address},
        {"finalReport", string_snapshot},
        {"creationDate", CUtils::getNow()}}});

    bool pushRes = SendingQHandler::pushIntoSendingQ(
      CConsts::GQL,
      code,
      body,
      "Sending node snapshot to neighbor(" + neighbor + ")",
      {neighbor});

    CLog::log("Sending node snapshot to neighbor(" + neighbor + ") pushRes(" + CUtils::dumpIt(pushRes) + ")");
    return pushRes;
  }

  return false;
}

void DAGStateHandler::doGeneralCompares(
  CompareState& the_compare_state,
  std::function<QString(const QVDicT& values)> doSerial,
  const QString& key_ref,
  const QVDRecordsT& local_data,
  const QJsonArray& remote_data,
  const QString& hash_field,
  const QString& test_step_number,
  const int compare_level,
  const bool shortening_refs)
{
  QString msg = "";

  QString hash_field_value;

  QStringList local_hashes = {};
  QV2DicT local_dict = {};
  for (QVDicT a_local: local_data)
  {
    hash_field_value = a_local[hash_field].toString();
    local_hashes.append(hash_field_value);
    local_dict[hash_field_value] = a_local;
    local_dict[hash_field_value]["recordHash"] = CCrypto::keccak256(doSerial(a_local));
  }

  QStringList remote_hashes = {};
  QV2DicT remote_dict = {};
  for (auto a_remote: remote_data)
  {
    hash_field_value = a_remote[hash_field].toString();
    remote_hashes.append(hash_field_value);
    remote_dict[hash_field_value] = BlockUtils::convertToQVDicT(a_remote.toObject());
    remote_dict[hash_field_value]["recordHash"] = CCrypto::keccak256(doSerial(remote_dict[hash_field_value]));
  }

  int32_t records_count_are_same = 0;
  QStringList local_missed_records = CUtils::arrayDiff(remote_hashes, local_hashes);
  if (local_missed_records.size() > 0)
  {
    records_count_are_same = -local_missed_records.size();

    if (shortening_refs)
    {
      msg = "local missed records for key(" + key_ref + "." + hash_field + "): (" + CUtils::shortStringsList(CUtils::arrayUnique(local_missed_records)).join(", ") + ")";
    } else {
      msg = "local missed records for key(" + key_ref + "." + hash_field + "): (" + CUtils::arrayUnique(local_missed_records).join(", ") + ")";
    }
    the_compare_state.m_nodes_discrepancies.append(msg);
  }

  QStringList local_has_more_records = CUtils::arrayDiff(local_hashes, remote_hashes);
  if (local_has_more_records.size() > 0)
  {
    records_count_are_same = local_has_more_records.size();
    if (shortening_refs) {
      msg = "Local has extra records for key(" + key_ref + "." + hash_field + "): (" + CUtils::shortStringsList(CUtils::arrayUnique(local_has_more_records)).join(", ") + ")";
    } else {
      msg = "Local has extra records for key(" + key_ref + "." + hash_field + "): (" + CUtils::arrayUnique(local_has_more_records).join(", ") + ")";
    }
    the_compare_state.m_nodes_discrepancies.append(msg);
  }

  the_compare_state.m_are_same_in_records_count[key_ref] = records_count_are_same;

  QStringList discrepancy_in_content = {};
  for (QString a_hash: local_hashes)
  {
    if (local_dict.keys().contains(a_hash) && remote_dict.keys().contains(a_hash))
    {
      if (local_dict[a_hash]["recordHash"].toString() != remote_dict[a_hash]["recordHash"].toString())
      {
        discrepancy_in_content.append(a_hash);
        CLog::log("Discrepancy in content for key(" + key_ref + "." + hash_field + ") \nlocal: " + CUtils::dumpIt(local_dict[a_hash]) + " \nremote: " + CUtils::dumpIt(remote_dict[a_hash]), "app", "warning");
        if (shortening_refs)
        {
            msg = "Discrepancy in content for key(" + key_ref + "." + hash_field + ") \tlocal: " + doSerial(local_dict[a_hash]) + " \n\tremote: " + doSerial(remote_dict[a_hash]);
        } else {
            msg = "Discrepancy in content for key(" + key_ref + "." + hash_field + ") \tlocal: " + doSerial(local_dict[a_hash]) + " \n\tremote: " + doSerial(remote_dict[a_hash]);
        }
        the_compare_state.m_nodes_discrepancies.append(msg);
      }
    }
  }
  the_compare_state.m_are_same_in_content[key_ref] = (discrepancy_in_content.size() == 0);

  the_compare_state.m_local_missed_records[key_ref] = local_missed_records;
  the_compare_state.m_local_has_more_records[key_ref] = local_has_more_records;
  the_compare_state.m_discrepancy_in_content[key_ref] = discrepancy_in_content;
}

std::tuple<bool, CompareState> DAGStateHandler::compareSnapshots(
  const int snapshot_id,
  int compare_level)
{

  //  compare_level = VALIDITY_CONTROL_LEVEL::ACCEPTABLE; //later remove it

  CompareState the_compare_state = {};

  // refresh pollings status
  PollingHandler::maybeUpdateOpenPollingsStat();


  QueryRes remote_report_ = DbModel::select(
    stbl_nodes_snapshots,
    stbl_nodes_snapshots_fields,
    {{"nss_id", snapshot_id}});
  Unwrapped unwrapped;
  try {
    unwrapped = BlockUtils::unwrapSafeContentForDB(remote_report_.records[0].value("nss_content").toString());
    if (!unwrapped.status)
      return {false, the_compare_state};

  }catch(std::exception){
    return {false, the_compare_state};
  }

  QJsonObject remote_report = CUtils::parseToJsonObj(unwrapped.content);
  QStringList remote_report_keys = remote_report.keys();
  remote_report_keys.sort();
  if (remote_report_keys.size() == 0)
    return {false, the_compare_state};

  GRecordsT local_report = generateNodeSnapshot();

  QStringList local_report_keys = local_report.keys();
  local_report_keys.sort();
  the_compare_state.m_have_same_keys = remote_report_keys.join(",") == local_report_keys.join(",");
  if (!the_compare_state.m_have_same_keys)
  {
    the_compare_state.m_addition_keys_in_local = CUtils::arrayDiff(local_report_keys, remote_report_keys);
    the_compare_state.m_addition_keys_in_remote = CUtils::arrayDiff(remote_report_keys, local_report_keys);
  }

//  the_compare_state.areChecked = [];
  // comparing all tables of locl with a given remote
  for (QString a_key: local_report_keys)
  {
    if (!remote_report.contains(a_key))
      continue;

    the_compare_state.m_compared_tables.append(a_key);

    QVDRecordsT local_data = local_report[a_key];
    QJsonArray remote_data = remote_report[a_key].toArray();


    if (local_data.size() != remote_data.size())
      the_compare_state.m_nodes_discrepancies.append("Table elements count not matched: table(" + a_key + ") local rows(" + QString::number(local_data.size()) + ") remote rows(" + QString::number(remote_data.size()) + ")");

    bool dummy_jump_tests = false;
    if (a_key == "_blocks")
    {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_blocks;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_blocks", // key_ref
        local_data,
        remote_data,
        "b_hash", // hash_field
        "1", // test_step_number
        compare_level);

    } else if (a_key == "_docs_blocks_map") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_docs_blocks_map;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_docs_blocks_map", // key_ref
        local_data,
        remote_data,
        "dbm_block_hash", // hash_field
        "2", // test_step_number
        compare_level);

    } else if (a_key == "_block_extinfos") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_block_extinfos;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_block_extinfos", // key_ref
        local_data,
        remote_data,
        "x_block_hash", // hash_field
        "3", // test_step_number
        compare_level);

    } else if (a_key == "_proposals") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_proposals;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_proposals", // key_ref
        local_data,
        remote_data,
        "pr_hash", // hash_field
        "4", // test_step_number
        compare_level);

    } else if (a_key == "_polling_profiles") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_polling_profiles;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_polling_profiles", // key_ref
        local_data,
        remote_data,
        "ppr_name", // hash_field
        "5", // test_step_number
        compare_level);

    } else if (a_key == "_pollings") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_pollings;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_pollings", // key_ref
        local_data,
        remote_data,
        "pll_hash", // hash_field
        "6", // test_step_number
        compare_level);

    } else if (a_key == "_ballots") {
      std::function<QString(const QVDicT& values)> doSerial1 = doSerial_ballots_essentials;
      doGeneralCompares(
        the_compare_state,
        doSerial1,
        "_ballots_essentials", // key_ref
        local_data,
        remote_data,
        "ba_hash", // hash_field
        "7a", // test_step_number
        compare_level);

//      std::function<QString(const QVDicT& values)> doSerial2 = doSerial_ballots_reception_time;
//      doGeneralCompares(
//        the_compare_state,
//        doSerial2,
//        "_ballots_reception_time", // key_ref
//        local_data,
//        remote_data,
//        "ba_hash", // hash_field
//        "7b", // test_step_number
//        compare_level);

    } else if (a_key == "_dna_shares") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_dna_shares;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_dna_shares", // key_ref
        local_data,
        remote_data,
        "dn_doc_hash", // hash_field
        "8", // test_step_number
        compare_level);

    } else if (a_key == "_treasury") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_treasury;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_treasury", // key_ref
        local_data,
        remote_data,
        "tr_coin", // hash_field
        "9", // test_step_number
        compare_level);

    } else if (a_key == "_trx_spend") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_trx_spend;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_trx_spend", // key_ref
        local_data,
        remote_data,
        "sp_coin", // hash_field
        "10", // test_step_number
        compare_level);

    } else if (a_key == "_trx_utxos") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_trx_utxos;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_trx_utxos", // key_ref
        local_data,
        remote_data,
        "ut_coin", // hash_field
        "11", // test_step_number
        compare_level);

    } else if (a_key == "_pledged_accounts") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_pledged_accounts;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_pledged_accounts", // key_ref
        local_data,
        remote_data,
        "pgd_hash", // hash_field
        "12", // test_step_number
        compare_level);

    } else if (a_key == "_signals") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_signals;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_signals", // key_ref
        local_data,
        remote_data,
        "sig_block_hash", // hash_field
        "13", // test_step_number
        compare_level);

    } else if (a_key == "_agoras") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_agoras;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_agoras", // key_ref
        local_data,
        remote_data,
        "ag_hash", // hash_field
        "14", // test_step_number
        compare_level);

    } else if (a_key == "_agoras_posts") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_agoras_posts;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_agoras_posts", // key_ref
        local_data,
        remote_data,
        "ap_ag_hash", // hash_field
        "15", // test_step_number
        compare_level);

    } else if (a_key == "_wiki_pages") {
      if (dummy_jump_tests)
        continue;
      std::function<QString(const QVDicT& values)> doSerial = doSerial_wiki_pages;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_wiki_pages", // key_ref
        local_data,
        remote_data,
        "wkp_hash", // hash_field
        "16", // test_step_number
        compare_level);

    } else if (a_key == "_wiki_contents") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_wiki_contents;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_wiki_contents", // key_ref
        local_data,
        remote_data,
        "wkc_wkp_hash", // hash_field
        "17", // test_step_number
        compare_level);

    } else if (a_key == "_iname_records") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_iname_records;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_iname_records", // key_ref
        local_data,
        remote_data,
        "in_hash", // hash_field
        "18", // test_step_number
        compare_level);

    } else if (a_key == "_iname_bindings") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_iname_bindings;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_iname_bindings", // key_ref
        local_data,
        remote_data,
        "nb_doc_hash", // hash_field
        "19", // test_step_number
        compare_level);

    } else if (a_key == "_administrative_pollings") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_administrative_pollings;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_administrative_pollings", // key_ref
        local_data,
        remote_data,
        "apr_hash", // hash_field
        "20", // test_step_number
        compare_level);

    } else if (a_key == "_administrative_refines_history") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_administrative_refines_history;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_administrative_refines_history", // key_ref
        local_data,
        remote_data,
        "arh_hash", // hash_field
        "21", // test_step_number
        compare_level);

    } else if (a_key == "_administrativeCurrentValues") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_administrativeCurrentValues;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_administrativeCurrentValues", // key_ref
        local_data,
        remote_data,
        "cycleStartDate", // FIXME: in case of having 2 or more adm-polling in a cycle, this handle will be ambigious
        "22", // test_step_number
        compare_level);

    } else if (a_key == "_trx_suspect_transactions") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_trx_suspect_transactions;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_trx_suspect_transactions", // key_ref
        local_data,
        remote_data,
        "_uniq_hash",
        "23", // test_step_number
        compare_level);

    } else if (a_key == "_trx_rejected_transactions") {
      std::function<QString(const QVDicT& values)> doSerial = doSerial_trx_rejected_transactions;
      doGeneralCompares(
        the_compare_state,
        doSerial,
        "_trx_rejected_transactions", // key_ref
        local_data,
        remote_data,
        "_uniq_hash",
        "24", // test_step_number
        compare_level);

    }

  }

  the_compare_state.m_discrepancy_in_records_count = QStringList {};
  for (QString a_key: the_compare_state.m_are_same_in_records_count.keys())
    if (the_compare_state.m_are_same_in_records_count[a_key])
      the_compare_state.m_discrepancy_in_records_count.append(a_key);
  the_compare_state.m_discrepancy_in_records_count = CUtils::arrayDiff(the_compare_state.m_compared_tables, the_compare_state.m_discrepancy_in_records_count);

  the_compare_state.m_discrepancy_in_records_contents = QStringList {};
  for (QString a_key: the_compare_state.m_are_same_in_content.keys())
    if (the_compare_state.m_are_same_in_content[a_key])
      the_compare_state.m_discrepancy_in_records_contents.append(a_key);
  the_compare_state.m_discrepancy_in_records_contents = CUtils::arrayDiff(the_compare_state.m_compared_tables, the_compare_state.m_discrepancy_in_records_contents);

  CLog::log(dumpCompareState(the_compare_state));
  return {true, the_compare_state};
}

QString DAGStateHandler::dumpCompareState(
  const CompareState& report)
{
  QString out = "";

  if (report.m_addition_keys_in_local.size() > 0)
    out += "\naddition_keys_in_local: " + report.m_addition_keys_in_local.join(", ");

  if (report.m_addition_keys_in_remote.size() > 0)
    out += "\naddition_keys_in_remote: " + report.m_addition_keys_in_remote.join(", ");

  if (report.m_nodes_discrepancies.size() > 0)
    out += "\nnodes_discrepancies: " + report.m_nodes_discrepancies.join("\n");

  if (report.m_discrepancy_in_records_count.size() > 0)
    out += "\ndiscrepancy_in_records_count: " + report.m_discrepancy_in_records_count.join(", ");

  if (report.m_discrepancy_in_records_contents.size() > 0)
    out += "\ndiscrepancy_in_records_contents: " + report.m_discrepancy_in_records_contents.join(", ");

  return out;
}

bool DAGStateHandler::RecordNodeSnapshotInDb(
  const QString& content,
  const QString& name)
{
  QVDicT values {
    {"nss_label", name},
    {"nss_content", BlockUtils::wrapSafeContentForDB(content).content},
    {"nss_creation_date", CUtils::getNow()}};
  bool res = DbModel::insert(
    stbl_nodes_snapshots,
    values);
  CGUI::signalUpdateSnapshots();

  return res;
}

QVDRecordsT DAGStateHandler::listSnapshots()
{
  QueryRes res = DbModel::select(
    stbl_nodes_snapshots,
    {"nss_id", "nss_label", "nss_creation_date"},
    {},
    {{"nss_creation_date", "ASC"}});

  return res.records;
}
