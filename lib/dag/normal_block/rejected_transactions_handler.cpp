#include "stable.h"
#include "rejected_transactions_handler.h"

const QString RejectedTransactionsHandler::stbl_trx_rejected_transactions = "c_trx_rejected_transactions";
const QStringList RejectedTransactionsHandler::stbl_trx_rejected_transactions_fields = {"rt_block_hash", "rt_doc_hash", "rt_coin", "rt_insert_date"};

RejectedTransactionsHandler::RejectedTransactionsHandler()
{

}

QVDRecordsT RejectedTransactionsHandler::searchInRejectedTrx(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int& limit)
{
  QueryRes res = DbModel::select(
    stbl_trx_rejected_transactions,
    fields,
    clauses,
    order,
    limit);

  return res.records;
}

bool RejectedTransactionsHandler::addTransaction(
  const CBlockHashT& block_hash,
  const CDocHashT& doc_hash,
  const CCoinCodeT& coin)
{
  QVDicT values {
    {"rt_block_hash", block_hash},
    {"rt_doc_hash", doc_hash},
    {"rt_coin", coin},
    {"rt_insert_date", CUtils::getNow()}};

  CLog::log("Add a new rejected coin: " + CUtils::dumpIt(values), "trx", "warning");

  DbModel::insert(
    stbl_trx_rejected_transactions,
    values);

  return true;
}
