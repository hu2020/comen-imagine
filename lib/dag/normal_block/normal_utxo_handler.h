#ifndef NORMALUTXOHANDLER_H
#define NORMALUTXOHANDLER_H

class UTXOImportDataContainer;


#include "lib/transactions/basic_transactions/utxo/utxo_handler.h"


class NormalUTXOHandler
{

public:
  NormalUTXOHandler();
  static void loopImportNormalUTXOs();
  static void importNormalBlockUTXOs(QString cDate = "");
  static void doImportUTXOs(QString cDate = "");

  static QVDRecordsT retrieveProperBlocks(QString cDate = "");
};

#endif // NORMALUTXOHANDLER_H
