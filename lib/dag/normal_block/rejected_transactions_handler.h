#ifndef REJECTEDTRANSACTIONSHANDLER_H
#define REJECTEDTRANSACTIONSHANDLER_H


class RejectedTransactionsHandler
{
public:
  RejectedTransactionsHandler();

  const static QString stbl_trx_rejected_transactions;
  const static QStringList stbl_trx_rejected_transactions_fields;

  static QVDRecordsT searchInRejectedTrx(
    const ClausesT& clauses = {},
    const QStringList& fields = stbl_trx_rejected_transactions_fields,
    const OrderT& order = {},
    const int& limit = 0);

  static bool addTransaction(
    const CBlockHashT& block_hash,
    const CDocHashT& doc_hash,
    const CCoinCodeT& coin);

};

#endif // REJECTEDTRANSACTIONSHANDLER_H
