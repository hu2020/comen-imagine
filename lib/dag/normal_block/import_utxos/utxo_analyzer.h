#ifndef UTXOANALYZER_H
#define UTXOANALYZER_H

class UTXOImportDataContainer;

class UTXOAnalyzer
{
public:
  UTXOAnalyzer();

  static void analyzeBlockUsedCoins(
    UTXOImportDataContainer* block_inspect_container,
    const Block* block);

  static void analyzeATransactionCoins(
    UTXOImportDataContainer* block_inspect_container,
    const Block* block,
    const Document* doc);

  static void extractDocImportableUTXOs(
    UTXOImportDataContainer* block_inspect_container,
    const Block* block,
    const Document* doc);

  static void doSusTreatments(
    UTXOImportDataContainer* block_inspect_container,
    const Block* block,
    const Document* doc);

};

#endif // UTXOANALYZER_H
