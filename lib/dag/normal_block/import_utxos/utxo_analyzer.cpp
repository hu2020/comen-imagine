#include "stable.h"

#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"
#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/transactions/basic_transactions/utxo/coin.h"
#include "lib/transactions/basic_transactions/utxo/suspect_trx_handler.h"
#include "lib/dag/normal_block/rejected_transactions_handler.h"


#include "utxo_analyzer.h"

const bool OUTPUT_TIMELOCK_IS_ENABLED = false;    //TODO: develope, teset and release this feature ASAP

UTXOAnalyzer::UTXOAnalyzer()
{

}



void UTXOAnalyzer::extractDocImportableUTXOs(
  UTXOImportDataContainer* block_inspect_container,
  const Block* block,
  const Document* doc)
{
  QString msg = "";
  CLog::log("importing UTXOs from block(" + CUtils::hash8c(block->getBlockHash()) + ") doc(" + doc->m_doc_type +  " / " + CUtils::hash8c(doc->getDocHash()) + ")", "trx", "info");

  if (doc->m_doc_type == CConsts::DOC_TYPES::DPCostPay)
  {
    // the block fee payment transaction always has to have no input and 2 outputs.
    // 1. TP_DP
    // 2. backer fee
    block_inspect_container->m_block_DPCost_treasury = BlockDPCostTreasury {
      "TP_DP",
      "TP_DP Backing fee block(" + CUtils::hash8c(block->getBlockHash()) + ")",
      "Backing fee block(" + CUtils::hash8c(block->getBlockHash()) + ")",
      CUtils::packCoinCode(doc->getDocHash(), 0),
      doc->getOutputs()[0]->m_amount};

    block_inspect_container->m_block_DPCost_backer = BlockDPCostBacker {
      CUtils::packCoinCode(doc->getDocHash(), 1),
      doc->getOutputs()[1]->m_address,
      doc->getOutputs()[1]->m_amount};
    return;
  }

  if (doc->getOutputs().size() == 0)
    return;

  for (COutputIndexT outputIndex = 0; outputIndex < doc->getOutputs().size(); outputIndex++)
  {
    TOutput* anOutput = doc->getOutputs()[outputIndex];
    CCoinCodeT newCoin = "";

    // exclude backerfee from all documents outputs, except the integrated one in which there is also treasury income
    if ((CConsts::TREASURY_PAYMENTS.contains(anOutput->m_address)) && (doc->m_doc_type != CConsts::DOC_TYPES::DPCostPay))
    {
      if (!block_inspect_container->m_block_alter_treasury_incomes.keys().contains(anOutput->m_address))
        block_inspect_container->m_block_alter_treasury_incomes[anOutput->m_address] = {};
      msg = "going to insert a blockAlterTreasuryIncomes(" + anOutput->m_address + ") ";
      CLog::log(msg);
      block_inspect_container->m_block_alter_treasury_incomes[anOutput->m_address].emplace_back(BlockAlterTreasuryIncome {
        doc->getDocHash(),
        CUtils::packCoinCode(doc->getDocHash(), outputIndex),
        anOutput->m_amount
      });
      CLog::log("block_inspect_container.blockAlterTreasuryIncomes " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_block_alter_treasury_incomes), "trx", "trace");

    } else {
      newCoin = CUtils::packCoinCode(doc->getDocHash(), outputIndex);

    }


    // exclude backerfee(s), the one intended for this current backer and potentially other cloned backer's fee
    if (
        (doc->m_doc_type != CConsts::DOC_TYPES::DPCostPay)
        && (doc->getDPIs().size() > 0)
        && doc->getDPIs().contains(outputIndex)
        // && (anOutput[0] == block.backer)
    ) {
      newCoin = "";  //cutCeasedTrxFromUTXOs
    }

    if (newCoin != "")
      block_inspect_container->m_importable_UTXOs.push_back(CCoin {
        newCoin,
        anOutput->m_address,
        anOutput->m_amount,
        block->m_block_creation_date});
  }

  return;

}

void UTXOAnalyzer::doSusTreatments(
  UTXOImportDataContainer* block_inspect_container,
  const Block* block,
  const Document* doc)
{
  CDocHashT doc_hash = doc->getDocHash();
  QStringList considered_sus_coin_codes = block_inspect_container->m_transactions_validity_check[doc_hash].m_susVoteRes.keys();

  ValidityCheck validity = block_inspect_container->m_transactions_validity_check[doc_hash];

  CLog::log("validity response for block(" + CUtils::hash8c(block->getBlockHash()) + ") doc(" + CUtils::hash8c(doc_hash) + ") validity: " + UTXOImportDataContainer::dumpMe(validity), "trx", "info");

  for (CCoinCodeT the_coin: considered_sus_coin_codes)
  {
    if (validity.m_susVoteRes[the_coin].m_valid == true)
    {
      // this input is valid, but by rejecting whole transaction and restoring inputs the input will remain un-spended.
//      block_inspect_container->m_to_be_restored_coins.push_back(the_coin);
      block_inspect_container->m_sus_inputs_detection.emplace_back(SusInputDetection {
        the_coin,
        "valid input of considered sus coins!!!"});
      continue;
    }

    if (validity.m_susVoteRes[the_coin].m_action == "reject")
    {
      // even one rejection or donation of inputs is enough to cut trxFee dfrom blockTrxFee & bloockTreasuryFee
      block_inspect_container->m_must_not_import_trx_outputs.push_back(doc_hash);
      block_inspect_container->m_transactions_detection[doc_hash] = "reject";

      CLog::log("susVotes: Reject coin(" + CUtils::shortCoinRef(the_coin) + ") of transaction(" + CUtils::hash8c(doc_hash) + ") in block(" + CUtils::hash8c(block->getBlockHash()) + ") because of susVotes", "trx", "warning");
      block_inspect_container->m_sus_inputs_detection.emplace_back(SusInputDetection {
        the_coin,
        "Already spended input"
      });

      if (!block_inspect_container->m_rejected_transactions.keys().contains(doc_hash))
        block_inspect_container->m_rejected_transactions[doc_hash] = std::vector<CCoinCodeT> {};

      block_inspect_container->m_rejected_transactions[doc_hash].push_back(the_coin);

      RejectedTransactionsHandler::addTransaction(
        block->getBlockHash(),
        doc_hash,
        the_coin);

      continue;
    }

    if (validity.m_susVoteRes[the_coin].m_action == "donate")
    {
      // even one rejection or donation of inputs is enough to cut trxFee dfrom blockTrxFee & bloockTreasuryFee
      block_inspect_container->m_must_not_import_trx_outputs.push_back(doc_hash);

      CLog::log("susVotes: Donating coin(" + CUtils::shortCoinRef(the_coin) + ") of transaction(" + CUtils::hash8c(doc_hash) + ") in block(" + CUtils::hash8c(block->getBlockHash()) + ") because of susVotes", "trx", "warning");
      // donate conflicted input, and since do not consider transaction, so the other inputs remaine untouched
      std::vector<CCoin> donateRefLocsBlocks = DAG::retrieveBlocksInWhichARefLocHaveBeenProduced(the_coin);

      CLog::log("Donate Transaction Input. blocks by the_coin:(" + CUtils::shortCoinRef(the_coin) + ") of transaction(" + CUtils::hash8c(doc_hash) + ") in block(" + CUtils::hash8c(block->getBlockHash()) + ") because of susVotes: " + UTXOImportDataContainer::dumpMe(donateRefLocsBlocks), "trx", "warning");
      // big FIXME: for cloning transactions issue
      block_inspect_container->m_block_treasury_logs.emplace_back(BlockTreasuryLog {
        "Donate because of DOUBLE-SPENDING",
        "TP_DONATE_DOUBLE_SPEND",
        "Pay to treasury because of trx conflict in block(" + CUtils::hash8c(block->getBlockHash()) + ")  transaction(" + CUtils::hash8c(doc_hash) +")",
        the_coin,
        donateRefLocsBlocks[0].m_amount, // outputValue
        donateRefLocsBlocks});

      block_inspect_container->m_sus_inputs_detection.emplace_back(SusInputDetection {
        the_coin,
        "donate input"});

      if (!block_inspect_container->m_rejected_transactions.keys().contains(doc_hash))
        block_inspect_container->m_rejected_transactions[doc_hash] = {};
      block_inspect_container->m_rejected_transactions[doc_hash].push_back(the_coin);

      // record rejected transaction
      RejectedTransactionsHandler::addTransaction(
        block->getBlockHash(),
        doc_hash,
        the_coin);

    } else {
      CUtils::exiter("\n\n\nstrange situation in sus voting", 98);
    }

  }

  // the entire UTXOs of a block during adding block to DAG are removed from trx_utxos
  // so here we have to resotore used inocent UTXOs of this rejected-transaction or donated-transaction into spendable UTXOs,
  // (except conflicted the_coins)
  for (TInput* input: doc->getInputs())
  {
    CCoinCodeT a_coin = input->getCoinCode();
    if (!considered_sus_coin_codes.contains(a_coin))
    {
      QV2DicT refLocBlock = DAG::getCoinsGenerationInfoViaSQL({a_coin});
      CCoin tmp_coin {
        a_coin,
        refLocBlock[a_coin].value("coinGenOutputAddress").toString(),
        static_cast<CMPAIValueT>(refLocBlock[a_coin].value("coinGenOutputValue").toDouble()),
        refLocBlock[a_coin].value("coinGenCreationDate").toString(),
        block->getBlockHash()};
      tmp_coin.m_cycle = refLocBlock[a_coin].value("coinGenCycle").toString();
      block_inspect_container->m_to_be_restored_coins.push_back(tmp_coin);

      block_inspect_container->m_sus_inputs_detection.emplace_back(SusInputDetection {
        a_coin,
        "valid input"});
    }
  }

  return;
}

void UTXOAnalyzer::analyzeATransactionCoins(
  UTXOImportDataContainer* block_inspect_container,
  const Block* block,
  const Document* doc)
{
  QString msg = "";
  CDocHashT doc_hash = doc->getDocHash();


  /**
  * find DPCost payments, to not importing in UTXOs
  * and also in case of doublespending removing these incomes from blockTotalTrxFee & treauryTotalTrxFeeIncome
  */

  for (auto inx: doc->getDPIs())
  {
    if ((doc->m_doc_type != CConsts::DOC_TYPES::DPCostPay) && (doc->getOutputs()[inx]->m_address == block->getBacker()))
    {
      block_inspect_container->m_a_single_trx_DPCost[doc_hash] = SingleTrxDPCost {
        CUtils::packCoinCode(doc_hash, inx),
        doc->getOutputs()[inx]->m_address,
        doc->getOutputs()[inx]->m_amount,
        block->m_block_creation_date
      };
    }
  }

  block_inspect_container->m_transactions_detection[doc_hash] = "Normal";

  if (OUTPUT_TIMELOCK_IS_ENABLED)
  {

    //    // retrieve latest redeemTimes(if exist)
    //    let { docInputs, docMaxRedeem } = outputTimeLockHandler.getDocInputsAndMaxRedeem({
    //        doc,
    //        docExtInfo
    //    });
    //    let isOutputTimeLockedRelatedDoc = outputTimeLockHandler.isTimeLockRelated(docInputs);
    //    if (isOutputTimeLockedRelatedDoc)
    //        block_inspect_container.oTimeLockedRelatedDocs[doc.hash] = outputTimeLockHandler.isTimeLockRelated(docInputs);
    //    clog.trx.info(`doc(${utils.hash6c(doc.hash)}) is time Locked Related Doc(${isOutputTimeLockedRelatedDoc}) inputs(${docInputs.map(x => iutils.shortCoinRef(x))})`);
    //    console.log(`doc(${utils.hash6c(doc.hash)}) is time Locked Related Doc(${isOutputTimeLockedRelatedDoc}) inputs(${docInputs.map(x => iutils.shortCoinRef(x))})`);
    //    if ((0 < docMaxRedeem) || isOutputTimeLockedRelatedDoc) {
    //        // save redeem info to apply it on right time, and return
    //        let refLocBlocks = dagHandler.getCoinsGenerationInfoViaSQL(docInputs);
    //        for (let refLoc of docInputs) {
    //            block_inspect_container.timeLockedDocs.push({
    //                blockHash: block.blockHash,
    //                docHash: doc.hash,
    //                pureHash: trxHashHandler.getPureHash(doc),
    //                refLoc,
    //                doc,
    //                redeemTime: utils.minutesAfter(docMaxRedeem + iConsts.getCycleByMinutes(), block.creation Date),
    //                docMaxRedeem,
    //                cloneCode: block.cycle,
    //                refCreation Date: refLocBlocks[refLoc].coinGenCreation Date
    //            });
    //        }
    //        let redeemTime = utils.minutesAfter(docMaxRedeem + iConsts.getCycleByMinutes(), block.creation Date);
    //        console.log(`doc(${utils.hash6c(doc.hash)}) created On(${block.creation Date}) MaxRedeem: ${utils.stringify(docMaxRedeem)} redeemTime(${redeemTime})`);
    //        clog.trx.info(`doc(${utils.hash6c(doc.hash)}) created On(${block.creation Date}) MaxRedeem: ${utils.stringify(docMaxRedeem)} redeemTime(${redeemTime})`);

    //        // do nothing more with this document
    //        block_inspect_container.transactionsDetection[doc.hash] = 'timeLocked';
    //        return;
    //    }
  }

  if (!block_inspect_container->m_block_is_sus_case)
  {
    block_inspect_container->m_can_import_normally = true;
    extractDocImportableUTXOs(
      block_inspect_container,
      block,
      doc);

    return;
  }



  // control if there is atleast one susVote for this document?
  auto[all_coins_are_valid, raw_votes] = SuspectTrxHandler::getSusInfoByDocHash(doc_hash);
  if (all_coins_are_valid)
  {
    msg = "doc(" + CUtils::hash8c(doc_hash) + ") of block(" + CUtils::hash8c(block->getBlockHash()) + ") ";
    msg += "is not sus (even if block is blockIsSusCase) allCoinsOfDocAreValid";
    CLog::log(msg, "trx", "info");
    block_inspect_container->m_can_import_normally = true;
    block_inspect_container->m_transactions_detection[doc_hash] = "There is no sus vote for doc";
    extractDocImportableUTXOs(
      block_inspect_container,
      block,
      doc);

    return;
  }

  auto[status, updated_raw_votes] = SuspectTrxHandler::retrieveVoterPercentages(raw_votes);
  Q_UNUSED(status);
  block_inspect_container->m_raw_votes = updated_raw_votes;

  // analyze votes and decide about dobiuos transactions
  SuspectTrxHandler::checkDocValidity(
    block_inspect_container,
    doc_hash,
    true);
//  block_inspect_container->m_transactions_validity_check[doc_hash] = validity;

  block_inspect_container->m_can_import_normally = false;
  if (block_inspect_container->m_transactions_validity_check[doc_hash].m_cloned == "cloned")
  {
    // the document/transaction is cloned, so can import outputs regularely
    CLog::log("recognized a cloned trx(${utils.hash6c(doc.hash)}) in block(${utils.hash6c(block.blockHash)", "trx", "trace");
    block_inspect_container->m_can_import_normally = true;
  }

  if ((block_inspect_container->m_transactions_validity_check[doc_hash].m_susVoteRes.keys().size() == 1) &&
    (block_inspect_container->m_transactions_validity_check[doc_hash].m_susVoteRes[block_inspect_container->m_transactions_validity_check[doc_hash].m_susVoteRes.keys()[0]].m_valid == true))
  {
    CLog::log("Only one sus-input exist and it is valid trx(" + CUtils::hash8c(doc_hash) + ") in block(" + CUtils::hash8c(block->getBlockHash()) + ") ", "trx", "info");
    block_inspect_container->m_can_import_normally = true;
    block_inspect_container->m_transactions_detection[doc_hash] = "Only one sus-input exist and it is valid";
  }

  if (block_inspect_container->m_can_import_normally)
  {
    // so doc can be imported normally
    extractDocImportableUTXOs(
      block_inspect_container,
      block,
      doc);
    return;
  }

  // transaction needs special treatment.
  // no out put of this transaction will be imported to UTXOs
  // depends on vote result, transaction inputs can be
  // 1. donated to treasury,
  // 2. restored in UTXOs,
  // 3. denied
  doSusTreatments(
    block_inspect_container,
    block,
    doc);
  return;
}


void UTXOAnalyzer::analyzeBlockUsedCoins(
  UTXOImportDataContainer* block_inspect_container,
  const Block* block)
{
  QString msg;
  // UTXO Import Module Common Data Container

  // retrieve extinfo
  auto[status_extBInfo, stat_ext_exist, bExtInfo] = block->getBlockExtInfo();
  Q_UNUSED(status_extBInfo);
  Q_UNUSED(stat_ext_exist);
  Q_UNUSED(bExtInfo);

  //  if (bExtInfoRes.bExtInfo == null)
  //  {
  //    msg = `missed bExtInfo5 (${utils.hash16c(block.blockHash)})`;
  //    clog.sec.error(msg);
  //    return { err: true, msg }
  //  }
  //  let bExtInfo = bExtInfoRes.bExtInfo

  // FIXME: is it possible to code reach here with a normal block while there is no entry in sus records?
  // in this case normal block's output will be recorded as spendable outputs!!!!
  auto[has_sus_records, votes_dict] = SuspectTrxHandler::getSusInfoByBlockHash(block->getBlockHash());  // susVotesByBlockHash =
  block_inspect_container->m_block_is_sus_case = has_sus_records;
  block_inspect_container->m_votes_dict = votes_dict;

  CLog::log("extract UTXOs from block(" + CUtils::hash8c(block->getBlockHash()) + ") block is sus case(" + CUtils::dumpIt(block_inspect_container->m_block_is_sus_case) + ")", "trx", "info");

  // control shares
  auto[shareCount_, huPercent] = DNAHandler::getAnAddressShares(CConsts::HU_DNA_SHARE_ADDRESS, block->m_block_creation_date);
  Q_UNUSED(shareCount_);
  // whenever comunity riched 29 percent of voting power, so they will not more hu's voting in susBlock consideration
  if ((huPercent < 71) && (100 - huPercent < CConsts::MINIMUM_SUS_VOTES_TO_ALLOW_CONSIDERING_SUS_BLOCK))
  {
    block_inspect_container->m_minimum_floating_vote = 100 - huPercent;
  }

  if (block_inspect_container->m_block_is_sus_case)
  {
    CLog::log("block_inspect_container->susVotesByBlockHash: " + block_inspect_container->dumpMe(), "sec", "error");
    if (!block_inspect_container->m_votes_dict.keys().contains(block->getBlockHash()))
    {
      msg = "The sus block(" + CUtils::hash8c(block->getBlockHash()) + ") hasn't even one vote! so has to wait to more susVotes to be inserted";
      CLog::log(msg, "trx", "error");
      block_inspect_container->m_does_enough_sus_votes_exist = "notEnoughSusVotesExist";
      return;
    }

    block_inspect_container->m_current_votes_percentage = block_inspect_container->m_votes_dict[block->getBlockHash()].m_sumPercent;
    if (block_inspect_container->m_current_votes_percentage < block_inspect_container->m_minimum_floating_vote)
    {
      // the machine has to wait to enought susVotes to be inserted
      msg = "the sus block(" + CUtils::hash8c(block->getBlockHash()) + ") has " + QString::number(block_inspect_container->m_votes_dict[block->getBlockHash()].m_sumPercent) + " percents, so the machine has to wait to more susVotes to be inserted";
      CLog::log(msg, "trx", "info");
      block_inspect_container->m_does_enough_sus_votes_exist = "notEnoughSusVotesExist";
      return;
    }

    block_inspect_container->m_does_enough_sus_votes_exist = "Yes";
    CLog::log("the block(" + CUtils::hash8c(block->getBlockHash()) + ") discovered as an suspiciuos, cloned or P4P transaction", "trx", "warning");
  }


  // retrieve p4p supported transactions
  for (Document* aDoc: block->getDocuments())
  {
    if (aDoc->m_doc_class == CConsts::TRX_CLASSES::P4P)
      block_inspect_container->m_P4P_docs.push_back(aDoc);
  }
  // retrieve org ceased backer fee outputs
  if (block_inspect_container->m_P4P_docs.size() > 0)
  {
    for (Document* aDoc: block_inspect_container->m_P4P_docs)
    {
      for (COutputIndexT outInx: aDoc->getDPIs())
      {
        if (aDoc->getOutputs()[outInx]->m_address != block->getBacker())
        {
          /**
           * this output of orginal ceased transaction must not be imported in UTXOs because of taking place in this block
           * cut From Importable UTXOs Because OF Referenced Ceased Trx
           **/
          block_inspect_container->m_cut_ceased_trx_from_UTXOs.push_back(CUtils::packCoinCode(aDoc->getDocHash(), outInx));
        }
      }
    }
  }


  // looping on documents in a block
  for (CDocIndexT docInx = 0; docInx < block->getDocuments().size(); docInx++)
  {
    Document* aDoc = block->getDocuments()[docInx];

    if (aDoc->getRef() != "")
    {
      if (aDoc->isBasicTransaction())
      {
        block_inspect_container->m_trx_U_dict[aDoc->getDocHash()] = aDoc;
        block_inspect_container->m_map_U_trx_hash_to_trx_ref[aDoc->getDocHash()] = aDoc->getRef();
        block_inspect_container->m_map_U_trx_ref_to_trx_hash[aDoc->getRef()] = aDoc->getDocHash();
      } else {
        block_inspect_container->m_map_U_referencer_to_referenced[aDoc->getDocHash()] = aDoc->getRef();
        block_inspect_container->m_map_U_referenced_to_referencer[aDoc->getRef()] = aDoc->getDocHash();
      }
    }

    if (!aDoc->isBasicTransaction() && !aDoc->isDPCostPayment())
        continue;

//    let docExtInfo = _.has(bExtInfo, docInx) ? bExtInfo[docInx] : null;
    analyzeATransactionCoins(
      block_inspect_container,
      block,
      aDoc);
  }

  /**
  * since each doc type need different treatment, so maybe it is not good to have a general controller.
  * here we can only remove not-payed-docs from blockAlterTreasuryIncomes
  * control if all documents costs are paying by a transaction?
  **/
  CLog::log("block_inspect_container->blockAlterTreasuryIncomes333: " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_block_alter_treasury_incomes), "trx", "trace");
  for (QString aDocType: block_inspect_container->m_block_alter_treasury_incomes.keys())
  {
    for (auto aDoc: block_inspect_container->m_block_alter_treasury_incomes[aDocType])
    {
    }
  }

  return;

}
