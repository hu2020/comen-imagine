#ifndef UTXOIMPORTDATACONTAINER_H
#define UTXOIMPORTDATACONTAINER_H

#include "lib/transactions/basic_transactions/utxo/coin.h"

class BasicTxDocument;

class BlockDPCostTreasury
{
public:
  QString m_cat = "";
  QString m_title = "";
  QString m_descriptions = "";
  CCoinCodeT m_coin = ""; // refLoc: iutils.packCoinCode(doc.hash, 0),
  CMPAIValueT m_value = 0;

  void reset();
};


class BlockDPCostBacker
{
public:
  CCoinCodeT m_coin = ""; // refLoc: iutils.packCoinCode(doc.hash, 1),
  CAddressT m_address = "";
  CMPAIValueT m_value = 0;

  void reset();
};

class BlockTreasuryLog
{
public:
   QString m_title = "";
   QString m_cat = "";
   QString m_descriptions = "";
   CCoinCodeT m_coin = ""; // refLoc,
   CMPAIValueT m_value = 0;
   std::vector<CCoin> m_donateRefLocsBlocks {};
};

class BlockAlterTreasuryIncome
{
public:
  CDocHashT m_trx_hash = ""; // TPTrxHash: doc.hash,
  CCoinCodeT m_coin = ""; // TPRefLoc: iutils.packCoinCode(doc.hash, parseInt(outputIndex)),
  CMPAIValueT m_value = 0;  // TPValue: iutils.convertBigIntToJSInt(anOutput[1])
};

class SingleTrxDPCost
{
public:
  CCoinCodeT m_coin = ""; // refLoc: iutils.packCoinCode(doc.hash, parseInt(inx)),
  CAddressT m_address = ""; //address: doc.outputs[inx][0],
  CMPAIValueT m_value = 0;  // value: iutils.convertBigIntToJSInt(doc.outputs[inx][1]),
  CDateT m_ref_creation_date = "";
};

class TimeLockedDoc
{
public:
  CBlockHashT m_block_hash = "";
  CDocHashT m_doc_hash = "";
  CDocHashT m_doc_pure_hash = "";
  CCoinCodeT m_coin = ""; // refLoc,
  BasicTxDocument* m_doc = nullptr;
  CDateT m_redeem_time = "";
  uint64_t m_doc_max_redeem = 0;
  QString m_clone_code = "";
  CDateT m_ref_creation_date = "";
};

class RawVote
{
public:
  RawVote(const QVDicT& a_record);
  // are extracted from table trx_suspect_transactions
  CAddressT m_voter = "";
  CDateT m_vote_date = "";
  CCoinCodeT m_voting_coin = "";
  CBlockHashT m_logger_block = "";
  CBlockHashT m_spender_block = "";
  CDocHashT m_spender_doc = "";
  uint32_t m_receive_order = 0;
  CDateT m_spend_date = "";

  DNASharePercentT m_voterPercentage = 0.0; // which is calculated

};

class SBSCDS // Spender Block's Stated Creation Date Structure
{
public:
  CAddressT m_spdDate = "";
  CDocHashT m_spdDoc = "";
};

class CoinVoterDocInfo
{
public:
  CDateT m_coinSpendDate = "";
  uint32_t m_spend_receive_order = 0; // order of receiving spends for same refLoc
  DNASharePercentT m_voterPercentage = 0.0;
  CDateT m_voteDate = "";
};

class FirstSpenderInfo
{
public:
  CDocHashT m_spDoc = "";
  CDateT m_spTime = "";
}; // will be used to recognize if the 2 usage of coins have less than 6 hours different or not

class CoinVoterInfo
{
public:
  QHash<QString, SBSCDS> m_spendOIBSBSCD {};  //spend Orders Info By Spender Block's Stated Creation Date
  QHash<CDocHashT, CoinVoterDocInfo> m_docsInfo {};
  QHash<QString, CDocHashT> m_rOrders {};
//  QStringList m_spendTimes {};
//  FirstSpenderInfo m_firstSpenderInfo {}; // will be used to recognize if the 2 usage of coins have less than 6 hours different or not
  bool m_spendsLessThan6HNew = false;
  bool m_spendsLessThan6H = false;
  bool m_canControlLess6Condition = false;
};

class VoteData
{
public:
  CDocHashT m_docHash = "";
  uint64_t m_inside6VotersCount = 0;
  double m_inside6VotesGain = 0;
  uint64_t m_outside6VotersCount = 0;
  double m_outside6VotesGain = 0;
  double m_voteGain = 0.0;
  QHash<CAddressT, CoinVoterDocInfo> m_details {};
};

class CoinOrderedSpender
{
public:
  VoteData m_voteData {};
  std::vector<CoinVoterDocInfo> m_docs;
};

class InOutside6hElm
{
public:
  CDocHashT m_docHash = "";
  double m_votes = 0.0;
  uint64_t m_voters = 0;  // voters count
};

class CoinAndPosition
{
public:
  std::vector<InOutside6hElm> m_inside6h {};
  std::vector<InOutside6hElm> m_outside6h {};
  float m_insideTotal = 0.0;
  float m_outsideTotal = 0.0;
};


class SusVote
{
public:
  bool m_valid = false;
  QString m_action = "";
  uint64_t m_voters = 0;
  double m_votes = 0.0;
};

class ValidityCheck
{
public:
  std::vector<RawVote> m_votes_dict;
  QHash<CCoinCodeT, QHash<CAddressT, CoinVoterInfo> > m_coinsAndVotersDict {};
  QHash<CCoinCodeT, QHash<uint32_t, QHash<CDocHashT, CoinOrderedSpender> > > m_coinsAndOrderedSpendersDict {};
  QHash<CCoinCodeT, QHash<uint32_t, CoinAndPosition> > m_coinsAndPositionsDict;
  QHash<CCoinCodeT, SusVote> m_susVoteRes;
  bool m_valid = false;
  QString m_cloned = "";

};

class SusInputDetection
{
public:
  CCoinCodeT m_coin = ""; // refLoc: iutils.shortCoinRef(refLoc),
  QString m_detection = "";
};

class TheVote
{
public:
   CAddressT m_voter = "";
   DNASharePercentT m_sharesPercent = 0.0;
};

class TheVotes
{
public:
  std::vector<TheVote> m_votes {};
  DNASharePercentT m_sumPercent = 0.0;
};

class CostPaymentStatus
{
public:
  QString m_message = "";
  bool m_is_payed = true;
};



class UTXOImportDataContainer
{
public:
  UTXOImportDataContainer();

  bool m_block_is_sus_case = false;
  bool m_can_import_normally = false;
  QString m_does_enough_sus_votes_exist = "";
  DNASharePercentT m_current_votes_percentage = 0.0;
  std::vector<RawVote> m_raw_votes {};
  CMPAIValueT m_to_cut_from_backer_fee = 0;
  CMPAISValueT m_block_DPCost_backer_final = 0;
  CMPAIValueT m_to_cut_from_treasury_fee = 0;
  CMPAIValueT m_block_DPCost_treasury_final = 0;
  bool m_block_has_income = false;

  QHash<CBlockHashT, TheVotes> m_votes_dict {};
  DNASharePercentT m_minimum_floating_vote = CConsts::MINIMUM_SUS_VOTES_TO_ALLOW_CONSIDERING_SUS_BLOCK;

  std::vector<CCoin> m_importable_UTXOs {};
  std::vector<CCoinCodeT> m_cut_ceased_trx_from_UTXOs {};
  std::vector<CDocHashT> m_supported_P4P {};

  BlockDPCostTreasury m_block_DPCost_treasury;
  BlockDPCostBacker m_block_DPCost_backer;
  std::vector<BlockTreasuryLog> m_block_treasury_logs {};

  std::vector<Document*> m_P4P_docs {};

  QHash<CAddressT, std::vector<BlockAlterTreasuryIncome> > m_block_alter_treasury_incomes {};

  QHash<CDocHashT, Document*> m_trx_U_dict {};
  QHash<CDocHashT, CDocHashT> m_map_U_trx_ref_to_trx_hash {};
  QHash<CDocHashT, CDocHashT> m_map_U_trx_hash_to_trx_ref {};

  QHash<CDocHashT, SingleTrxDPCost> m_a_single_trx_DPCost {};
  std::vector<CCoinCodeT> m_DPCost_coin_codes {};
  std::vector<CCoin> m_to_be_restored_coins {};
  std::vector<TimeLockedDoc> m_time_locked_docs {};

  std::vector<CDocHashT> m_must_not_import_trx_outputs {};    // because trx is rejected or donated for double-spending
  QHash<CDocHashT, QString> m_transactions_detection {};

  QHash<CDocHashT, ValidityCheck> m_transactions_validity_check {};

  std::vector<SusInputDetection> m_sus_inputs_detection {};
  QHash<CDocHashT, std::vector<CCoinCodeT> > m_rejected_transactions {};   // for each input refLoc must be inserted on record(even in one same transaction)

  QHash<CDocHashT, CDocHashT> m_map_U_referencer_to_referenced {};
  QHash<CDocHashT, CDocHashT> m_map_U_referenced_to_referencer {};

  QHash<CDocHashT, bool> m_output_time_locked_related_docs {};    // TODO: implemet it ASAP

  typedef QString document_type;
  QHash<document_type, QHash<CDocHashT, CostPaymentStatus> > m_cost_payment_status;

  void reset();

  QString dumpMe();
  static QString dumpCoinDetails(const CCoin& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(std::vector<QString> value,  const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<QString, std::vector<QString> > value,  const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<QString, QString> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(std::vector<CCoin> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const SusVote& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const RawVote& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(std::vector<RawVote> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const SingleTrxDPCost& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CCoinCodeT, SusVote> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const SBSCDS& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const CoinVoterInfo& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(std::vector<CoinVoterDocInfo> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CDocHashT, SingleTrxDPCost> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const CoinVoterDocInfo& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CAddressT, CoinVoterDocInfo> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const SusInputDetection& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const VoteData& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const BlockAlterTreasuryIncome& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(std::vector<BlockAlterTreasuryIncome> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const FirstSpenderInfo& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CCoinCodeT, QHash<CAddressT, CoinVoterInfo> > value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const ValidityCheck& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const InOutside6hElm& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const std::vector<InOutside6hElm> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const CoinAndPosition& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CCoinCodeT, QHash<uint32_t, CoinAndPosition> > value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const CoinOrderedSpender& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CDocHashT, CoinOrderedSpender> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CAddressT, std::vector<BlockAlterTreasuryIncome> > value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CCoinCodeT, QHash<uint32_t, QHash<CDocHashT, CoinOrderedSpender> > > value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const BlockDPCostBacker& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const BlockDPCostTreasury& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(std::vector<BlockTreasuryLog> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const BlockTreasuryLog& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(std::vector<SusInputDetection> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(QHash<CDocHashT, ValidityCheck> value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpMe(const CostPaymentStatus& value, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
};

#endif // UTXOIMPORTDATACONTAINER_H
