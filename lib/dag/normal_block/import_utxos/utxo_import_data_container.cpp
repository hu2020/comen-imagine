#include "stable.h"

#include "lib/block/document_types/document.h"
#include "lib/block/document_types/basic_tx_document.h"
#include "lib/transactions/basic_transactions/utxo/coin.h"

#include "utxo_import_data_container.h"


UTXOImportDataContainer::UTXOImportDataContainer()
{

}


RawVote::RawVote(const QVDicT& a_record)
{
  m_voting_coin = a_record.value("the_coin").toString();
  m_voter = a_record.value("st_voter").toString();
  m_vote_date = a_record.value("st_vote_date").toString();
  m_logger_block = a_record.value("st_logger_block").toString();
  m_spender_block = a_record.value("st_spender_block").toString();
  m_spender_doc = a_record.value("st_spender_doc").toString();
  m_receive_order = a_record.value("st_receive_order").toUInt();
  m_spend_date = a_record.value("st_spend_date").toString();
  if(a_record.keys().contains("voterPercentage"))
    m_voterPercentage = a_record.value("voterPercentage").toFloat();
}

void BlockDPCostTreasury::reset()
{
  m_cat = "";
  m_title = "";
  m_descriptions = "";
  m_coin = ""; // refLoc: iutils.packCoinCode(doc.hash, 0),
  m_value = 0;
}

void BlockDPCostBacker::reset()
{
  m_coin = ""; // refLoc: iutils.packCoinCode(doc.hash, 1),
  m_address = "";
  m_value = 0;
}

void UTXOImportDataContainer::reset()
{
  m_block_is_sus_case = false;
  m_can_import_normally = false;
  m_does_enough_sus_votes_exist = "";
  m_current_votes_percentage = 0.0;
  m_raw_votes = {};
  m_to_cut_from_backer_fee = 0;
  m_block_DPCost_backer_final = 0;
  m_to_cut_from_treasury_fee = 0;
  m_block_DPCost_treasury_final = 0;
  m_block_has_income = false;
  m_votes_dict = {};
  m_minimum_floating_vote = CConsts::MINIMUM_SUS_VOTES_TO_ALLOW_CONSIDERING_SUS_BLOCK;
  m_importable_UTXOs = {};
  m_cut_ceased_trx_from_UTXOs = {};
  m_supported_P4P = {};

  m_block_DPCost_treasury.reset();

  m_block_DPCost_backer.reset();
  m_block_treasury_logs = {};

  m_P4P_docs = {};

  m_block_alter_treasury_incomes = {};

  m_trx_U_dict = {};
  m_map_U_trx_ref_to_trx_hash = {};
  m_map_U_trx_hash_to_trx_ref = {};

  m_a_single_trx_DPCost = {};
  m_DPCost_coin_codes = {};
  m_to_be_restored_coins = {};
  m_time_locked_docs = {};

  m_must_not_import_trx_outputs = {};    // because trx is rejected or donated for double-spending
  m_transactions_detection = {};

  m_transactions_validity_check = {};

  m_sus_inputs_detection = {};
  m_rejected_transactions = {};   // for each input refLoc must be inserted on record(even in one same transaction)

  m_map_U_referencer_to_referenced = {};
  m_map_U_referenced_to_referencer = {};

  m_output_time_locked_related_docs = {};    // TODO: implemet it ASAP

  m_cost_payment_status = {};


  return;
}

QString UTXOImportDataContainer::dumpCoinDetails(const CCoin& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "Coin Details: ";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "coin: " + value.m_coin;
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "value: " + CUtils::microPAIToPAI6(value.m_amount) + " PAIs";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "owner: " + value.m_owner;

  if (value.m_creation_date != "")
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "creation date: " + value.m_creation_date;

  if (value.m_block_hash != "")
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "block: " + CUtils::hash8c(value.m_block_hash);

  if (value.m_doc_hash != "")
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "doc: " + CUtils::hash8c(value.m_doc_hash) + " index: " + value.m_doc_index + " output: " + value.m_output_index;

  return out;
}

QString UTXOImportDataContainer::dumpMe(std::vector<CCoin> value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "Coins Details(" + QString::number(value.size()) + " coin): ";
  for(uint64_t inx = 0; inx < value.size(); inx++)
  {
    out += CConsts::NL + prefix_tabs + dumpCoinDetails(value[inx], CConsts::DUMPER_INDENT + QString::number(inx) + ". ");
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(const SusVote& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "A Sus Vote: ";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Is Valid: " + CUtils::dumpIt(value.m_valid);
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Action: " + value.m_action;
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Voters Count: " + QString::number(value.m_voters);
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Vote Rank: " + QString::number(value.m_votes);
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CCoinCodeT, SusVote> value, const QString& prefix_tabs)
{
  QStringList coins = value.keys();
  coins.sort();

  QString out = CConsts::NL + prefix_tabs + "The Coins(" + QString::number(coins.size()) + " coin): ";
  for(int64_t coin_inx = 0; coin_inx < coins.size(); coin_inx++)
  {
    out += CConsts::NL + prefix_tabs + dumpMe(value[coins[coin_inx]], CConsts::DUMPER_INDENT + QString::number(coin_inx) + ". coin(" + CUtils::shortCoinRef(coins[coin_inx]) + ") ");
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(const SBSCDS& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "an spend: date(" + value.m_spdDate + " doc(" + CUtils::hash8c(value.m_spdDoc) + ")";
  return out;
}

QString UTXOImportDataContainer::dumpMe(const FirstSpenderInfo& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "First Spending Date/Loc: " + value.m_spTime + " doc(" + CUtils::hash8c(value.m_spDoc) + ")";
  return out;
}

QString UTXOImportDataContainer::dumpMe(const CoinVoterDocInfo& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "voter(" + QString::number(value.m_voterPercentage) + " %) ";
  out += "vote date(" + value.m_voteDate + ") ";
  out += "spend receive order(" + QString::number(value.m_spend_receive_order) + ") ";
  out += "spend date(" + value.m_coinSpendDate + ") ";
  return out;
}

QString UTXOImportDataContainer::dumpMe(const CoinVoterInfo& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "Coin Voters Info: ";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "m_spendsLessThan6HNew: " + CUtils::dumpIt(value.m_spendsLessThan6HNew);
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "m_spendsLessThan6H: " + CUtils::dumpIt(value.m_spendsLessThan6H);
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "m_canControlLess6Condition: " + CUtils::dumpIt(value.m_canControlLess6Condition);
//  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "First Spender Info: " + dumpMe(value.m_firstSpenderInfo);

  if (value.m_spendOIBSBSCD.keys().size() > 0)
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Spender Times: " + value.m_spendOIBSBSCD.keys().join(", ");

  if (value.m_rOrders.keys().size() > 0)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Ordered key be receive time";
    for(QString a_key: value.m_rOrders.keys())
    {
      out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + a_key + ": " + CUtils::hash8c(value.m_rOrders[a_key]);
    }
  }

  QStringList docs_hash = value.m_docsInfo.keys();
  if (docs_hash.size() > 0)
  {
    docs_hash.sort();
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Voter's Report:";
    for(CDocHashT a_doc: docs_hash)
    {
      out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + dumpMe(value.m_docsInfo[a_doc]);
    }
  }

  QStringList cus_keys = value.m_spendOIBSBSCD.keys();
  if (cus_keys.size() > 0)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Orders Info By Spender Block's Stated Creation Date:";
    for(CDocHashT a_key: cus_keys)
    {
      out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + dumpMe(value.m_spendOIBSBSCD[a_key]);
    }
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CCoinCodeT, QHash<CAddressT, CoinVoterInfo> > value, const QString& prefix_tabs)
{
  QStringList coins = value.keys();
  coins.sort();

  QString out = CConsts::NL + prefix_tabs + "The Coins(" + QString::number(coins.size()) + " coin): ";
  for(int64_t coin_inx = 0; coin_inx < coins.size(); coin_inx++)
  {
    QStringList owners = value[coins[coin_inx]].keys();
    owners.sort();
    for(int64_t owner_inx = 0; owner_inx < owners.size(); owner_inx++)
    {
      out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + dumpMe(value[coins[coin_inx]][owners[owner_inx]], CConsts::DUMPER_INDENT + QString::number(owner_inx) + ". owner(" + CUtils::shortBech16(owners[owner_inx]) + ") ");
    }
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(const ValidityCheck& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "Validity Check: ";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Is Valid: " + CUtils::dumpIt(value.m_valid);
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Is Cloned: " + value.m_cloned;

  if (value.m_susVoteRes.keys().size() > 0)
    out += CConsts::NL + prefix_tabs + "Sus Votes: " + dumpMe(value.m_susVoteRes, CConsts::DUMPER_INDENT);

  if (value.m_coinsAndVotersDict.keys().size() > 0)
    out += CConsts::NL + prefix_tabs + "Coins And Voters: " + dumpMe(value.m_coinsAndVotersDict, CConsts::DUMPER_INDENT);

  return out;
}

QString UTXOImportDataContainer::dumpMe(const InOutside6hElm& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "voting: doc(" + CUtils::hash8c(value.m_docHash) + ") vote(" + QString::number(value.m_votes) + ") voters(" + QString::number(value.m_voters) + ") ";
  return out;
}

QString UTXOImportDataContainer::dumpMe(const std::vector<InOutside6hElm> value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "voting in/out: " + QString::number(value.size()) + " vote ";
  for (InOutside6hElm a_doc_vote_sum: value)
  {
    out += CConsts::NL + prefix_tabs + dumpMe(a_doc_vote_sum) + "";
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(const CoinAndPosition& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "Positions Inside Totals:" + QString::number(value.m_insideTotal) + "";
  out += CConsts::NL + prefix_tabs + "Positions Outside Totals:" + QString::number(value.m_outsideTotal) + "";
  out += CConsts::NL + prefix_tabs + "Inside Details:" + dumpMe(value.m_inside6h) + "";
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CCoinCodeT, QHash<uint32_t, CoinAndPosition> > value, const QString& prefix_tabs)
{
  QStringList coins = value.keys();
  coins.sort();

  QString out = CConsts::NL + prefix_tabs + "Coins And Positions for " + QString::number(coins.size()) + " coin: ";
  for(int64_t coin_inx = 0; coin_inx < coins.size(); coin_inx++)
  {
    QList<uint32_t> positions = value[coins[coin_inx]].keys();
//    owners.sort();
    for(int64_t pos_inx = 0; pos_inx < positions.size(); pos_inx++)
    {
      out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " pos(" + CUtils::hash8c(coins[coin_inx]) + ") pos(" + QString::number(pos_inx);
      out += dumpMe(value[coins[coin_inx]][positions[pos_inx]], CConsts::DUMPER_INDENT);
    }
  }

  return out;
}

QString UTXOImportDataContainer::dumpMe(const BlockAlterTreasuryIncome& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Trx(" + CUtils::hash8c(value.m_trx_hash) + ") value(" + CUtils::microPAIToPAI6(value.m_value) + ") coin(" + CUtils::shortCoinRef(value.m_coin) + ") ";
  return out;
}

QString UTXOImportDataContainer::dumpMe(std::vector<BlockAlterTreasuryIncome> value, const QString& prefix_tabs)
{
  QString out = "";
  for(uint64_t inx = 0; inx < value.size(); inx++)
  {
    out += dumpMe(value[inx], CConsts::DUMPER_INDENT + prefix_tabs + QString::number(inx) + ". ");
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CAddressT, std::vector<BlockAlterTreasuryIncome> > value, const QString& prefix_tabs)
{
  QStringList treasury_accounts = value.keys();
  treasury_accounts.sort();

  QString out = CConsts::NL + prefix_tabs + "Block Alter Treasury Incomes: The Accounts(" + QString::number(treasury_accounts.size()) + " coin): ";
  for(int64_t account_inx = 0; account_inx < treasury_accounts.size(); account_inx++)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Account(" + treasury_accounts[account_inx] + ") " + dumpMe(value[treasury_accounts[account_inx]], CConsts::DUMPER_INDENT);
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CAddressT, CoinVoterDocInfo> value, const QString& prefix_tabs)
{
  QStringList addresses = value.keys();
  addresses.sort();
  QString out = CConsts::NL + prefix_tabs + "Addresses: " + QString::number(addresses.size());
  for(int64_t addr_inx = 0; addr_inx < addresses.size(); addr_inx++)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + QString::number(addr_inx) + ". Address(" + CUtils::hash8c(addresses[addr_inx]) + ") " + dumpMe(value[addresses[addr_inx]]);
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(const VoteData& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + prefix_tabs + "A Vote Data: ";
  out += CConsts::NL + prefix_tabs + "doc(" + CUtils::hash8c(value.m_docHash) + ")";
  out += CConsts::NL + prefix_tabs + "Vote Gain(" + QString::number(value.m_voteGain) + ")";
  out += CConsts::NL + prefix_tabs + "Vote m_inside6VotersCount(" + QString::number(value.m_inside6VotersCount) + ")";
  out += CConsts::NL + prefix_tabs + "Vote m_inside6VotesGain(" + QString::number(value.m_inside6VotesGain) + ")";
  out += CConsts::NL + prefix_tabs + "Vote m_outside6VotesGain(" + QString::number(value.m_outside6VotesGain) + ")";
  out += CConsts::NL + prefix_tabs + "Vote Gain(" + QString::number(value.m_voteGain) + ")";

  out += CConsts::NL + prefix_tabs + "Vote Details: " + dumpMe(value.m_details);

  return out;
}

QString UTXOImportDataContainer::dumpMe(std::vector<CoinVoterDocInfo> value, const QString& prefix_tabs)
{
  QString out = "Coin voter docs(" + QString::number(value.size())+ " docs)";
  for(uint64_t elm_inx = 0; elm_inx < value.size(); elm_inx++)
  {
    out += CConsts::NL + prefix_tabs + QString::number(elm_inx) + ". " + dumpMe(value[elm_inx], prefix_tabs);
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(const CoinOrderedSpender& value, const QString& prefix_tabs)
{
   QString out = CConsts::NL + prefix_tabs + "Vote Data: " + dumpMe(value.m_voteData, prefix_tabs);
   out += CConsts::NL + prefix_tabs + "Coin Voter doc: " + dumpMe(value.m_docs, prefix_tabs);
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CDocHashT, CoinOrderedSpender> value, const QString& prefix_tabs)
{
  QStringList docs = value.keys();
  docs.sort();
  QString out = CConsts::NL + prefix_tabs + "Coin Ordered Spender: " + QString::number(docs.size()) + " docs: ";
  for(int64_t doc_inx = 0; doc_inx < docs.size(); doc_inx++)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + QString::number(doc_inx) + ". Doc(" + CUtils::hash8c(docs[doc_inx]) + ") " + dumpMe(value[docs[doc_inx]]);
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(const SingleTrxDPCost& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "coin(" + CUtils::shortCoinRef(value.m_coin) + ") value("+ CUtils::microPAIToPAI6(value.m_value) + ") owner(" + CUtils::shortBech16(value.m_address) + ") creation date(" + value.m_ref_creation_date + ")";
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CDocHashT, SingleTrxDPCost> value, const QString& prefix_tabs)
{
  QStringList docs = value.keys();
  docs.sort();
  QString out = CConsts::NL + prefix_tabs + "Single Trx DPCost: " + QString::number(docs.size()) + " docs: ";
  for(int64_t doc_inx = 0; doc_inx < docs.size(); doc_inx++)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + QString::number(doc_inx) + ". Doc(" + CUtils::hash8c(docs[doc_inx]) + ") " + dumpMe(value[docs[doc_inx]], CConsts::DUMPER_INDENT);
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CCoinCodeT, QHash<uint32_t, QHash<CDocHashT, CoinOrderedSpender> > > value, const QString& prefix_tabs)
{
  QStringList coins = value.keys();
  coins.sort();
  QString out = CConsts::NL + prefix_tabs + "Coins And Ordered Spenders: " + QString::number(coins.size()) + " coin: ";
  for(int64_t coin_inx = 0; coin_inx < coins.size(); coin_inx++)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " coin(" + CUtils::hash8c(coins[coin_inx]) + ") ";
    QList<uint32_t> orders = value[coins[coin_inx]].keys();
    for (int32_t order_inx = 0; order_inx < orders.size(); order_inx++)
    {
      out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + dumpMe(value[coins[coin_inx]][orders[order_inx]], QString::number(order_inx) + ". " + QString::number(orders[order_inx]) + " ");
    }
  }

  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<CDocHashT, ValidityCheck> value, const QString& prefix_tabs)
{
  QStringList coins = value.keys();
  coins.sort();
  QString out = CConsts::NL + prefix_tabs + "A Doc Validity Check: " + QString::number(coins.size()) + " coin: ";
  for(int64_t coin_inx = 0; coin_inx < coins.size(); coin_inx++)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " coin(" + CUtils::hash8c(coins[coin_inx]) + ") ";
    out += dumpMe(value[coins[coin_inx]], QString::number(coin_inx) + ". ");
  }

  return out;
}

QString UTXOImportDataContainer::dumpMe(std::vector<QString> value, const QString& prefix_tabs)
{
  QString out = "";
  for(QString elm: value)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + elm;
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<QString, QString> value, const QString& prefix_tabs)
{
  QString out = "";
  QStringList keys_ = value.keys();
  keys_.sort();
  for(QString a_key: keys_)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + a_key + ": " + value[a_key];
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(QHash<QString, std::vector<QString> > value, const QString& prefix_tabs)
{
  QString out = "";
  QStringList keys_ = value.keys();
  keys_.sort();
  for(QString a_key: keys_)
  {
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + a_key + ": " + dumpMe(value[a_key]);
  }
  return out;
}

QString UTXOImportDataContainer::dumpMe(const BlockDPCostTreasury& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Treasury entry:";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Category(" + value.m_cat + ") Title(" + value.m_title + ") ";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Coin(" + CUtils::shortCoinRef(value.m_coin) + ") Value(" + CUtils::microPAIToPAI6(value.m_value) + ") ";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Descriptions: " + value.m_descriptions + " ";
  return out;
}

QString UTXOImportDataContainer::dumpMe(const BlockDPCostBacker& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "A DPCost for Backer:";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Coin(" + CUtils::shortCoinRef(value.m_coin) + ") Value(" + CUtils::microPAIToPAI6(value.m_value) +") Owner(" + CUtils::shortBech16(value.m_address) + ") ";
  return out;
}

QString UTXOImportDataContainer::dumpMe(const BlockTreasuryLog& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "A Block Treasury Log:";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Category(" + value.m_cat + ") Title(" + value.m_title + ") ";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Coin(" + CUtils::shortCoinRef(value.m_coin) + ") Value(" + CUtils::microPAIToPAI6(value.m_value) +") ";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Descriptions: " + value.m_descriptions;
  out += "m_donateRefLocsBlocks: " + dumpMe(value.m_donateRefLocsBlocks);
  return out;
}

QString UTXOImportDataContainer::dumpMe(std::vector<SusInputDetection> value, const QString& prefix_tabs)
{
  QString out = "";
  for(SusInputDetection elm: value)
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + dumpMe(elm, prefix_tabs);
  return out;
}

QString UTXOImportDataContainer::dumpMe(std::vector<BlockTreasuryLog> value, const QString& prefix_tabs)
{
  QString out = "";
  for(BlockTreasuryLog elm: value)
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + dumpMe(elm, prefix_tabs);
  return out;
}

QString UTXOImportDataContainer::dumpMe(const RawVote& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Voter(" + CUtils::shortBech16(value.m_voter)+ QString::number(value.m_voterPercentage) + " %) Vote Date(" + value.m_vote_date + ") coin(" + CUtils::shortCoinRef(value.m_voting_coin) + ") Logged in block" + CUtils::hash8c(value.m_logger_block) + ")";
  out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + "Order(" + QString::number(value.m_receive_order)+ ") spended in block/doc(" + CUtils::hash8c(value.m_spender_block) + " / " + CUtils::hash8c(value.m_spender_doc) + ") spend date(" + value.m_spend_date + ")";
  return out;
}

QString UTXOImportDataContainer::dumpMe(std::vector<RawVote> value, const QString& prefix_tabs)
{
  QString out = "";
  for(RawVote elm: value)
    out += CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + dumpMe(elm, prefix_tabs);
  return out;
}

QString UTXOImportDataContainer::dumpMe(const SusInputDetection& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " Coin(" + CUtils::shortCoinRef(value.m_coin) + ") Detection(" + value.m_detection + ") ";
  return out;
}

QString UTXOImportDataContainer::dumpMe(const CostPaymentStatus& value, const QString& prefix_tabs)
{
  QString out = CConsts::NL + CConsts::DUMPER_INDENT + prefix_tabs + " is payed(" + CUtils::dumpIt(value.m_is_payed) + ") Message(" + value.m_message+ ") ";
  return out;
}






QString UTXOImportDataContainer::dumpMe()
{
  // implement it ASAP
  QString out = "\n\n-   -  -  -  ";
  out += "\n\n- Is sus block: " + CUtils::dumpIt(m_block_is_sus_case);
  out += "\n\n- Can Import Normally: " + CUtils::dumpIt(m_can_import_normally);
  out += "\n\n- Block Has Income: " + CUtils::dumpIt(m_block_has_income);
  out += "\n\n- Does Enough Sus Votes Exist: " + m_does_enough_sus_votes_exist;
  out += "\n\n- Current Votes Percentage: " + QString::number(m_current_votes_percentage);
  out += "\n\n-   -  -  -  ";

  out += "\n\n- To Cut From Backer Fee: " + CUtils::microPAIToPAI6(m_to_cut_from_backer_fee);
  out += "\n\n- Block DPCost Backer Final: " + CUtils::microPAIToPAI6(m_block_DPCost_backer_final);
  out += "\n\n- To Cut From Treasury Fee: " + CUtils::microPAIToPAI6(m_to_cut_from_treasury_fee);
  out += "\n\n- Block DPCost Treasury Final: " + CUtils::microPAIToPAI6(m_block_DPCost_treasury_final);

  out += "\n\n- Importable UTXOs: " + dumpMe(m_importable_UTXOs);
  out += "\n\n- Cut Ceased Trx From UTXOs: " + dumpMe(m_cut_ceased_trx_from_UTXOs);
  out += "\n\n- Supported P4P: " + dumpMe(m_supported_P4P);
  out += "\n\n- Block DPCost Treasury: " + dumpMe(m_block_DPCost_treasury);
  out += "\n\n- Block DPCost Backer: " + dumpMe(m_block_DPCost_backer);
  out += "\n\n- Transactions Validity Check: " + dumpMe(m_transactions_validity_check);
  out += "\n\n- DPCosts Ref Locs: " + dumpMe(m_to_be_restored_coins);
  out += "\n\n- Transactions Detection: " + dumpMe(m_transactions_detection);
  out += "\n\n- Must Not Import Trx Outputs: " + dumpMe(m_must_not_import_trx_outputs);

  out += "\n\n- Sus Inputs Detection: " + dumpMe(m_sus_inputs_detection);
  out += "\n\n- Block Treasury Logs: " + dumpMe(m_block_treasury_logs);
  out += "\n\n- Rejected Transactions: " + dumpMe(m_rejected_transactions);

  out += "\n\n- Block Alter Treasury Incomes: " + dumpMe(m_block_alter_treasury_incomes, CConsts::DUMPER_INDENT);

//  out += "\n\n- Trx U Dict: " + dumpMe(m_trx_U_dict);

  out += "\n\n- Map U Trx Ref To Trx Hash: " + dumpMe(m_map_U_trx_ref_to_trx_hash);
  out += "\n\n- Map U Trx Hash To Trx Ref: " + dumpMe(m_map_U_trx_hash_to_trx_ref);
  out += "\n\n- Map U Referencer To Referenced: " + dumpMe(m_map_U_referencer_to_referenced);
  out += "\n\n- Map U Referenced To Referencer: " + dumpMe(m_map_U_referenced_to_referencer);

  out += "\n\n- A Single Trx DPCost: " + dumpMe(m_a_single_trx_DPCost);
  out += "\n\n- DPCosts Ref Locs: " + dumpMe(m_DPCost_coin_codes);
//  out += "\n\n- Time Locked Docs: " + dumpMe(m_time_locked_docs);

  out += "\n\n- Raw votes: " + dumpMe(m_raw_votes);
//  out += "\n\n- Votes dict: " + dumpMe(m_votes_dict);

  return out;
}

