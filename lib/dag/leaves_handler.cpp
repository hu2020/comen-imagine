#include "stable.h"
#include "leaves_handler.h"


LeavesHandler::LeavesHandler()
{

}

const QString LeavesHandler::stbl_kvalue = "c_kvalue";

std::tuple<bool, QString> LeavesHandler::removeFromLeaveBlocks(const QStringList& leaves)
{
  QJsonObject current = getLeaveBlocks();
  QJsonObject newLeaves {};
  QStringList keys = current.keys();
  for (QString a_key : keys)
  {
    if (std::find(leaves.begin(), leaves.end(), a_key) == leaves.end())
    {
      // push it to new vector
      newLeaves.insert(a_key, current.value(a_key));
    }
  }

  // update db
  QString newLeavesSer = CUtils::serializeJson(newLeaves);
  QVDicT values;
  values["kv_value"] = newLeavesSer;
  values["kv_last_modified"] = CUtils::getNow();

  DbModel::upsert(
    LeavesHandler::stbl_kvalue,
    "kv_key",
    "DAG_LEAVE_BLOCKS",
    values,
    true);

  return {true, ""};
}

QJsonObject LeavesHandler::getLeaveBlocks(const CDateT& only_before_date)
{
  QString value = KVHandler::getValue("DAG_LEAVE_BLOCKS");
  if (value == "")
    return QJsonObject {};

//  QueryRes currentLeaves = DbModel::select(
//    LeavesHandler::stbl_kvalue,
//    QStringList {"kv_value"},
//    {ModelClause("kv_key", QString::fromStdString("DAG_LEAVE_BLOCKS"))},
//    {},   // order
//    1);   // limit
//  if (currentLeaves.records.size() == 0) {
//    QJsonObject json_obj {};
//    return json_obj;
//  }

  QJsonObject json_obj = CUtils::parseToJsonObj(value);
  if (only_before_date == "")
  {
    return json_obj;
  }

  // filter older leaves  FIXME: complete it
  QJsonObject filterd_json_obj {};
  for (QString a_key: json_obj.keys())
  {
    QJsonObject aLeave = json_obj.value(a_key).toObject();
    if (
       (aLeave.value("bType").toString() == CConsts::BLOCK_TYPES::Genesis) ||
       (aLeave.value("bCDate").toString() < only_before_date)
       )
    {
      filterd_json_obj.insert(a_key, json_obj.value(a_key));
    }
  }

  return filterd_json_obj;
}

std::tuple<bool, QString> LeavesHandler::addToLeaveBlocks(
  const CBlockHashT& block_hash,
  const CDateT& creation_date,
  const QString& bType)
{
  QJsonObject current = getLeaveBlocks();
  QJsonObject jsonObj
  {
      {"bType", bType},
      {"bCDate", creation_date},
  };
  current.insert(block_hash, jsonObj);

  QVDicT values;
  values["kv_value"] = CUtils::serializeJson(current);
  values["kv_last_modified"] = CUtils::getNow();

  DbModel::upsert(
    LeavesHandler::stbl_kvalue,
    "kv_key",
    "DAG_LEAVE_BLOCKS",
    values,
    true);

  return {true, ""};
}

QJsonObject LeavesHandler::getFreshLeaves()
{
  // the leaves younger than two cylce (24 hours) old
  QJsonObject leaves = getLeaveBlocks();

  CLog::log("current leaves: " + CUtils::serializeJson(leaves));

  if (leaves.keys().size() == 0)
    return leaves;

  QString now = CUtils::getNow();
  QJsonObject refreshes = {};
  for (QString a_key : leaves.keys())
  {
    QJsonObject aLeave = leaves.value(a_key).toObject();
    uint64_t leaveAge = CUtils::timeDiff(aLeave.value("bCDate").toString(), now).asMinutes;
    QString msg = "leave("+CUtils::hash8c(a_key)+") age (" + QString("%1").arg(leaveAge) + ") minutes is " +
          ((leaveAge < CMachine::getCycleByMinutes() * 2) ? "younger" : "older") +
        " than 2 cycles";
    CLog::log(msg);
    if (leaveAge < CMachine::getCycleByMinutes() * 2)
    {
      refreshes.insert(a_key, leaves[a_key]);
    }
  }

  return refreshes;
}

bool LeavesHandler::hasFreshLeaves()
{
  QJsonObject freshLeaves = getFreshLeaves();
  return (freshLeaves.keys().size() > 0);
}





