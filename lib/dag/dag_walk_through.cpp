#include "stable.h"

#include "dag.h"



/**
 * returns latest block which is already recorded in DAG
 */
std::tuple<bool, BlockRecord> DAG::getLatestBlockRecord()
{
  QVDRecordsT lastBLockRecord = searchInDAG(
    {},
    {"b_hash", "b_creation_date"},
    {{"b_creation_date", "DESC"}},
    1
  );
  if (lastBLockRecord.size() == 0)
  {
    CLog::log("The DAG hasn't any node!", "sec", "fatal");
    BlockRecord b;
    return {false, b};
  }
  BlockRecord blockRecord(lastBLockRecord[0]);
  return {false, blockRecord};
}

std::tuple<bool, CBlockHashT, CDateT> DAG::getLatestBlock()
{
  QVDRecordsT last_block_record = searchInDAG(
    {},
    {"b_hash", "b_creation_date"},
    {{"b_creation_date", "DESC"}},
    1);

  if (last_block_record.size() == 0)
  {
    CLog::log("The DAG hasn't any node!", "sc", "error");
    return {false, "", ""};
  }

  QVDicT wBlock = last_block_record[0];
  return {
    true,
    wBlock.value("b_hash").toString(),
    wBlock.value("b_creation_date").toString()
  };
}

/**
 *
 * @param {*} block_hashes
 * @param {*} level
 * this is not aggregative and returns ONLY given level's ancestors and not the blocks in backing track way
 */
QStringList DAG::getAncestors(
  QStringList block_hashes,
  uint level)
{
  if (block_hashes.size() == 0)
    return {};

  QVDRecordsT res = searchInDAG(
    {{"b_hash", block_hashes, "IN"}},
    {"b_ancestors"});

  if (res.size()== 0)
    return {};

  QStringList out {};
  for (QVDicT aRes: res)
    out = CUtils::arrayAdd(out, aRes.value("b_ancestors").toString().split(","));

  out = CUtils::arrayUnique(out);

  if (level == 1)
    return out;

  return getAncestors(out, level - 1);
}

/**
 *
 * @param {*} block_hash
 * returns the first generation descentents of a given block, by finding all blocks in DAG which have the given block as a ancestor
 */
QStringList DAG::findDescendetsBlockByAncestors(const CBlockHashT& block_hash)
{
//  CLog::log("find DescendetsBlockByAncestors lokking for desc of block(" + CUtils::hash8c(block_hash) + ")", "app", "trace");
  QVDRecordsT res = searchInDAG(
    {{"b_ancestors", "%" + block_hash + "%", "LIKE"}},
    {"b_hash"},
    {},
    0,
    false,
    false);
  if (res.size() == 0)
    return {};

  QStringList out = {};
  for(auto a_res: res)
    out.append(a_res.value("b_hash").toString());
  return out;
}

QStringList DAG::getDescendents(
  QStringList block_hashes,
  int level)
{
  if (block_hashes.size()== 0)
    return {};

  CLog::log("get Descendents of blocks: " + block_hashes.join(", "), "app", "trace");
  // 1. retrieve descendent blocks from DAG by descendents property
  // 2. if step one hasn's answer tries to find descendents by ancestors link of blocks

  // 1. retrieve descendent blocks from DAG by descendents property
  QVDRecordsT block_records = searchInDAG(
    {{"b_hash", block_hashes, "IN"}},
    {"b_hash", "b_descendents"});
  if (block_records.size()== 0)
  {
    // TODO: the block(s) is valid and does not exist in local. or
    // invalid block invoked, maybe some penal for sender!
    CLog::log("The blocks looking descendents does not exist in local! blocks: " + block_hashes.join(", "), "app", "trace");
    return {};
  }
  QStringList descendents {};
  for (QVDicT a_block_record: block_records)
  {
    bool descendent_was_found = false;
    if (a_block_record.value("b_descendents").toString() != "")
    {
      QStringList desc = CUtils::removeEmptyElements(a_block_record.value("b_descendents").toString().split(","));
      if (desc.size() > 0)
      {
        descendent_was_found = true;
        descendents = CUtils::arrayAdd(descendents, desc);
      }
    }
    if (!descendent_was_found)
    {
      QStringList desc = findDescendetsBlockByAncestors(a_block_record.value("b_hash").toString());
      descendents = CUtils::arrayAdd(descendents, desc);
    }
  };
  descendents = CUtils::arrayUnique(descendents);

  if (level == 1)
    return CUtils::removeEmptyElements(descendents);

  return getDescendents(descendents, level - 1);
}

/**
*
* @param {*} block_hash
* returns all descendents of block(include the block also)
*/
std::tuple<bool, QVDRecordsT, double> DAG::getAllDescendents(
  const CBlockHashT& block_hash,
  const bool& retrieve_validity_percentage)
{
  QStringList decends {block_hash};
  QStringList previous_descendents = decends;
  int i = 0;
  CLog::log("The Block previous_descendents " + QString::number(i++) + " " + previous_descendents.join(", "), "trx", "trace");
  while (decends.size() > 0)
  {
    decends = getDescendents(decends, 1);
    previous_descendents = CUtils::arrayUnique(CUtils::arrayAdd(previous_descendents, decends));
    CLog::log("The Blocks previous_descendents " + QString::number(i++) + ": " + previous_descendents.join(", "), "trx", "trace");
  }
  // exclude floating signature blocks
  QStringList fields = {"b_hash", "b_cycle", "b_creation_date"};
  if (retrieve_validity_percentage)
    fields.append("b_backer");

  QVDRecordsT block_records = excludeFloatingBlocks(previous_descendents, fields);
  DNASharePercentT validity_percentage = 0.0;
  if (retrieve_validity_percentage)
  {
    QHash<QString, QHash<QString, double> > backerOnDateSharePercentage {};
    for (QVDicT aBlock: block_records)
    {
      if (validity_percentage > 100.0)
        break;

      QString the_backer = aBlock.value("b_backer").toString();
      if (!backerOnDateSharePercentage.keys().contains(the_backer))
        backerOnDateSharePercentage[the_backer] = {};

      QString b_creation_date = aBlock.value("b_creation_date").toString();
      if (!backerOnDateSharePercentage[the_backer].keys().contains(b_creation_date))
      {
        auto [shares_, percentage] = DNAHandler::getAnAddressShares(the_backer, b_creation_date);
        Q_UNUSED(shares_);
        backerOnDateSharePercentage[the_backer][b_creation_date] = percentage;
        validity_percentage += percentage;
      } else {
        validity_percentage += backerOnDateSharePercentage[the_backer][b_creation_date];
      }
      CLog::log(
        "backer/Date/percentage, validity_percentage \nthe_backer: " +  the_backer +
        " \nb_creation_date: " +  b_creation_date +
        " \nbackerOnDateSharePercentage: " +  QString("%1").arg(backerOnDateSharePercentage[the_backer][b_creation_date]) +
        " \nvalidity_percentage: " +  QString("%1").arg(validity_percentage) , "app", "trace");
    }
  }

  CLog::log("The descendents after exclude floating signature blocks: " + CUtils::dumpIt(block_records), "trx", "trace");
  if (validity_percentage > 100)
    validity_percentage = 100.0;
  return {true, block_records, validity_percentage};
}

bool DAG::refreshCachedBlocks()
{
  auto[status, cachedBlocks] = CMachine::cachedBlocks();
  if (cachedBlocks.size() < 500)
  {
    QVDRecordsT blocks = DAG::searchInDAG(
      {},
      {"b_type", "b_hash", "b_creation_date", "b_utxo_imported"},
      {{"b_creation_date", "ASC"},
      {"b_type", "DESC"}});    // TODO: optimize it ASAP
    CMachine::cachedBlocks("assign", blocks);

    QString tmp_hash = "";
    for (QVDicT a_block: blocks)
    {
      tmp_hash = a_block.value("b_hash").toString();
      CMachine::cachedBlockHashes("append", {tmp_hash});
    }

  } else {
//    QStringList ten_latest_block_hashes = {};
//    QStringList ten_latest_block_dates = {};
//    int start_elm_inx = CMachine::cachedBlocks().size() - 10;
//    for (int i = start_elm_inx; i < CMachine::cachedBlocks().size(); i++)
//    {
//      ten_latest_block_hashes.append(CMachine::cachedBlocks()[i].value("b_hash").toString());
//      ten_latest_block_dates.append(CMachine::cachedBlocks()[i].value("b_creation_date").toString());
//    }
//    ten_latest_block_dates.sort();

//    QVDRecordsT new_blocks = DAG::searchInDAG(
//      {{"b_creation_date", ten_latest_block_dates[0], ">="},
//      {"b_hash", ten_latest_block_hashes, "NOT IN"}},
//      {"b_type", "b_hash", "b_creation_date", "b_utxo_imported"},
//      {{"b_creation_date", "ASC"},
//      {"b_type", "DESC"}});
//    for (QVDicT a_block: new_blocks)
//    {
//      CMachine::cachedBlocks("append", a_block);
//      CMachine::cachedBlockHashes("append", {a_block.value("b_hash").toString()});
//    }
  }

  return true;
}

bool DAG::updateCachedBlocks(
  const QString& b_type,
  const CBlockHashT& b_hash,
  const CDateT& b_creation_date,
  const QString& b_utxo_imported)
{
  CMachine::cachedBlocks("append", {{
    {"b_type", b_type},
    {"b_hash", b_hash},
    {"b_creation_date", b_creation_date},
    {"b_utxo_imported", b_utxo_imported}}});

  CMachine::cachedBlockHashes("append", {b_hash});

  return true;
}

QVDRecordsT DAG::loadCachedBlocks()
{
  refreshCachedBlocks();
  auto[status, records] = CMachine::cachedBlocks();
    if(!status)
      return {};
  return records;
}

QStringList DAG::getCachedBlocksHashes()
{
  refreshCachedBlocks();
  auto[status, hashes] = CMachine::cachedBlockHashes();
  if(!status)
      return {};
  return hashes;
}

struct TmpBlock
{
  CBlockHashT hash = "";
  QStringList ancestors = {};
  QStringList descendents = {};
  CDateT creation_date = "";
};

std::tuple<bool, QStringList> DAG::controllDAGHealth()
{
  QStringList error_messages = {};
  bool final_stat = true;

  QStringList all_block_hashes = {};
  QHash<CBlockHashT, TmpBlock> blocks_info = {};
  QHash<CBlockHashT, QStringList> ancestors_by_block = {};
  QHash<CBlockHashT, QStringList> descendents_by_block = {};

  QVDRecordsT blocks = searchInDAG(
    {},
    {"b_hash", "b_ancestors", "b_descendents", "b_creation_date"});
  QStringList the_ancestors;
  QStringList the_descendents;
  QStringList blocks_with_no_ancestors;
  QStringList blocks_with_no_descendents;
  TmpBlock block;
  for(QVDicT item: blocks)
  {
    CBlockHashT b_hash = item.value("b_hash").toString();

//    all_block_hashes.append(b_hash);
    the_ancestors = CUtils::convertCommaSeperatedToArray(item.value("b_ancestors").toString());
    if (the_ancestors.size() == 0)
      blocks_with_no_ancestors.append(b_hash);

    the_descendents = CUtils::convertCommaSeperatedToArray(item.value("b_descendents").toString());
    if (the_descendents.size() == 0)
      blocks_with_no_descendents.append(b_hash);

    block = TmpBlock {
      b_hash,
      the_ancestors,
      the_descendents,
      item.value("b_creation_date").toString()};
    blocks_info[b_hash] = block;
  }

  // controll all blocks (except Genesis) have ancestor(s)
  if (blocks_with_no_ancestors.size() > 1)
  {
    error_messages.append("Some blocks haven't ancestors!" + CUtils::arrayDiff(all_block_hashes, ancestors_by_block.keys()).join(","));
    final_stat &= false;
  }


  // controll backward moving
  QStringList exit_in_backward_moving = {};
  QStringList blocks_to_be_considered = blocks_with_no_descendents;
  int counter = 0;
  QStringList visited_blocks = {};
  while ((counter < blocks_info.keys().size()/* in worst case it is not a DAG but a link-list*/) && (blocks_to_be_considered.size() > 0))
  {
    counter++;

    QStringList new_ancestors = {};
    for (CBlockHashT a_hash: blocks_to_be_considered)
    {
      visited_blocks.append(a_hash);
      new_ancestors = CUtils::arrayAdd(new_ancestors, blocks_info[a_hash].ancestors);
    }
    blocks_to_be_considered = CUtils::arrayUnique(new_ancestors);
  }
  QStringList missed_blocks = CUtils::arrayDiff(blocks_info.keys(), visited_blocks);
  if (missed_blocks.size() > 0)
  {
    error_messages.append("Some blocks weren't visible in backward moving!" + missed_blocks.join(","));
    final_stat &= false;
  }

  // controll forward moving
  QVDRecordsT genesis = searchInDAG(
    {{"b_type", CConsts::BLOCK_TYPES::Genesis}},
    {"b_hash", "b_ancestors", "b_descendents", "b_creation_date"});
  QStringList exit_in_forward_moving = {};
  blocks_to_be_considered = QStringList {genesis[0].value("b_hash").toString()};
  counter = 0;
  visited_blocks = QStringList {};

  while ((counter < blocks_info.keys().size()/* in worst case it is not a DAG but a link-list*/) && (blocks_to_be_considered.size() > 0))
  {
    counter++;

    QStringList new_descendents = {};
    for (CBlockHashT a_hash: blocks_to_be_considered)
    {
      visited_blocks.append(a_hash);
      new_descendents = CUtils::arrayAdd(new_descendents, blocks_info[a_hash].descendents);
    }
    blocks_to_be_considered = CUtils::arrayUnique(new_descendents);
  }
  missed_blocks = CUtils::arrayDiff(blocks_info.keys(), visited_blocks);
  if (missed_blocks.size() > 0)
  {
    error_messages.append("Some blocks weren't visible in forward moving!" + missed_blocks.join(","));
    final_stat &= false;
  }


  return {final_stat, error_messages};
}

