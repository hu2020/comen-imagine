#ifndef DAG_H
#define DAG_H


class BlockRecord;
class CCoin;
#include "lib/block/block_types/block.h"


class DAG
{

public:
  static QString stbl_blocks;
  static QStringList stbl_blocks_fields;

  static QString stbl_docs_blocks_map;
  DAG();


  /**
   *
   * @param {array} blockHashes
   * @param {array} descendents
   * adds given descendents to given blocks
   */
  static void appendDescendents(const QStringList& blockHashes, const QStringList& newDescendents);

  static QVDRecordsT searchInDAG(
    const ClausesT& clauses,
    const QStringList& fields = stbl_blocks_fields,
    const OrderT& order = {},
    const int& limit = 0,
    const bool& is_transactional = false,
    const bool& do_log = false);

  static std::tuple<QVDRecordsT, GRecordsT> getWBlocksByDocHash(
    const QStringList& doc_hashes,
    const QString& hashes_type = CConsts::COMPLETE);

  static std::tuple<bool, QJsonObject, CDocIndexT, MerkleNodeData, QJsonArray> retrieveDocByDocHash(
    const CDocHashT& doc_hash,
    const bool need_doc_ext_info = false,
    const bool need_doc_merkle_proof = false);

  static QV2DicT getCoinsGenerationInfoViaSQL(const QStringList& coins);

  static std::tuple<QStringList, GRecordsT> getBlockHashesByDocHashes(
    const QStringList& doc_hashes,
    const QString& hashes_type = CConsts::COMPLETE);

  static QVDRecordsT excludeFloatingBlocks(
    const QStringList& blockHashes,
    const QStringList& fields = {"b_hash", "b_cycle", "b_creation_date"});

  static void updateUtxoImported(
    const QString& block_hash,
    const QString& status);

  static QVDRecordsT getCyclesList();

  static std::tuple<bool, QVDicT> getMostConfidenceCoinbaseBlockFromDAG(CDateT cDate = "");


  //  -  -  -  walkthrough DAG
  static std::tuple<bool, QStringList> controllDAGHealth();
  static bool updateCachedBlocks(
    const QString& b_type,
    const CBlockHashT& b_hash,
    const CDateT& b_creation_date,
    const QString& b_utxo_imported = CConsts::NO);
  static bool refreshCachedBlocks();
  static QVDRecordsT loadCachedBlocks();
  static QStringList getCachedBlocksHashes();

  static std::tuple<bool, BlockRecord> getLatestBlockRecord();

  static std::tuple<bool, CBlockHashT, CDateT> getLatestBlock();

  static QStringList getAncestors(
    QStringList blockHashes = {},
    uint level = 1);

  static void recursive_backwardInTime(
    const QStringList& blockHashes,
    const QString& date,
    QStringList& ancestors);

  static QStringList returnAncestorsYoungerThan(
    const QStringList& blockHashes,
    const QString& byDate = "",
    const int32_t& byMinutes = -1,
    const int32_t& cycle = -1);

  static QSDRecordsT analyzeDAGHealth(const bool shouldUpdateDescendants = false);

  static QStringList findDescendetsBlockByAncestors(const QString& blockHash);

  static QStringList getDescendents(
    QStringList blockHashes,
    int level = 1);

  static std::tuple<bool, QVDRecordsT, double> getAllDescendents(
    const QString& block_hash,
    const bool& retrieveValidityPercentage = false);

  static bool isDAGUptodated(QString cDate = "");

  static bool loopPrerequisitiesRemover();
  static bool doPrerequisitiesRemover();

  static bool DAGHasBlocksWhichAreCreatedInCurrrentCycle(QString cDate = "");

  static std::vector<CCoin> retrieveBlocksInWhichARefLocHaveBeenProduced(const CCoinCodeT& the_coin);

  static std::tuple<CMPAIValueT, QVDRecordsT, CMPAIValueT> getNotImportedCoinbaseBlocks();

  static std::tuple<CMPAIValueT, QStringList, QString> getNotImportedNormalBlock();

  static std::tuple<CMPAIValueT, QHash<CBlockHashT, CMPAIValueT>, CMPAIValueT, QHash<CBlockHashT, CMPAIValueT>, CMPAIValueT> getCBBlocksStat();

  static QVDicT getLatestRecordedBlcok();

};

#endif // DAG_H
