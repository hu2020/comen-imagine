#include "stable.h"

#include "lib/block_utils.h"
#include "lib/block/document_types/document.h"

#include "super_control_til_coinbase_minting.h"


//let resObj1 = {
//    calledAncestors: [],
//    ref: null,
//    err: false,
//    msg: ''
//}

void setter1(QVDicT& resObj, const QVDicT& values)
{
  for(QString key: values.keys())
    resObj[key] = values[key];

    // if (_.has(args, 'ref'))
    //     resObj1.ref = args.ref;

    // if (_.has(args, 'err'))
    //     resObj1.err = args.err;

    // if (_.has(args, 'msg'))
    //     resObj1.msg = args.msg;
}

QVariant getter1(QVDicT& resObj, QString k = "")
{
  if (k == "")
    return "";

  // clog.trx.info(`SuperValidate, getter1111 called for key: ${k}`);
  if (resObj.keys().contains(k))
    return resObj[k];
  return "";
}

SuperControlTilCoinbaseMinting::SuperControlTilCoinbaseMinting()
{

}

/**
*
* @param {*} block
* @param {*} interested_docs
* going back all the way to find the Coinbase or RlBlock block for every spent inputs in given block
* returns [status, msg, coin_track]
*/
std::tuple<bool, QString, QVDRecordsT> SuperControlTilCoinbaseMinting::trackingBackTheCoins(
  const Block* block,
  const QStringList& interested_docs,
  const QStringList& interested_coins)
{
  uint16_t level = 0;
  QVDRecordsT coin_track = {};
  if (CConsts::log_super_validate_debug)
  {
    CLog::log(QString::number(level) + ".SCUUCM, Block(" + CUtils::hash8c(block->m_block_hash) + ")", "trx", "trace");
    CLog::log(QString::number(level) + ".SCUUCM, block: " + CUtils::dumpIt(block), "trx", "trace");
  }

  auto[validate_status, validate_msg] = trackingBackTheCoinsRecursive(
    coin_track,
    block->exportBlockToJSon(true),
    interested_docs,
    interested_coins,
    "Generated",
    level);

  if (CConsts::log_super_validate_debug)
    CLog::log(QString::number(level) + ".SCUUCM, coin Track: " + CUtils::dumpIt(coin_track), "trx", "trace");

  return {validate_status, validate_msg, coin_track};
}


/**
 * @brief SuperControlTilCoinbaseMinting::trackingBackTheCoinsRecursive
 * @param coin_track
 * @param block
 * @param interested_docs
 * @param interested_coins
 * @param descendent
 * @param level
 * returns [status, msg]
 */
std::tuple<bool, QString> SuperControlTilCoinbaseMinting::trackingBackTheCoinsRecursive(
  QVDRecordsT& coin_track,
  const QJsonObject& block,
  QStringList interested_docs,
  QStringList interested_coins,
  const QString& descendent,
  uint16_t level)
{
  QString msg;
  level++;


  if ((level == 1) && (interested_docs.size() == 0))
  {
    // log all docs as interested
    for(auto a_doc: block.value("docs").toArray())
      interested_docs.append(a_doc.toObject().value("dHash").toString());
  }

  if ((level == 1) && (interested_coins.size() == 0))
  {
    // log all coins as interested
    for (CDocHashT a_doc_hash: interested_docs)
    {
      auto[inx_, the_doc] = Block::getDocumentJByHash(block, a_doc_hash);
      Q_UNUSED(inx_);
      for (auto anInp: the_doc.value("inputs").toArray())
        interested_coins.append(CUtils::packCoinCode(anInp.toArray()[0].toString(), anInp.toArray()[1].toString()));
    }
  }

  QVDicT trace_back {
    {"level", level},
    {"bType", block.value("bType").toString()},
    {"creationDate", block.value("bCDate").toString()},
    {"bHash", block.value("bHash").toString()},
    {"descendent", descendent},
    {"interested_docs", interested_docs},
    {"interested_coins", interested_coins}
  };
  coin_track.push_back(trace_back);
  QJsonArray documents = block.value("docs").toArray();
  for (CDocIndexT doc_inx = 0; doc_inx < documents.size(); doc_inx++)
  {
    QJsonObject doc = documents[doc_inx].toObject();

    if (CConsts::log_super_validate_debug)
      CLog::log(QString::number(level) + ".SCUUCM, block.blockHash(" + CUtils::hash8c(block.value("bHash").toString()) + " doc(" + CUtils::hash8c(doc.value("dHash").toString()) + ") doc_inx(" + QString::number(doc_inx) + "/" + doc.value("dType").toString(), "trx", "info");

    // if it is not first level, so it MUST have certain interested doc_hash to trace back
    if ((level != 1) && !interested_docs.contains(doc.value("dHash").toString()))
      continue;

    /**
     * in first level we can ask for control of all/some docs in block
     * if is insisted to control certain docs and current doc is not one of mentioned docs,
     * so contiue the loop
     */
    if ((level == 1) &&
      (interested_docs.size() > 0) &&
      (!interested_docs.contains(doc.value("dHash").toString()))
    )
      continue;


    if (CConsts::log_super_validate_debug)
      CLog::log(QString::number(level) + ".SCUUCM, control doc(" + CUtils::hash8c(doc.value("dHash").toString()) + ")", "app", "trace");


    if (!Document::trxHasInput(doc.value("dType").toString()))
    {
      /**
       * transaction has not input, so it must be the very transaction in which the fresh output was created.
       * either because of being Coinbase Trx or being the DPCostPay Trx
       */
      if (Document::trxHasNotInput(doc.value("dType").toString()))
      {
        if (interested_docs.contains(doc.value("dHash").toString()) || ((level == 1) && (interested_docs.size() == 0)))
        {
          // we found one interested fresh coin creating location
          coin_track.push_back(QVDicT {
            {"level", level},
            {"bType", block.value("bType")},
            {"creationDate", block.value("bCDate")},
            {"bHash", block.value("bHash")},
            {"dType", doc.value("dType").toString()},
            {"descendent", doc.value("dHash").toString()},
            {"interested_docs", QStringList{doc.value("dHash").toString()}},
            {"interested_coins", QStringList{"fresh coin"}}});
        }
        continue;

      } else {
        msg = QString::number(level) + ".SCUUCM, The trx must be Coinbase or DPCostPay and it is " + block.value("bType").toString() + "/" + doc.value("dType").toString() + " block(" + CUtils::hash8c(block.value("bHash").toString()) + ") doc(" + CUtils::hash8c(doc.value("dHash").toString());
        CLog::log(QString::number(level) + ".SCUUCM, block(" + CUtils::hash8c(block.value("bHash").toString()) + ") doc(" + CUtils::hash8c(doc.value("dHash").toString()) +" must be Coinbase or DPCostPay and it is " + block.value("bType").toString() + "/" + doc.value("dType").toString(), "sec", "error");
        return {false, msg};
      }
    }

    QJsonArray docExtInfos = {};
    if(block.keys().contains("bExtInfo"))
    {
      QJsonArray bExtInfo = block.value("bExtInfo").toArray();
      docExtInfos = bExtInfo[doc_inx].toArray();
    }

    auto inputs = doc.value("inputs").toArray();

    if (CConsts::log_super_validate_debug)
      CLog::log(QString::number(level) + ".SCUUCM, 1, the doc(" + CUtils::hash8c(doc.value("dHash").toString()) + " has some inputs(" + QString::number(inputs.size()) + " to trace back", "trx", "info");

    for (CInputIndexT input_index = 0; input_index < inputs.size(); input_index++)
    {
      QJsonArray input = inputs[input_index].toArray();

      CDocHashT coin_creator_doc_hash = input[0].toString();
      COutputIndexT coin_creator_doc_output_index = input[1].toDouble();

      // mapping input ref to proper container block
      auto[ref_block_hashes, map_doc_to_block_] = DAG::getBlockHashesByDocHashes({coin_creator_doc_hash});
      Q_UNUSED(map_doc_to_block_);
      if (ref_block_hashes.size() == 0)
      {
        msg = QString::number(level) + ".SCUUCM, the refloc " + CUtils::packCoinCode(coin_creator_doc_hash, coin_creator_doc_output_index) + " does not mapped to it's container-block!";
        CLog::log(msg, "sec", "error");
        return {false, msg};
      }
      if (CConsts::log_super_validate_debug)
      {
        CLog::log(QString::number(level) + ".SCUUCM, doc_hash " + CUtils::hash8c(coin_creator_doc_hash));
        CLog::log(QString::number(level) + ".SCUUCM, ref_block_hashes " + CUtils::shortStringsList(ref_block_hashes).join(", "));
      }

      // another paranoic test
      bool control_coin_by_ancestors_links = true;
      if (control_coin_by_ancestors_links)
      {
        // go back to history by ancestors link to find reference location(which block creates this ref as output) for coin_creator_doc_hash
        QVDicT resObj = {};
        lookForDocByAnc(
          block,
          coin_creator_doc_hash, //doc_hash
          setter1,
          getter1,
          resObj);

        CLog::log(QString::number(level) + ".SCUUCM, getter1 finalresssssssssssssssss ${JSON.stringify(getter1())}");

        if (getter1(resObj, "err").toBool() == true)
        {
          msg = getter1(resObj, "msg").toString();
          CLog::log(msg, "trx", "error");
          return {false, msg};
        }

        if (!ref_block_hashes.contains(getter1(resObj, "ref").toString()))
        {
          msg = QString::number(level) + ".SCUUCM, the doc_hash(" + CUtils::hash8c(coin_creator_doc_hash) + ") which is used in doc(" + CUtils::hash8c(doc.value("dHash").toString()) +" in block(" + CUtils::hash8c(block.value("bHash").toString()) + ") is referring to 2 diff blocks by Ancestors(" + getter1(resObj, "ref").toString() + ") & i_docs_blocks_map(" + CUtils::shortStringsList(ref_block_hashes).join(", ") + ")";
          CLog::log(msg, "sec", "error");
          return {false, msg};
        }
      }

      // retrieve container block info
      QVDRecordsT block_records = DAG::searchInDAG(
        {{"b_hash", ref_block_hashes, "IN"}});
      if (block_records.size() != CUtils::arrayUnique(ref_block_hashes).size())
      {
        msg = QString::number(level) + ".SCUUCM, some of the blocks(" + CUtils::shortStringsList(ref_block_hashes).join(", ") + ") do not exist in DAG (" + QString::number(block_records.size()) + ")";
        CLog::log(msg, "sec", "error");
        return {false, msg};
      }
      for (QVDicT a_block_record: block_records)
      {
        QJsonObject refBlock = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(a_block_record.value("b_body").toString()).content);
        if (
          (block.value("bType").toString() != CConsts::BLOCK_TYPES::RpBlock) &&
          (CUtils::timeDiff(refBlock.value("bCDate").toString(), block.value("bCDate").toString()).asMinutes < CMachine::getCycleByMinutes())
        ) {
          msg = QString::number(level) + ".SCUUCM, block(" + CUtils::hash8c(block.value("bHash").toString()) + " " + block.value("bCDate").toString() + ") ";
          msg += "uses an output of block(" + CUtils::hash8c(refBlock.value("bCDate").toString()) + ") " + refBlock.value("bCDate").toString() + ") before being maturated";
          CLog::log(msg, "sec", "error");
          return {false, msg};
        }

        // looking for refBlock.output = block.input
        bool inputExistInRefBlock = false;
        for (auto refDoc: refBlock.value("docs").toArray())
        {
          QJsonObject a_doc = refDoc.toObject();
          if (a_doc.value("dHash").toString() == coin_creator_doc_hash)
          {
            inputExistInRefBlock = true;

            if (!Document::trxHasNotInput(a_doc.value("dType").toString()))
            {
              // also output address of ref controls!
              bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
                docExtInfos[input_index].toObject().value("uSet").toObject(),
                a_doc.value("outputs").toArray()[coin_creator_doc_output_index].toArray()[0].toString());
              if (is_valid_unlock != true)
              {
                msg = QString::number(level) + ".SCUUCM, Invalid given unlock structure for trx:" + coin_creator_doc_hash + " input: " + QString::number(input_index);
                CLog::log(msg, "sec", "error");
                return {false, msg};
              }
            }

            uint8_t mature_cycles_count = CUtils::getMatureCyclesCount(a_doc.value("dType").toString());
            if (mature_cycles_count > 1)
            {
              // control maturity
              if (
                  (!QStringList{CConsts::BLOCK_TYPES::RpBlock}.contains(block.value("bType").toString())) &&
                  (CUtils::timeDiff(refBlock.value("bCDate").toString(), block.value("bCDate").toString()).asMinutes < (mature_cycles_count * CMachine::getCycleByMinutes()))
              ) {
                msg = QString::number(level) + ".SCUUCM, block(" + CUtils::hash8c(block.value("bHash").toString()) + " " + block.value("bCDate").toString() + ") ";
                msg += "uses an output(" + a_doc.value("dType").toString() + " " + CUtils::hash8c(a_doc.value("dHash").toString()) + ") of block(" + CUtils::hash8c(refBlock.value("bHash").toString()) + " " + refBlock.value("bCDate").toString() + " ";
                msg += "before being maturated by " + QString::number(mature_cycles_count) + " cycles ";
                CLog::log(msg, "sec", "error");
                return {false, msg};
              }
            }

            // controlling the ref of ref
            return trackingBackTheCoinsRecursive(
              coin_track,
              refBlock,
              {a_doc.value("dHash").toString()},
              {},
              doc.value("dHash").toString(),
              level);

          }
        }

        if (!inputExistInRefBlock)
        {
          msg = QString::number(level) + ".SCUUCM, the input(" + CUtils::hash8c(coin_creator_doc_hash) + ") which is used in block(" + CUtils::hash8c(block.value("bHash").toString()) + ") do not exist in block(" + CUtils::hash8c(refBlock.value("bHash").toString()) + ")";
          CLog::log(msg, "sec", "error");
          return {false, msg};
        }
      }
    }
  }
  return {true, msg};
}

//QString doSerial_blocks(const QVDicT& item)
//{
// QString out =
//   item.value("b_hash").toString() +
//   item.value("b_creation_date").toString() +
//   item.value("b_ancestors").toString() +
//   item.value("b_type").toString() +
//   item.value("b_utxo_imported").toString();
// return out;
//}



void SuperControlTilCoinbaseMinting::lookForDocByAnc(
  const QJsonObject& block,
  const CDocHashT& doc_hash,
  std::function <void(QVDicT& resObj, const QVDicT& values)> vSetter,
  std::function <QVariant(QVDicT& resObj, QString k)> vGetter,
  QVDicT& resObj)
{

  vSetter(resObj, QVDicT {
    {"ref", ""},
    {"calledAncestors", {}},
    {"err", false},
    {"msg", ""}
  });
  lookForDocByAncRecursive(
    block,
    doc_hash,
    vSetter,
    vGetter,
    resObj,
    0);
  return;
}

void SuperControlTilCoinbaseMinting::lookForDocByAncRecursive(
  const QJsonObject& block,
  const CDocHashT& doc_hash,
  std::function<void(QVDicT& resObj, const QVDicT& values)> vSetter,
  std::function<QVariant(QVDicT& resObj, QString k)> vGetter,
  QVDicT& resObj,
  uint16_t level_anc)
{
//  let doc_hash = args.doc_hash;
//  let level = _.has(args, 'level') ? args.level : 'u0';

  if (vGetter(resObj, "ref").toString() != "")
    CLog::log(QString::number(level_anc) + "." + QString::number(level_anc) + " SuperValidate, founded ref=" + vGetter(resObj, "ref").toString());


  level_anc++;
  QString msg;
  if (CConsts::log_super_validate_debug)
    CLog::log(QString::number(level_anc) + "." + QString::number(level_anc) + " SuperValidate, look For Doc By Anc Recursive: looking for doc_hash(" + CUtils::hash8c(doc_hash) + ") in block(" + CUtils::hash8c(block.value("bHash").toString()) + ")", "trx", "trace");

  // check if invoked doc exist in this block
  if (!QStringList{CConsts::BLOCK_TYPES::FSign, CConsts::BLOCK_TYPES::FVote}.contains(block.value("bType").toString()))
  {
    if(Block::getDocumentsHashes(block).contains(doc_hash))
    {
      CLog::log(QString::number(level_anc) + "." + QString::number(level_anc) + " SuperValidate, OK. Found ref for doc_hash(" + CUtils::hash8c(doc_hash) + ") in block(" + CUtils::hash8c(block.value("bHash").toString()) + ")", "trx", "trace");
      vSetter(resObj, QVDicT {
        {"ref", block.value("bHash")},
        {"err", false}});
      return;
    }
  }


  QStringList ancestors = CUtils::convertJSonArrayToQStringList(block.value("ancestors").toArray());
  if (CConsts::log_super_validate_debug)
    CLog::log(QString::number(level_anc) + "." + QString::number(level_anc) + " SuperValidate, loopiong on ancestors(" + ancestors.join(", ") + ")", "trx", "trace");

  if (ancestors.size() == 0)
  {
    msg = QString::number(level_anc) + "." + QString::number(level_anc) + " .SCUUCM, block(" + CUtils::hash8c(block.value("bHash").toString()) + ") has no ancestors and still can not find the doc(" + doc_hash + ")";
    CLog::log(msg, "trx", "error");
    return;
  }

  for (CBlockHashT ancHash: ancestors)
  {
    QVDRecordsT ancWBlocks = DAG::searchInDAG(
      {{"b_hash", ancHash}},
      {"b_body"});
    if (ancWBlocks.size() != 1)
    {
      msg = QString::number(level_anc) + ".SCUUCM, block(" + CUtils::hash8c(block.value("bHash").toString()) + ") has an ancestor(" + ancHash + ") which doesn't exist in DAG (" + ancWBlocks.size() + ")";
      CLog::log(msg, "sec", "error");
      vSetter(resObj, QVDicT {
        {"err", true},
        {"msg", msg}});
      return;
    }
    QVDicT ancWBlock = ancWBlocks[0];
    QJsonObject Jblock = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(ancWBlock.value("b_body").toString()).content);
    // let ancBlock = blockUtils.openDBSafeObject(ancWBlock.value("b_body").toString()).content;
    if (CConsts::log_super_validate_debug)
      CLog::log(QString::number(level_anc) + "." + QString::number(level_anc) + " SuperValidate, refBlockHashFoundByAncestors ============(" + vGetter(resObj, "ref").toString() + ")", "trx", "trace");

    if ((vGetter(resObj, "ref").toString() == "") && !CUtils::convertJSonArrayToQStringList(vGetter(resObj, "calledAncestors").toJsonArray()).contains(Jblock.value("bHash").toString()))
    {
      QStringList tmp = CUtils::convertJSonArrayToQStringList(vGetter(resObj, "calledAncestors").toJsonArray());
      tmp.append(Jblock.value("bHash").toString());
      vSetter(resObj, QVDicT { {"calledAncestors", tmp} });
      lookForDocByAncRecursive(
        Jblock,
        doc_hash,
        vSetter,
        vGetter,
        resObj,
        level_anc);
    }
  }

}
