#ifndef SCEPTICALDAGINTEGRITYCONTROL_H
#define SCEPTICALDAGINTEGRITYCONTROL_H

#include "constants.h"
#include "lib/clog.h"

class ScepticalDAGIntegrityControl
{
public:
  ScepticalDAGIntegrityControl();
  static GenRes insertNewBlockControlls(const QString& blockHash);
};

#endif // SCEPTICALDAGINTEGRITYCONTROL_H
