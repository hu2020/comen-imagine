#ifndef LEAVESHANDLER_H
#define LEAVESHANDLER_H

#include <tuple>

class QString;
class QJsonObject;



class LeavesHandler
{
public:
  static const QString stbl_kvalue;
  LeavesHandler();
  static QJsonObject getLeaveBlocks(const CDateT& only_before_date = "");
  static std::tuple<bool, QString> removeFromLeaveBlocks(const QStringList& leaves);
  static std::tuple<bool, QString> addToLeaveBlocks(
    const CBlockHashT& block_hash,
    const CDateT& creation_date,
    const QString& bType = "UB"
  );
  static QJsonObject getFreshLeaves();
  static bool hasFreshLeaves();

};

#endif // LEAVESHANDLER_H
