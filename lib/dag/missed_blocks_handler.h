#ifndef MISSEDBLOCKSHANDLER_H
#define MISSEDBLOCKSHANDLER_H


#include "lib/dag/dag.h"
#include "lib/parsing_q_handler/parsing_q_handler.h"

class MissedBlocksHandler
{
public:
  static const QString stbl_missed_blocks;
  static const QStringList stbl_missed_blocks_fields;

  MissedBlocksHandler();

  static QVDRecordsT listMissedBlocks(
    QStringList fields,
    const ClausesT& clauses = {},
    const OrderT& order = {},
    const int& limit = 0);

  static QStringList getMissedBlocksToInvoke(const uint64_t& limit = 0);

  static bool addMissedBlocksToInvoke(QStringList hashes = {});

  static bool removeFromMissedBlocks(const CBlockHashT& block_hash);

  static bool increaseAttempNumber(const CBlockHashT& block_hash);

  static bool refreshMissedBlock();


};

#endif // MISSEDBLOCKSHANDLER_H
