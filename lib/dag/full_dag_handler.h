#ifndef FULLDAGHANDLER_H
#define FULLDAGHANDLER_H


class FullDAGHandler
{
public:
  FullDAGHandler();

  static bool invokeFullDAGDlRequest(CDateT start_from = "");

  static std::tuple<QString , QString> prepareFullDAGDlRequest(const CDateT& start_from);

  static void archiveDAGBundle();

  static std::tuple<bool, bool> prepareFullDAGDlResponse(
    const QString& sender,
    const QJsonObject& payload,
    const QString& connection_type);

};

#endif // FULLDAGHANDLER_H
