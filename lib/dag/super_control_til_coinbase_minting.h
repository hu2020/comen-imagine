#ifndef SUPERCONTROLTILCOINBASEMINTING_H
#define SUPERCONTROLTILCOINBASEMINTING_H


class SuperControlTilCoinbaseMinting
{
public:
  SuperControlTilCoinbaseMinting();

  static std::tuple<bool, QString, QVDRecordsT> trackingBackTheCoins(
    const Block* spend_block,
    const QStringList& interested_docs,
    const QStringList& interested_coins);

  static std::tuple<bool, QString> trackingBackTheCoinsRecursive(
    QVDRecordsT& coin_track,
    const QJsonObject& spend_block,
    QStringList interested_docs,
    QStringList interested_coins,
    const QString& descendent,
    uint16_t level);

  static void lookForDocByAnc(
    const QJsonObject& block,
    const CDocHashT& doc_hash,
    std::function <void(QVDicT& resObj, const QVDicT& values)> vSetter,
    std::function <QVariant(QVDicT& resObj, QString k)> vGetter,
    QVDicT& resObj);

  static void lookForDocByAncRecursive(
    const QJsonObject& block,
    const CDocHashT& doc_hash,
    std::function<void(QVDicT& resObj, const QVDicT& values)> vSetter,
    std::function<QVariant(QVDicT& resObj, QString k)> vGetter,
    QVDicT& resObj,
    uint16_t level);
};

#endif // SUPERCONTROLTILCOINBASEMINTING_H
