#include "stable.h"

#include "lib/dag/dag.h"
#include "lib/block_utils.h"
#include "full_dag_handler.h"
#include "lib/services/polling/ballot_handler.h"
#include "lib/messaging_protocol/graphql_handler.h"
#include "lib/sending_q_handler/sending_q_handler.h"

#include "lib/file_handler/file_handler.h"

FullDAGHandler::FullDAGHandler()
{

}

bool FullDAGHandler::invokeFullDAGDlRequest(CDateT start_from)
{
  if (start_from == "")
  {
    auto[status, block_hash, block_creation_date] = DAG::getLatestBlock();
    Q_UNUSED(status);
    Q_UNUSED(block_hash);
    start_from = block_creation_date;
  }
  auto[code, body] = prepareFullDAGDlRequest(start_from);
  CLog::log("invoke Full DAG Dl Request code(" + code + ") body" + body, "app", "trace");
  return SendingQHandler::pushIntoSendingQ(
    CConsts::GQL,
    code,
    body,
    "Invoke Full DAG blocks after(" + start_from + ")");
}

std::tuple<QString , QString> FullDAGHandler::prepareFullDAGDlRequest(const CDateT& start_from)
{
  return GraphQLHandler::makeAPacket(
    QJsonArray{ QJsonObject {
      {"cdType", CConsts::CARD_TYPES::FullDAGDownloadRequest},
      {"cdVer", "0.0.1"},
      {"startFrom", start_from}}});
}


void FullDAGHandler::archiveDAGBundle()
{
  FileHandler::fileCopy(
    CConsts::HD_FILES + "/DAGBundle.txt",
    CConsts::HD_FILES + "/DAGBundle-" + CUtils::getNowSSS() + ".txt");

  FileHandler::deleteFile(CConsts::HD_FILES + "/DAGBundle.txt");

}


std::tuple<bool, bool> FullDAGHandler::prepareFullDAGDlResponse(
  const QString& sender,
  const QJsonObject& payload,
  const QString& connection_type)
{
  Q_UNUSED(connection_type);
  CLog::log("Prepare Full DAG Download Response: ", "app", "trace");

  /**
  * TODO: implement sub system to chunk data and send step by step
  * and also not dedicate entire machine to reponse the requests
  */
  CDateT start_from = CMachine::getLaunchDate();
  if (payload.keys().contains("startFrom"))
    start_from = payload.value("startFrom").toString();

  QVDRecordsT block_records = DAG::searchInDAG(
    {{"b_creation_date", start_from, ">="}},
    {"b_hash", "b_body"},
    {{"b_creation_date", "ASC"}});
  QJsonArray cards {};
  for (QVDicT a_block_record: block_records)
  {
    QJsonObject Jblock;
    try {
      Jblock = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(a_block_record.value("b_body").toString()).content);

      if (Jblock.keys().size() == 0)
        continue;

      auto[status, extExist, block_ext_info] = Block::getBlockExtInfo(a_block_record.value("b_hash").toString());
      if (extExist)
        Jblock["bExtInfo"] = block_ext_info;

    } catch (std::exception) {
      CLog::log("failed on fetch block(" + CUtils::hash8c(a_block_record.value("b_hash").toString()) + ") from DB", "app", "error");
      continue;
    }

    QJsonObject a_card {
      {"cdType", CConsts::CARD_TYPES::FullDAGDownloadResponse},
      {"cdVer", "0.0.1"},
      {"block", Jblock}};
    cards.push_back(a_card);

    if (CUtils::serializeJson(cards).length() > CConsts::MAX_FullDAGDownloadResponse_LENGTH_BY_CHAR)
      break;
  }

  // also prepare the ballots received time
  // TODO: investigate to send ballots chuck by chunk (like blocks)
  QVDRecordsT ballots = BallotHandler::searchInOnchainBallots(
  {},
  {"ba_hash", "ba_receive_date", "ba_vote_r_diff"});

  QJsonObject ballotsReceiveDates {};
  for (QVDicT a_ballot: ballots)
  {
    ballotsReceiveDates[a_ballot.value("ba_hash").toString()] = QJsonObject {
      {"baReceiveDate", a_ballot.value("ba_eceive_date").toString()},
      {"baVoteRDiff", a_ballot.value("ba_vote_r_diff").toString()}};
  }
  QJsonObject a_card = {
    {"cdType", CConsts::CARD_TYPES::BallotsReceiveDates},
    {"cdVer", "0.0.1"},
    {"ballotsReceiveDates", ballotsReceiveDates}};
  cards.push_back(a_card);


  auto[code, body] = GraphQLHandler::makeAPacket(cards);

  CLog::log("Goint to insert Full DAG Dl Response: " + QString::number(cards.size()) + " cards", "app", "info");
  bool push_res = SendingQHandler::pushIntoSendingQ(
    CConsts::GQL,
    code,
    body,
    "GQL Full-DAG-blocks response packet(" + CUtils::hash8c(code) + ")",
    {sender});

  return {push_res, true};
}
