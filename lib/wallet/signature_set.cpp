#include "signature_set.h"

using namespace std;
#include "constants.h"

SignatureSet::SignatureSet(const QString &sKey, const QString &pPledge, const QString &pDelegate)
{
    m_sKey = sKey;
    m_pPledge = pPledge;
    m_pDelegate = pDelegate;
}
