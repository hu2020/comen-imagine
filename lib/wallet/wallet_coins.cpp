#include "stable.h"

#include "lib/block_utils.h"
#include "lib/block/document_types/document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/document_types/basic_tx_document.h"
#include "lib/dag/normal_block/rejected_transactions_handler.h"

#include "wallet.h"


// js name was retrieveSpendableUTXOsAsync
QVDRecordsT Wallet::retrieveSpendableCoins(QStringList w_addresses)
{
  QStringList wallet_ddresses {};

  if (w_addresses.size() == 0)
  {
    auto[address_records, details] = getAddressesList();
    Q_UNUSED(details);
    for(QVDicT add: address_records)
      wallet_ddresses.append(add.value("wa_address").toString());
  }
  QVDRecordsT UTXOs = UTXOHandler::extractUTXOsBYAddresses(wallet_ddresses);
  return UTXOs;
}


bool Wallet::refreshCoins()
{
  QString mp_code = CMachine::getSelectedMProfile();

  //prepare the wallet addreses:
  auto[addresses_, details] = getAddressesList(mp_code, {"wa_address"}, false);
  Q_UNUSED(details);
  QStringList addresses;
  for(QVDicT elm: addresses_)
    addresses.append(elm.value("wa_address").toString());

  if (addresses.size() == 0)
    return false;

  CDateT latest_update =  KVHandler::getValue("latest_refresh_funds");
  CLog::log("latest refresh funds: " + latest_update, "app" "info");
  if (latest_update == "")
    KVHandler::upsertKValue("latest_refresh_funds", CMachine::getLaunchDate());


  QVDRecordsT block_records = DAG::searchInDAG(
    {{"b_type", {CConsts::BLOCK_TYPES::FSign, CConsts::BLOCK_TYPES::FVote, CConsts::BLOCK_TYPES::POW}, "NOT IN"},
    {"b_creation_date", CMachine::getLaunchDate(), ">="}}, // TODO improve it to reduce process load. (e.g. use latest_update instead)
    {"b_type", "b_hash", "b_body"},
    {{"b_creation_date", "ASC"}});

  // FIXME: (improve it) remove this and search in c_blocks only new blocks
  DbModel::dDelete(
    stbl_machine_wallet_funds,
    {{"wf_mp_code", mp_code}});

  for (QVDicT a_block_records: block_records)
    updateFundsFromNewBlock(a_block_records, addresses);

  KVHandler::upsertKValue("latest_refresh_funds", CUtils::getNow());

  return true;
}

QVDRecordsT Wallet::getCoinsList(const bool should_refresh_coins)
{
  if (should_refresh_coins)
    refreshCoins();

  QString mp_code = CMachine::getSelectedMProfile();

  QueryRes res = DbModel::select(
    stbl_machine_wallet_funds,
    stbl_machine_wallet_funds_fields,
    {{"wf_mp_code", mp_code}},
    {{"wf_mature_date", "ASC"}});

  return res.records;
}

bool Wallet::insertAnUTXOInWallet(
  const CBlockHashT& wf_block_hash,
  const CDocHashT& wf_trx_hash,
  const COutputIndexT wf_o_index,
  const CAddressT& wf_address,
  const CMPAIValueT& wf_o_value,
  const QString& wf_trx_type,
  const CDateT& wf_creation_date,
  const CDateT& wf_mature_date,
  QString wf_mp_code)
{
  if (wf_mp_code == "")
    wf_mp_code = CMachine::getSelectedMProfile();

  QueryRes dblChk = DbModel::select(
    stbl_machine_wallet_funds,
    {"wf_trx_hash"},
    {{"wf_mp_code", wf_mp_code},
    {"wf_trx_hash", wf_trx_hash},
    {"wf_o_index", wf_o_index}},
    {},
    0,
    false,
    false);
  if (dblChk.records.size() > 0)
  {
    // maybe update!

  } else {
    //insert
    QVDicT values {
      {"wf_mp_code", wf_mp_code},
      {"wf_address", wf_address},
      {"wf_block_hash", wf_block_hash},
      {"wf_trx_type", wf_trx_type},
      {"wf_trx_hash", wf_trx_hash},
      {"wf_o_index", wf_o_index},
      {"wf_o_value", QVariant::fromValue(wf_o_value)},
      {"wf_creation_date", wf_creation_date},
      {"wf_mature_date", wf_mature_date},
      {"wf_last_modified", CUtils::getNow()}};

    DbModel::insert(
      stbl_machine_wallet_funds,
      values);

  }

  return true;
}


bool Wallet::deleteFromFunds(
  const CDocHashT& wf_trx_hash,
  const COutputIndexT wf_o_index,
  QString wf_mp_code)
{
  if (wf_mp_code == "")
  wf_mp_code = CMachine::getSelectedMProfile();

  DbModel::dDelete(
    stbl_machine_wallet_funds,
    {{"wf_mp_code", wf_mp_code},
    {"wf_trx_hash", wf_trx_hash},
    {"wf_o_index", wf_o_index}});

  return true;
}

bool Wallet::deleteFromFunds(
  const BasicTxDocument* trx)
{
  bool res = true;
  for (auto an_input: trx->getInputs())
    res &= deleteFromFunds(an_input->m_transaction_hash, an_input->m_output_index);
  return res;
}


bool Wallet::updateFundsFromNewBlock(
  const QVDicT& block_record,
  const QStringList& wallet_addresses)
{
  // since all validation controls (relatd to transaction input/output) are already done before recording block_record in DAG,
  //here we trust the block_record information and just extract the funds from transactions

//  QString mp_code = CMachine::getSelectedMProfile();
  QJsonObject block = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(block_record.value("b_body").toString()).content);    // do not need safe open check
  CLog::log("Update wallet funds for Block(" + block.value("b_type").toString() + " / " + CUtils::hash8c(block.value("b_hash").toString()) + ")", "app" , "trace");

  // extract unmatured outputs
  for (auto aDoc_: block.value("docs").toArray())
  {
    QJsonObject a_doc = aDoc_.toObject();

    if (a_doc.value("dType").toString() == CConsts::DOC_TYPES::Coinbase)
    {
      QJsonArray outputs = a_doc.value("outputs").toArray();
      for (COutputIndexT output_index = 0; output_index < outputs.size(); output_index++)
      {
        QJsonArray anOutput = outputs[output_index].toArray();
        if (!wallet_addresses.contains(anOutput[0].toString()))
          continue;

        insertAnUTXOInWallet(
          block.value("bHash").toString(),
          a_doc.value("dHash").toString(),
          output_index,
          anOutput[0].toString(),
          anOutput[1].toDouble(),
          a_doc.value("dType").toString(),
          block.value("bCDate").toString(),
          CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate(block.value("bCDate").toString()));

      }

    } else if (a_doc.value("dType").toString() == CConsts::DOC_TYPES::DPCostPay) {
      QJsonArray outputs = a_doc.value("outputs").toArray();
      for (COutputIndexT output_index = 0; output_index < outputs.size(); output_index++)
      {
        QJsonArray anOutput = outputs[output_index].toArray();
        // import only wallet controlled funds, implicitely removes the "TP_DP" outputs too.
        if (!wallet_addresses.contains(anOutput[0].toString()))
          continue;

        insertAnUTXOInWallet(
          block.value("bHash").toString(),
          a_doc.value("dHash").toString(),
          output_index,
          anOutput[0].toString(),
          anOutput[1].toDouble(),
          a_doc.value("dType").toString(),
          block.value("bCDate").toString(),
          CUtils::minutesAfter(CUtils::getCycleByMinutes(), block.value("bCDate").toString()));

      }

    } else if (a_doc.value("dType").toString() == CConsts::DOC_TYPES::BasicTx) {
      QJsonArray outputs = a_doc.value("outputs").toArray();
      for (COutputIndexT output_index = 0; output_index < outputs.size(); output_index++)
      {
        QJsonArray anOutput = outputs[output_index].toArray();

        // import only wallet controlled funds, implicitely removes the outputs dedicated to treasury payments too
        if (!wallet_addresses.contains(anOutput[0].toString()))
          continue;

        // do not import DPCost payment outputs, because they are already spen in DPCostPay doc
        if (a_doc.value("dPIs").toArray().contains(output_index))
          continue;

        insertAnUTXOInWallet(
          block.value("bHash").toString(),
          a_doc.value("dHash").toString(),
          output_index,
          anOutput[0].toString(),
          anOutput[1].toDouble(),
          a_doc.value("dType").toString(),
          block.value("bCDate").toString(),
          CUtils::minutesAfter(CUtils::getCycleByMinutes(), block.value("bCDate").toString()));

      }

      // removing spent UTXOs in block too
      for (auto input: a_doc.value("inputs").toArray())
        deleteFromFunds(input.toArray()[0].toString(), input.toArray()[1].toInt());

    } else if (a_doc.value("dType").toString() == CConsts::DOC_TYPES::RpDoc) {
      QJsonArray outputs = a_doc.value("outputs").toArray();
      for (COutputIndexT output_index = 0; output_index < outputs.size(); output_index++)
      {
        QJsonArray anOutput = outputs[output_index].toArray();

        // import only wallet controlled funds, implicitely removes the outputs dedicated to treasury payments too
        if (a_doc.value("dPIs").toArray().contains(output_index))
          continue;

        // import only wallet controlled funds, implicitely removes the outputs dedicated to treasury payments too
        if (!wallet_addresses.contains(anOutput[0].toString()))
          continue;

        insertAnUTXOInWallet(
          block.value("bHash").toString(),
          a_doc.value("dHash").toString(),
          output_index,
          anOutput[0].toString(),
          anOutput[1].toDouble(),
          a_doc.value("dType").toString(),
          block.value("bCDate").toString(),
          CUtils::minutesAfter(CUtils::getCycleByMinutes(), block.value("bCDate").toString()));

      }

      // removing spent UTXOs in block too
      for (auto input: a_doc.value("inputs").toArray())
        deleteFromFunds(input.toArray()[0].toString(), input.toArray()[1].toInt());


    } else if (a_doc.value("dType").toString() == CConsts::DOC_TYPES::RlDoc) {
      // FIXME: needs more tests before relaese the first reserved block
      //for (let output_index = 0; output_index < a_doc.value("outputs.length; output_index++) {
      //  let anOutput = a_doc.value("outputs[output_index];
      //  if (!wallet_addresses.includes(anOutput[0]))
      //      continue;

      //  await WalletHandler.insertAnUTXOInWallet({
      //      wfmpCode: mp_code,
      //      wfTrxHash: a_doc.value("hash,
      //      wfOIndex: output_index,
      //      wfAddress: anOutput[0],
      //      wfBlockHash: block.blockHash,
      //      wfTrxType: a_doc.value("dType,
      //      wfOValue: anOutput[1],
      //      wfCreation Date: block.creation Date,
      //      wfMatureDate: coinbaseUTXOs.calcCoinbasedOutputMaturationDate(block.creation Date)
      //  });
      //}
    }
  }

  // finally remove locally used coins from wallet funds
  excludeLocallyUsedCoins();

  return true;
}


std::tuple<bool, QString, QHash<CCoinCodeT, TInput>, CMPAISValueT> Wallet::getSomeCoins(
  CMPAIValueT minimum_spendable,
  const QString& selection_method,
  const uint64_t unlocker_index,
  QStringList excluded_coins,
  const bool allowedToUseRejectedCoinsToTest)
{
  QString msg;
  if (minimum_spendable == 0)
    minimum_spendable = SocietyRules::getTransactionMinimumFee();
  msg = "Fetch some inputs equal to " + CUtils::microPAIToPAI6(minimum_spendable) + " PAIs to use in a transaction";
  CLog::log(msg, "trx", "info");

  // retrieve rejected transactions
  QVDRecordsT rejected_coins_records = RejectedTransactionsHandler::searchInRejectedTrx();
  QStringList rejected_coins {};
  for (QVDicT a_rej_coin: rejected_coins_records)
    rejected_coins.append(a_rej_coin.value("rt_coin").toString());

  CLog::log("Rejected coins: " + rejected_coins.join(", "), "trx", "info");

  // intentioinally sending rejected funds to test network
  if (allowedToUseRejectedCoinsToTest)
    rejected_coins = QStringList {};  // empty rejected coins

  QJsonObject addresses_dict {};
  auto[wallet_controlled_accounts, details] = getAddressesList("", {"wa_address", "wa_title", "wa_detail"});
  for (QVDicT an_address: wallet_controlled_accounts)
  {
    CAddressT address_account = an_address.value("wa_address").toString();
    addresses_dict[address_account] = QJsonObject {
      {"title", an_address.value("wa_title").toString()},
      {"detail", CUtils::parseToJsonObj(an_address.value("wa_detail").toString())}};
  }

  QStringList addresses_accounts = addresses_dict.keys();
  CLog::log("The wallet/profile controls " + QString::number(addresses_accounts.size()) + " addresses: " + addresses_accounts.join(", ") + rejected_coins.join(", "), "trx", "info");

  QVDRecordsT spendable_coins = UTXOHandler::extractUTXOsBYAddresses(addresses_accounts);
  if (spendable_coins.size() == 0)
  {
    msg = "Wallet couldn't find proper coins to spend " + CUtils::sepNum(minimum_spendable) + " micro PAIs!";
    CLog::log(msg, "trx", "warning");
    return {false, msg, {}, 0};
  }
  CLog::log("Going to select some of these coins: " + CUtils::dumpIt(spendable_coins), "trx", "trace");

  QVDRecordsT locally_marked_coins_records = Wallet::searchLocallyMarkedUTXOs(
    {{"lu_mp_code", CMachine::getSelectedMProfile()}});

  QStringList locally_marked_coins {};
  for (QVDicT a_coin: locally_marked_coins_records)
    locally_marked_coins.append(a_coin.value("lu_coin").toString());

  CLog::log("Localy marked UTXOs as spend coins(" + QString::number(locally_marked_coins.size()) + "): " + locally_marked_coins.join(", "), "trx", "info");

  QVDRecordsT tmpCoins {};
  for (QVDicT aCoin: spendable_coins)
    if (!locally_marked_coins.contains(aCoin.value("ut_coin").toString()))
      tmpCoins.push_back(aCoin);

  spendable_coins = tmpCoins;
  CLog::log("spendable coins after removing localy markewd coins (" + QString::number(spendable_coins.size()) + " coins): " + CUtils::dumpIt(spendable_coins), "trx", "info");
  if (spendable_coins.size() == 0)
  {
    msg = "Wallet hasn't any un-spended coins to use!";
    CLog::log(msg, "trx", "warning");
    return {false, msg, {}, 0};
  }

  CMPAISValueT sum_coin_values = 0;
  QHash<CCoinCodeT, TInput> selected_coins {};
  uint64_t loop_x_count = 0; // to prevent unlimited cycle
  while ((sum_coin_values < minimum_spendable) && (spendable_coins.size() > 0) && (loop_x_count < 1000))
  {
    loop_x_count++;

    CLog::log("Get Some Inputs, excluding these coins: " + excluded_coins.join(", "), "trx", "info");

    // exclude used coins
    QVDRecordsT tmp_spendable_coins {};
    for(QVDicT a_coin: spendable_coins)
      if (!excluded_coins.contains(a_coin.value("ut_coin").toString()) && !rejected_coins.contains(a_coin.value("ut_coin").toString()))
        tmp_spendable_coins.push_back(a_coin);
    spendable_coins = tmp_spendable_coins;

    CLog::log("Going to select one of these real spendable coins: " + CUtils::dumpIt(spendable_coins), "trx", "info");

    QVDicT the_selected_coin;
    std::vector<CMPAIValueT> values {};
    QHash<CMPAIValueT, QVDicT> objByValues = {};
    if (selection_method == CConsts::COIN_SELECTING_METHOD::PRECISE)
    {
      values = {};
      objByValues = {};
      for (QVDicT a_coin: spendable_coins)
      {
        CMPAIValueT val = a_coin.value("ut_o_value").toDouble();
        values.push_back(val);
        objByValues[val] = a_coin;
      }
      std::sort(values.begin(), values.end());
      auto last = std::unique(values.begin(), values.end());
      values.erase(last, values.end());

      CLog::log("get Some Coins: values.sort() : " + CUtils::dumpIt(values), "trx", "info");

      for (CMPAIValueT a_value: values)
      {
        CLog::log("get Some Coins: controlling if a coin value is greater than needed money! " + CUtils::microPAIToPAI6(a_value) + " is >= " + CUtils::microPAIToPAI6(minimum_spendable) + " - " + CUtils::microPAIToPAI6(sum_coin_values), "trx", "info");
        if ((the_selected_coin.keys().size() == 0) && (a_value >= (minimum_spendable - sum_coin_values)))
          the_selected_coin = objByValues[a_value];
      }
      if (the_selected_coin.keys().size() == 0)
      {
        if (objByValues.keys().size() == 0)
        {
          msg = "Failed in select coin to spend";
          CLog::log(msg, "trx", "warning");
          return {false, msg, {}, 0};
        }
        std::reverse(values.begin(), values.end());
        the_selected_coin = objByValues[values[0]];
      }

    } else if (selection_method == CConsts::COIN_SELECTING_METHOD::BIGGER_FIRST)
    {
      values = {};
      objByValues = {};
      for (QVDicT a_coin: spendable_coins)
      {
        CMPAIValueT val = a_coin.value("ut_o_value").toDouble();
        values.push_back(val);
        objByValues[val] = a_coin;
      }
      std::sort(values.begin(), values.end());
      auto last = std::unique(values.begin(), values.end());
      values.erase(last, values.end());
      std::reverse(values.begin(), values.end());
      the_selected_coin = objByValues[values[0]];

    } else if (selection_method == CConsts::COIN_SELECTING_METHOD::SMALLER_FIRST)
    {
      values = {};
      objByValues = {};
      for (QVDicT a_coin: spendable_coins)
      {
        CMPAIValueT val = a_coin.value("ut_o_value").toDouble();
        values.push_back(val);
        objByValues[val] = a_coin;
      }
      std::sort(values.begin(), values.end());
      auto last = std::unique(values.begin(), values.end());
      values.erase(last, values.end());
      the_selected_coin = objByValues[values[0]];

    } else if (selection_method == CConsts::COIN_SELECTING_METHOD::RANDOM)
    {
      int spendableInx = floor(random() * spendable_coins.size());
      the_selected_coin = spendable_coins[spendableInx];

    }

    CLog::log("The selected Coin: " + CUtils::dumpIt(the_selected_coin), "trx", "info");

    if (the_selected_coin.keys().size() > 0)
    {
      // console.log('the_selected_coin', the_selected_coin);
      CMPAIValueT val = the_selected_coin.value("ut_o_value").toDouble();
      CCoinCodeT coin_code = the_selected_coin.value("ut_coin").toString();
      CAddressT coin_owner = the_selected_coin.value("ut_o_address").toString();
      excluded_coins.append(coin_code);
      sum_coin_values += val;

      QJsonObject detail = addresses_dict[coin_owner].toObject().value("detail").toObject();
      QJsonObject anUnlockSet = detail.value("uSets")[unlocker_index].toObject();

      auto[doc_hash_, output_index_] = CUtils::unpackCoinCode(coin_code);
      selected_coins[coin_code] = TInput {
        doc_hash_,
        output_index_,
        coin_owner,
        val,
        CUtils::convertJSonArrayToQStringList(detail.value("the_private_keys")[anUnlockSet.value("salt").toString()].toArray()),
        anUnlockSet};
    }
    for (auto a_coin: selected_coins)
      CLog::log("The selected Coins: " + a_coin.dumpMe(), "trx", "info");

  }

  if (selected_coins.keys().size() == 0)
  {
    msg = "No input was selected to spend";
    CLog::log(msg, "trx", "warning");
    return {false, msg, {}, 0};
  }

  return {true, "selected", selected_coins, sum_coin_values};
}

void Wallet::removeRef(CCoinCodeT coin_code)
{
  QString mp_code = CMachine::getSelectedMProfile();
  CLog::log("removing unused coin from machine_used_utxos (" + CUtils::shortCoinRef(coin_code) + ")");
  DbModel::dDelete(
    stbl_machine_used_coins,
    {{"lu_mp_code", mp_code},
    {"lu_coin", coin_code}});
}

void Wallet::restorUnUsedUTXOs()
{
  QString mp_code = CMachine::getSelectedMProfile();
  // retrtieve all marked as spen which are not recorded in DAG and markewd as used before than 2 cycle
  CDateT cDate = CUtils::minutesBefore(CMachine::getCycleByMinutes());
  QString q = " SELECT * FROM " + stbl_machine_used_coins+ " WHERE lu_mp_code='" + mp_code + "' AND lu_coin NOT IN (SELECT sp_coin FROM c_trx_spend) AND lu_insert_date<'" + cDate + "'";
  QueryRes res = DbModel::customQuery(
    "db_comen_blocks",
    q,
    {"lu_coin"},
    0,
    {},
    false,
    true);
  CLog::log("found unused coins: " + CUtils::dumpIt(res.records));
  for (QVDicT row: res.records)
    removeRef(row.value("lu_coin").toString());
}
