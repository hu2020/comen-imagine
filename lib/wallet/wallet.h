#ifndef WALLET_H
#define WALLET_H

#include "lib/machine/machine_handler.h"

class UnlockDocument;
class BasicTxDocument;
class TInput;

class WalletAddress
{
public:
  QString m_mp_code = ""; // machine profile code
  QString m_address = "";
  QString m_title = "";
  UnlockDocument *m_unlock_doc;
  QString m_creation_date = "";

  WalletAddress();
  WalletAddress(
    UnlockDocument *unlock_doc,
    const QString& mp_code,
    const QString& title,
    const QString& creation_date = "");

};

class Wallet
{
public:
  Wallet();

  static const QString stbl_machine_wallet_addresses;
  static const QStringList stbl_machine_wallet_addresses_fields;

  static const QString stbl_machine_wallet_funds;
  static const QStringList stbl_machine_wallet_funds_fields;

  static const QString stbl_machine_used_coins;
  static const QStringList stbl_machine_used_coins_fields;

  static QVDRecordsT retrieveSpendableCoins(QStringList w_addresses = {});


  //  -  -  -  address handling
  static std::tuple<bool, QVDRecordsT> searchWalletAdress(
    const QStringList& addresses,
    QString mp_code = "",
    const QStringList& fields = stbl_machine_wallet_addresses_fields);

  static std::tuple<QVDRecordsT, QV2DicT> getAddressesList(
    QString mp_code = "",
    const QStringList& fields = {"wa_address"},
    const bool& sum = false);

  static bool insertAddress(const WalletAddress& w_address);

  static QVDRecordsT getAddressesInfo(
    const QStringList& addresses,
    const QStringList& fields = stbl_machine_wallet_addresses_fields);

  static GenRes createANewAddress(
    const QString& signature_type,
    const QString& signature_mod = CConsts::DEFAULT_SIGNATURE_MOD,
    const QString& signature_version = "0.0.0");

  static GenRes getAnOutputAddress(
    bool make_new_address = false,
    const QString& signature_type = CConsts::SIGNATURE_TYPES::Basic,
    const QString& signature_mod = CConsts::DEFAULT_SIGNATURE_MOD,
    const QString& signature_version = "0.0.0");

    static QVDicT convertToValues(const WalletAddress& w_address);

  //  -  -  - coins
  static bool refreshCoins(); // js name was refreshFunds

  static QVDRecordsT getCoinsList(const bool should_refresh_coins = false);   // js name was getFundsList

  static bool insertAnUTXOInWallet(
    const CBlockHashT& wf_block_hash,
    const CDocHashT& wf_trx_hash,
    const COutputIndexT wf_o_index,
    const CAddressT& wf_address,
    const CMPAIValueT& wf_o_value,
    const QString& wf_trx_type,
    const CDateT& wf_creation_date,
    const CDateT& wf_mature_date,
    QString wf_mp_code = "");

  static bool deleteFromFunds(
    const CDocHashT& wf_trx_hash,
    const COutputIndexT wf_o_index,
    QString wf_mp_code = "");

  static bool deleteFromFunds(
    const BasicTxDocument* trx);

  static bool updateFundsFromNewBlock(
    const QVDicT& block_record,
    const QStringList& wallet_addresses);

  static std::tuple<bool, QString, QHash<CCoinCodeT, TInput>, CMPAISValueT> getSomeCoins(
    CMPAIValueT minimum_spendable = 0,
    const QString& selection_method = "random",
    const uint64_t unlocker_index = 0,
    QStringList excluded_coins = {},
    const bool allowedToUseRejectedCoinsToTest = false);

  static void removeRef(CCoinCodeT coin_code);

  static void restorUnUsedUTXOs();


  //  -  -  -  signer part
  static std::tuple<bool, QString> walletSigner(
    QStringList coins,
    CMPAIValueT sending_amount,
    CMPAIValueT max_data_process_cost,
    CAddressT recipient,
    CAddressT change_back_address = "",
    CMPAIValueT output_bill_amount = 0,  // the amount of output(s). zero means one output for all amount, while 1000000 means each output must be a million mPAI
    QString description = "");

  static void locallyMarkUTXOAsUsed(const BasicTxDocument* trx);

  static bool excludeLocallyUsedCoins();

  static QVDRecordsT searchLocallyMarkedUTXOs(
    const ClausesT& clauses,
    const QStringList& fields = stbl_machine_used_coins_fields,
    const OrderT& order = {},
    const int& limit = 0);

  static std::tuple<bool, QString, QStringList, QJsonObject> signByAnAddress(
      const CAddressT& signer_address,
      const QString& sign_message,
      CSigIndexT unlocker_index = 0);

};



#endif // WALLET_H
