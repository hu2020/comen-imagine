#include "stable.h"

#include "gui/c_gui.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_set.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_document.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/individual_signature.h"

#include "wallet.h"

WalletAddress::WalletAddress(UnlockDocument *unlock_doc, const QString& mp_code, const QString& title, const QString& creation_date)
{
  m_unlock_doc = new UnlockDocument();
  *m_unlock_doc = *unlock_doc;
  m_mp_code = mp_code; // machine profile code
  m_address = unlock_doc->m_account_address;
  m_title = title;
  m_creation_date = creation_date;
  if (m_creation_date == "")
    m_creation_date = CUtils::getNow();
}




// -  -  -  Wallet



std::tuple<bool, QVDRecordsT> Wallet::searchWalletAdress(
  const QStringList& addresses,
  QString mp_code,
  const QStringList& fields)
{
  ClausesT clauses;

  if( mp_code == "")
    mp_code = CMachine::getSelectedMProfile();

  if (mp_code != CConsts::ALL)
    clauses.push_back({"wa_mp_code", mp_code});

  clauses.push_back(
    ModelClause("wa_address", addresses, "IN")
  );

  QueryRes res = DbModel::select(
    stbl_machine_wallet_addresses,
    fields,
    clauses);

  return {true, res.records};
}

std::tuple<QVDRecordsT, QV2DicT> Wallet::getAddressesList(
    QString mp_code,
    const QStringList& fields,
    const bool& sum)
{
  ClausesT clauses{};

  if (mp_code == "")
    mp_code = CMachine::getSelectedMProfile();

  if (mp_code != CConsts::ALL)
    clauses.push_back(ModelClause("wa_mp_code", mp_code));

  QueryRes addresses_info = DbModel::select(
    stbl_machine_wallet_addresses,
    fields,
    clauses
  );

  if (sum == false)
    return {addresses_info.records, {}};

  CDateT nowT = CUtils::getNow();
  QV2DicT addressDict = {};
  QString complete_query = "select wf_address, SUM(wf_o_value) mat_sum, COUNT(*) mat_count FROM " + stbl_machine_wallet_funds + " ";
  complete_query += "WHERE wf_mp_code=:wf_mp_code AND wf_mature_date<:wf_mature_date GROUP BY wf_address";
  QueryRes tmpRes = DbModel::customQuery(
    "db_comen_wallets",
    complete_query,
    {"wf_address", "mat_sum", "mat_count"},
    0,
    {{"wf_mp_code", mp_code}, {"wf_mature_date", nowT}});

  for (QVDicT elm: tmpRes.records)
  {
    CAddressT add = elm.value("wf_address").toString();
    if (!addressDict.keys().contains(add))
      addressDict[add] = QVDicT {
      {"mat_sum", elm.value("mat_sum").toDouble()},
      {"mat_count", elm.value("mat_count").toDouble()}};
  }

  // unmaturated coins
  complete_query = "SELECT wf_address, SUM(wf_o_value) unmat_sum, COUNT(*) unmat_count FROM " + stbl_machine_wallet_funds + " ";
  complete_query += "WHERE wf_mp_code=:wf_mp_code AND wf_mature_date >= :wf_mature_date GROUP BY wf_address";
  tmpRes = DbModel::customQuery(
    "db_comen_wallets",
    complete_query,
    {"wf_address", "unmat_sum", "unmat_count"},
    0,
    {{"wf_mp_code", mp_code}, {"wf_mature_date", CUtils::getNow()}});

  for (QVDicT elm: tmpRes.records)
  {
    CAddressT add = elm.value("wf_address").toString();
    if (!addressDict.keys().contains(add))
    {
      addressDict[add] = QVDicT {
      {"unmat_sum", elm.value("unmat_sum").toDouble()},
      {"unmat_count", elm.value("unmat_count").toDouble()}};
    }else{
      addressDict[add]["unmat_sum"] = elm.value("unmat_sum").toDouble();
      addressDict[add]["unmat_count"] = elm.value("unmat_count").toDouble();
    }
  }

  return {addresses_info.records, addressDict};

}

QVDicT Wallet::convertToValues(const WalletAddress& w_address)
{
  QVDicT values {
    {"wa_mp_code", w_address.m_mp_code},
    {"wa_address", w_address.m_address},
    {"wa_title", w_address.m_title},
    {"wa_creation_date", w_address.m_creation_date},
    {"wa_detail", CUtils::serializeJson(w_address.m_unlock_doc->exportJson())}
  };
  return values;
}

bool Wallet::insertAddress(const WalletAddress& w_address)
{
  auto[status, addresses] = searchWalletAdress(
    QStringList {w_address.m_address},
    w_address.m_mp_code,
    {"wa_address"});
  if (!status)
    return false;

  if (addresses.size() == 0)
  {
    QVDicT values = convertToValues(w_address);
    CLog::log("Insert new address to machine wallet" + CUtils::dumpIt(values), "app", "trace");
    DbModel::insert(
      stbl_machine_wallet_addresses,
      values
    );
    return true;

  }

  return false;
}

QVDRecordsT Wallet::getAddressesInfo(
  const QStringList& addresses,
  const QStringList& fields)
{
  QString mp_code = CMachine::getSelectedMProfile();
  auto[status, res] = searchWalletAdress(addresses, mp_code, fields);
  if (!status)
      return {};
  return res;
}

GenRes Wallet::createANewAddress(
  const QString& signature_type,
  const QString& signature_mod,
  const QString& signature_version)
{
  auto[status, unlock_doc] = CAddress::createANewAddress(
    signature_type,
    signature_mod,
    signature_version);
  if (!status)
    return {false, ""};// {false, "Couldn't creat ECDSA key pairs (for public channel)"};

  insertAddress( WalletAddress (
    &unlock_doc,
    CMachine::getSelectedMProfile(),   // mp code
    signature_type + " address (" + signature_mod + " signatures) ver(" + signature_version + ")",
    CUtils::getNow()));

  CGUI::signalUpdateWalletCoins();
  CGUI::signalUpdateWalletAccounts();

  return {true, unlock_doc.m_account_address};
}

GenRes Wallet::getAnOutputAddress(
  bool make_new_address,
  const QString& signature_type,
  const QString& signature_mod,
  const QString& signature_version)
{
  CAddressT the_address;
  if (make_new_address)
  {
    return createANewAddress(signature_type, signature_mod, signature_version);
  }

  auto[wallet_controlled_accounts, details] = getAddressesList();
  the_address = wallet_controlled_accounts[rand() * wallet_controlled_accounts.size()].value("wa_address").toString();
  return {(the_address != ""), the_address};
}
