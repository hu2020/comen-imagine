#include "stable.h"

#include "lib/transactions/basic_transactions/utxo/utxo_handler.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"

#include "wallet.h"

const QString Wallet::stbl_machine_wallet_addresses = "c_machine_wallet_addresses";
const QStringList Wallet::stbl_machine_wallet_addresses_fields = {"wa_id", "wa_mp_code", "wa_address", "wa_title", "wa_detail", "wa_creation_date"};

const QString Wallet::stbl_machine_wallet_funds = "c_machine_wallet_funds";
const QStringList Wallet::stbl_machine_wallet_funds_fields = {"wf_id", "wf_mp_code", "wf_address", "wf_block_hash", "wf_trx_type", "wf_trx_hash", "wf_o_index", "wf_o_value", "wf_creation_date", "wf_mature_date", "wf_last_modified"};

Wallet::Wallet()
{

}
