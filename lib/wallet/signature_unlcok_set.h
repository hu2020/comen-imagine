#ifndef SIGNATUREUNLCOKSET_H
#define SIGNATUREUNLCOKSET_H


#include "signature_set.h"

using namespace std;

class SignatureUnlcokSet
{

public:
    string m_sType; // signature type
    string m_sVer = "0.0.0"; // signature version
    vector<SignatureSet> m_sSets;
    vector<string> m_merkle_proof;
    string m_left_hash = ""; // merkle left hash
    string m_salt; // encryption salt

    SignatureUnlcokSet();

};

#endif // SIGNATUREUNLCOKSET_H
