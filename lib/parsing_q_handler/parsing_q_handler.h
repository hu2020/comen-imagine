#ifndef PARSINGQHANDLER_H
#define PARSINGQHANDLER_H

class Block;

#include "lib/block/block_types/block.h"
#include "lib/block/block_types/block_factory.h"
#include "lib/block_utils.h"
#include "lib/dag/dag.h"
#include "lib/dag/missed_blocks_handler.h"
#include "lib/block/block_types/block_coinbase/coinbase_utxo_handler.h"
#include "lib/dag/normal_block/normal_utxo_handler.h"

class ParsingQHandler
{
public:
  static const QString stbl_parsing_q;
  static const QStringList stbl_parsing_q_fields;
  static const QString stbldev_parsing_q;

  ParsingQHandler();
  static void loopSmartPullFromParsingQ();

  static std::tuple<bool, bool> handlePulledPacket(const QVDicT& packet);

  static std::tuple<bool, bool> parsePureBlock(
    const QString& sender,
    const QString& pq_type,
    const Block* block,
    const QString& connection_type,
    const QString& receive_date);

  static std::tuple<bool, bool> pushToParsingQ(
      const QJsonObject& message,
      const QString& creation_date,
      const QString& type,
      const QString& code,
      const QString& sender,
      const QString& connection_type,
      QStringList prerequisites = {});

  static bool rmoveFromParsingQ(const ClausesT& clauses);


  // -  -  -  -  -  -  -  Queue picker
  static std::tuple<QString, QVDicT> prepareSmartQuery(const uint16_t& limit = 1);

  static bool increaseToparseAttempsCountSync(const QVDicT& packet);

  static bool smartPullQ();

  // -  -  -  -  -  -  -  Queue utils
  static QVDRecordsT searchParsingQ(
    const ClausesT& clauses = {},
    const QStringList& fields = stbl_parsing_q_fields,
    const OrderT& order = {},
    const int& limit = 0);

  static bool appendPrerequisites(
    const QString& block_hash,
    const QStringList& prerequisites,
    const QString& pq_type = "");

  static std::tuple<bool, bool> ancestorsConroll(
    const QString& pq_type,
    const Block* block);

  static void removePrerequisites(const QString& blockHash);

};

#endif // PARSINGQHANDLER_H
