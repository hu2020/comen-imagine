#include "stable.h"

#include "multi_language.h"

const std::vector<QSDicT > MultiLanguage::s_languages = {
    {{"iso", "ara"}, {"enName", "Arabic"}, {"lName", "العربية" }},
    {{"iso", "eng"}, {"enName", "English"}, {"lName", "English" }},
    {{"iso", "fas"}, {"enName", "Persian"}, {"lName", "فارسی" }},
    {{"iso", "fra"}, {"enName", "French"}, {"lName", "French" }},
    {{"iso", "hye"}, {"enName", "Armenian"}, {"lName", "Հայերեն" }},
    {{"iso", "ita"}, {"enName", "Italian"}, {"lName", "Italiano" }},
    {{"iso", "jpn"}, {"enName", "Japanese"}, {"lName", "日本語 (にほんご)" }},
    {{"iso", "srd"}, {"enName", "Sardinian"}, {"lName", "sardu" }},
    {{"iso", "snd"}, {"enName", "Sindhi"}, {"lName", "सिन्धी, سنڌي، سندھی" }},
    {{"iso", "sin"}, {"enName", "Sinhalese"}, {"lName", "සිංහල" }},
    {{"iso", "spa"}, {"enName", "Spanish"}, {"lName", "español" }},
    {{"iso", "tur"}, {"enName", "Turkish"}, {"lName", "Türkçe" }},
};

QHash<QString, QSDicT > MultiLanguage::s_map_language_code_to_language_name = {};

MultiLanguage::MultiLanguage()
{

}

QSDicT MultiLanguage::mapLangCodeToLangName(const QString& lang_code)
{
  if (s_map_language_code_to_language_name.keys().size() == 0)
    for (QSDicT a_lang: s_languages)
      s_map_language_code_to_language_name.insert(a_lang.value("iso"), QSDicT {
        {"enName", a_lang.value("enName")},
        {"lName", a_lang.value("lName")}});

  if (s_map_language_code_to_language_name.keys().contains(lang_code))
    return s_map_language_code_to_language_name[lang_code];

  return {};
}


