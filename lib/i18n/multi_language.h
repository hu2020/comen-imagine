#ifndef MULTILANGUAGE_H
#define MULTILANGUAGE_H

#include <vector>
#include <QString>
#include <QHash>

class MultiLanguage
{
public:
  MultiLanguage();

  const static std::vector<QSDicT> s_languages;
  static QHash<QString, QSDicT> s_map_language_code_to_language_name;

  static QSDicT mapLangCodeToLangName(const QString& lang_code);


};

#endif // MULTILANGUAGE_H
