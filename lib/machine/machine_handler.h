#ifndef CMACHINE_H
#define CMACHINE_H

class MainWindow;
class CUtils;
class TransientBlockInfo;


#include "lib/transactions/basic_transactions/signature_structure_handler/individual_signature.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_set.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_document.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"

#include "lib/address/address_handler.h"
#include "lib/wallet/wallet.h"
#include "lib/k_v_handler.h"
#include "lib/dag/dag.h"


class MachineTransientBalances
{
public:
  CMPAIValueT m_one_cycle_issued = 0;
  uint64_t m_cycle_counts_from_began = 0;
  uint64_t m_distinct_coinbases_count = 0;
  CMPAIValueT m_total_minted_coins = 0;
  CMPAIValueT m_total_spendable_coins = 0;

  QVDRecordsT m_wallet_spendable_UTXOs {};

  CMPAIValueT m_cb_not_imported_sum = 0;
  QVDRecordsT m_cb_not_imported_processed_outputs = {};
  CMPAIValueT m_cb_not_imported_coinbase_value = 0;

  CMPAIValueT m_normal_not_imported_sum = 0;
  CMPAIValueT m_waited_treasury_incomes = 0;

  CMPAIValueT m_cb_floorished_coins = 0;

  CMPAISValueT m_final_balance = 0;
  CMPAIValueT m_wallet_wealth_value = 0;

  QVDRecordsT m_coins_existance = {};

};


class EmailSettings
{
public:
  QString m_address = "abc@def.gh";
  QString m_password = "";
  QString m_income_IMAP = "993";
  QString m_income_POP3 = "995";
  QString m_incoming_mail_server = "";
  QString m_outgoing_mail_server = "";
  QString m_outgoing_SMTP = "465";
  QString m_fetching_interval_by_minute = "5";  // it depends on smtp server, but less than 5 minute is useless
  QString m_PGP_private_key = "";
  QString m_PGP_public_key = "";

  EmailSettings()
  {}
  void importJson(const QJsonObject& obj);
  QJsonObject exportJson() const;
};

class MPSetting
{
public:
  MPSetting();

  EmailSettings m_public_email;
  EmailSettings m_private_email;

  QString m_machine_alias = "imagineNode";
  UnlockDocument* m_backer_detail{};
  QString m_language = CConsts::DEFAULT_LANG;
  QString m_term_of_services = CConsts::NO;
  QJsonArray m_already_presented_neighbors = {};

  void importJson(const QJsonObject& obj);
  QJsonObject exportJson() const;

};

class MachineProfile
{
public:
  QString m_mp_code = "";
  QString m_mp_name = "";
  QString m_mp_last_modified = "";
  MPSetting m_mp_settings;
  QJsonObject m_this_json;

  // provides a new template profile
  MachineProfile();
  // retrieve profile from DB and sets to object
  MachineProfile(const QString& mp_code);
  // set passed JSon object to itself
  MachineProfile(const QJsonObject& profile);

  void importJson(const QJsonObject& profile);
  QJsonObject exportJson() const;

};

struct CoinsVisibilityRes {
  bool status = false;
  QStringList records = {};
  bool is_visible = false;
};

class CMachine
{


public:
  CMachine(const CMachine&) = delete;


  const static QString stbl_machine_profiles;
  const static QString stbl_machine_neighbors;

  const static QString stb_machine_block_buffer;
  const static QStringList stb_machine_block_buffer_fields;

  static const QString stbl_machine_onchain_contracts;
  static const QStringList stbl_machine_onchain_contracts_fields;

  Config* m_global_configs {};
  uint32_t m_recorded_blocks_in_db = 0; // TODO: remove this variable(mechanism) after fixing sqlite database lock problem
  QStringList m_cache_coins_visibility = {}; // TODO: remove this variable(mechanism) after fixing sqlite database lock problem and bloom filter implementation
  QVDRecordsT m_cache_spendable_coins = {}; // TODO: remove this variable(mechanism) after fixing sqlite database lock problem
  QVDRecordsT m_DAG_cached_blocks; // TODO: optimize it ASAP
  QStringList m_DAG_cached_block_hashes = {}; // TODO: optimize it ASAP

  MachineProfile m_profile;
//  static MainWindow* getMW(){ return get().m_mw; };


  static CMachine& get(){return s_instance;};

  static void parseArgs(int argc, char *argv[], int manual_clone_id = 0);
  static void onAboutToQuit(MainWindow* w){ get().IonAboutToQuit(w); };

  static QString getLaunchDate(){return get().IgetLaunchDate(); };
  static void setLaunchDateAndCloneId(const CDateT& cDate, uint8_t clone_id = 0){return get().IsetLaunchDateAndCloneId(cDate, clone_id); };

  static void emptyDB();

  // machine_handler.cpp
  GenRes initDefaultProfile();
  static bool bootMachine(){return get().IbootMachine();}

  static void setDAGIsInitialized(bool status){get().s_DAG_is_initialized = status; }
  static bool getDAGIsInitialized(){ return get().s_DAG_is_initialized; }

  static std::tuple<bool, QVDRecordsT> cachedSpendableCoins(
    const QString& action = "read",
    const QVDRecordsT& coins = {},
    const CBlockHashT& visible_by = "",
    const CCoinCodeT& the_coin = "") { return get().IcachedSpendableCoins(action, coins, visible_by, the_coin); };

  static std::tuple<bool, QVDRecordsT&> cachedBlocks(
    const QString& action = "read",
    const QVDRecordsT& blocks = {},
    const QString& status = "") { return get().IcachedBlocks(action, blocks, status); };

  static std::tuple<bool, QStringList&> cachedBlockHashes(
    const QString& action = "read",
    const QStringList& block_hashes = {}) { return get().IcachedBlockHashes(action, block_hashes); };

  static CoinsVisibilityRes cachedCoinsVisibility(
    const QString& action = "read",
    const QStringList& entries = {}){ return get().IcachedCoinsVisibility(action, entries); };

  static bool setCloneDev(const uint8_t clone_id, const bool is_develop_mod){return get().IsetCloneDev(clone_id, is_develop_mod);}
  static bool isDevelopMod(){ return get().IisDevelopMod();}
  static uint8_t getAppCloneId(){return get().IgetAppCloneId();}
  static QString getAppCloneIdStr(){return (get().IgetAppCloneId()>0) ? QString::number(get().IgetAppCloneId()) : "";}
  static bool createFolders(){return get().IcreateFolders();}
  static QString getHDPath(){return get().IgetHDPath();}
  static QString getReportsPath(){return get().IgetReportsPath();}
  static QString getInboxPath(){return get().IgetInboxPath();}
  static QString getOutboxPath(){return get().IgetOutboxPath();}
  static QString getLogsPath(){return get().IgetLogsPath();}
  static QString getDAGBackup(){return get().IgetDAGBackup();}

  static bool shouldLoopThreads(){return get().IshouldLoopThreads();}
  static void setShouldLoopThreads(const bool v){return get().IsetShouldLoopThreads(v); }

  static bool databasesAreCreated(){return get().IdatabasesAreCreated();}
  static void setDatabasesAreCreated(const bool v){return get().IsetDatabasesAreCreated(v); }

  static bool canStartLazyLoadings(){return get().IcanStartLazyLoadings();}
  static void setCanStartLazyLoadings(bool v){ get().IsetCanStartLazyLoadings(v);}
  static void reportThreadStatus(const QString& thread_prefix, const QString& thread_code, const QString& thread_status){ get().IreportThreadStatus(thread_prefix, thread_code, thread_status); }
  static QString mapThreadCodeToPrefix(const QString& code){ return get().ImapThreadCodeToPrefix(code);}


  static QString getBackerAddress(){return get().IgetBackerAddress();}
  static UnlockDocument* getBackerDetails(){return get().IgetBackerDetails();}
  static MachineProfile getProfile(){return get().IgetProfile();}
  static EmailSettings getPubEmailInfo(){return get().IgetPubEmailInfo();}
  static EmailSettings getPrivEmailInfo(){return get().IgetPrivEmailInfo();}
  static bool setPublicEmailAddress(const QString&  v){return get().IsetPublicEmailAddress(v);}
  static bool setPublicEmailInterval(const QString&  v){return get().IsetPublicEmailInterval(v);}
  static bool setPublicEmailIncomeHost(const QString&  v){return get().IsetPublicEmailIncomeHost(v);}
  static bool setPublicEmailPassword(const QString&  v){return get().IsetPublicEmailPassword(v);}
  static bool setPublicEmailIncomeIMAP(const QString&  v){return get().IsetPublicEmailIncomeIMAP(v);}
  static bool setPublicEmailIncomePOP(const QString&  v){return get().IsetPublicEmailIncomePOP(v);}
  static bool setPublicEmailOutgoingSMTP(const QString&  v){return get().IsetPublicEmailOutgoingSMTP(v);}
  static bool setPublicEmailOutgoingHost(const QString&  v){return get().IsetPublicEmailOutgoingHost(v);}
  static bool setTermOfServices(const QString&  v){return get().IsetTermOfServices(v);}
  static bool saveSettings(){return get().IsaveSettings();}
  static QJsonObject getLastSyncStatus(){return get().IgetLastSyncStatus();};
  static bool isInSyncProcess(const bool& force_to_control_based_on_DAG_status = false){return get().IisInSyncProcess(force_to_control_based_on_DAG_status);}
  static void setIsInSyncProcess(const bool status, const CDateT& cDate){ get().IsetIsInSyncProcess(status, cDate);}
  static QString getSelectedMProfile(){ return get().IgetSelectedMProfile(); };

  static bool isGUIConnected(){ return get().IisGUIConnected(); }
  static void setIsGUIConnected(const bool status){ get().IsetIsGUIConnected(status); }

  bool loadSelectedProfile();

  static std::tuple<bool, QString, UnlockSet, QStringList> signByMachineKey(
    const QString& signMsg,
    const CSigIndexT& unlockIndex = 0);

  static uint64_t getCycleByMinutes();
  static TimeBySecT getPopEmailGap();
  static TimeBySecT getParsingQGap();
  static TimeBySecT getSendingQGap();
  static TimeBySecT getSendEmailGap();
  static TimeBySecT getBlockInvokeGap();
  static TimeBySecT getInvokeLeavesGap();
  static TimeBySecT getNBUTXOsImportGap();
  static TimeBySecT getCoinbaseImportGap();
  static TimeBySecT getHardDiskReadingGap();
  static TimeBySecT getAcceptableBlocksGap();
  static TimeBySecT getINamesSettlementGap();
  static TimeBySecT getConcludeTreatmentGap();
  static TimeBySecT getPrerequisitiesRemoverGap();

  static double getMinPollingTimeframeByHour();
  static TimeByHoursT getMinVotingTimeframe();

  static QString getCachePath(QString appCloneId = "");

  static double getMachineServiceInterests(
    const QString& dType,
    const QString& dClass = "",
    const DocLenT& dLen = 0,
    const DocLenT& extra_length = 0,
    const int& supported_P4P_trx_count = 1)
  {
      return get().IgetMachineServiceInterests(
        dType,
        dClass,
        dLen,
        extra_length,
        supported_P4P_trx_count);
  }


  //  -  -  -  -  -  machine_backup.cpp


  //  -  -  -  -  -  machine_services_interests.cpp


  //  -  -  -  -  -  neighbors handler

  static QVDRecordsT getNeighbors(
    const QString& neighbor_type = "",
    const QString& connection_status = "",
    const QString& mp_code = getSelectedMProfile(),
    const QString& n_id = "",
    const QString& n_email = ""){ return get().IgetNeighbors(neighbor_type, connection_status, mp_code, n_id, n_email); };

  static QVDRecordsT getActiveNeighbors(const QString& mp_code = getSelectedMProfile());

  static std::tuple<bool, QString> addANewNeighbor(
    const QString& email,
    const QString& connection_type,
    const QString& public_key = "",
    const QString& mp_code = getSelectedMProfile(),
    const QString& is_active = CConsts::YES,
    const QJsonObject& neighbor_info = QJsonObject(),
    QString creation_date = "");

  static bool handshakeNeighbor(const QString& n_id, const QString& connection_type);

  static std::tuple<bool, bool> parseHandshake(
    const QString& sender_email,
    const QJsonObject& message,
    const QString& connection_type);

  static bool floodEmailToNeighbors(
    const QString& email,
    QString PGP_public_key = ""){ return get().IfloodEmailToNeighbors(email, PGP_public_key); };

  static bool setAlreadyPresentedNeighbors(const QJsonArray& already_presented_neighbors){ return get().IsetAlreadyPresentedNeighbors(already_presented_neighbors); }

  static QJsonArray getAlreadyPresentedNeighbors(){ return get().IgetAlreadyPresentedNeighbors(); }

  static bool deleteNeighbors(
    const QString& n_id,
    const QString& connection_type,
    const QString& mp_code = getSelectedMProfile()){return get().IdeleteNeighbors(n_id, connection_type, mp_code);}

  static std::tuple<bool, bool> parseNiceToMeetYou(
    const QString& sender_email,
    const QJsonObject& message,
    const QString& connection_type);

  //  -  -  -  accounts balances
  static MachineTransientBalances machineBalanceChk();


  //  -  -  -  block buffer part
  static QVDRecordsT searchBufferedDocs(
    const ClausesT& clauses = {},
    const QStringList& fields = stb_machine_block_buffer_fields,
    const OrderT& order = {},
    const uint64_t limit = 0);

  static std::tuple<bool, QString> pushToBlockBuffer(
    const Document* doc,
    const CMPAIValueT dp_cost,
    const QString& mp_code = getSelectedMProfile());

  static std::tuple<bool, QString> broadcastBlock(
    const QString& cost_pay_mode = "normal",
    const QString& create_date_type = "");

  static std::tuple<bool, bool, QString> fetchBufferedTransactions(
    Block* block,
    TransientBlockInfo& transient_block_info);

  static std::tuple<bool, bool, QString> retrieveAndGroupBufferedDocuments(
    Block* block,
    TransientBlockInfo& transient_block_info);

  static bool removeFromBuffer(const ClausesT& clauses);


  //  -  -  -  on-chain contracts part
  static QJsonArray searchInMyOnchainContracts(
    const ClausesT& clauses,
    const QStringList& fields = stbl_machine_onchain_contracts_fields,
    const OrderT order = {},
    const uint64_t limit = 0);



private:
  CMachine(){};
  static CMachine s_instance;
//  MainWindow* m_mw;

  QSDicT m_threads_status {};
  QSDicT m_map_thread_code_to_prefix {};
  uint8_t m_clone_id = 0;
  bool m_is_develop_mod = false;
  CDateT m_develop_launch_date = "";

  bool m_machine_is_loaded = false;
  bool m_is_GUI_connected = false;
  QString m_selected_profile = "";

  bool m_is_in_sync_process = true;
  CDateT m_last_sync_status_check = "";

  bool m_db_is_connected = false;
  bool s_DAG_is_initialized;
  bool m_should_loop_threads = true;
  bool m_databases_are_created = false;
  bool m_can_start_lazy_loadings = false;

  bool IbootMachine();
  void IsetLaunchDateAndCloneId(const CDateT& cDate, uint8_t clone_id = 0);
  QString IgetLaunchDate();
  QString IgetBackerAddress();
  UnlockDocument* IgetBackerDetails();
  bool IsetPublicEmailAddress(const QString&  v);
  bool IsetPublicEmailInterval(const QString&  v);
  bool IsetPublicEmailIncomeHost(const QString&  v);
  bool IsetPublicEmailPassword(const QString&  v);
  bool IsetPublicEmailIncomeIMAP(const QString&  v);
  bool IsetPublicEmailIncomePOP(const QString&  v);
  bool IsetPublicEmailOutgoingSMTP(const QString&  v);
  bool IsetPublicEmailOutgoingHost(const QString&  v);
  bool IsetTermOfServices(const QString&  v);
  bool IsaveSettings();
  bool ImaybeAddSeedNeighbors();

  QString IgetSelectedMProfile();

  QJsonObject IgetLastSyncStatus();
  bool IinitLastSyncStatus();
  bool IisInSyncProcess(const bool& force_to_control_based_on_DAG_status = false);
  void IsetIsInSyncProcess(const bool status, const CDateT& cDate)
  {
    m_is_in_sync_process = status;
    m_last_sync_status_check = cDate;
  }

  void IsetIsGUIConnected(const bool status)
  {
    m_is_GUI_connected = status;
  }

  bool IisGUIConnected()
  {
    return m_is_GUI_connected;
  }

  static double IgetMachineServiceInterests(
    const QString& dType,
    const QString& dClass = "",
    const DocLenT& dLen = 0,
    const DocLenT& extra_length = 0,
    const int& supported_P4P_trx_count = 1);

  uint8_t IgetAppCloneId()
  {
    return m_clone_id;
  }

  void IsetShouldLoopThreads(const bool v)
  {
    m_should_loop_threads = v;
  }

  bool IshouldLoopThreads()
  {
    return m_should_loop_threads;
  }

  void IsetDatabasesAreCreated(const bool v)
  {
    m_databases_are_created = v;
  }

  bool IdatabasesAreCreated()
  {
    return m_databases_are_created;
  }

  bool IcanStartLazyLoadings()
  {
    return m_can_start_lazy_loadings;
  }

  void IsetCanStartLazyLoadings(bool v)
  {
    m_can_start_lazy_loadings = v;
  }

  bool IisDevelopMod()
  {
    return m_is_develop_mod;
  }

  bool IsetCloneDev(const uint8_t clone_id, const bool is_develop_mod) //, MainWindow* mw
  {
//    m_mw = mw;  //MainWindow* mw
    m_clone_id = clone_id;
    m_is_develop_mod = is_develop_mod;
    return true;
  }

  bool IcreateFolders();
  QString IgetReportsPath();
  QString IgetHDPath();
  QString IgetInboxPath();
  QString IgetOutboxPath();
  QString IgetLogsPath();
  QString IgetDAGBackup();

  MachineProfile IgetProfile()
  {
    return m_profile;
  }

  EmailSettings IgetPubEmailInfo(){return m_profile.m_mp_settings.m_public_email;}
  EmailSettings IgetPrivEmailInfo(){return m_profile.m_mp_settings.m_private_email;}

  std::tuple<bool, QVDRecordsT&> IcachedBlocks(
    const QString& action = "read",
    const QVDRecordsT& blocks = {},
    const QString& status = "");

  std::tuple<bool, QStringList&> IcachedBlockHashes(
    const QString& action = "read",
    const QStringList& block_hashes = {});

  std::tuple<bool, QVDRecordsT> IcachedSpendableCoins(
    const QString& action,
    const QVDRecordsT& coins = {},
    const CBlockHashT& visible_by = "",
    const CCoinCodeT& the_coin = "");

  CoinsVisibilityRes IcachedCoinsVisibility(
    const QString& action,
    const QStringList& entries);

  //  -  -  -  -  -  neighbors handler
  QVDRecordsT IgetNeighbors(
    const QString& neighbor_type = "",
    const QString& connection_status = "",
    const QString& mp_code = getSelectedMProfile(),
    const QString& n_id = "",
    const QString& n_email = "");

  bool IfloodEmailToNeighbors(
    const QString& email,
    QString PGP_public_key = "");

  QJsonArray IgetAlreadyPresentedNeighbors(){ return m_profile.m_mp_settings.m_already_presented_neighbors; }
  bool IsetAlreadyPresentedNeighbors(const QJsonArray& already_presented_neighbors){ m_profile.m_mp_settings.m_already_presented_neighbors = already_presented_neighbors; return true; }
  bool IdeleteNeighbors(
    const QString& n_id,
    const QString& connection_type,
    const QString& mp_code = getSelectedMProfile());

  void IonAboutToQuit(MainWindow* w);

  void IreportThreadStatus(
    const QString& thread_prefix,
    const QString& thread_code,
    const QString& thread_status);

  QString ImapThreadCodeToPrefix(const QString& code);

};

#endif // CMACHINE_H
