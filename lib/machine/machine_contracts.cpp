#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"

//#include "lib/network/network_handler.h"
//#include "lib/messaging_protocol/message_handler.h"
//#include "lib/messaging_protocol/graphql_handler.h"
//#include "lib/sending_q_handler/sending_q_handler.h"

#include "machine_handler.h"

const QString CMachine::stbl_machine_onchain_contracts = "c_machine_onchain_contracts";
const QStringList CMachine::stbl_machine_onchain_contracts_fields = {"lc_id", "lc_type", "lc_class", "lc_ref_hash", "lc_descriptions", "lc_body"};


QJsonArray CMachine::searchInMyOnchainContracts(
    const ClausesT& clauses,
    const QStringList& fields,
    const OrderT order,
    const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_machine_onchain_contracts,
    fields,
    clauses,
    order,
    limit);
  if (res.records.size() == 0)
    return {};

  CDateT max_date = CUtils::getACycleRange().to;

  QJsonArray contracts {};

  for (QVDicT a_contract: res.records)
  {
    QJsonObject contract_body = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(a_contract.value("lc_body").toString()).content);
    CLog::log("contract_body::::: " + CUtils::serializeJson(contract_body));

    QJsonObject json_contract {
      {"lc_id", a_contract["lc_id"].toInt()},
      {"lc_type", a_contract["lc_type"].toString()},
      {"lc_class", a_contract["lc_class"].toString()},
      {"lc_ref_hash", a_contract["lc_ref_hash"].toString()},  // pledge docHash
      {"contract_body", contract_body},
    };

    bool is_it_mine = false;
    QJsonArray actions {};
    if (a_contract.value("lc_type").toString() == CConsts::DOC_TYPES::Pledge)
    {
      auto the_pledge = DocumentFactory::create(contract_body);
      auto[signer_status, signer_type, by_type] = GeneralPledgeHandler::recognizeSignerTypeInfo(the_pledge);

      if (signer_type == "pledgee") {
        is_it_mine = true;
        actions.push_back(QJsonObject {
          {"actTitle", "Unpledge By Pledgee"},
          {"actCode", CConsts::PLEDGE_CONCLUDER_TYPES::ByPledgee},
          {"actSubjType", CConsts::DOC_TYPES::Pledge},
          {"actSubjCode", a_contract.value("lc_id").toDouble()}});
      }

      if (signer_type == "arbiter") {
        is_it_mine = true;
        actions.push_back(QJsonObject {
          {"actTitle", "Unpledge By Arbiter"},
          {"actCode", CConsts::PLEDGE_CONCLUDER_TYPES::ByArbiter},
          {"actSubjType", CConsts::DOC_TYPES::Pledge},
          {"actSubjCode", a_contract.value("lc_id").toDouble()}});
      }

      if (signer_type == "pledger") {
          is_it_mine = true;
          actions.push_back(QJsonObject {
            {"actTitle", "Unpledge By Pledger"},
            {"actCode", CConsts::PLEDGE_CONCLUDER_TYPES::ByPledger},
            {"actSubjType", CConsts::DOC_TYPES::Pledge},
            {"actSubjCode", a_contract.value("lc_id").toDouble()}});

        }
    }
    json_contract["actions"] = actions;

    if (is_it_mine)
    {
      // retrieve complete info about contract
      QVDRecordsT pledges = GeneralPledgeHandler::searchInPledgedAccounts({{"pgd_hash", contract_body.value("dHash").toString()}});
      if (pledges.size() == 1)
      {
        QVDicT pledge = pledges[0];
        bool is_active = false;
        if ((pledge.value("pgd_status").toString() == CConsts::OPEN && pledge.value("pgd_activate_date").toString() < max_date) ||
            (pledge.value("pgd_status").toString() == CConsts::CLOSE && pledge.value("pgd_close_date").toString() > max_date))
          is_active = true;

        QString status;
        if (is_active) {
          status = "Active";

        } else {
          if (pledge.value("pgd_activate_date").toString() > max_date)
          {
            status = "Waiting to be active";

          } else if ((CUtils::isValidDateForamt(pledge.value("pgd_close_date").toString())) &&
                     (pledge.value("pgd_close_date").toString() < max_date)) {
            status = "Closed";

          }
        }

        CDateT real_close_date = CUtils::isValidDateForamt(pledge.value("pgd_close_date").toString()) ? pledge.value("pgd_close_date").toString() : "-";
        json_contract["complementaryInfo"] = QJsonObject {
          {"status", status},
          {"realActivateDate", pledge.value("pgd_activate_date").toString()},
          {"realCloseDate", real_close_date }};
      }

      contracts.push_back(json_contract);
    }
  }

  return contracts;
}

