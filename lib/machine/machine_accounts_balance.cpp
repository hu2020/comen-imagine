#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/messaging_protocol/message_handler.h"
#include "lib/messaging_protocol/graphql_handler.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/network/network_handler.h"
#include "lib/block/block_types/block_coinbase/coinbase_issuer.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/transactions/basic_transactions/utxo/utxo_handler.h"
#include "lib/wallet/wallet.h"

#include "machine_handler.h"


MachineTransientBalances CMachine::machineBalanceChk()
{

  uint64_t DAG_age_as_minutes = CUtils::timeDiff(CMachine::getLaunchDate()).asMinutes;
  uint64_t cycle_counts_from_began = CUtils::CFloor(DAG_age_as_minutes / getCycleByMinutes()) + 1;
  auto[one_cycle_max_coins_, one_cycle_issued, sum_shares_, share_amount_per_holder_] = CoinbaseIssuer::calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore();
  Q_UNUSED(one_cycle_max_coins_);
  Q_UNUSED(sum_shares_);
  Q_UNUSED(share_amount_per_holder_);

  uint64_t distinct_coinbases_count = DAG::getCyclesList().size();

  // also retrieve treasury unspendables(if exist)
  CMPAIValueT waited_treasury_incomes = TreasuryHandler::getWaitedIncomes();

  auto[
    total_spendable_coins,
    utxos_,
    utxos_dict_] = UTXOHandler::getSpendablesInfo();
  Q_UNUSED(utxos_);
  Q_UNUSED(utxos_dict_);

  auto[
    cb_not_imported_sum,
    cb_not_imported_processed_outputs,
    cb_not_imported_coinbase_value] = DAG::getNotImportedCoinbaseBlocks();

  auto[
    normal_not_imported_sum,
    normal_not_imported_dbl_spends,
    normal_not_imported_processed_outputs] = DAG::getNotImportedNormalBlock();
    Q_UNUSED(normal_not_imported_dbl_spends);
    Q_UNUSED(normal_not_imported_processed_outputs);

  auto[
    cb_floorished_coins,
    cb_stat_burned_by_block,
    total_minted_coins,
    cb_stat_outputs_by_block,
    cb_stat_waited_coinbases_to_be_spendable] = DAG::getCBBlocksStat();
    Q_UNUSED(cb_stat_burned_by_block);
    Q_UNUSED(cb_stat_outputs_by_block);
    Q_UNUSED(cb_stat_waited_coinbases_to_be_spendable);

  CMPAISValueT final_balance =
      total_minted_coins -
      total_spendable_coins -
      cb_not_imported_sum -
      normal_not_imported_sum -
      cb_floorished_coins - // the pais lost because of Math.floor
      waited_treasury_incomes;

  QString repOut = "\n --------------- \n\nminted PAIs\t" + CUtils::microPAIToPAI6(total_minted_coins) +" \t-" + CUtils::microPAIToPAI6(total_minted_coins) +" ";
  repOut += "\n-total_spendable_coins\t-" + CUtils::microPAIToPAI6(total_spendable_coins) +"     \t" + CUtils::microPAIToPAI6(total_spendable_coins) +" ";
  repOut += "\n-waited coinbases To Be Spendable\t-" + CUtils::microPAIToPAI6(cb_not_imported_sum) +"     \t" + CUtils::microPAIToPAI6(cb_not_imported_sum) +" ";
  repOut += "\n-waited Normals To Be Spendable\t-" + CUtils::microPAIToPAI6(normal_not_imported_sum) +"    \t" + CUtils::microPAIToPAI6(normal_not_imported_sum) +" ";
  repOut += "\n-floorish\t-" + CUtils::microPAIToPAI6(cb_floorished_coins) +"     \t" + CUtils::microPAIToPAI6(cb_floorished_coins) +" ";
  repOut += "\n-waited Treasury Incomes\t-" + CUtils::microPAIToPAI6(waited_treasury_incomes) +"      \t" + CUtils::microPAIToPAI6(waited_treasury_incomes) +" ";
  repOut += "\n-final Balance\t" + CUtils::microPAIToPAI6(final_balance) +"      \t-" + CUtils::microPAIToPAI6(final_balance) +" ";
  CLog::log(repOut, "app", "trace");


  // control trnsactions refLocs are realy moved from UTXOs?
  QStringList used_coins {};
  QVDRecordsT normalWBlocs = DAG::searchInDAG(
    {{"b_type", CConsts::BLOCK_TYPES::Normal}},
    {"b_body"});
  for (QVDicT wBlock: normalWBlocs)
  {
    QJsonObject block = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(wBlock.value("b_body").toString()).content);

    if (block.keys().contains("docs"))
    {
      auto docs = block.value("docs").toArray();
      for (auto aDoc_: docs)
      {
        auto aDoc = aDoc_.toObject();
        if (aDoc.keys().contains("inputs"))
          for (auto input: aDoc.value("inputs").toArray())
            used_coins.append(CUtils::packCoinCode(input.toArray()[0].toString(), input.toArray()[1].toDouble()));
      }
    }
  }

  // retrieve node wealth
  QVDRecordsT wallet_spendable_UTXOs = Wallet::retrieveSpendableCoins(); //will return valid(or invalid in case of doublespend tests) inputs
  QVDRecordsT wallet_wealth_details {};

  CMPAIValueT wallet_wealth_value = 0;
  for (QVDicT anUtxo: wallet_spendable_UTXOs)
  {
    wallet_wealth_value += anUtxo.value("ut_o_value").toDouble();
    wallet_wealth_details.push_back(QVDicT {
      {"coin", CUtils::shortCoinRef(anUtxo.value("ut_coin").toString())},
      {"address", CUtils::shortBech16(anUtxo.value("ut_o_address").toString())},
      {"value", CUtils::microPAIToPAI6(anUtxo.value("ut_o_value").toDouble())},
      {"coin_creation_date", anUtxo.value("ut_ref_creation_date").toString()}});
  }

  QStringList tmpDtl {};
  for (auto i = 0; i < wallet_wealth_details.size(); i++)
  {
    QVDicT x = wallet_wealth_details[i];
    tmpDtl.append(QStringList {
      QString::number(i+1),
      x.value("coin").toString(),
      x.value("address").toString(),
      x.value("coin_creation_date").toString(),
      QString::number(x.value("value").toDouble())}.join(" "));
  }
  QString wallet_wealth_details_str = tmpDtl.join("\n");

  MachineTransientBalances mb = {};

  mb.m_one_cycle_issued = one_cycle_issued;
  mb.m_cycle_counts_from_began = cycle_counts_from_began;
  mb.m_distinct_coinbases_count = distinct_coinbases_count;
  mb.m_total_minted_coins = total_minted_coins;
  mb.m_wallet_spendable_UTXOs = wallet_spendable_UTXOs;
  mb.m_total_spendable_coins = total_spendable_coins;

  mb.m_cb_not_imported_sum = cb_not_imported_sum;
  mb.m_cb_not_imported_processed_outputs = cb_not_imported_processed_outputs;
  mb.m_cb_not_imported_coinbase_value = cb_not_imported_coinbase_value;

  mb.m_normal_not_imported_sum = normal_not_imported_sum;
  mb.m_waited_treasury_incomes = waited_treasury_incomes;
  mb.m_cb_floorished_coins = cb_floorished_coins;
  mb.m_final_balance = final_balance;
  mb.m_wallet_wealth_value = wallet_wealth_value;

  return mb;
}
