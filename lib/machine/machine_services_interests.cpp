#include "stable.h"

#include "machine_handler.h"


double CMachine::IgetMachineServiceInterests(
  const QString& dType,
  const QString& dClass,
  const DocLenT& dLen,
  const DocLenT& extra_length,
  const int& supported_P4P_trx_count)
{
  Q_UNUSED(dClass);
  Q_UNUSED(dLen);
  Q_UNUSED(extra_length);
  Q_UNUSED(supported_P4P_trx_count);
  // TODO: these values must be costumizable for backers
  QHash<QString, double> services_price_coefficient {
    {CConsts::DOC_TYPES::BasicTx, 1.000},    // the output must be 1 or greater. otherwise other nodes reject the block
    {CConsts::DOC_TYPES::FPost, 1.001}};

  if (services_price_coefficient.keys().contains(dType))
      return services_price_coefficient[dType];

  // node doesn't support this type of documents so accept it as a base feePerByte
  return 1.000;
}
