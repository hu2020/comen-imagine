#ifndef BLOCKUTILS_H
#define BLOCKUTILS_H

struct WrapDBObj
{
  QString sfVer = "";
  QString content = "";
};

struct Unwrapped
{
  bool status = false;
  QString version = "0.0.0";
  QString content = "";
};

class BlockUtils
{
public:
  BlockUtils();

  static bool ifAncestorsAreValid(const QStringList& ancestors);

  static std::tuple<bool, uint64_t, uint64_t> retrieveDPCostInfo(
    const Document* doc,
    const QString& backer);

  static Unwrapped wrapSafeContentForDB(
    const QString& content,
    const QString& sfVer = "0.0.0");
  static Unwrapped unwrapSafeContentForDB(const QString& wrapped);
  static Unwrapped unwrapSafeContentForDB(const QVariant& wrapped);

  static QVDicT convertToQVDicT(const QJsonObject& record);
  static QJsonObject convertToJSonObject(const QVDicT& record);
  static QJsonArray convertToJSonArray(const QVDRecordsT& records);

  static QStringList normalizeAncestors(QStringList ancestors);

  static MerkleNodeData getDocumentMerkleProof(
    const QJsonObject& block,
    const CDocHashT& docHash);

};

#endif // BLOCKUTILS_H
