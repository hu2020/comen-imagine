#ifndef FREEDOCUMENTSINRELATEDBLOCK_H
#define FREEDOCUMENTSINRELATEDBLOCK_H

class FreeDocument;

class FreeDocumentsInRelatedBlock
{
public:
  FreeDocumentsInRelatedBlock();

  static std::tuple<bool, std::vector<BlockApprovedDocument> > controlPOWFDocs(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static bool validatePOWFreeDocs(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static std::tuple<bool, std::vector<FreeDocument*>, std::vector<BlockApprovedDocument> > preControl(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static bool validateInBlock(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static std::tuple<bool, bool, QString> appendFreeDocsToBlock(
    Block* block,
    TransientBlockInfo& transient_block_info);
};

#endif // FREEDOCUMENTSINRELATEDBLOCK_H
