#include "stable.h"

#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/free_documents/free_document.h"

#include "free_documents_in_related_block.h"

FreeDocumentsInRelatedBlock::FreeDocumentsInRelatedBlock()
{

}

std::tuple<bool, std::vector<BlockApprovedDocument> > FreeDocumentsInRelatedBlock::controlPOWFDocs(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{

  std::vector<BlockApprovedDocument> approved_documents {};

  std::vector<Document*> fDocs {};
  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::FPost))
    fDocs = transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::FPost];

  if (fDocs.size() == 0)
    return {false, approved_documents};

  CLog::log("The POW block has " + QString::number(fDocs.size()) + " Freee dosuments(aka fDocs)", "app", "info");

  for (Document* a_free_doc: fDocs)
  {
    // control doc length limitation
    if (a_free_doc->m_doc_length > CConsts::MAX_DOC_LENGTH_BY_CHAR)
    {
      CLog::log("a Free Doc exceeds allowed size! doc hash(" + CUtils::hash8c(a_free_doc->getDocHash()) + ") size(" + QString::number(a_free_doc->m_doc_length) + ")", "sec", "error");
      return {false, approved_documents};
    }

    // controll document signature
    CDocIndexT doc_index = transient_block_info.m_doc_index_by_hash[a_free_doc->getDocHash()];
    QJsonObject dExtInfo; // the type of dExtInfo depends on the block/document type!
    if (a_free_doc->m_doc_ext_info.size() == 0)
    {
      dExtInfo = a_free_doc->m_doc_ext_info[0].toObject();
    }

    // control document hash
    QString custHashRes = a_free_doc->calcDocHash();
    if ((a_free_doc->getDocHash() != custHashRes))
      return {false, approved_documents};

//    // validate signer (if exist)
//    bool is_valid_doc = a_free_doc->veridfyDocSignature();
//    if (!is_valid_doc)
//       return {false, approved_documents};

    // calculate doc register request cost
    auto [dp_cost_status, free_post_cost] = a_free_doc->calcDocDataAndProcessCost(
      block->m_block_creation_date,
      stage);

    if (((!dp_cost_status) || (free_post_cost == 0)) && (block->m_block_type != CConsts::BLOCK_TYPES::POW))
      return {false, approved_documents};

//    approved_documents.push_back(BlockApprovedDocument {
//      a_free_doc,
//      a_free_doc->getDocHash(),
//      dExtInfo,
//      a_free_doc->m_doc_ext_hash});
  }

  return {true, approved_documents};
}


bool FreeDocumentsInRelatedBlock::validatePOWFreeDocs(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  std::vector<Document*> fDocs {};
  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::FPost))
    fDocs = transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::FPost];

  if (fDocs.size() == 0)
    return true;

  auto[status, approved_docs] = controlPOWFDocs(block, transient_block_info, stage);
  if (!status)
    return false;

  QStringList approvedDocHashes;
  for (BlockApprovedDocument a_approved_doc: approved_docs)
      approvedDocHashes.append(a_approved_doc.m_approved_doc_hash);
  if (approvedDocHashes.size() != CUtils::arrayUnique(approvedDocHashes).size())
  {
    CLog::log("a Free Doc has duplicated document in same block! block hash(" + CUtils::hash8c(block->getBlockHash()) + ") ", "sec", "error");
    return false;
  };

  // control if all documents are approved
  for (auto a_free_doc: fDocs)
  {
    if (!approvedDocHashes.contains(a_free_doc->getDocHash()))
    {
      CLog::log("The freeDoc is not approved! freeDoc(" + CUtils::hash8c(a_free_doc->getDocHash()) + ")  block hash(" + CUtils::hash8c(block->getBlockHash()) + ") ", "sec", "error");
      return false;
    }
  }

  return true;
}

// js name was controlFDocs
std::tuple<bool, std::vector<FreeDocument*>, std::vector<BlockApprovedDocument> > FreeDocumentsInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{

  std::vector<BlockApprovedDocument> approved_documents {};
  std::vector<FreeDocument*> free_docs {};

  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::FPost))
    for (auto a_doc: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::FPost])
      free_docs.push_back(dynamic_cast<FreeDocument*>(a_doc));

  if (free_docs.size() == 0)
    return {true, free_docs, approved_documents};

  CLog::log("The NORMAL block will have " + QString::number(free_docs.size()) + " free documents. block(" + block->getBlockHash() + ") ", "app", "trace");


  for (FreeDocument* a_free_doc: free_docs)
  {
    auto[status, locally_recalculate_doc_dp_cost] = a_free_doc->calcDocDataAndProcessCost(
      stage,
      block->m_block_creation_date);
    if (!status)
      return {false, free_docs, approved_documents};

    Document* costPayerTrx = transient_block_info.m_transactions_dict[transient_block_info.m_map_trx_ref_to_trx_hash[a_free_doc->getDocHash()]];
    QHash<CAddressT, CMPAIValueT> costPayerTrxOutputs {};
    for (TOutput* anOutput: costPayerTrx->getOutputs())
      costPayerTrxOutputs[anOutput->m_address] = anOutput->m_amount;

    // validate payment trx outputs
    if (!costPayerTrxOutputs.contains("TP_FDOC") ||
        (costPayerTrxOutputs["TP_FDOC"] < locally_recalculate_doc_dp_cost))
    {
      CLog::log("referenced Trx for iname binding costs has less payment! pays(" + CUtils::sepNum(costPayerTrxOutputs["TP_FDOC"]) + ") mcPAIs < " + CUtils::sepNum(locally_recalculate_doc_dp_cost) + " mcPAIs for adm polling(" + a_free_doc->getDocHash() + ")! block(" + block->getBlockHash(), "sec", "error");
      return {false, free_docs, approved_documents};
    }

    // control if the trx of doc cost payment is valid
    if (costPayerTrx->getRef() != a_free_doc->getDocHash())
    {
      CLog::log("The free doc cost is not payed by trx! type/class(" + a_free_doc->m_doc_type + " / " + a_free_doc->m_doc_class + ") doc(" + a_free_doc->getDocHash() + ")! block(" + block->getBlockHash() +") " + ")! payer ref(" +
        costPayerTrx->getRef() +") ", "sec", "error");
      return {false, free_docs, approved_documents};
    }

    approved_documents.push_back(BlockApprovedDocument {
      a_free_doc,
      a_free_doc->getDocHash(),
      a_free_doc->m_doc_ext_info,
      a_free_doc->m_doc_ext_hash});
  }

  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    CLog::log("Block has duplicated approved iName binding! Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
    return {false, free_docs, approved_documents};
  };

  if (free_docs.size() != approved_documents.size())
    return {false, free_docs, approved_documents};

  // control if all documents are approved
  for (auto a_free_doc: free_docs)
  {
    if (!approved_doc_hashes.contains(a_free_doc->getDocHash()))
    {
      CLog::log("The free doc is not approved! type/class(" + a_free_doc->m_doc_type + " / " + a_free_doc->m_doc_class + ") doc(" + CUtils::hash8c(a_free_doc->getDocHash()) + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, free_docs, approved_documents};
    }
  }

  return {true, free_docs, approved_documents};
}

// js name was validateFreeDocs
bool FreeDocumentsInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  auto[status, free_docs, approved_documents] = preControl(block, transient_block_info, stage);
  Q_UNUSED(free_docs);
  Q_UNUSED(approved_documents);
  return status;
}

/**
* @brief FreeDocumentsInRelatedBlock::appendFreeDocsToBlock
* @param block
* @param transient_block_info
* @return {creating block status, should empty buffer, msg}
*/
std::tuple<bool, bool, QString> FreeDocumentsInRelatedBlock::appendFreeDocsToBlock(
  Block* block,
  TransientBlockInfo& transient_block_info)
{
  auto[status, free_docs, approved_documents] = preControl(block, transient_block_info, CConsts::STAGES::Creating);
  if (!status)
    return {false, false, "fail in pre control"};

  if (approved_documents.size() > 0)
  {
    for (BlockApprovedDocument a_doc: approved_documents)
    {
      block->appendToDocuments(a_doc.m_approved_doc);
      //block->appendToExtInfo(a_doc.m_approved_doc_ext_info);
      //transient_block_info.m_block_documents_hashes.append(a_doc.m_approved_doc->m_doc_hash);
      //transient_block_info.m_block_ext_infos_hashes.append(a_doc.m_approved_doc->m_doc_ext_hash);
    }
  }

  return {true, true, "free docs are appended to block" };
}
