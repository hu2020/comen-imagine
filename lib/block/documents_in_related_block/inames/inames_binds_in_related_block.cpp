#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/iname_documents/iname_bind_document.h"

#include "inames_binds_in_related_block.h"

INamesBindsInRelatedBlock::INamesBindsInRelatedBlock()
{

}

// js name was controlINameBinds
std::tuple<bool, std::vector<BlockApprovedDocument> > INamesBindsInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  std::vector<BlockApprovedDocument> approved_documents {};
  std::vector<INameBindDocument*> bindings {};

  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::INameBind))
    for (auto a_doc: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::INameBind])
      bindings.push_back(dynamic_cast<INameBindDocument*>(a_doc));

  if (bindings.size() == 0)
    return {true, approved_documents};

  CLog::log("The NORMAL block will have " + QString::number(bindings.size()) + " iname binding documents. block(" + block->getBlockHash() + ") ", "app", "trace");

  for (INameBindDocument* a_binding: bindings)
  {

    // validate owner
    if (!CCrypto::isValidBech32(a_binding->m_pgp_binding_iname_owner))
    {
      CLog::log("invalid owner(" + CUtils::shortBech16(a_binding->m_pgp_binding_iname_owner) + ")", "sec", "error");
      return {false, approved_documents};
    }

    auto[status, locally_recalculate_doc_dp_cost] = a_binding->calcDocDataAndProcessCost(
      stage,
      block->m_block_creation_date);
    if (!status)
      return {false, approved_documents};

    Document* costPayerTrx = transient_block_info.m_transactions_dict[transient_block_info.m_map_trx_ref_to_trx_hash[a_binding->getDocHash()]];
    QHash<CAddressT, CMPAIValueT> cost_payer_trx_outputs {};
    for (TOutput* anOutput: costPayerTrx->getOutputs())
      cost_payer_trx_outputs[anOutput->m_address] = anOutput->m_amount;

    // validate payment trx outputs
    if (!cost_payer_trx_outputs.contains("TP_INAME_BIND") ||
        (cost_payer_trx_outputs["TP_INAME_BIND"] < locally_recalculate_doc_dp_cost))
    {
      CLog::log("referenced Trx for iname binding costs has less payment! pays(" + CUtils::sepNum(cost_payer_trx_outputs["TP_INAME_BIND"]) + ") mcPAIs < " + CUtils::sepNum(locally_recalculate_doc_dp_cost) + " mcPAIs for adm polling(" + a_binding->getDocHash() + ")! block(" + block->getBlockHash(), "sec", "error");
      return {false, approved_documents};
    }

    // control if the trx of doc cost payment is valid
    if (costPayerTrx->getRef() != a_binding->getDocHash())
    {
      CLog::log("The adm Polling cost is not payed by trx! iname binding(" + a_binding->getDocHash() + ")! block(" + block->getBlockHash() +") " + ")! payer ref(" +
        costPayerTrx->getRef() +") ", "sec", "error");
      return {false, approved_documents};
    }

    approved_documents.push_back(BlockApprovedDocument {
      a_binding,
      a_binding->getDocHash(),
      a_binding->m_doc_ext_info,
      a_binding->m_doc_ext_hash});
  }

  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    CLog::log("Block has duplicated approved iName binding! Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
    return {false, approved_documents};
  };

  if (bindings.size() != approved_documents.size())
    return {false, approved_documents};

  // control if all documents are approved
  for (auto a_binding: bindings)
  {
    if (!approved_doc_hashes.contains(a_binding->getDocHash()))
    {
      CLog::log("The ballot is not approved!! ballot(" + CUtils::hash8c(a_binding->getDocHash()) + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }
  }

  return {true, approved_documents};
}

// js name was validateINameBinds
bool INamesBindsInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  auto[status, approved_documents] = preControl(block, transient_block_info, stage);
  Q_UNUSED(approved_documents);
  return status;
}

std::tuple<bool, bool, QString> INamesBindsInRelatedBlock::appendINameBindsToBlock(
  Block* block,
  TransientBlockInfo& transient_block_info)
{
  auto[status, approved_documents] = preControl(
    block,
    transient_block_info,
    CConsts::STAGES::Creating);
  if (!status)
    return {false, false, "failed in appending iPGP binding to iNames pre control"};

  if (approved_documents.size() > 0)
    for (BlockApprovedDocument a_doc: approved_documents)
      block->appendToDocuments(a_doc.m_approved_doc);

  return {true, true, "binded iPGP to iNames are appended to block" };
}
