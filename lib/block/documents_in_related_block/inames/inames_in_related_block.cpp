#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/document_types/iname_documents/iname_reg_document.h"
#include "lib/services/collisions/collisions_handler.h"
#include "lib/services/contracts/flens/iname_handler.h"

#include "inames_in_related_block.h"

INamesInRelatedBlock::INamesInRelatedBlock()
{

}

std::tuple<bool, std::vector<BlockApprovedDocument> > INamesInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  QString msg;
  std::vector<BlockApprovedDocument> approved_documents {};
  std::vector<INameRegDocument*> reg_inames {};

  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::INameReg))
    for (auto a_doc: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::INameReg])
      reg_inames.push_back(dynamic_cast<INameRegDocument*>(a_doc));

  if (reg_inames.size() == 0)
    return {true, approved_documents};


  QStringList all_inames {};
  for (INameRegDocument* an_iname: reg_inames)
  {
    if (all_inames.contains(an_iname->m_iname_string))
    {
      CLog::log("duplicated iName in a block! iName(" + an_iname->m_iname_string + ") inameReg(" + an_iname->getDocHash() + ") block(" + block->getBlockHash() + ")", "sec", "error");
      return {false, approved_documents};
    }
    all_inames.append(an_iname->m_iname_string);


    // validate owner
    if (!CCrypto::isValidBech32(an_iname->m_iname_owner))
    {
      CLog::log("invalid owner of iName in a block! owner(" + an_iname->m_iname_owner + ") iName(" + an_iname->m_iname_string + ") inameReg(" + an_iname->getDocHash() + ") block(" + block->getBlockHash() + ")", "sec", "error");
      return {false, approved_documents};
    }

    // validate owner register capacity
    auto[shares, allowed_inames, already_used, availables_count] = INameHandler::ownerRegisteredRecords(an_iname->m_iname_owner);
    Q_UNUSED(shares);
    Q_UNUSED(allowed_inames);
    Q_UNUSED(already_used);
    if (availables_count < 1)
    {
      CLog::log("owner already exceed allowed iNames, so can not register a new one! owner(" + an_iname->m_iname_owner + ") iName(" + an_iname->m_iname_string + ") inameReg(" + an_iname->getDocHash() + ") block(" + block->getBlockHash() + ")", "sec", "error");
      return {false, approved_documents};
    }

    if (stage == CConsts::STAGES::Creating)
    {
      // validate if name is already registered!
      auto[already_registerd, record_info] = INameHandler::alreadyRegistered(an_iname->m_iname_string);
      Q_UNUSED(record_info);
      if (already_registerd)
      {
        CLog::log("the iName already registered! owner(" + an_iname->m_iname_owner + ") iName(" + an_iname->m_iname_string + ") inameReg(" + an_iname->getDocHash() + ") block(" + block->getBlockHash() + ")", "sec", "error");
        return {false, approved_documents};
      }

      // validate if name is already in queue of registering!
      auto[has_collision, the_collisioned_records] = CollisionsHandler::hasCollision(an_iname->m_iname_string);
      Q_UNUSED(the_collisioned_records);
      if (has_collision)
      {
        CLog::log("the iName already placed in collisions Queue! owner(" + an_iname->m_iname_owner + ") iName(" + an_iname->m_iname_string + ") inameReg(" + an_iname->getDocHash() + ") block(" + block->getBlockHash() + ")", "sec", "error");
        return {false, approved_documents};
      }
    }

    Document* costPayerTrx = transient_block_info.m_transactions_dict[transient_block_info.m_map_trx_ref_to_trx_hash[an_iname->getDocHash()]];
    QHash<CAddressT, CMPAIValueT> cost_payer_trx_outputs {};
    for (TOutput* anOutput: costPayerTrx->getOutputs())
      cost_payer_trx_outputs[anOutput->m_address] = anOutput->m_amount;

    // control if the trx of doc cost payment is valid
    if (costPayerTrx->getRef() != an_iname->getDocHash())
    {
      CLog::log("The iName cost is not payed by trx! iName(" + an_iname->m_iname_string + ") inameReg(" + an_iname->getDocHash() + ") payer trx(" + costPayerTrx->getDocHash() + ") block(" + block->getBlockHash() + ")", "sec", "error");
      return {false, approved_documents};
    }

    // calculate doc register request cost
    auto[status, locally_recalculate_doc_dp_cost] = an_iname->calcDocDataAndProcessCost(
      stage,
      block->m_block_creation_date);
    if (!status)
      return {false, approved_documents};

    // validate payment trx outputs
    if (!cost_payer_trx_outputs.contains("TP_INAME_REG") ||
        (cost_payer_trx_outputs["TP_INAME_REG"] < locally_recalculate_doc_dp_cost))
    {
      msg = "Referenced Trx for registering iName costs hasn't suffcient payment! remote: ";
      msg += CUtils::microPAIToPAI6(cost_payer_trx_outputs["TP_INAME_REG"]) + " locally_recalculate_doc_dp_cost: ";
      msg += CUtils::microPAIToPAI6(locally_recalculate_doc_dp_cost) + " iName(" + an_iname->m_iname_string;
      msg += ") inameReg(" + CUtils::hash8c(an_iname->getDocHash()) + ") payer trx(";
      msg += CUtils::hash8c(costPayerTrx->getDocHash()) + ") block(" + CUtils::hash8c(block->getBlockHash());
      msg += ") an_iname: " + an_iname->safeStringifyDoc(true);
      CLog::log(msg, "sec", "error");
      return {false, approved_documents};
    }

    approved_documents.push_back(BlockApprovedDocument {
      an_iname,
      an_iname->getDocHash(),
      an_iname->m_doc_ext_info,
      an_iname->m_doc_ext_hash});
  }

  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    CLog::log("Block has duplicated approved iname reg! Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
    return {false, approved_documents};
  };

  if (reg_inames.size() != approved_documents.size())
    return {false, approved_documents};

  // control if all documents are approved
  for (auto a_reg_request: reg_inames)
  {
    if (!approved_doc_hashes.contains(a_reg_request->getDocHash()))
    {
      CLog::log("iName reg is not approved! doc(" + CUtils::hash8c(a_reg_request->getDocHash()) + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }
  }

  return {true, approved_documents};
}

// js name was validateINames
bool INamesInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  auto[status, approved_documents] = preControl(block, transient_block_info, stage);
  Q_UNUSED(approved_documents);
  return status;
}

std::tuple<bool, bool, QString> INamesInRelatedBlock::appendINamesToBlock(
  Block* block,
  TransientBlockInfo& transient_block_info)
{
  auto[status, approved_documents] = preControl(
    block,
    transient_block_info,
    CConsts::STAGES::Creating);
  if (!status)
    return {false, false, "failed in appending iNames pre control"};

  if (approved_documents.size() > 0)
  {
    for (BlockApprovedDocument a_doc: approved_documents)
      block->appendToDocuments(a_doc.m_approved_doc);

    if (!CMachine::isInSyncProcess())
    {
      CGUI::signalUpdateRegisteredINames();
      CGUI::initINamesCombo(CGUI::get().m_ui->comboBox_agora_domain);
    }
  }

  return {true, true, "iNames are appended to block" };
}
