#ifndef INAMESBINDSINRELATEDBLOCK_H
#define INAMESBINDSINRELATEDBLOCK_H

class INameBindDocument;

class INamesBindsInRelatedBlock
{
public:
  INamesBindsInRelatedBlock();

  static std::tuple<bool, std::vector<BlockApprovedDocument> > preControl(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static bool validateInBlock(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static std::tuple<bool, bool, QString> appendINameBindsToBlock(
    Block* block,
    TransientBlockInfo& transient_block_info);
};

#endif // INAMESBINDSINRELATEDBLOCK_H
