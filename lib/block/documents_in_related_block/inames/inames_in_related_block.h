#ifndef INAMESINRELATEDBLOCK_H
#define INAMESINRELATEDBLOCK_H


class INamesInRelatedBlock
{
public:
  INamesInRelatedBlock();

  static std::tuple<bool, std::vector<BlockApprovedDocument> > preControl(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static bool validateInBlock(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static std::tuple<bool, bool, QString> appendINamesToBlock(
    Block* block,
    TransientBlockInfo& transient_block_info);

};

#endif // INAMESINRELATEDBLOCK_H
