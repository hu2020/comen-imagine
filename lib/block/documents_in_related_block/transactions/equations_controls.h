#ifndef EQUATIONSCONTROLS_H
#define EQUATIONSCONTROLS_H


class EquationsControls
{
public:
  EquationsControls();

  static bool validateEquation(
    const Block* block,
    const QV2DicT& used_coins_dict,
    const QV2DicT& invalid_coins_dict);
};

#endif // EQUATIONSCONTROLS_H
