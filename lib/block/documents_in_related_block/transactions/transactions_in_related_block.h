#ifndef TRANSACTIONSINRELATEDBLOCK_H
#define TRANSACTIONSINRELATEDBLOCK_H

class BlockOverview
{
public:
  bool m_status = false;
  QString m_msg = "";

  QStringList m_supported_P4P {};
  QStringList m_block_used_coins {};
  QSDicT m_map_coin_to_spender_doc {};
  QV2DicT m_used_coins_dict {};
  QStringList m_block_not_matured_coins {};
};

class TransactionsInRelatedBlock
{
public:
  TransactionsInRelatedBlock();

  static BlockOverview prepareBlockOverview(
    const Block *block);

  static std::tuple<bool, bool, QString, SpendCoinsList*> validateTransactions(
    const Block* block,
    const QString& stage);


  static std::tuple<bool, QV2DicT, QV2DicT, bool, SpendCoinsList*> considerInvalidCoins(
    const QString& blockHash,
    const QString& blockCreationDate,
    const QStringList& block_used_coins,
    QV2DicT used_coins_dict,
    QStringList maybe_invalid_coins,
    const QSDicT& map_coin_to_spender_doc);

  static std::tuple<bool, bool, QString> appendTransactions(
    Block* block,
    TransientBlockInfo& transient_block_info);
};

#endif // TRANSACTIONSINRELATEDBLOCK_H
