#ifndef COINSVISIBILITYHANDLER_H
#define COINSVISIBILITYHANDLER_H


class CoinsVisibilityHandler
{
public:
  CoinsVisibilityHandler();

  static bool controlCoinsVisibilityInGraphHistory(
    const QStringList &blockUsedCoins,
    const QStringList &ancestors,
    const QString &blockHash);

};

#endif // COINSVISIBILITYHANDLER_H
