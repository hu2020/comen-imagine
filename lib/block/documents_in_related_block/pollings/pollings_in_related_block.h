#ifndef POLLINGSINRELATEDBLOCK_H
#define POLLINGSINRELATEDBLOCK_H

class PollingDocument;

class PollingsInRelatedBlock
{
public:
  PollingsInRelatedBlock();

  static std::tuple<bool, std::vector<BlockApprovedDocument> > preControl(
    const Block* block,
    const TransientBlockInfo& transient_block_info,
    const QString& stage);

  static bool validateInBlock(
    const Block*,
    const TransientBlockInfo&,
    const QString& stage);

  static std::tuple<bool, bool, QString> appendPollingsToBlock(
    Block* block,
    TransientBlockInfo& transient_block_info);
};

#endif // POLLINGSINRELATEDBLOCK_H
