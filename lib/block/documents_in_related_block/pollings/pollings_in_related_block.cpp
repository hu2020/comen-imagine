#include "stable.h"

#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/polling_document.h"
#include "lib/services/polling/polling_handler.h"

#include "pollings_in_related_block.h"

PollingsInRelatedBlock::PollingsInRelatedBlock()
{

}

std::tuple<bool, std::vector<BlockApprovedDocument> > PollingsInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  std::vector<BlockApprovedDocument> approved_documents {};
  QVector<PollingDocument*> pollings {};

  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::Polling))
    for (auto a_doc: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::Polling])
      pollings.push_back(dynamic_cast<PollingDocument*>(a_doc));

  if (pollings.size() == 0)
    return {true, approved_documents};


  // control if all pollings are payed by a transaction
  QStringList pollingHashes = transient_block_info.m_map_referencer_to_referenced.keys();
  QStringList trxRefHashes = transient_block_info.m_map_trx_ref_to_trx_hash.keys();
  if (CUtils::arrayDiff(pollingHashes, trxRefHashes).size() > 0)
  {
    CLog::log(
      "There are some pollings which are not payed by transaction pollingHashes: " + pollingHashes.join(", ") +
      " trxRefHashes: " + trxRefHashes.join(", "), "sec", "error");
    return {false, approved_documents};
  }


  CLog::log("The NORMAL block will have " + QString::number(pollings.size()) + " documents of type Polling.", "app", "trace");

  for (PollingDocument* a_polling: pollings)
  {
    QString referencedDocHash = transient_block_info.m_map_referencer_to_referenced[a_polling->getDocHash()];
    // Document* referencedDoc = transient_block_info.m_doc_by_hash[referencedDocHash];
    QJsonArray dExtInfo;
    if (a_polling->m_doc_ext_info.size() > 0 )
    {
      dExtInfo = block->m_block_ext_info[transient_block_info.m_doc_index_by_hash[a_polling->getDocHash()]].toArray();
    }

    // validate voting logevity

    a_polling->m_voting_timeframe = PollingHandler::normalizeVotingTimeframe(a_polling->m_voting_timeframe);
    if (a_polling->m_voting_timeframe < CMachine::getMinVotingTimeframe())
    {
      CLog::log("voting timeframe is less than minimum voting longivity! (" + QString::number(a_polling->m_voting_timeframe) + " > " + QString::number(CMachine::getMinVotingTimeframe()) + ") " , "sec", "error");
      return {false, approved_documents};
    }

    // validate voting class
    if (a_polling->m_doc_class != PollingHandler::POLLING_PROFILE_CLASSES["Basic"]["ppName"].toString())
    {
      CLog::log("THe polling class is not supported yet! class(" + a_polling->m_doc_class + ") " , "sec", "error");
      return {false, approved_documents};
    }

    //uint64_t m_potential_voters_count = 0;

//    listener.doCallSync('SASH_custom_validate_polling', { polling: aPoll });

    // validate the document which is refered by polling document(if exist) & also validate the polling cost (based on polling itsefl and eventually related docs)
    if(a_polling->getRefType() == CConsts::POLLING_REF_TYPE::AdmPolling)
    {
      auto[total_shares_, share_amount_per_holder, holdersOrderByShares_] = DNAHandler::getSharesInfo();
      Q_UNUSED(total_shares_);
      Q_UNUSED(holdersOrderByShares_);
      a_polling->m_potential_voters_count = share_amount_per_holder.keys().size();

    }
    else if(a_polling->getRefType() == CConsts::POLLING_REF_TYPE::ReqForRelRes)
    {
      // uncomment lines below and implement it ASAP
//      let potentialVotersInfo = reservedHandler.calcm_potential_voters_count(referencedDoc.eyeBlock, referencedDoc.reserveNumber);
//      clog.app.info(`potentialVotersInfo ${utils.stringify(potentialVotersInfo)}`);
//      if (potentialVotersInfo.err != false)
//        return potentialVotersInfo;
//      m_potential_voters_count = potentialVotersInfo.votersCount;

    }
    else if(a_polling->getRefType() == CConsts::POLLING_REF_TYPE::Proposal)
    {
       // maybe some controls!
    }

    /**
     * polling costs control which is applyable for all non "POLLING_REF_TYPE.Proposal" pollings
     */
    if (a_polling->getRefType() != CConsts::POLLING_REF_TYPE::Proposal)
    {
      Document* costPayerTrx = transient_block_info.m_transactions_dict[transient_block_info.m_map_trx_ref_to_trx_hash[a_polling->getDocHash()]];
      QHash<CAddressT, CMPAIValueT> cost_payer_trx_outputs {};
      for (TOutput* anOutput: costPayerTrx->getOutputs())
        cost_payer_trx_outputs[anOutput->m_address] = anOutput->m_amount;

      // control if the trx of doc cost payment is valid
      if (costPayerTrx->getRef() != a_polling->getDocHash())
      {
        CLog::log("The polling cost is not payed by candidated trx! polling(" + a_polling->getDocHash() + ") payer trx(" + costPayerTrx->getDocHash() + ") block(" + block->getBlockHash() + ") " , "sec", "error");
        return {false, approved_documents};
      }

      auto[status, locally_recalculate_doc_dp_cost] = a_polling->calcDocDataAndProcessCost(
        stage,
        block->m_block_creation_date);
      if (!status)
        return {false, approved_documents};

      // validate payment trx outputs
      if (!cost_payer_trx_outputs.keys().contains("TP_POLLING") ||
        (cost_payer_trx_outputs["TP_POLLING"] < locally_recalculate_doc_dp_cost))
      {
        QString msg = "block(" + CUtils::hash8c(block->getBlockHash()) + ") referenced Trx(" +CUtils::hash8c(costPayerTrx->getDocHash()) + ")";
        msg += "for polling costs(" + CUtils::hash8c(a_polling->getDocHash()) + ") ";
        msg += "1has less payment ref(" + CUtils::hash8c(costPayerTrx->getRef()) + ")! ";
        msg += CUtils::sepNum(cost_payer_trx_outputs["TP_POLLING"]) + " mcPAIs < ";
        msg += CUtils::sepNum(locally_recalculate_doc_dp_cost) + " mcPAIs";
        CLog::log(msg, "sec", "error");
        return {false, approved_documents};
      }
    }

    approved_documents.push_back(BlockApprovedDocument {
      a_polling,
      a_polling->getDocHash(),
      dExtInfo,
      a_polling->m_doc_ext_hash
    });

  }

  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    CLog::log("Block has duplicated approved polling! Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
    return {false, approved_documents};
  };

  if (pollings.size() != approved_documents.size())
    return {false, approved_documents};

  for (PollingDocument* a_polling: pollings)
  {
    if (!approved_doc_hashes.contains(a_polling->getDocHash()))
    {
      CLog::log("The polling is not approved! Block(" + CUtils::hash8c(block->getBlockHash()) + ") polling doc(" + CUtils::hash8c(a_polling->getDocHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }
  }

  return {true, approved_documents};
}

bool PollingsInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  auto[status, approved_documents] = preControl(block, transient_block_info, stage);
  Q_UNUSED(approved_documents);
  return status;
}


std::tuple<bool, bool, QString> PollingsInRelatedBlock::appendPollingsToBlock(
  Block* block,
  TransientBlockInfo& transient_block_info)
{
  auto[status, approved_documents] = preControl(
    block,
    transient_block_info,
    CConsts::STAGES::Creating);
  if (!status)
    return {false, false, "failed in appending polling pre control"};

  if (approved_documents.size() > 0)
    for (BlockApprovedDocument a_doc: approved_documents)
      block->appendToDocuments(a_doc.m_approved_doc);

  return {true, true, "adm-pollings are appended to block" };
}
