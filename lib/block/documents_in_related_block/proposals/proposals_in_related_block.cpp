#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/dna_proposal_document.h"
#include "lib/block/document_types/pledge_documents/pledge_document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contracts/loan/loan_contract_handler.h"

#include "proposals_in_related_block.h"

ProposalsInRelatedBlock::ProposalsInRelatedBlock()
{

}

// js name was controlProposals
std::tuple<bool, std::vector<BlockApprovedDocument> > ProposalsInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& grpdDocuments,
  const QString& stage)
{
  std::vector<BlockApprovedDocument> approved_documents {};
  std::vector<DNAProposalDocument*> proposals {};
  std::vector<PledgeDocument*> pledges {};

  if (grpdDocuments.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::DNAProposal))
    for (Document* a_doc: grpdDocuments.m_groupped_documents[CConsts::DOC_TYPES::DNAProposal])
      proposals.push_back(dynamic_cast<DNAProposalDocument*>(a_doc));

  if (proposals.size() == 0)
    return {true, approved_documents};


  // prepare pledges
  if (grpdDocuments.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::Pledge))
    for (Document* a_doc: grpdDocuments.m_groupped_documents[CConsts::DOC_TYPES::Pledge])
      pledges.push_back(dynamic_cast<PledgeDocument*>(a_doc));

  QSDicT mapProposalToPledge {};
  QSDicT mapProposalToTrx {};
  QHash<QString, PledgeDocument*> pledgeDict {};
  for (PledgeDocument* aPledge: pledges)
  {
    if (aPledge->m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP)
    {
      pledgeDict[aPledge->getDocHash()] = aPledge;
      mapProposalToPledge[aPledge->m_proposal_ref] = aPledge->getDocHash();
      mapProposalToTrx[aPledge->m_proposal_ref] = aPledge->m_transaction_ref;
    }
  }

  // control if each trx is referenced byonly one pledge?
  QStringList tmpTrxs {};
  for(QString a_porp_ref: mapProposalToTrx.keys())
    tmpTrxs.append(mapProposalToTrx[a_porp_ref]);

//  let tmpTrxs = utils.objKeys(mapProposalToTrx).map(x => mapProposalToTrx[x]);
  if (tmpTrxs.size() != CUtils::arrayUnique(tmpTrxs).size())
  {
    CLog::log("Pledge transactions are not unique ! Block(" + CUtils::hash8c(block->getBlockHash()) + ") mapProposalToTrx: " + CUtils::dumpIt(mapProposalToTrx), "sec", "error");
    return {false, approved_documents};
  }

  CLog::log("The NORMAL block will have (" + QString::number(proposals.size()) + ") documents of DNA Proposal", "app", "info");

  for (DNAProposalDocument* a_proposal: proposals)
  {


    // validate voting logevity
    if (a_proposal->m_voting_timeframe < CMachine::getMinVotingTimeframe())
    {
      CLog::log("Voting timeframe is less than minimum voting longivity! timespan(" + QString::number(a_proposal->m_voting_timeframe) + " hours) Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }

    // validate voting class
    if (a_proposal->m_polling_profile != PollingHandler::POLLING_PROFILE_CLASSES["Basic"]["ppName"].toString())
    {
      CLog::log("proposal polling class is not supported yet!! class(" + a_proposal->m_polling_profile + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }

    // validate helpHours
    if (a_proposal->m_help_hours < 1)
    {
      CLog::log("proposal help hours is less than one! help(" +  QString::number(a_proposal->m_help_hours) + " hours) Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }

    // validate helpLevel
    if (a_proposal->m_help_level < 1)
    {
      CLog::log("proposal help level is less than one! help(" +  QString::number(a_proposal->m_help_level) + " hours) Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }

    // control proposal cost & if exist a trx to pay this cost
    auto[status, locally_recalculate_doc_dp_cost] = a_proposal->calcDocDataAndProcessCost(
      stage,
      block->m_block_creation_date);
    if (!status)
      return {false, approved_documents};

    auto[one_cycle_income, apply_cost] = ProposalHandler::calcProposalApplyCost(
      a_proposal->m_help_hours,
      a_proposal->m_help_level,
      block->m_block_creation_date);
    CLog::log("the proposal(" +  CUtils::hash8c(a_proposal->getDocHash()) + ") one_cycle_income(" +  CUtils::sepNum(one_cycle_income) + ") proposal Document locally_recalculate_doc_dp_cost(" +  CUtils::sepNum(locally_recalculate_doc_dp_cost) + ") apply_cost(" +  CUtils::sepNum(apply_cost) + ") help(" +  QString::number(a_proposal->m_help_level) + ") hours) Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "app", "info");

    // control if proposal is fund by a pledge contract or by an standalon transaction?
    if (mapProposalToPledge.keys().contains(a_proposal->getDocHash()))
    {
      // proposal costs is payed by pledgeing potential incomes and lending from pledgee.

      Document* costPayerTrx = grpdDocuments.m_transactions_dict[mapProposalToTrx[a_proposal->getDocHash()]];
      QHash<CAddressT, CMPAIValueT> costPayerTrxOutputs {};
      for (TOutput* anOutput: costPayerTrx->getOutputs())
        costPayerTrxOutputs[anOutput->m_address] = anOutput->m_amount;

      // validate payment trx outputs
      if (!costPayerTrxOutputs.contains("TP_PROPOSAL") ||
          (costPayerTrxOutputs["TP_PROPOSAL"] < apply_cost + locally_recalculate_doc_dp_cost))
      {
        CLog::log("referenced Trx for proposal costs has less payment! pays(" + CUtils::sepNum(costPayerTrxOutputs["TP_PROPOSAL"]) + ") mcPAIs < " + CUtils::sepNum(apply_cost + locally_recalculate_doc_dp_cost) + " mcPAIs for adm proposal(" + a_proposal->getDocHash() + ")! block(" + block->getBlockHash(), "sec", "error");
        return {false, approved_documents};
      }

      // control if the trx of doc cost payment is valid
      PledgeDocument* ref_pledge = pledgeDict[mapProposalToPledge[a_proposal->getDocHash()]];
      if (ref_pledge->m_pledger_address != a_proposal->m_contributor_account)
      {
        CLog::log("The Proposal income address(" + a_proposal->m_contributor_account + ") is different from pledged one(" + ref_pledge->m_pledger_address + ")! block(" + block->getBlockHash(), "sec", "error");
        return {false, approved_documents};
      }

      if (ref_pledge->m_redeem_principal < apply_cost + locally_recalculate_doc_dp_cost)
      {
        CLog::log("The Proposal (apply cost + proposal Documetn Cost) is higher than requested loan! (apply_cost " + CUtils::microPAIToPAI6(apply_cost) + " + proposal cost " + CUtils::microPAIToPAI6(locally_recalculate_doc_dp_cost) +") block(" + block->getBlockHash() + ")", "sec", "error");
        return {false, approved_documents};
      }

      if (ref_pledge->m_redeem_repayment_amount > one_cycle_income)
      {
        CLog::log("The Repayment value is hegher than one cycle income! repayment(" +
          CUtils::microPAIToPAI6(ref_pledge->m_redeem_repayment_amount) +
          ") one cycle income(" + CUtils::microPAIToPAI6(one_cycle_income) +
          ") ref pledge(" + CUtils::hash8c(ref_pledge->getDocHash()) +
          ") block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
        return {false, approved_documents};
      }

      if (ref_pledge->m_redeem_annual_interest < 0)
      {
        CLog::log("The annual interest is less than zero! interst(" + QString::number(ref_pledge->m_redeem_annual_interest) + ") one cycle income(" + CUtils::microPAIToPAI6(one_cycle_income) + " ref pledge" + CUtils::hash8c(ref_pledge->getDocHash()) +") block(" + block->getBlockHash() + ")", "sec", "error");
        return {false, approved_documents};
      }

      if (ref_pledge->m_redeem_repayment_schedule < 1)
      {
        CLog::log("The repayment schedule  is less than one!! interst(" + QString::number(ref_pledge->m_redeem_repayment_schedule) + ") one cycle income(" + CUtils::microPAIToPAI6(one_cycle_income) + " ref pledge" + CUtils::hash8c(ref_pledge->getDocHash()) +") block(" + block->getBlockHash() + ")", "sec", "error");
        return {false, approved_documents};
      }
      LoanDetails loan_details = LoanContractHandler::calcLoanRepayments(
        ref_pledge->m_redeem_principal,
        ref_pledge->m_redeem_annual_interest,
        ref_pledge->m_redeem_repayment_amount,
        ref_pledge->m_redeem_repayment_schedule);
      if (!loan_details.m_calculation_status)
      {
        CLog::log("Invalid repayment data! principal(" + QString::number(ref_pledge->m_redeem_principal) + ") " +
          " interest(" + QString::number(ref_pledge->m_redeem_annual_interest) + ")" +
          " amount(" + QString::number(ref_pledge->m_redeem_repayment_amount) + ")" +
          " schedule(" + QString::number(ref_pledge->m_redeem_repayment_schedule) + ")" +
          CUtils::hash8c(ref_pledge->getDocHash()) +") block(" + block->getBlockHash() + ")", "sec", "error");
        return {false, approved_documents};
      }


      // control if pledge trx is valid
      if (!mapProposalToTrx.contains(a_proposal->getDocHash()))
      {
        CLog::log("No Trx pays Proposal costs! proposal(" + a_proposal->getDocHash() + ")! block(" + block->getBlockHash(), "sec", "error");
        return {false, approved_documents};
      }

      if (!grpdDocuments.m_transactions_dict.contains(mapProposalToTrx[a_proposal->getDocHash()]))
      {
        CLog::log("referenced Trx(" + mapProposalToTrx[a_proposal->getDocHash()] + ")) for Proposal costs proposal(" + a_proposal->getDocHash() + ") does not exist in block(" + block->getBlockHash(), "sec", "error");
        return {false, approved_documents};
      }

      // validate payment trx
      if (costPayerTrx->getRef() != a_proposal->getDocHash())
      {
        CLog::log("referenced Trx(" + mapProposalToTrx[a_proposal->getDocHash()] + ") has different payment ref for Proposal costs proposal(" + a_proposal->getDocHash() + ") does not exist in block(" + block->getBlockHash() +") ref(" + costPayerTrx->getRef() + ")! ", "sec", "error");
        return {false, approved_documents};
      }


      approved_documents.push_back(BlockApprovedDocument {
        a_proposal,
        a_proposal->getDocHash(),
        QJsonArray{},
        CConsts::NO_EXT_HASH});    //a_proposal->getDocHash()    // since proposals haven't ext Info su as ext InfoHash, we use same as doc hash



    } else {
      // must find the transaction that pays the cost
      // TODO: implement it ASAP

      Document* costPayerTrx = grpdDocuments.m_transactions_dict[grpdDocuments.m_map_trx_ref_to_trx_hash[a_proposal->getDocHash()]];
      QHash<CAddressT, CMPAIValueT> costPayerTrxOutputs {};
      for (TOutput* anOutput: costPayerTrx->getOutputs())
        costPayerTrxOutputs[anOutput->m_address] = anOutput->m_amount;


      // validate payment trx outputs
      if (!costPayerTrxOutputs.contains("TP_PROPOSAL") ||
          (costPayerTrxOutputs["TP_PROPOSAL"] < apply_cost + locally_recalculate_doc_dp_cost))
      {
        CLog::log("referenced Trx for proposer paying costs doesn't cover all proposal's costs! pays(" + CUtils::sepNum(costPayerTrxOutputs["TP_PROPOSAL"]) + ") mcPAIs < " + CUtils::sepNum(apply_cost + locally_recalculate_doc_dp_cost) + " mcPAIs for adm proposal(" + a_proposal->getDocHash() + ")! block(" + block->getBlockHash(), "sec", "error");
        return {false, approved_documents};
      }

      approved_documents.push_back(BlockApprovedDocument {
        a_proposal,
        a_proposal->getDocHash(),
        QJsonArray{},
        CConsts::NO_EXT_HASH});    // a_proposal->getDocHash()    // since proposals haven't ext Info su as ext InfoHash, we use same as doc hash
    }
  }

  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    CLog::log("Block has duplicated approved ballot! Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
    return {false, approved_documents};
  };

  if (proposals.size() != approved_documents.size())
    return {false, approved_documents};

  // control if all documents are approved
  for (auto a_prop: proposals)
  {
    if (!approved_doc_hashes.contains(a_prop->getDocHash()))
    {
      CLog::log("The proposal is not approved! proposal(" + CUtils::hash8c(a_prop->getDocHash()) + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }
  }

  return {true, approved_documents};
}

// js name was validateProposals
bool ProposalsInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& grpdDocuments,
  const QString& stage)
{

  auto[status, approved_documents] = preControl(block, grpdDocuments, stage);
  Q_UNUSED(approved_documents);
  return status;
}

std::tuple<bool, bool, QString> ProposalsInRelatedBlock::appendProposalsToBlock(
  Block* block,
  TransientBlockInfo& transient_block_info)
{
  auto[status, approved_documents] = preControl(block, transient_block_info, CConsts::STAGES::Creating);
  if (!status)
    return {false, false, "failed in proposals pre control"};

  if (approved_documents.size() > 0)
  {
    for (BlockApprovedDocument a_doc: approved_documents)
    {
      block->appendToDocuments(a_doc.m_approved_doc);
//      block.docs.push(aDoc._doc);
//      docsHashes.push(aDoc._docHash);
//      block.bExtInfo.push(aDoc._dExtInfo);
//      externalInfoHashes.push(aDoc._dExtHash);
    }
  }

  return {true, true, "Proposals are appended to block" };
}
