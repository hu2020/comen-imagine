#ifndef PROPOSALSINRELATEDBLOCK_H
#define PROPOSALSINRELATEDBLOCK_H


class ProposalsInRelatedBlock
{
public:
  ProposalsInRelatedBlock();

  static std::tuple<bool, std::vector<BlockApprovedDocument> > preControl(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static bool validateInBlock(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static std::tuple<bool, bool, QString> appendProposalsToBlock(
    Block* block,
    TransientBlockInfo& transient_block_info);

};

#endif // PROPOSALSINRELATEDBLOCK_H
