#ifndef PLEDGESINRELATEDBLOCK_H
#define PLEDGESINRELATEDBLOCK_H


class PledgesInRelatedBlock
{
public:
  PledgesInRelatedBlock();

  static std::tuple<bool, std::vector<BlockApprovedDocument> > preControl(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static bool validateInBlock(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static std::tuple<bool, bool, QString> appendPledgesToBlock(
    Block* block,
    TransientBlockInfo& transient_block_info);

};

#endif // PLEDGESINRELATEDBLOCK_H
