#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/pledge_documents/pledge_document.h"
#include "lib/block/document_types/pledge_documents/close_pledge_document.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"

#include "close_pledges_in_related_block.h"

ClosePledgesInRelatedBlock::ClosePledgesInRelatedBlock()
{

}

// js name was prepareClosePledges
std::tuple<bool, std::vector<BlockApprovedDocument> > ClosePledgesInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& grpdDocuments,
  const QString& stage)
{
  std::vector<BlockApprovedDocument> approved_documents {};
  std::vector<ClosePledgeDocument*> close_pledges {};

  if (grpdDocuments.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::ClosePledge))
    for (auto a_doc: grpdDocuments.m_groupped_documents[CConsts::DOC_TYPES::ClosePledge])
      close_pledges.push_back(dynamic_cast<ClosePledgeDocument*>(a_doc));

  if (close_pledges.size() == 0)
    return {true, approved_documents};



  CLog::log("The NORMAL block will have " + QString::number(close_pledges.size()) + ") documents of close Pleadge", "app", "info");


  for (ClosePledgeDocument* a_close_pledge: close_pledges)
  {


    // control if concluder is the person in referenced pledge contract?
    QVDRecordsT ref_pledges = GeneralPledgeHandler::searchInPledgedAccounts(
      {{"pgd_hash", a_close_pledge->getRef()}});
    if (ref_pledges.size() != 1)
    {
      CLog::log("referenced pledge-contract to close, is not valid! ref(" + CUtils::hash8c(a_close_pledge->getRef()) + ") doc(" + CUtils::hash8c(a_close_pledge->getDocHash()) + ") block(" + CUtils::hash8c(block->getBlockHash()) + ") ", "sec", "error");
      return {false, approved_documents};
    }

    QVDicT the_ref_pledge = ref_pledges[0];

    if (!QStringList{CConsts::PLEDGE_CLASSES::PledgeP}.contains(the_ref_pledge.value("pgd_class").toString()))
    {
      CLog::log("System yet doesn't support this closing dClass(" + the_ref_pledge.value("pgd_class").toString() + " ref(" + CUtils::hash8c(a_close_pledge->getRef()) + ") doc(" + CUtils::hash8c(a_close_pledge->getDocHash()) + ") block(" + CUtils::hash8c(block->getBlockHash()) + ") ", "sec", "error");
      return {false, approved_documents};
    }

    if (a_close_pledge->m_conclude_type == CConsts::PLEDGE_CONCLUDER_TYPES::ByPledgee)
    {
      if (a_close_pledge->m_concluder_address != the_ref_pledge.value("pgd_pledgee").toString())
      {
        CLog::log("tThe concluder of pledge-close-contract, is not pledgee of referenced-pledge-contract ref(" + CUtils::hash8c(a_close_pledge->getRef()) + ") doc(" +
          CUtils::hash8c(a_close_pledge->getDocHash()) + ") doc(" + CUtils::hash8c(the_ref_pledge.value("pgd_pledgee").toString()) +
          ") dClass(" + the_ref_pledge.value("pgd_class").toString() + " block(" + CUtils::hash8c(block->getBlockHash()) + ") ", "sec", "error");
        return {false, approved_documents};
      }

    }
    else if (a_close_pledge->m_conclude_type == CConsts::PLEDGE_CONCLUDER_TYPES::ByArbiter)
    {
      if (a_close_pledge->m_concluder_address != the_ref_pledge.value("pgd_arbiter").toString())
      {
        CLog::log("tThe concluder of pledge-close-contract, is not arbiter of referenced-pledge-contract ref(" + CUtils::hash8c(a_close_pledge->getRef()) + ") doc(" +
          CUtils::hash8c(a_close_pledge->getDocHash()) + ") doc(" + CUtils::hash8c(the_ref_pledge.value("pgd_pledgee").toString()) +
          ") dClass(" + the_ref_pledge.value("pgd_class").toString() + " block(" + CUtils::hash8c(block->getBlockHash()) + ") ", "sec", "error");
        return {false, approved_documents};
      }

    }
    else if (a_close_pledge->m_conclude_type == CConsts::PLEDGE_CONCLUDER_TYPES::ByPledger)
    {
      if (a_close_pledge->m_concluder_address != the_ref_pledge.value("pgd_arbiter").toString())
      {
        CLog::log("tThe concluder of pledge-close-contract, is not pledger of referenced-pledge-contract ref(" + CUtils::hash8c(a_close_pledge->getRef()) + ") doc(" +
          CUtils::hash8c(a_close_pledge->getDocHash()) + ") doc(" + CUtils::hash8c(the_ref_pledge.value("pgd_pledgee").toString()) +
          ") dClass(" + the_ref_pledge.value("pgd_class").toString() + " block(" + CUtils::hash8c(block->getBlockHash()) + ") ", "sec", "error");
        return {false, approved_documents};
      }

    }
    else
    {
      CLog::log("Invalid close pledge request conclude type(" + a_close_pledge->m_conclude_type + ") doc(" +
        CUtils::hash8c(a_close_pledge->getDocHash()) + ") doc(" + CUtils::hash8c(the_ref_pledge.value("pgd_pledgee").toString()) +
        ") dClass(" + the_ref_pledge.value("pgd_class").toString() + " block(" + CUtils::hash8c(block->getBlockHash()) + ") ", "sec", "error");
      return {false, approved_documents};
    }

    auto[status, locally_recalculate_doc_dp_cost] = a_close_pledge->calcDocDataAndProcessCost(
      stage,
      block->m_block_creation_date);
    if (!status)
      return {false, approved_documents};

    Document* costPayerTrx = grpdDocuments.m_transactions_dict[grpdDocuments.m_map_trx_ref_to_trx_hash[a_close_pledge->getDocHash()]];
    QHash<CAddressT, CMPAIValueT> costPayerTrxOutputs {};
    for (TOutput* anOutput: costPayerTrx->getOutputs())
      costPayerTrxOutputs[anOutput->m_address] = anOutput->m_amount;

    if (!costPayerTrxOutputs.contains("TP_PLEDGE_CLOSE") ||
        (costPayerTrxOutputs["TP_PLEDGE_CLOSE"] < locally_recalculate_doc_dp_cost))
    {
      CLog::log("referenced Trx for close pledge costs has less payment! pays(" + CUtils::sepNum(costPayerTrxOutputs["TP_PLEDGE_CLOSE"]) + ") mcPAIs < " + CUtils::sepNum(locally_recalculate_doc_dp_cost) + " mcPAIs for adm polling(" + a_close_pledge->getDocHash() + ")! block(" + block->getBlockHash(), "sec", "error");
      return {false, approved_documents};
    }

    // control if the trx of doc cost payment is valid
    // control if the trx of doc cost payment is valid
    if (costPayerTrx->getRef() != a_close_pledge->getDocHash())
    {
      CLog::log("The adm Polling cost is not payed by trx! close pledge(" + a_close_pledge->getDocHash() + ")! block(" + block->getBlockHash() +") " + ")! payer ref(" +
        costPayerTrx->getRef() +") ", "sec", "error");
      return {false, approved_documents};
    }

    // if ref-pledge is PledgeP class,
    if (the_ref_pledge.value("pgd_class").toString() != CConsts::PLEDGE_CLASSES::PledgeP)
    {
      CLog::log("System still doesn't support, closing dClass(" + the_ref_pledge.value("pgd_class").toString() + ") pledge(" + a_close_pledge->getDocHash() + ")! block(" + block->getBlockHash() +") " + ")! payer ref(" +
        costPayerTrx->getRef() +") ", "sec", "error");
      return {false, approved_documents};
    }

    approved_documents.push_back(BlockApprovedDocument {
      a_close_pledge,
      a_close_pledge->getDocHash(),
      a_close_pledge->m_doc_ext_info,
      a_close_pledge->m_doc_ext_hash});
  }

  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    CLog::log("Block has duplicated approved ballot! Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
    return {false, approved_documents};
  };

  if (close_pledges.size() != approved_documents.size())
    return {false, approved_documents};


  // control if all documents are approved
  for (auto a_close_pledge: close_pledges)
  {
    if (!approved_doc_hashes.contains(a_close_pledge->getDocHash()))
    {
      CLog::log("The ballot is not approved!! ballot(" + CUtils::hash8c(a_close_pledge->getDocHash()) + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }
  }

  return {true, approved_documents};
}


//js name was validateClosePledges
bool ClosePledgesInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& grpdDocuments,
  const QString& stage)
{
  auto[status, approved_documents] = preControl(block, grpdDocuments, stage);
  Q_UNUSED(approved_documents);
  return status;
}

