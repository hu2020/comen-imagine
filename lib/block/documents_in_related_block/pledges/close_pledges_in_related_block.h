#ifndef CLOSEPLEDGESINRELATEDBLOCK_H
#define CLOSEPLEDGESINRELATEDBLOCK_H


class ClosePledgesInRelatedBlock
{
public:
  ClosePledgesInRelatedBlock();

  static std::tuple<bool, std::vector<BlockApprovedDocument> > preControl(
      const Block* block,
      const TransientBlockInfo& grpdDocuments,
      const QString& stage);

  static bool validateInBlock(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

};

#endif // CLOSEPLEDGESINRELATEDBLOCK_H
