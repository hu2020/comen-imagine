#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/document_types/dna_proposal_document.h"
#include "lib/services/contracts/loan/loan_contract_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/block/document_types/pledge_documents/pledge_document.h"

#include "pledges_in_related_block.h"

PledgesInRelatedBlock::PledgesInRelatedBlock()
{

}

// js name was preparePledges
std::tuple<bool, std::vector<BlockApprovedDocument> > PledgesInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  std::vector<BlockApprovedDocument> approved_documents {};
  std::vector<PledgeDocument*> pledges {};

  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::Pledge))
    for (auto a_doc: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::Pledge])
      pledges.push_back(dynamic_cast<PledgeDocument*>(a_doc));

  if (pledges.size() == 0)
    return {true, approved_documents};


  QHash<QString, DNAProposalDocument*> proposals_dict {};
  if (transient_block_info.m_groupped_documents.contains(CConsts::DOC_TYPES::DNAProposal))
  {
    for (Document* a_proposal: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::DNAProposal])
    {
      DNAProposalDocument* tmp_proposal = dynamic_cast<DNAProposalDocument*>(a_proposal);
      proposals_dict[tmp_proposal->getDocHash()] = tmp_proposal;
    }
  }

  CLog::log("The NORMAL block will have some documents of Pledge(" + QString::number(pledges.size()) + ")", "app", "info");

  QString t_now = CUtils::getNow();
  for (PledgeDocument* a_pledge: pledges)
  {


    // control dates
    if (a_pledge->m_pledger_sign_date > block->m_block_creation_date)
    {
      CLog::log("pledger Sign Date is newer than block creation date! (" + a_pledge->m_pledger_sign_date + " > " + block->m_block_creation_date + ")", "sec", "error");
      return {false, approved_documents};
    }

    if (a_pledge->m_pledger_sign_date > t_now)
    {
      CLog::log("pledger Sign Date is newer than block now date! (" + a_pledge->m_pledger_sign_date + ")", "sec", "error");
      return {false, approved_documents};
    }

    if (a_pledge->m_pledgee_sign_date > block->m_block_creation_date)
    {
      CLog::log("pledgee Sign Date is newer than block creation date! (" + a_pledge->m_pledgee_sign_date + " > " + block->m_block_creation_date + ")", "sec", "error");
      return {false, approved_documents};
    }

    if (a_pledge->m_pledgee_sign_date > t_now)
    {
      CLog::log("pledger Sign Date is newer than block now date! (" + a_pledge->m_pledgee_sign_date + ")", "sec", "error");
      return {false, approved_documents};
    }

    if (a_pledge->m_pledger_sign_date > a_pledge->m_pledgee_sign_date)
    {
      CLog::log("pledger Sign Date is newer than is newer than pledgee Sign Date! (" + a_pledge->m_pledger_sign_date + " > " + a_pledge->m_pledgee_sign_date + " )", "sec", "error");
      return {false, approved_documents};
    }

    // TODO: add arbiter signature date & validity controls too


    // control if the trx of doc cost payment is valid
    if (a_pledge->m_redeem_principal <= 0)
    {
      CLog::log("loan principal is less than zero! (" + CUtils::microPAIToPAI6(a_pledge->m_redeem_principal) + " )", "sec", "error");
      return {false, approved_documents};
    }

//    if (a_pledge->m_redeem_repayment_amount < 0)
//    {
//      CLog::log("The repayment value is less than zero! repayment amount(" + CUtils::microPAIToPAI6(a_pledge->m_redeem_repayment_amount) + " )", "sec", "error");
//      return {false, approved_documents};
//    }
    if (a_pledge->m_redeem_annual_interest < 0)
    {
      CLog::log("The annual interest is less than zero! annual interst(" + QString::number(a_pledge->m_redeem_annual_interest) + ") ref pledge" + CUtils::hash8c(a_pledge->getDocHash()) +") block(" + block->getBlockHash() + ")", "sec", "error");
      return {false, approved_documents};
    }

    if (a_pledge->m_redeem_repayment_schedule < 0)
    {
      CLog::log("repayment Schedule is less than zero!! repay schadule(" + QString::number(a_pledge->m_redeem_repayment_schedule ) + ") ref pledge" + CUtils::hash8c(a_pledge->getDocHash()) +") block(" + block->getBlockHash() + ")", "sec", "error");
      return {false, approved_documents};
    }

    LoanDetails loan_details = LoanContractHandler::calcLoanRepayments(
      a_pledge->m_redeem_principal,
      a_pledge->m_redeem_annual_interest,
      a_pledge->m_redeem_repayment_amount,
      a_pledge->m_redeem_repayment_schedule);
    if (!loan_details.m_calculation_status)
    {
      CLog::log("Invalid repayment data! principal(" + QString::number(a_pledge->m_redeem_principal) + ") " +
        " interest(" + QString::number(a_pledge->m_redeem_annual_interest) + ")" +
        " amount(" + QString::number(a_pledge->m_redeem_repayment_amount) + ")" +
        " schedule(" + QString::number(a_pledge->m_redeem_repayment_schedule) + ")" +
        CUtils::hash8c(a_pledge->getDocHash()) +") block(" + block->getBlockHash() + ")", "sec", "error");
      return {false, approved_documents};
    }


    // if proposal & pledge cost is payed?
    Document* proposalCostPayerTrx = transient_block_info.m_transactions_dict[a_pledge->m_transaction_ref];
    QHash<CAddressT, CMPAIValueT> costPayerTrxOutputs {};
    for (TOutput* anOutput: proposalCostPayerTrx->getOutputs())
      costPayerTrxOutputs[anOutput->m_address] = anOutput->m_amount;
    QHash<CAddressT, CMPAIValueT> proposalCostPayerTrxOutputs {};
    for (TOutput* anOutput: proposalCostPayerTrx->getOutputs())
      proposalCostPayerTrxOutputs[anOutput->m_address] = anOutput->m_amount;

    Document* pledgeDocCostPayerTrx = transient_block_info.m_transactions_dict[transient_block_info.m_map_trx_ref_to_trx_hash[a_pledge->getDocHash()]];
    QHash<CAddressT, CMPAIValueT> pledgeDocCostPayerTrxOutputs {};
    for (TOutput* anOutput: pledgeDocCostPayerTrx->getOutputs())
      pledgeDocCostPayerTrxOutputs[anOutput->m_address] = anOutput->m_amount;

  // if pledge is PledgeP class,
  if (a_pledge->m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP)
  {
    // do a bunch of control on pledgeRequest

    if (!proposals_dict.contains(a_pledge->m_proposal_ref))
    {
      CLog::log("referenced proposal for Pledge does not exist in block! proposal(" + CUtils::hash8c(a_pledge->m_proposal_ref) + ") the pledge(" + CUtils::hash8c(a_pledge->getDocHash()) + " block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }

    // validate pledger signature
    DNAProposalDocument* proposal = dynamic_cast<DNAProposalDocument*>(proposals_dict[a_pledge->m_proposal_ref]);
    auto[status_pledger_sign, repayments_number, validate_res_msg] = GeneralPledgeHandler::validatePledgerSignedRequest(
      proposal,
      a_pledge,
      stage,
      block->m_block_creation_date);
    Q_UNUSED(repayments_number);
    if (!status_pledger_sign)
    {
      CLog::log("Invalid pledger signature! proposal(" + CUtils::hash8c(a_pledge->m_proposal_ref) + ") the pledge(" + CUtils::hash8c(a_pledge->getDocHash()) + " block(" + CUtils::hash8c(block->getBlockHash()) + ") " + validate_res_msg, "sec", "error");
      return {false, approved_documents};
    }

    // validate payment trx
    if (proposalCostPayerTrx->getRef() != a_pledge->m_proposal_ref)
    {
      CLog::log("referenced Trx for Pledge Proposal has different payment ref! trx(" +
        CUtils::hash8c(a_pledge->m_transaction_ref) + ") the ref(" +
        CUtils::hash8c(proposalCostPayerTrx->getRef()) + ") the proposal(" +
        CUtils::hash8c(a_pledge->m_proposal_ref) + ") the pledge(" +
        CUtils::hash8c(a_pledge->getDocHash()) + " block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }

    // validate payment trx outputs
    if (!proposalCostPayerTrxOutputs.contains("TP_PROPOSAL") ||
      (proposalCostPayerTrxOutputs["TP_PROPOSAL"] != a_pledge->m_redeem_principal))
    {
      QString msg = "the proposal costs(" + CUtils::microPAIToPAI6(proposalCostPayerTrxOutputs["TP_PROPOSAL"]) + ") ";
      msg += "referenced Trx(" + a_pledge->m_transaction_ref + ") for pledge Proposal(" + CUtils::hash8c(a_pledge->m_proposal_ref) + ") ";
      msg += "has different amount(" + CUtils::microPAIToPAI6(a_pledge->m_redeem_principal) + ")!";
      CLog::log(msg, "sec", "error");
      return {false, approved_documents};
    }

    // calcPldgeDocumentCost
    auto[status_cost, locally_recalculate_doc_dp_cost] = a_pledge->calcDocDataAndProcessCost(
      stage,
      block->m_block_creation_date);
    if(!status_cost)
    {
      CLog::log("Invalid cost! proposal(" + CUtils::hash8c(a_pledge->m_proposal_ref) + ") the pledge(" + CUtils::hash8c(a_pledge->getDocHash()) + " block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }

    if (!pledgeDocCostPayerTrxOutputs.contains("TP_PLEDGE") ||
      (pledgeDocCostPayerTrxOutputs.value("TP_PLEDGE") < locally_recalculate_doc_dp_cost))
    {
      QString msg = "The remote pledge contract costs(" + CUtils::microPAIToPAI6(pledgeDocCostPayerTrxOutputs.value("TP_PLEDGE")) + ") referenced Trx(" + CUtils::hash8c(a_pledge->m_transaction_ref) + ") ";
      msg += "for pledge-doc contract(" + CUtils::hash8c(a_pledge->m_proposal_ref) + ") ";
      msg += "has different than locally recalculated amount(" + CUtils::microPAIToPAI6(locally_recalculate_doc_dp_cost) + ")! ";
      CLog::log(msg, "sec", "error");
      return {false, approved_documents};
    }

  } else {
    CLog::log("System doesn't support dClass(" + a_pledge->m_doc_class + ") !", "sec", "error");
    return {false, approved_documents};

  }

    approved_documents.push_back(BlockApprovedDocument {
      a_pledge,
      a_pledge->getDocHash(),
      a_pledge->m_doc_ext_info,
      a_pledge->m_doc_ext_hash
    });
  }

  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    CLog::log("Block has duplicated approved pledge! Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
    return {false, approved_documents};
  };

  if (pledges.size() != approved_documents.size())
    return {false, approved_documents};

  // control if all documents are approved
  for (auto a_plg: pledges)
  {
    if (!approved_doc_hashes.contains(a_plg->getDocHash()))
    {
      CLog::log("pledge is not approved! pledge(" + CUtils::hash8c(a_plg->getDocHash()) + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }
  }

  return {true, approved_documents};
}


// js name was validatePledges
bool PledgesInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{

  auto[status, approved_documents] = preControl(block, transient_block_info, stage);
  Q_UNUSED(approved_documents);
  return status;
}

std::tuple<bool, bool, QString> PledgesInRelatedBlock::appendPledgesToBlock(
  Block* block,
  TransientBlockInfo& transient_block_info)
{
  auto[status, approved_documents] = preControl(block, transient_block_info, CConsts::STAGES::Creating);
  if (!status)
    return {false, false, "failed in pledges pre control"};

  if (approved_documents.size() > 0)
  {
    for (BlockApprovedDocument a_doc: approved_documents)
    {
      block->appendToDocuments(a_doc.m_approved_doc);
//        block.bExtInfo.push(aDoc._dExtInfo);
//        delete aDoc._doc.dExtInfo;
//        block.docs.push(aDoc._doc);
//        docsHashes.push(aDoc._docHash);
//        externalInfoHashes.push(aDoc._dExtHash);
    }
  }

  return {true, true, "Proposals are appended to block" };
}
