#ifndef ADMINISTRATIVEPOLLINGSINRELATEDBLOCK_H
#define ADMINISTRATIVEPOLLINGSINRELATEDBLOCK_H

class AdministrativePollingDocument;

class AdministrativePollingsInRelatedBlock
{
public:
  AdministrativePollingsInRelatedBlock();

  static std::tuple<bool, std::vector<BlockApprovedDocument> > preControl(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static bool validateInBlock(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static std::tuple<bool, bool, QString> appendAdmPollingsToBlock(
    Block* block,
    TransientBlockInfo& transient_block_info);

};

#endif // ADMINISTRATIVEPOLLINGSINRELATEDBLOCK_H
