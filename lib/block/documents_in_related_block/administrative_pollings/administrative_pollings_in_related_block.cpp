#include "stable.h"

#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/polling_document.h"
#include "lib/block/document_types/administrative_polling_document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/polling/polling_handler.h"

#include "administrative_pollings_in_related_block.h"

AdministrativePollingsInRelatedBlock::AdministrativePollingsInRelatedBlock()
{

}

std::tuple<bool, std::vector<BlockApprovedDocument> > AdministrativePollingsInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  std::vector<BlockApprovedDocument> approved_documents {};
  std::vector<AdministrativePollingDocument*> adm_pollings {};

  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::AdmPolling))
    for (auto a_doc: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::AdmPolling])
      adm_pollings.push_back(dynamic_cast<AdministrativePollingDocument*>(a_doc));

  if (adm_pollings.size() == 0)
    return {true, approved_documents};

  // controll pollings as well
  QVector<PollingDocument*> pollings {};
  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::Polling))
    for (auto a_doc: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::Polling])
      pollings.push_back(dynamic_cast<PollingDocument*>(a_doc));

  if (pollings.size() == 0)
  {
    CLog::log("There is no polling since block contains atleast one adm Polling! block(" + block->getBlockHash(), "sec", "error");
    return {false, approved_documents};
  }

  CLog::log("The NORMAL block will have " + QString::number(adm_pollings.size()) + " adm polling documents. block(" + block->getBlockHash() + ") ", "app", "trace");

  for (AdministrativePollingDocument* anAdmPll: adm_pollings)
  {
    // has Valid Subject
    // TODO: do more complete checks such as pFee, pShare, timeframe...
    if (!SocietyRules::getAdmDefaultValues().keys().contains(anAdmPll->m_polling_subject))
    {
      CLog::log("invalid adm polling subject(" + anAdmPll->m_polling_subject + ") for adm polling(" + anAdmPll->getDocHash() + ")! block(" + block->getBlockHash(), "sec", "error");
      return {false, approved_documents};
    }

    // validate payment trx outputs
    auto[status, locally_recalculate_doc_dp_cost] = anAdmPll->calcDocDataAndProcessCost(
      stage,
      block->m_block_creation_date);
    if (!status)
      return {false, approved_documents};

    Document* costPayerTrx = transient_block_info.m_transactions_dict[transient_block_info.m_map_trx_ref_to_trx_hash[anAdmPll->getDocHash()]];
    QHash<CAddressT, CMPAIValueT> costPayerTrxOutputs {};
    for (TOutput* anOutput: costPayerTrx->getOutputs())
      costPayerTrxOutputs[anOutput->m_address] = anOutput->m_amount;


    if (!costPayerTrxOutputs.contains("TP_ADM_POLLING") ||
        (costPayerTrxOutputs["TP_ADM_POLLING"] < locally_recalculate_doc_dp_cost))
    {
      CLog::log("referenced Trx for adm Polling costs has less payment! pays(" + CUtils::sepNum(costPayerTrxOutputs["TP_ADM_POLLING"]) + ") mcPAIs < " + CUtils::sepNum(locally_recalculate_doc_dp_cost) + " mcPAIs for adm polling(" + anAdmPll->getDocHash() + ")! block(" + block->getBlockHash(), "sec", "error");
      return {false, approved_documents};
    }

    // control if the trx of doc cost payment is valid
    if (costPayerTrx->getRef() != anAdmPll->getDocHash())
    {
      CLog::log("The adm Polling cost is not payed by trx! adm polling(" + anAdmPll->getDocHash() + ")! block(" + block->getBlockHash() +") " + ")! payer ref(" +
        costPayerTrx->getRef() +") ", "sec", "error");
      return {false, approved_documents};
    }

//    let unlockSet, isValidUnlock;
//    bool mustCutExtInfoAtTheEnd = false;

    QJsonObject dExtInfo;
    if (anAdmPll->m_doc_ext_info[0].toObject().keys().size() > 0 )
    {
      dExtInfo = anAdmPll->m_doc_ext_info[0].toObject();
    } else {
      dExtInfo = block->m_block_ext_info[transient_block_info.m_doc_index_by_hash[anAdmPll->getDocHash()]].toObject();
    }


    /**
     * some control if polling Doc is valid...
     * indeed the main controlls for polling validate take place in polling modules (e.g. pollings-in-related-block & general-poll-handler)
     **/
    if (!transient_block_info.m_map_referenced_to_referencer.contains(anAdmPll->getDocHash()))
    {
      CLog::log("The adm Polling is not referenced by any document!! adm polling(" + anAdmPll->getDocHash() + ")! block(" + block->getBlockHash() +") " + ")! payer ref(" +
        costPayerTrx->getRef() +") ", "sec", "error");
      return {false, approved_documents};
    }

    QString pollingDocHash = transient_block_info.m_map_referenced_to_referencer[anAdmPll->getDocHash()];
    if (!transient_block_info.m_doc_by_hash.contains(pollingDocHash))
    {
      CLog::log("The adm Polling referenced polling is not presented in block! adm polling(" + anAdmPll->getDocHash() + ")! ref(" + pollingDocHash +") " + ")!", "sec", "error");
      return {false, approved_documents};
    }

    PollingDocument* pollingDoc = dynamic_cast<PollingDocument*>(transient_block_info.m_doc_by_hash[pollingDocHash]);
    if ((pollingDoc->m_doc_type != CConsts::DOC_TYPES::Polling) ||
        (pollingDoc->m_doc_class != PollingHandler::POLLING_PROFILE_CLASSES["Basic"]["ppName"].toString()))
    {
      CLog::log("The adm Polling is not referenced by any polling document! it is (" + pollingDoc->getDocHash() + " / " + pollingDoc->m_doc_type + " / " + pollingDoc->m_doc_class + ")! ref(" + pollingDocHash +")", "sec", "error");
      return {false, approved_documents};
    }


    // TODO: maybe add creator validate!(probably it is not important)


//    if (mustCutExtInfoAtTheEnd)
//        delete anAdmPll.dExtInfo;

    approved_documents.push_back(BlockApprovedDocument {
      anAdmPll,
      anAdmPll->getDocHash(),
      anAdmPll->m_doc_ext_info,
      anAdmPll->m_doc_ext_hash
    });

  }


  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    CLog::log("Block has duplicated approved adm polling! Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
    return {false, approved_documents};
  };

  if (adm_pollings.size() != approved_documents.size())
    return {false, approved_documents};

  // control if all documents are approved
  for (auto anAdmPll: adm_pollings)
  {
    if (!approved_doc_hashes.contains(anAdmPll->getDocHash()))
    {
      CLog::log("Adm polling is not approved!! adm poll(" + CUtils::hash8c(anAdmPll->getDocHash()) + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")", "sec", "error");
      return {false, approved_documents};
    }
  }

  return {true, approved_documents};
}


bool AdministrativePollingsInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  auto[status, approved_documents] = preControl(block, transient_block_info, stage);
  Q_UNUSED(approved_documents);
  return status;
}

std::tuple<bool, bool, QString> AdministrativePollingsInRelatedBlock::appendAdmPollingsToBlock(
  Block* block,
  TransientBlockInfo& transient_block_info)
{
  auto[status, approved_documents] = preControl(
    block,
    transient_block_info,
    CConsts::STAGES::Creating);
  if (!status)
    return {false, false, "failed in appending adm-polling pre control"};

  if (approved_documents.size() > 0)
    for (BlockApprovedDocument a_doc: approved_documents)
      block->appendToDocuments(a_doc.m_approved_doc);

  return {true, true, "adm-pollings are appended to block" };
}

