#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/block_types/block.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/ballot_document.h"
#include "lib/services/polling/ballot_handler.h"
#include "lib/services/society_rules/society_rules.h"

#include "ballots_in_related_block.h"

BallotsInRelatedBlock::BallotsInRelatedBlock()
{

}

std::tuple<bool, QString, std::vector<BlockApprovedDocument>, QStringList> BallotsInRelatedBlock::preControl(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  QString msg;
  std::vector<BlockApprovedDocument> approved_documents {};
  std::vector<BallotDocument*> ballots {};

  if (transient_block_info.m_groupped_documents.keys().contains(CConsts::DOC_TYPES::Ballot))
    for (auto a_doc: transient_block_info.m_groupped_documents[CConsts::DOC_TYPES::Ballot])
      ballots.push_back(dynamic_cast<BallotDocument*>(a_doc));

  if (ballots.size() == 0)
    return {true, "", approved_documents, {}};

  QStringList pre_requisities_ancestors_ {};

  for (BallotDocument* a_ballot: ballots)
  {
    // validate vote range
    if ((a_ballot->m_vote < -100) || (a_ballot->m_vote > 100))
    {
      msg = "vote value is invalid! block(" + CUtils::hash8c(block->getBlockHash()) + ") vote(" + QString::number(a_ballot->m_vote) + ")";
      CLog::log(msg, "sec", "error");
      return {false, msg, approved_documents, {}};
    }

    // validate voter
    if (!CCrypto::isValidBech32(a_ballot->m_voter))
    {
      msg = "invalid voter(" + a_ballot->m_voter + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ") vote(" + QString::number(a_ballot->m_vote) + ")";
      CLog::log(msg, "sec", "error");
      return {false, msg, approved_documents, {}};
    }

    // validate voter shares
    auto[shares_, percentage] = DNAHandler::getAnAddressShares(a_ballot->m_voter, block->m_block_creation_date);
    Q_UNUSED(shares_);
    if (percentage < SocietyRules::getMinShareToAllowedVoting(block->m_block_creation_date))
    {
      msg = "There is no enough share for voter(" + a_ballot->m_voter + ") block(" + CUtils::hash8c(block->getBlockHash()) + ") vote(" + QString::number(a_ballot->m_vote) + ")";
      CLog::log(msg, "sec", "error");
      return {false, msg, approved_documents, {}};
    }

    auto[status, locally_recalculate_doc_dp_cost] = a_ballot->calcDocDataAndProcessCost(
      stage,
      block->m_block_creation_date);
    if (!status)
      return {false, "Failed in calc-Doc-Data-And-Process-Cost for a Ballot", approved_documents, {}};

    Document* cost_payer_trx = transient_block_info.m_transactions_dict[transient_block_info.m_map_trx_ref_to_trx_hash[a_ballot->getDocHash()]];
    QHash<CAddressT, CMPAIValueT> cost_payer_trx_outputs {};
    for (TOutput* anOutput: cost_payer_trx->getOutputs())
      cost_payer_trx_outputs[anOutput->m_address] = anOutput->m_amount;

    if (!cost_payer_trx_outputs.contains("TP_BALLOT"))
    {
      msg = "The ballot has not payer transaction! stage(" + stage + "), ballot(" + a_ballot->getDocHash() + ")! block(" + block->getBlockHash() + " Ballot: " + a_ballot->safeStringifyDoc(true);
      CLog::log(msg, "sec", "error");
      return {false, msg, approved_documents, {}};
    }

    if (cost_payer_trx_outputs["TP_BALLOT"] < locally_recalculate_doc_dp_cost)
    {
      msg = "referenced Trx for ballot costs has less payment! stage(" + stage + "), pays(" + CUtils::microPAIToPAI6(cost_payer_trx_outputs["TP_BALLOT"]) + ") PAIs < " + CUtils::microPAIToPAI6(locally_recalculate_doc_dp_cost) + " PAIs for a ballot(" + a_ballot->getDocHash() + ")! block(" + block->getBlockHash() + " Ballot: " + a_ballot->safeStringifyDoc(true);
      CLog::log(msg, "sec", "error");
      return {false, msg, approved_documents, {}};
    }

    // control if the trx of doc cost payment is valid
    if (cost_payer_trx->getRef() != a_ballot->getDocHash())
    {
      msg = "The adm Polling cost is not payed by trx! ballot(" + a_ballot->getDocHash() + ")! block(" + block->getBlockHash() +") " + ")! payer ref(" +cost_payer_trx->getRef() +") ";
      CLog::log(msg, "sec", "error");
      return {false, msg, approved_documents, {}};
    }

    bool already_voted = BallotHandler::alreadyVoted(
        a_ballot->m_voter,
        a_ballot->m_doc_ref);
    bool vote_amendment_allowed = (CConsts::NO != CConsts::NO);   // TODO: it must be retreived from dynamc POLLING_PROFILE_CLASSES. implement it!
    if (already_voted)
    {
      if (!vote_amendment_allowed)
      {
        msg = "vote amendment is not allowed! voter(" + a_ballot->m_voter + ") vote(" + a_ballot->m_vote + ") ballot(" + a_ballot->getDocHash() + ") block(" + block->getBlockHash() +") " + ")! payer ref(" + cost_payer_trx->getRef() +") ";
        CLog::log(msg, "sec", "error");
        return {false, msg, approved_documents, {}}; // it could be happend because of another thread already validated and inserted block, so this threas just drops the block gracefully
      }
    }

    if (stage == CConsts::STAGES::Creating)
    {
      // control if referenced polling exist in DAG and also adding polling-container-blockHash
      // to newly-creating-block as an acestor
      // in such a way other machine if they do not have polling block in local DAG,
      // they do not process this block until receiving prerequsit block
      auto[block_hashes, map_doc_to_block] = DAG::getBlockHashesByDocHashes({a_ballot->m_doc_ref});
      Q_UNUSED(map_doc_to_block);
      pre_requisities_ancestors_ = CUtils::arrayUnique(CUtils::arrayAdd(pre_requisities_ancestors_, block_hashes));
    }

    approved_documents.push_back(BlockApprovedDocument {
      a_ballot,
      a_ballot->getDocHash(),
      a_ballot->m_doc_ext_info,
      a_ballot->m_doc_ext_hash});
  }


  QStringList approved_doc_hashes {};
  for (auto a_doc: approved_documents)
    approved_doc_hashes.append(a_doc.m_approved_doc_hash);
  if (approved_doc_hashes.size() != CUtils::arrayUnique(approved_doc_hashes).size())
  {
    msg = "Block has duplicated approved ballot! Block(" + CUtils::hash8c(block->getBlockHash()) + ")";
    CLog::log(msg, "sec", "error");
    return {false, msg, approved_documents, {}};
  };

  if (ballots.size() != approved_documents.size())
    return {false, "Failed in Ballots count", approved_documents, {}};


  // control if all documents are approved
  for (auto a_ballot: ballots)
  {
    if (!approved_doc_hashes.contains(a_ballot->getDocHash()))
    {
      msg = "The ballot is not approved!! ballot(" + CUtils::hash8c(a_ballot->getDocHash()) + ") Block(" + CUtils::hash8c(block->getBlockHash()) + ")";
      CLog::log(msg, "sec", "error");
      return {false, msg, approved_documents, {}};
    }
  }

  return {true, "", approved_documents, pre_requisities_ancestors_};
}

bool BallotsInRelatedBlock::validateInBlock(
  const Block* block,
  const TransientBlockInfo& transient_block_info,
  const QString& stage)
{
  auto[status, control_msg, approved_documents, pre_requisities_ancestors_] = preControl(block, transient_block_info, stage);
  Q_UNUSED(approved_documents);
  Q_UNUSED(pre_requisities_ancestors_);
  return status;
}

std::tuple<bool, bool, QString> BallotsInRelatedBlock::appendBallotsToBlock(
  Block* block,
  TransientBlockInfo& transient_block_info)
{
  auto[status, control_msg, approved_documents, pre_requisities_ancestors] = preControl(block, transient_block_info, CConsts::STAGES::Creating);
  if (!status)
    return {false, false, control_msg};

  if (approved_documents.size() > 0)
    for (BlockApprovedDocument a_doc: approved_documents)
      block->appendToDocuments(a_doc.m_approved_doc);

  transient_block_info.m_pre_requisities_ancestors = CUtils::arrayAdd(transient_block_info.m_pre_requisities_ancestors, pre_requisities_ancestors);
  return {true, true, "Ballots are appended to block" };
}
