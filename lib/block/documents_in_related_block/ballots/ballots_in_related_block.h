#ifndef BALLOTSINRELATEDBLOCK_H
#define BALLOTSINRELATEDBLOCK_H


class BallotsInRelatedBlock
{
public:
  BallotsInRelatedBlock();

  static std::tuple<bool, QString, std::vector<BlockApprovedDocument>, QStringList> preControl(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static bool validateInBlock(
    const Block* block,
    const TransientBlockInfo& grpdDocuments,
    const QString& stage);

  static std::tuple<bool, bool, QString> appendBallotsToBlock(
    Block* block,
    TransientBlockInfo& transient_block_info);
};

#endif // BALLOTSINRELATEDBLOCK_H
