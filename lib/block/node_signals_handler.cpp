#include "node_signals_handler.h"


NodeSignalsHandler::NodeSignalsHandler()
{

}

const QString NodeSignalsHandler::stbl_signals = "c_signals";
const QStringList NodeSignalsHandler::stbl_signals_fields = {"sig_id", "sig_block_hash", "sig_signaler", "sig_key", "sig_value", "sig_creation_date"};

void NodeSignalsHandler::logSignals(const Block &block)
{
  for (QJsonValue aSignal_ : block.m_signals)
  {
    QJsonObject aSignal = aSignal_.toObject();
    QString sigKey = aSignal.value("sig").toString();
    QString sigVal;
    if (aSignal.keys().contains("val"))
    {
      auto tmpSigVal = aSignal.value("val");
      if (tmpSigVal.isObject())
      {
        sigVal = CUtils::serializeJson(tmpSigVal.toObject());

      }
      else if (tmpSigVal.isString())
      {
        sigVal = tmpSigVal.toString();

      }
    }
    else
    {
      // backward compatibility
      sigVal = aSignal.value("ver").toString();
    }

    QVDicT values{
      {"sig_signaler", block.m_block_backer},
      {"sig_block_hash", block.m_block_hash},
      {"sig_key", sigKey},
      {"sig_value", sigVal},
      {"sig_creation_date", block.m_block_creation_date}};

    DbModel::insert(
      NodeSignalsHandler::stbl_signals,     // table
      values, // values to insert
      true
    );
  }
}

QVDRecordsT NodeSignalsHandler::searchInSignals(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int& limit)
{
  QueryRes res = DbModel::select(
    stbl_signals,
    fields,
    clauses,
    order,
    limit);

  return res.records;
}



QJsonObject NodeSignalsHandler::getMachineSignals()
{
  QJsonObject signals_ {
    {"nodeInfo", QJsonObject {{"spec", "C++"}, {"ver", CConsts::CLIENT_VERSION}}},
    {"P4Psupport", CConsts::NO}};

  //signals = listener.doCallSync('SASH_signals', { signals_ });
  return signals_;
}

