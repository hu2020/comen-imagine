#include "stable.h"

#include "block.h"
#include "block_genesis/genesis_block.h"
#include "block_normal/normal_block.h"
#include "block_coinbase/coinbase_block.h"
#include "block_repayback/repayback_block.h"
#include "block_pow/pow_block.h"
#include "block_floating_signature/floating_signature_block.h"
#include "block_floating_vote/floating_vote_block.h"

#include "block_factory.h"

BlockFactory::BlockFactory()
{

}


Block* BlockFactory::create(const QJsonObject &obj)
{
  QString block_type = obj.value("bType").toString();
  if (block_type == CConsts::BLOCK_TYPES::Normal)
  {
    return new NormalBlock(obj);

  }
  else if (block_type == CConsts::BLOCK_TYPES::Coinbase)
  {
    Block *b{new CoinbaseBlock(obj)};
    return b;

  }
  else if (block_type == CConsts::BLOCK_TYPES::RpBlock)
  {
    Block *b{new RepaybackBlock(obj)};
    return b;

  }
  else if (block_type == CConsts::BLOCK_TYPES::FSign)
  {
    Block *b{new FloatingSignatureBlock(obj)};
    return b;

  }
  else if (block_type == CConsts::BLOCK_TYPES::FVote)
  {
    Block *b{new FloatingVoteBlock(obj)};
    return b;

  }
  else if (block_type == CConsts::BLOCK_TYPES::POW)
  {
    Block *b{new POWBlock(obj)};
    return b;

  }
  else if (block_type == CConsts::BLOCK_TYPES::Genesis)
  {
    return new GenesisBlock(obj);
  }

  return new Block(obj);
}
