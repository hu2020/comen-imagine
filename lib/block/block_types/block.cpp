#include "stable.h"

class Document;
class NodeSignalsHandler;
class DbHandler;
class CCrypto;

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/dag/dag.h"
#include "lib/dag/leaves_handler.h"
#include "lib/block/block_ext_info.h"
#include "lib/file_handler/file_handler.h"
#include "lib/block/node_signals_handler.h"
#include "lib/block/document_types/document.h"
#include "lib/dag/sceptical_dag_integrity_control.h"
#include "lib/parsing_q_handler/parsing_q_handler.h"
#include "lib/dag/normal_block/normal_utxo_handler.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/document_ext_info_types/document_ext_info.h"
#include "lib/block/block_types/block_coinbase/coinbase_utxo_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "block.h"

QString TransientBlockInfo::dumpMe()
{
  QString out = "\n Block total outputs amount: " + QString::number(m_block_total_output);
  out += "\n COMPLETE ME!";
  return out;
}

//  -  -  -  Block Record
bool BlockRecord::setByRecordDict(const QVDicT& values)
{
  if (values.value("b_id", "").toString() != "")
    m_id = values.value("b_id", "").toUInt();
  if (values.value("b_hash", "").toString() != "")
    m_hash = values.value("b_hash", "").toString();
  if (values.value("b_type", "").toString() != "")
    m_type = values.value("b_type", "").toString();
  if (values.value("b_cycle", "").toString() != "")
    m_cycle = values.value("b_cycle", "").toString();

  if (values.value("b_confidence", "").toString() != "")
    m_confidence = values.value("b_confidence", "").toFloat();

  if (values.value("b_ext_root_hash", "").toString() != "")
    m_ext_root_hash = values.value("b_ext_root_hash", "").toString();
  if (values.value("b_docs_root_hash", "").toString() != "")
    m_documents_root_hash = values.value("b_docs_root_hash", "").toString();
  if (values.value("b_signals", "").toString() != "")
    m_signals = values.value("b_signals", "").toString();
  if (values.value("b_trxs_count", "").toString() != "")
    m_trxs_count = values.value("b_trxs_count", "").toUInt();
  if (values.value("b_docs_count", "").toString() != "")
    m_docs_count = values.value("b_docs_count", "").toUInt();

  if (values.value("b_ancestors_count", "").toString() != "")
    m_ancestors_count = values.value("b_ancestors_count", "").toUInt();

  if (values.value("b_ancestors", "").toString() != "")
    m_ancestors = values.value("b_ancestors", "").toString().split(",");

  if (values.value("b_descendents", "").toString() != "")
    m_descendents = values.value("b_descendents", "").toString().split(",");

  if (values.value("b_body", "").toString() != "")
    m_body = values.value("b_body", "").toString();
  if (values.value("b_creation_date", "").toString() != "")
    m_creation_date = values.value("b_creation_date", "").toString();
  if (values.value("b_receive_date", "").toString() != "")
    m_receive_date = values.value("b_receive_date", "").toString();
  if (values.value("b_confirm_date", "").toString() != "")
    m_confirm_date = values.value("b_confirm_date", "").toString();
  if (values.value("b_backer", "").toString() != "")
    m_block_backer = values.value("b_backer", "").toString();
  if (values.value("b_utxo_imported", "").toString() != "")
    m_utxo_imported = values.value("b_utxo_imported", "").toString();
  return true;
}

//  -  -  -  Block

const QString Block::stbl_blocks = "c_blocks";
const QStringList Block::stbl_blocks_fields = {"b_id", "b_hash", "b_type", "b_cycle", "b_confidence", "b_ext_root_hash", "b_docs_root_hash", "b_signals", "b_trxs_count", "b_docs_count", "b_ancestors_count", "b_ancestors", "b_descendents", "b_body", "b_creation_date", "b_receive_date", "b_confirm_date", "b_backer", "b_utxo_imported"};

const QString Block::stbl_block_extinfos = "c_block_extinfos";
const QStringList Block::stbl_block_extinfos_fields = {};

Block::Block()
{
}

Block::~Block()
{
  // delete documents
  for(Document* d: m_documents)
    delete d;
}

/**
 * @brief Block::setByReceivedJsonDoc
 * @param obj
 * @return
 * converts a JSon object(based on parsing text stream) to a standard c++ object
 */
bool Block::
setByJsonObj(const QJsonObject& obj)
{
  QStringList object_keys = obj.keys();

  if (obj.value("local_receive_date").toString() != "")
    m_block_receive_date = obj.value("local_receive_date").toString();

  if (obj.value("net").toString() != "")
    m_net = obj.value("net").toString();

  if (obj.value("bVer").toString() != "")
    m_block_version = obj.value("bVer").toString();

  if (obj.value("bType").toString() != "")
    m_block_type = obj.value("bType").toString();

  if (obj.value("descriptions").toString() != "")
    m_block_descriptions = obj.value("descriptions").toString();

  if (object_keys.contains("confidence"))
    m_block_confidence = obj.value("confidence").toDouble();

  // JS backward compatibility
  if (obj.value("blockLength").toString() != "")
    m_block_length = static_cast<BlockLenT>(CUtils::convertPaddedStringToInt(obj.value("blockLength").toString()));

  if (obj.value("bLen").toString() != "")
    m_block_length = static_cast<BlockLenT>(CUtils::convertPaddedStringToInt(obj.value("bLen").toString()));

  // JS backward compatibility
  if (obj.value("blockHash").toString() != "")
    m_block_hash = obj.value("blockHash").toString();

  if (obj.value("bHash").toString() != "")
    m_block_hash = obj.value("bHash").toString();

  if (obj.value("ancestors").toArray().size() > 0)
    m_ancestors = CUtils::convertJSonArrayToQStringList(obj.value("ancestors").toArray());

  if (obj.value("signals").toObject().keys().size() > 0)
    m_signals = obj.value("signals").toObject();

  //JS backward
  if (obj.value("creationDate").toString() != "")
    m_block_creation_date = obj.value("creationDate").toString();

  if (obj.value("bCDate").toString() != "")
    m_block_creation_date = obj.value("bCDate").toString();


  if (obj.value("docsRootHash").toString() != "")
    m_documents_root_hash = obj.value("docsRootHash").toString();

  if (obj.value("bExtHash").toString() != "")
    m_block_ext_root_hash = obj.value("bExtHash").toString();

  if (obj.keys().contains("bExtInfo"))
    m_block_ext_info = obj.value("bExtInfo").toArray();

  if (object_keys.contains("docs"))
    createDocuments(obj.value("docs"));

  if (obj.value("cycle").toString() != "")
    m_cycle = obj.value("cycle").toString();

  if (obj.value("backer").toString() != "")
    m_block_backer = obj.value("backer").toString();

  if (obj.value("fVotes").toString() != "")
    m_floating_votes = obj.value("fVotes").toArray();

  return true;
}

/**
 * @brief Block::objectAssignmentsControlls
 * @return
 *
 * the tests to avoid injection/maleformed data in received Jsons
 * FIXME: add more tests
 */
bool Block::objectAssignmentsControlls()
{
  if (!CUtils::isValidHash(m_block_hash))
  {
    CLog::log("Invalid blockHash after js assignment", "sec", "error");
    return false;
  }

  if ((m_block_type != CConsts::BLOCK_TYPES::Coinbase) && !CCrypto::isValidBech32(m_block_backer))
  {
    CLog::log("Invalid block backer after js assignment", "sec", "error");
    return false;
  }



  return true;

}

void Block::doHashObject()
{
}

QString Block::stringifyBExtInfo() const
{
  if (!QStringList {CConsts::BLOCK_TYPES::Coinbase, CConsts::BLOCK_TYPES::Normal, CConsts::BLOCK_TYPES::POW}.contains(m_block_type))
  {
    CLog::log("DUMMY BREAKPOINT LOG :)");
  }

  QJsonArray block_ext_info {};
  for(Document* a_doc: m_documents)
    block_ext_info.append(a_doc->m_doc_ext_info);

  QString out = CUtils::serializeJson(block_ext_info);
  return out;
}

QString Block::safeStringifyBlock(const bool ext_info_in_document) const
{
  QJsonObject block = exportBlockToJSon(ext_info_in_document);

  // maybe remove add some item in object

  // recaluculate block final length
  block["bLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(block).length());

  QString out = CUtils::serializeJson(block);
  CLog::log("Safe sringified block(Base class) Block(" + CUtils::hash8c(m_block_hash) + ") length(" + QString::number(out.length()) + ") the block: " + out, "app", "trace");

  return out;
}

QString Block::getBlockHashableString() const
{
  return "";
}

QString Block::calcBlockHash() const
{
  return "";
}

QStringList Block::getDocumentsHashes(const QJsonObject& block)
{
  QStringList hashes {};
  QJsonArray documents = block.value("docs").toArray();
  for (auto a_doc: documents)
    hashes.append(a_doc.toObject().value("dHash").toString());
  return hashes;
}

QStringList Block::getDocumentsHashes() const
{
  QStringList hashes {};
  for (Document* a_doc: m_documents)
    hashes.append(a_doc->m_doc_hash);
  return hashes;
}

std::tuple<bool, CDocHashT> Block::calcDocumentsRootHash() const
{
  auto[root, verifies, merkle_version, levels, leaves] = CMerkle::generate(getDocumentsHashes());
  Q_UNUSED(verifies);
  Q_UNUSED(merkle_version);
  Q_UNUSED(levels);
  Q_UNUSED(leaves);
  return {true, root};
}

std::tuple<bool, CDocHashT> Block::calcBlockExtRootHash() const
{
  return {true, ""};
}

bool Block::fillInBlockExtInfo()
{
  m_block_ext_info = {};
  for (Document* doc: m_documents)
    m_block_ext_info.push_back(SignatureStructureHandler::compactUnlockersArray(doc->getDocExtInfo()));
  return true;
}

QJsonArray Block::exportDocumentsToJSon(const bool ext_info_in_document) const
{
  QJsonArray documents {};
  for(auto a_doc: m_documents)
  {
    documents.push_back(a_doc->exportDocToJson(ext_info_in_document));
  }
  return documents;
}

QJsonObject Block::exportBlockToJSon(const bool ext_info_in_document) const
{
  QJsonObject out {
    {"ancestors", QVariant::fromValue(m_ancestors).toJsonArray()},
    {"bCDate", m_block_creation_date},
    {"bExtHash", m_block_ext_root_hash},
    {"bExtInfo", m_block_ext_info},
    {"bHash", m_block_hash},
    {"bLen", CUtils::paddingLengthValue(m_block_length)},
    {"bType", m_block_type},
    {"bVer", m_block_version},
    {"docs", exportDocumentsToJSon(ext_info_in_document)},
    {"docsRootHash", m_documents_root_hash},
    {"fVotes", m_floating_votes},
    {"net", m_net},
    {"signals", m_signals}
  };

  if (m_block_backer != "")
    out["backer"] = m_block_backer;

  return out;
}

void Block::calcAndSetBlockLength()
{
  QString stringyfied_block = safeStringifyBlock(false);
  m_block_length = static_cast<BlockLenT>(stringyfied_block.length());
}

BlockLenT Block::calcBlockLength(const QJsonObject& block_obj) const
{
  return CUtils::serializeJson(block_obj).length();
}

std::tuple<bool, bool> Block::handleReceivedBlock() const
{
  return {false, false};
}

BlockLenT Block::getMaxBlockSize() const
{
  return SocietyRules::getMaxBlockSize(m_block_type);
}

bool Block::controlBlockLength() const
{
  QString stringyfied_block = safeStringifyBlock(false);
  if (static_cast<BlockLenT>(stringyfied_block.length()) != m_block_length)
  {
    CLog::log("Mismatch (Base class)block length Block(" + CUtils::hash8c(m_block_hash) + ") local length(" + QString::number(stringyfied_block.length()) + ") remote length(" + QString::number(m_block_length) + ") stringyfied_block:" + stringyfied_block, "sec", "error");
    return false;
  }
  return true;
}


std::tuple<bool, bool> Block::blockGeneralControls() const
{
  if (m_net != CConsts::SOCIETY_NAME)
  {
    CLog::log("Invalid society communication! Block(" + CUtils::hash8c(m_block_hash) + ") Society(" + m_net + ")", "sec", "error");
    return {false, true};
  }

  // block create date control
  if (CUtils::isGreaterThanNow(m_block_creation_date))
  {
    CLog::log("Invalid block creation date! Block(" + CUtils::hash8c(m_block_hash) + ") creation date(" + m_block_creation_date + ")", "sec", "error");
    return {false, true};
  }

  if (m_block_length > getMaxBlockSize())
  {
    CLog::log("Invalid block length block(" + CUtils::hash8c(m_block_hash) + ") length(" + QString::number(m_block_length) + " > MAX: " + QString::number(getMaxBlockSize()) + ")", "sec", "error");
    return {false, true};
  }

  // Block length control
  if (!controlBlockLength())
    return {false, true};


  if ((m_block_version == "") || !CUtils::isValidVersionNumber(m_block_version))
  {
    CLog::log("Invalid bVer block(" + CUtils::hash8c(m_block_hash) + ") block(" + m_block_version + ")", "sec", "error");
    return {false, true};
  }

  if (!CUtils::isValidDateForamt(m_block_creation_date))
  {
    CLog::log("Invalid creation date block(" + CUtils::hash8c(m_block_hash) + ") creation date(" + m_block_creation_date + ")", "sec", "error");
    return {false, true};
  }

  if (CUtils::isGreaterThanNow(m_block_creation_date))
  {
    CLog::log("Block whith future creation date is not acceptable(" + m_block_creation_date + ") not Block(" + m_block_hash + ")!", "sec", "error");
    return {false, true};
  }

  // ancestors control
  if (m_ancestors.size() < 1)
  {
    CLog::log("Invalid ancestors for block(" + CUtils::hash8c(m_block_hash) + ")", "sec", "error");
    return {false, true};
  }

  if (!CUtils::isValidHash(m_block_hash))
  {
    CLog::log("Invalid block Hash(" + CUtils::hash8c(m_block_hash) +")", "sec", "error");
    return {false, true};
  }

  // docRootHash control
  if (m_documents.size() > 0)
  {
    QStringList doc_hashes {};
    for (Document* a_doc: m_documents)
      doc_hashes.append(a_doc->getDocHash());
    auto[root, verifies, version, levels, leaves] = CMerkle::generate(doc_hashes);
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (m_documents_root_hash != root)
    {
      CLog::log("Mismatch block DocRootHash for type(" + m_block_type + ") block(" + CUtils::hash8c(m_block_hash) + ") creation date(" + m_block_creation_date + ")", "sec", "error");
      return {false, true};
    }

    // ext root hash control
    if (m_block_ext_root_hash != "")
    {
      auto[status, block_ext_root_hash] = calcBlockExtRootHash();
      if (!status || (block_ext_root_hash != m_block_ext_root_hash))
      {
        CLog::log("Mismatch block Ext DocRootHash for type(" + m_block_type + ") block(" + CUtils::hash8c(m_block_hash) + ") creation date(" + m_block_creation_date + ")", "sec", "error");
        return {false, true};
      }
    }

    // re-calculate block hash
    QString re_calc_block_hash = calcBlockHash();
    if (re_calc_block_hash != m_block_hash)
    {
      CLog::log("Mismatch block kHash. localy calculated(" + CUtils::hash8c(re_calc_block_hash) +") remote(" + m_block_type + " / " + CUtils::hash8c(m_block_hash) + ") " + safeStringifyBlock(true), "sec", "error");
      return {false, true};
    }

//    Block* tmp_block = new Block(QJsonObject {
//      {"bCDate", this->m_block_creation_date},
//      {"bType", this->m_block_type},
//      {"bHash", this->m_block_hash}});
    for (Document* a_doc: m_documents)
      if (!a_doc->fullValidate(this).status)
      {
//        delete tmp_block;
        return {false, true};
      }

//    delete tmp_block;
  }

  return {true, true};
}

QString Block::getBlockHash() const
{
  return m_block_hash;
}

void Block::setBlockHash(const CBlockHashT& hash)
{
  m_block_hash = hash;
}

QString Block::getBacker() const
{
  return m_block_backer;
}

std::tuple<bool, QString> Block::getBlockSignMsg() const
{
  // by default blocks have no prt to signing
  return {false, ""};
}

QString Block::dumpBlock() const
{
  return "dumpBlock not implemented!";
}

bool Block::addBlockToDAG() const
{
  // duplicate check
  QueryRes existed_blocks = DbModel::select(
    stbl_blocks,
    {"b_hash"},     // fields
    {{"b_hash", m_block_hash}}, //clauses
    {{"b_creation_date", "ASC"}, {"b_id", "ASC"}},   // order
    1   // limit
  );
  if (existed_blocks.records.size() > 0)
    return true;

  // save hard copy of blocks(timestamped by receive date) to have backup
  // in case of curruptions in DAG or bootstrp the DAG, machine doesn't need to download again entire DAG
  // you can simply copy files from ~/backup-dag to folder ~/temporary/inbox
  if (CConsts::DO_HARDCOPY_DAG_BACKUP)
    FileHandler::write(
      CMachine::getDAGBackup(),
      CUtils::getNowSSS() + "_" + m_block_type + "_" + m_block_hash + ".txt",
      safeStringifyBlock(false));

  //TODO: implementing atomicity(transactional) either in APP or DB


  // insert into DB
  QVDicT values {
    {"b_hash", m_block_hash},
    {"b_type", m_block_type},
    {"b_confidence", m_block_confidence},
    {"b_body", BlockUtils::wrapSafeContentForDB(safeStringifyBlock()).content},
    {"b_docs_root_hash", m_documents_root_hash},
    {"b_ext_root_hash", m_block_ext_root_hash},
    {"b_signals", CUtils::serializeJson(m_signals)},
    {"b_trxs_count", 0},
    {"b_docs_count", static_cast<CDocIndexT>(m_documents.size())},
    {"b_ancestors", m_ancestors.join(",")},
    {"b_ancestors_count", static_cast<BlockAncestorsCountT>(m_ancestors.size())},
    {"b_descendents", m_descendents.join(",")},
    {"b_creation_date", m_block_creation_date},
    {"b_receive_date", m_block_receive_date},
    {"b_confirm_date", m_block_confirm_date},
    {"b_cycle", m_cycle},
    {"b_backer", m_block_backer},
    {"b_utxo_imported", CConsts::NO}};

  CLog::log("--- recording block in DAG Block(" + CUtils::hash8c(m_block_hash)+")");
  CLog::log("addBlockToDAG in Thread(" + QString::number((quint64)QThread::currentThread(), 16) + ")", "app", "trace");
  DbModel::insert(
    stbl_blocks,     // table
    values, // values to insert
    true,
    false);

  // add newly recorded block to cache in order to reduce DB load. TODO: improve it
  DAG::updateCachedBlocks(
    m_block_type,
    m_block_hash,
    m_block_creation_date,
    CConsts::NO);

  // recording block ext Info (if exist)
  QString bExtInfo = stringifyBExtInfo();
  if (bExtInfo != "")
    insertBlockExtInfoToDB(bExtInfo, m_block_hash, m_block_creation_date);

  // adjusting leave blocks
  LeavesHandler::removeFromLeaveBlocks(m_ancestors);
  LeavesHandler::addToLeaveBlocks(m_block_hash, m_block_creation_date, m_block_type);

  // insert block signals
  NodeSignalsHandler::logSignals(*this);


  if (m_documents.size() > 0)
  {
    for (CDocIndexT doc_inx=0; doc_inx<m_documents.size(); doc_inx++)
    {
      //FIXME: implement suspicious docs filtering!

      Document* a_doc = m_documents[doc_inx];

      a_doc->applyDocFirstImpact(*this);

      // connect documents and blocks
      a_doc->mapDocToBlock(m_block_hash, doc_inx);

    }
  }

  // update ancestor's descendent info
  DAG::appendDescendents(m_ancestors, {m_block_hash});

  // sceptical_dag_integrity_controls
  GenRes scepRes = ScepticalDAGIntegrityControl::insertNewBlockControlls(m_block_hash);
  if (scepRes.status != true)
  {
    CLog::log("error in sceptical Data Integrity Check: block(" + CUtils::hash8c(m_block_hash) + ") ", "app", "error");
    return false;
  }

  if (m_block_type != CConsts::BLOCK_TYPES::Genesis)
    if (!CMachine::isInSyncProcess())
       CGUI::signalUpdateBlocks();

  {
    // TODO: remove this block(variable/mechanism) after fixing sqlite database lock problem
    if (CMachine::get().m_recorded_blocks_in_db == 0)
    {
      QueryRes res = DbModel::customQuery(
        "db_comen_blocks",
        "SELECT COUNT(*) AS count_blocks FROM c_blocks",
        {"count_blocks"},
        0,
        {},
        false,
        true);
      CMachine::get().m_recorded_blocks_in_db = res.records[0].value("count_blocks").toInt();

    }else{
      CMachine::get().m_recorded_blocks_in_db++;

    }
   }

  return true;
}


bool Block::postAddBlockToDAG() const
{
  // remove perequisity, if any block in parsing Q was needed to this block

  ParsingQHandler::removePrerequisites(m_block_hash);

  /**
  * sometimes (e.g. repayback blocks which can be created by delay and causing to add block to missed blocks)
  * we need to doublecheck if the block still is in missed blocks list and remove it
  */
  MissedBlocksHandler::removeFromMissedBlocks(getBlockHash());

  /**
  * inherit UTXO visibilities of ancestors of newly DAG-added block
  * current block inherits the visibility of it's ancestors
  * possibly first level ancestors can be floating signatures(which haven't entry in table trx_utxos),
  * so add ancestors of ancestors too, in order to being sure we keep good and reliable history in utxos
  */
  if (!QStringList {CConsts::BLOCK_TYPES::FSign, CConsts::BLOCK_TYPES::FVote}.contains(m_block_type))
  {
    QStringList ancestors = m_ancestors;
    ancestors = CUtils::arrayAdd(ancestors, DAG::getAncestors(ancestors));
    ancestors = CUtils::arrayAdd(ancestors, DAG::getAncestors(ancestors));
    ancestors = CUtils::arrayUnique(ancestors);
    UTXOHandler::inheritAncestorsVisbility(
      ancestors,
      m_block_creation_date,
      getBlockHash());
  }

  if (m_block_type != CConsts::BLOCK_TYPES::Genesis)
  {
    if (!CMachine::isInSyncProcess())
    {
      CGUI::signalUpdateDAGLeaves();
      CGUI::signalUpdateMissedBlocks();
    }
  }

  return true;
}



bool Block::createDocuments(const QJsonValue& documents)
{
  QJsonArray docs = documents.toArray();
  for(CDocIndexT doc_inx = 0; doc_inx < static_cast<CDocIndexT>(docs.size()); doc_inx++)
  {
    Document* d = DocumentFactory::create(docs[doc_inx].toObject(), this, doc_inx);
    m_documents.push_back(d);
  }
  return true;
}

/**
 * @brief Block::getBlockExtInfo
 * @param blockHash
 * @return <status, extInfoExist?, extinfoJsonObj>
 */
std::tuple<bool, bool, QJsonArray> Block::getBlockExtInfo(const QString& block_hash)
{
  QJsonArray block_ext_info;
  QueryRes res = DbModel::select(
    stbl_block_extinfos,
    {"x_block_hash", "x_detail"},
    {{"x_block_hash", block_hash}});
  if (res.records.size() != 1)
  {
    CLog::log("get Block Ext Infos: the block(" + CUtils::hash8c(block_hash) + ") has not ext Info", "app", "trace");
    return {true, false, {}};
  }

  QString serialized_block = BlockUtils::unwrapSafeContentForDB(res.records[0].value("x_detail").toString()).content;
  block_ext_info = CUtils::parseToJsonArr(serialized_block);
  return {true, true, block_ext_info};
}

std::tuple<bool, bool, QJsonArray> Block::getBlockExtInfo() const
{
  if (m_block_ext_info.size() > 0)
    return {true, true, m_block_ext_info[0].toArray()};

  return Block::getBlockExtInfo(m_block_hash);
}

bool Block::appendToDocuments(Document* doc)
{
  m_documents.push_back(doc);
  return true;
}

bool Block::appendToExtInfo(const QJsonArray& an_ext_info)
{
  m_block_ext_info.push_back(an_ext_info);
  return true;
}

QJsonArray Block::getBlockExtInfoByDocIndex(const CDocIndexT& document_index) const
{
  return m_block_ext_info[document_index].toArray();
}

QVDRecordsT Block::searchInBlockExtInfo(
  const ClausesT& clauses,
  const QStringList fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_block_extinfos,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}

/**
 *
 * @param {*} args
 * do a groupping and some general validations on entire documents of a Block
 * TODO: maybe enhance it to use memory buffer
 */

TransientBlockInfo Block::groupDocsOfBlock(const QString& stage) const
{
  TransientBlockInfo transient_block_info {false, this, stage};


  if (m_documents.size() == 0)
    return transient_block_info;

  QString now_ = CUtils::getNow();
  for (CDocIndexT doc_inx = 0; doc_inx < m_documents.size(); doc_inx++)
  {
    Document *a_doc = m_documents[doc_inx];
    transient_block_info.m_doc_by_hash[a_doc->getDocHash()] = a_doc;

    if ((a_doc->m_doc_creation_date > m_block_creation_date) || (a_doc->m_doc_creation_date > now_))
    {
      CLog::log("Block has document with creationdate after block-creation Date! stage(" + stage + "), block(" + CUtils::hash8c(m_block_hash) + ") Doc(" + CUtils::hash8c(a_doc->getDocHash()) + ")", "app", "error");
      return transient_block_info;
    }
    transient_block_info.m_doc_index_by_hash[a_doc->getDocHash()] = doc_inx;

    if (!CUtils::isValidVersionNumber(a_doc->m_doc_version))
    {
      CLog::log("invalid dVer in group Docs Of Block stage(" + stage + ") for doc(" + CUtils::hash8c(a_doc->getDocHash()) + ")", "sec", "error");
      return transient_block_info;
    }


    // document length control
    Document *tmp_doc = DocumentFactory::create(a_doc->exportDocToJson(), this, doc_inx);

    DocLenT recalculate_doc_length = static_cast<DocLenT>(tmp_doc->calcDocLength());
    if ((tmp_doc->m_doc_type != CConsts::DOC_TYPES::DPCostPay) &&
        ((tmp_doc->m_doc_length != recalculate_doc_length) ||
         (tmp_doc->m_doc_length != a_doc->m_doc_length))
        )
    {
      QString msg = "The doc stated dLen is not same as real length. stage(" + stage + ") doc type(" + tmp_doc->m_doc_type + "/" + CUtils::hash8c(tmp_doc->getDocHash()) + ") stated dLen(" + QString::number(a_doc->m_doc_length) + "), ";
      msg += " real length(" + QString::number(tmp_doc->calcDocLength()) + ")";
      CLog::log(msg, "sec", "error");

      delete tmp_doc;

      return transient_block_info;
    }
    delete tmp_doc;


    if (!transient_block_info.m_groupped_documents.keys().contains(a_doc->m_doc_type))
      transient_block_info.m_groupped_documents[a_doc->m_doc_type] = {};

    transient_block_info.m_groupped_documents[a_doc->m_doc_type].push_back(a_doc);

    if (a_doc->getRef() != "")
    {
      if (Document::canBeACostPayerDoc(a_doc->m_doc_type))
      {
        transient_block_info.m_transactions_dict[a_doc->getDocHash()] = a_doc;
        transient_block_info.m_map_trx_hash_to_trx_ref[a_doc->getDocHash()] = a_doc->getRef();
        transient_block_info.m_map_trx_ref_to_trx_hash[a_doc->getRef()] = a_doc->getDocHash();
      } else {
        transient_block_info.m_map_referencer_to_referenced[a_doc->getDocHash()] = a_doc->getRef();
        transient_block_info.m_map_referenced_to_referencer[a_doc->getRef()] = a_doc->getDocHash();
      }
    }
  }

  QStringList payedRefs1 = transient_block_info.m_map_trx_ref_to_trx_hash.keys();
  QStringList payedRefs2;
  for(QString key: transient_block_info.m_map_trx_hash_to_trx_ref.keys())
    payedRefs2.append(transient_block_info.m_map_trx_hash_to_trx_ref[key]);

  for (Document* a_doc: m_documents)
  {
    if (!Document::isNoNeedCostPayerDoc(a_doc->m_doc_type))
    {
      // there must be a transaction to pay for this document
      if (!payedRefs1.contains(a_doc->getDocHash()) || !payedRefs2.contains(a_doc->getDocHash()))
      {
        if ((a_doc->m_doc_type == CConsts::DOC_TYPES::FPost) && (a_doc->m_doc_class == CConsts::FPOST_CLASSES::DMS_Post))
        {
          if ((getNonce() == "") || (m_block_creation_date > "2021-01-01 00:00:00"))
          {
            CLog::log("The document DMS_Post has not Nonce & not payed by no transaction. stage(" + stage + ") document(" + CUtils::hash8c(a_doc->getDocHash()) + ") ", "sec", "error");
            return transient_block_info;
          }
        } else {
          CLog::log("The document is not payed by no transaction. stage(" + stage + ") document(" + CUtils::hash8c(a_doc->getDocHash()) + ") ", "sec", "error");
          return transient_block_info;
        }
      }
    }
  }

  if (transient_block_info.m_map_trx_ref_to_trx_hash.keys().size() != transient_block_info.m_map_trx_hash_to_trx_ref.keys().size())
  {
    CLog::log("transaction count and ref count are different! stage(" + stage + ") mapTrxRefToTrxHash: " + CUtils::dumpIt(transient_block_info.m_map_trx_ref_to_trx_hash) + " mapTrxHashToTrxRef: " + CUtils::dumpIt(transient_block_info.m_map_trx_hash_to_trx_ref) + " ", "sec", "error");
    return transient_block_info;
  }

  for (QString a_ref: transient_block_info.m_map_trx_ref_to_trx_hash.keys())
  {
    if (!transient_block_info.m_transactions_dict.keys().contains(transient_block_info.m_map_trx_ref_to_trx_hash[a_ref]))
    {
      CLog::log("missed some1 transaction to support referenced documents. stage(" + stage + ") trxDict: " + CUtils::dumpIt(transient_block_info.m_transactions_dict) + " mapTrxRefToTrxHash: " + CUtils::dumpIt(transient_block_info.m_map_trx_ref_to_trx_hash) + " ", "sec", "error");
      return transient_block_info;
    }
  }
  if (CUtils::arrayDiff (transient_block_info.m_map_trx_hash_to_trx_ref.keys(), transient_block_info.m_transactions_dict.keys()).size() != 0)
  {
    CLog::log("missed some transaction, to support referenced documents. stage(" + stage + ") trxDict: " + CUtils::dumpIt(transient_block_info.m_transactions_dict) + " mapTrxRefToTrxHash: " + CUtils::dumpIt(transient_block_info.m_map_trx_ref_to_trx_hash) + " ", "sec", "error");
    return transient_block_info;
  }
  for (QString a_ref: transient_block_info.m_map_trx_ref_to_trx_hash.keys())
  {
    if (!transient_block_info.m_doc_by_hash.keys().contains(a_ref))
    {
      CLog::log("missed a referenced document. stage(" + stage + ") referenced doc(" + CUtils::hash8c(a_ref) + "), referencer doc(" + CUtils::hash8c(transient_block_info.m_map_trx_ref_to_trx_hash[a_ref]) + ") ", "sec", "error");
      return transient_block_info;
    }
  }

  if (static_cast<uint32_t>(transient_block_info.m_doc_index_by_hash.keys().size()) != static_cast<uint32_t>(m_documents.size()))
  {
    CLog::log("There is duplicated doc.hash in block. stage(" + stage + ") block(" + CUtils::hash8c(m_block_hash) + ") ", "sec", "error");
    return transient_block_info;
  }

  QStringList doc_types = transient_block_info.m_groupped_documents.keys();
  doc_types.sort();
  for(QString a_type: doc_types)
    CLog::log("block(" + CUtils::hash8c(m_block_hash) + ") has " + QString::number(transient_block_info.m_groupped_documents[a_type].size()) + " Document(s) of type(" + a_type + ") ", "app", "trace");

  transient_block_info.m_status = true;
  return transient_block_info;

//  {
//    true,
//    trxDict,
//    docByHash,
//    grpdDocuments,
//    docIndexByHash,
//    mapTrxHashToTrxRef,
//    mapTrxRefToTrxHash,
//    mapReferencedToReferencer,
//    mapReferencerToReferenced
//  };
}


QString Block::getNonce() const
{
  CUtils::exiter("m_nonce isn't implement for Block Base class", 0);
  return "";
}

std::vector<Document *> Block::getDocuments() const
{
  return m_documents;
}

//std::tuple<bool, QJsonObject> Block::selectBExtInfosFromDB(const QString& block_hash)
//{
//  QueryRes bExtInfo = DbModel::select(
//    "db_comen_blocks",
//    stbl_block_extinfos,
//    QStringList {"x_block_hash", "x_detail"},     // fields
//    {ModelClause("x_block_hash", block_hash)},
//    {},
//    1   // limit
//  );
//  if (bExtInfo.records.size() == 0)
//    return {false, QJsonObject {}};

//  QVariant x_detail = bExtInfo.records[0].value("x_detail");
//  QString serializedBextInfo = x_detail.toString();
//  auto[unwrap_status, unwrap_ver, unwrap_content] = BlockUtils::unwrapSafeContentForDB(serializedBextInfo);

//  QJsonObject JsonObj = CUtils::parseToJsonObj(unwrap_content);

//  return {true, JsonObj};

//}

//std::tuple<bool, QJsonObject> Block::selectBExtInfosFromDB() const
//{
//  return selectBExtInfosFromDB(m_block_hash);
//}

bool Block::insertBlockExtInfoToDB(
  const QString& serializedBextInfo,
  const CBlockHashT& block_hash,
  const CDateT& creation_date)
{

  QVDicT values {
    {"x_block_hash", block_hash},
    {"x_detail", BlockUtils::wrapSafeContentForDB(serializedBextInfo).content},
    {"x_creation_date", creation_date}};

  CLog::log("--- recording bExtInfo in DAG Block(" + CUtils::hash8c(block_hash)+")");
  return DbModel::insert(
    stbl_block_extinfos,     // table
    values, // values to insert
    true);
}

/**
 * picks a doc from a block which is complatly loaded in memory
 * @param {*} block
 * @param {*} doc_hash
 * return {doc_index, doc_pointer}
 *
 */
std::tuple<int64_t, Document*> Block::getDocumentByHash(const CDocHashT& doc_hash) const
{
  auto documents = getDocuments();
  for (CDocIndexT doc_inx = 0; doc_inx < documents.size(); doc_inx++)
  {
    if (documents[doc_inx]->m_doc_hash == doc_hash)
      return {doc_inx, documents[doc_inx]};
  }
  return {-1, nullptr};
}

std::tuple<int64_t, QJsonObject> Block::getDocumentJByHash(
  const QJsonObject& block,
  const CDocHashT& doc_hash)
{
  QJsonArray documents = block.value("docs").toArray();
  for (CDocIndexT doc_inx = 0; doc_inx < documents.size(); doc_inx++)
  {
    if (documents[doc_inx].toObject().value("dHash").toString() == doc_hash)
      return {doc_inx, documents[doc_inx].toObject()};
  }
  return {-1, {}};
}

/**
*
* @param {string} blockHash
* static retrieves complete version of recorded block in 2 different tables i_blocks & i_block_segwits
*/
std::tuple<bool, QJsonObject> Block::regenerateBlock(const CBlockHashT& block_hash)
{
  QJsonObject Jblock {};
  //listener.doCallSync('SPSH_before_regenerate_block', { block_hash });
  QVDRecordsT block_records = DAG::searchInDAG({{"b_hash", block_hash}});
  if (block_records.size() == 0)
  {
    // TODO: the block is valid and does not exist in local. or
    // invalid block invoked, maybe some penal for sender!
    CLog::log("The requested block to regenrate doesn't exist in DAG! Block("+ CUtils::hash8c(block_hash) + ") ", "app", "warning");
    return {false, Jblock};
  }

  try {
    QVDicT the_block = block_records[0];
    Jblock = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(the_block.value("b_body").toString()).content);

    auto[status, extExist, block_ext_info] = getBlockExtInfo(block_hash);
    if (extExist)
      Jblock["bExtInfo"] = block_ext_info;

    return {true, Jblock};

  } catch (std::exception) {
    return {false, Jblock};
  }

}
