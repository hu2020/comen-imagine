#ifndef COINBASEUTXOHANDLER_H
#define COINBASEUTXOHANDLER_H


class CoinbaseUTXOHandler
{

public:
  CoinbaseUTXOHandler();
  static void loopImportCoinbaseUTXOs();
  static void importCoinbasedUTXOs(const QString &cDate);
  static QString calcCoinbasedOutputMaturationDate(QString cDate = "");

};

#endif // COINBASEUTXOHANDLER_H
