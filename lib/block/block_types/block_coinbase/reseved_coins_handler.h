#ifndef RESEVEDCOINSHANDLER_H
#define RESEVEDCOINSHANDLER_H


class ResevedCoinsHandler
{
public:
  ResevedCoinsHandler();

  static const QString stbl_released_reserves;
  static const QString stbl_requests_for_release_reserved_coins;
  static const QString stbl_pollings;


  static bool removeReqRelRes(const CDocHashT& req_for_rel_hash);
};

#endif // RESEVEDCOINSHANDLER_H
