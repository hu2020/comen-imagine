#ifndef COINBASEISSUER_H
#define COINBASEISSUER_H


struct TmpHolder {
  QString holder = "";
  CMPAIValueT dividend = 0;
};


class MonthCoinsReport
{
public:
  CMPAIValueT m_one_cycle_max_coins = 0;
  CMPAIValueT m_one_cycle_issued = 0;
  CMPAIValueT m_one_cycle_income = 0;
  CMPAIValueT m_income_per_month = 0;
  DNAShareCountT m_sum_shares = 0;
  QString m_due = "";
};

class FutureIncomes
{
public:
  CMPAIValueT m_definite_incomes = 0;
  CMPAIValueT m_reserve_incomes = 0;
  std::vector<MonthCoinsReport> m_monthly_incomes;
  CDateT m_first_income_date;
  CDateT m_last_income_date;
};


class CoinbaseIssuer
{
public:
  CoinbaseIssuer();
  static QJsonObject getCoinbaseTemplateObject();

  static QJsonObject createCBCore(
    QString cycle,
    const QString& mode = CConsts::STAGES::Creating,
    const QString& version = "0.0.0");

  static std::tuple<bool, QJsonObject> doGenerateCoinbaseBlock(
    const QString& cycle = CUtils::getCoinbaseCycleStamp(),
    const QString& mode = CConsts::STAGES::Creating,
    const QString& version = "0.0.0");

  static uint8_t calculateReleasableCoinsBasedOnContributesVolume(const uint64_t& sumShares);

  static CMPAIValueT calcPotentialMicroPaiPerOneCycle(QString year_);

  static std::tuple<CMPAIValueT, CMPAIValueT, DNAShareCountT, QHash<QString, DNAShareCountT> > calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore(
    const CDateT& cDate = CUtils::getNow());

  static std::tuple<CMPAIValueT, CMPAIValueT, DNAShareCountT> predictReleaseableMicroPAIsPerOneCycle(
    const uint32_t& annualContributeGrowthRate,
    const DNAShareCountT& currentTotalSahres,
    const QString& prevDue,
    CDateT due = CUtils::getNow(),
    CDateT cCDate = CUtils::getNow());

  static FutureIncomes predictFutureIncomes(
    const uint32_t& theContribute,  // contributeHours * contributeLevel
    CDateT cCDate = "", // contribute creation date (supposing aproved date of proposal)
    uint32_t months = 7 * 12,
    DNAShareCountT currentTotalSahres = 0,
    uint32_t annualContributeGrowthRate = 100);

  static void loopMaybeIssueACoinbaseBlock();

  static bool doesDAGHasMoreConfidenceCB();

  static std::tuple<QString, QString, QString, QSDicT> makeEmailHashDict();

  static bool haveIFirstHashedEmail(const QString order = "asc");

  static bool controlCoinbaseIssuanceCriteria();

  static bool passedCertainTimeOfCycleToRecordInDAG(const CDateT& cDate = CUtils::getNow());

  static void maybeCreateCoinbaseBlock();

  static void tryCreateCoinbaseBlock();

};

#endif // COINBASEISSUER_H
