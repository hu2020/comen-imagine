#include "stable.h"

#include "reseved_coins_handler.h"


const QString ResevedCoinsHandler::stbl_released_reserves = "c_released_reserves";
const QString ResevedCoinsHandler::stbl_requests_for_release_reserved_coins = "c_requests_for_release_reserved_coins";
const QString ResevedCoinsHandler::stbl_pollings = "c_pollings";

ResevedCoinsHandler::ResevedCoinsHandler()
{

}


bool ResevedCoinsHandler::removeReqRelRes(const CDocHashT& req_for_rel_hash)
{
  DbModel::dDelete(
    stbl_requests_for_release_reserved_coins,
    {{"rlq_hash", req_for_rel_hash}});

  return true;
}



//// start from preparingAReserveReleasePolling


//let reqForRelResDoc = {
//    hash: "0000000000000000000000000000000000000000000000000000000000000000",
//    dType: iConsts.DOC_TYPES.ReqForRelRes,
//    dClass: iConsts.REQ_FOR_REL_RES_CLASSES.Basic,
//    dLen: "0000000",
//    dVer: "0.0.0",
//    dComment: "",
//    eyeBlock: "0000000000000000000000000000000000000000000000000000000000000000",    // the original coinbase black which is requested to be released
//    reserveNumber: "",  // the reserve number (1,2,3 or 4)
//    creationDate: "",
//    creator: "",    // the bech32 address of shareholder
//    dExtInfo: {},
//    dExtHash: "0000000000000000000000000000000000000000000000000000000000000000"
//};

//let rlDocTpl = {
//    hash: "0000000000000000000000000000000000000000000000000000000000000000",
//    dType: iConsts.DOC_TYPES.RlDoc,
//    refPolling: "0000000000000000000000000000000000000000000000000000000000000000",
//    refBlock: "0000000000000000000000000000000000000000000000000000000000000000",
//    refCycle: "", // 'yyyy-mm-dd am' / 'yyyy-mm-dd pm'
//    reserveNumber: "",
//    mintedCoins: 0,
//    outputs: []
//};

//let rlBlockTpl = {
//    net: "im",
//    bVer: "0.0.0",
//    bType: iConsts.BLOCK_TYPES.RlBlock,
//    blockLength: "0000000", // seialized block size by byte (char). this number is also a part of block root hash
//    blockHash: null,
//    ancestors: [],
//    signals: [], // e.g. ["mimblewimble", "taproot"]
//    creationDate: "", // 'yyyy-mm-dd hh:mm:ss'
//    docsRootHash: "", //the hash root of merkle tree of transactions
//    docs: []
//};

//class ResevedCoinsHandler {

//    static customizedValidationZZZ(args) {
//        let msg;
//        let polling = args.polling;
//        let dExtInfo = args.dExtInfo;
//        let block = args.block;
//        let docByHash = args.docByHash;
//        let costPayerTrx = args.costPayerTrx;
//        let stage = args.stage;
//        let docIndexByHash = args.docIndexByHash;
//        if (!_.has(docByHash, polling.ref)) {
//            msg = `THe polling referenced doc(${utils.hash6c(polling.ref)}) does not exist in block`;
//            clog.sec.error(msg);
//            return { err: true, msg, shouldPurgeMessage: true }
//        }
//        let reqRelDoc = _.clone(docByHash[polling.ref]);

//        // TODO: maybe some validation on reqRelDoc document!
//        let reserveNumber = parseInt(reqRelDoc.reserveNumber); // it could be 1,2,3 or 4
//        if ((reserveNumber < 0) || (reserveNumber > 4))
//            return { err: true, msg: `Invalide reserve number(${reserveNumber})` };


//        let shareHoldersCount = relResDtl.shareHolders.length;
//        // let isResRel = ResevedCoinsHandler.is ReserveReleasable({ blockHash: reqRelDoc.eyeBlock, reserveNumber });
//        // if (!isResRel.isReleasable)
//        //     return isResRel;


//        // let reqRelCost = ResevedCoinsHandler.calcReqRelCost({
//        //     stage,
//        //     reqRelDoc,
//        //     pollingDoc: polling,
//        //     voters: shareHoldersCount
//        // });
//        // if (reqRelCost.err != false)
//        //     return reqRelCost;

//        let costPayerTrxOutputs = {}
//        for (let anOutput of costPayerTrx.outputs) {
//            costPayerTrxOutputs[anOutput[0]] = anOutput[1];
//        }
//        // control if the trx of doc cost payment is valid
//        if (costPayerTrx.ref != polling.hash) {
//            msg = `block(${utils.hash6c(blockHash)}) The polingg(${utils.hash6c(polling.hash)}) cost is not payed by trx(${utils.hash6c(costPayerTrx.hash)})!`;
//            clog.sec.error(msg);
//            return { err: true, msg, shouldPurgeMessage: true }
//        }
//        if (!_.has(costPayerTrxOutputs, 'TP_POLLING') ||
//            (costPayerTrxOutputs['TP_POLLING'] != reqRelCost.cost)) {
//            msg = `the polling(3 docs) costs(${costPayerTrxOutputs['TP_POLLING']}) referenced Trx(${polling.trx}) for poling(${utils.hash6c(polling.ref)}), has different amount(${reqRelCost.cost})!`;
//            clog.sec.error(msg);
//            return { err: true, msg, shouldPurgeMessage: true }
//        }




//        // controll reqRelDoc signatures
//        let unlockSet, isValidUnlock, signMsg;
//        unlockSet = dExtInfo.uSet;
//        isValidUnlock = mOfNHandler.validateSigStruct({
//            address: reqRelDoc.creator,
//            uSet: unlockSet
//        });
//        if (isValidUnlock != true) {
//            msg = `polling reqRelDoc can Invalid! given creator unlock structure for (${utils.hash6c(reqRelDoc.hash)}): ${utils.stringify({
//                address: reqRelDoc.creator,
//                sSets: unlockSet.sSets,
//                lHash: unlockSet.lHash,
//                proofs: unlockSet.proofs,
//                salt: unlockSet.salt
//            })}`;
//            clog.sec.error(msg);
//            return { err: true, msg }
//        }
//        // reqRelDoc signature & permission validate check
//        signMsg = ResevedCoinsHandler.getSignMsgDReqRel(reqRelDoc);
//        for (let signInx = 0; signInx < dExtInfo.signatures.length; signInx++) {
//            let aSignature = dExtInfo.signatures[signInx];
//            try {
//                let verifyRes = crypto.verifySignature(signMsg, aSignature, unlockSet.sSets[signInx].sKey);
//                if (!verifyRes) {
//                    msg = `The reqRelDoc, (${utils.hash6c(reqRelDoc.hash)}) has invalid creator signature`;
//                    return { err: true, msg }
//                }
//            } catch (e) {
//                msg = `error in verify reqRelDoc Signature (${utils.hash6c(reqRelDoc.hash)}): ${utils.stringify(args)}`;
//                clog.trx.error(msg);
//                clog.trx.error(e);
//                return { err: true, msg };
//            }
//        }

//        return { err: false }
//    }

//    /**
//     *
//     * @param {*} args
//     * given blockHash, returns releasable details
//     *
//     */
//    static getReservesDetails(args) {
//        if (dagHandler == null)
//            dagHandler = require('../graph-handler/dag-handler');

//        let blockHashes = _.has(args, 'blockHashes') ? args.blockHashes : null;
//        let nowT = utils.getNow();

//        let query = [['b_type', iConsts.BLOCK_TYPES.Coinbase]];
//        if (!utils._nilEmptyFalse(blockHashes))
//            query.push(['b_hash', ['IN', blockHashes]]);

//        let wBlocks = dagHandler.searchInDAGSync({
//            fields: ['b_cycle', 'b_hash', 'b_body'],
//            query,
//            order: [['b_creation_date', 'ASC']]
//        });
//        if (wBlocks.length == 0)
//            return { err: true, msg: `block(${blockHashes.map(x => utils.hash6c(x))}) not exist in DAG!` }


//        // control if already not released! TODO: FIXIT to insert ALL coinbases(with different block hash but same cycle) to this table
//        let eyeBlocks = wBlocks.map(x => x.bHash);
//        console.log('eyeBlocks', eyeBlocks);
//        // todo: instead use i_requests_for_release_reserved_coins.rlq_released talbe
//        let releasedBlocks = model.sRead({
//            table,
//            query: [['rb_eye_block', ['IN', eyeBlocks]]]
//        });
//        let rBlockDict = {};
//        for (let rb of releasedBlocks) {
//            if (!_.has(rBlockDict, rb.rb_eye_block))
//                rBlockDict[rb.rb_eye_block] = {};
//            rBlockDict[rb.rb_eye_block][rb.rb_reserve_number] = rb;
//        }

//        let reserves = {};
//        let coinbases = [];
//        let orderedBlockHashes = [];
//        let consideredBlocks = [];
//        for (let wBlock of wBlocks) {
//            if (consideredBlocks.includes(wBlock.bCycle))
//                continue;
//            consideredBlocks.push(wBlock.bCycle);

//            let coinbase = blockUtils.openDBSafeObject(wBlock.bBody).content;
//            coinbases.push(coinbase);
//            orderedBlockHashes.push(coinbase.blockHash);

//            reserves[coinbase.blockHash] = {
//                eyeCreationDate: coinbase.creationDate,
//                releasableCoins: coinbase.docs[0].mintedCoins,
//                shareHolders: coinbase.docs[0].outputs,
//                reservesInfo: {}
//            };

//            for (let reserveNumber = 1; reserveNumber < 5; reserveNumber++) {
//                reserves[coinbase.blockHash].reservesInfo[reserveNumber] = {};
//                let releasableDate;
//                if (iConsts.TIME_GAIN == 1) {
//                    releasableDate = utils.yearsAfter(iConsts.MAP_RESERVE_NUMBER_TO_TIME_LOCK[reserveNumber], coinbase.creationDate)
//                } else {
//                    // if it is in test environment
//                    releasableDate = utils.minutesAfter(iConsts.MAP_RESERVE_NUMBER_TO_TIME_LOCK[reserveNumber] * iConsts.getCycleByMinutes(), coinbase.creationDate)
//                }

//                let isReleased = iConsts.CONSTS.NO;
//                let releasedDate = '-';
//                if (_.has(rBlockDict, coinbase.blockHash) && _.has(rBlockDict[coinbase.blockHash], reserveNumber)) {
//                    isReleased = iConsts.CONSTS.YES;
//                    releasedDate = rBlockDict[coinbase.blockHash][reserveNumber].rb_release_date
//                }
//                let neededVotes = 100 - (reserveNumber * 20);
//                if (isReleased == iConsts.CONSTS.YES) {
//                    // it already released
//                    reserves[coinbase.blockHash].reservesInfo[reserveNumber] = {
//                        isReleasable: iConsts.CONSTS.NO,
//                        isReleased,
//                        releasableDate,
//                        releasedDate,
//                        neededVotes,
//                        msg: `The Reserve(${reserveNumber}) of Block(${utils.hash6c(coinbase.blockHash)}) already released on(${releasedDate}})`,
//                    }

//                } else {
//                    let isReleasable = (releasableDate < nowT) ? iConsts.CONSTS.YES : iConsts.CONSTS.NO;
//                    reserves[coinbase.blockHash].reservesInfo[reserveNumber] = {
//                        isReleasable,
//                        isReleased,
//                        releasedDate,
//                        releasableDate,
//                        neededVotes,
//                        msg: `The Reserve(${reserveNumber}) of Block(${utils.hash6c(coinbase.blockHash)}) will not releasable before(${releasableDate}})`,
//                    }

//                }
//            }
//        }
//        return { err: false, coinbases, reserves, orderedBlockHashes };
//    }


//    static calcPotentialVotersCount(eyeBlock, reserveNumber) {
//        let resInfo = ResevedCoinsHandler.getReservesDetails({ blockHashes: [eyeBlock] });
//        console.log(resInfo);
//        let reserves = resInfo.reserves;

//        if (!_.has(reserves, eyeBlock) || !_.has(reserves[eyeBlock].reservesInfo, reserveNumber))
//            return { err: true, msg: `req ForRelRes: The eye block(${utils.hash6c(eyeBlock)}) reserve number(${reserveNumber}) combination does not exist3!` }

//        if (reserves[eyeBlock].reservesInfo[reserveNumber].isReleasable != iConsts.CONSTS.YES)
//            return reserves;

//        let votersCount = reserves[eyeBlock].shareHolders.length;
//        return { err: false, votersCount };
//    }

//    /**
//     * request for release reserved coins
//     * @param {*} args
//     */
//    static async makeReqForRelRes(args) {
//        let blockHash = args.blockHash;
//        let reserveNumber = parseInt(args.reserveNumber); // it could be 1,2,3 or 4
//        if ((reserveNumber < 0) || (reserveNumber > 4))
//            return { err: true, msg: `Invalide reserve number(${reserveNumber})` };

//        if (utils._nilEmptyFalse(blockHash) || utils._nilEmptyFalse(reserveNumber))
//            return { err: true, msg: `Failed req ForRelRes, because invalid request ${utils.stringify(args)}` };

//        let resInfo = ResevedCoinsHandler.getReservesDetails({ blockHashes: [blockHash] });
//        console.log(resInfo);
//        let reserves = resInfo.reserves;

//        if (!_.has(reserves, blockHash) || !_.has(reserves[blockHash].reservesInfo, reserveNumber))
//            return { err: true, msg: `req ForRelRes: The eye block(${utils.hash6c(blockHash)}) reserve number(${reserveNumber}) combination does not exist3!` }
//        if (reserves[blockHash].reservesInfo[reserveNumber].isReleasable != iConsts.CONSTS.YES)
//            return reserves;
//        let votersInfo = ResevedCoinsHandler.calcPotentialVotersCount(blockHash, reserveNumber);
//        if (votersInfo.err != false)
//            return votersInfo;
//        let votersCount = votersInfo.votersCount;

//        let pollingDType = _.has(args, 'dType') ? args.dType : iConsts.DOC_TYPES.Polling;
//        let pollingDClass = _.has(args, 'dClass') ? args.dClass : iConsts.POLLING_PROFILE_CLASSES.Basic.ppName;
//        let timeframe = _.has(args, 'timeframe') ? args.timeframe : iConsts.getMinVotingTimeframe();
//        let dComment = _.has(args, 'dComment') ? args.dComment : `Request For Release Reserved Coins block(${utils.hash16c(blockHash)}) reserve number(${reserveNumber})`;
//        let creator = _.has(args, 'creator') ? args.creator : machine.getMProfileSettingsSync().backerAddress;
//        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

//        // 1. create a req-rel-res-doc for block release
//        let reqRelDoc = _.clone(reqForRelResDoc);
//        reqRelDoc.dType = iConsts.DOC_TYPES.ReqForRelRes;
//        reqRelDoc.dClass = iConsts.REQ_FOR_REL_RES_CLASSES.Basic;
//        reqRelDoc.eyeBlock = blockHash;
//        reqRelDoc.reserveNumber = reserveNumber;
//        reqRelDoc.creationDate = utils.getNow();
//        reqRelDoc.dComment = dComment;
//        reqRelDoc.creator = creator;

//        let signMsg = ResevedCoinsHandler.getSignMsgDReqRel(reqRelDoc);
//        clog.app.info(`going to sign SignMsgD ReqRel ${signMsg}`);
//        const walletAddressHandler = require('../../web-interface/wallet/wallet-address-handler');
//        let signatureInfo = walletAddressHandler.signByAnAddress({ signMsg, signerAddress: creator });
//        if (signatureInfo.err != false)
//            return signatureInfo;
//        reqRelDoc.dExtInfo = {
//            signatures: signatureInfo.signatures,
//            uSet: signatureInfo.uSet
//        }
//        reqRelDoc.dExtHash = iutils.doHashObject({
//            signatures: reqRelDoc.dExtInfo.signatures,
//            uSet: reqRelDoc.dExtInfo.uSet
//        });

//        reqRelDoc.dLen = iutils.paddingDocLength(utils.stringify(reqRelDoc).length);
//        reqRelDoc.hash = ResevedCoinsHandler.calcHashDReqRelDoc(reqRelDoc);


//        // 2. create a release-polling for block release
//        let pollingDoc = pollHandler.prepareNewPolling({
//            dType: pollingDType,
//            dClass: pollingDClass,
//            dVer: '0.0.8',
//            creationDate: reqRelDoc.creationDate,
//            creator: reqRelDoc.creator,
//            dComment: reqRelDoc.dComment,

//            ref: reqRelDoc.hash,
//            refType: reqRelDoc.dType,
//            refClass: reqRelDoc.dClass,

//            timeframe: timeframe
//        });


//        // 3. create trx to pay ReqForRelRes doc cost
//        let reqRelCost = ResevedCoinsHandler.calcReqRelCost({
//            cDate: utils.getNow(),
//            stage: iConsts.STAGES.Creating,
//            reqRelDoc
//        });
//        if (reqRelCost.err != false)
//            return reqRelCost;

//        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
//        let outputs = [
//            [changeAddress, 1],  // a new address for change back
//            ['TP_REQRELRES', reqRelCost.cost]
//        ];
//        let spendables = walletHandler.coinPicker.getSomeCoins({
//            selectionMethod: 'precise',
//            minimumSpendable: utils.floor(reqRelCost.cost * 1.3) // an small portion bigger to support DPCosts
//        });
//        console.log('spendables: ', spendables);
//        if (spendables.err != false)
//            return spendables;

//        if (!_.has(spendables, 'selectedCoins') || (utils.objKeys(spendables.selectedCoins).length == 0)) {
//            msg = `Wallet couldn't find! proper UTXOs to spend in reqResRel`;
//            clog.app.info(msg);
//            return { err: true, msg };
//        }
//        let inputs = spendables.selectedCoins;
//        let trxNeededArgs = {
//            maxDPCost: utils.floor(reqRelCost.cost * 2),
//            DPCostChangeIndex: 0, // to change back
//            dType: iConsts.DOC_TYPES.BasicTx,
//            dClass: iConsts.TRX_CLASSES.SimpleTx,
//            ref: reqRelDoc.hash,
//            description: 'Pay for ReqForRelRes (doc)',
//            inputs,
//            outputs,
//        }
//        let reqRelDocTrxDtl = walletHandler.makeATransaction(trxNeededArgs);
//        if (reqRelDocTrxDtl.err != false)
//            return reqRelDocTrxDtl;
//        console.log('signed req rel res cost trx: ', utils.stringify(reqRelDocTrxDtl));


//        // 4. create trx to pay polling costs
//        let pollingCost = pollHandler.calcPollingCost({
//            cDate: utils.getNow(),
//            stage: iConsts.STAGES.Creating,
//            polling: pollingDoc,
//            voters: votersCount
//        });
//        if (pollingCost.err != false)
//            return pollingCost;

//        // create a transaction for payment
//        changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
//        outputs = [
//            [changeAddress, 1],  // a new address for change back
//            ['TP_POLLING', pollingCost.cost]
//        ];
//        spendables = walletHandler.coinPicker.getSomeCoins({
//            excludeRefLocs: utils.objKeys(inputs),  // avoid double spending inputs
//            selectionMethod: 'precise',
//            minimumSpendable: utils.floor(pollingCost.cost * 1.3) // an small portion bigger to support DPCosts
//        });
//        console.log('spendables: ', spendables);
//        if (spendables.err != false)
//            return spendables;

//        if (!_.has(spendables, 'selectedCoins') || (utils.objKeys(spendables.selectedCoins).length == 0)) {
//            msg = `Wallet couldn't find! proper UTXOs to spend in reqResRel`;
//            clog.app.info(msg);
//            return { err: true, msg };
//        }
//        inputs = spendables.selectedCoins;
//        trxNeededArgs = {
//            maxDPCost: utils.floor(pollingCost.cost * .3),
//            DPCostChangeIndex: 0, // to change back
//            dType: iConsts.DOC_TYPES.BasicTx,
//            dClass: iConsts.TRX_CLASSES.SimpleTx,
//            ref: pollingDoc.hash,
//            description: 'Pay for ReqForRelRes (Polling)',
//            inputs,
//            outputs,
//        }
//        let pollingPayTrxDtl = walletHandler.makeATransaction(trxNeededArgs);
//        if (pollingPayTrxDtl.err != false)
//            return pollingPayTrxDtl;
//        console.log('signed req rel res cost trx: ', utils.stringify(pollingPayTrxDtl));


//        // push in buffer
//        let res = await docBufferHandler.pushInAsync({ doc: reqRelDoc, DPCost: reqRelCost.cost });
//        if (res.err != false)
//            return res;
//        res = await docBufferHandler.pushInAsync({ doc: pollingDoc, DPCost: reqRelCost.cost });
//        if (res.err != false)
//            return res;
//        res = await docBufferHandler.pushInAsync({ doc: reqRelDocTrxDtl.trx, DPCost: reqRelCost.cost });
//        if (res.err != false)
//            return res;
//        res = await docBufferHandler.pushInAsync({ doc: pollingPayTrxDtl.trx, DPCost: pollingCost.cost });
//        if (res.err != false)
//            return res;

//        // mark UTXOs as used in local machine
//        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(reqRelDocTrxDtl.trx);
//        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(pollingPayTrxDtl.trx);

//        if (dTarget == iConsts.CONSTS.TO_BUFFER)
//            return { err: false, msg: `Your RelResReq is pushed to bBlock uffer` };

//        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
//        let r = wBroadcastBlock.broadcastBlock();
//        return r;

//    }

//    static calcReqRelCost(args) {
//        let reqRelDoc = args.reqRelDoc;
//        let stage = args.stage;

//        let dLen = parseInt(reqRelDoc.dLen);

//        let theCost =
//            dLen *
//            cnfHandler.getBasePricePerChar({ cDate }) *
//            cnfHandler.getDocExpense({ cDate, dType: reqRelDoc.dType, dClass: reqRelDoc.dClass, dLen });

//        if (stage == iConsts.STAGES.Creating)
//            theCost = theCost * machine.getMachineServiceInterests({
//                dType: reqRelDoc.dType,
//                dClass: reqRelDoc.dClass,
//                dLen
//            });

//        return { err: false, cost: Math.floor(theCost) };
//    }


//    static getSignMsgDReqRel(reqRelDoc) {
//        // as always ordering properties by alphabet
//        let signables = {
//            creationDate: reqRelDoc.creationDate,
//            creator: reqRelDoc.creator,
//            dClass: reqRelDoc.dClass,
//            dVer: reqRelDoc.dVer,
//            dComment: reqRelDoc.dComment,
//            dType: reqRelDoc.dType,
//            eyeBlock: reqRelDoc.eyeBlock,
//            reserveNumber: reqRelDoc.reserveNumber,
//        }
//        clog.app.info(`RfRR signables: ${utils.stringify(signables)}`);
//        clog.app.info(`reqRelDoc.reserveNumber type: ${typeof reqRelDoc.reserveNumber}`);
//        let hash = iutils.doHashObject(signables);
//        let signMsg = hash.substring(0, iConsts.SIGN_MSG_LENGTH);
//        return signMsg;
//    }

//    static calcHashDReqRelDoc(reqRelDoc) {
//        // as always ordering properties by alphabet
//        let hashables = {
//            dExtHash: reqRelDoc.dExtHash,
//            dLen: reqRelDoc.dLen
//        }
//        clog.app.info(`RfRR hashables: ${utils.stringify(hashables)}`);
//        let hash = iutils.doHashObject(hashables);
//        return hash;
//    }

//    static calcRlDocHash(rlDoc) {
//        // as always ordering properties by alphabet
//        let hashables = {
//            dClass: rlDoc.dClass,
//            dType: rlDoc.dType,
//            eyeBlock: rlDoc.eyeBlock,
//            reserveNumber: rlDoc.reserveNumber
//        }
//        clog.app.info(`relCoins doc hashables: ${utils.stringify(hashables)}`);
//        let hash = iutils.doHashObject(hashables);
//        return hash;
//    }



//    static getRetRelResDocByHash(reqRelResHash) {
//        if (dagHandler == null)
//            dagHandler = require('../graph-handler/dag-handler');

//        let { blockHashes } = dagHandler.getBlockHashesByDocHashes({ docsHashes: [reqRelResHash] });
//        let wBlocks = dagHandler.searchInDAGSync({
//            query: [['b_hash', ['IN', blockHashes]]],
//            order: [['b_creation_date', 'ASC']]
//        });
//        if (wBlocks.length == 0) {
//            return { err: true, msg: `get RetRelRes Doc By Hash, Invalid reqrelres polling document: ${utils.stringify(polling)}` }
//        }
//        let block = blockUtils.openDBSafeObject(wBlocks[0].bBody).content;
//        let reqRelResDoc = null;
//        if (_.has(block, 'docs') && Array.isArray(block.docs) && (block.docs.length > 0)) {
//            for (let aDoc of block.docs) {
//                if (aDoc.hash == reqRelResHash)
//                    reqRelResDoc = aDoc;
//            }
//        }
//        if (reqRelResDoc == null)
//            return { err: true, msg: `release TheCoins, The reqrelres polling document(${utils.hash6c(reqRelResHash)}) doesn't exist in block(${utils.hash6c(block.blockHash)}) ` };

//        return { err: false, reqRelResDoc };

//    }

//    static recordAReqRelRes(args) {
//        let msg;
//        clog.app.info(`record A New Req for release reserved coins args: ${utils.stringify(args)}`);
//        let block = args.block;
//        let reqRelRes = args.reqRelRes;

//        let dbl = model.sRead({
//            table: tableOnchainReqRelRes,
//            query: [['rlq_hash', reqRelRes.hash]]
//        });
//        if (dbl.length > 0) {
//            msg = `try to double insert existed request for RelRes ${utils.hash6c(reqRelRes.hash)}`;
//            clog.sec.error(msg);
//            return { err: false, msg }
//        }

//        let rlq_hash = reqRelRes.hash;
//        let rlq_eye_block = reqRelRes.dType;
//        let rlq_reserve_number = reqRelRes.reserveNumber;
//        let rlq_requester = reqRelRes.creator;
//        let rlq_comment = reqRelRes.dComment;
//        let rlq_creation_date = block.creationDate;
//        let rlq_conclude_date = '';
//        let rlq_approved = iConsts.CONSTS.NO;
//        let rlq_released = iConsts.CONSTS.NO;

//        let values = {
//            rlq_hash,
//            rlq_eye_block,
//            rlq_reserve_number,
//            rlq_requester,
//            rlq_comment,
//            rlq_creation_date,
//            rlq_conclude_date,
//            rlq_approved,
//            rlq_released
//        };
//        console.log(`new reqRelRes is creates args: ${utils.stringify(values)}`);
//        model.sCreate({
//            table: tableOnchainReqRelRes,
//            values
//        })
//        clog.app.info(`new reqRelRes is creates args: ${utils.stringify(values)}`);
//        return { err: false };
//    }

//    static removeReqRelRes(args) {
//        model.sDelete({
//            table: tableOnchainReqRelRes,
//            query: [['rlq_hash', args.reqRelHash]]
//        });
//        return { err: false }
//    }

//    static doReqRelConcludeTreatment(args) {
//        if (dagHandler == null)
//            dagHandler = require('../graph-handler/dag-handler');

//        console.log(args);
//        return;

//        let polling = args.polling;
//        let reqRelResHash = polling.pllRef;

//        // retrieve reqrelres document
//        let getRes = ResevedCoinsHandler.getRetRelResDocByHash(reqRelResHash);
//        if (getRes.err != false)
//            return getRes;
//        let reqRelResDoc = getRes.reqRelResDoc;
//        let eyeBlockHash = reqRelResDoc.eyeBlock;
//        let reserveNumber = reqRelResDoc.reserveNumber;// it could be 1,2,3 or 4


//        let eyeBlockW = dagHandler.searchInDAGSync({
//            query: [
//                ['b_hash', eyeBlockHash],
//                ['b_type', iConsts.BLOCK_TYPES.Coinbase]
//            ],
//        });
//        if (eyeBlockW.length != 1)
//            return { err: true, msg: `The eveBlock(${utils.hash6c(eyeBlockHash)}) not exist in DAG!` }
//        args.eyeBlock = blockUtils.openDBSafeObject(eyeBlockW[0].bBody).content;


//        let neededPercent = 100 - (reserveNumber * 20);
//        let actualPercent = (polling.pllYValue * 100) / (polling.pllYValue + polling.pllNValue);

//        if ((polling.pllYValue >= polling.pllNValue) && (actualPercent >= neededPercent)) {
//            clog.app.info(`Winer ReqForRelRes polling(${utils.hash6c(polling.pllHash)}) ${utils.stringify(polling)}`);
//            ResevedCoinsHandler.releaseTheCoins(args);
//        } else {
//            clog.app.info(`Missed ReqForRelRes polling(${utils.hash6c(polling.pllHash)}). ${utils.stringify(polling)}`);
//            ResevedCoinsHandler.concludeReqRelRes(args);
//        }

//        // mark polling as treated
//        model.sUpdate({
//            table: tablePollings,
//            query: [
//                ['pll_hash', aPolling.pllHash]
//            ],
//            updates: { pll_ct_done: iConsts.CONSTS.YES }
//        });

//    }

//    static releaseTheCoins(args) {
//        if (dagHandler == null)
//            dagHandler = require('../graph-handler/dag-handler');

//        let polling = args.polling;
//        let pollingEndDate = args.pollingEndDate;
//        let reqRelResHash = polling.pllRef;
//        let eyeBlock = args.eyeBlock;

//        let reserveNumber = parseInt(args.reserveNumber); // it could be 1,2,3 or 4
//        if ((reserveNumber < 0) || (reserveNumber > 4))
//            return { err: true, msg: `Invalide reserve number(${reserveNumber})` };


//        // super sceptical test(s)
//        let reservesInfo = ResevedCoinsHandler.getReservesDetails({ blockHashes: [eyeBlock.blockHash] });
//        if (!_.has(reservesInfo, reqRelDoc.eyeBlock) || !_.has(reservesInfo[reqRelDoc.eyeBlock].reservesInfo, reserveNumber))
//            return { err: true, msg: `releaseTheCoins: The eye block(${utils.hash6c(reqRelDoc.eyeBlock)}) reserve number(${reserveNumber}) combination does not exist4!` }
//        reservesInfo = reservesInfo[reqRelDoc.eyeBlock].reservesInfo[reserveNumber];
//        if (reservesInfo.isReleasable != iConsts.CONSTS.YES)
//            return reservesInfo;
//        // let shareHoldersCount = reservesInfo.shareHolders.length;
//        // let isValidRes = ResevedCoinsHandler.is ReserveReleasable({ blockHash: eyeBlock.blockHash, reserveNumber });
//        // if (!isValidRes.isReleasable)
//        //     return isValidRes;


//        // clone eyeBlock and record in DAG directly a new RlBlock

//        let rlDoc = _.clone(rlDocTpl);
//        rlDoc.dType = iConsts.DOC_TYPES.RlDoc;
//        rlDoc.dVer = "0.0.0";
//        rlDoc.refPolling = polling.hash;
//        rlDoc.refCycle = eyeBlock.cycle;
//        rlDoc.eyeBlock = eyeBlock.blockHash;
//        rlDoc.reserveNumber = eyeBlock.reserveNumber;
//        rlDoc.mintedCoins = eyeBlock.mintedCoins;
//        rlDoc.outputs = eyeBlock.outputs;
//        rlDoc.hash = ResevedCoinsHandler.calcRlDocHash(rlDoc);

//        let rlBlock = _.clone(rlBlockTpl);
//        rlBlock.bType = iConsts.BLOCK_TYPES.RlBlock;
//        rlBlock.creator = polling.creator;
//        rlBlock.dComment = polling.dComment;
//        rlBlock.ancestors = [eyeBlock.blockHash];
//        rlBlock.refCycle = eyeBlock.cycle;
//        // rlBlock.signals = iutils.getMachineSignals();   // since it is not going to broadcost, so do not need the signals
//        rlBlock.creationDate = pollingEndDate;
//        rlBlock.docsRootHash = rlDoc.hash;
//        rlBlock.docs = [rlDoc];

//        rlBlock.blockLength = iutils.offsettingLength(JSON.stringify(rlBlock).length);
//        rlBlock.blockHash = ResevedCoinsHandler.hashRlBlock(rlBlock);

//        // record it in DAG
//        dagHandler.addBH.addBlockToDAG({ block: rlBlock })

//        /**
//         * immidiately insert in trx_utxos too & mark block.b_utxo_imported='Y'  as imported
//         */
//        ResevedCoinsHandler.addReleasedUTXOs({ block: rlBlock, reserveNumber });

//        dagHandler.addBH.postAddBlockToDAG({ block: rlBlock });


//        // insert eyeblock in released block table (i_released_reserves)

//        return { err: false }
//    }

//    static addReleasedUTXOs(args) {
//        if (dagHandler == null)
//            dagHandler = require('../graph-handler/dag-handler');


//        let block = args.block;
//        let reserveNumber = args.reserveNumber;

//        for (let outputIndex = 0; outputIndex < block.docs[0].outputs.length; outputIndex++) {
//            let anOutput = block.docs[0].outputs[outputIndex];
//            let refLoc = iutils.packCoinCode(block.docs[0].hash, outputIndex.toString());

//            let uArgs = {
//                cloneCode: rlBlock.refCycle,
//                refLoc: refLoc,
//                creationDate: wBlockEFS.bCreationDate,
//                visibleBy: wBlockEFS.bHash,
//                address: anOutput[0],
//                value: iutils.convertBigIntToJSInt(anOutput[1]).toString(),
//                refCreationDate: block.creationDate
//            };
//            clog.trx.info(`insert new utxo(CB) ${JSON.stringify(uArgs)}`);
//            utxoHandler.addNewUTXO(uArgs);
//        }

//        dagHandler.updateUtxoImported(block.blockHash, iConsts.CONSTS.YES);
//    }

//    static hashRlBlock(block) {
//        // in order to have almost same hash! we sort the attribiutes alphabeticaly
//        let hashableBlock = {
//            ancestors: block.ancestors,
//            bType: block.bType,
//            bVer: block.bVer,
//            confidence: block.confidence,
//            creationDate: block.creationDate,
//            cycle: block.cycle,
//            net: block.net,
//            docsRootHash: block.docsRootHash, // note that we do not put the transactions directly in block hash, instead using transactions-merkle-root-hash
//        };
//        clog.app.info(`Rl Block hashableBlock: ${utils.stringify(hashableBlock)}`);
//        return iutils.doHashObject(hashableBlock);
//    }


//    static concludeReqRelRes(args) {
//        // do nothing
//        let polling = args.polling;
//        let reqRelResHash = polling.pllRef;
//        return { err: false }
//    }

//    /**
//     * preparing vote ballot
//     *
//     * @param {} args
//     *
//     */
//    static async voteReleaseReseve(args) {
//        return 'move to adm voting';

//        let msg;
//        clog.app.info(`vote Release Reseve args: ${utils.stringify(args)}`);
//        // in order to send vote to network, must create a documetn of type vote and pay vote fee too
//        let dTarget = _.has(args, 'dTarget') ? args.dTarget : iConsts.CONSTS.TO_BUFFER;

//        // create ballot doc
//        let ballot = pollHandler.ballotHandler.ZprepareABallot({
//            ref: args.pllHash,
//            vote: args.vote,
//            voteComment: args.voteComment
//        });
//        clog.app.info(`prepared Release Reseve ballot to send ${utils.stringify(ballot)}`);
//        console.log(`prepared Release Reseve ballot to send ${utils.stringify(ballot)}`);

//        // calculate ballot cost
//        let ballotCost = pollHandler.ballotHandler.calcBallotCost({
//            cDate: utils.getNow(),
//            ballot,
//            stage: iConsts.STAGES.Creating
//        });
//        if (ballotCost.err != false)
//            return ballotCost;

//        let spendables = walletHandler.coinPicker.getSomeCoins({
//            selectionMethod: 'precise',
//            minimumSpendable: ballotCost.cost * 1.3  // an small portion bigger to support DPCosts
//        });
//        clog.trx.info(`retrieve spendable UTXOs to pay for Release Reseve Ballot: ${utils.stringify(ballot)}\nspendables: ${utils.stringify(spendables)}`);
//        if (spendables.err != false)
//            return spendables;

//        // create a transaction for payment
//        let changeAddress = walletHandler.getAnOutputAddressSync({ makeNewAddress: true, signatureType: 'Basic', signatureMod: '1/1' });
//        let outputs = [
//            [changeAddress, 1],  // a new address for change back
//            ['TP_BALLOT', ballotCost.cost]
//        ];
//        let inputs = spendables.selectedCoins;
//        let trxNeededArgs = {
//            maxDPCost: utils.floor(ballotCost.cost * 1.3),
//            DPCostChangeIndex: 0, // to change back
//            dType: iConsts.DOC_TYPES.BasicTx,
//            dClass: iConsts.TRX_CLASSES.SimpleTx,
//            ref: ballot.hash,
//            description: 'Pay for Vote a Release Reseve',
//            inputs,
//            outputs,
//        }
//        let trxDtl = walletHandler.makeATransaction(trxNeededArgs);
//        if (trxDtl.err != false)
//            return trxDtl;
//        console.log('signed Release Reseve voting cost trx: ', utils.stringify(trxDtl));

//        // mark UTXOs as used in local machine
//        walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);

//        // inser ballot in local db (as a voted polling);
//        let res = pollHandler.ballotHandler.insertALocalBallot(ballot);
//        if (res.err != false)
//            return res;

//        // push trx & vote-ballot to Block buffer
//        res = await docBufferHandler.pushInAsync({ doc: trxDtl.trx, DPCost: trxDtl.DPCost });
//        if (res.err != false)
//            return res;

//        res = await docBufferHandler.pushInAsync({ doc: ballot, DPCost: ballotCost.cost });
//        if (res.err != false)
//            return res;


//        if (dTarget == iConsts.CONSTS.TO_BUFFER)
//            return { err: false, msg: `Your Release Reseve vote is pushed to Block buffer` };

//        const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
//        // return new Promise((resolve, reject))
//        let r = await wBroadcastBlock.broadcastBlockAsync();
//    }


//    static getCoinsReleaseReq(args) {
//        if (dagHandler == null)
//            dagHandler = require('../graph-handler/dag-handler');

//        clog.app.info(`get CoinsReleaseReq args ${utils.stringify(args)}`);

//        let voter = _.has(args, 'voter') ? args.voter : false;

//        // retrieve machine votes
//        let votes = pollHandler.ballotHandler.searchInLocalBallots();
//        let votesDict = {}
//        for (let aVote of votes)
//            votesDict[aVote.lbtpllHash] = aVote;
//        // console.log('votes votes votes ', votes);
//        // console.log('votesDict votes Dict ', votesDict);

//        let cusQ = `
//            SELECT ppr.ppr_name, ppr.ppr_perform_type, ppr.ppr_votes_counting_method,

//            pll.pll_hash, pll.pll_ref, pll.pll_start_date, pll.pll_end_date, pll.pll_timeframe, pll.pll_status,
//            pll.pll_y_count, pll.pll_n_count, pll.pll_a_count,
//            pll.pll_y_shares, pll.pll_n_shares, pll.pll_a_shares,
//            pll.pll_y_gain, pll.pll_n_gain, pll.pll_a_gain,
//            pll.pll_y_value, pll.pll_n_value, pll.pll_a_value


//            FROM i_pollings pll
//            JOIN i_polling_profiles ppr ON ppr.ppr_name=pll.pll_class

//            WHERE pll.pll_ref_type=$1 AND ppr.ppr_name=$2
//            ORDER BY pll.pll_start_date
//        `;

//        let records = model.customQuery({
//            query: cusQ,
//            values: [iConsts.POLLING_REF_TYPE.ReqForRelRes, iConsts.POLLING_PROFILE_CLASSES.Basic.ppName]
//        });
//        for (let aRes of records) {
//            aRes.pllEndDateYes = pll_end_date utils.minutesAfter(aRes.pll_timeframe * 60, aRes.pll_start_date);
//            aRes.pllEndDateAbstainOrNo = utils.minutesAfter(utils.floor(aRes.pll_timeframe * 60 * 1.5), aRes.pll_start_date);
//            aRes.pllStatus = iConsts.CONSTS.STATUS_TO_LABEL[aRes.pll_status];
//            aRes.machineBallot = _.has(votesDict, aRes.pll_hash) ? votesDict[aRes.pll_hash] : null;

//            // calc potential voter gains
//            if (voter) {
//                let diff = utils.timeDiff(aRes.pll_start_date).asMinutes;
//                let vGain = pollHandler.calculateVoteGain(diff, diff, aRes.pll_timeframe * 60);
//                // console.log('diff', diff);
//                // console.log('pll_timeframe', aRes.pll_timeframe * 60);
//                // console.log('vGain', vGain);
//                aRes.yourGainY = utils.customFloorFloat(vGain.gainYes * 100, 2);
//                aRes.yourGainN = utils.customFloorFloat(vGain.gainNoAbstain * 100, 2);
//                aRes.yourGainA = utils.customFloorFloat(vGain.gainNoAbstain * 100, 2);

//            } else {
//                aRes.yourGainY = 0.0;
//                aRes.yourGainN = 0.0;
//                aRes.yourGainA = 0.0;
//            }

//            // retrieve release request detail
//            let reqDtl;
//            let theDoc = dagHandler.retrieveDocByDocHash({ docHash: aRes.pll_ref });
//            if (theDoc.err != false) {
//                clog.app.error(`err: release request detail document ${utils.stringify(theDoc)}`);
//                aRes.pollingTitle = 'unknown request comment'
//                aRes.creatorColor = `#${utils.hash6c(crypto.keccak256(''))}`;
//                aRes.eyeBlock = 'theDoc.document.eyeBlock';
//                aRes.reserveNumber = 'theDoc.document.reserveNumber';

//            } else {
//                aRes.pollingTitle = theDoc.document.dComment;
//                aRes.creatorColor = `#${utils.hash6c(crypto.keccak256(theDoc.document.creator))}`;
//                aRes.eyeBlock = theDoc.document.eyeBlock;
//                aRes.reserveNumber = theDoc.document.reserveNumber;
//            }
//            aRes.reqDtl = reqDtl;
//        }
//        return { records, err: false };
//    }

//}
