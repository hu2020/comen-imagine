#ifndef COINBASEBLOCK_H
#define COINBASEBLOCK_H


class CoinbaseBlock : public Block
{
public:
  CoinbaseBlock(const QJsonObject& obj);

  bool setByJsonObj(const QJsonObject& obj) override;
  std::tuple<bool, bool> handleReceivedBlock() const override;

  QString getBlockHashableString() const override;
  QString calcBlockHash() const override;
  QString stringifyBExtInfo() const override;

  BlockLenT calcBlockLength(const QJsonObject& block_obj) const override;
  bool controlBlockLength() const override;

  QJsonObject exportBlockToJSon(const bool ext_info_in_document = true) const override;
  QString safeStringifyBlock(const bool ext_info_in_document = true) const override;  // the out put is a serialized (partial JSonObject ) by which extracting real length of transferring block which is equal to remote block

  //  -  -  -  not drived methods
  std::tuple<bool, bool> validateCoinbaseBlock() const;

};


#endif // COINBASEBLOCK_H
