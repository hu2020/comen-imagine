#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/block_types/block_coinbase/coinbase_issuer.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/block/block_types/block_factory.h"

#include "coinbase_block.h"

CoinbaseBlock::CoinbaseBlock(const QJsonObject& obj)
{
  setByJsonObj(obj);
}


bool CoinbaseBlock::setByJsonObj(const QJsonObject& obj)
{
  Block::setByJsonObj(obj);

  // maybe drived class assignings
  return true;
}


/**
* @brief CoinbaseBlock::handleReceivedBlock(
* @return <status, should_purge_record>
*/
std::tuple<bool, bool> CoinbaseBlock::handleReceivedBlock() const
{
  auto[status, should_purge_record] = validateCoinbaseBlock();
  CLog::log("Received validate CoinbaseBlock result: status(" + CUtils::dumpIt(status) + ") should_purge_record(" + CUtils::dumpIt(should_purge_record) + ")", "cb", "trace");
  if (!status) {
    // do something (e.g. bad reputation log for sender neighbor)
    return {false, true};
  }

  CLog::log("dummy log pre add to DAG a CoinbaseBlock: " + CUtils::serializeJson(exportBlockToJSon()), "cb", "trace");

  addBlockToDAG();

  postAddBlockToDAG();


  // broadcast block to neighbors
  if (CUtils::isInCurrentCycle(m_block_creation_date))
  {
    bool push_res = SendingQHandler::pushIntoSendingQ(
      m_block_type,
      m_block_hash,
      safeStringifyBlock(false),
      "Broadcasting the confirmed coinbase block(" + CUtils::hash8c(m_block_hash) + ") in current cycle(" + m_cycle + ")");

    CLog::log("coinbase push_res(" + CUtils::dumpIt(push_res) + ")");
  }

  return {true, true};
}


/**
* @brief CoinbaseBlock::validateCoinbaseBlock
* @return <status, should_purge_record>
*/
std::tuple<bool, bool> CoinbaseBlock::validateCoinbaseBlock() const
{

  auto[cycleStamp, from_, to_, fromHour, toHour] = CUtils::getCoinbaseInfo("", m_cycle);
  Q_UNUSED(cycleStamp);
  Q_UNUSED(fromHour);
  Q_UNUSED(toHour);
  CLog::log("\nValidate Coinbase Block(" + CUtils::hash8c(m_block_hash) + ") cycle:(" + m_cycle + ") from:(" + from_ + ") to:(" + to_ + ")", "cb", "info");
//  let res = { err: true, shouldPurgeMessage: true, msg: '', sender: sender };


  // in case of synching, we force machine to (maybe)conclude the open pollings
  // this code should be somewhere else, because conceptually it has nothing with coinbase flow!
  if (CMachine::isInSyncProcess())
    PollingHandler::doConcludeTreatment(m_block_creation_date);

  auto[status, local_regenerated_coinbase] = CoinbaseIssuer::doGenerateCoinbaseBlock(
    m_cycle,
    CConsts::STAGES::Regenerating,
    m_block_version);
  Q_UNUSED(status);

  // re-write remote values on local values
  local_regenerated_coinbase["ancestors"] = CUtils::convertQStringListToJSonArray(m_ancestors);
  local_regenerated_coinbase["bHash"] = "0000000000000000000000000000000000000000000000000000000000000000";
  local_regenerated_coinbase["bLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(local_regenerated_coinbase).length());

  Block* tmp_block = BlockFactory::create(local_regenerated_coinbase);
  tmp_block->setBlockHash(tmp_block->calcBlockHash());
  local_regenerated_coinbase["bHash"] = tmp_block->getBlockHash();
  QJsonObject tmp_Jblock = tmp_block->exportBlockToJSon();
//  tmp_Jblock["bLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(tmp_Jblock).length());
  CLog::log("dummy dumping after calculating it's length(serialized): " + CUtils::serializeJson(tmp_Jblock) , "cb", "info");
  delete tmp_block;

  CLog::log("dummy dumping local_regenerated_coinbase before calculating it's length(serialized): " + CUtils::serializeJson(local_regenerated_coinbase) , "cb", "info");
//  CLog::log("dummy dumping local_regenerated_coinbase beofr calculating it's length(object): " + CUtils::dumpIt(local_regenerated_coinbase) , "cb", "info");

  if (tmp_Jblock.value("docsRootHash").toString() != m_documents_root_hash)
  {
    QString msg = "Discrepancy in docsRootHash locally created coinbase docsRootHash(" + CUtils::hash8c(tmp_Jblock.value("docsRootHash").toString());
    msg += ") and remote-docsRootHash(" + CUtils::hash8c(m_documents_root_hash) + ") Block(" + CUtils::hash8c(m_block_hash) + ") ";
    CLog::log(msg, "cb", "error");
    CLog::log("Remote block" + dumpBlock(), "cb", "error");
    CLog::log("Local regenrated block" + CUtils::serializeJson(tmp_Jblock), "cb", "error");

    {
      // TODO: remove this block of code after all tests
      auto[status, local_regenerated_coinbase] = CoinbaseIssuer::doGenerateCoinbaseBlock(
        m_cycle,
        CConsts::STAGES::Regenerating,
        m_block_version);
      Q_UNUSED(status);

      Block* tmp_block = BlockFactory::create(local_regenerated_coinbase);
      auto dummyHash = tmp_block->calcBlockHash();
    }

    return {false, true};
  }

  if (tmp_Jblock.value("bHash").toString() == "")
  {
    CLog::log("big failllllllll 1 . Regenerated coinbase in local has no hash! " + CUtils::dumpIt(tmp_Jblock), "cb", "error");
    return {false, true};
  }

  if (tmp_Jblock.value("bHash").toString() != m_block_hash)
  {
    CLog::log("big failllllllll2 in Regenerating coinbase in local, has differetnt hash! " + CUtils::dumpIt(tmp_Jblock), "cb", "error");
    return {false, true};
  }

  CLog::log("remoteConfidence(" + QString::number(m_block_confidence)+ ")", "cb", "info");
  CLog::log("Valid Coinbase block has received. Block(" + m_block_hash + ")", "cb", "info");
  return {true, true};
}

QString CoinbaseBlock::getBlockHashableString() const
{
  // in order to have almost same hash! we sort the attribiutes alphabeticaly
  QString hashable_block = "{";
  hashable_block += "\"ancestors\":" + CUtils::serializeJson(QVariant::fromValue(m_ancestors).toJsonArray()) + ",";
  hashable_block += "\"bCDate\":\"" + m_block_creation_date + "\",";
  hashable_block += "\"bLen\":\"" + CUtils::paddingLengthValue(m_block_length) + "\",";
  hashable_block += "\"bType\":\"" + m_block_type + "\",";
  hashable_block += "\"bVer\":\"" + m_block_version + "\",";
  hashable_block += "\"cycle\":\"" + m_cycle + "\",";
  hashable_block += "\"docsRootHash\":\"" + m_documents_root_hash + "\",";  // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
  hashable_block += "\"net\":\"" + m_net + "\"}";
  return hashable_block;
}

QString CoinbaseBlock::calcBlockHash() const
{
  QString hashable_block = getBlockHashableString();

  // clonedTransactionsRootHash: block.clonedTransactionsRootHash,
  // note that we do not put the clonedTransactions directly in block hash,
  // instead using clonedTransactions-merkle-root-hash

  CLog::log("The Coinbase! block hashable: " + hashable_block + "\n", "app", "trace");
  QString hash = CCrypto::keccak256(hashable_block);
  return hash;
}

QJsonObject CoinbaseBlock::exportBlockToJSon(const bool ext_info_in_document) const
{
  QJsonObject block = Block::exportBlockToJSon(ext_info_in_document);

  // maybe remove add some item in object
  block.remove("bExtInfo");

  if (block.keys().contains("bExtHash"))
    block.remove("bExtHash");

  if (m_block_descriptions == "")
    block["descriptions"] = CConsts::JS_FAKSE_NULL;

  if (block["bVer"].toString() > "0.0.0")
    block.remove("descriptions");

  if (block.keys().contains("fVotes"))
    block.remove("fVotes");

  if (block.keys().contains("signals"))
    block.remove("signals");

  if (block.keys().contains("backer"))
    block.remove("backer");

  block.insert("cycle", m_cycle);
  block["bLen"] = CUtils::paddingLengthValue(calcBlockLength(block));
  return block;
}

QString CoinbaseBlock::safeStringifyBlock(const bool ext_info_in_document) const
{
  QJsonObject block = exportBlockToJSon(ext_info_in_document);

  // recaluculate block final length
  QString tmp_stringified = CUtils::serializeJson(block);
  block["bLen"] = CUtils::paddingLengthValue(tmp_stringified.length());

  QString out = CUtils::serializeJson(block);
  CLog::log("Safe sringified block(Coinbase) Block(" + CUtils::hash8c(m_block_hash) + ") length(" + QString::number(out.length()) + ") the block: " + out, "app", "trace");

  return out;
}

BlockLenT CoinbaseBlock::calcBlockLength(const QJsonObject& block_obj) const
{
  return CUtils::serializeJson(block_obj).length();
}

bool CoinbaseBlock::controlBlockLength() const
{
  QString stringyfied_block = safeStringifyBlock(false);
  if (
      (static_cast<BlockLenT>(stringyfied_block.length()) != m_block_length) &&
      (static_cast<BlockLenT>(stringyfied_block.length()) != m_block_length + 136) // legacy JS coinbase created blocks have mis-calculated block length
  )
  {
    CLog::log("Mismatch coinbase block length Block(" + CUtils::hash8c(m_block_hash) + ") local length(" + QString::number(stringyfied_block.length()) + ") remote length(" + QString::number(m_block_length) + ") stringyfied_block:" + stringyfied_block, "sec", "error");
    return false;
  }
  return true;
}

QString CoinbaseBlock::stringifyBExtInfo() const
{
  return "";
}
