#ifndef FLOATINGVOTEBLOCK_H
#define FLOATINGVOTEBLOCK_H


class FloatingVoteBlock : public Block
{
public:
  FloatingVoteBlock(const QJsonObject& obj);
  bool setByJsonObj(const QJsonObject& obj) override;
  std::tuple<bool, bool> handleReceivedBlock() const override;

  QString dumpBlock() const override;

  QString getBlockHashableString() const override;
  QString calcBlockHash() const override;
  std::tuple<bool, QString> calcBlockExtRootHash() const override;

  QJsonObject exportBlockToJSon(const bool ext_info_in_document = true) const override;
  QString safeStringifyBlock(const bool ext_info_in_document = true) const override;  // the out put is a serialized (partial JSonObject ) by which extracting real length of transferring block which is equal to remote block


  BlockLenT calcBlockLength(const QJsonObject& block_obj) const override;
  bool controlBlockLength() const override;

//  QJsonArray getBlockExtInfoByDocIndex(const CDocIndexT& document_index) const override;


  QString stringifyFloatingVotes() const;

  static QString getSignMsgBFVote(const QJsonObject&);

  static QString calcFVoteExtInfoHash(
    const QJsonArray& signatures,
    const QJsonObject& uSet);

  static std::tuple<bool, Block*> createFVoteBlock(
    const QString& uplink,  // the block which we are voting for
    const QString& block_category,
    const QJsonObject& vote_data,
    const QString& cDate);


};

#endif // FLOATINGVOTEBLOCK_H
