#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/block_types/block.h"
#include "lib/block/block_types/block_factory.h"
#include "lib/block/node_signals_handler.h"
#include "lib/services/society_rules/society_rules.h"

#include "floating_vote_block.h"


FloatingVoteBlock::FloatingVoteBlock(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

bool FloatingVoteBlock::setByJsonObj(const QJsonObject& obj)
{
  Block::setByJsonObj(obj);

  // drived class assignings

  return true;
}


std::tuple<bool, QString> FloatingVoteBlock::calcBlockExtRootHash() const
{
  QString hashables = "{";
  hashables += "\"signatures\":" + CUtils::serializeJson(m_block_ext_info[0].toObject().value("signatures").toArray()) + ",";
  hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_block_ext_info[0].toObject().value("uSet").toObject()) + "}";
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("Floating Vote Block Hashables for ext hash: " + hashables + "\nRegenrated Ext hash: " + hash, "app", "trace");

  return {true, hash};
}

bool FloatingVoteBlock::controlBlockLength() const
{
  QString stringyfied_block = safeStringifyBlock(false);
  if (static_cast<BlockLenT>(stringyfied_block.length()) != m_block_length)
  {
    CLog::log("Mismatch floating vote block length Block(" + CUtils::hash8c(m_block_hash) + ") local length(" + QString::number(stringyfied_block.length()) + ") remote length(" + QString::number(m_block_length) + ") stringyfied_block:" + stringyfied_block, "sec", "error");
    return false;
  }
  return true;
}

BlockLenT FloatingVoteBlock::calcBlockLength(const QJsonObject& block_obj) const
{
  return Block::calcBlockLength(block_obj);
}

QJsonObject FloatingVoteBlock::exportBlockToJSon(const bool ext_info_in_document) const
{
  QJsonObject Jblock = Block::exportBlockToJSon(ext_info_in_document);

  Jblock["bLen"] = CUtils::paddingLengthValue(calcBlockLength(Jblock));

  return Jblock;
}

QString FloatingVoteBlock::safeStringifyBlock(const bool ext_info_in_document) const
{
  QJsonObject block = exportBlockToJSon(ext_info_in_document);

  // maybe remove add some item in object


  // recaluculate block final length
  QString tmp_stringified = CUtils::serializeJson(block);
  block["bLen"] = CUtils::paddingLengthValue(tmp_stringified.length());

  QString out = CUtils::serializeJson(block);
  CLog::log("Safe sringified block(floating vote) Block(" + CUtils::hash8c(m_block_hash) + ") length(" + QString::number(out.length()) + ") the block: " + out, "app", "trace");

  return out;
}

/**
* @brief NormalBlock::handleReceivedBlock
* @return <status, should_purge_record>
*/
std::tuple<bool, bool> FloatingVoteBlock::handleReceivedBlock() const
{
  return {false, true};
}

QString FloatingVoteBlock::dumpBlock() const
{
  // firsdt call parent dump
  QString out = Block::dumpBlock();

  // then child dumpping
  out += "\n in child";
  return out;
}

QString FloatingVoteBlock::calcBlockHash() const
{
  QString hashable_block = getBlockHashableString();

  // clonedTransactionsRootHash: block.clonedTransactionsRootHash, // note that we do not put the clonedTransactions directly in block hash, instead using clonedTransactions-merkle-root-hash

  CLog::log("The floatinf vote! block hashable: " + hashable_block + "\n", "app", "trace");
  return CCrypto::keccak256(hashable_block);
}

QString FloatingVoteBlock::getSignMsgBFVote(const QJsonObject& Jblock)
{
  QString signMsg = "{";
  signMsg += "\"ancestors\":" + CUtils::serializeJson(QVariant::fromValue(Jblock.value("ancestors")).toJsonArray()) + ",";
  signMsg += "\"bCDate\":\"" + Jblock.value("bCDate").toString() + "\",";
  signMsg += "\"bType\":\"" + Jblock.value("bType").toString() + "\",";
  signMsg += "\"bVer\":\"" + Jblock.value("bVer").toString() + "\",";
  signMsg += "\"confidence\":" + QString::number(Jblock.value("confidence").toDouble()) + ",";
  signMsg += "\"net\":\"" + Jblock.value("net").toString() + "\",";
  signMsg += "\"voteData\":" + CUtils::serializeJson(Jblock.value("voteData").toObject()) + "}";

  signMsg = CCrypto::keccak256(signMsg).midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  return signMsg;
}


std::tuple<bool, Block*> FloatingVoteBlock::createFVoteBlock(
  const QString& uplink,  // the block which we are voting for
  const QString& block_category,
  const QJsonObject& vote_data,
  const QString& cDate)
{
  CLog::log("create Floating Vote Block uplink(" + CUtils::hash8c(uplink) + ") bloack Cat(" + block_category + ") cDate(" + cDate + ") vote Data: " + CUtils::dumpIt(vote_data) + "", "app", "trace");

  auto[backer_address_, shares_, percentage] = DNAHandler::getMachineShares();
  Q_UNUSED(backer_address_);
  Q_UNUSED(shares_);
  DNASharePercentT minShare = SocietyRules::getMinShareToAllowedIssueFVote(cDate);
  if (percentage < minShare)
  {
    CLog::log(
      "machine hasn't sufficient shares (" +
      QString::number(percentage) + " < " + QString::number(minShare) +
      " to issue a Floting vote for any collision on block uplink(" +
      CUtils::hash8c(uplink) + ") Category(" + block_category + ") cDate(" +
      cDate + ") vote Data: " + CUtils::dumpIt(vote_data) + "", "app", "trace");

    // TODO: push fVote info to machine buffer in order to broadcost to network in first generated block.
    // therefore even machines with amall amount of shares will vote and pay the vote cost and won't whelm the network
    return {false, nullptr};
  }

  QJsonObject Jblock {
    {"bHash", "0000000000000000000000000000000000000000000000000000000000000000"},
    {"bType", CConsts::BLOCK_TYPES::FVote},
    {"bCat", block_category},
    {"confidence", percentage},
    {"ancestors", QJsonArray{uplink}},
    {"bCDate", CUtils::getNow()},
    {"signals", NodeSignalsHandler::getMachineSignals()},
    {"voteData", vote_data}};

  // create floating vote block
  QString signMsg = getSignMsgBFVote(Jblock);
  auto[status, backer, uSet, signatures] = CMachine::signByMachineKey(signMsg);
  Q_UNUSED(status);

  Jblock["bExtInfo"] = QJsonObject{
    {"uSet", uSet.exportToJson()},
    {"signatures", CUtils::convertQStringListToJSonArray(signatures)}};

  Jblock["backer"] = backer;

  Block* block = BlockFactory::create(Jblock);
  auto[status_ext, bExtHash] = block->calcBlockExtRootHash();
  Q_UNUSED(status_ext);
  block->m_block_ext_root_hash = bExtHash;
  block->getBlockHash() = block->calcBlockHash();

  return {true, block};
}

QString FloatingVoteBlock::getBlockHashableString() const
{
  QString hashables = "{";
  hashables += "\"backer\":\"" + m_block_backer + "\",";
  hashables += "\"block\":\"" + m_block_ext_root_hash + "\",";
  hashables += "\"bLen\":\"" + CUtils::paddingLengthValue(m_block_length) + "\"}";
  return hashables;
}
