#ifndef REPAYBACKBLOCK_H
#define REPAYBACKBLOCK_H

class RepaymentDocument;

class RepaybackBlock : public Block
{
public:
  RepaybackBlock(){};
  RepaybackBlock(const QJsonObject& obj);
  ~RepaybackBlock() override;

  std::vector<RepaymentDocument*>  m_rp_documents;

  bool setByJsonObj(const QJsonObject& obj) override;
  std::tuple<bool, bool> handleReceivedBlock() const override;

  QString getBlockHashableString() const override;
  static QString getBlockHashableString(const QJsonObject& Jblock);

  CBlockHashT calcBlockHash() const override;
  static CBlockHashT calcBlockHash(const QJsonObject& Jblock);

  QJsonObject exportBlockToJSon(const bool ext_info_in_document = true) const override;
  QJsonArray exportDocumentsToJSon(const bool ext_info_in_document = true) const override;
  QString stringifyBExtInfo() const override;

  BlockLenT calcBlockLength(const QJsonObject& block_obj) const override;

  QString safeStringifyBlock(const bool ext_info_in_document = true) const override;

  //  -  -  -  not drived methods
//  std::tuple<bool, bool> validateRepaybackBlock() const;



  static QJsonObject getRepayBlockTpl();

  static void createRepaymentBlock(
    const QJsonObject& block,
    const JORecordsT& repaymentDocs,
    const QVDRecordsT& wBlocksEFS);

  static void importDoubleCheck();
};

#endif // REPAYBACKBLOCK_H
