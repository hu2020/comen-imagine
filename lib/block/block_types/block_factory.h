#ifndef BLOCKFACTORY_H
#define BLOCKFACTORY_H


class BlockFactory
{
public:
  BlockFactory();
  static Block* create(const QJsonObject &obj);

};

#endif // BLOCKFACTORY_H
