#ifndef NORMALBLOCK_H
#define NORMALBLOCK_H


class NormalBlock : public Block
{
public:
  NormalBlock(const QJsonObject& obj);
  bool setByJsonObj(const QJsonObject& obj) override;
  std::tuple<bool, bool> handleReceivedBlock() const override;
  std::tuple<bool, bool, QString, SpendCoinsList*> validateNormalBlock(const QString& stage) const;
  QString dumpBlock() const override;

  QString getBlockHashableString() const override;
  QString calcBlockHash() const override;
  std::tuple<bool, QString> calcBlockExtRootHash() const override;

  QJsonObject exportBlockToJSon(const bool ext_info_in_document = true) const override;

  BlockLenT calcBlockLength(const QJsonObject& block_obj) const override;
  bool controlBlockLength() const override;

//  QJsonArray getBlockExtInfoByDocIndex(const CDocIndexT& document_index) const override;


  QString stringifyFloatingVotes() const;

};

#endif // NORMALBLOCK_H
