#ifndef NORMALBLCOKHANDLER_H
#define NORMALBLCOKHANDLER_H

class NormalBlcokHandler
{
public:
  NormalBlcokHandler();

  static std::tuple<bool, Block*, bool, QString> createANormalBlock(
    QStringList ancestors = {},
    CDateT creation_date = "",
    const bool allowed_to_double_spend = false); // test purpose

};

#endif // NORMALBLCOKHANDLER_H
