#include "stable.h"

#include "lib/dag/dag.h"
#include "lib/ccrypto.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "floating_signature_block.h"

FloatingSignatureBlock::FloatingSignatureBlock(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

bool FloatingSignatureBlock::setByJsonObj(const QJsonObject& obj)
{
  Block::setByJsonObj(obj);

  // drived class assignings
  m_fsign_ext_info = FSignExtInfo {
    SignatureStructureHandler::convertJsonUSetToStruct(obj.value("bExtInfo").toObject().value("uSet").toObject()),
    CUtils::convertJSonArrayToQStringList(obj.value("bExtInfo").toObject().value("signatures").toArray())};

  return true;
}


bool FloatingSignatureBlock::validateFSBlock() const
{
  QString msg;
  // control shares/confidence
  CLog::log("validate FSBlock block Creation Date(" + m_block_creation_date + ") backer(" + m_block_backer  + ") ");
  auto[shares_, issuer_shares_percentage] = DNAHandler::getAnAddressShares(m_block_backer, m_block_creation_date);
  Q_UNUSED(shares_);
  if (m_block_confidence != issuer_shares_percentage)
  {
    msg = "FSBlock(" + CUtils::hash8c(m_block_hash) + ") was rejected because of wrong! confidence(" + QString("%1").arg(m_block_confidence) + ")";
    msg += "!=local(" + QString("%1").arg(issuer_shares_percentage) + ")";
    CLog::log(msg, "app", "error");
    return false;
  }

  // control signature
  if (
    (m_fsign_ext_info.m_unlock_set.m_signature_sets.size() == 0) ||
    (m_fsign_ext_info.m_signatures.size() == 0)
  )
  {
    msg = "Rejected FSBlock because of missed bExtInfo FSBlock(" + CUtils::hash8c(m_block_hash) + ") ";
    CLog::log(msg, "app", "error");
    return false;
  }


  bool isValidUnlock = SignatureStructureHandler::validateSigStruct(
    m_fsign_ext_info.m_unlock_set,
    m_block_backer);
  if (isValidUnlock != true) {
    msg = "Invalid given uSet structure for FSBlock(" + CUtils::hash8c(m_block_hash) + ")";
    CLog::log(msg, "app", "error");
    return false;
  }

  QString ancestors_str = "[\"" + m_ancestors[0] + "\"]";
//  QString ancestors_str = m_ancestors[0];
  QString signMsg = CCrypto::keccak256(ancestors_str).midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
//  let signMsg = crypto.convertToSignMsg(block.ancestors)

  for (int singatureInx = 0; singatureInx < m_fsign_ext_info.m_unlock_set.m_signature_sets.size(); singatureInx++)
  {
    bool verifyRes = CCrypto::ECDSAVerifysignature(
      m_fsign_ext_info.m_unlock_set.m_signature_sets[singatureInx].m_signature_key,
      signMsg,
      m_fsign_ext_info.m_signatures[singatureInx]);

    if (verifyRes != true)
    {
      msg = "Invalid given signature for FSBlock(" + CUtils::hash8c(m_block_hash) + ")";
      CLog::log(msg, "app", "error");
      return false;
    }
  }

  msg = "received FSBlock(" + CUtils::hash8c(m_block_hash) + ") is valid";
  CLog::log(msg, "app", "info");
  return true;
}

/**
 * @brief FloatingSignatureBlock::handleReceivedBlock
 * @return <status, should_purge_record>
 */
std::tuple<bool, bool> FloatingSignatureBlock::handleReceivedBlock() const
{

  bool is_valid = validateFSBlock();
  if (!is_valid)
  {
    // do something
    return {false, true};
  }

  // record in dag

  CLog::log("add a valid FSBlock(" + CUtils::hash8c(m_block_hash) + ") to DAG", "app", "info");
  addBlockToDAG();
  postAddBlockToDAG();

  // broadcast to neighbors
  if (CUtils::isInCurrentCycle(m_block_creation_date))
  {
    bool pushRes = SendingQHandler::pushIntoSendingQ(
      m_block_type,
      m_block_hash,
      safeStringifyBlock(false),
      "Broadcasting the confirmed FS block(" + CUtils::hash8c(m_block_hash) + ") in current cycle(" + m_cycle + ")");

    CLog::log("FS pushRes(" + CUtils::dumpIt(pushRes) + ")");

  }

  return {true, true};

}

QJsonObject FloatingSignatureBlock::exportBlockToJSon(const bool ext_info_in_document) const
{
  QJsonObject block = Block::exportBlockToJSon(ext_info_in_document);

  block.remove("docs");
  block.remove("fVotes");
  block.remove("docsRootHash");

  block["confidence"] = m_block_confidence;
  block["cycle"] = m_cycle;

  block["bLen"] = CUtils::paddingLengthValue(calcBlockLength(block));

  return block;
}

/**
*
* @param {time} cDate
* the functions accepts a time and searchs for all floating signatures which are signed the prev coinbase block(either linked or not linked blocks)
*/
std::tuple<double, QStringList, QStringList> FloatingSignatureBlock::aggrigateFloatingSignatures(
  const QString& cDate)
{
  // retrieve prev cycle info
  if (CUtils::getNow() > CUtils::getCoinbaseRange(CMachine::getLaunchDate()).to)
  {
    auto[cycleStamp_, from_, to_, fromHour, toHour] = CUtils::getPrevCoinbaseInfo(cDate);
    Q_UNUSED(fromHour);
    Q_UNUSED(to_);
    Q_UNUSED(toHour);
//    clog.cb.info(`rerieve prevCoinbaseInfo ${cDate}=>${JSON.stringify(prevCoinbaseInfo)} `)

    // retrieve prev cycle coinbases
    QVDRecordsT prvCoinbaseBlocks = DAG::searchInDAG(
      {
        {"b_type", CConsts::BLOCK_TYPES::Coinbase},
        {"b_cycle", cycleStamp_},
        {"b_creation_date", from_, ">="}
      },
      {"b_hash"},
      {
        {"b_confidence", "DESC"},
        {"b_hash", "ASC"}
      });
    CLog::log("prvCoinbaseBlocks: " + CUtils::dumpIt(prvCoinbaseBlocks), "cb", "trace");
    QStringList prvCoinbaseBlocks_ {};
    for(QVDicT a_row: prvCoinbaseBlocks)
      prvCoinbaseBlocks_.append(a_row["b_hash"].toString());

    // retrieve all floating signature blocks which are created in prev cycle
    CLog::log("retrieve floating signatures for cycle(" + cycleStamp_ + ") from(" + from_ + ") ", "cb", "trace");

    QVDRecordsT fSWBlocks = DAG::searchInDAG(
      {
        {"b_type", CConsts::BLOCK_TYPES::FSign},
        {"b_cycle", cycleStamp_},
        {"b_creation_date", from_, ">=" }
      }, // TODO add a max Date to reduce results
      {"b_hash", "b_ancestors", "b_confidence", "b_backer"},
      {
        {"b_confidence", "DESC"},
        {"b_hash", "ASC"}});

    QStringList blockHashes = {};
    doubleDicT backers = {};
    for (QVDicT fSWBlock: fSWBlocks)
    {
      // drop float if it is not linked to proper coinbase block
      bool isLinkedToPropoerCB = false;
      QStringList tmpAncs = fSWBlock.value("b_ncestors").toString().split(",");
      for (QString anAnc: tmpAncs)
      {
        if (prvCoinbaseBlocks_.contains(anAnc))
          isLinkedToPropoerCB = true;
      }
      if (!isLinkedToPropoerCB)
          continue;

      backers[fSWBlock.value("b_backer").toString()] = fSWBlock.value("b_confidence").toFloat();
      blockHashes.append(fSWBlock.value("b_hash").toString());
    }
    double confidence = 0.0;
    for (QString bckr: backers.keys())
    {
      confidence += backers[bckr];
    }
    confidence = CUtils::iFloorFloat(confidence);

    return {
      confidence,
      blockHashes,
      backers.keys()};

  } else {
    // machine is in init cycle, so there is no floating signture
    QVDRecordsT genesis = DAG::searchInDAG({{"b_type", CConsts::BLOCK_TYPES::Genesis}});

    return {
      100.00,
      {genesis[0].value("b_hash").toString()}, // only the genesis block hash
      {}};
  }
}

QString FloatingSignatureBlock::safeStringifyBlock(const bool ext_info_in_document) const
{
  QJsonObject block = exportBlockToJSon(ext_info_in_document);

  // maybe remove add some item in object
  if (m_block_descriptions == "")
    block["descriptions"] = CConsts::JS_FAKSE_NULL;

  // recaluculate block final length
  QString tmp_stringified = CUtils::serializeJson(block);
  block["bLen"] = CUtils::paddingLengthValue(tmp_stringified.length());

  QString out = CUtils::serializeJson(block);
  CLog::log("Safe sringified block(floating signature) Block(" + CUtils::hash8c(m_block_hash) + ") length(" + QString::number(out.length()) + ") the block: " + out, "app", "trace");

  return out;
}

bool FloatingSignatureBlock::createDocuments(const QJsonValue& documents)
{
  return true;
}


QString FloatingSignatureBlock::stringifyBExtInfo() const
{
  QJsonArray block_ext_info {m_block_ext_info[0].toObject()};
  QString out = CUtils::serializeJson(block_ext_info);
  return out;
}
