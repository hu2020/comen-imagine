#ifndef FLOATINGSIGNATUREBLOCK_H
#define FLOATINGSIGNATUREBLOCK_H

class FSignExtInfo {
public:
  UnlockSet m_unlock_set;
  QStringList m_signatures;
};


//   -  -  -  FloatingSignatureBlock
class FloatingSignatureBlock : public Block
{
public:
  FloatingSignatureBlock(const QJsonObject& obj);

  FSignExtInfo m_fsign_ext_info = {};

  bool setByJsonObj(const QJsonObject& obj) override;
  bool createDocuments(const QJsonValue& documents) override;
  QJsonObject exportBlockToJSon(const bool ext_info_in_document = true) const override;
  QString stringifyBExtInfo() const override;

  QString safeStringifyBlock(const bool ext_info_in_document = true) const override;  // the out put is a serialized (partial JSonObject ) by which extracting real length of transferring block which is equal to remote block

  std::tuple<bool, bool> handleReceivedBlock() const override;

  bool validateFSBlock() const;

  static std::tuple<double, QStringList, QStringList> aggrigateFloatingSignatures(
    const QString& cDate = CUtils::getNow());
};

#endif // FLOATINGSIGNATUREBLOCK_H
