#ifndef GENESIS_H
#define GENESIS_H

#include <QJsonObject>

class Block;
#include "lib/block/block_types/block.h"
#include "lib/block/document_types/dna_proposal_document.h"
#include "lib/services/dna/dna_handler.h"
#include "lib/services/polling/polling_handler.h"

class GenesisBlock : public Block
{
public:
  GenesisBlock();
  GenesisBlock(const QJsonObject &obj){ setByJsonObj(obj); };
  bool setByJsonObj(const QJsonObject &obj) override;

  GenRes initGenesisBlock();
  QJsonObject exportBlockToJSon(const bool ext_info_in_document = true) const override;
  QString safeStringifyBlock(const bool ext_info_in_document = true) const override;

  QString getBlockHashableString() const override;
  QString calcBlockHash() const override;

  GenRes initShares();
};

#endif // GENESIS_H
