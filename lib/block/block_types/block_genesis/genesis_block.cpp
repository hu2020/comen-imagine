#include "genesis_block.h"

#include "lib/ccrypto.h"
#include "lib/clog.h"
#include "lib/dag/dag.h"
#include "lib/block/block_types/block.h"

GenesisBlock::GenesisBlock()
{
    m_block_descriptions = "Imagine all the people sharing all the world";
    m_block_confidence = 100.0;
}

bool GenesisBlock::setByJsonObj(const QJsonObject &obj)
{
  Block::setByJsonObj(obj);

  // custom settings for Genesis block
  m_block_type = CConsts::BLOCK_TYPES::Genesis;

  return true;
}

GenRes GenesisBlock::initGenesisBlock()
{
  QString msg;
  m_ancestors = QStringList {","};
  m_block_type = CConsts::BLOCK_TYPES::Genesis;
  m_block_creation_date = CMachine::getLaunchDate();
  m_documents = {};

  CDateT proposal_creation_date = CUtils::minutesBefore(CMachine::getCycleByMinutes()*2, CMachine::getLaunchDate());
  QJsonObject Jdoc {
    {"dType", CConsts::DOCUMENT_TYPES::DNAProposal},
    {"dTitle", "fair effort, fair gain, win win win"},
    {"dCDate", proposal_creation_date},
    {"projectHash", CCrypto::convertTitleToHash("imagine")},
    {"helpHours", 1000000},
    {"helpLevel", 1},
    {"contributor", CConsts::HU_DNA_SHARE_ADDRESS},
    {"dTags", "initialize, DNA"},
    {"dComment", "imagine's contributors time & effort is recorded here"},
    {"pollingProfile", "Basic"},
    {"pTimeframe", 24},
    {"dExtInfo", QJsonObject {}},
  };
  DNAProposalDocument* dna_doc = new DNAProposalDocument(Jdoc);
  dna_doc->m_block_creation_date = proposal_creation_date;
  dna_doc->m_approval_date = proposal_creation_date;
  dna_doc->m_shares = dna_doc->m_help_hours * dna_doc->m_help_level;
  dna_doc->m_votes_yes = static_cast<uint64_t>(1000000);
  dna_doc->m_votes_abstain = 0;
  dna_doc->m_votes_no = 0;
  dna_doc->m_doc_hash = dna_doc->calcDocHash();
  m_documents.push_back(dna_doc);

  m_documents_root_hash = dna_doc->getDocHash(); // "fb20e4323d695db7728eabcf3a44a1c0516d23362622fa3093e7cf887ef88396";
  m_block_hash = calcBlockHash();//"7a2e58190452d3764afd690ffd13a1360193fdf30f932fc1b2572e834b72c291";
  m_block_backer = CConsts::HU_DNA_SHARE_ADDRESS;

  GenRes res = addBlockToDAG();
  if (!res.status)
  {
    msg = "Failed in add genesis block to DAG. " + res.msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // set initial shares
  return initShares();
}

GenRes GenesisBlock::initShares()
{
  const QString startVotingDate = CUtils::minutesBefore(5 * CMachine::getCycleByMinutes(), CMachine::getLaunchDate());

  DNAProposalDocument* initial_proposal = dynamic_cast<DNAProposalDocument*>(m_documents.front());
  QString proposal_hash = initial_proposal->getDocHash();

  // update proposal status in DB
  QVDicT update_values {
    {"pr_start_voting_date", startVotingDate},
    {"pr_conclude_date", CConsts::LAUNCH_DATE},
    {"pr_approved", CConsts::YES}};

  DNAProposalDocument::updateProposal(
    update_values,
    {ModelClause("pr_hash", proposal_hash)});

  // conclude the polling
  QVDicT pollingUpdValues;
  pollingUpdValues.insert("pll_start_date", startVotingDate);
  pollingUpdValues.insert("pll_end_date", CUtils::minutesAfter(36 * 60, startVotingDate));
  pollingUpdValues.insert("pll_status", CConsts::CLOSE);
  pollingUpdValues.insert("pll_ct_done", CConsts::YES);
  PollingHandler::updatePolling(
    pollingUpdValues,
    {{"pll_ref", proposal_hash}});

  // also insert in db DNA initiate shares
  return DNAHandler::insertAShare(initial_proposal);
}

QJsonObject GenesisBlock::exportBlockToJSon(const bool ext_info_in_document) const
{
  QJsonObject block = Block::exportBlockToJSon(ext_info_in_document);
  return block;
}

QString GenesisBlock::safeStringifyBlock(const bool ext_info_in_document) const
{
  QJsonObject block = exportBlockToJSon(ext_info_in_document);

  // recaluculate block final length
  QString tmp_stringified = CUtils::serializeJson(block);
  block["bLen"] = CUtils::paddingLengthValue(tmp_stringified.length());

  QString out = CUtils::serializeJson(block);
  CLog::log("Safe sringified block(Genesis) Block(" + CUtils::hash8c(m_block_hash) + ") length(" + QString::number(out.length()) + ") the block: " + out, "app", "trace");

  return out;
}

QString GenesisBlock::getBlockHashableString() const
{
  // in order to have almost same hash! we sort the attribiutes alphabeticaly
  QString hashable_block = "{";
  hashable_block += "\"ancestors\":[],";
  hashable_block += "\"bCDate\":\"" + m_block_creation_date + "\",";
  hashable_block += "\"bType\":\"" + m_block_type + "\",";
  hashable_block += "\"bVer\":\"" + m_block_version + "\",";
  hashable_block += "\"docsRootHash\":\"" + m_documents_root_hash + "\",";  // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
  hashable_block += "\"net\":\"" + m_net + "\"";
  hashable_block += "}";
  return hashable_block;
}

CBlockHashT GenesisBlock::calcBlockHash() const
{
  QString hashable_block = getBlockHashableString();

  // clonedTransactionsRootHash: block.clonedTransactionsRootHash, // note that we do not put the clonedTransactions directly in block hash, instead using clonedTransactions-merkle-root-hash

  CBlockHashT block_hash = CCrypto::keccak256(hashable_block);
  CLog::log("The Genesis! block(" + block_hash  + ") hashable: " + hashable_block + "\n", "app", "trace");

  return block_hash;
}
