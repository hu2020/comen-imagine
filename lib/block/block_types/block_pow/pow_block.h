#ifndef POWBLOCK_H
#define POWBLOCK_H


class POWBlock: public Block
{
public:
  POWBlock(const QJsonObject& obj);
  QString m_block_nonce;

  bool setByJsonObj(const QJsonObject& obj) override;
  std::tuple<bool, bool> handleReceivedBlock() const override;

  QString getBlockHashableString() const override;
  QString calcBlockHash() const override;
  std::tuple<bool, QString> calcBlockExtRootHash() const override;

  BlockLenT calcBlockLength(const QJsonObject& block_obj) const override;

  QJsonObject exportBlockToJSon(const bool ext_info_in_document = true) const override;
  QString safeStringifyBlock(const bool ext_info_in_document = true) const override;  // the out put is a serialized (partial JSonObject ) by which extracting real length of transferring block which is equal to remote block
  QString stringifyBExtInfo() const override;


  QString getNonce() const override;


  //  -  -  -  not drived methods
  static QString getTargetPattern(
    const QString& cDate,
    uint8_t difficulty = 0);

  bool validatePOWBlock(const QString& stage) const;

  bool hashDifficultyCheck() const;

};


#endif // POWBLOCK_H
