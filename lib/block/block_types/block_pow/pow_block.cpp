#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/block/block_types/block_factory.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/dna/dna_handler.h"
#include "lib/block/documents_in_related_block/free_documents/free_documents_in_related_block.h"

#include "pow_block.h"



POWBlock::POWBlock(const QJsonObject& obj)
{
  setByJsonObj(obj);
}


bool POWBlock::setByJsonObj(const QJsonObject& obj)
{
  Block::setByJsonObj(obj);

  // maybe drived class assignings
  m_block_nonce = obj.value("nonce").toString();

  auto[shares_, confidence] = DNAHandler::getAnAddressShares(m_block_backer, m_block_creation_date);
  Q_UNUSED(shares_);
  m_block_confidence = confidence;

  return true;
}

BlockLenT POWBlock::calcBlockLength(const QJsonObject& block_obj) const
{
  return CUtils::serializeJson(block_obj).length();
}

QJsonObject POWBlock::exportBlockToJSon(const bool ext_info_in_document) const
{
  QJsonObject out = Block::exportBlockToJSon(ext_info_in_document);
  out["nonce"] = m_block_nonce;
  out["bLen"] = CUtils::paddingLengthValue(calcBlockLength(out));

  return out;
}

std::tuple<bool, QString> POWBlock::calcBlockExtRootHash() const
{
  // for POW blocks the block has only one document and the dExtHash of doc and bExtHash of block are equal
  Document* tmp_doc = DocumentFactory::create(m_documents[0]->exportDocToJson(), this, 0);
  QString hash = tmp_doc->calcDocExtInfoHash();
  delete tmp_doc;
  return {true, hash};
}


QString POWBlock::getBlockHashableString() const
{
  // in order to have almost same hash! we sort the attribiutes alphabeticaly
  QString hashable_block = "{";
  hashable_block += "\"ancestors\":" + CUtils::serializeJson(QVariant::fromValue(m_ancestors).toJsonArray()) + ",";
  hashable_block += "\"backer\":\"" + m_block_backer + "\",";
  hashable_block += "\"bLen\":\"" + CUtils::paddingLengthValue(m_block_length) + "\",";
  hashable_block += "\"bType\":\"" + m_block_type + "\",";
  hashable_block += "\"bVer\":\"" + m_block_version + "\",";
  hashable_block += "\"bCDate\":\"" + m_block_creation_date + "\",";
  if (m_block_descriptions != "")
    hashable_block += "\"descriptions\":\"" + m_block_descriptions + "\",";
  hashable_block += "\"docsRootHash\":\"" + m_documents_root_hash + "\",";
  hashable_block += "\"bExtHash\":\"" + m_block_ext_root_hash + "\",";
  hashable_block += "\"fVotes\":" + CUtils::serializeJson(m_floating_votes) + ",";
  hashable_block += "\"net\":\"" + m_net + "\",";
  hashable_block += "\"nonce\":\"" + m_block_nonce + "\",";
  hashable_block += "\"signals\":" + CUtils::serializeJson(m_signals) + "}";
  return hashable_block;
}

QString POWBlock::calcBlockHash() const
{
  QString hashable_block = getBlockHashableString();

  QString hash = CCrypto::keccak256(hashable_block);
  CLog::log("The POW block hashable string: " + hashable_block + "\ncalculated hash: " + hash, "app", "trace");
  return hash;
}

QString POWBlock::safeStringifyBlock(const bool ext_info_in_document) const
{
  QJsonObject blcok = exportBlockToJSon(ext_info_in_document);

  // maybe remove add some item in object

  // recaluculate block final length
  blcok["bLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(blcok).length());

  QString out = CUtils::serializeJson(blcok);
  CLog::log("Safe sringified block(POW) Block(" + CUtils::hash8c(m_block_hash) + ") length(" + QString::number(out.length()) + ") the block: " + out, "app", "trace");

  return out;
}

QString POWBlock::getTargetPattern(
  const QString& cDate,
  uint8_t difficulty)
{
  if (difficulty == 0)
    difficulty = SocietyRules::getPoWDifficulty(cDate);

  // MMDDHHmmssYYYY // "2020-03-23 00:00:00"
  QStringList dateDetails = cDate.split(" ")[0].split("-");
  QStringList hourDetails = cDate.split(" ")[1].split(":");

  QString targetPattern = dateDetails[1] + dateDetails[2] + hourDetails[0] + hourDetails[1] + hourDetails[2] + dateDetails[0];
  targetPattern += targetPattern;

  targetPattern = targetPattern.midRef(0, difficulty).toString();
  return targetPattern;
}

bool POWBlock::hashDifficultyCheck() const
{
  QString targetPattern = getTargetPattern(m_block_creation_date);
  if (m_block_hash.startsWith(targetPattern))
    return true;

  CLog::log("The POW block didn't meet desired hash difficulty! Block(" + m_block_hash + ")", "sec", "error");
  return false;
}

bool POWBlock::validatePOWBlock(const QString& stage) const
{
  // re-calculate block hash
  QString block_hash = calcBlockHash();
  if (block_hash != m_block_hash)
  {
    CLog::log("POW mismatch hash local(" + m_block_hash + ") remote(" + block_hash + ")", "app", "trace");
    return false;
  }

  if (!hashDifficultyCheck())
    return false;

  if (m_documents.size() > 1)
  {
    CLog::log("POW block can not have more than one dosument! Block(" + CUtils::hash8c(m_block_hash) + ")", "sec", "error");
    return false;
  }


  TransientBlockInfo grpdRes = groupDocsOfBlock(stage);
  if (!grpdRes.m_status)
    return false;

  // TODO: important! currently the order of validating documents of block is important(e.g. polling must be validate before proposals and pledges)
  // improve the code and remove this dependency


  /**
  * validate free-docs (if exist)
  */
  bool freeDocsValidateRes = FreeDocumentsInRelatedBlock::validatePOWFreeDocs(
    this,
    grpdRes,
    stage);
  if (!freeDocsValidateRes)
    return false;


  // validate...
  CLog::log("Confirmed POW Block(" + CUtils::hash8c(m_block_hash) + ")", "app", "trace");

//  let hookValidate = listener.doCallSync('SASH_validate_POW_block', block);
//  if (_.has(hookValidate, 'err') && (hookValidate.err != false)) {
//    return hookValidate;
//  }

  return true;
}


/**
* @brief POWBlock::handleReceivedBlock(
* @return <status, should_purge_record>
*/
std::tuple<bool, bool> POWBlock::handleReceivedBlock() const
{
  CLog::log("Handling a received POW Block(" + m_block_hash + ") ", "app", "info");

  bool validateRes = validatePOWBlock(CConsts::STAGES::Validating);
  if (!validateRes)
  {
    // maybe do something
    return {false, true};
  }

  //TODO: prepare a mega query to run in atomic transactional mod

  // add block to DAG(refresh leave blocks, remove from utxos, add to utxo12...)
  // insert the block in i_blocks as a confirmed block
  addBlockToDAG();
  postAddBlockToDAG();

  // broadcast block to neighbors
  if (DAG::isDAGUptodated())
  {
    bool pushRes = SendingQHandler::pushIntoSendingQ(
      m_block_type,
      m_block_hash,
      safeStringifyBlock(false),
      "Broadcasting confirmed POW block(" + CUtils::hash8c(m_block_hash) + ")");
    CLog::log("POW pushRes(" + CUtils::dumpIt(pushRes) + ")");

    return {true, true};
  }

  return {false, true};
}

QString POWBlock::getNonce() const
{
  return m_block_nonce;  // only free doc (by POW) can have nonce and m_nonce; member
}

QString POWBlock::stringifyBExtInfo() const
{
  QString out = CUtils::serializeJson(m_block_ext_info);
  return out;
}
