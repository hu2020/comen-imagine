#ifndef BLOCK_H
#define BLOCK_H

#include "stable.h"


typedef QHash<QString, Document*> DocDicT;
typedef QHash<QString, std::vector<Document*> > DocDicVecT;

/**
 * @brief The BlockRecord class
 * it contains information about a block(m_body) and some meta information of block
 */
class BlockRecord
{
public:
  uint64_t m_id = 0;
  QString m_hash = ""; // block root hash
  QString m_type = ""; // block type (genesis/coinbase/normal)
  QString m_cycle = ""; // the coin base cycle
  double m_confidence = 0.0; // if the block is coinbase it denots to percentage of share of signers
  QString m_ext_root_hash = ""; // it was ext_infos_root_hash segwits/zippedInfo... root hashes
  QString m_documents_root_hash = ""; // it was docs_root_hash documents root hash
  QString m_signals = ""; // block signals
  uint64_t m_trxs_count = 0; // transaction counts
  uint64_t m_docs_count = 0; // documents counts
  uint64_t m_ancestors_count = 0; // ancestors counts
  QStringList m_ancestors = {}; // comma seperated block ancestors
  QStringList m_descendents = {}; // comma seperated block descendents
  QString m_body = ""; // stringified json block full body(except block ext info)
  QString m_creation_date = ""; // the block creation date which stated by block creator
  QString m_receive_date = ""; // the block receive date in local, only for statistics
  QString m_confirm_date = ""; // the block confirmation date in local node
  QString m_block_backer = ""; // the BECH32 address of who got paid because of creating this block
  QString m_utxo_imported = ""; // does UTXO imported to i_trx_utxo table?

  BlockRecord(const QVDicT& values = {})
  {
    if (values.keys().size() > 0)
      setByRecordDict(values);
  };
  bool setByRecordDict(const QVDicT& values = {});

};

class Block;

class TransientBlockInfo
{
public:
  bool m_status = false;
  const Block* m_block;
  QString m_stage = "";

  QSDicT m_map_trx_hash_to_trx_ref {};
  QSDicT m_map_trx_ref_to_trx_hash {};
  QSDicT m_map_referencer_to_referenced {};
  QSDicT m_map_referenced_to_referencer {};

  QHash<QString, Document*> m_doc_by_hash {};
  QHash<QString, Document*> m_transactions_dict {};
  DocDicVecT m_groupped_documents {};
  QHash<CDocHashT, CDocIndexT> m_doc_index_by_hash {};


  CMPAISValueT m_block_total_output = 0;
  QStringList m_block_documents_hashes {};
  QStringList m_block_ext_infos_hashes {};
  QStringList m_pre_requisities_ancestors {}; // in case of creating a block which contains some ballots, the block explicitely includes the related polling blocks, in order to force and asure existance of polling recorded in DAG, befor applying the ballot(s)


  QString dumpMe();

};

class BlockApprovedDocument
{
public:
  Document* m_approved_doc;
  QString m_approved_doc_hash = "";
  QJsonArray m_approved_doc_ext_info {};
  QString m_approved_doc_ext_hash = "";
};

class Block
{

public:
  Block();
  Block(const QJsonObject& obj){ setByJsonObj(obj); };
  virtual ~Block();

  static const QString stbl_blocks ;
  static const QStringList stbl_blocks_fields;

  static const QString stbl_block_extinfos;
  static const QStringList stbl_block_extinfos_fields;

  QString m_net = "im";
  QString m_block_descriptions = "";
  BlockLenT m_block_length = 0;
  QString m_block_hash = "0000000000000000000000000000000000000000000000000000000000000000";
  QString m_block_type = "";
  QString m_block_version = "0.0.0";
  QStringList m_ancestors = {};
  QStringList m_descendents = {};
  QJsonObject m_signals {};
  QString m_block_backer = ""; // the address which is used to collect transaction-fees
  double m_block_confidence = 0.0; // the sahre percentage of block backer (the node who creatd the block)
  QString m_block_creation_date = "";
  QString m_block_receive_date = "";
  QString m_block_confirm_date = "";
  QString m_cycle = "";
  QString m_descriptions = "";
  QString m_documents_root_hash = "0000000000000000000000000000000000000000000000000000000000000000";

  QString m_block_ext_root_hash = "";
  std::vector<Document *> m_documents = {}; // different type of documents which are inherited from Document
  QJsonArray m_block_ext_info {}; // fixed different type of objects
  QJsonArray m_floating_votes = {}; // TODO: to be implemented later



  //  -  -  -  base class methods
  static std::tuple<bool, bool, QJsonArray> getBlockExtInfo(const QString& blockHash);
  static QStringList getDocumentsHashes(const QJsonObject& block);

  std::tuple<bool, bool, QJsonArray> getBlockExtInfo() const;

  TransientBlockInfo groupDocsOfBlock(const QString& stage) const;


  //  -  -  -  virtual methods
  virtual bool setByJsonObj(const QJsonObject& obj);
  virtual bool objectAssignmentsControlls();

  virtual QString getBlockHashableString() const;
  virtual QString calcBlockHash() const;
  virtual QJsonObject exportBlockToJSon(const bool ext_info_in_document = true) const;

  virtual void calcAndSetBlockLength();
  virtual BlockLenT calcBlockLength(const QJsonObject& block_obj) const;

  virtual bool controlBlockLength() const;

  /**
   * @brief createDocuments
   * makes a loop for JSon documents and converts them to C++ Document object
   * @param documents
   * @return
   */
  virtual bool createDocuments(const QJsonValue& documents);

  virtual BlockLenT getMaxBlockSize() const;

  virtual QStringList getDocumentsHashes() const;
  virtual std::tuple<bool, CDocHashT> calcDocumentsRootHash() const;
  virtual std::tuple<bool, CDocHashT> calcBlockExtRootHash() const;
  virtual bool fillInBlockExtInfo();

  virtual void doHashObject();
  virtual QString stringifyBExtInfo() const;
  /**
   * @brief safeStringifyBlock
   * @return
   * the out put is a serialized (partial JSonObject ) which the length is equal to remote block
   * the out put also will be broadcast to neighbors (if it needed to be published in network)
   */
  virtual QString safeStringifyBlock(const bool ext_info_in_document = true) const;


  /**
   * @brief getBlockSignMsg
   * @return {has_signable_part, sign_message}
   */
  virtual std::tuple<bool, QString> getBlockSignMsg() const;

  virtual std::tuple<bool, bool> handleReceivedBlock() const;
  virtual std::tuple<bool, bool> blockGeneralControls() const;

  virtual QString dumpBlock() const;

  virtual QString getNonce() const;

  virtual bool appendToDocuments(Document* doc);
  virtual std::vector<Document*> getDocuments() const;

  virtual QJsonArray exportDocumentsToJSon(const bool ext_info_in_document = true) const;

  virtual QJsonArray getBlockExtInfoByDocIndex(const CDocIndexT& document_index) const;
  virtual bool appendToExtInfo(const QJsonArray& an_ext_info);


  static std::tuple<bool, QJsonObject> regenerateBlock(const CBlockHashT& block_hash);

  static QVDRecordsT searchInBlockExtInfo(
    const ClausesT& clauses = {},
    const QStringList fields = stbl_block_extinfos_fields,
    const OrderT order = {},
    const uint64_t limit = 0);

  bool addBlockToDAG() const;
  bool postAddBlockToDAG() const;


  void setBlockHash(const CBlockHashT& hash);
  QString getBlockHash() const;
  QString getBacker() const;

  // old name was insertToDB
  static bool insertBlockExtInfoToDB(
    const QString& serializedBextInfo,
    const CBlockHashT& blockHash,
    const CDateT& creation_date);

  static std::tuple<int64_t, QJsonObject> getDocumentJByHash(
    const QJsonObject& block,
    const CDocHashT& doc_hash);

//  static std::tuple<bool, QJsonObject> selectBExtInfosFromDB(const QString&);
//  std::tuple<bool, QJsonObject> selectBExtInfosFromDB() const;
  std::tuple<int64_t, Document*> getDocumentByHash(const CDocHashT& doc_hash) const;

};

#endif // BLOCK_H
