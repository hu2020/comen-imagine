#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "administrative_polling_document.h"

AdministrativePollingDocument::AdministrativePollingDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

AdministrativePollingDocument::~AdministrativePollingDocument()
{

}

bool AdministrativePollingDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  if (obj.value("pSubject").toString() != "")
    m_polling_subject = obj.value("pSubject").toString();

  if (obj.value("pValues").toObject().keys().size() > 0)
    m_polling_values = obj.value("pValues").toObject();

  if (obj.value("dCreator").toString() != "")
    m_polling_creator = obj.value("dCreator").toString();

  if (obj.value("dComment").toString() != "")
    m_doc_comment = obj.value("dComment").toString();

  return true;
}

// old name was calcAdmPollingDocCost
std::tuple<bool, CMPAIValueT> AdministrativePollingDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
//    let doc = args.doc;
//    let stage = args.stage;
//    let cDate = args.cDate;

  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;

  CMPAIValueT the_cost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);
//      cnfHandler.getDocExpense({ cDate, cDate, dType: doc.dType, dClass: doc.dClass, dLen });

  if (stage == CConsts::STAGES::Creating)
  {
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc adm polling the_cost + machine interest(" + CUtils::sepNum(the_cost) +" micro PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }

  return {true, CUtils::CFloor(the_cost)};
}

bool AdministrativePollingDocument::hasSignable() const
{
  return true;
}

QString AdministrativePollingDocument::safeStringifyPollingValues() const
{
  QString out = "";
  if (QStringList {
    POLLING_TYPES::RFRfMinS2Wk,
    POLLING_TYPES::RFRfMinS2DA,
    POLLING_TYPES::RFRfMinS2V,
    POLLING_TYPES::RFRfMinFSign,
    POLLING_TYPES::RFRfMinFVote}.contains(m_polling_subject))
  {
    QString out = "{";
    out += "\"pShare\":\"" + m_polling_values.value("pShare").toString() + "\",";
    out += "\"pTimeframe\":" + QString::number(m_polling_values.value("pTimeframe").toDouble()) + "}";
    return out;

  } else if (QStringList {
    POLLING_TYPES::RFRfPLedgePrice,
    POLLING_TYPES::RFRfPollingPrice,
    POLLING_TYPES::RFRfTxBPrice,
    POLLING_TYPES::RFRfClPLedgePrice,
    POLLING_TYPES::RFRfDNAPropPrice,
    POLLING_TYPES::RFRfBallotPrice,
    POLLING_TYPES::RFRfINameRegPrice,
    POLLING_TYPES::RFRfINameBndPGPPrice,
    POLLING_TYPES::RFRfINameMsgPrice,
    POLLING_TYPES::RFRfFPostPrice,
    POLLING_TYPES::RFRfBasePrice,
    POLLING_TYPES::RFRfBlockFixCost}.contains(m_polling_subject)) {
    QString out = "{";
    out += "\"pFee\":\"" + m_polling_values.value("pFee").toString() + "\",";
    out += "\"pTimeframe\":" + QString::number(m_polling_values.value("pTimeframe").toDouble()) + "";
    out += "}";
    return out;

  } else {
    CUtils::exiter("Unknown Adm polling subject! " + m_polling_subject, 78);
  }
  return out;
}

QString AdministrativePollingDocument::getDocToBeSignedHash() const
{
  QString signables = "{";
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  signables += "\"dCreator\":\"" + m_polling_creator + "\",";
  signables += "\"dClass\":\"" + m_doc_class + "\",";
  signables += "\"dComment\":\"" + m_doc_comment + "\",";
  signables += "\"dVer\":\"" + m_doc_version + "\",";
  signables += "\"pTimeframe\":" + QString::number(m_voting_timeframe) + "}";
  signables += "\"dType\":\"" + m_doc_type + "\",";
  signables += "\"pSubject\":\"" + m_polling_subject + "\",";
  signables += "\"pValues\":" + safeStringifyPollingValues() + "}";

  QString to_be_signed_hash = CCrypto::keccak256(signables);
  CLog::log("Adm-polling to_be_signed_hash(" + to_be_signed_hash + ") signables: " + signables + " ", "app", "trace");
  return to_be_signed_hash;
}

QString AdministrativePollingDocument::calcDocExtInfoHash() const
{
  QString hash, hashables = "";
  hashables += "{\"signatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("signatures").toVariant().toJsonArray()) + ",";
  hashables += "\"signedHash\":\"" + getDocToBeSignedHash() + "\",";
  hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("uSet").toObject()) + "}";

  hash = CCrypto::keccak256(hashables);
  CLog::log("Ext Hash Hashables adm-polling(" + m_doc_hash + ") Regenrated Ext hash: " + hash + "\nhashables: " + hashables, "app", "trace");
  return hash;
}

// old name was getSignMsgDAdmPolling
QString AdministrativePollingDocument::getDocSignMsg() const
{
  // as always ordering properties by alphabet
  QString signables = "{";
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  signables += "\"dCreator\":\"" + m_polling_creator + "\",";
  signables += "\"dClass\":\"" + m_doc_class + "\",";
  signables += "\"dComment\":\"" + CUtils::textCleaner(m_doc_comment) + "\",";
  signables += "\"dVer\":\"" + m_doc_version + "\",";
  signables += "\"dType\":\"" + m_doc_type + "\",";
  signables += "\"pSubject\":\"" + m_polling_subject + "\",";
  signables += "\"pValues\":" + CUtils::serializeJson(m_polling_values) + "}";

  QString sign_message = CCrypto::keccak256(signables);
  sign_message = sign_message.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  CLog::log("RFRf(" + m_polling_subject +") sign_message(" + sign_message +") signables: " + signables, "app", "trace");
  return sign_message;
}

bool AdministrativePollingDocument::veridfyDocSignature() const
{
  QJsonObject dExtInfo = m_doc_ext_info[0].toObject();
  auto unlockSet = dExtInfo.value("uSet").toObject();

  bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
    unlockSet,
    m_polling_creator);

  if (!is_valid_unlock)
  {
    CLog::log("Invalid creator signature structure on adm polling(" + m_doc_hash + ")! ", "sec", "error");
    return false;
  }

  // anAdmPll signature & permission validate check
  QString sign_message = getDocSignMsg();
  QJsonArray signatures = m_doc_ext_info[0].toObject().value("signatures").toArray();
  for (CSigIndexT signature_index = 0; signature_index < signatures.size(); signature_index++)
  {
    QString a_signature = signatures[signature_index].toString();
    try {
      bool verifyRes = CCrypto::ECDSAVerifysignature(
        unlockSet.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
        sign_message,
        a_signature);
      if (!verifyRes)
      {
        CLog::log("Invalid creator signature sign on adm polling(" + m_doc_hash + ")! ", "sec", "error");
        return false;
      }
    } catch (std::exception) {
      CLog::log("Exception! Invalid creator signature sign on adm polling(" + m_doc_hash + ")! ", "sec", "error");
      return false;
    }
  }
  return true;
}

QString AdministrativePollingDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject document = exportDocToJson(ext_info_in_document);

  // recaluculate block final length
  document["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(document).length());

  CLog::log("1 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(document).length()) + " serialized document: " + CUtils::serializeJson(document), "app", "trace");

  return CUtils::serializeJson(document);
}

QJsonObject AdministrativePollingDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document["pSubject"] = m_polling_subject;
  document["pValues"] = m_polling_values;
  document["dCreator"] = m_polling_creator;

  return document;
}

QString AdministrativePollingDocument::getDocHashableString() const
{
  QString hahsables = "{";
  hahsables += "\"dExtHash\":\"" + m_doc_ext_hash + "\",";
  hahsables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\"}";
  return hahsables;
}

// old name was calcHashDAdmPollingDoc
QString AdministrativePollingDocument::calcDocHash() const
{
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("Hashable string for adm Polling doc doc(" + m_doc_type + " " + m_polling_subject + " / " +
    m_doc_class + ") hash(" + hash + ")" + hashables, "app", "trace");
  return hash;
}

// js name recordAnAdmPolling
bool AdministrativePollingDocument::applyDocFirstImpact(const Block& block) const
{
  bool res = SocietyRules::recordAnAdministrativePollingInDB(block, this);
  if (res)
    CGUI::signalUpdateSocietyPollings();
  return res;
}


// js name was importAdmPollingsCost
void AdministrativePollingDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{
  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  if (block_inspect_container->m_block_alter_treasury_incomes.keys().contains("TP_ADM_POLLING"))
  {
    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_ADM_POLLING"])
    {
      // if polling costs was payed by a valid trx
      bool doc_cost_is_payed = true;

      if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"supporter transaction(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") for Adm Polling is rejected because of doublespending", false};
      }

      if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"The Adm Polling costs is not supported by any trx", false};
      }

      CDocHashT supported_doc_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];
      auto[inx_, polling_doc] = block->getDocumentByHash(supported_doc_hash);
      Q_UNUSED(inx_);

      if (polling_doc->m_doc_class != PollingHandler::POLLING_PROFILE_CLASSES["Basic"]["ppName"].toString())
      {
        doc_cost_is_payed = false;
        cost_payment_status[supported_doc_hash] = {"admPolling dClass(" + polling_doc->m_doc_class +") is not supported", false};
      }

      CDocHashT supporter_trx = block_inspect_container->m_map_U_trx_ref_to_trx_hash[supported_doc_hash];
      QStringList rejected_transactions =block_inspect_container->m_rejected_transactions.keys();
      if (rejected_transactions.contains(supporter_trx) || rejected_transactions.contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[supported_doc_hash] = {"supporter transaction is rejected because of doublespending", false};
      }

      if (doc_cost_is_payed)
      {
        CLog::log("Successfully TP_ADM_POLLING Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_ADM_POLLING)", "app", "trace");

        cost_payment_status[supported_doc_hash].m_message = "Adm-Polling Cost imported to treasury succsessfully.";
        QString title = "TP_ADM_POLLING Polling(" + CUtils::hash6c(supported_doc_hash) + ")";
        TreasuryHandler::insertIncome(
          title,
          "TP_ADM_POLLING",
          title,
          block->m_block_creation_date,
          a_treasury_entry.m_value,
          block->getBlockHash(),
          a_treasury_entry.m_coin);

      } else {
        CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_ADM_POLLING)", "sec", "error");
        CLog::log("cost_payment_status not payed: " + UTXOImportDataContainer::dumpMe(cost_payment_status[supported_doc_hash]), "sec", "error");

        SocietyRules::removeAdmPolling(supported_doc_hash);

        // also remove the related polling
        PollingHandler::removePollingByRelatedAdmPolling(supported_doc_hash);

      }
    }
  }

  block_inspect_container->m_cost_payment_status["TP_ADM_POLLING"] = cost_payment_status;
}

