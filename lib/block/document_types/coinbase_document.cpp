#include "stable.h"

#include "document.h"
#include "lib/ccrypto.h"
#include "coinbase_document.h"


CoinbaseDocument::CoinbaseDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

CoinbaseDocument::~CoinbaseDocument()
{
  deleteOutputs();
}

bool CoinbaseDocument::setByJsonObj(const QJsonObject& obj)
{

  Document::setByJsonObj(obj);

  if (obj.value("cycle").toString() != "")
    m_doc_cycle = obj.value("cycle").toString();

  if (obj.value("treasuryFrom").toString() != "")
    m_treasury_from = obj.value("treasuryFrom").toString();

  if (obj.value("treasuryTo").toString() != "")
    m_treasury_to = obj.value("treasuryTo").toString();

  if (obj.value("mintedCoins").toDouble() > 0)
    m_minted_coins = obj.value("mintedCoins").toDouble();

  if (obj.value("treasuryIncomes").toDouble() > 0)
    m_treasury_incomes = obj.value("treasuryIncomes").toDouble();

  if (obj.value("outputs").toArray().size() > 0)
    setDocumentOutputs(obj.value("outputs"));

  return true;
}

bool CoinbaseDocument::setDocumentOutputs(const QJsonValue& obj)
{
  QJsonArray outputs = obj.toArray();
  for(QJsonValueRef an_output: outputs)
  {
    QJsonArray oo = an_output.toArray();
    TOutput *o  = new TOutput({oo[0].toString(), static_cast<CMPAIValueT>(oo[1].toDouble())});
    m_outputs.push_back(o);
  }
  return true;
}

QString CoinbaseDocument::getDocHashableString() const
{
  QString hahsables = "{";
  hahsables += "\"cycle\":\"" + m_doc_cycle + "\",";
  if (m_doc_class != "")
    hahsables += "\"dClass\":\"" + m_doc_class + "\",";
  hahsables += "\"dType\":\"" + m_doc_type + "\",";
  hahsables += "\"dVer\":\"" + m_doc_version + "\",";
  hahsables += "\"mintedCoins\":" + QString::number(m_minted_coins) + ",";
  hahsables += "\"outputs\":" + stringifyOutputs() + ",";
  hahsables += "\"treasuryFrom\":\"" + m_treasury_from + "\",";
  hahsables += "\"treasuryIncomes\":" + QString::number(m_treasury_incomes) + ",";
  hahsables += "\"treasuryTo\":\"" + m_treasury_to + "\"";
  hahsables += "}";
  return hahsables;
}

QString CoinbaseDocument::calcDocHash() const
{
  QString to_be_hashed_string = getDocHashableString();
  CLog::log("\nHashable string for coinbase block: " + to_be_hashed_string, "app", "trace");
  QString hash = CCrypto::keccak256Dbl(to_be_hashed_string); // NOTE: absolutely using double hash for more security
  return hash;
}

bool CoinbaseDocument::deleteInputs()
{
  return true;
}

bool CoinbaseDocument::deleteOutputs()
{
  for (TOutput* an_output: m_outputs)
    delete an_output;
  return true;
}

std::vector<TInput*> CoinbaseDocument::getInputs() const
{
  return {};
}

std::vector<TOutput*> CoinbaseDocument::getOutputs() const
{
  return m_outputs;
}

std::tuple<bool, QJsonArray> CoinbaseDocument::exportInputsToJson() const
{
  return {false, QJsonArray {}};
}

std::tuple<bool, QJsonArray> CoinbaseDocument::exportOutputsToJson() const
{
  QJsonArray outputs {};
  if (m_outputs.size() ==0)
    return {false, outputs};

  for(auto an_output: m_outputs)
    outputs.push_back(QJsonArray {
      an_output->m_address,
      QVariant::fromValue(an_output->m_amount).toDouble()});

  return {true, outputs};
}

QJsonObject CoinbaseDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document.remove("dExtInfo");

  if (document.keys().contains("dCDate"))
    document.remove("dCDate");

  if (document.keys().contains("dLen"))
    document.remove("dLen");

  if (document.keys().contains("descriptions"))
    document.remove("descriptions");

  document["cycle"] = m_doc_cycle;
  document["treasuryFrom"] = m_treasury_from;
  document["treasuryTo"] = m_treasury_to;
  document["treasuryIncomes"] = QVariant::fromValue(m_treasury_incomes).toDouble();
  document["mintedCoins"] = QVariant::fromValue(m_minted_coins).toDouble();
  return document;
}

bool CoinbaseDocument::applyDocFirstImpact(const Block& block) const
{
  Q_UNUSED(block);
  // coinbase documents haven't first impact functionalities
  return true;
}
