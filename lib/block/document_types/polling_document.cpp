#include "stable.h"

#include "lib/ccrypto.h"
#include "document.h"
#include "lib/utils/version_handler.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/block_types/block_coinbase/reseved_coins_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "polling_document.h"


PollingDocument::PollingDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

PollingDocument::~PollingDocument()
{

}

bool PollingDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  if (obj.value("pTimeframe").toDouble() != 0)
    m_voting_timeframe = obj.value("pTimeframe").toDouble();

  if (obj.value("dRef").toString() != "")
    m_polling_ref = obj.value("dRef").toString();

  if (obj.value("dRefType").toString() != "")
    m_polling_ref_type = obj.value("dRefType").toString();

  if (obj.value("dRefClass").toString() != "")
    m_polling_ref_class = obj.value("dRefClass").toString();

  if (obj.value("dComment").toString() != "")
    m_polling_comment = obj.value("dComment").toString();

  if (obj.value("dCreator").toString() != "")
    m_polling_creator = obj.value("dCreator").toString();

  if (obj.value("startDate").toString() != "")
    m_polling_start_date = obj.value("startDate").toString();

  if (obj.value("status").toString() != "")
    m_polling_status = obj.value("status").toString();

  return true;
}

QJsonObject PollingDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document["dCreator"] = m_polling_creator;
  document["dRefType"] = m_polling_ref_type;
  document["dRefClass"] = m_polling_ref_class;
  document["pTimeframe"] = m_voting_timeframe;

  return document;
}

QString PollingDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject document = exportDocToJson(ext_info_in_document);

  // recaluculate block final length
  document["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(document).length());

  CLog::log("11 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(document).length()) + " serialized document: " + CUtils::serializeJson(document), "app", "trace");

  return CUtils::serializeJson(document);
}

QString PollingDocument::getDocHashableString() const
{
  QString hahsables = "{";
  hahsables += "\"dExtHash\":\"" + m_doc_ext_hash + "\",";
  hahsables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\"}";
  return hahsables;
}

QString PollingDocument::calcDocHash() const // old name was calcHashDPolling
{
  // as always alphabetical sort
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("Hashable string for Polling doc doc(" + m_doc_type + " / " +
    m_doc_class + ") hash(" + hash + ")" + hashables, "app", "trace");
  return hash;
}

// old name was calcPollingCost
std::tuple<bool, CMPAIValueT> PollingDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;

  CMPAIValueT the_cost =
      m_potential_voters_count *
      dLen *
      SocietyRules::getBasePricePerChar(cDate) *
      SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);
//      cnfHandler.getDocExpense({ cDate, dType: polling.dType, dClass: polling.dClass, dLen });

  if (stage == CConsts::STAGES::Creating)
  {
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc cutom post the_cost + machine interest(" + CUtils::sepNum(the_cost) +" micro PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }

  return {true, CUtils::CFloor(the_cost)};
}

std::tuple<bool, QJsonArray> PollingDocument::exportInputsToJson() const
{
  return {false, QJsonArray {}};
}

std::tuple<bool, QJsonArray> PollingDocument::exportOutputsToJson() const
{
  return {false, QJsonArray {}};
}

// js name was recordAPolling & recordPollingInDB
bool PollingDocument::applyDocFirstImpact(const Block& block) const
{
  return PollingHandler::recordPollingInDB(
    block,
    this);
}

QString PollingDocument::getRef() const
{
  return m_polling_ref;
}

QString PollingDocument::getRefType() const
{
  return m_polling_ref_type;
}

QString PollingDocument::getRefClass() const
{
  return m_polling_ref_class;
}

QString PollingDocument::getDocToBeSignedHash() const
{
  QString signables = "{";
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  signables += "\"dClass\":\"" + m_doc_class + "\",";
  signables += "\"dComment\":\"" + m_polling_comment + "\",";
  signables += "\"dCreator\":\"" + m_polling_creator + "\",";
  signables += "\"dRef\":\"" + getRef() + "\",";
  signables += "\"dRefClass\":\"" + m_polling_ref_class + "\",";
  signables += "\"dRefType\":\"" + m_polling_ref_type + "\",";
  signables += "\"dType\":\"" + m_doc_type + "\",";
  signables += "\"dVer\":\"" + m_doc_version + "\",";
  signables += "\"pTimeframe\":" + QString::number(m_voting_timeframe) + "}";

  QString to_be_signed_hash = CCrypto::keccak256(signables);
  CLog::log("Polling to_be_signed_hash(" + to_be_signed_hash + ") signables: " + signables + " ", "app", "trace");
  return to_be_signed_hash;
}

QString PollingDocument::calcDocExtInfoHash() const //calcTrxExtRootHash()
{
  QString hash, hashables = "";

  hashables += "{\"signatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("signatures").toVariant().toJsonArray()) + ",";
  hashables += "\"signedHash\":\"" + getDocToBeSignedHash() + "\",";
  hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("uSet").toObject()) + "}";

  hash = CCrypto::keccak256(hashables);
  CLog::log("Ext Hash Hashables polling(" + m_doc_hash + ") Regenrated Ext hash: " + hash + " hashables: " + hashables, "app", "trace");
  return hash;
}

// old name was importPollingsCost
void PollingDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{
  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  if (block_inspect_container->m_block_alter_treasury_incomes.keys().contains( "TP_POLLING"))
  {
    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_POLLING"])
    {
        // if polling costs was payed by a valid trx
      bool doc_cost_is_payed = true;

      if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"supporter transaction(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") for Polling is rejected because of doublespending", false};
      }

      if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"The Polling costs is not supported by any trx", false};
      }
      CDocHashT polling_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];
      auto[inx_, polling_doc] = block->getDocumentByHash(polling_hash);
      Q_UNUSED(inx_);

      if (polling_doc->m_doc_class != PollingHandler::POLLING_PROFILE_CLASSES["Basic"]["ppName"].toString())
      {
        doc_cost_is_payed = false;
        cost_payment_status[polling_hash] = {"Polling dClass(" + polling_doc->m_doc_class + ") is not supported", false};
      }

      CDocHashT supporter_trx = block_inspect_container->m_map_U_trx_ref_to_trx_hash[polling_hash];
      QStringList rejected_transactions =block_inspect_container->m_rejected_transactions.keys();
      if (rejected_transactions.contains(supporter_trx) || rejected_transactions.contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[polling_hash] = {"supporter transaction is rejected because of doublespending", false};
      }


      if (doc_cost_is_payed)
      {
        CLog::log("Successfully TP_POLLING Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_POLLING)", "app", "trace");

        cost_payment_status[polling_hash].m_message = "Ballot Cost imported to treasury succsessfully.";
        QString title = "TP_POLLING Polling(" + CUtils::hash6c(polling_hash) + ")";
        TreasuryHandler::insertIncome(
          title,
          "TP_POLLING",
          title,
          block->m_block_creation_date,
          a_treasury_entry.m_value,
          block->getBlockHash(),
          a_treasury_entry.m_coin);

        } else {
          CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_POLLING)", "sec", "error");
          CLog::log("cost_payment_status not payed: " + UTXOImportDataContainer::dumpMe(cost_payment_status[polling_hash]), "sec", "error");

          PollingHandler::removePollingG(polling_hash);

          // remove referenced & related doc for which there is a polling
          QString ref_type = polling_doc->getRefType();
          if (ref_type == CConsts::DOC_TYPES::ReqForRelRes)
          {
            ResevedCoinsHandler::removeReqRelRes(polling_doc->getRef());

          }else{

          }
        }
    }
  }
  block_inspect_container->m_cost_payment_status["TP_POLLING"] = cost_payment_status;

}
