#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/services/contracts/flens/iname_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "iname_bind_document.h"

INameBindDocument::INameBindDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

INameBindDocument::~INameBindDocument()
{

}

bool INameBindDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  m_iname_binding_type = obj.value("nbType").toString();
  m_iname_binding_title = obj.value("nbTitle").toString();
  m_iname_binding_comment = obj.value("nbComment").toString();

  // import name binding info(nbInfo)
  QJsonObject iname_binging_info = obj.value("nbInfo").toObject();
  m_pgp_binding_iname_hash = iname_binging_info.value("inameHash").toString();
  m_pgp_binding_iname_owner = iname_binging_info.value("inameOwner").toString();

  m_pgp_binding_label = iname_binging_info.value("pBLabel").toString(); // PGP Bind Label
  m_pgp_binding_comment = iname_binging_info.value("pBComment").toString(); // PGP Bind Comment
  m_pgp_binding_version = iname_binging_info.value("pBVersion").toString(); // PGP Bind Version
  m_pgp_binding_pub_key = iname_binging_info.value("pBPubKey").toString();  // PGP Bind public key

  return true;
}

QJsonObject INameBindDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document["nbType"] = m_iname_binding_type;
  document["nbTitle"] = m_iname_binding_title;
  document["nbComment"] = m_iname_binding_comment;
  document["nbInfo"] = QJsonObject {
    {"inameOwner", m_pgp_binding_iname_owner},
    {"inameHash", m_pgp_binding_iname_hash},
    {"pBLabel", m_pgp_binding_label},
    {"pBComment", m_pgp_binding_comment},
    {"pBVersion", m_pgp_binding_version},
    {"pBPubKey", m_pgp_binding_pub_key}};

  return document;
}


QString INameBindDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject document_json = exportDocToJson(ext_info_in_document);

  if (ext_info_in_document)
    document_json["dExtInfo"] = SignatureStructureHandler::compactUnlockersArray(document_json["dExtInfo"].toArray());

//  // recaluculate block final length
//  document_json["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(document_json).length());

  CLog::log("7 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(document_json).length()) + " serialized document: " + CUtils::serializeJson(document_json), "app", "trace");

  return CUtils::serializeJson(document_json);
}

// js name was calcINameBindCost
std::tuple<bool, CMPAIValueT> INameBindDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;

  CMPAIValueT the_cost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);

  if (stage == CConsts::STAGES::Creating)
  {
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc custom post the_cost + machine interest(" + CUtils::sepNum(the_cost) +" micro PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }
  return {true, CUtils::CFloor(the_cost)};
}

// js name was recordINameBindInDAG, recordABindDoc
bool INameBindDocument::applyDocFirstImpact(const Block& block) const
{
  return INameHandler::recordABindDoc(block, this);
}

QJsonObject INameBindDocument::exportBindInfoToJSon() const
{
  return QJsonObject {
    {"inameHash", m_pgp_binding_iname_hash},
    {"inameOwner", m_pgp_binding_iname_owner},
    {"pBLabel", m_pgp_binding_label},
    {"pBComment", m_pgp_binding_comment},
    {"pBVersion", m_pgp_binding_version},
    {"pBPubKey", m_pgp_binding_pub_key}
  };
}

bool INameBindDocument::hasSignable() const
{
  return true;
}

// old name was getSignMsgDBindReq
QString INameBindDocument::getDocSignMsg() const
{
  QString iname_binding_info = "";
  if (m_iname_binding_type == CConsts::BINDING_TYPES::IPGP)
  {
    iname_binding_info = "{";
    iname_binding_info += "\"pBVersion\":\"" + m_pgp_binging_info.value("pBVersion").toString() + "\",";
    iname_binding_info += "\"pBPubKey\":\"" + m_pgp_binging_info.value("pBPubKey").toString() + "\"";
    iname_binding_info += "}";
  }

  QString signables = "{";
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\"," ;
  signables += "\"dClass\":\"" + m_doc_class + "\"," ;
  signables += "\"dType\":\"" + m_doc_type + "\"," ;
  signables += "\"dVer\":\"" + m_doc_version + "\"," ;
  signables += "\"iNBComment\":\"" + m_iname_binding_comment + "\"," ; // iName binding comment
  signables += "\"iNBInfo\":" + iname_binding_info + "," ;  // iName binding info
  signables += "\"iNBTitle\":\"" + m_iname_binding_title + "\"," ; // iName binding title
  signables += "\"iNBType\":\"" + m_iname_binding_type + "\"," ; // iName binding type


  QString sign_message = CCrypto::keccak256(signables);
  sign_message = sign_message.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  CLog::log("IName bind sign_message(" + sign_message + ") signables: " + signables + " ", "app", "trace");
  return sign_message;
}

bool INameBindDocument::veridfyDocSignature() const
{

  QJsonObject dExtInfo = m_doc_ext_info[0].toObject();
  auto unlockSet = dExtInfo.value("uSet").toObject();

  bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
    unlockSet,
    m_pgp_binding_iname_owner);

  if (!is_valid_unlock)
  {
    CLog::log("Invalid creator signature structure on iname(" + m_doc_hash + ")! ", "sec", "error");
    return false;
  }

  // ballot signature & permission validate check
  QString sign_message = getDocSignMsg();
  QJsonArray signatures = m_doc_ext_info[0].toObject().value("signatures").toArray();
  for (CSigIndexT signature_index = 0; signature_index < signatures.size(); signature_index++)
  {
    QString a_signature = signatures[signature_index].toString();
    try {
      bool verifyRes = CCrypto::ECDSAVerifysignature(
        unlockSet.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
        sign_message,
        a_signature);
      if (!verifyRes)
      {
        CLog::log("Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
        return false;
      }
    } catch (std::exception) {
      CLog::log("Exception! Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
      return false;
    }
  }
  return true;
}

// js name was calcHashBBindReqDoc
QString INameBindDocument::getDocHashableString() const
{
  QString iname_binding_info = "";
  if (m_iname_binding_type == CConsts::BINDING_TYPES::IPGP)
  {
    iname_binding_info = "{";
    iname_binding_info += "\"pBVersion\":\"" + m_pgp_binging_info.value("pBVersion").toString() + "\",";
    iname_binding_info += "\"pBPubKey\":\"" + m_pgp_binging_info.value("pBPubKey").toString() + "\"";
    iname_binding_info += "}";
  }

  QString hahsables = "{";
  hahsables += "\"dCDate\":\"" + m_doc_creation_date + "\"," ;
  hahsables += "\"dClass\":\"" + m_doc_class + "\"," ;
  hahsables += "\"dExtHash\":" + m_doc_ext_hash + ",";
  hahsables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\",";
  hahsables += "\"dType\":\"" + m_doc_type + "\"," ;
  hahsables += "\"dVer\":\"" + m_doc_version + "\"," ;
  hahsables += "\"iNBComment\":\"" + m_iname_binding_comment + "\"," ; // iName binding comment
  hahsables += "\"iNBInfo\":" + iname_binding_info + "," ;  // iName binding info
  hahsables += "\"iNBTitle\":\"" + m_iname_binding_title + "\"," ; // iName binding title
  hahsables += "\"iNBType\":\"" + m_iname_binding_type + "\"," ; // iName binding type

  return hahsables;
}

// old name was calcHashDINameRegReqS
QString INameBindDocument::calcDocHash() const
{
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("\nHashable string for iname binding doc doc(" + m_doc_type + " / " +
    m_doc_class + ") hash(" + hash + ")" + hashables, "app", "trace");
  return hash;
}

// js funcs calcHashBBindReqDoc
QString INameBindDocument::calcDocExtInfoHash() const
{
  QString hashables = "{";
  hashables += "\"signatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("signatures").toArray()) + ",";
  hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("uSet").toObject()) + "}";
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("Bind iName Ext Root Hash Hashables Doc(" + m_doc_hash + ") hashables: " + hashables + "\nRegenrated hash: " + hash, "app", "trace");
  return hash;
}

void INameBindDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{

  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  // handle iName pgp binding costs
  if (block_inspect_container->m_block_alter_treasury_incomes.keys().contains("TP_INAME_BIND"))
  {
    CLog::log("importing FleNS-bind payments: " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_block_alter_treasury_incomes["TP_INAME_BIND"]), "trx", "trace");

    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_INAME_BIND"])
    {
      // if proposal costs was payed by Pledging & a lending transaction
      bool doc_cost_is_payed = true;

      CDocHashT bind_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];
//      auto[inx_, bind_doc] = block->getDocumentByHash(bind_hash);

      if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[bind_hash] = {"supporter transaction(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") for iName-bind is rejected because of doublespending", false};
      }

      if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"The iName-bind costs is not supported by any trx", false};
      }



      if (doc_cost_is_payed)
      {
        CLog::log("Successfully TP_INAME_BIND Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_INAME_BIND)", "app", "trace");

        QString income_title = "TP_INAME_BIND iName(" + CUtils::hash8c(bind_hash) + ") Trx(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") ";
        cost_payment_status[bind_hash] = {"Done", true};
        TreasuryHandler::insertIncome(
          income_title,
          "TP_INAME_BIND",
          income_title,
          block->m_block_creation_date,
          a_treasury_entry.m_value,
          block->getBlockHash(),
          a_treasury_entry.m_coin);

      } else {
        CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_INAME_BIND)", "sec", "error");
        CLog::log("Failed cost_payment_status: " + UTXOImportDataContainer::dumpMe(cost_payment_status[bind_hash]), "sec", "error");
        INameHandler::removeBind(bind_hash); // iNameBindInRelatedBlock.removeBindingBecauseOfPaymentsFail(bind_hash);

      }
    }
  }

  block_inspect_container->m_cost_payment_status["TP_INAME_BIND"] = cost_payment_status;
}
