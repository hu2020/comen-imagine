#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/contracts/flens/iname_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "iname_reg_document.h"

INameRegDocument::INameRegDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

INameRegDocument::~INameRegDocument()
{

}

bool INameRegDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  if (obj.value("iName").toString() != "")
    m_iname_string = obj.value("iName").toString();

  if (obj.value("iNOwner").toString() != "")
    m_iname_owner = obj.value("iNOwner").toString();


  return true;
}

QJsonObject INameRegDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document["iName"] = m_iname_string;
  document["iNOwner"] = m_iname_owner;

  return document;
}


QString INameRegDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject document_json = exportDocToJson(ext_info_in_document);

  if (ext_info_in_document)
    document_json["dExtInfo"] = SignatureStructureHandler::compactUnlockersArray(document_json["dExtInfo"].toArray());

//  // recaluculate block final length
//  document_json["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(document_json).length());

  CLog::log("8 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(document_json).length()) + " serialized document: " + CUtils::serializeJson(document_json), "app", "trace");

  return CUtils::serializeJson(document_json);
}

// js name was calcCostDINameRegReq
std::tuple<bool, CMPAIValueT> INameRegDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;

  CMPAIValueT the_cost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);

  CMPAIValueT pure_cost = INameHandler::getPureINameRegCost(m_iname_string);
  the_cost += pure_cost;

  if (stage == CConsts::STAGES::Creating)
  {
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc custom post the_cost + machine interest(" + CUtils::sepNum(the_cost) +" micro PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }
  return {true, CUtils::CFloor(the_cost)};
}

  // js name was recordINameInDAG
bool INameRegDocument::applyDocFirstImpact(const Block& block) const
{
  bool res = INameHandler::recordINameInDAG(block, this);
  if (res)
  {
    CGUI::refresh_inames_info();
  }
  return res;
}


bool INameRegDocument::hasSignable() const
{
  return true;
}

// old name was getSignMsgDFleNS
QString INameRegDocument::getDocSignMsg() const
{
  QString iName = INameHandler::normalizeIName(m_iname_string);
  QString signables = "{";
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\"," ;
  signables += "\"dClass\":\"" + m_doc_class + "\"," ;
  signables += "\"dType\":\"" + m_doc_type + "\"," ;
  signables += "\"dVer\":\"" + m_doc_version + "\"," ;
  signables += "\"iName\":\"" + iName + "\"," ;
  signables += "\"iNOwner\":\"" + m_iname_owner + "\"}" ;

  QString sign_message = CCrypto::keccak256(signables);
  sign_message = sign_message.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  CLog::log("IName reg sign_message(" + sign_message + ") signables: " + signables + " ", "app", "trace");
  return sign_message;
}

bool INameRegDocument::veridfyDocSignature() const
{

  QJsonObject dExtInfo = m_doc_ext_info[0].toObject();
  auto unlock_set = dExtInfo.value("uSet").toObject();

  bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
    unlock_set,
    m_iname_owner);

  if (!is_valid_unlock)
  {
    CLog::log("Invalid creator signature structure on iname(" + m_doc_hash + ")! ", "sec", "error");
    return false;
  }

  // iname signature & permission validate check
  QString sign_message = getDocSignMsg();
  QJsonArray signatures = m_doc_ext_info[0].toObject().value("signatures").toArray();
  for (CSigIndexT signature_index = 0; signature_index < signatures.size(); signature_index++)
  {
    QString a_signature = signatures[signature_index].toString();
    try {
      bool verifyRes = CCrypto::ECDSAVerifysignature(
        unlock_set.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
        sign_message,
        a_signature);
      if (!verifyRes)
      {
        CLog::log("Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
        return false;
      }
    } catch (std::exception) {
      CLog::log("Exception! Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
      return false;
    }
  }
  return true;
}


QString INameRegDocument::getDocHashableString() const
{
  QString iname = INameHandler::normalizeIName(m_iname_string);
  QString hahsables = "{";
  hahsables += "\"dCDate\":\"" + m_doc_creation_date + "\"," ;
  hahsables += "\"dClass\":\"" + m_doc_class + "\"," ;
  hahsables += "\"dExtHash\":" + m_doc_ext_hash + ",";
  hahsables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\",";
  hahsables += "\"dType\":\"" + m_doc_type + "\"," ;
  hahsables += "\"dVer\":\"" + m_doc_version + "\"," ;
  hahsables += "\"iName\":\"" + iname + "\"," ;
  hahsables += "\"iNOwner\":\"" + m_iname_owner + "\"}" ;
  return hahsables;
}

// old name was calcHashDINameRegReqS
QString INameRegDocument::calcDocHash() const
{
  CLog::log("calc HashDPolling iname reg: " + safeStringifyDoc(), "app", "trace");
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("\nHashable string for iname reg doc doc(" + m_doc_type + " / " +
    m_doc_class + ") hash(" + hash + ")" + hashables, "app", "trace");
  return hash;
}

QString INameRegDocument::calcDocExtInfoHash() const
{
  QString hashables = "{";
  hashables += "\"signatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("signatures").toArray()) + ",";
  hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("uSet").toObject()) + "}";
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("Reg iName Ext Root Hash Hashables Doc(" + m_doc_hash + ") hashables: " + hashables + "\nRegenrated hash: " + hash, "app", "trace");
  return hash;
}

void INameRegDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{

  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  // handle iName register costs

  if (block_inspect_container->m_block_alter_treasury_incomes.contains("TP_INAME_REG"))
  {
    CLog::log("importing FleNS payments " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_block_alter_treasury_incomes["TP_INAME_REG"]), "trx", "trace");

    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_INAME_REG"])
    {
      // if proposal costs was payed by Pledging & a lending transaction
      bool doc_cost_is_payed = true;

      CDocHashT iName_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];

      if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[iName_hash] = {"supporter transaction(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") for reg-iname is rejected because of doublespending", false};
      }

      if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"The iName costs is not supported by any trx", false};
      }


      if (doc_cost_is_payed)
      {
        CLog::log("Successfully TP_INAME_REG Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_INAME_REG)", "app", "trace");

        QString title =  "TP_INAME_REG iName(" + CUtils::hash8c(iName_hash) + ") Trx(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") ";
        cost_payment_status[iName_hash].m_message = "iName reg Cost imported to treasury succsessfully.";
        TreasuryHandler::insertIncome(
          title,
          "TP_INAME_REG",
          title,
          block->m_block_creation_date,
          a_treasury_entry.m_value,
          block->getBlockHash(),
          a_treasury_entry.m_coin);

      } else {
        CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_INAME_REG)", "sec", "error");
        CLog::log("cost_payment_status not payed: " + UTXOImportDataContainer::dumpMe(cost_payment_status[iName_hash]), "sec", "error");

        INameHandler::removeINameByHash(iName_hash);  // iNameInRelatedBlock.removeINameBecauseOfPaymentsFail(iName_hash);

      }
    }
  }

  block_inspect_container->m_cost_payment_status["TP_INAME_REG"] = cost_payment_status;
}

std::tuple<bool, QString> INameRegDocument::signINameRegReq()
{
  QString msg;

  QVDRecordsT addresses_details = Wallet::getAddressesInfo({m_iname_owner});
  if (addresses_details.size() != 1)
    return {false, "The owner address " + m_iname_owner + " is not controlled by wallet"};

  QJsonObject addrDtl = CUtils::parseToJsonObj(addresses_details[0].value("wa_detail").toString());
  QStringList signatures {};
  QJsonObject dExtInfo {};


  CSigIndexT unlocker_index = 0;  // TODO: the unlocker should be customizable
  QJsonObject unlock_set = addrDtl.value("uSets").toArray()[unlocker_index].toObject();

  QString sign_message = getDocSignMsg();
  QJsonArray sSets = unlock_set.value("sSets").toArray();
  for (CSigIndexT inx = 0; inx < sSets.size(); inx++)
  {
    auto[signing_res, signature_hex, signature] = CCrypto::ECDSAsignMessage(
      addrDtl.value("the_private_keys").toObject()[unlock_set.value("salt").toString()].toArray()[inx].toString(),
      sign_message);
    if (!signing_res)
    {
      msg = "Failed in sign pledge, Salt(" + unlock_set.value("salt").toString() + ")";
      CLog::log(msg, "app", "error");
      return {false, msg};
    }
    signatures.append(QString::fromStdString(signature_hex));
  }
  if (signatures.size() == 0)
  {
    msg = "The FleNS couldn't be signed";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  m_doc_ext_info = QJsonArray{QJsonObject {
    {"uSet", unlock_set},
    {"signatures", CUtils::convertQStringListToJSonArray(signatures)}}};

  return {true, "done"};
}
