#ifndef INAMEBINDDOCUMENT_H
#define INAMEBINDDOCUMENT_H

class Document;

class INameBindDocument: public Document
{
public:
  INameBindDocument(const QJsonObject& obj);
  ~INameBindDocument();

//  QString m_nbInfo = "";
  QString m_iname_binding_type = "";
  QString m_iname_binding_title = "";
  QString m_iname_binding_comment = "";

  QJsonObject m_pgp_binging_info {};
  CDocHashT m_pgp_binding_iname_hash = "";
  CAddressT m_pgp_binding_iname_owner = "";
  QString m_pgp_binding_label = "";
  QString m_pgp_binding_comment = "";
  QString m_pgp_binding_version = "";
  QString m_pgp_binding_pub_key = "";

  bool setByJsonObj(const QJsonObject& obj) override;

  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;  // calcTrxExtRootHash

  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;

  QJsonObject exportBindInfoToJSon() const;

//  QString getRef() const override;
//  QString getRefType() const;


  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;

  bool applyDocFirstImpact(const Block& block) const override;

  bool hasSignable() const override;
  QString getDocSignMsg() const override;
  bool veridfyDocSignature() const override;

  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);

};

#endif // INAMEBINDDOCUMENT_H
