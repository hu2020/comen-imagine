#ifndef INAMEREGDOCUMENT_H
#define INAMEREGDOCUMENT_H

class Document;

class INameRegDocument: public Document
{
public:
  INameRegDocument(const QJsonObject& obj);
  ~INameRegDocument();

  QString m_iname_string = "";
  QString m_iname_owner = "";

  bool setByJsonObj(const QJsonObject& obj) override;

  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;  // calcTrxExtRootHash

  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;
  bool applyDocFirstImpact(const Block& block) const override;

  bool hasSignable() const override;
  QString getDocSignMsg() const override;
  bool veridfyDocSignature() const override;

  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);

  std::tuple<bool, QString> signINameRegReq();
};


#endif // INAMEREGDOCUMENT_H
