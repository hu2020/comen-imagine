#include "stable.h"

#include "lib/transactions/trx_utils.h"
#include "lib/ccrypto.h"


#include "rp_docdocument.h"



RepaymentDocument::RepaymentDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

RepaymentDocument::~RepaymentDocument()
{
  deleteInputs();
  deleteOutputs();
}

bool RepaymentDocument::deleteInputs()
{
  for (TInput* an_input: m_inputs)
    delete an_input;
  return true;
}

bool RepaymentDocument::deleteOutputs()
{
  for (TOutput* an_output: m_outputs)
    delete an_output;
  return true;
}

bool RepaymentDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  // maybe some drived class assigning

  if (obj.value("inputs").toArray().size() > 0)
    setDocumentInputs(obj.value("inputs"));

  if (obj.value("outputs").toArray().size() > 0)
    setDocumentOutputs(obj.value("outputs"));

  return true;
}

QString RepaymentDocument::getRef() const
{
  return "";
}

std::tuple<bool, QJsonArray> RepaymentDocument::exportInputsToJson() const
{
  QJsonArray inputs {};
  for (TInput* an_input: m_inputs)
    inputs.append(QJsonArray{
      an_input->m_transaction_hash,
      an_input->m_output_index});
  return {true, inputs};
}

std::tuple<bool, QJsonArray> RepaymentDocument::exportOutputsToJson() const
{
  QJsonArray outputs {};
  for (TOutput* an_output: m_outputs)
    outputs.append(QJsonArray{
      an_output->m_address,
      QVariant::fromValue(an_output->m_amount).toDouble()});
  return {true, outputs};
}

QJsonObject RepaymentDocument::getRepayDocTpl()
{
  return QJsonObject {
    {"dHash", "0000000000000000000000000000000000000000000000000000000000000000"},
    {"dType", CConsts::DOC_TYPES::RpDoc},
    {"dClass", CConsts::DOC_TYPES::RpDoc},
    {"dVer", "0.0.0"},
    {"cycle", ""}, // 'yyyy-mm-dd am' / 'yyyy-mm-dd pm'
    {"inputs", QJsonArray{}},
    {"outputs", QJsonArray{}}};
}

QJsonObject RepaymentDocument::calcRepaymentDetails(
  const QString& coinbase_trx_hash,
  const COutputIndexT& output_index,
  const CMPAIValueT& coinbased_output_value,
  const GRecordsT& pledged_accounts_info,
  const CAddressT& the_pledged_account)
{
  CLog::log("calc RepaymentDetails args: ${utils.stringify(args)}", "cb", "info");

  QJsonObject a_repayback_doc = {
    {"input", QJsonArray {coinbase_trx_hash, output_index}},
    {"outputs", {}}};

  CMPAIValueT real_income = coinbased_output_value;
  // create a repayment block cut repayment parts from income
  // repaymentInputs.push(refLoc);

  // order pledges by register date and cut by order
  CMPAIValueT totalToBeCut = 0;
  QHash<QString, QVariant> pleged_accouns_by_time = {};
  for (QVDicT aPldg: pledged_accounts_info[the_pledged_account])
  {
    QString key = aPldg.value("pgd_activate_date").toString() + "_" + aPldg.value("pgd_hash").toString(); // make sure for all machine we have a certain order even for same date pledge activated contract
    pleged_accouns_by_time[key] = aPldg;
    totalToBeCut += aPldg.value("pgd_repayment_amount").toDouble();
  }
  CLog::log("Pledged Account income per cycle: Account(" + CUtils::shortBech16(the_pledged_account) + ") Income(" + QString::number(real_income) + ") total To Be Cut(" + QString::number(totalToBeCut) + ")", "trx", "trace");

  // cutting
  QStringList keys = pleged_accouns_by_time.keys();
  keys.sort();
  for (QString a_repay: keys)
  {
    // TODO implement an efficient way to check if repayments alredy done? in this case close contract
    CMPAIValueT toCut = static_cast<CMPAIValueT>(pleged_accouns_by_time[a_repay].toHash().value("pgd_repayment_amount").toUInt());
    CAddressT repaymentAccount = pleged_accouns_by_time[a_repay].toHash().value("pgd_pledgee").toString();
    if (real_income >= toCut)
    {
      CLog::log("Pledged Account cuttings. Account(" + CUtils::shortBech16(the_pledged_account) + " cutting " + CUtils::sepNum(toCut) + " PAIs repayments to pay(" + CUtils::shortBech16(repaymentAccount) + ")", "trx", "trace");
      auto tmp_outputs = a_repayback_doc["outputs"].toArray();
      tmp_outputs.append(QJsonArray {repaymentAccount, QVariant::fromValue(toCut).toDouble()});
      a_repayback_doc["outputs"] = tmp_outputs;
      real_income = real_income - toCut;

    } else {
      CLog::log("Pledged Account(" + CUtils::shortBech16(the_pledged_account) + ") cutting " + CUtils::sepNum(real_income) + " mcPAIs (remained PAIs) which not covers completely repayments to (" + CUtils::shortBech16(repaymentAccount) + ")", "trx", "warning");
      // repaymentOutputs.push([repaymentAccount, real_income]);
      auto tmp_outputs = a_repayback_doc["outputs"].toArray();
      tmp_outputs.append(QJsonArray {repaymentAccount, QVariant::fromValue(real_income).toDouble()});
      a_repayback_doc["outputs"] = tmp_outputs;
      real_income = 0;

    }
  }

  // pay what remains, to account itself
  if (real_income > 0)
  {
    CLog::log("Pledged Account(" + CUtils::shortBech16(the_pledged_account) + ") get paying " + CUtils::sepNum(real_income) + " mc PAIs to pledger after cut all repayments", "trx", "info");
    // repaymentOutputs.push([the_pledged_account, real_income]);
    auto tmp_outputs = a_repayback_doc["outputs"].toArray();
    tmp_outputs.append(QJsonArray {the_pledged_account, QVariant::fromValue(real_income).toDouble()});
    a_repayback_doc["outputs"] = tmp_outputs;
  }

  return a_repayback_doc;

}

QJsonObject RepaymentDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document.remove("dCDate");
  document.remove("dLen");

  // recaluculate doc final length
  document["cycle"] = m_doc_cycle;

  return document;
}


QString RepaymentDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject Jdoc = exportDocToJson(ext_info_in_document);


  CLog::log("12 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(Jdoc).length()) + " serialized document: " + CUtils::serializeJson(Jdoc), "app", "trace");

  return CUtils::serializeJson(Jdoc);
}

CDocHashT RepaymentDocument::calcDocHash() const
{
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256Dbl(hashables); // NOTE: absolutely using double hash for more security
  CLog::log("Hashable string for repayback block, doc hash(" + hash + ") hashables:" + hashables, "app", "trace");
  return hash;
}


QString RepaymentDocument::getDocHashableString() const
{
  // in order to have almost same hash! we sort the attribiutes alphabeticaly
  QString hashable_doc = "{";
  hashable_doc += "\"cycle\":\"" + m_doc_cycle + "\",";
  hashable_doc += "\"dClass\":\"" + m_doc_class + "\",";
  hashable_doc += "\"dType\":\"" + m_doc_type + "\",";

  hashable_doc += "\"inputs\":" + SignatureStructureHandler::stringifyInputs(m_inputs) + ",";
  hashable_doc += "\"outputs\":" + SignatureStructureHandler::stringifyOutputs(m_outputs) + "}";

  return hashable_doc;
}


QString RepaymentDocument::getDocHashableString2(RepaymentDocument* a_doc)
{
  // in order to have almost same hash! we sort the attribiutes alphabeticaly
  QString hashable_doc = "{";
  hashable_doc += "\"hash\":\"" + a_doc->m_doc_hash + "\",";
  hashable_doc += "\"dType\":\"" + a_doc->m_doc_type + "\",";
  hashable_doc += "\"dClass\":\"" + a_doc->m_doc_class + "\",";
  hashable_doc += "\"dVer\":\"" + a_doc->m_doc_version + "\",";
  hashable_doc += "\"cycle\":\"" + a_doc->m_doc_cycle + "\",";

  hashable_doc += "\"inputs\":" + SignatureStructureHandler::stringifyInputs(a_doc->m_inputs) + ",";
  hashable_doc += "\"outputs\":" + SignatureStructureHandler::stringifyOutputs(a_doc->m_outputs) + "}";

  return hashable_doc;
}


bool RepaymentDocument::setDocumentInputs(const QJsonValue& obj)
{
  QJsonArray inputs = obj.toArray();
  for(QJsonValueRef an_input: inputs)
  {
    QJsonArray io = an_input.toArray();
    TInput* i  = new TInput({io[0].toString(), static_cast<COutputIndexT>(io[1].toVariant().toDouble())});
    m_inputs.push_back(i);
  }
  return true;
}

bool RepaymentDocument::setDocumentOutputs(const QJsonValue& obj)
{
  QJsonArray outputs = obj.toArray();
  for(QJsonValueRef an_output: outputs)
  {
    QJsonArray oo = an_output.toArray();
    TOutput *o  = new TOutput({oo[0].toString(), static_cast<CMPAIValueT>(oo[1].toDouble())});
    m_outputs.push_back(o);
  }
  return true;
}

