#ifndef RPDOCDOCUMENT_H
#define RPDOCDOCUMENT_H

class Document;
#include "document.h"

class RepaymentDocument : public Document
{
public:
  RepaymentDocument(){};
  RepaymentDocument(const QJsonObject& obj);
  ~RepaymentDocument();

  QString m_doc_cycle = "";
  std::vector<TInput*> m_inputs = {};
  std::vector<TOutput*> m_outputs = {};

  bool setByJsonObj(const QJsonObject& obj) override;
  bool setDocumentInputs(const QJsonValue& obj) override;
  bool setDocumentOutputs(const QJsonValue& obj) override;
  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString getRef() const override;

  bool deleteInputs() override;
  bool deleteOutputs() override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;

  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

//  BlockLenT calcBlockLength(const QJsonObject &block_obj) const override;

  QString getDocHashableString() const override;
  static QString getDocHashableString2(RepaymentDocument* a_doc); // customized for calculating full-block hash. FIXME: upgrade version and remove full-document from full-block hash. instead put the doc root hash.
  CDocHashT calcDocHash() const override;

  static QJsonObject getRepayDocTpl();

  static QJsonObject calcRepaymentDetails(
    const QString& coinbaseTrxHash,
    const COutputIndexT& outputIndex,
    const CMPAIValueT& coinbasedOutputValue,
    const GRecordsT& pledgedAccountsInfo,
    const CAddressT& the_pledged_account);


};

#endif // RPDOCDOCUMENT_H
