#ifndef BALLOTDOCUMENT_H
#define BALLOTDOCUMENT_H


class BallotDocument: public Document
{
public:
  BallotDocument(const QJsonObject& obj);
  ~BallotDocument();

  CVoteT m_vote = 0;
  CAddressT m_voter = "";
  QString m_doc_comment = "";


  bool setByJsonObj(const QJsonObject& obj) override;

  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;  // calcTrxExtRootHash

  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;

  QString getRef() const override;
//  QString getRefType() const;


  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;

  bool applyDocFirstImpact(const Block& block) const override;

  bool hasSignable() const override;
  QString getDocToBeSignedHash() const override;
  QString getDocSignMsg() const override;
  bool veridfyDocSignature() const override;

  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);

};

#endif // BALLOTDOCUMENT_H
