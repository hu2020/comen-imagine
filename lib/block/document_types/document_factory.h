#ifndef DOCUMENTFACTORY_H
#define DOCUMENTFACTORY_H


class DocumentFactory
{
public:
  DocumentFactory();
  static Document* create(
    const QJsonObject &obj,
    const Block* block = nullptr,
    const int64_t doc_index = -1);
};

#endif // DOCUMENTFACTORY_H
