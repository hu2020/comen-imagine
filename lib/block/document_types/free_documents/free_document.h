#ifndef FREEDOCUMENT_H
#define FREEDOCUMENT_H

class Document;
#include "lib/block/document_types/document.h"

class FreeDocument : public Document
{
public:
  FreeDocument(const QJsonObject& obj);
  ~FreeDocument();

  QString m_iname = "";

  QString m_content_language = CConsts::DEFAULT_LANG;

  //  -  -  -  Demos/Agora part
  QString m_free_doc_version = "";
  QString m_doc_signer = "";
  QJsonObject m_doc_attributes = {};

  bool m_is_pre_proposal = false;
  QJsonObject m_pre_proposal_details = {};
  QString m_agora_hash = "";  // in case of pre proposal documents as well as demos/sgora docs

  QString m_parent_agora = "";
  QString m_agora_opinion = "";
  QString m_agora_reply = ""; // new agora post reply (napReply)
  int8_t m_agora_reply_point = 0;  // napReplyPoint

  //  -  -  -  wiki part
  QString m_doc_content = "";
  QString m_wiki_page_hash = "";  // keccack256(iname + "/" + title)

  //  -  -  -  file part
  QString m_file_label = "";
  QString m_file_mime_type = "";
  QString m_file_name = "";
  QString m_file_content = "";  // file content should be byte stream or something more efficient


  bool setByJsonObj(const QJsonObject& obj) override;
  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString calcDocExtInfoHash() const override;  // calcTrxExtRootHash
  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;


  QString getDocHashableString() const override;
  CDocHashT calcDocHash() const override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;
  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

  bool applyDocFirstImpact(const Block& block) const override;

  bool deleteInputs() override;
  bool deleteOutputs() override;

  bool hasSignable() const override;
  QString getDocSignMsg() const override;
  bool veridfyDocSignature() const override;
  GenRes userAllowedForThisDoc(const Block* block) const;
  GenRes customValidateDoc(const Block* block) const override;

  QString getRef() const override;

  //  -  -  -  base class methods
  bool validatefDocSigner(
    const QString& stage,
    const QString& sign_message) const;

  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);


  //  -  -  -  Demos posts
  QString getSignMsgDNewPost() const;
  QString getSignMsgDRegWikiPage() const;
  QString getDemosPostHashableString() const;
  bool customValidateAgoraPost() const;

  //  -  -  -  wiki part
  CDocHashT getWikiHashableString() const;


};

#endif // FREEDOCUMENT_H
