#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/document_types/document.h"
#include "lib/services/free_docs/free_doc_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "free_document.h"

FreeDocument::FreeDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

FreeDocument::~FreeDocument()
{
  deleteInputs();
  deleteOutputs();
}

bool FreeDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  if (obj.value("iName").toString() != "")
    m_iname = obj.value("iName").toString();

  if (obj.value("dSigner").toString() != "")
    m_doc_signer = obj.value("dSigner").toString();

  if (obj.value("fVer").toString() != "")
    m_free_doc_version = obj.value("fVer").toString();

  if (obj.value("cLang").toString() != "")
    m_content_language = obj.value("cLang").toString();

  if (obj.value("opinion").toString() != "")
    m_agora_opinion = obj.value("opinion").toString();

  if (obj.value("dAttrs").toObject().keys().size() > 0)
    m_doc_attributes = obj.value("dAttrs").toObject();

  m_doc_title_hash = CCrypto::convertTitleToHash(m_doc_title);


  if (m_doc_class == CConsts::FPOST_CLASSES::DMS_RegAgora)
  {
    // create an Agora
    m_agora_hash = obj.value("agHash").toString();
    m_parent_agora = obj.value("parent").toString();

  }
  else if (m_doc_class == CConsts::FPOST_CLASSES::DMS_Post)
  {
    m_agora_hash = obj.value("agHash").toString();
    m_agora_reply = obj.value("reply").toString();
    m_agora_reply_point = obj.value("replyPoint").toInt();

  }
  else if (QStringList{CConsts::FPOST_CLASSES::WK_CreatePage,
           CConsts::FPOST_CLASSES::WK_EditPage}.contains(m_doc_class))
  {
    m_wiki_page_hash = CCrypto::keccak256(m_iname + "/" + m_doc_title);

    if (obj.value("dContent").toString() != "")
      m_doc_content = BlockUtils::unwrapSafeContentForDB(obj.value("dContent").toString()).content;



  }
  else if (m_doc_class == CConsts::FPOST_CLASSES::File)
  {
    m_file_label = obj.value("cPFileLabel").toString();
    m_file_mime_type = obj.value("cPFileMimetype").toString();
    m_file_name = obj.value("cPFileName").toString();
    m_file_content = obj.value("cPFileContent").toString();

  }

  if (m_doc_attributes.keys().size() > 0)
  {
    if (m_doc_attributes.keys().contains("isPreProposal") &&
       m_doc_attributes.value("isPreProposal").toString() == CConsts::YES)
    {
      m_is_pre_proposal = true;
      m_pre_proposal_details = obj.value("opinion").toObject();
      m_agora_hash = obj.value("agHash").toString();
    }
  }

  return true;
}

QString FreeDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject document = exportDocToJson(ext_info_in_document);

  QString out = CUtils::serializeJson(document);

  CLog::log("6 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + CUtils::sepNum(out.length()) + " serialized:" + out, "app", "trace");
  return out;	
}

// js name was customHash
QString FreeDocument::getDocHashableString() const
{
  QString hashables = "";

  // TODO: change design pattern in order to get rid of these if elses
  if (m_doc_class == CConsts::FPOST_CLASSES::DMS_Post)
  {
    return getDemosPostHashableString();
  } else if (QStringList{CConsts::FPOST_CLASSES::WK_EditPage, CConsts::FPOST_CLASSES::WK_CreatePage}.contains(m_doc_class))
  {
    return getWikiHashableString();

  }



//  }else if (m_doc_class == CConsts::FPOST_CLASSES::File) {

//    case iConsts.FPOST_CLASSES.File:
//        return FreeDocumentHandler.fileDocHandler.calcHashDCustomFile(args)
//        break;

//    case iConsts.FPOST_CLASSES.DMS_RegAgora:
//        return FreeDocumentHandler.demos.calcHashDAgoraRegReq(args)
//        break;
  return "-=-=-=-=-=";
}

QString FreeDocument::calcDocHash() const
{
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("\nHashable string for free doc(" + m_doc_type + " / " + m_doc_class +
            ") hash(" + hash + ")" + hashables, "app", "trace");
  return hash;
}


bool FreeDocument::validatefDocSigner(
  const QString& stage,
  const QString& sign_message) const
{
  if (!CCrypto::isValidBech32(m_doc_signer))
  {
    CLog::log("invalid signer/owner for free document signer(" + CUtils::shortBech16(m_doc_signer) + ") type(" + m_doc_class + ") hash:" + CUtils::hash8c(m_doc_hash) + ")", "sec", "error");
    return false;
  }

  QJsonObject unlock_set = m_doc_ext_info[0].toObject().value("uSet").toObject();
  // test unlock structure & signature
  bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
    unlock_set,
    m_doc_signer);
  if (!is_valid_unlock)
  {
    CLog::log("Invalid! given freedoc(file sender signature) unlock structure for req stage(" + stage + ") signer(" +CUtils::shortBech16(m_doc_signer) + ") type(" + m_doc_class + ") hash:" + CUtils::hash8c(m_doc_hash) + ") unlock_set: " + CUtils::dumpIt(unlock_set), "sec", "error");
    return false;
  }

  // signature & permission validate check
  QJsonArray signatures = m_doc_ext_info[0].toObject().value("signatures").toArray();
  for (CSigIndexT signature_index = 0; signature_index < signatures.size(); signature_index++)
  {
    QString a_signature = signatures[signature_index].toString();
    try {
      bool verify_res = CCrypto::ECDSAVerifysignature(
        unlock_set.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
        sign_message,
        a_signature);
      if (!verify_res)
      {
        CLog::log("Block creating has invalid signature! stage(" + stage + ") signer(" +CUtils::shortBech16(m_doc_signer) + ") type(" + m_doc_class + ") hash:" + CUtils::hash8c(m_doc_hash) + ") unlock_set: " + CUtils::dumpIt(unlock_set), "sec", "error");
        return false;
      }
    } catch (std::exception) {
      CLog::log("Error in verify FreeDoc Signature! stage(" + stage + ") signer(" +CUtils::shortBech16(m_doc_signer) + ") type(" + m_doc_class + ") hash:" + CUtils::hash8c(m_doc_hash) + ") unlock_set: " + CUtils::dumpIt(unlock_set), "sec", "error");
      return false;
    }
  }
  return true;
}

bool FreeDocument::customValidateAgoraPost() const
{
  if (m_agora_reply_point > 100)
    return false;

  if (m_agora_reply_point < -100)
    return false;

  return true;
}

GenRes FreeDocument::userAllowedForThisDoc(const Block* block) const
{
  QString msg;
  DNASharePercentT min_needed_shares = 0.0;
  CDateT block_creation_date = block->m_block_creation_date;
  if (QStringList{CConsts::FPOST_CLASSES::DMS_Post, CConsts::FPOST_CLASSES::DMS_RegAgora}.contains(m_doc_class))
  {
    min_needed_shares = SocietyRules::getMinShareToAllowedDemos(block_creation_date);
  } else if (QStringList{CConsts::FPOST_CLASSES::WK_CreatePage, CConsts::FPOST_CLASSES::WK_EditPage}.contains(m_doc_class))
  {
    min_needed_shares = SocietyRules::getMinShareToAllowedWiki(block_creation_date);
  }

  auto[shares_, signer_shares_percentage] = DNAHandler::getAnAddressShares(m_doc_signer, block_creation_date);
  if (signer_shares_percentage >= min_needed_shares)
      return {true, ""};

  msg = "The signer has not enough shares to participate! signer(" +CUtils::shortBech16(m_doc_signer)+ ") action(" +m_doc_class+ ")";
  CLog::log(msg, "sec", "error");
  return {false, msg};
}

GenRes FreeDocument::customValidateDoc(const Block* block) const
{
  GenRes user_allowed_to_this_doc = userAllowedForThisDoc(block);
  if (!user_allowed_to_this_doc.status)
    return {false, user_allowed_to_this_doc.msg};

  if (m_doc_class == CConsts::FPOST_CLASSES::DMS_Post)
    return customValidateAgoraPost();

  return {true, "is valid"};
}

// old name was calcCostDCustomPost
std::tuple<bool, CMPAIValueT> FreeDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;
  CMPAIValueT the_cost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);

  CLog::log("calc cutom post the_cost(" + CUtils::microPAIToPAI6(the_cost) +" micro PAIs) Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");

  if (stage == CConsts::STAGES::Creating)
  {
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc cutom post the_cost + machine interest(" + CUtils::microPAIToPAI6(the_cost) +" micro PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }
  return {true, CUtils::CFloor(the_cost)};
}

QString FreeDocument::calcDocExtInfoHash() const //calcTrxExtRootHash()
{
  QString hashables = "{";
  hashables += "\"signatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("signatures").toArray()) + ",";
  hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("uSet").toObject()) + "}";
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("Free Doc Ext Root Hash Hashables Doc(" + m_doc_hash + ") hashables: " + hashables + "\nRegenrated hash: " + hash, "app", "trace");
  return hash;
}


QJsonObject FreeDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  if (m_doc_attributes.keys().size() > 0)
    document["dAttrs"] = m_doc_attributes;

  if (m_free_doc_version != "")
    document["fVer"] = m_free_doc_version;

  if (m_doc_signer != "")
    document["dSigner"] = m_doc_signer;

  if (m_iname != "")
    document["iName"] = m_iname;

  if (m_agora_opinion != "")
    document["opinion"] = m_agora_opinion;

  if (m_content_language != "")
    document["cLang"] = m_content_language;

  if (m_doc_class == CConsts::FPOST_CLASSES::DMS_RegAgora)
  {
    // create an Agora
    if (m_agora_hash != "")
      document["agHash"] = m_agora_hash;

    if (m_agora_hash != "")
      document["parent"] = m_parent_agora;

  }
  else if (m_doc_class == CConsts::FPOST_CLASSES::DMS_Post)
  {
    if (m_agora_hash != "")
      document["agHash"] = m_agora_hash;

    if (m_agora_reply != "")
    {
      document["reply"] = m_agora_reply;
      document["replyPoint"] = m_agora_reply_point;
    }

  }
  else if (QStringList{CConsts::FPOST_CLASSES::WK_CreatePage,
           CConsts::FPOST_CLASSES::WK_EditPage}.contains(m_doc_class))
  {
    if (m_doc_content != "")
      document["dContent"] = BlockUtils::wrapSafeContentForDB(m_doc_content).content;

  }
  else if (m_doc_class == CConsts::FPOST_CLASSES::File)
  {
    //m_file_label = obj.value("cPFileLabel").toString();
    //m_file_mime_type = obj.value("cPFileMimetype").toString();
    //m_file_name = obj.value("cPFileName").toString();
    //m_file_content = obj.value("cPFileContent").toString();

  }


  if (m_is_pre_proposal)
  {
    if (m_agora_hash != "")
      document["agHash"] = m_agora_hash;

    document["opinion"] = m_pre_proposal_details;
  }

  if (ext_info_in_document)
    document["dExtInfo"] = SignatureStructureHandler::compactUnlockersArray(document["dExtInfo"].toArray());

  return document;
}

std::tuple<bool, QJsonArray> FreeDocument::exportInputsToJson() const
{
  return {false, QJsonArray {}};  // free dosc haven't inputs
}

std::tuple<bool, QJsonArray> FreeDocument::exportOutputsToJson() const
{
  return {false, QJsonArray {}};  // free dosc haven't outputs
}

bool FreeDocument::deleteInputs()
{
  return true;
}

bool FreeDocument::deleteOutputs()
{
  return true;
}

QString FreeDocument::getRef() const
{
  return "";
}

// js name was customValidate
bool FreeDocument::veridfyDocSignature() const
{

    QJsonObject dExtInfo = m_doc_ext_info[0].toObject();
    auto unlockSet = dExtInfo.value("uSet").toObject();

    bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
      unlockSet,
      m_doc_signer);

    if (!is_valid_unlock)
    {
      CLog::log("Invalid creator signature structure on FPost(" + CUtils::hash8c(m_doc_hash) + ") class(" + m_doc_class + ")! ", "sec", "error");
      return false;
    }

    // ballot signature & permission validate check
    QString sign_message = getDocSignMsg();
    QJsonArray signatures = m_doc_ext_info[0].toObject().value("signatures").toArray();
    for (CSigIndexT signature_index = 0; signature_index < signatures.size(); signature_index++)
    {
      QString a_signature = signatures[signature_index].toString();
      try {
        bool verifyRes = CCrypto::ECDSAVerifysignature(
          unlockSet.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
          sign_message,
          a_signature);
        if (!verifyRes)
        {
          CLog::log("Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
          return false;
        }
      } catch (std::exception) {
        CLog::log("Exception! Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
        return false;
      }
    }
    return true;

  // return listener.doCallSync('SASH_custom_validate_FreeDocs', args);
}

bool FreeDocument::hasSignable() const
{
  // maybe switch case by doc class!
  return true;
}

QString FreeDocument::getDocSignMsg() const
{
  // TODO: implemet a good design pattern and rid of these switches
  if (m_doc_class == CConsts::FPOST_CLASSES::DMS_Post) {
    // only signer validate!
    return getSignMsgDNewPost();    // return validatefDocSigner(stage, sign_message);

  }else if (m_doc_class == CConsts::FPOST_CLASSES::File) {
     // return FreeDocumentHandler.fileDocHandler.validateFileDoc(args);

  }else if (m_doc_class == CConsts::FPOST_CLASSES::DMS_RegAgora) {
    // return FreeDocumentHandler.demos.validateRegAgora(args);

  }else if ((m_doc_class == CConsts::FPOST_CLASSES::WK_CreatePage) || (m_doc_class == CConsts::FPOST_CLASSES::WK_EditPage)) {
     return getSignMsgDRegWikiPage();   // FreeDocumentHandler.wiki.pages.validateWikiPage(args);

  }

  return "";
}

// old name was importFDocsCost
void FreeDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{

  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  if (block_inspect_container->m_block_alter_treasury_incomes.keys().contains("TP_FDOC"))
  {
    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_FDOC"])
    {
      // if free-doc costs was payed by a valid trx
      bool doc_cost_is_payed = true;

      if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"supporter transaction(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") for free-doc is rejected because of doublespending", false};
      }

      if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"The free-doc costs is not supported by any trx", false};
      }

      CDocHashT fdoc_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];
//      auto[inx_, theDoc] = block->getDocumentByHash(fdoc_hash);

      CDocHashT supporter_trx = block_inspect_container->m_map_U_trx_ref_to_trx_hash[fdoc_hash];
      QStringList rejected_transactions = block_inspect_container->m_rejected_transactions.keys();
      if (rejected_transactions.contains(supporter_trx) || rejected_transactions.contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[fdoc_hash] = {"supporter transaction is rejected because of doublespending", false};
      }

      if (doc_cost_is_payed)
      {
        CLog::log("Successfully TP_FDOC Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_FDOC)", "app", "trace");

        cost_payment_status[fdoc_hash].m_message = "Free doc Cost imported to treasury succsessfully.";
        QString title = "TP_FDOC fDoc(" + CUtils::hash8c(fdoc_hash) + ")";
        TreasuryHandler::insertIncome(
          title,
          "TP_FDOC",
          title,
          block->m_block_creation_date,
          a_treasury_entry.m_value,
          block->getBlockHash(),
          a_treasury_entry.m_coin);

      } else {
        CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_FDOC)", "sec", "error");
        CLog::log("cost_payment_status not payed: " + UTXOImportDataContainer::dumpMe(cost_payment_status[fdoc_hash]), "sec", "error");

        FreeDocHandler::removeFDoc(fdoc_hash);

      }
    }
  }

  block_inspect_container->m_cost_payment_status["TP_FDOC"] = cost_payment_status;
}

// js name was customPostSpecialTreatments, customTreatments
bool FreeDocument::applyDocFirstImpact(const Block& block) const
{
  return FreeDocHandler::customTreatments(block, this);
}
