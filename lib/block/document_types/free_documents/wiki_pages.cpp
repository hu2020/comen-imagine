#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"

#include "free_document.h"

CDocHashT FreeDocument::getSignMsgDRegWikiPage() const
{
  // as always ordering properties by alphabet
  QString signables = "{";
  signables += "\"cLang\":\"" + m_content_language + "\",";
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  signables += "\"dClass\":\"" + m_doc_class + "\",";
  signables += "\"dContent\":\"" + m_doc_content + "\",";
  signables += "\"dTitle\":\"" + m_doc_title + "\",";
  signables += "\"dType\":\"" + m_doc_type + "\",";
  signables += "\"dVer\":\"" + m_doc_version + "\",";
  signables += "\"fVer\":\"" + m_free_doc_version + "\",";
  signables += "\"iName\":\"" + m_iname + "\",";
  signables += "}";

  CLog::log("prepare new post Wiki signables: " + signables, "app", "trace");
  QString hash = CCrypto::keccak256(signables);
  hash = hash.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  return hash;
}

// js name was calcHashDWikiRegReq
QString FreeDocument::getWikiHashableString() const
{
  QString hashables = "{";
  hashables += "\"cLang\":\"" + m_content_language + "\",";
  hashables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  hashables += "\"dClass\":\"" + m_doc_class + "\",";
  hashables += "\"dContent\":\"" + m_doc_content + "\",";
  hashables += "\"dExtHash\":\"" + m_doc_ext_hash + "\",";
  hashables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\",";
  hashables += "\"dTitle\":\"" + m_doc_title + "\",";
  hashables += "\"dType\":\"" + m_doc_type + "\",";
  hashables += "\"dVer\":\"" + m_doc_version + "\",";
  hashables += "\"fVer\":\"" + m_free_doc_version + "\",";
  hashables += "\"iName\":\"" + m_iname + "\",";
  hashables += "\"signer\":\"" + m_doc_signer + "\"";
  hashables += "}";
  return hashables;
}
