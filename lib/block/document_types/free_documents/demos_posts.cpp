#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"

#include "free_document.h"

CDocHashT FreeDocument::getSignMsgDNewPost() const
{
  // as always ordering properties by alphabet
  QString signables = "{";
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  signables += "\"dClass\":\"" + m_doc_class + "\",";
  signables += "\"opinion\":\"" + m_agora_opinion + "\",";
  signables += "\"dType\":\"" + m_doc_type + "\",";
  signables += "\"dVer\":\"" + m_doc_version + "\"";

  if (m_agora_reply != "")
  {
    signables += ",\"reply\":\"" + m_agora_reply + "\"";
    signables += ",\"replyPoint\":" + QString::number(m_agora_reply_point);
  }

  if (m_doc_attributes.keys().size() > 0)
    signables += ",\"dAttrs\":\"" + CUtils::serializeJson(m_doc_attributes) + "\"";

  signables += "}";

  CLog::log("prepare new post Agora signables: " + signables, "app", "trace");
  QString hash = CCrypto::keccak256(signables);
  hash = hash.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  return hash;
}

QString FreeDocument::getDemosPostHashableString() const
{
  // as always ordering properties by alphabet
  QString hashables = "{";
  hashables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  hashables += "\"dClass\":\"" + m_doc_class + "\",";
  hashables += "\"dExtHash\":\"" + m_doc_ext_hash + "\",";
  hashables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\",";
  hashables += "\"dType\":\"" + m_doc_type + "\",";
  hashables += "\"dVer\":\"" + m_doc_version + "\",";
  hashables += "\"opinion\":\"" + m_agora_opinion + "\"";

  if (m_doc_attributes.keys().size() > 0)
    hashables += ",\"dAttrs\":\"" + CUtils::serializeJson(m_doc_attributes) + "\"";

  if (m_agora_reply != "")
  {
    hashables += ",\"reply\":\"" + m_agora_reply + "\"";
    hashables += ",\"replyPoint\":" + QString::number(m_agora_reply_point);
  }

  hashables += ",\"signer\":\"" + m_doc_signer + "\"";

  hashables += "}";


  CLog::log("prepare new post Agora hashables: " + hashables, "app", "trace");
  return hashables;
}


