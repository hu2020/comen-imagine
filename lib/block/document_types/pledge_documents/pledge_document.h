#ifndef PLEDGEDOCUMENT_H
#define PLEDGEDOCUMENT_H


class PledgeDocument: public Document
{
public:
  PledgeDocument(const QJsonObject& obj);
  ~PledgeDocument();

  CAddressT m_pledger_address = "";
  CAddressT m_pledgee_address = "";
  CAddressT m_arbiter_address = "";

  QString m_pledger_sign_date = "";
  QString m_pledgee_sign_date = "";
  QString m_arbiter_sign_date = "";

  QString m_proposal_ref = "";
  QString m_transaction_ref = ""; // .trx

  UnlockSet m_pledger_unlock_set {};
  UnlockSet m_pledgee_unlock_set {};
  UnlockSet m_arbiter_unlock_set {};

  CMPAIValueT m_redeem_principal = 0;
  double m_redeem_annual_interest = 0.0;
  int64_t m_redeem_repayment_offset = 0;
  CMPAIValueT m_redeem_repayment_amount = 0;
  int64_t m_redeem_repayment_schedule = 0; // payments per year
  int64_t m_redeem_repayments_number = 0; // payments per year


  bool setByJsonObj(const QJsonObject& obj) override;

  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;  // calcTrxExtRootHash

  QString getRef() const override;
  QString getProposalRef() const override;
  QString getPayerTrxLinkBack() const override;

  std::vector<TInput*> getInputs() const override;
  std::vector<TOutput*> getOutputs() const override;

  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;
  bool applyDocFirstImpact(const Block& block) const override;

  bool hasSignable() const override;
  bool veridfyDocSignature() const override;

  QString getSignMsgAsPledger() const;
  QString getSignMsgAsPledgee() const;
  QString extractSignablePartsForPledger() const;
  QString extractSignablePartsForPledgee() const;

  QJsonObject convertRedeemTermsToJson() const;

  // old name was importPledgesCost
  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);

};

#endif // PLEDGEDOCUMENT_H
