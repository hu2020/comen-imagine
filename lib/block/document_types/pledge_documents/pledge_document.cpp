#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/services/contracts/loan/loan_contract_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "pledge_document.h"


PledgeDocument::PledgeDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

PledgeDocument::~PledgeDocument()
{

}

bool PledgeDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  if (obj.value("pledger").toString() != "")
    m_pledger_address = obj.value("pledger").toString();

  if (obj.value("pledgee").toString() != "")
    m_pledgee_address = obj.value("pledgee").toString();

  if (obj.value("arbiter").toString() != "")
    m_arbiter_address = obj.value("arbiter").toString();

  if (obj.value("pledgerSignDate").toString() != "")
    m_pledger_sign_date = obj.value("pledgerSignDate").toString();

  if (obj.value("pledgeeSignDate").toString() != "")
    m_pledgee_sign_date = obj.value("pledgeeSignDate").toString();

  if (obj.value("arbiterSignDate").toString() != "")
    m_arbiter_sign_date = obj.value("arbiterSignDate").toString();

  if (obj.value("pRef").toString() != "")
    m_proposal_ref = obj.value("pRef").toString();

  // JS backward compatibility
  if (obj.value("trx").toString() != "")
    m_transaction_ref = obj.value("trx").toString();
  if (obj.value("tRef").toString() != "")
    m_transaction_ref = obj.value("tRef").toString();

  if (obj.keys().contains("redeemTerms"))
  {
    QJsonObject redeem_terms = obj.value("redeemTerms").toObject();

    if (redeem_terms.keys().contains("principal"))
      m_redeem_principal = redeem_terms.value("principal").toDouble();

    if (redeem_terms.keys().contains("annualInterest"))
      m_redeem_annual_interest = redeem_terms.value("annualInterest").toDouble();

    if (redeem_terms.keys().contains("repaymentOffset"))
      m_redeem_repayment_offset = redeem_terms.value("repaymentOffset").toInt();

    if (redeem_terms.keys().contains("repaymentAmount"))
      m_redeem_repayment_amount = redeem_terms.value("repaymentAmount").toDouble();

    if (redeem_terms.keys().contains("repaymentSchedule"))
      m_redeem_repayment_schedule = redeem_terms.value("repaymentSchedule").toInt();

    if (redeem_terms.keys().contains("repaymentsNumber"))
      m_redeem_repayments_number = redeem_terms.value("repaymentsNumber").toInt();

  }

  return true;
}

QJsonObject PledgeDocument::convertRedeemTermsToJson() const
{
  return QJsonObject {
    {"principal", QVariant::fromValue(m_redeem_principal).toDouble()},
    {"annualInterest", QVariant::fromValue(m_redeem_annual_interest).toDouble()},
    {"repaymentOffset", QVariant::fromValue(m_redeem_repayment_offset).toDouble()},
    {"repaymentAmount", QVariant::fromValue(m_redeem_repayment_amount).toDouble()},
    {"repaymentSchedule", QVariant::fromValue(m_redeem_repayment_schedule).toDouble()},
    {"repaymentsNumber", QVariant::fromValue(m_redeem_repayments_number).toDouble()}
  };
}

QJsonObject PledgeDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document["pledgerSignDate"] = m_pledger_sign_date;
  document["pledgeeSignDate"] = m_pledgee_sign_date;
  document["arbiterSignDate"] = m_arbiter_sign_date;
  document["pledger"] = m_pledger_address;
  document["pledgee"] = m_pledgee_address;
  document["arbiter"] = m_arbiter_address;
  document["redeemTerms"] = convertRedeemTermsToJson();

  if (m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP)
    document["pRef"] = m_proposal_ref;

  if (m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP)
    document["tRef"] = m_transaction_ref;

  // compacting doc ext info
  if (ext_info_in_document)
  {
    QJsonObject dExtInfo = document["dExtInfo"].toArray()[0].toObject();
    dExtInfo["pledgerUSet"] = SignatureStructureHandler::compactUnlocker(dExtInfo["pledgerUSet"].toObject());
    dExtInfo["pledgeeUSet"] = SignatureStructureHandler::compactUnlocker(dExtInfo["pledgeeUSet"].toObject());
    if (dExtInfo.keys().contains("arbiterUSet"))
      dExtInfo["arbiterUSet"] = SignatureStructureHandler::compactUnlocker(dExtInfo["arbiterUSet"].toObject());
    document["dExtInfo"] = QJsonArray {dExtInfo};
    document["dExtInfo"] = SignatureStructureHandler::compactUnlockersArray(document["dExtInfo"].toArray());
  }


  return document;
}

std::vector<TInput*> PledgeDocument::getInputs() const
{
  return {};
}

std::vector<TOutput*> PledgeDocument::getOutputs() const
{
  return {};
}

std::tuple<bool, QJsonArray> PledgeDocument::exportInputsToJson() const
{
  return {false, QJsonArray {}};
}

std::tuple<bool, QJsonArray> PledgeDocument::exportOutputsToJson() const
{
  return {false, QJsonArray {}};
}


QString PledgeDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject Jdoc = exportDocToJson(ext_info_in_document);

  // recaluculate block final length
  Jdoc["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(Jdoc).length());

  CLog::log("10 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(Jdoc).length()) + " serialized document: " + CUtils::serializeJson(Jdoc), "app", "trace");

  return CUtils::serializeJson(Jdoc);
}

QString PledgeDocument::getRef() const
{
  return "";
}

QString PledgeDocument::getProposalRef() const
{
  return m_proposal_ref;
}

QString PledgeDocument::getPayerTrxLinkBack() const
{
  return m_transaction_ref;
}

// old name was calcPldgeDocumentCost
std::tuple<bool, CMPAIValueT> PledgeDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  LoanDetails loan_details = LoanContractHandler::calcLoanRepayments(
    m_redeem_principal,
    m_redeem_annual_interest,
    m_redeem_repayment_amount,
    m_redeem_repayment_schedule);

  if (!loan_details.m_calculation_status)
    return {false, 0};

  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;

  CMPAIValueT the_cost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate) *
    loan_details.m_repayments.size();

  if (stage == CConsts::STAGES::Creating)
  {
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc cutom post the_cost + machine interest(" + CUtils::sepNum(the_cost) +" micro PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }
  return {true, CUtils::CFloor(the_cost)};
}

bool PledgeDocument::applyDocFirstImpact(const Block& block) const
{
  return GeneralPledgeHandler::activatePledge(block, this);
}

// js name was calcExtRoot
QString PledgeDocument::calcDocExtInfoHash() const //calcTrxExtRootHash()
{
  QString hashables = "";
  if (m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP)
  {
    hashables = "{";

    if(m_doc_ext_info[0].toObject().value("arbiterSignatures").toArray().size() > 0 )
    {
      hashables += "\"arbiterSignatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("arbiterSignatures").toArray()) + "," ;
      hashables += "\"arbiterUSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("arbiterUSet").toObject()) + "," ;
    }

    hashables += "\"pledgeeSignatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("pledgeeSignatures").toArray()) + "," ;
    hashables += "\"pledgeeUSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("pledgeeUSet").toObject()) + "," ;

    hashables += "\"pledgerSignatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("pledgerSignatures").toArray()) + "," ;
    hashables += "\"pledgerUSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("pledgerUSet").toObject()) + "" ;

    hashables += "}" ;
  }

  QString hash = CCrypto::keccak256(hashables);
  CLog::log("\nHashable string for ext info of pledge doc(" + m_doc_type + " / " +
    m_doc_class + ") hash(" + hash + ") hashables: " + hashables, "app", "trace");
  return hash;
}

bool PledgeDocument::hasSignable() const
{
  return true;
}

QString PledgeDocument::extractSignablePartsForPledgee() const
{
  QString signables = extractSignablePartsForPledger();
  signables += "\"pledgerSignatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("pledgerSignatures").toArray()) + "," ;
  signables += "\"pledgeeSignDate\":\"" + m_pledgee_sign_date + "\"," ;
//  signables += "\"pledgeeUSet\":" + m_pledgee_unlock_set + "\"," ;
//  signables += "\"trx\":" + pledge.trx + "\"," ;
  return signables;
}

QString PledgeDocument::getSignMsgAsPledgee() const
{

  QString signables = extractSignablePartsForPledgee();
  signables += "}" ;

  QString sign_message = CCrypto::keccak256Dbl(signables);
  sign_message = sign_message.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  CLog::log("pledge sign message as pledgee(" + sign_message + ") signables: " + signables + " ", "app", "trace");

  return sign_message;
}

// getDocSignMsg is not same as other docs
QString PledgeDocument::extractSignablePartsForPledger() const
{
  QString redeem_terms = "{";
  redeem_terms += "\"annualInterest\":" + QString::number(m_redeem_annual_interest) + ",";
  redeem_terms += "\"principal\":" + QString::number(m_redeem_principal) + ",";
  redeem_terms += "\"repaymentAmount\":" + QString::number(m_redeem_repayment_amount) + ",";
  redeem_terms += "\"repaymentOffset\":" + QString::number(m_redeem_repayment_offset) + ",";
  redeem_terms += "\"repaymentSchedule\":" + QString::number(m_redeem_repayment_schedule) + "}";

  QString signables = "{";
  signables += "\"arbiter\":\"" + m_arbiter_address + "\"," ;
  signables += "\"dClass\":\"" + m_doc_class + "\"," ;
  signables += "\"dType\":\"" + m_doc_type + "\"," ;
  signables += "\"pledgee\":\"" + m_pledgee_address + "\"," ;
  signables += "\"pledger\":\"" + m_pledger_address + "\"," ;
  signables += "\"pledgerSignDate\":\"" + m_pledger_sign_date + "\"," ;
  if (m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP)
    signables += "\"pRef\":\"" + m_proposal_ref + "\"," ;
  signables += "\"redeemTerms\":" + redeem_terms;
//  signables += "}" ;
  return signables;
}

// getDocSignMsg is not same as other docs
QString PledgeDocument::getSignMsgAsPledger() const
{
  QString signables = extractSignablePartsForPledger();
  signables += "}" ;
  QString sign_message = CCrypto::keccak256Dbl(signables);  // for security reason do double hash
  sign_message = sign_message.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  CLog::log("pledge sign message as pledger(" + sign_message + ") signables: " + signables + " ", "app", "trace");
  return sign_message;
}

bool PledgeDocument::veridfyDocSignature() const
{
  // validate pledger signature
  QJsonObject dExtInfo = m_doc_ext_info[0].toObject();
  auto pledgerUSet = dExtInfo.value("pledgerUSet").toObject();

  bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
    pledgerUSet,
    m_pledger_address);

  if (!is_valid_unlock)
  {
    CLog::log("Invalid pledger signature structure on Pledge(" + m_doc_hash + ")! ", "sec", "error");
    return false;
  }


  // validate pledgeee signature
  auto pledgeeUSet = dExtInfo.value("pledgeeUSet").toObject();
  is_valid_unlock = SignatureStructureHandler::validateSigStruct(
    pledgeeUSet,
    m_pledgee_address);

  if (!is_valid_unlock)
  {
    CLog::log("Invalid pledgee signature structure on Pledge(" + m_doc_hash + ")! ", "sec", "error");
    return false;
  }

  // pledger signature & permission validate check
  bool permitedToPledge = false;
  QString sign_message_pledger = getSignMsgAsPledger();
  QJsonArray pledger_signatures = dExtInfo.value("pledgerSignatures").toArray();
  for (CSigIndexT signature_index = 0; signature_index < pledger_signatures.size(); signature_index++)
  {
    QString a_signature = pledger_signatures[signature_index].toString();
    try {
      bool verifyRes = CCrypto::ECDSAVerifysignature(
        pledgerUSet.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
        sign_message_pledger,
        a_signature);
      if (!verifyRes)
      {
        CLog::log("Invalid creator signature sign on Pledge(" + m_doc_hash + ")! ", "sec", "error");
        return false;
      }
    } catch (std::exception) {
      CLog::log("Exception! Invalid creator signature sign on Pledge(" + m_doc_hash + ")! ", "sec", "error");
      return false;
    }

    if (pledgerUSet.value("sSets").toArray()[signature_index].toObject().value("pPledge").toString() == CConsts::YES)
      permitedToPledge = true;

  }
  if (!permitedToPledge)
  {
    CLog::log("Block creating, the signer of Pledge has not permited to pledge! (" + m_doc_hash + ")! ", "sec", "error");
    return false;
  }


  // pledgee signature & permission validate check
  QString sign_message_pledgee = getSignMsgAsPledgee();
  QJsonArray pledgee_signatures = dExtInfo.value("pledgeeSignatures").toArray();
  for (CSigIndexT signature_index = 0; signature_index < pledgee_signatures.size(); signature_index++)
  {
    QString a_signature = pledgee_signatures[signature_index].toString();
    try {
      bool verifyRes = CCrypto::ECDSAVerifysignature(
        pledgeeUSet.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
        sign_message_pledgee,
        a_signature);
      if (!verifyRes)
      {
        CLog::log("Invalid creator signature sign on Pledge(" + m_doc_hash + ")! ", "sec", "error");
        return false;
      }
    } catch (std::exception) {
      CLog::log("Exception! Invalid creator signature sign on Pledge(" + m_doc_hash + ")! ", "sec", "error");
      return false;
    }
  }

  return true;
}


QString PledgeDocument::getDocHashableString() const
{
  QString hashables = "{";
  if (m_arbiter_address != "")
  {
    hashables += "\"arbiter\":\"" + m_arbiter_address + "\"," ;
    hashables += "\"arbiterSignDate\":\"" + m_arbiter_sign_date + "\"," ;
  }

  hashables += "\"dCDate\":\"" + m_doc_creation_date + "\"," ;
  hashables += "\"dClass\":\"" + m_doc_class + "\"," ;
  hashables += "\"dExtHash\":\"" + m_doc_ext_hash + "\"," ;
  hashables += "\"dComment\":\"" + m_doc_comment + "\"," ;
  hashables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\"," ;
  hashables += "\"dType\":\"" + m_doc_type + "\"," ;
  hashables += "\"dVer\":\"" + m_doc_version + "\"" ;
  hashables += "\"pledgee\":\"" + m_pledgee_address + "\"," ;
  hashables += "\"pledgeeSignDate\":\"" + m_pledgee_sign_date + "\"," ;
  hashables += "\"pledger\":\"" + m_pledger_address + "\"," ;
  hashables += "\"pledgerSignDate\":\"" + m_pledger_sign_date + "\"," ;

  if (m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP)
    hashables += "\"pRef\":\"" + m_proposal_ref + "\"," ;

  QString redeem_terms = "{";
  redeem_terms += "\"annualInterest\":" + QString::number(m_redeem_annual_interest) + ",";
  redeem_terms += "\"principal\":" + QString::number(m_redeem_principal) + ",";
  redeem_terms += "\"repaymentAmount\":" + QString::number(m_redeem_repayment_amount) + ",";
  redeem_terms += "\"repaymentOffset\":" + QString::number(m_redeem_repayment_offset) + ",";
  redeem_terms += "\"repaymentSchedule\":" + QString::number(m_redeem_repayment_schedule) + "}";
  hashables += ",\"redeemTerms\":" + redeem_terms + "" ;

  if (m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP)
    hashables += ",\"tRef\":\"" + m_transaction_ref + "\"" ;

  hashables += "}" ;
  return hashables;
}

// old name was calcHashDPledge
QString PledgeDocument::calcDocHash() const
{
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256Dbl(hashables);
  CLog::log("\nHashable string for pledge doc(" + m_doc_type + " / " +
    m_doc_class + ") hash(" + hash + ") hashables: " + hashables, "app", "trace");
  return hash;
}

void PledgeDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{
  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  if (block_inspect_container->m_block_alter_treasury_incomes.keys().contains("TP_PLEDGE"))
  {
    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_PLEDGE"])
    {
      // if proposal costs was payed by Pledging & a lending transaction
      bool doc_cost_is_payed = true;
      CDocHashT pledge_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];

      if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[pledge_hash] = {"Pledge(" +CUtils::hash8c(pledge_hash) + ") is payed by a rejected transaction(" +CUtils::hash8c(a_treasury_entry.m_trx_hash) + ")", false};
      }

      auto[inx_, pledge_doc] = block->getDocumentByHash(pledge_hash);
      Q_UNUSED(inx_);
      if (pledge_doc->m_doc_class != CConsts::PLEDGE_CLASSES::PledgeP)
      {
        doc_cost_is_payed = false;
        cost_payment_status[pledge_hash] = {"Pledge dClass(" + pledge_doc->m_doc_class + ") is not supported", false};
      }

      if (doc_cost_is_payed)
      {
        CLog::log("Successfully TP_PLEDGE Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_PLEDGE)", "app", "trace");
        cost_payment_status[pledge_hash].m_message = "Pledge Cost imported to treasury succsessfully.";

        QString income_title = "TP_PLEDGE Pledge(" + CUtils::hash8c(pledge_hash) + ") Trx(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ")";

        cost_payment_status[pledge_hash].m_message = "Proposal Cost imported to treasury succsessfully.";
        TreasuryHandler::insertIncome(
          income_title,
          "TP_PLEDGE",
          income_title,
          block->m_block_creation_date,
          a_treasury_entry.m_value,
          block->getBlockHash(),
          a_treasury_entry.m_coin);

      } else {
        CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_PLEDGE)", "sec", "error");
        CLog::log("Failed cost_payment_status: Proposal(" + CUtils::hash8c(pledge_hash) + ")" + UTXOImportDataContainer::dumpMe(cost_payment_status[pledge_hash]), "sec", "error");

        GeneralPledgeHandler::removePledgeBecauseOfPaymentsFail(pledge_hash);

        // remove related proposal as well
        ProposalHandler::removeProposal(pledge_doc->getProposalRef());

      }
    }
  }

  block_inspect_container->m_cost_payment_status["TP_PLEDGE"] = cost_payment_status;
}
