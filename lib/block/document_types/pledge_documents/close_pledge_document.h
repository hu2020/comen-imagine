#ifndef CLOSEPLEDGEDOCUMENT_H
#define CLOSEPLEDGEDOCUMENT_H


class ClosePledgeDocument: public Document
{
public:
  ClosePledgeDocument(const QJsonObject& obj);
  ~ClosePledgeDocument();

  QString m_ref = "";
  CAddressT m_concluder_address = "";
  QString m_conclude_type = "";

  bool setByJsonObj(const QJsonObject& obj) override;
  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString calcDocExtInfoHash() const override;  // calcTrxExtRootHash
  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;


  QString getDocHashableString() const override;
  QString calcDocHash() const override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;
  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

  bool deleteInputs() override;
  bool deleteOutputs() override;

  bool hasSignable() const override;
  QString getDocSignMsg() const override;
  bool veridfyDocSignature() const override;

  bool applyDocFirstImpact(const Block& block) const override;

  QString getRef() const override;

  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);

  //  -  -  -  base class methods




//  bool validatefDocSigner(
//    const QString& stage,
//    const QString& sign_message) const;
//  //  -  -  -  Demos posts
//  QString getSignMsgDNewPost() const;


};

#endif // CLOSEPLEDGEDOCUMENT_H
