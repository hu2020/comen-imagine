#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/document_types/document.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "close_pledge_document.h"

ClosePledgeDocument::ClosePledgeDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

ClosePledgeDocument::~ClosePledgeDocument()
{
  deleteInputs();
  deleteOutputs();
}

bool ClosePledgeDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  if (obj.value("concludeType").toString() != "")
    m_conclude_type = obj.value("concludeType").toString();

  return true;
}

QString ClosePledgeDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject Jdoc = exportDocToJson();

  // recaluculate block final length
  Jdoc["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(Jdoc).length());

  CLog::log("9 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(Jdoc).length()) + " serialized:" + CUtils::serializeJson(Jdoc), "app", "trace");

  return CUtils::serializeJson(Jdoc);
}

// js name was customHash
QString ClosePledgeDocument::getDocHashableString() const
{
  QString hashables = "";
  // TODO: since this document not exist in DAG yet, we can refactor it completely before its first real usage
  hashables = "{";
  hashables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\",";
  hashables += "\"dExtHash\":\"" + m_doc_ext_hash + "\"}";

  CLog::log("calc close pledge type(" + m_doc_class + ") hashables: " + hashables, "app", "trace");
  return hashables;
}
// js name was calcHashDPledgeClose
QString ClosePledgeDocument::calcDocHash() const
{
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("\nHashable string for close pledge doc(" + m_doc_type + " / " + m_doc_class +
            ") hash(" + hash + ")" + hashables, "app", "trace");
  return hash;
}

// old name was calcClosePldgCost
std::tuple<bool, CMPAIValueT> ClosePledgeDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;
  CMPAIValueT theCost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);

  // if pledger asks for closing contract, entire network must audit entire payments(maybe for years) and decide about closing. this process s TOO costly
  // TODO: implement, machine calc cost based on repayments count.
  if (m_conclude_type == CConsts::PLEDGE_CONCLUDER_TYPES::ByPledger)
    theCost *= 1000;  // call recognizeSignerTypeInfo to recognizing concluder type

  CLog::log("calc cutom post theCost(" + CUtils::sepNum(theCost) +" micro PAIs) Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");

  if (stage == CConsts::STAGES::Creating)
  {
    theCost = theCost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc cutom post theCost + machine interest(" + CUtils::sepNum(theCost) +" micro PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }
  return {true, CUtils::CFloor(theCost)};
}

QString ClosePledgeDocument::calcDocExtInfoHash() const //calcTrxExtRootHash()
{
  QString hashables = "{";
  hashables += "\"signatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("signatures").toArray()) + ",";
  hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("uSet").toObject()) + "}";
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("Close pledge Doc Ext Root Hash Hashables Doc(" + m_doc_hash + ") hashables: " + hashables + "\nRegenrated hash: " + hash, "app", "trace");
  return hash;
}

QJsonObject ClosePledgeDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  return document;
}

std::tuple<bool, QJsonArray> ClosePledgeDocument::exportInputsToJson() const
{
  return {false, QJsonArray {}};  // free dosc haven't inputs
}

std::tuple<bool, QJsonArray> ClosePledgeDocument::exportOutputsToJson() const
{
  return {false, QJsonArray {}};  // free dosc haven't outputs
}

bool ClosePledgeDocument::deleteInputs()
{
  return true;
}

bool ClosePledgeDocument::deleteOutputs()
{
  return true;
}

QString ClosePledgeDocument::getRef() const
{
  return m_ref;
}

// js name was customValidate
bool ClosePledgeDocument::veridfyDocSignature() const
{
  QJsonObject dExtInfo = m_doc_ext_info[0].toObject();
  auto unlockSet = dExtInfo.value("uSet").toObject();

  bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
    unlockSet,
    m_concluder_address);

  if (!is_valid_unlock)
  {
    CLog::log("Invalid creator signature structure on iname(" + m_doc_hash + ")! ", "sec", "error");
    return false;
  }

  // doc signature & permission validate check
  QString sign_message = getDocSignMsg();
  QJsonArray signatures = m_doc_ext_info[0].toObject().value("signatures").toArray();
  bool permitedToClosePledge = false;
  for (CSigIndexT signature_index = 0; signature_index < signatures.size(); signature_index++)
  {
    QString a_signature = signatures[signature_index].toString();
    try {
      bool verifyRes = CCrypto::ECDSAVerifysignature(
        unlockSet.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
        sign_message,
        a_signature);
      if (!verifyRes)
      {
        CLog::log("Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
        return false;
      }

      if (unlockSet.value("sSets").toArray()[signature_index].toObject().value("pPledge").toString() == CConsts::YES)
        permitedToClosePledge = true;

    } catch (std::exception) {
      CLog::log("Exception! Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
      return false;
    }
  }

  if (!permitedToClosePledge)
  {
    CLog::log("the close-Pledge closer has not permited to close-pledge(" + getDocHash() + ")! ", "sec", "error");
    return false;
  }

  return true;
}

bool ClosePledgeDocument::hasSignable() const
{
  // maybe switch case by doc class!
  return true;
}

// js name was getSignMsgDForCloseContract
QString ClosePledgeDocument::getDocSignMsg() const
{
  QString signables = "{";
  signables += "\"concluder\":\"" + m_concluder_address + "\"," ;
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\"," ;
  signables += "\"dComment\":\"" + m_doc_comment + "\"," ;
  signables += "\"dType\":\"" + m_doc_type+ "\"," ;
  signables += "\"dVer\":\"" + m_doc_version + "\"," ;
  signables += "\"dRef\":\"" + getRef() + "\"}" ;

  QString sign_message = CCrypto::keccak256(signables);
  sign_message = sign_message.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  CLog::log("Close pledge sign_message(" + sign_message + ") signables: " + signables + " ", "app", "trace");
  return sign_message;
}

// old name was importClosePledgesCost
void ClosePledgeDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{

  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  if (block_inspect_container->m_block_alter_treasury_incomes.keys().contains("TP_PLEDGE_CLOSE"))
  {
    CLog::log("importing TP_PLEDGE_CLOSE payments " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_block_alter_treasury_incomes["TP_PLEDGE_CLOSE"]), "trx", "trace");

    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_PLEDGE_CLOSE"]) {
        // if closing cost is payed by a valid trx
        bool doc_cost_is_payed = true;

        if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
        {
          doc_cost_is_payed = false;
          cost_payment_status[a_treasury_entry.m_trx_hash] = {"Supporter transaction(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") for close-pledge is rejected because of doublespending", false};
        }

        if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(a_treasury_entry.m_trx_hash))
        {
          doc_cost_is_payed = false;
          cost_payment_status[a_treasury_entry.m_trx_hash] = {"The Close Pledge costs is not supported by any trx!", false};
        }

        CDocHashT close_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];
        auto[inx_, pledge_doc] = block->getDocumentByHash(close_hash);
        Q_UNUSED(inx_);
        if ((pledge_doc->m_doc_type != CConsts::DOC_TYPES::ClosePledge) || (pledge_doc->m_doc_class != CConsts::PLEDGE_CLOSE_CLASESS::General))
        {
          doc_cost_is_payed = false;
          cost_payment_status[close_hash] = {"Close Pledge dType(" + pledge_doc->m_doc_type + ") or dClass(" + pledge_doc->m_doc_class + ") is not supported", false};
        }

        CDocHashT supporter_trx = block_inspect_container->m_map_U_trx_ref_to_trx_hash[close_hash];
        QStringList rejected_transactions = block_inspect_container->m_rejected_transactions.keys();
        if (rejected_transactions.contains(supporter_trx) || rejected_transactions.contains(a_treasury_entry.m_trx_hash))
        {
          doc_cost_is_payed = false;
          cost_payment_status[close_hash] = {"Supporter transaction for close pledge is rejected because of doublespending", false};
        }

        if (doc_cost_is_payed)
        {
          CLog::log("Successfully TP_PLEDGE_CLOSE Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_PLEDGE_CLOSE)", "app", "trace");

          cost_payment_status[close_hash].m_message = "Close pledge Cost imported to treasury succsessfully.";
          QString title = "TP_PLEDGE_CLOSE doc(" + CUtils::hash8c(close_hash) + ")";
          TreasuryHandler::insertIncome(
            title,
            "TP_PLEDGE_CLOSE",
            title,
            block->m_block_creation_date,
            a_treasury_entry.m_value,
            block->getBlockHash(),
            a_treasury_entry.m_coin);

        } else {
          CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_PLEDGE_CLOSE)", "sec", "error");
          CLog::log("cost_payment_status not payed: " + UTXOImportDataContainer::dumpMe(cost_payment_status[close_hash]), "sec", "error");

          // re-pleadging account
          GeneralPledgeHandler::reOpenPledgeBecauseOfPaymentsFail(close_hash);

        }
    }
  }

  block_inspect_container->m_cost_payment_status["TP_PLEDGE_CLOSE"] = cost_payment_status;
}

// js name was applyPledgeClosing, doApplyClosingPledge
bool ClosePledgeDocument::applyDocFirstImpact(const Block& block) const
{
  return GeneralPledgeHandler::doApplyClosingPledge(block, this);
}
