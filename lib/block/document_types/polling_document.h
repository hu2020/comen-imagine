#ifndef POLLINGDOCUMENT_H
#define POLLINGDOCUMENT_H


class PollingDocument: public Document
{
public:
  PollingDocument(const QJsonObject& obj);
  ~PollingDocument();

  TimeByHoursT m_voting_timeframe = 0;
  QString m_polling_ref = ""; // reference to document for which is running this polling
  QString m_polling_ref_type = "";  // refType
  QString m_polling_ref_class = "";
  QString m_polling_comment = "";
  QString m_polling_creator = "";
  QString m_polling_start_date = "";
  QString m_polling_status = "";

  uint64_t m_potential_voters_count = 0;

  bool setByJsonObj(const QJsonObject& obj) override;
//  bool setDocumentInputs(const QJsonValue& obj) override;
//  bool setDocumentOutputs(const QJsonValue& obj) override;

  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;  // calcTrxExtRootHash

  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;

  QString getRef() const override;
  QString getRefType() const override;
  QString getRefClass() const;

  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

  QString getDocToBeSignedHash() const override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;
//  QVector<COutputIndexT> getDPIs() override;

//  std::vector<TInput*> getInputs() const override;
//  std::vector<TOutput*> getOutputs() const override;

  bool applyDocFirstImpact(const Block& block) const override;


  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);


};

#endif // POLLINGDOCUMENT_H
