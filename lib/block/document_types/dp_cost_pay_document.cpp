#include "stable.h"

#include "document.h"
#include "lib/ccrypto.h"
#include "dp_cost_pay_document.h"


DPCostPayDocument::DPCostPayDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);

}

DPCostPayDocument::~DPCostPayDocument()
{
  deleteOutputs();
}


bool DPCostPayDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);
  if (obj.value("outputs").toArray().size() > 0)
      setDocumentOutputs(obj.value("outputs"));
  m_doc_ext_hash = CConsts::NO_EXT_HASH;  // legacy unnecessary property
  return true;
}

bool DPCostPayDocument::deleteInputs()
{
  return true;
}

bool DPCostPayDocument::deleteOutputs()
{
  for (TOutput* an_output: m_outputs)
    delete an_output;
  return true;
}

std::tuple<bool, QJsonArray> DPCostPayDocument::exportInputsToJson() const
{
  return {false, QJsonArray {}};
}

std::tuple<bool, QJsonArray> DPCostPayDocument::exportOutputsToJson() const
{
  QJsonArray outputs {};
  if (m_outputs.size() ==0)
    return {false, outputs};

  for(auto an_output: m_outputs)
    outputs.push_back(QJsonArray {
      an_output->m_address,
      QVariant::fromValue(an_output->m_amount).toDouble()});

  return {true, outputs};
}

QJsonObject DPCostPayDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);


  if (document.keys().contains("dLen"))
    document.remove("dLen");

  if (document.keys().contains("dExtHash"))
    document.remove("dExtHash");

  document["dClass"] = "";  // legacy unnecessary property

//  if (m_doc_comment != "")
//    document["description"] = m_doc_comment;  // legacy unnecessary property too

  return document;
}

QString DPCostPayDocument::getRef() const
{
  return "";
}

QVector<COutputIndexT> DPCostPayDocument::getDPIs() const
{
  return {};
}

bool DPCostPayDocument::setDocumentOutputs(const QJsonValue& obj)
{
  QJsonArray outputs = obj.toArray();
  for(QJsonValueRef an_output: outputs)
  {
    QJsonArray oo = an_output.toArray();
    TOutput *o  = new TOutput({oo[0].toString(), static_cast<CMPAIValueT>(oo[1].toDouble())});
    m_outputs.push_back(o);
  }
  return true;
}

QString DPCostPayDocument::getDocHashableString() const
{
  QString hahsableTrx = "{";
  hahsableTrx += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  hahsableTrx += "\"dClass\":\"" + m_doc_class + "\",";
  hahsableTrx += "\"dType\":\"" + m_doc_type + "\",";
  hahsableTrx += "\"dVer\":\"" + m_doc_version + "\",";
  hahsableTrx += "\"outputs\":" + stringifyOutputs() + "";
  hahsableTrx += "}";
  return hahsableTrx;
}

QString DPCostPayDocument::calcDocHash() const
{
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256Dbl(hashables); // NOTE: absolutely using double hash for more security
  CLog::log("\nHashable string for DPCOst doc(" + m_doc_type + " / " + m_doc_class +
    ") hash(" + hash + ") hashables:" + hashables, "app", "trace");
  return hash;
}

QString DPCostPayDocument::calcDocExtInfoHash() const
{
  return CConsts::NO_EXT_HASH;
}

bool DPCostPayDocument::applyDocFirstImpact(const Block &block) const
{
  Q_UNUSED(block);
  // dp cost docs haven't first impact
  return true;
}

std::vector<TInput*> DPCostPayDocument::getInputs() const
{
  return {};
}

std::vector<TOutput*> DPCostPayDocument::getOutputs() const
{
  return m_outputs;
}


//static createDPCostPaymentTrx(args) {
////  let creationDate = _.has(args, 'creationDate') ? args.creationDate : utils.getNow();
////  let treasury = _.has(args, 'treasury') ? args.treasury : 0;
////  let backerNetFee = _.has(args, 'backerNetFee') ? args.backerNetFee : 0;
////  let trx = {
////    hash: "0000000000000000000000000000000000000000000000000000000000000000",
////    dType: iConsts.DOC_TYPES.DPCostPay,
////    dClass: "",
////    dVer: "0.0.0",
////    description: `Data & Process Cost Payment`,
////    creationDate,
////    outputs: [
////        ['TP_DP', treasury],
////        [machine.getMProfileSettingsSync().backerAddress, backerNetFee]
////    ]
//  };
//  trx.hash = trxHashHandler.doHashTransaction(trx);
//  clog.trx.info(`Data & Process Cost Payment transaction: ${JSON.stringify(trx)}`);
//  return trx;
//}
