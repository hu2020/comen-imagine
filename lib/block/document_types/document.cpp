#include "stable.h"

#include "lib/ccrypto.h"

class Block;
class DbHandler;

#include "lib/block/block_types/block.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"
#include "document.h"

const QString Document::stbl_docs_blocks_map = "c_docs_blocks_map";
const QStringList Document::stbl_docs_blocks_map_fields = {"dbm_block_hash", "dbm_doc_index", "dbm_doc_hash", "dbm_last_control"};

Document::~Document()
{

}

QJsonArray Document::getDocExtInfo() const
{
  return m_doc_ext_info;
}

void Document::maybeAssignDocExtInfo(
  const Block* block,
  const CDocIndexT& doc_inx)
{
  if (m_doc_ext_info.size() == 0)
    m_doc_ext_info = block->getBlockExtInfoByDocIndex(doc_inx);

}

QString Document::calcDocExtInfoHash() const
{
  return "";
}

bool Document::setByJsonObj(const QJsonObject& obj)
{
  QStringList object_keys = obj.keys();

  if (obj.value("dHash").toString() != "")
    m_doc_hash = obj.value("dHash").toString();

  if (obj.value("dType").toString() != "")
    m_doc_type = obj.value("dType").toString();

  if (obj.value("dClass").toString() != "")
    m_doc_class = obj.value("dClass").toString();

  if (obj.value("dRef").toString() != "")
    m_doc_ref = obj.value("dRef").toString();

  if (obj.value("dCDate").toString() != "")
    m_doc_creation_date = obj.value("dCDate").toString();

  if (obj.value("dVer").toString() != "")
    m_doc_version = obj.value("dVer").toString();

  if (obj.value("dLen").toString() != "")
    m_doc_length = CUtils::convertPaddedStringToInt(obj.value("dLen").toString());

  if (obj.value("dComment").toString() != "")
    m_doc_comment = obj.value("dComment").toString();

  if (obj.value("dTitle").toString() != "")
    m_doc_title = obj.value("dTitle").toString();

  if (obj.keys().contains("dExtInfo"))
    m_doc_ext_info = obj.value("dExtInfo").toArray();

  if (obj.value("dExtHash").toString() != "")
    m_doc_ext_hash = obj.value("dExtHash").toString();

  if (obj.value("dTags").toString() != "")
    m_doc_tags = obj.value("dTags").toString();

  return true;
}

QString Document::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject Jdoc = exportDocToJson(ext_info_in_document);

  // recaluculate block final length
  Jdoc["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(Jdoc).length());

  CLog::log("5 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(Jdoc).length()) + " serialized document: " + CUtils::serializeJson(Jdoc), "app", "trace");

  return CUtils::serializeJson(Jdoc);
}

QString Document::getRef() const
{
  return "";
}

QString Document::getRefType() const
{
  return "";
}

QString Document::getProposalRef() const
{
  return "";
}

QString Document::getPayerTrxLinkBack() const
{
  return "";
}

CMPAISValueT Document::getDocCosts() const
{
  return -1;
}

bool Document::deleteInputs()
{
  CUtils::exiter("deleteInputs is n't implement for Document Base class document(" + m_doc_type + ")", 0);
  return false;
}

bool Document::deleteOutputs()
{
  CUtils::exiter("deleteOutputs is n't implement for Document Base class document(" + m_doc_type + ")", 0);
  return false;
}

bool Document::setDocumentInputs(const QJsonValue& obj)
{
  CUtils::exiter("set Document Inputs is n't implement for Document Base class document(" + m_doc_type + ")", 0);
  return false;
}

bool Document::setDocumentOutputs(const QJsonValue& obj)
{
  CUtils::exiter("set Document Outputs is n't implement for Document Base class document(" + m_doc_type + ")", 0);
  return false;
}


std::tuple<bool, QJsonArray> Document::exportInputsToJson() const
{
  return {false, QJsonArray {}};
}

std::tuple<bool, QJsonArray> Document::exportOutputsToJson() const
{
  return {false, QJsonArray {}};
}

QJsonObject Document::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document {
    {"dHash", m_doc_hash},
    {"dType", m_doc_type},
    {"dVer", m_doc_version},
    {"dCDate", m_doc_creation_date},
    {"dLen", CUtils::paddingLengthValue(m_doc_length)}
  };

  if (m_doc_class != "")
    document["dClass"] = m_doc_class;

  // maybe add inputs
  auto[has_input, Jinputs] = exportInputsToJson();
  if (has_input)
    document["inputs"] = Jinputs;

  // maybe add outputs
  auto[has_output, Joutputs] = exportOutputsToJson();
  if (has_output)
    document["outputs"] = Joutputs;

  if (m_doc_ref != "")
    document["dRef"] = m_doc_ref;

  if (m_doc_title != "")
    document["dTitle"] = m_doc_title;

  if (m_doc_comment != "")
    document["dComment"] = m_doc_comment;

  if (m_doc_tags != "")
    document["dTags"] = m_doc_tags;

  if (m_doc_ext_hash != "")
    document["dExtHash"] = m_doc_ext_hash;

  if (ext_info_in_document)
    document["dExtInfo"] = m_doc_ext_info;

  return document;
}

QVector<COutputIndexT> Document::getDPIs() const
{
  CUtils::exiter("attribute data_and_process_payment_indexes is n't implement for Base class document(" + m_doc_type + ")", 0);
  return {};
}


QString Document::getDocHash() const
{
  return m_doc_hash;
}


std::vector<TInput*> Document::getInputs() const
{
  return {};
}

std::vector<TOutput*> Document::getOutputs() const
{
  return {};
}

QString Document::getDocHashableString() const
{
  return "";
}

CDocHashT Document::getDocRef() const
{
  return m_doc_ref;
}

CDocHashT Document::calcDocHash() const
{
//  QString customStringified = safeStringifyDoc();
//  QString hash = CCrypto::keccak256(customStringified);
//  CLog::log("hash " + hash);
//  return hash;
  return "";
}

DocLenT Document::calcDocLength() const
{
  DocLenT doc_length = safeStringifyDoc().length();
  return doc_length;
}

void Document::setDocHash()
{
  m_doc_hash = calcDocHash();
}

void Document::setDocLength()
{
  m_doc_length = calcDocLength();
}

void Document::setDExtHash()
{
  m_doc_ext_hash = calcDocExtInfoHash();
}

GenRes Document::customValidateDoc(const Block* block) const
{
  Q_UNUSED(block);
  return {true, ""};
}

GenRes Document::fullValidate(const Block* block) const
{
  QString msg;
  // doc length controll
  DocLenT doc_lenght = safeStringifyDoc(true).length();
  if(doc_lenght < 1)
  {
    msg = "Doc length can not be zero or negative! block(" + block->m_block_type + " / " + CUtils::hash8c(block->m_block_hash) + ") doc(" + m_doc_type + " / " + CUtils::hash8c(getDocHash()) + ") doc class(" + m_doc_class + ")";
    CLog::log(msg, "sec", "error");
    return {false, msg};
  }
  if(doc_lenght > CConsts::MAX_DOC_LENGTH_BY_CHAR)
  {
    CLog::log("Doc length excced! block(" + block->m_block_type + " / " + CUtils::hash8c(block->m_block_hash) + ") doc(" + m_doc_type + " / " + CUtils::hash8c(getDocHash()) + ") doc class(" + m_doc_class + ")", "sec", "error");
    return {false, msg};
  }
  if ((m_doc_length != 0) && (doc_lenght > m_doc_length))
  {
    msg = "Doc real length is biger than stated length! block(" + block->m_block_type + " / " + CUtils::hash8c(block->m_block_hash) + ") doc(" + m_doc_type + " / " + CUtils::hash8c(getDocHash()) + ") doc class(" + m_doc_class + ")";
    CLog::log(msg, "sec", "error");
    return {false, msg};
  }

  // recalculate documents hash
  if(getDocHash() != calcDocHash())
  {
    msg = "Mismatch document Hash! doc(" + m_doc_type + " / " + CUtils::hash8c(getDocHash()) + ") localy calculated(" + CUtils::hash8c(calcDocHash()) +") block(" + block->m_block_type + " / " + CUtils::hash8c(block->m_block_hash) + ") ";
    CLog::log(msg, "sec", "error");
    return {false, msg};
  }

  // controll doc ext hash
  if(m_doc_ext_hash != calcDocExtInfoHash())
  {
    msg = "Mismatch doc ext Hash. remote doc ext hash(" + m_doc_ext_hash + ") localy calculated(" + calcDocExtInfoHash() +") block(" + CUtils::hash8c(block->m_block_hash) + ") " + safeStringifyDoc();
    CLog::log(msg, "sec", "error");
    return {false, msg};
  }

  // controll document signatures (if exist)
  if(hasSignable())
    if(!veridfyDocSignature())
      return {false, "Failed in vrify doc signature"};

  // general doc validation
  GenRes custom_validate = customValidateDoc(block);
  if(!custom_validate.status)
  {
    msg = "Failed validate Doc " +custom_validate.msg+ " hash(" + m_doc_ext_hash + ") block(" + CUtils::hash8c(block->m_block_hash) + ")";
    CLog::log(msg, "sec", "error");
    return {false, msg};
  }

  return {true, "is Valid"};
}


/**
 * @brief Document::applyDocFirstImpact
 * depends on the document type, the node does some impacts on database
 * e.g. records a FleNS in DB
 */
bool Document::applyDocFirstImpact(const Block& block) const
{
  Q_UNUSED(block);
  return false;
}

//QJsonObject Document::exportJson() const
//{
//  QJsonObject r;
//  return r;
//}

std::tuple<bool, CMPAIValueT> Document::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  Q_UNUSED(stage);
  Q_UNUSED(cDate);
  Q_UNUSED(extraLength);
  return {false, 0};
}

bool Document::hasSignable() const
{
  // by default documents have no part to signing
  return false;
}

QString Document::getDocToBeSignedHash() const
{
  // by default documents have no part to signing
  return "";
}

QString Document::getDocSignMsg() const
{
  return "";
}

bool Document::veridfyDocSignature() const
{
  return true;
}

GenRes Document::applyDocImpact2()
{
  return
  {
    false
  };
}


/**
 * @brief Document::mapDocToBlock
 * inserts a row in table "c_docs_blocks_map" to connect documents to block
 */
void Document::mapDocToBlock(
  const CBlockHashT& block_hash,
  const CDocIndexT doc_index)
{
  return mapDocToBlock(m_doc_hash, block_hash, doc_index);
}

void Document::mapDocToBlock(
  const CDocHashT& doc_hash,
  const CBlockHashT& block_hash,
  const CDocIndexT doc_index)
{
  QVDicT values = QVDicT {
    {"dbm_block_hash", block_hash},
    {"dbm_doc_index", doc_index}, //QString("%1").arg(doc_index)
    {"dbm_doc_hash", doc_hash},
    {"dbm_last_control", CUtils::getNow()}};

  CLog::log("--- connecting Doc(" + CUtils::hash8c(doc_hash) + ") to Block(" + CUtils::hash8c(block_hash) + ")");
  DbModel::insert(
    Document::stbl_docs_blocks_map,     // table
    values, // values to insert
    true);
}

bool Document::trxHasInput(const QString& document_type)
{
  return QStringList {
    CConsts::DOC_TYPES::BasicTx,
    CConsts::DOC_TYPES::RpDoc
  }.contains(document_type);
}

bool Document::trxHasInput()
{
  return Document::trxHasInput(m_doc_type);
}

bool Document::trxHasNotInput(const QString& document_type)
{
  return QStringList {
    CConsts::DOC_TYPES::Coinbase,
    CConsts::DOC_TYPES::DPCostPay,
    CConsts::DOC_TYPES::RlDoc
  }.contains(document_type);
}

bool Document::trxHasNotInput()
{
  return Document::trxHasNotInput(m_doc_type);
}

bool Document::isBasicTransaction(const QString& dType)
{
  // Note: the iConsts.DOC_TYPES.Coinbase  and iConsts.DOC_TYPES.DPCostPay altough are transactions, but have special tretmnent
  // Note: the iConsts.DOC_TYPES.RpDoc altough is a transaction, but since is created directly by node and based on validated coinbase info, so does not need validate
  return QStringList{CConsts::DOC_TYPES::BasicTx}.contains(dType);
}

bool Document::isBasicTransaction()
{
  return isBasicTransaction(m_doc_type);
}

bool Document::isDPCostPayment(const QString& dType)
{
  return QStringList{CConsts::DOC_TYPES::DPCostPay}.contains(dType);
}

bool Document::isDPCostPayment()
{
  return isDPCostPayment(m_doc_type);
}

bool Document::canBeACostPayerDoc(const QString& dType)
{
  return QStringList{CConsts::DOC_TYPES::BasicTx}.contains(dType);
}

/**
 * The documents which do not need another doc to pay their cost.
 * instead they can pay the cost of another docs
 *
 * @param {} dType
 */
bool Document::isNoNeedCostPayerDoc(const QString& dType)
{
  return (QStringList {
    CConsts::DOC_TYPES::BasicTx,
    CConsts::DOC_TYPES::DPCostPay,
    CConsts::DOC_TYPES::RpDoc,
    CConsts::DOC_TYPES::RlDoc
  }.contains(dType));
}

QString Document::stringifyInputs() const
{
  return SignatureStructureHandler::stringifyInputs(getInputs());
}

QString Document::stringifyOutputs() const
{
  return SignatureStructureHandler::stringifyOutputs(getOutputs());
}

// old name was trxHasOutput
bool Document::docHasOutput(const QString& document_type)
{
  return QStringList {
    CConsts::DOC_TYPES::Coinbase,
    CConsts::DOC_TYPES::BasicTx,
    CConsts::DOC_TYPES::RpDoc,
    CConsts::DOC_TYPES::RlDoc,}.contains(document_type);
}

bool Document::docHasOutput()
{
  return docHasOutput(m_doc_type);
}

void Document::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{
  CUtils::exiter("Import Costs To Treasury not implemented for base class block Type(" + block->m_block_type + ")", 97);
}

QVDRecordsT Document::searchInDocBlockMap(
  const ClausesT& clauses,
  const QStringList fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_docs_blocks_map,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}
