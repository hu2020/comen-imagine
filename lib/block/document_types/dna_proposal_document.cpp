#include "stable.h"

#include "document.h"
#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/services/polling/polling_handler.h"

#include "dna_proposal_document.h"

const QString DNAProposalDocument::stbl_dna_shares = "c_dna_shares";
const QString DNAProposalDocument::stbl_machine_draft_proposals = "c_machine_draft_proposals";
const QString DNAProposalDocument::stbl_proposals = "c_proposals";


DNAProposalDocument::DNAProposalDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

DNAProposalDocument::~DNAProposalDocument()
{

}

bool DNAProposalDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  m_help_hours = obj.value("helpHours").toInt();
  m_help_level = obj.value("helpLevel").toInt();
  m_project_hash = obj.value("projectHash").toString();
  m_contributor_account = obj.value("contributor").toString();
  m_polling_profile = obj.value("pollingProfile").toString();
  m_polling_version = obj.value("pollingVersion").toString();

  m_voting_timeframe = obj.value("pTimeframe").toDouble();
  if(CConsts::TIME_GAIN == 1)
    m_voting_timeframe = static_cast<uint64_t>(m_voting_timeframe);     // because of test ambient the longivity can be float and les than 1 hour

  return true;
}

QJsonObject DNAProposalDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document["projectHash"] = m_project_hash;
  document["helpHours"] = QVariant::fromValue(m_help_hours).toInt();
  document["helpLevel"] = QVariant::fromValue(m_help_level).toInt();
  document["contributor"] = m_contributor_account;
  document["pTimeframe"] = QVariant::fromValue(m_voting_timeframe).toDouble();

  if (ext_info_in_document)
    document["dExtInfo"] = m_doc_ext_info;

  document["pollingProfile"] = m_polling_profile;
  document["pollingVersion"] = m_polling_version;

  return document;
}

//QJsonObject DNAProposalDocument::ZexportJson() const
//{
//  QJsonObject params = QJsonObject
//  {
//    {"m_doc_hash", m_doc_hash},
//    {"m_doc_tags", m_doc_tags},
//    {"m_project_hash", m_project_hash},
//    {"m_doc_title", m_doc_title},
//    {"m_doc_comment", m_doc_comment},
//    {"m_shareholder", m_contributor_account}, // creator  contributor
//    {"m_help_hours", QString::number(m_help_hours)},
//    {"m_help_level", QString::number(m_help_level)},

//    {"m_votes_yes", QString::number(m_votes_yes)},
//    {"m_votes_abstain", QString::number(m_votes_abstain)},
//    {"m_votes_no", QString::number(m_votes_no)},
//    {"m_voting_timeframe", QString::number(m_voting_timeframe)},
//    {"m_polling_profile", m_polling_profile},
//    {"m_doc_creation_date", m_doc_creation_date},
//    {"m_block_creation_date", m_block_creation_date},

//    {"m_shares", QString::number(m_shares)},
//    {"m_approval_date", m_approval_date},

//  //    {"comment", QJsonValue(QString::fromStdString("Polling for proposal(" + CUtils::hash6c(proposal.getDocHash()) + "), " + proposal.m_doc_title + " "))},
//  //    {"status", QJsonValue(QString::fromStdString(CConsts::OPEN))}
//    };
//  return params;
//}


QString DNAProposalDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject Jdoc = exportDocToJson(ext_info_in_document);

  // recaluculate block final length
  Jdoc["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(Jdoc).length());

  CLog::log("4 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(Jdoc).length()) + " serialized document: " + CUtils::serializeJson(Jdoc), "app", "trace");

  return CUtils::serializeJson(Jdoc);
}


// old name was calcProposalDocumetnCost
std::tuple<bool, CMPAIValueT> DNAProposalDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;

  CMPAIValueT the_cost =
      dLen *
      SocietyRules::getBasePricePerChar(cDate) *
      SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);

  if (stage == CConsts::STAGES::Creating)
  {
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc cutom post the_cost + machine interest(" + CUtils::sepNum(the_cost) +" micro PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }
  return {true, CUtils::CFloor(the_cost)};
}

QString DNAProposalDocument::getRef() const
{
  return m_doc_ref;
}

QString DNAProposalDocument::calcDocExtInfoHash() const
{
  return CConsts::NO_EXT_HASH;
}

bool DNAProposalDocument::hasSignable() const
{
  return false;
}

//// old name was
//QString DNAProposalDocument::getDocSignMsg() const
//{
//  QString signables = "{";
////  signables += "\"creation Date\":\"" + m_doc_creation_date + "\"," ;
////  signables += "\"dClass\":\"" + m_doc_class + "\"," ;
////  signables += "\"dType\":\"" + m_doc_type + "\"," ;
////  signables += "\"dVer\":\"" + m_doc_version + "\"," ;
////  signables += "\"ref\":\"" + m_doc_ref + "\"," ;
////  signables += "\"vote\":" + QString::number(m_vote) + "," ;
////  signables += "\"voter\":\"" + m_voter + "\"}" ;

////  QString sign_message = CCrypto::keccak256(signables);
////  sign_message = sign_message.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
////  CLog::log("Ballot sign_message(" + sign_message + ") signables: " + signables + " ", "app", "trace");
////  return sign_message;
//}

//bool DNAProposalDocument::veridfyDocSignature() const
//{
//  QJsonObject dExtInfo = m_doc_ext_info;
////  auto unlockSet = dExtInfo.value("uSet").toObject();

////  bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
////    unlockSet,
////    m_voter);

////  if (!is_valid_unlock)
////  {
////    CLog::log("Invalid creator signature structure on Ballot(" + m_doc_hash + ")! ", "sec", "error");
////    return false;
////  }

////  // ballot signature & permission validate check
////  QString sign_message = getDocSignMsg();
////  QJsonArray signatures = m_doc_ext_info[0].toObject().value("signatures").toArray();
////  for (CSigIndexT signature_index = 0; signature_index < signatures.size(); signature_index++)
////  {
////    QString a_signature = signatures[signature_index].toString();
////    try {
////      bool verifyRes = CCrypto::ECDSAVerifysignature(
////        unlockSet.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
////        sign_message,
////        a_signature);
////      if (!verifyRes)
////      {
////        CLog::log("Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
////        return false;
////      }
////    } catch (std::exception) {
////      CLog::log("Exception! Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
////      return false;
////    }
////  }
//  return false;
//}

// js name was extractHashableParts
QString DNAProposalDocument::getDocHashableString() const
{
  QString hahsables = "";
  hahsables = "{";
  hahsables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  hahsables += "\"dClass\":\"" + m_doc_class + "\",";
  hahsables += "\"dComment\":\"" + m_doc_comment + "\",";
  hahsables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\",";
  hahsables += "\"dTags\":\"" + m_doc_tags + "\",";
  hahsables += "\"dTitle\":\"" + m_doc_title + "\",";
  hahsables += "\"dType\":\"" + m_doc_type + "\",";
  hahsables += "\"dVer\":\"" + m_doc_version + "\",";
  hahsables += "\"helpHours\":" + QString::number(m_help_hours) + ",";
  hahsables += "\"helpLevel\":" + QString::number(m_help_level) + ",";
  hahsables += "\"pollingProfile\":\"" + m_polling_profile + "\",";
  hahsables += "\"pollingVersion\":\"" + m_polling_version + "\",";
  hahsables += "\"projectHash\":\"" + m_project_hash + "\",";
  hahsables += "\"contributor\":\"" + m_contributor_account + "\",";
  hahsables += "\"pTimeframe\":" + QString::number(m_voting_timeframe) + "}";
  return hahsables;
}

// old name was doHashProposal
QString DNAProposalDocument::calcDocHash() const
{
  CLog::log("calc DNA proposal Hash: " + safeStringifyDoc(), "app", "trace");
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("\nHashable string for adm Polling doc doc(" + m_doc_type + " / " +
    m_doc_class + ") hash(" + hash + ")" + hashables, "app", "trace");
  return hash;
}

std::tuple<bool, QJsonArray> DNAProposalDocument::exportInputsToJson() const
{
  return {false, QJsonArray {}};
}

std::tuple<bool, QJsonArray> DNAProposalDocument::exportOutputsToJson() const
{
  return {false, QJsonArray {}};
}

std::vector<TInput*> DNAProposalDocument::getInputs() const
{
  return {};
}

std::vector<TOutput*> DNAProposalDocument::getOutputs() const
{
  return {};
}

// js name was activatePollingForProposal
bool DNAProposalDocument::applyDocFirstImpact(const Block& block) const
{
  // recording block in DAG means also starting voting period for proposals inside block(if exist)
  // it implicitly leads to create a new polling and activate it in order to collect shareholders opinion about proposal
  CLog::log("Try to activate Proposal Polling(" + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block.m_block_hash) + ")");  //  activatePollingForProposal(block);

  // record in c_proposals (i_proposal)
  CLog::log("recordANewProposal args: ");
  QueryRes dbl = DbModel::select(
    DNAProposalDocument::stbl_proposals,
    QStringList {"pr_hash"},
    {ModelClause("pr_hash", m_doc_hash)} //clauses
  );
  if (dbl.records.size()> 0) {
    QString msg = "try to double insert existed proposal " + CUtils::hash8c(m_doc_hash);
    CLog::log(msg, "sec", "error");
  }
  uint64_t helpLevel = static_cast<uint64_t>(m_help_level);
  QVDicT values {
    {"pr_hash", m_doc_hash},
    {"pr_type", m_doc_type},
    {"pr_class", m_doc_class},
    {"pr_version", m_doc_version},
    {"pr_title", m_doc_title},
    {"pr_descriptions", m_doc_comment},
    {"pr_tags", m_doc_tags},
    {"pr_project_id", m_project_hash},
    {"pr_help_hours", QString("%1").arg(m_help_hours)},
    {"pr_help_level", QString("%1").arg(helpLevel)},
    {"pr_voting_timeframe", QString("%1").arg(m_voting_timeframe)},
    {"pr_polling_profile", m_polling_profile},
    {"pr_contributor_account", m_contributor_account},
    {"pr_start_voting_date", block.m_block_creation_date},
    {"pr_conclude_date", QString::fromStdString("")},
    {"pr_approved", CConsts::NO}};

  DbModel::insert(
    DNAProposalDocument::stbl_proposals,
    values);


  // create a new polling
  QJsonObject params {
    {"dType", CConsts::DOCUMENT_TYPES::Polling},
    {"dClass", m_polling_profile}, // default is iConsts.POLLING_PROFILE_CLASSES.Basic.ppName
    {"dVer", m_doc_version},
    {"dRef", m_doc_hash},
    {"dRefType", CConsts::POLLING_REF_TYPE::Proposal},
    {"dRefClass", CConsts::PLEDGE_CLASSES::PledgeP},
    {"startDate", block.m_block_creation_date},
    {"pTimeframe", QVariant::fromValue(m_voting_timeframe).toDouble()},
    {"dCreator", m_contributor_account},
    {"related_proposal", m_doc_hash},
    {"dComment", "Polling for proposal(" + CUtils::hash6c(m_doc_hash) + "), " + m_doc_title + " "},
    {"status", CConsts::OPEN}};

  bool res = PollingHandler::autoCreatePollingForProposal(params);

  if (block.m_block_type != CConsts::BLOCK_TYPES::Genesis)
    CGUI::signalUpdateContributes();

  return res;
}

GenRes DNAProposalDocument::insertDocShares()
{
  QueryRes exist = DbModel::select(
    DNAProposalDocument::stbl_dna_shares,
    QStringList {"dn_doc_hash"},     // fields
    {ModelClause("dn_doc_hash", m_doc_hash)}
    );
  if (exist.records.size() > 0)
  {
    // maybe some updates
    return
    {
      false,
      "The DNA document (${utils.hash6c(dna.hash)}) is already recorded"
    };
  }

  return { false };
}


GenRes DNAProposalDocument::updateProposal(
  const QVDicT& values,
  const ClausesT& clauses,
  const bool& is_transactional)
{
  DbModel::update(
    DNAProposalDocument::stbl_proposals,
    values, // update values
    clauses,  // update clauses
    is_transactional);

  return { true };
}

void DNAProposalDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{

  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  if (block_inspect_container->m_block_alter_treasury_incomes.contains("TP_PROPOSAL"))
  {
    CLog::log("Try to import TP_PROPOSAL for block(" + CUtils::hash8c(block->getBlockHash()) + "): " + UTXOImportDataContainer::dumpMe(block_inspect_container->m_block_alter_treasury_incomes["TP_PROPOSAL"]), "trx", "trace");

    QSDicT mapPledgeHashToPayerTrxHash {};
    QSDicT mapProposalToPledgedContract {};
    for (Document* aDoc: block->getDocuments())
    {
      // retrieve proposals & pledge mappings
      if (
        (aDoc->m_doc_type == CConsts::DOC_TYPES::Pledge) &&
        (aDoc->m_doc_class == CConsts::PLEDGE_CLASSES::PledgeP) &&
        (aDoc->getProposalRef() != ""))
      {
        if(aDoc->getPayerTrxLinkBack() != "")
          mapPledgeHashToPayerTrxHash[aDoc->getDocHash()] = aDoc->getPayerTrxLinkBack();
        mapProposalToPledgedContract[aDoc->getProposalRef()] = aDoc->getDocHash();
      }
    }


    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_PROPOSAL"])
    {
      // if proposal costs was payed by Pledging & a lending transaction
      bool doc_cost_is_payed = true;

      if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"Supporter transaction(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") for Proposal is rejected because of doublespending", false};
      }

      if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"The proposal costs is not supported by any trx!", false};
      }

      CDocHashT proposal_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];

      auto[_index, proposal_doc] = block->getDocumentByHash(proposal_hash);
      Q_UNUSED(_index);

      if (proposal_doc->m_doc_class != CConsts::PROPOSAL_CLASESS::General)
        cost_payment_status[proposal_hash] = {"Proposal dClass(" + proposal_doc->m_doc_class + ") is not supported yet", false};


      QString income_title = "";
      CDocHashT payer_trx_hash = block_inspect_container->m_map_U_trx_ref_to_trx_hash[proposal_hash];

      CDocHashT supporter_pledge = "";
      if (mapProposalToPledgedContract.keys().contains(proposal_hash))
      {
        // the proposer, in order to pay proposal costs pledged its future incomes
        supporter_pledge = mapProposalToPledgedContract[proposal_hash];
        if (!mapPledgeHashToPayerTrxHash.keys().contains(supporter_pledge))
        {
          doc_cost_is_payed = false;
          cost_payment_status[proposal_hash] = {"Proposal is not payed by a transaction", false};
        }


        CDocHashT payer_trx_hash2 = mapPledgeHashToPayerTrxHash[supporter_pledge];
        if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(payer_trx_hash2) ||
            (block_inspect_container->m_map_U_trx_hash_to_trx_ref[payer_trx_hash2] != proposal_hash))
        {
          doc_cost_is_payed = false;
          cost_payment_status[proposal_hash] = {"Supporter transaction is referred to different doc", false};
        }


        if (payer_trx_hash != payer_trx_hash2)
        {
          doc_cost_is_payed = false;
          cost_payment_status[proposal_hash] = {"Not same payer_trx_hash! " + CUtils::hash8c(payer_trx_hash) + "!=" + CUtils::hash8c(payer_trx_hash2) + " !!", false};
        }


        income_title = "TP_PROPOSAL Proposal(" + CUtils::hash8c(proposal_hash) + ") Pledge(" + CUtils::hash8c(supporter_pledge) + ") Trx(" + CUtils::hash8c(payer_trx_hash) + ")";

      } else {
        // probably the proposal costs payed by a transaction directly signed by proposer
        income_title = "TP_PROPOSAL Proposal(" + CUtils::hash8c(proposal_hash) + ") Trx(" + CUtils::hash8c(payer_trx_hash) + ")";

      }

      if (block_inspect_container->m_rejected_transactions.keys().contains(payer_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[proposal_hash] = {"Supporter transaction is rejected because of doublespending", false};
      }

      if (!block_inspect_container->m_map_U_trx_ref_to_trx_hash.keys().contains(proposal_hash) ||
          (block_inspect_container->m_map_U_trx_ref_to_trx_hash[proposal_hash] != payer_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[proposal_hash] = {"supporter transaction ref is defferent than proposal hash", false};
      }

      cost_payment_status[proposal_hash].m_is_payed = doc_cost_is_payed;
      if (doc_cost_is_payed)
      {
        CLog::log("Successfully TP_PROPOSAL Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_PROPOSAL)", "app", "trace");

        cost_payment_status[proposal_hash].m_message = "Proposal Cost imported to treasury succsessfully.";
        TreasuryHandler::insertIncome(
          income_title,
          "TP_PROPOSAL",
          income_title,
          block->m_block_creation_date,
          a_treasury_entry.m_value,
          block->getBlockHash(),
          a_treasury_entry.m_coin);

      } else {
          CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_PROPOSAL)", "sec", "error");
          CLog::log("Failed cost_payment_status: Proposal(" + CUtils::hash8c(proposal_hash) + ")" + UTXOImportDataContainer::dumpMe(cost_payment_status[proposal_hash]), "sec", "error");

          // since by adding block to DAG, the proposals(and related pollings) also were added to DAG, and after 12 hours we found the payment
          // for that particulare proposal is failed, so we must remove both (proposal & polling) from data base
          // also removing pledge!(if exist)
          ProposalHandler::removeProposal(proposal_hash);

          PollingHandler::removePollingByRelatedProposal(proposal_hash);

          if (supporter_pledge != "")
            GeneralPledgeHandler::removePledgeBecauseOfPaymentsFail(supporter_pledge);

      }
    }
  }

  block_inspect_container->m_cost_payment_status["TP_PROPOSAL"] = cost_payment_status;
}
