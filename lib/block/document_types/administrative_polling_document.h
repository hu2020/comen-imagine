#ifndef ADMINISTRATIVEPOLLINGDOCUMENT_H
#define ADMINISTRATIVEPOLLINGDOCUMENT_H

class SocietyRules;
class Document;

class AdministrativePollingDocument: public Document
{
public:
  AdministrativePollingDocument(const QJsonObject& obj);
  ~AdministrativePollingDocument();

  QString m_polling_subject = "";
  double m_voting_timeframe = 0;
  QJsonObject m_polling_values = {};
  QString m_polling_creator = "";
  QString m_doc_comment = "";

  bool setByJsonObj(const QJsonObject& obj) override;
  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;
  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;

  bool applyDocFirstImpact(const Block& block) const override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;

  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;

  bool hasSignable() const override;
  QString getDocToBeSignedHash() const override;
  QString getDocSignMsg() const override;
  bool veridfyDocSignature() const override;

  QString safeStringifyPollingValues() const;

  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);

};

#endif // ADMINISTRATIVEPOLLINGDOCUMENT_H
