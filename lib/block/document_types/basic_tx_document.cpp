#include "stable.h"

#include "document.h"
#include "lib/ccrypto.h"
#include "lib/utils/version_handler.h"
#include "lib/transactions/trx_utils.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/transactions/basic_transactions/basic_transaction_handler.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"

#include "basic_tx_document.h"


BasicTxDocument::BasicTxDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

BasicTxDocument::~BasicTxDocument()
{
  deleteInputs();
  deleteOutputs();
}


bool BasicTxDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  if (obj.value("dPIs").toArray().size() > 0)
  {
    m_data_and_process_payment_indexes = {};
    for (auto a_dpi: obj.value("dPIs").toArray())
    {
      m_data_and_process_payment_indexes.push_back(a_dpi.toDouble());
    }
  }

  if (obj.value("inputs").toArray().size() > 0)
    setDocumentInputs(obj.value("inputs"));

  if (obj.value("outputs").toArray().size() > 0)
    setDocumentOutputs(obj.value("outputs"));

  return true;
}

QString BasicTxDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject document = exportDocToJson(ext_info_in_document);

//  // recaluculate block final length
//  document["dLen"] = "0000000";
//  document["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(document).length());

  QString res = CUtils::serializeJson(document);
  CLog::log("3 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length: " + CUtils::sepNum(res.length()) + " serialized document: " + res, "app", "trace");

  return res;
}

QString BasicTxDocument::calcDocExtInfoHash() const //JS name was calcTrxExtRootHash()
{
  QStringList hashes = {};
  for (auto an_ext_info_: m_doc_ext_info)
  {
    QJsonObject an_ext_info = an_ext_info_.toObject();
    QString hashables = "{";
    hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(an_ext_info.value("uSet").toObject()) + ",";
    hashables += "\"signatures\":" + CUtils::serializeJson(an_ext_info.value("signatures").toArray()) + "}";
    QString hash = CCrypto::keccak256(hashables);
    CLog::log("Doc Ext Root Hash Hashables Doc(" + m_doc_hash + ") Regenrated Ext hash: " + hash + "\nhashables: " + hashables, "app", "trace");

    hashes.append(hash);
  }
  auto[root, final_verifies, version, levels, leaves] = CMerkle::generate(hashes);
  Q_UNUSED(final_verifies);
  Q_UNUSED(version);
  Q_UNUSED(levels);
  Q_UNUSED(leaves);
  return root;
}

std::tuple<bool, CMPAIValueT> BasicTxDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extra_length) const //calcTrxDPCost
{
  if (cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;

  if (stage == CConsts::STAGES::Creating)
    dLen += extra_length + CConsts::TRANSACTION_PADDING_LENGTH;

  if (stage == CConsts::STAGES::Validating)
  {
    if (dLen != static_cast<DocLenT>(calcDocLength()))
    {
      CLog::log("The trx len and local re-calc len are not same! stage(" + stage + ") remoteLen(" + QString::number(dLen) + ") local Len(" + QString::number(calcDocLength()) + ") trx(" + CUtils::hash8c(m_doc_hash) + ")", "trx", "error");
      return {false, 0};
    }
  } else {
    if (dLen < static_cast<DocLenT>(calcDocLength()))
    {
      CLog::log("The trx len and local re-calc len are not same! stage(" + stage + ") remoteLen(" + QString::number(dLen) + ") local Len(" + QString::number(calcDocLength()) + ") trx(" + CUtils::hash8c(m_doc_hash) + ")", "trx", "error");
      return {false, 0};
    }
  }

  if (m_doc_class == CConsts::TRX_CLASSES::P4P)
    dLen = dLen * getDPIs().size();  // the transaction which new transaction is going to pay for

  uint64_t theCost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);

  if (stage == CConsts::STAGES::Creating)
    theCost = theCost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen,
      extra_length,
      getDPIs().size());

  return {true, trunc(theCost) };
}

bool BasicTxDocument::deleteInputs()
{
  for (TInput* an_input: m_inputs)
    delete an_input;
  return true;
}

bool BasicTxDocument::deleteOutputs()
{
  for (TOutput* an_output: m_outputs)
    delete an_output;
  return true;
}

QJsonObject BasicTxDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  // impacting uSets
  if (ext_info_in_document)
    document["dExtInfo"] = SignatureStructureHandler::compactUnlockersArray(document["dExtInfo"].toArray());

  if (m_data_and_process_payment_indexes.size() > 0)
  {
    QJsonArray dPIs {};
    for(auto a_dpi: m_data_and_process_payment_indexes)
      dPIs.push_back(a_dpi);
    document["dPIs"] = dPIs;
  }

  return document;
}


std::tuple<bool, QJsonArray> BasicTxDocument::exportInputsToJson() const
{
  QJsonArray inputs {};
  for (TInput* an_input: m_inputs)
    inputs.append(QJsonArray{
      an_input->m_transaction_hash,
      an_input->m_output_index});
  return {true, inputs};
}

std::tuple<bool, QJsonArray> BasicTxDocument::exportOutputsToJson() const
{
  QJsonArray outputs {};
  for (TOutput* an_output: m_outputs)
    outputs.append(QJsonArray{
      an_output->m_address,
      QVariant::fromValue(an_output->m_amount).toDouble()});
  return {true, outputs};
}

QString BasicTxDocument::getDocHashableString() const
{
  QJsonArray dPIs {};
  for (auto a_payer: m_data_and_process_payment_indexes)
    dPIs.push_back(a_payer);

  QString hahsables = "{";
  hahsables += "\"dCDate\":\"" + m_doc_creation_date + "\",";
  hahsables += "\"dClass\":\"" + m_doc_class + "\",";
  hahsables += "\"dComment\":\"" + m_doc_comment + "\",";
  hahsables += "\"dExtHash\":\"" + m_doc_ext_hash + "\",";
  hahsables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\",";
  hahsables += "\"dPIs\":" + CUtils::serializeJson(dPIs) + ",";
  hahsables += "\"dRef\":\"" + getRef() + "\",";
  hahsables += "\"dType\":\"" + m_doc_type + "\",";
  hahsables += "\"dVer\":\"" + m_doc_version + "\",";
  hahsables += "\"inputs\":" + stringifyInputs() + ",";
  hahsables += "\"outputs\":" + stringifyOutputs() + "";
  hahsables += "}";
  return hahsables;
}



// TODO: some unit test for pure hashable
QString BasicTxDocument::extractHPureParts_simple() const
{
  // the hTrx MUST be constant and NEVER change the order of attribiutes (alphabetical)
  // in case of change the version MUST be changed and the code treats it in new manner
  std::vector<TInput*> normalized_inputs = TrxUtils::normalizeInputs(m_inputs);

  if (VersionHandler::isNewerThan(m_doc_version, "0.0.1"))
  {
    auto[status, normalized_outputs] = TrxUtils::normalizeOutputs(m_outputs);
    if (!status)
        return "";

    QString hahsables = "{";
    hahsables += "\"inputs\":" + SignatureStructureHandler::stringifyInputs(normalized_inputs) + ",";
    hahsables += "\"outputs\":" + SignatureStructureHandler::stringifyOutputs(normalized_outputs) + "}" ;

    return hahsables;

  } else {
    return SignatureStructureHandler::stringifyInputs(normalized_inputs);

  }
}

QString BasicTxDocument::extractTransactionPureHashableParts() const
{
    if (m_doc_type == CConsts::DOC_TYPES::BasicTx)
    {
      return extractHPureParts_simple();
    }
    return "";
}

QString BasicTxDocument::getPureHash() const
{
  QString hashable_string = extractTransactionPureHashableParts();
  QString hash = CCrypto::keccak256Dbl(hashable_string);    // NOTE: useing double hash for more security
  CLog::log("get Pure Hash res! hash(" + CUtils::hash8c(hash) + ") version(" + m_doc_version + ") hashabled string: (" + hashable_string + ") trx(" + CUtils::hash8c(getDocHash()) + ")", "trx", "trace");
  return hash;
}

// js name was
QString BasicTxDocument::calcDocHash() const
{
  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256Dbl(hashables); // NOTE: absolutely using double hash for more security

  // generate deterministic part of trx hash
  QString pure_hash = getPureHash();
  if (VersionHandler::isNewerThan(m_doc_version, "0.0.0"))
  {
    hash = pure_hash.midRef(32, 32).toString() + hash.midRef(32, 32).toString();
  } else {
    hash = pure_hash.midRef(0, 32).toString() + hash.midRef(0, 32).toString();
  }


  CLog::log("\nHashable string for Basic Trx doc(" + m_doc_type + " / " + m_doc_class +
    ") hash(" + hash + ") version(" + m_doc_version + ") hashables:" + hashables, "app", "trace");
  return hash;
}

bool BasicTxDocument::applyDocFirstImpact(const Block& block) const
{
  return false;
}

QString BasicTxDocument::getRef() const
{
  return m_doc_ref;
}

QVector<COutputIndexT> BasicTxDocument::getDPIs() const
{
  return m_data_and_process_payment_indexes;
}

std::vector<TInput*> BasicTxDocument::getInputs() const
{
  return m_inputs;
}

std::vector<TOutput*> BasicTxDocument::getOutputs() const
{
  return m_outputs;
}

bool BasicTxDocument::setDocumentInputs(const QJsonValue& obj)
{
  QJsonArray inputs = obj.toArray();
  for(QJsonValueRef an_input: inputs)
  {
    QJsonArray io = an_input.toArray();
    TInput* i  = new TInput({io[0].toString(), static_cast<COutputIndexT>(io[1].toVariant().toDouble())});
    m_inputs.push_back(i);
  }
  return true;
}

bool BasicTxDocument::setDocumentOutputs(const QJsonValue& obj)
{
  QJsonArray outputs = obj.toArray();
  for(QJsonValueRef an_output: outputs)
  {
    QJsonArray oo = an_output.toArray();
    TOutput *o  = new TOutput({oo[0].toString(), static_cast<CMPAIValueT>(oo[1].toDouble())});
    m_outputs.push_back(o);
  }
  return true;
}

bool BasicTxDocument::appendOutput(
  CAddressT address,
  CMPAIValueT value)
{
  TOutput *o  = new TOutput({address, static_cast<CMPAIValueT>(value)});
  m_outputs.push_back(o);
  return true;
}

bool BasicTxDocument::removeOutputByIndex(COutputIndexT index)
{
  delete m_outputs[index];
  m_outputs.erase(m_outputs.begin() + index, m_outputs.begin() + index + 1);
  return true;
}


int64_t BasicTxDocument::getOutputIndexByAddressValue(const QString& address_plus_value)
{
  for (COutputIndexT inx = 0; inx < m_outputs.size(); inx++)
  {
    if (m_outputs[inx]->m_address + m_outputs[inx]->m_amount == address_plus_value)
      return inx;
  }
  return -1;
}

QJsonArray BasicTxDocument::generateInputTuples()
{
  QJsonArray input_tuples {};
  for(TInput* the_in: m_inputs)
  {
    QJsonArray elm {the_in->m_transaction_hash, QVariant::fromValue(the_in->m_output_index).toDouble()};
    input_tuples.push_back(elm);
  }
  return input_tuples;
}

QJsonArray BasicTxDocument::generateOutputTuples()
{
  QJsonArray output_tuples {};
  for(TOutput* out: m_outputs)
  {
    QJsonArray elm {out->m_address, QVariant::fromValue(out->m_amount).toDouble()};
    output_tuples.push_back(elm);
  }
  return output_tuples;
}

QString BasicTxDocument::getInputOutputSignables(
  const QString& sig_hash,
  const CDateT& cDate)
{
  QJsonArray inputs = generateInputTuples();
  QJsonArray outputs = generateOutputTuples();
  return getInputOutputSignables(inputs, outputs, sig_hash, getDocRef(), cDate);
}



QString BasicTxDocument::getInputOutputSignables(
  const QJsonArray& inputs_,
  const QJsonArray& outputs_,
  const QString& sig_hash,
  const CDocHashT& document_ref,
  const CDateT& cDate)
{
  auto[inputs, outputs] = extractInputOutputs(inputs_, outputs_);

  QString signables = "{";
  signables += "\"dCDate\":\"" + cDate + "\",";
  if (document_ref != "")
    signables += "\"dRef\":\"" + document_ref + "\",";
  signables += "\"inputs\":" + SignatureStructureHandler::stringifyInputs(inputs) + ",";
  signables += "\"outputs\":" + SignatureStructureHandler::stringifyOutputs(outputs) + ",";
  signables += "\"sigHash\":\"" + sig_hash + "\"}";
  return signables;
}

QString BasicTxDocument::signingInputOutputs(
  const QString& private_key,
  const QJsonArray& inputs,
  const QJsonArray& outputs,
  const QString& sig_hash,
  const CDocHashT& document_ref,
  const CDateT& cDate)
{
  QString signables = getInputOutputSignables(inputs, outputs, sig_hash, document_ref, cDate);
  QString hash = CCrypto::keccak256Dbl(signables);      // because of securiy, MUST use double hash
  auto[status, signature_hex, signature] = CCrypto::ECDSAsignMessage(
    private_key,
    hash.midRef(0, CConsts::SIGN_MSG_LENGTH).toString());
  CLog::log("The transaction has been signed. signature(" + QString::fromStdString(signature_hex) + ") hash(" + hash + ") signables: " + signables, "trx", "info");
  return QString::fromStdString(signature_hex);
}

bool BasicTxDocument::verifyInputOutputsSignature(
  const QString& public_key,
  const QString& signature,
  const QString& sig_hash,
  const CDateT& cDate
      )
{
  if (public_key == "")
  {
    CLog::log("Missed public_key!", "app", "error");
    return false;
  }

  if (signature == "")
  {
    CLog::log("Missed signature!", "app", "error");
    return false;
  }

  if (sig_hash == "")
  {
    CLog::log("Missed sig_hash!", "app", "error");
    return false;
  }

  QString signables = getInputOutputSignables(sig_hash, cDate);
  signables = CCrypto::keccak256Dbl(signables);      // because of securiy, MUST use double hash
  try {
    bool verify_res = CCrypto::ECDSAVerifysignature(
      public_key,
      signables.midRef(0, CConsts::SIGN_MSG_LENGTH).toString(),
      signature);
    return verify_res;

  } catch (std::exception) {
    CLog::log("Failed in transaction signature verify trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ")", "trx", "error");
    return false;
  }

}


// js name was trxSignatureValidator
bool BasicTxDocument::validateSignatures(
    const QV2DicT& used_coins_dict,
    const QStringList& exclude_coins,
    const CBlockHashT& block_hash)
{
//  let block = args.block;
//  let trx = block.docs[args.docInx];
  QJsonArray doc_ext_info = m_doc_ext_info;


  QString msg;
  CDateT cDate = CUtils::getNow();

  // for each input must control if given unlock structutr will be finished in a right(and valid) output address?
  // the order of inputs and ext Info ARE IMPORTANT. the wallet MUST sign and send inputs in order to bip 69
  CLog::log("Signature validating for trx(" + CUtils::hash8c(m_doc_hash), "trx", "trace");

  QHash<CInputIndexT, QHash<CSigIndexT, QStringList> > the_coins_must_be_signed_by_a_single_sign_set {};
  for (CInputIndexT input_index = 0; input_index < m_inputs.size(); input_index++)
  {
    // for each input must find proper block and bech32 address of output and insert into validate function
    CCoinCodeT coin_code = m_inputs[input_index]->getCoinCode();
    // scape validating invalid inputs(in this case double-spended inputs)
    if (exclude_coins.contains(coin_code))
      continue;

    QJsonObject an_unlock_document = doc_ext_info[input_index].toObject();
    QJsonObject an_unlock_set = an_unlock_document["uSet"].toObject();

    bool is_valid_unlocker = SignatureStructureHandler::validateSigStruct(
      an_unlock_set,
      used_coins_dict[coin_code].value("ut_o_address").toString());

    if (!is_valid_unlocker)
    {
      msg = "Invalid block, because of invalid unlock struture! Block(" + CUtils::hash8c(block_hash) + ") transaction(" + CUtils::hash8c(getDocHash()) + ") input-index(" + QString::number(input_index) + " unlock structure: " + CUtils::dumpIt(an_unlock_set);
      CLog::log(msg, "trx", "error");
      return false;
    }

    // prepare a signature dictionary
    the_coins_must_be_signed_by_a_single_sign_set[input_index] = {};
    QJsonArray sign_sets = an_unlock_set.value("sSets").toArray();
    for (CSigIndexT singature_index = 0; singature_index < sign_sets.size(); singature_index++)
    {
      QJsonArray sigInfo = an_unlock_document.value("signatures").toArray()[singature_index].toArray();
      auto[inputs, outputs] = extractInputOutputs(
        m_inputs,
        m_outputs,
        sigInfo[1].toString());
      the_coins_must_be_signed_by_a_single_sign_set[input_index][singature_index] = QStringList {};
      for (auto an_inp: inputs)
        the_coins_must_be_signed_by_a_single_sign_set[input_index][singature_index].append(an_inp->getCoinCode());
    }
  }


  for (CInputIndexT input_index = 0; input_index < m_inputs.size(); input_index++)
  {
    auto the_input = m_inputs[input_index];
    // for each input must find proper block and bech32 address of output and insert into validate function
    CCoinCodeT coin_code = the_input->getCoinCode();

    // scape validating invalid inputs(in this case double-spended inputs)
    if (exclude_coins.contains(coin_code))
      continue;

    QJsonObject an_unlock_document = doc_ext_info[input_index].toObject();
    QJsonObject an_unlock_set = an_unlock_document["uSet"].toObject();
    QJsonArray sign_sets = an_unlock_set.value("sSets").toArray();

    // for each input and proper spending way, must control if the signature is valid?
    for (CSigIndexT singature_index = 0; singature_index < sign_sets.size(); singature_index++)
    {
      QJsonArray sigInfo = an_unlock_document.value("signatures").toArray()[singature_index].toArray();
      QJsonObject aSignSet = sign_sets[singature_index].toObject();
      bool is_verified = verifyInputOutputsSignature(
        aSignSet.value("sKey").toString(), //pubKey
        sigInfo[0].toString(), //signature
        sigInfo[1].toString(), //sig_hash
        m_doc_creation_date);
      if (!is_verified)
      {
        msg = "Invalid given signature for input(" + QString::number(input_index) + ") trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block_hash) + ") ";
        CLog::log(msg, "trx", "error");
        return false;
      }

      if (canHaveTimeLockedInput())
      {
        //// control input timeLocks
        //if (_.has(aSignSet, 'iTLock') && (aSignSet.iTLock > 0)) {
        //  clog.trx.info(`>>>>>>>>>>>>> signed RefLocs By A Single SignSet ${utils.stringify(the_coins_must_be_signed_by_a_single_sign_set)}`);
        //  let iTLock = iutils.convertBigIntToJSInt(aSignSet.iTLock);
        //  for (aRefLoc of the_coins_must_be_signed_by_a_single_sign_set[input_index][singature_index])
        //  {
        //      let inputCreationDate = used_coins_dict[aRefLoc].utRefCreationDate;
        //      let inputRedeemDate = utils.minutesAfter(iTLock, inputCreationDate);

        //      msg = `info::: inputTimeLock compareTime(${cDate}) block(${utils.hash6c(block.blockHash)}) trx(${utils.hash6c(trx.hash)}) `;
        //      msg += `input(${input_index} locked for ${iTLock} Minutes after creation) created on(${inputCreationDate}) `;
        //      msg += `has inputTimeLock and can be redeemed after(${inputRedeemDate}) `;
        //      if (cDate < inputRedeemDate) {
        //          msg = `\ninput is not useable because of ${msg}`;
        //          clog.sec.error(msg);
        //          return { err: true, msg, shouldPurgeMessage: true }
        //      } else {
        //          msg = `\ninput is released ${msg}`;
        //          clog.trx.info(msg);
        //      }
        //  }
        //}
      }
    }
  }

  msg = "All trx have valid signatures Block(" + CUtils::hash8c(block_hash) + ") ";
  CLog::log(msg, "trx", "trace");
  return true;
}

bool BasicTxDocument::canHaveTimeLockedInput()
{
  return false;
  // TODO: implement input time lock (both flexible and strict input timelock) ASAP
  return (
    (m_doc_type == CConsts::DOC_TYPES::BasicTx) &&
    QStringList{CConsts::TRX_CLASSES::SimpleTx, CConsts::TRX_CLASSES::P4P}.contains(m_doc_class)
  );
}


std::tuple<std::vector<TInput*>, std::vector<TOutput*> > BasicTxDocument::extractInputOutputs(QString sig_hash)
{
  if (sig_hash == CConsts::SIGHASH::ALL)
    return {m_inputs, m_outputs};

  if (sig_hash == CConsts::SIGHASH::NONE)
    return {m_inputs, {}};

  return {m_inputs, m_outputs};
}

std::tuple<std::vector<TInput*>, std::vector<TOutput*> > BasicTxDocument::extractInputOutputs(
  std::vector<TInput*> inputs,
  std::vector<TOutput*> outputs,
  QString sig_hash)
{
  if (sig_hash == CConsts::SIGHASH::ALL) {
    // change nothing
  } else if (sig_hash == CConsts::SIGHASH::NONE) {
    outputs = {};
  }
  return { inputs, outputs };
}

bool BasicTxDocument::validateGeneralRulsForTransaction()
{
  QString msg;
//  let localHash = trxHashHandler.doHashTransaction(transaction);
//  if (localHash != transaction.hash) {
//    msg = `Mismatch trx hash locally recalculated:${localHash} received: ${transaction.hash} block(${utils.hash6c(blockHash)})`;
//    return { err: true, msg: msg };
//  }


  if (CUtils::isGreaterThanNow(m_doc_creation_date))
  {
    msg = "Transaction whith future creation date is not acceptable " + getDocHash();
    CLog::log(msg, "sec", "error");
    return false;
  }

  return true;
}

/**
 * @brief BasicTxDocument::equationCheck
 * @param used_coins_dict
 * @param invalid_coins_dict
 * @return {status, msg, total_inputs_amounts, total_outputs_amounts}
 */
std::tuple<bool, QString, CMPAIValueT, CMPAIValueT> BasicTxDocument::equationCheck(
  const QV2DicT& used_coins_dict,
  const QV2DicT& invalid_coins_dict,
  const CBlockHashT& block_hash)
{
  QString msg;
  CMPAIValueT total_inputs_amounts = 0;
  CMPAIValueT total_outputs_amounts = 0;

  if (m_inputs.size() > 0)
  {
    for (TInput* input: m_inputs)
    {
      CCoinCodeT a_coin_code = input->getCoinCode();
      if (used_coins_dict.keys().contains(a_coin_code))
      {
        if (used_coins_dict[a_coin_code].value("ut_o_value").toDouble() >= MAX_COIN_VALUE)
        {
          msg = "The transaction has input bigger than MAX_SAFE_INTEGER! trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block_hash) + ")  value: " + CUtils::microPAIToPAI6(used_coins_dict[a_coin_code].value("ut_o_value").toDouble());
          CLog::log(msg, "sec", "error");
          return {false, msg, 0, 0};
        }
        total_inputs_amounts += used_coins_dict[a_coin_code].value("ut_o_value").toDouble();

      } else {
        /**
        * trx uses already spent outputs! so try invalid_coins_dict
        * probably it is a double-spend case, which will be decided after 12 hours, in importing step
        * BTW ALL trx must have balanced equation (even duoble-spendeds)
        */
        if (invalid_coins_dict.keys().contains(a_coin_code))
        {
          if (invalid_coins_dict[a_coin_code].value("coinGenOutputValue").toDouble() >= MAX_COIN_VALUE)
          {
            msg = "The transaction has inv-input bigger than MAX_SAFE_INTEGER! trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block_hash) + ")  value: " + CUtils::microPAIToPAI6(invalid_coins_dict[a_coin_code].value("coinGenOutputValue").toDouble());
            CLog::log(msg, "sec", "error");
            return {false, msg, 0, 0};
          }
          total_inputs_amounts += invalid_coins_dict[a_coin_code].value("coinGenOutputValue").toDouble();

        } else {
          msg = "The input absolutely missed! not in tables neither in DAG! coin(" + a_coin_code + ") trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block_hash) + ")";
          CLog::log(msg, "sec", "error");
          return {false, msg, 0, 0};
        }
      }
    }
  }

  if (m_outputs.size() > 0)
  {
    for (TOutput* output: m_outputs)
    {
      if (output->m_address != CUtils::stripOutputAddress(output->m_address))
      {
        msg = "The transaction has not digit charecters in bech 32 address(" + output->m_address + ")! trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block_hash) + ")";
        CLog::log(msg, "sec", "error");
        return {false, msg, 0, 0};
      }

      if (output->m_amount == 0)
      {
        msg = "The transaction has zero output! trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block_hash) + ")";
        CLog::log(msg, "sec", "error");
        return {false, msg, 0, 0};
      }

      if (output->m_amount < 0)
      {
        msg = "The transaction has negative output! trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block_hash) + ")";
        CLog::log(msg, "sec", "error");
        return {false, msg, 0, 0};
      }

      if (output->m_amount >= MAX_COIN_VALUE)
      {
        msg = "The transaction has output bigger than MAX_SAFE_INTEGER! trx(" + m_doc_type + " / " + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block_hash) + ")";
        CLog::log(msg, "sec", "error");
        return {false, msg, 0, 0};
      }

      total_outputs_amounts += output->m_amount;
    }
  }

  return {true, msg, total_inputs_amounts, total_outputs_amounts};
}

GenRes BasicTxDocument::customValidateDoc(const Block* block) const
{
  QString msg;
  if (m_doc_class == CConsts::TRX_CLASSES::P4P)
  {
    if (!CConsts::SUPPORTS_P4P_TRANSACTION)
    {
      msg = "Network! still doen't support P4P transactions!";
      CLog::log(msg, "trx", "error");
      return {false, msg};
    }
  }

  for (auto an_output: m_outputs)
  {
    if (an_output->m_amount == 0)
    {
      msg = "creating block, the transaction has zero output! trx(" + CUtils::hash8c(getDocHash()) + ")  ";
      CLog::log(msg, "trx", "error");
      return {false, msg};
    }

    if (an_output->m_amount < 0)
    {
      msg = "creating block, the transaction has negative output! trx(" + CUtils::hash8c(getDocHash()) + ")  ";
      CLog::log(msg, "trx", "error");
      return {false, msg};
    }

  }

  return {true, ""};
}

CMPAISValueT BasicTxDocument::getDocCosts() const
{
  CMPAISValueT costs = 0;
  for (auto a_cost_index: getDPIs())
    costs += m_outputs[a_cost_index]->m_amount;
  return costs;
}

std::tuple<QJsonArray, QJsonArray> BasicTxDocument::extractInputOutputs(
  QJsonArray inputs,
  QJsonArray outputs,
  QString sig_hash)
{
  if (sig_hash == CConsts::SIGHASH::ALL) {
    // change nothing
  } else if (sig_hash == CConsts::SIGHASH::NONE) {
    outputs = {};
  }
  return { inputs, outputs };

  ////TODO: implement CUSTOM
  //case iConsts.SIGHASH.CUSTOM:
  //    let cust = {
  //        inputs: ['fdde3ddwdwedewwdded3:2', 'fdde3ddwdwedewwd656:10', 'fdde3d6yydyyewwdded3:4'],
  //        outputs: ['ALL']
  //    }
  //    cust = {
  //        inputs: 'ALL',
  //        outputs: ['fdde3ddwdwedewwdded3:2', 'fdde3ddwdwedewwd656:10', 'fdde3d6yydyyewwdded3:4']
  //    }
  //    cust = {
  //        inputs: ['fdde3ddwdwedewwdded3:2', 'fdde3ddwdwedewwd656:10', 'fdde3d6yydyyewwdded3:4'],
  //        outputs: ['fdde3ddwdwedewwdded3:2', 'fdde3ddwdwedewwd656:10', 'fdde3d6yydyyewwdded3:4']
  //    }
  //    inputs = args.inputs;
  //    outputs = args.outputs;
  //    break;
  // since they have conflict with BIP69 all are disabled
  // instead, use custom
  // case iConsts.SIGHASH['SINGLE']:
  //     inputs = args.inputs;
  //     outputs = [args.outputs[args.selectedIndex]];
  //     break;
  // case iConsts.SIGHASH['ALL|ANYONECANPAY']:
  //     inputs = [args.inputs[args.selectedIndex]];
  //     outputs = args.outputs;
  //     break;
  // case iConsts.SIGHASH['NONE|ANYONECANPAY']:
  //     inputs = [args.inputs[args.selectedIndex]];
  //     outputs = [];
  //     break;
  // case iConsts.SIGHASH['SINGLE|ANYONECANPAY']:
  //     inputs = [args.inputs[args.selectedIndex]];
  //     outputs = [args.outputs[args.selectedIndex]];
  //     break;

}


