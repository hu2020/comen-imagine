#ifndef BASICTXDOCUMENT_H
#define BASICTXDOCUMENT_H


class BasicTxDocument : public Document
{
public:
  BasicTxDocument(const QJsonObject& obj);
  ~BasicTxDocument();

  std::vector<TInput*> m_inputs = {};
  std::vector<TOutput*> m_outputs = {};
  QVector<COutputIndexT> m_data_and_process_payment_indexes = {};  // dPIs

  bool setByJsonObj(const QJsonObject& obj) override;
  bool setDocumentInputs(const QJsonValue& obj) override;
  bool setDocumentOutputs(const QJsonValue& obj) override;

  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;
  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;

  QString getRef() const override;

  CMPAISValueT getDocCosts() const override;

  bool deleteInputs() override;
  bool deleteOutputs() override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;
  QVector<COutputIndexT> getDPIs() const override;

  std::vector<TInput*> getInputs() const override;
  std::vector<TOutput*> getOutputs() const override;

  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

  GenRes customValidateDoc(const Block* block) const override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;

  bool applyDocFirstImpact(const Block& block) const override;


  //  -  -  -  not drived functions

  static QString getInputOutputSignables(
    const QJsonArray& inputs_,
    const QJsonArray& outputs_,
    const QString& sigHash,
    const CDocHashT& document_ref,
    const CDateT& cDate);

  static QString signingInputOutputs(
    const QString& private_key,
    const QJsonArray& inputs_,
    const QJsonArray& outputs_,
    const QString& sigHash,
    const CDocHashT& document_ref = "",
    const CDateT& cDate = CUtils::getNow());

  static std::tuple<std::vector<TInput*>, std::vector<TOutput*> > extractInputOutputs(
    std::vector<TInput*> inputs,
    std::vector<TOutput*> outputs,
    QString sigHash);

  static std::tuple<QJsonArray, QJsonArray> extractInputOutputs(
    QJsonArray inputs,
    QJsonArray outputs,
    QString sigHash = CConsts::SIGHASH::ALL);

  std::tuple<bool, QString, CMPAIValueT, CMPAIValueT> equationCheck(
    const QV2DicT& used_coins_dict,
    const QV2DicT& invalid_coins_dict,
    const CBlockHashT& block_hash = "dummy_block_hash");

  bool canHaveTimeLockedInput();

  QJsonArray generateInputTuples();
  QJsonArray generateOutputTuples();

  QString getInputOutputSignables(
    const QString& sig_hash,
    const CDateT& cDate);

  bool verifyInputOutputsSignature(
    const QString& public_key,
    const QString& signature,
    const QString& sig_hash = CConsts::SIGHASH::ALL,
    const CDateT& cDate = CUtils::getNow());

  std::tuple<std::vector<TInput*>, std::vector<TOutput*> > extractInputOutputs(QString sigHash);

  bool validateGeneralRulsForTransaction();

  bool validateSignatures(
    const QV2DicT& used_coins_dict,
    const QStringList& exclude_coins = {},
    const CBlockHashT& block_hash = "");

  QString extractHPureParts_simple() const;
  QString extractTransactionPureHashableParts() const;
  QString getPureHash() const;

  bool appendOutput(
    CAddressT address,
    CMPAIValueT value);

  bool removeOutputByIndex(COutputIndexT index);
  int64_t getOutputIndexByAddressValue(const QString& address_plus_value);


};

#endif // BASICTXDOCUMENT_H
