#ifndef DNAPROPOSALDOCUMENT_H
#define DNAPROPOSALDOCUMENT_H

#include "document.h"
#include "lib/services/polling/polling_handler.h"

class DNAProposalDocument : public Document
{
public:
  DNAProposalDocument(){};
  DNAProposalDocument(const QJsonObject& obj);
  ~DNAProposalDocument();

  static const QString stbl_dna_shares;
  static const QString stbl_machine_draft_proposals;
  static const QString stbl_proposals;

  CDocHashT m_project_hash = "";//58be4875eaa3736f60622e26bda746fb81812e8d7ecad50a2c3f97f0605a662c
  CDateT m_approval_date = "";
  CAddressT m_contributor_account = "";
  uint64_t m_help_hours = 0;
  uint64_t m_help_level = 0;
  uint64_t m_shares = 0;
  uint64_t m_votes_yes = 0;
  uint64_t m_votes_abstain = 0;
  uint64_t m_votes_no = 0;
  TimeByHoursT m_voting_timeframe = 24;
  QString m_polling_profile = "";//Basic
  QString m_polling_version = "";


  bool setByJsonObj(const QJsonObject& obj) override;

  QString safeStringifyDoc(const bool ext_info_in_document = true) const override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;  // calcTrxExtRootHash

  std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const override;

  QString getRef() const override;


  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;
  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

  std::vector<TInput*> getInputs() const override;
  std::vector<TOutput*> getOutputs() const override;

  bool applyDocFirstImpact(const Block& block) const override;

  bool hasSignable() const override;
//  QString getDocSignMsg() const override;
//  bool veridfyDocSignature() const override;

  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);

  static GenRes updateProposal(const QVDicT& values, const ClausesT& clauses, const bool& is_transactional = false);

  GenRes insertDocShares();
//  QJsonObject exportJson() const override;
//  virtual GenRes applyDocImpact2() override;
};

#endif // DNAPROPOSALDOCUMENT_H
