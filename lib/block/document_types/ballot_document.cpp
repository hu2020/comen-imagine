#include "stable.h"

#include "lib/ccrypto.h"
#include "document.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/services/polling/ballot_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/dag/normal_block/import_utxos/utxo_import_data_container.h"

#include "ballot_document.h"

BallotDocument::BallotDocument(const QJsonObject& obj)
{
  setByJsonObj(obj);
}

BallotDocument::~BallotDocument()
{

}

bool BallotDocument::setByJsonObj(const QJsonObject& obj)
{
  Document::setByJsonObj(obj);

  if (obj.value("dRef").toString() != "")
    m_doc_ref = obj.value("dRef").toString();

  if (obj.keys().contains("vote"))
    m_vote = obj.value("vote").toInt();

  if (obj.value("voter").toString() != "")
    m_voter = obj.value("voter").toString();

  if (obj.value("dComment").toString() != "")
    m_doc_comment = obj.value("dComment").toString();

  return true;
}

QJsonObject BallotDocument::exportDocToJson(const bool ext_info_in_document) const
{
  QJsonObject document = Document::exportDocToJson(ext_info_in_document);

  document["vote"] = m_vote;
  document["voter"] = m_voter;

  return document;
}


QString BallotDocument::safeStringifyDoc(const bool ext_info_in_document) const
{
  QJsonObject document_json = exportDocToJson(ext_info_in_document);

  if (ext_info_in_document)
    document_json["dExtInfo"] = SignatureStructureHandler::compactUnlockersArray(document_json["dExtInfo"].toArray());

//  // recaluculate block final length
//  document_json["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(document_json).length());

  CLog::log("2 safe Sringify Doc(" + CUtils::hash8c(m_doc_hash) + "): " + m_doc_type + " / " + m_doc_class + " length:" + QString::number(CUtils::serializeJson(document_json).length()) + " serialized document: " + CUtils::serializeJson(document_json), "app", "trace");
  return CUtils::serializeJson(document_json);
}


// old name was calcPollingCost
std::tuple<bool, CMPAIValueT> BallotDocument::calcDocDataAndProcessCost(
  const QString& stage,
  QString cDate,
  const uint32_t& extraLength) const
{
  Q_UNUSED(extraLength);
  if (cDate == "")
    cDate = CUtils::getNow();

  DocLenT dLen = m_doc_length;

  CMPAIValueT the_cost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);

  if (stage == CConsts::STAGES::Creating)
  {
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen);
    CLog::log("calc custom post the_cost + machine interest(" + CUtils::microPAIToPAI6(the_cost) +" PAIs) type/class(" + m_doc_type + "/" + m_doc_class + ") Doc(" + CUtils::hash8c(m_doc_hash) + ")", "app", "trace");
  }
  return {true, CUtils::CFloor(the_cost)};
}

// js name was recordABallot
bool BallotDocument::applyDocFirstImpact(const Block& block) const
{
  auto[shares, percentages_] = DNAHandler::getAnAddressShares(m_voter, block.m_block_creation_date);// it chenged from ballot.creatioindate to block.creationdate! be sure it works either in real time or synching
  Q_UNUSED(percentages_);

  QVDRecordsT pollings_info = PollingHandler::searchInPollings({{"pll_hash", m_doc_ref}});
  if (pollings_info.size() == 0)
  {
    // there are 2 possibilities, or the polling is not valid
    // or polling document still does not exist in node

    CLog::log("There is no polling for the Ballot(" + m_doc_hash + ")", "sec", "error");
    return true;
  }
  CDateT date_of_start_voting = pollings_info[0].value("pll_start_date").toString();

  // * @param {*} voteCreationTimeByMinute: timeDiff by minutes(statingVoteDateRecordedInBloc.creation Date - date_of_start_voting)
  // * @param {*} voteReceiveingTimeByMinute: timeDiff by minutes(receivingDate - date_of_start_voting)

  CLog::log("Record A Ballot(" + CUtils::hash8c(getDocHash()) + ") Creation Date Diff(" + QString::number(CUtils::timeDiff(date_of_start_voting, block.m_block_creation_date).asMinutes) + " Minutes)");
  CDateT nowT = CUtils::getNow();
  CDateT ballot_receive_date = nowT;
  uint64_t ballot_receive_date_diff = CUtils::timeDiff(date_of_start_voting, nowT).asMinutes;
  if (CMachine::isInSyncProcess())
  {
    QString ballots_receive_time_dict_ = KVHandler::getValue("ballotsReceiveDates");
    if (ballots_receive_time_dict_ != "")
    {
      QJsonObject ballots_receive_time_dict = CUtils::parseToJsonObj(ballots_receive_time_dict_);
      if (ballots_receive_time_dict.keys().contains(getDocHash()))
      {
        ballot_receive_date = ballots_receive_time_dict[getDocHash()].toObject().value("baReceiveDate").toString();
        ballot_receive_date_diff = ballots_receive_time_dict[getDocHash()].toObject().value("baVoteRDiff").toDouble();
      }
    }
  }

  return BallotHandler::recordBallotInDB(
    getDocHash(),
    getRef(),
    block.m_block_creation_date,    // ballot creation date, extracted from container block. creation Date
    ballot_receive_date,
    m_voter,
    shares,
    m_vote,
    m_doc_comment,
    CUtils::timeDiff(date_of_start_voting, block.m_block_creation_date).asMinutes,
    ballot_receive_date_diff);
}

QString BallotDocument::getRef() const
{
  return m_doc_ref;
}

//QString BallotDocument::getRefType() const
//{
//  return m_doc_ref_type;
//}

QString BallotDocument::calcDocExtInfoHash() const //calcTrxExtRootHash()
{
  // js name calcBallotExtInfoHash
  QString hash, hashables = "";

  if (m_doc_version == "0.0.0")
  {
    hashables += "{\"signatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("signatures").toVariant().toJsonArray()) + ",";
    hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("uSet").toObject()) + "}";
    hash = CCrypto::keccak256(hashables);
    CLog::log("Ext Hash ver == 0.0.0 Hashables Ballot(" + m_doc_hash + ") Regenrated Ext hash: " + hash + "\nhashables: " + hashables, "app", "trace");
    return m_doc_ext_hash;  // because of legacy maleformed implementation the hash calculation doesn't work properly

  } else {
    hashables += "{\"signatures\":" + CUtils::serializeJson(m_doc_ext_info[0].toObject().value("signatures").toVariant().toJsonArray()) + ",";
    hashables += "\"signedHash\":\"" + getDocToBeSignedHash() + "\",";
    hashables += "\"uSet\":" + SignatureStructureHandler::safeStringifyUnlockSet(m_doc_ext_info[0].toObject().value("uSet").toObject()) + "}";
//      obj = {
//          signatures: blt.dExtInfo.signatures,
//          signedHash,
//          uSet: blt.dExtInfo.uSet
//      }
    hash = CCrypto::keccak256(hashables);
    CLog::log("Ext Hash ver > 0.0.0 Hashables Ballot(" + m_doc_hash + ") Regenrated Ext hash: " + hash + "\nhashables: " + hashables, "app", "trace");
    return hash;
  }
}

//static extractSignablePartsOfBallot(blt)
//{
//  let signable = {
//    creation Date: blt.creation Date,
//    dClass: blt.dClass,
//    dType: blt.dType,
//    dVer: blt.dVer,
//    ref: blt.ref,
//    vote: blt.vote,
//    voter: blt.voter
//  }
//  return signable;
//}

//static getSignMsgDBallot(blt)
//{
//  let signables = GeneralBallotHandler.extractSignablePartsOfBallot(blt);
//  clog.app.info(`Ballot signables: ${utils.stringify(signables)}`);
//  let signedHash = iutils.doHashObject(signables);
//  let signMsg = signedHash.substring(0, iConsts.SIGN_MSG_LENGTH);
//  return { signedHash, signMsg };
//}

bool BallotDocument::hasSignable() const
{
  return true;
}

// old name was getSignMsgDBallot & extractSignablePartsOfBallot
QString BallotDocument::getDocToBeSignedHash() const
{
  QString signables = "{";
  signables += "\"dCDate\":\"" + m_doc_creation_date + "\"," ;
  signables += "\"dClass\":\"" + m_doc_class + "\"," ;

  if (m_doc_comment != "")
    signables += "\"dComment\":\"" + m_doc_comment + "\"," ;

  signables += "\"dType\":\"" + m_doc_type + "\"," ;
  signables += "\"dRef\":\"" + m_doc_ref + "\"," ;
  signables += "\"dVer\":\"" + m_doc_version + "\"," ;
  signables += "\"vote\":" + QString::number(m_vote) + "," ;
  signables += "\"voter\":\"" + m_voter + "\"}" ;

  QString to_be_signed_hash = CCrypto::keccak256(signables);
  CLog::log("Ballot to_be_signed_hash(" + to_be_signed_hash + ") signables: " + signables + " ", "app", "trace");
  return to_be_signed_hash;
}

QString BallotDocument::getDocSignMsg() const
{
  QString to_be_signed_hash = getDocToBeSignedHash();
  return to_be_signed_hash.midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
}

bool BallotDocument::veridfyDocSignature() const
{
  QJsonObject dExtInfo = m_doc_ext_info[0].toObject();
  auto unlockSet = dExtInfo.value("uSet").toObject();

  bool is_valid_unlock = SignatureStructureHandler::validateSigStruct(
    unlockSet,
    m_voter);

  if (!is_valid_unlock)
  {
    CLog::log("Invalid creator signature structure on Ballot(" + m_doc_hash + ")! ", "sec", "error");
    return false;
  }

  // ballot signature & permission validate check
  QString sign_message = getDocSignMsg();
  QJsonArray signatures = m_doc_ext_info[0].toObject().value("signatures").toArray();
  for (CSigIndexT signature_index = 0; signature_index < signatures.size(); signature_index++)
  {
    QString a_signature = signatures[signature_index].toString();
    try {
      bool verifyRes = CCrypto::ECDSAVerifysignature(
        unlockSet.value("sSets").toArray()[signature_index].toObject().value("sKey").toString(),
        sign_message,
        a_signature);
      if (!verifyRes)
      {
        CLog::log("Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
        return false;
      }
    } catch (std::exception) {
      CLog::log("Exception! Invalid creator signature sign on ballot(" + m_doc_hash + ")! ", "sec", "error");
      return false;
    }
  }
  return true;
}


QString BallotDocument::getDocHashableString() const
{
  QString hahsables = "{";
  hahsables += "\"dExtHash\":\"" + m_doc_ext_hash + "\",";
  hahsables += "\"dLen\":\"" + CUtils::paddingLengthValue(m_doc_length) + "\"}";
  return hahsables;
}

// old name was calcHashDBallot
QString BallotDocument::calcDocHash() const
{
//  CLog::log("calc calcHashDBallot: " + safeStringifyDoc(), "app", "trace");

  QString hashables = getDocHashableString();
  QString hash = CCrypto::keccak256(hashables);
  CLog::log("\nHashable string for ballot remote hash(" + m_doc_type + " / " +
    m_doc_class + ") local hash(" + hash + ") hashables: " + hashables, "app", "trace");
  return hash;
}

// old name was importBallotsCost
void BallotDocument::importCostsToTreasury(
  const Block* block,
  UTXOImportDataContainer* block_inspect_container)
{
  QHash<CDocHashT, CostPaymentStatus> cost_payment_status {};

  if (block_inspect_container->m_block_alter_treasury_incomes.keys().contains("TP_BALLOT"))
  {
    for (BlockAlterTreasuryIncome a_treasury_entry: block_inspect_container->m_block_alter_treasury_incomes["TP_BALLOT"])
    {
      // if ballot costs was payed by a valid trx
      bool doc_cost_is_payed = true;

      if (block_inspect_container->m_rejected_transactions.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"supporter transaction(" + CUtils::hash8c(a_treasury_entry.m_trx_hash) + ") for Ballot is rejected because of doublespending", false};
      }

      if (!block_inspect_container->m_map_U_trx_hash_to_trx_ref.keys().contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[a_treasury_entry.m_trx_hash] = {"The Ballot costs is not supported by any trx", false};
      }

      CDocHashT ballot_hash = block_inspect_container->m_map_U_trx_hash_to_trx_ref[a_treasury_entry.m_trx_hash];
      auto[inx_, ballot_doc] = block->getDocumentByHash(ballot_hash);
      Q_UNUSED(inx_);

      if (ballot_doc->m_doc_class != PollingHandler::POLLING_PROFILE_CLASSES["Basic"]["ppName"].toString())
      {
        doc_cost_is_payed = false;
        cost_payment_status[ballot_hash] = {"Ballot dClass(" + ballot_doc->m_doc_class + ") is not supported", false};
      }

      CDocHashT supporter_trx = block_inspect_container->m_map_U_trx_ref_to_trx_hash[ballot_hash];
      QStringList rejected_transactions = block_inspect_container->m_rejected_transactions.keys();
      if (rejected_transactions.contains(supporter_trx) || rejected_transactions.contains(a_treasury_entry.m_trx_hash))
      {
        doc_cost_is_payed = false;
        cost_payment_status[ballot_hash] = {"supporter transaction is rejected because of doublespending", false};
      }

      if (doc_cost_is_payed)
      {
        CLog::log("Successfully TP_BALLOT Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_BALLOT)", "app", "trace");

        cost_payment_status[ballot_hash].m_message = "Ballot Cost imported to treasury succsessfully.";
        QString title = "TP_BALLOT Ballot(" + CUtils::hash8c(ballot_hash) + ")";
        TreasuryHandler::insertIncome(
          title,
          "TP_BALLOT",
          title,
          block->m_block_creation_date,
          a_treasury_entry.m_value,
          block->getBlockHash(),
          a_treasury_entry.m_coin);

      } else {
        CLog::log("Failed TP_... Block(" + CUtils::hash8c(block->getBlockHash()) + ") Coin(" + CUtils::shortCoinRef(a_treasury_entry.m_coin) + ") importing(TP_BALLOT)", "sec", "error");
        CLog::log("cost_payment_status not payed: " + UTXOImportDataContainer::dumpMe(cost_payment_status[ballot_hash]), "sec", "error");

        BallotHandler::removeBallot(ballot_hash);

      }
    }
  }
  block_inspect_container->m_cost_payment_status["TP_BALLOT"] = cost_payment_status;

}

