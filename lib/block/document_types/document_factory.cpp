#include "stable.h"
#include "document.h"
#include "basic_tx_document.h"
#include "dp_cost_pay_document.h"
#include "coinbase_document.h"
#include "rp_docdocument.h"
#include "rl_docdocument.h"
#include "polling_document.h"
#include "administrative_polling_document.h"
#include "ballot_document.h"
#include "lib/block/document_types/iname_documents/iname_reg_document.h"
#include "lib/block/document_types/iname_documents/iname_bind_document.h"
#include "free_documents/free_document.h"
#include "dna_proposal_document.h"
#include "pledge_documents/pledge_document.h"
#include "pledge_documents/close_pledge_document.h"

#include "document_factory.h"

DocumentFactory::DocumentFactory()
{

}

Document* DocumentFactory::create(
  const QJsonObject &obj,
  const Block* block,
  const int64_t doc_index)
{
  Document* doc;
  QString doc_type = obj.value("dType").toString();
  if (doc_type == CConsts::DOCUMENT_TYPES::BasicTx)
  {
    doc = new BasicTxDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::DPCostPay)
  {
    doc = new DPCostPayDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::Coinbase)
  {
    doc = new CoinbaseDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::RpDoc)
  {
    doc = new RepaymentDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::FPost)
  {
    doc = new FreeDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::Ballot)
  {
    doc = new BallotDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::Polling)
  {
    doc = new PollingDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::AdmPolling)
  {
    doc = new AdministrativePollingDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::DNAProposal)
  {
    doc = new DNAProposalDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::Pledge)
  {
    doc = new PledgeDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::ClosePledge)
  {
    doc = new ClosePledgeDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::INameReg)
  {
    doc = new INameRegDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::INameBind)
  {
    doc = new INameBindDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::RlDoc)
  {
    doc = new RlDocDocument(obj);

  }
  else
  {
    doc = new Document(obj);

  }


  if ((doc->m_doc_ext_info.size() == 0) && (doc_index != -1) && (block != nullptr))
    doc->maybeAssignDocExtInfo(block, doc_index);

  return doc;
}
