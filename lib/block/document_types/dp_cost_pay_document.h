#ifndef DPCOSTPAYDOCUMENT_H
#define DPCOSTPAYDOCUMENT_H


class DPCostPayDocument : public Document
{
public:
  DPCostPayDocument(const QJsonObject& obj);
  ~DPCostPayDocument();
  bool setByJsonObj(const QJsonObject& obj) override;
  bool setDocumentOutputs(const QJsonValue& obj) override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;
  QString calcDocExtInfoHash() const override;

  QString getRef() const override;
  QVector<COutputIndexT> getDPIs() const override;

  bool deleteInputs() override;
  bool deleteOutputs() override;

  std::vector<TInput*> getInputs() const override;
  std::vector<TOutput*> getOutputs() const override;

  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;

  bool applyDocFirstImpact(const Block& block) const override;

  std::vector<TOutput*> m_outputs = {};

};

#endif // DPCOSTPAYDOCUMENT_H
