#ifndef COINBASEDOCUMENT_H
#define COINBASEDOCUMENT_H


class CoinbaseDocument : public Document
{
public:
  CoinbaseDocument(const QJsonObject& obj);
  ~CoinbaseDocument();
  bool setByJsonObj(const QJsonObject& obj) override;
  bool setDocumentOutputs(const QJsonValue& obj) override;

  QString getDocHashableString() const override;
  QString calcDocHash() const override;

  std::vector<TInput*> getInputs() const override;
  std::vector<TOutput*> getOutputs() const override;

  bool deleteInputs() override;
  bool deleteOutputs() override;

  std::tuple<bool, QJsonArray> exportInputsToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;

  QJsonObject exportDocToJson(const bool ext_info_in_document = true) const override;

  bool applyDocFirstImpact(const Block& block) const override;

  QString m_doc_cycle = "";
  QString m_treasury_from = "";
  QString m_treasury_to = "";
  uint64_t m_minted_coins = 0;
  uint64_t m_treasury_incomes = 0;

  std::vector<TOutput*> m_outputs = {};

};

#endif // COINBASEDOCUMENT_H
