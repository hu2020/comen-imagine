#ifndef DOCUMENT_H
#define DOCUMENT_H

class Block;

class Document
{
public:
  Document(){};
  Document(const QJsonObject& obj){ setByJsonObj(obj); };
  virtual ~Document();

  static const QString stbl_docs_blocks_map;
  static const QStringList stbl_docs_blocks_map_fields;

//  Block m_containterBlock;
  //  -  -  -  common members
  QString m_doc_hash = "0000000000000000000000000000000000000000000000000000000000000000";
  QString m_doc_type = "";
  QString m_doc_class = "";
  QString m_doc_version = CConsts::DEFAULT_DOCUMENT_VERSION;
  QString m_doc_title = "";
  QString m_doc_ref = ""; // reference of document that the transaction pay for it, or vote for, ...
  QString m_doc_title_hash = "";

  QString m_doc_comment = "";
  /**
   * @brief m_doc_descriptions
   * this property exist to backward wompatibility
   * The descriptions dedicated to block whereas description is deidcated to documents,
   * but in basic transactions the field named wrogly descriptions
   * QString m_doc_descriptions = "";
   */

  QString m_doc_tags = "";
  QString m_doc_creation_date = "";
  QString m_block_creation_date = "";

  QString m_doc_ext_hash = "";
  QJsonArray m_doc_ext_info {};

  DocLenT m_doc_length = 0;


  //  -  -  -  static methods
  bool isBasicTransaction();
  static bool isBasicTransaction(const QString& dType);
  bool isDPCostPayment();
  static bool isDPCostPayment(const QString& dType);
  static bool canBeACostPayerDoc(const QString& dType);
  static bool isNoNeedCostPayerDoc(const QString& dType);

  //  -  -  -  virtual methods
  /**
   * @brief safeStringifyDoc
   * creates an ordered JSon object and serialize it, for the sake of uniqness of hash of the document
   * @return
   */
  virtual QString safeStringifyDoc(const bool ext_info_in_document = true) const;

  virtual CDocHashT getDocRef() const;
  virtual QString getDocHashableString() const;
  virtual CDocHashT calcDocHash() const;
  virtual DocLenT calcDocLength() const;

  virtual void setDocHash();
  virtual void setDocLength();
  virtual void setDExtHash();
  virtual bool applyDocFirstImpact(const Block& block) const;
//  virtual QJsonObject exportJson() const;
  virtual GenRes applyDocImpact2();
  virtual bool setByJsonObj(const QJsonObject& obj);

  virtual QJsonArray getDocExtInfo() const;
  virtual void maybeAssignDocExtInfo(
    const Block* block,
    const CDocIndexT& doc_inx);
  virtual QString calcDocExtInfoHash() const;

  virtual std::tuple<bool, CMPAIValueT> calcDocDataAndProcessCost(
    const QString& stage,
    QString cDate = "",
    const uint32_t& extraLength = 0) const;

  virtual QString getRef() const;
  virtual QString getRefType() const;
  virtual QString getProposalRef() const;
  virtual QString getPayerTrxLinkBack() const;
  virtual CMPAISValueT getDocCosts() const;

  virtual bool setDocumentInputs(const QJsonValue& obj);
  virtual bool setDocumentOutputs(const QJsonValue& obj);

  virtual bool deleteInputs();
  virtual bool deleteOutputs();

  QString stringifyInputs() const;
  QString stringifyOutputs() const;

  virtual bool hasSignable() const;
  virtual QString getDocToBeSignedHash() const;
  virtual QString getDocSignMsg() const;
  virtual bool veridfyDocSignature() const;

  virtual QJsonObject exportDocToJson(const bool ext_info_in_document = true) const;

  /**
   * @brief exportInputsToJson
   * @return {has_inputs, inputs_Json_Array}
   */
  virtual std::tuple<bool, QJsonArray> exportInputsToJson() const;

  /**
   * @brief exportOutputsToJson
   * @return {has_outputs, outputs_json_array}
   */
  virtual std::tuple<bool, QJsonArray> exportOutputsToJson() const;

  virtual QVector<COutputIndexT> getDPIs() const;

  virtual std::vector<TInput*> getInputs() const;
  virtual std::vector<TOutput*> getOutputs() const;

  virtual GenRes customValidateDoc(const Block* block) const;
  virtual GenRes fullValidate(const Block* block) const;

  static void importCostsToTreasury(
    const Block* block,
    UTXOImportDataContainer* block_inspect_container);


  //  -  -  -  base class methods
  QString getDocHash() const;

  static bool trxHasInput(const QString& document_type);
  bool trxHasInput();

  static bool trxHasNotInput(const QString& document_type);
  bool trxHasNotInput();

  void mapDocToBlock(
    const CBlockHashT& block_hash,
    const CDocIndexT doc_inx);

  static void mapDocToBlock(
    const CDocHashT& doc_ash,
    const CBlockHashT& block_hash,
    const CDocIndexT doc_inx);

  static bool docHasOutput(const QString& document_type);
  bool docHasOutput();

  static QVDRecordsT searchInDocBlockMap(
    const ClausesT& clauses = {},
    const QStringList fields = stbl_docs_blocks_map_fields,
    const OrderT order = {},
    const uint64_t limit = 0);
};

#endif // DOCUMENT_H
