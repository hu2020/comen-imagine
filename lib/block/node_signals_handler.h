#ifndef NODESIGNALSHANDLER_H
#define NODESIGNALSHANDLER_H

class Block;

#include <QJsonObject>

#include "lib/block/block_types/block.h"
#include "lib/database/db_model.h"

class NodeSignalsHandler
{
public:
  static const QString stbl_signals;
  static const QStringList stbl_signals_fields;

  NodeSignalsHandler();
  static void logSignals(const Block &block);

  static QJsonObject getMachineSignals();

  static QVDRecordsT searchInSignals(
    const ClausesT& clauses,
    const QStringList& fields = stbl_signals_fields,
    const OrderT& order = {},
    const int& limit = 0);
};

#endif // NODESIGNALSHANDLER_H
