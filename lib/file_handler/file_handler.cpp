#include <QCoreApplication>
#include "stable.h"
#include "lib/ccrypto.h"
#include "file_handler.h"

FileHandler::FileHandler() {

}


std::tuple<bool, QString> FileHandler::read(
  QString file_path,
  QString file_name,
  const int clone_id)
{
  if (clone_id>0)
    file_path += QString::number(clone_id);

  if (file_path != "")
    file_path += "/";

  file_path += file_name;

  return read_(file_path);
}

std::tuple<bool, QString> FileHandler::read_(QString file_path)
{
  QFile f(file_path);
  f.open(QIODevice::ReadOnly);
  QByteArray ba = f.readAll();
  f.close();
  return {true, QString(ba)};
}

bool FileHandler::write(
  const QString& directory,
  const QString& file_name,
  const QString& content,
  const int clone_id)
{
  QString file_path = directory;
//  if (clone_id>0)
//    file_path += QString::number(clone_id);

  if (file_path != "")
    file_path += "/";

  file_path += file_name;

  return FileHandler::write_(file_path, content);
}

bool FileHandler::write_(const QString& file_path, const QString& content)
{
  CLog::log(file_path, "app", "trace");
  QFile f(file_path);
  if (!f.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    CLog::log( "Unable to write file" + file_path, "app", "fatal");
    return false;
  }

  QTextStream out(&f);
  out << content;

  f.close();
  return true;
}


bool FileHandler::deleteFile(const QString& file_path)
{
  QFile file (file_path);
  file.remove();
  return true;
}






//  -  -  -  email part
std::tuple<bool, QString, QString> FileHandler::writeEmailAsFile(
  const QString& title,
  const QString& message,
  const QString& sender,
  const QString& receiver,
  const bool& is_custom)
{
  try {
    CLog::log("write Em File args: title(" + title + ") sender(" + sender + ") receiver(" + receiver + ") is_custom(" + (is_custom?"Y":"N") + ") message(" + message + ")", "app", "trace");
    QString to_send_message = message;
    if (is_custom)
    {
      to_send_message= CConsts::MESSAGE_TAGS::customStartEnvelope + to_send_message + CConsts::MESSAGE_TAGS::customEndEnvelope;
    }

    QString outbox = CMachine::getOutboxPath();
    auto app_clone_id = CMachine::getAppCloneId();
//    if (app_clone_id > 0)
//        outbox = outbox + app_clone_id;

    QString email_body = CUtils::getNow() + CConsts::NL;
    email_body += "time: " + CUtils::getNowSSS() + CConsts::NL;
    email_body += CConsts::MESSAGE_TAGS::senderStartTag + sender + CConsts::MESSAGE_TAGS::senderEndTag + CConsts::NL;
    email_body += CConsts::MESSAGE_TAGS::receiverStartTag + receiver + CConsts::MESSAGE_TAGS::receiverEndTag + CConsts::NL;
    email_body += to_send_message + CConsts::NL;
    QString hash = CUtils::hash16c(CCrypto::keccak256(sender + receiver + to_send_message));
    email_body += CConsts::MESSAGE_TAGS::hashStartTag + hash + CConsts::MESSAGE_TAGS::hashEndTag + CConsts::NL;
    CLog::log("email_body: " + email_body, "app", "trace");
    QString file_name = "";
    if (CMachine::isDevelopMod())
    {
      file_name = QStringList {CUtils::getNowSSS(), sender, receiver, title + ".txt"}.join(",");
    } else {
      file_name = QStringList {receiver, CUtils::getNowSSS(), CUtils::lightRandom(10000), ".txt"}.join(" ");
    }
    CLog::log("file Name: " + file_name, "app", "trace");
    CLog::log("Try to write1: " + outbox + "/" + file_name, "app", "trace");
    bool res = write(outbox + "/", file_name, email_body, app_clone_id);
    return { res, email_body, hash };

  } catch (std::exception) {
    CLog::log("write Email As File failed!", "app", "fatal");
    return { false, "", ""};
  }

}

bool FileHandler::fileCopy(QString srcFileName, QString dstFileName)
{
  QFile::copy(srcFileName, dstFileName);
  return true;
}

