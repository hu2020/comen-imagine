#ifndef FILEREADRES_H
#define FILEREADRES_H

#include <QString>

struct FileReadRes {
    std::error_code stat;
    bool thereIsPacketToParse = false;
    bool secIssue = false;
    QString msg;
    QString fileName;
    QString content;
};


#endif // FILEREADRES_H
