#ifndef FILEHANDLER_H
#define FILEHANDLER_H


class FileReadRes;

#include "file_read_res.h"

class FileHandler
{

public:
  FileHandler();

  static std::tuple<bool, QString> read(
    QString file_path,
    QString file_name,
    const int clone_id = 0);

  static std::tuple<bool, QString> read_(QString file_path);

  static bool write(
    const QString& directory,
    const QString& fileName,
    const QString& content,
    const int clone_id = 0);

  static bool write_(const QString& file_path, const QString& content);

  static bool deleteFile(const QString& file_path);
  static bool fileCopy(QString srcFileName, QString dstFileName);

  //  -  -  -  email part
  static std::tuple<bool, QString, QString> writeEmailAsFile(
    const QString& title,
    const QString& message,
    const QString& sender,
    const QString& receiver,
    const bool& is_custom = false);
};

#endif // FILEHANDLER_H
