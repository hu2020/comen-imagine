#include "stable.h"
#include "k_v_handler.h"

QString KVHandler::stbl_kvalue = "c_kvalue";

KVHandler::KVHandler()
{

}








QString KVHandler::getValue(const QString &kvKey)
{
  QueryRes res = DbModel::select(
    KVHandler::stbl_kvalue,
    {"kv_value"},
    {{"kv_key", kvKey}}
  );
  if (res.records.size() == 0)
      return "";
  return res.records[0].value("kv_value").toString();
}

bool KVHandler::deleteKey(const QString &kvKey)
{
  QueryRes res = DbModel::dDelete(
    stbl_kvalue,
    {{"kv_key", kvKey}}
  );
  return res.status == true;
}

QVDRecordsT KVHandler::serach(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int& limit)
{
  try {
    QueryRes res = DbModel::select(
      stbl_kvalue,
      fields,
      clauses,
      order,
      limit,
      false,
      true);
    return res.records;

  } catch (std::exception) {
    CLog::log("faile in search in kvhandler", "app", "fatal");
    return {};
  }
}

//  static prepareIt(args) {
//    args.table = 'i_kvalue';
//    let { _fields, _clauses, _values, _order, _limit } = db.preQueryGenerator(args)
//    let _query = 'SELECT ' + _fields + ' FROM i_kvalue ' + _clauses + _order + _limit;
//    return { _query, _values }
//  }


bool KVHandler::updateKValue(const QString &key, const QString &value)
{
  return DbModel::update(
    stbl_kvalue,
    {{"kv_value", value}, {"kv_last_modified", CUtils::getNow()}},
    {{"kv_key", key}});
}


bool KVHandler::upsertKValue(
    const QString &key,
    const QString &value,
    const bool &log)
{
  return DbModel::upsert(
    stbl_kvalue,
    "kv_key", key,
    {{"kv_value", value}, {"kv_last_modified", CUtils::getNow()}});
}

bool KVHandler::setValue(
    const QString &key,
    const QString &value,
    const bool &log)
{
  return upsertKValue(key, value, log);
}

