#ifndef FILEBUFFERHANDLER_H
#define FILEBUFFERHANDLER_H

/**
 * @brief The FileBufferHandler class
 * this class reads from inbox folder and parse it as well as writes to outbox folder
 */
class FileBufferHandler
{
public:
  FileBufferHandler();

  static bool maybeBootDAGFromBundle();
  static std::tuple<bool, QString> readDAGBundleIfExist();
  static std::tuple<bool, QString, QString, QString, QString> readEmailFile();
  static void loopReadAndParseHardDiskInbox();
  static bool doReadAndParseHardDiskInbox();
  static bool readAndSendHardDiskOutbox();
  static QStringList listHardDiskInbox();
  static QStringList listHardDiskOutbox();
  static bool maybePurgeMessage(const QString& full_path, const bool& should_purge_fessage);
};

#endif // FILEBUFFERHANDLER_H
