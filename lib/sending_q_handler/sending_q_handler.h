#ifndef SENDINGQHANDLER_H
#define SENDINGQHANDLER_H



#include "lib/machine/machine_handler.h"
#include "lib/network/broadcast_logger.h"
#include "lib/pgp/cpgp.h"



class SendingQHandler
{
public:
  static const QString stbl_sending_q;
  static const QStringList stbl_sending_q_fields;
  static const QString stbldev_sending_q;

  SendingQHandler();

  static CListListT preparePacketsForNeighbors(
    const QString& sqType,
    const QString& sqCode,
    const QString& sqPayload,
    const QString& sqTitle  = "newBlock",
    const QStringList& sqReceivers = {},
    const QStringList& noReceivers = {},
    const bool& denayDoubleSendCheck = false);

  static bool pushIntoSendingQ(
    const QString& sqType,
    const QString& sqCode,
    const QString& sqPayload,
    const QString& sqTitle,
    const QStringList& sqReceivers = {},
    const QStringList& noReceivers = {},
    const bool& denayDoubleSendCheck = false);

  static QVDRecordsT fetchFromSendingQ(
    QStringList fields = stbl_sending_q_fields,
    ClausesT clauses = {},
    OrderT order = {});

  static bool sendOutThePacket();

  static bool rmoveFromSendingQ(const ClausesT& clauses);

  static void loopPullSendingQ();

  static void cancelIvokeBlockRequest(const CBlockHashT& block_hash);

  static void maybeCancelIvokeBlocksRequest();


};

#endif // SENDINGQHANDLER_H
