#ifndef THREADSHANDLER_H
#define THREADSHANDLER_H


#include "lib/parsing_q_handler/parsing_q_handler.h"
#include "lib/dag/normal_block/normal_utxo_handler.h"

class ThreadsHandler
{
public:
  ThreadsHandler();
  static void launchThreads();
  static void launchSmartPullFromParsingQ();
};

#endif // THREADSHANDLER_H
