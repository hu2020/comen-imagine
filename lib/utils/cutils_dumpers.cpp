class Documents;
#include "lib/block/document_types/document.h"
//#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"

#include "cutils.h"

QString CUtils::dumpIt(const bool& status)
{
  if (status)
    return "true";
  return "false";
}

QString CUtils::dumpIt(const std::vector<CMPAIValueT>& values, const QString& prefix_tabs)
{
  QStringList out;
  for (uint64_t i=0; i < values.size(); i++)
  {
    out.append(prefix_tabs + prefix_tabs + QString::number(i) + ": " + QString::number(values[i]));
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const std::vector<QStringList>& values, const QString& prefix_tabs)
{
  QStringList out;
  for (uint64_t i=0; i < values.size(); i++)
  {
    out.append(prefix_tabs + prefix_tabs + QString::number(i) + ": " + dumpIt(values[i]));
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const std::vector<double>& values, const QString& prefix_tabs)
{
  QStringList out;
  for (uint64_t i=0; i < values.size(); i++)
  {
    out.append(prefix_tabs + prefix_tabs + QString::number(i) + ": " + QString::number(values[i]));
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QStringList& values, const QString& prefix_tabs)
{
  QStringList out;
  for (int i=0; i<values.size(); i++)
  {
    out.append(prefix_tabs + prefix_tabs + QString::number(i) + ": " + values[i]);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QVector<QStringList>& rows, const QString& prefix_tabs)
{
  QStringList out;
  for (QStringList a_row: rows)
  {
    QString dumped_row = prefix_tabs + dumpIt(a_row, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + " - - - - - - - - - - - - " + CConsts::NL + prefix_tabs + dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}


QString CUtils::dumpIt(const QVDicT& values, const QString& prefix_tabs)
{
  QStringList out;
  QStringList keys_ = values.keys();
  keys_.sort();
  for (QString a_key: keys_)
  {
    out.append(prefix_tabs + a_key + ": " + QString("%1").arg(values.value(a_key).toString()));
  }
  return CConsts::NL + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const UI64DicT& values, const QString& prefix_tabs)
{
  QStringList out;
  QStringList keys_ = values.keys();
  keys_.sort();
  for (QString a_key: keys_)
  {
    out.append(prefix_tabs + a_key + ": " + QString::number(values.value(a_key)));
  }
  return CConsts::NL + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QList<QVDicT>& rows, const QString& prefix_tabs)
{
  QStringList out;
  for (QVDicT a_row: rows)
  {
    QString dumped_row = dumpIt(a_row, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + prefix_tabs + " - - - - - - - - - - - - " + dumped_row);
  }
  return CConsts::NL + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QVector<QVector<QVDicT> >& rows, const QString& prefix_tabs)
{
  QStringList out;
  for (QVector<QVDicT> a_row: rows)
  {
    QString dumped_row = dumpIt(a_row, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + " - - - - - - - - - - - - " + CConsts::NL + prefix_tabs + dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QVDRecordsT& rows, const QString& prefix_tabs)
{
  QStringList out;
  for (QVDicT a_row: rows)
  {
    QString dumped_row = dumpIt(a_row, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + " - - - - - - - - - - - - " + CConsts::NL + prefix_tabs + dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const JORecordsT& rows, const QString& prefix_tabs)
{
  QStringList out;
  for (QJsonObject a_row: rows)
  {
    QString dumped_row = dumpIt(a_row, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + " - - - - - - - - - - - - " + CConsts::NL + prefix_tabs + dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QJsonArray& rows, const QString& prefix_tabs)
{
  QStringList out;
  for (auto a_row: rows)
  {
    QString dumped_row = dumpIt(a_row, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + " - - - - - - - - - - - - " + CConsts::NL + prefix_tabs + dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QHash<QString, QHash<QString, QVDRecordsT>>& groupped_rows, const QString& prefix_tabs)
{
  QStringList out;
  QStringList keys_ = groupped_rows.keys();
  keys_.sort();
  for (QString a_groupp_key: keys_)
  {
    QHash<QString, QVDRecordsT> a_grp = groupped_rows.value(a_groupp_key);
    QString dumped_row = dumpIt(a_grp, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + " - - - - - - Group(" + a_groupp_key + ") - - - - - - " + CConsts::NL + prefix_tabs + dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QHash<QString, DNAShareCountT>& values, const QString& prefix_tabs)
{
  QString out;
  QStringList keys_ = values.keys();
  keys_.sort();
  for (QString a_key: keys_)
  {
    out += CConsts::NL + prefix_tabs + a_key + ": " + QString::number(values[a_key]);
  }
  return out;
}

QString CUtils::dumpIt(const UnlockSet& custom_data, const QString& prefix_tabs)
{
  QString out_str = CConsts::NL + prefix_tabs + "Signature: " + custom_data.m_signature_type + "(" + custom_data.m_signature_ver + ")";
  out_str +=  CConsts::NL + prefix_tabs + "salt: " + custom_data.m_salt + "(lHash " + custom_data.m_left_hash + ")";
  out_str += CConsts::NL + prefix_tabs + "Proofs: " + dumpIt(custom_data.m_merkle_proof,  prefix_tabs + CConsts::DUMPER_INDENT);
  out_str += CConsts::NL + prefix_tabs + "Signature sets: " + dumpIt(custom_data.m_signature_sets, prefix_tabs + CConsts::DUMPER_INDENT);
  return out_str;
}

QString CUtils::dumpIt(const QVector<UnlockSet>& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  for (UnlockSet a_set: custom_data)
  {
    QString dumped_row = prefix_tabs + dumpIt(a_set, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QVector<IndividualSignature>& custom_data, const QString& prefix_tabs)
{
  QString out_str = CConsts::NL + prefix_tabs + "Individual signatures:" + CConsts::NL;
  try {

    QStringList out;
    for (IndividualSignature a_set: custom_data)
    {
      QString dumped_row = prefix_tabs + dumpIt(a_set, prefix_tabs + CConsts::DUMPER_INDENT);
      out.append(dumped_row);
    }
    QString tmp = out.join(CConsts::NL);
    out_str = prefix_tabs + " " + out_str + " " + tmp + " ";
    return out_str;

  } catch (std::exception) {
    return out_str;

  }
}

QString CUtils::dumpIt(const UnlockDocument& custom_data, const QString& prefix_tabs)
{
  QString out_str = CConsts::NL + prefix_tabs + "merkle_root: " + custom_data.m_merkle_root + "(" + custom_data.m_merkle_version + ")";
  out_str += CConsts::NL + prefix_tabs + "account_address: " + custom_data.m_account_address + CConsts::NL + "unlock_sets";
  return CConsts::NL + prefix_tabs + out_str + dumpIt(custom_data.m_unlock_sets, prefix_tabs + CConsts::DUMPER_INDENT);
}

QString CUtils::dumpIt(const IndividualSignature& custom_data, const QString& prefix_tabs)
{
  QString out_str = CConsts::NL + prefix_tabs + "signature_key: " + custom_data.m_signature_key;
  if (custom_data.m_permitted_to_pledge != "")
    out_str += CConsts::NL + prefix_tabs + "permitted_to_pledge: " + custom_data.m_permitted_to_pledge;
  if (custom_data.m_permitted_to_delegate != "")
  out_str += CConsts::NL + prefix_tabs + "permitted_to_delegate: " + custom_data.m_permitted_to_delegate;

  return out_str;
}

QString CUtils::dumpIt(const QV2DicT& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  QStringList keys_ = custom_data.keys();
  keys_.sort();
  for (QString a_key: keys_)
  {
    QVDicT a_res = custom_data.value(a_key);
    QString dumped_row = dumpIt(a_res, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + " - - - - - - Group(" + a_key + ") - - - - - - " + CConsts::NL + prefix_tabs + dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const CListListT& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  for (int i=0; i<custom_data.size(); i++)
  {
    QStringList an_elm = custom_data[i];
    QString dumped_row = dumpIt(an_elm, prefix_tabs + CConsts::DUMPER_INDENT);
    out.append(CConsts::NL + " - - - - - - Super List(" + QString::number(i) + ") - - - - - - " + CConsts::NL + prefix_tabs + dumped_row);
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QHash<QString, QString>& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  QStringList keys_ = custom_data.keys();
  keys_.sort();
  for (QString a_key: keys_)
  {
    out.append(CConsts::NL + prefix_tabs + a_key + ": " + custom_data.value(a_key, ""));
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QJsonValue& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  if (custom_data.isArray())
  {
    return dumpIt(custom_data.toArray(), prefix_tabs + CConsts::DUMPER_INDENT);

  }else if (custom_data.isString()){
    return CConsts::DUMPER_INDENT + prefix_tabs + custom_data.toString() + CConsts::DUMPER_INDENT;

  }else if (custom_data.isObject()){
    return dumpIt(custom_data.toObject(), prefix_tabs + CConsts::DUMPER_INDENT);

  }else if (custom_data.isBool()){
    return CConsts::DUMPER_INDENT + prefix_tabs + custom_data.toBool() + CConsts::DUMPER_INDENT;

  }else if (custom_data.isDouble()){
    return CConsts::DUMPER_INDENT + prefix_tabs + custom_data.toDouble() + CConsts::DUMPER_INDENT;

  }else{
    return CConsts::DUMPER_INDENT + prefix_tabs + "Unknown QJsonValue type!" + CConsts::DUMPER_INDENT;
  }
}

QString CUtils::dumpIt(const QJsonObject& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  QStringList keys_ = custom_data.keys();
  keys_.sort();
  for (QString a_key: keys_)
  {
    try {
      out.append(CConsts::NL + prefix_tabs + a_key + ": " + dumpIt(custom_data.value(a_key), prefix_tabs + CConsts::DUMPER_INDENT));

    } catch (std::exception) {

    }
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const GRecordsT& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  QStringList keys_ = custom_data.keys();
  keys_.sort();
  for (QString a_key: keys_)
  {
    try {
      out.append(CConsts::NL + prefix_tabs + a_key + ": " + dumpIt(custom_data.value(a_key), prefix_tabs + CConsts::DUMPER_INDENT));

    } catch (std::exception) {

    }
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QHash<QString, Document*>& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  QStringList keys_ = custom_data.keys();
  keys_.sort();
  for (QString a_key: keys_)
  {
    try {
      out.append(CConsts::NL + prefix_tabs + a_key + ": " + custom_data[a_key]->getDocHash());

    } catch (std::exception) {

    }
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const ClausesT& custom_data, const QString& prefix_tabs)
{
  QStringList out;
  QString a_value;
  for (ModelClause a_clause: custom_data)
  {
    try {
      a_value = QString("%1").arg(a_clause.m_field_single_value.toDouble()) + " " + a_clause.m_field_single_value.toString();
      out.append(CConsts::NL + prefix_tabs + "Field(" + a_clause.m_field_name + ") " + "Value(" + a_value + " " + a_clause.m_field_multi_values.join(", ") + ") " + "Operand(" + a_clause.m_clause_operand + ") ");

    } catch (std::exception) {

    }
  }
  return CConsts::NL + prefix_tabs + out.join(CConsts::NL);
}

QString CUtils::dumpIt(const QHash<QString, QJsonObject>& custom_data, const QString& prefix_tabs)
{
  QString out;
  QStringList keys = custom_data.keys();
  keys.sort();
  for (QString a_key: keys)
  {
    out += CConsts::NL + prefix_tabs + a_key + ":";
    out += CConsts::NL + prefix_tabs + dumpIt(custom_data[a_key], prefix_tabs);
  }
  return out;
}

QString CUtils::dumpDoubleSpends(const SpendCoinsList* spent, const QString& prefix_tabs)
{
  Q_UNUSED(spent);
  return prefix_tabs + "";
}
