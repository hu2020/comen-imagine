#include "stable.h"

#include "lib/block_utils.h"
#include "lib/file_handler/file_handler.h"

#include "excel_reporter.h"

// headers
const QStringList CSV_TRX_HEADRS {"", "Account", "Total Minted", "Minted", "Treasury", "Inputs", "Outputs", "Balance", ".",
                                  "Description", "Used Coins", "new Coins"};
const QString CELL_SEPERSATOR = "\t";
const QString END_LINE = "\n";

std::vector<std::vector<QString> > ExcelReporter::s_rows = {};
QStringList ExcelReporter::s_used_coins_codes = {};
CMPAISValueT ExcelReporter::s_doublespend_sum = 0;


ExcelReporter::ExcelReporter()
{

}

std::vector<QString> ExcelReporter::fillTheEmptyCells(std::vector<QString> the_row)
{
  int needed_sells = CSV_TRX_HEADRS.size()-the_row.size();
  for (int i = 0; i < needed_sells; i++)
    the_row.push_back("");
  return the_row;
}

void ExcelReporter::addHeader()
{
  std::vector<QString> the_row {};
  for (QString a_header: CSV_TRX_HEADRS)
    the_row.push_back(a_header);
  s_rows.push_back(the_row);
}

void ExcelReporter::addABlockHeader1(
  uint64_t block_index,
  QString block_type,
  CDateT block_creation_date)
{
  std::vector<QString> the_row {
    QString::number(block_index),
    block_type + " " + block_creation_date};
  the_row = fillTheEmptyCells(the_row);
  s_rows.push_back(the_row);
}

void ExcelReporter::addABlockHeader2(CBlockHashT block_hash)
{
  std::vector<QString> the_row {
    "",
    block_hash};
  the_row = fillTheEmptyCells(the_row);
  s_rows.push_back(the_row);
}

struct TempTupleCoin {
  CDocHashT m_hash = "";
  CMPAISValueT m_amount = 0;
};

void ExcelReporter::prepareARow(const QVDicT a_row)
{
  std::vector<QString> the_row {};
  the_row = fillTheEmptyCells(the_row);

  QStringList keys = a_row.keys();

  if (keys.contains("date"))
    the_row[1] = a_row.value("date").toString();


  if (keys.contains("account"))
  {
    if (CConsts::TREASURY_PAYMENTS.contains(a_row.value("account").toString()))
    {
      the_row[1] = a_row.value("account").toString(); // style TPIncome
    } else {
      the_row[1] = a_row.value("account").toString(); // style BechAdd
    }
  }

  if (keys.contains("total_minted"))
    the_row[2] = CUtils::sepNum(a_row.value("total_minted").toDouble());

  if (keys.contains("minted"))
    the_row[3] = CUtils::sepNum(a_row.value("minted").toDouble());

  if (keys.contains("treasury"))
    the_row[4] = CUtils::sepNum(a_row.value("treasury").toDouble());

  if (keys.contains("input"))
    the_row[5] = CUtils::sepNum(a_row.value("input").toDouble());

  if (keys.contains("output"))
    the_row[6] = CUtils::sepNum(a_row.value("output").toDouble());

  if (keys.contains("block_balance"))
    the_row[7] = CUtils::sepNum(a_row.value("block_balance").toDouble());

  if (keys.contains("dummy"))
    the_row[8] = a_row.value("dummy").toString();

  if (keys.contains("desc"))
    the_row[9] = a_row.value("desc").toString();


  if (keys.contains("coin_code"))
  {
    CCoinCodeT coin_code = a_row.value("coin_code").toString();
    // style = this.BechAdd;

    if (s_used_coins_codes.contains(coin_code) && (a_row.value("document_type").toString() != CConsts::DOC_TYPES::RpDoc))
    {
      // the input is already spend
      //style = this.BechAddRed;
      s_doublespend_sum += a_row.value("input").toDouble();
    }
    the_row[10] = coin_code; //style
    s_used_coins_codes.push_back(coin_code);
  }

  if (keys.contains("newRef"))
    the_row[11] = a_row.value("newRef").toString();

  s_rows.push_back(the_row);
}

void ExcelReporter::prepareAUTXORow(const QVDicT a_row)
{
  std::vector<QString> the_row {};
  the_row = fillTheEmptyCells(the_row);

  QStringList keys = a_row.keys();

  if (keys.contains("inx"))
    the_row[0] = CUtils::sepNum(a_row.value("inx").toDouble());

  if (keys.contains("coin_code"))
    the_row[1] = a_row.value("coin_code").toString();

  if (keys.contains("outValue"))
    the_row[2] = CUtils::sepNum(a_row.value("outValue").toDouble());

  if (keys.contains("creationDate"))
    the_row[3] = a_row.value("creationDate").toString();

  if (keys.contains("visibleBy"))
    the_row[4] = a_row.value("visibleBy").toString();

  s_rows.push_back(the_row);
}


// js name was dlTrxList
std::tuple<bool, QString> ExcelReporter::downloadTransactionsList()
{
  s_rows = {};

  s_used_coins_codes = QStringList {};
  s_doublespend_sum = 0;

  QVDRecordsT block_records = DAG::searchInDAG(
    {{"b_type", {CConsts::BLOCK_TYPES::Genesis, CConsts::BLOCK_TYPES::FSign, CConsts::BLOCK_TYPES::FVote}, "NOT IN"}},
    {"DISTINCT b_cycle", "b_hash", "b_type", "b_creation_date", "b_body"},
    {{"b_creation_date", "ASC"}});


  CMPAISValueT totalMinted = 0;
  CMPAISValueT spendasbleMinted_ByBlockClaim = 0;
  CMPAISValueT waitedMinted_ByBlockClaim = 0;
  CMPAISValueT waitedTreasury_ByBlockClaim = 0;
  CMPAISValueT waitedUTXOs_ByBlockClaim = 0;
  CMPAISValueT bBalance = 0;
  uint64_t blockInx = 0;
  QHash<CCoinCodeT, TempTupleCoin > AllUTXOsDict {};
  uint64_t rowNum = 0;

  CDateT max_creation_date = CUtils::getCbUTXOsDateRange().to;
  CDateT a_cycle_before = CUtils::minutesBefore(CMachine::getCycleByMinutes());

  addHeader();
  for (QVDicT a_block: block_records)
  {
    blockInx++;

    QJsonObject block = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(a_block.value("b_body").toString()).content);


    QString block_type = block.value("bType").toString();
    CBlockHashT block_hash = block.value("bHash").toString();

    CDateT block_creation_date = block.value("bCDate").toString();
    addABlockHeader1(blockInx, block_type, block_creation_date);
    addABlockHeader2(block_hash);
    addHeader();

    CMPAISValueT floorish = 0;
    if (block_type == CConsts::BLOCK_TYPES::Coinbase)
    {
      // insert minted coins & treasury incomes(if exist)
      QJsonObject the_doc = block.value("docs").toArray()[0].toObject();
      CDocHashT doc_hash = the_doc.value("dHash").toString();
      CMPAISValueT mintedCoins = the_doc.value("mintedCoins").toDouble();
      totalMinted += mintedCoins;
      floorish += mintedCoins;
      bBalance += mintedCoins;
      if (block_creation_date < max_creation_date)
      {
        spendasbleMinted_ByBlockClaim += mintedCoins;
      } else {
        waitedMinted_ByBlockClaim += mintedCoins;
      }


      prepareARow({
        {"document_type", the_doc.value("dType").toString()},
        {"block_creation_date", block_creation_date},
        {"account", "Minting"},
        {"total_minted", QVariant::fromValue(totalMinted)},
        {"minted", QVariant::fromValue(mintedCoins)},
        {"block_balance", QVariant::fromValue(bBalance)},
        {"desc", "Minting PAIs by block(" + CUtils::hash8c(block_hash) + ")"}});

      CMPAISValueT treasuryIncomes = the_doc.value("treasuryIncomes").toDouble();
      bBalance += treasuryIncomes;
      floorish += treasuryIncomes;

      prepareARow({
        {"document_type", the_doc.value("dType").toString()},
        {"block_creation_date", block_creation_date},
        {"account", "Treasury Incomes"},
        {"treasury", QVariant::fromValue(treasuryIncomes)},
        {"block_balance", QVariant::fromValue(bBalance)},
        {"desc", "Treasury Incomes PAIs by block(" + CUtils::hash8c(block_hash) + ")"}});

      // divide between shreholders
      QJsonArray outputs = the_doc.value("outputs").toArray();
      for (COutputIndexT outInx = 0; outInx < outputs.size(); outInx++)
      {
        QJsonArray anOut = outputs[outInx].toArray();
        CMPAISValueT outValue = anOut[1].toDouble();
        CCoinCodeT the_coin = CUtils::packCoinCode(doc_hash, outInx);
        AllUTXOsDict[the_coin] = TempTupleCoin {anOut[0].toString(), outValue};
        bBalance -= outValue;
        floorish -= outValue;
        prepareARow(
          {{"document_type", the_doc.value("dType").toString()},
          {"block_creation_date", block_creation_date},
          {"account", anOut[0].toString()},
          {"output", QVariant::fromValue(-1 * outValue)},
          {"newRef", CUtils::packCoinCode(doc_hash, outInx)},
          {"block_balance", QVariant::fromValue(bBalance)},
          {"desc", "Share dividend by block(" + CUtils::hash8c(block_hash) + ")"}});
      }

  } else if (QStringList{CConsts::BLOCK_TYPES::Normal, CConsts::BLOCK_TYPES::RpBlock}.contains(block_type)) {
    for (auto a_doc: block.value("docs").toArray())
    {
      QJsonObject the_doc = a_doc.toObject();
      CDocHashT doc_hash = the_doc.value("dHash").toString();

      if (the_doc.keys().contains("inputs"))
      {
        for (auto an_input: the_doc.value("inputs").toArray())
        {
          QJsonArray the_input = an_input.toArray();
          CCoinCodeT coin_code = CUtils::packCoinCode(the_input[0].toString(), the_input[1].toDouble());
          TempTupleCoin refLocInfo = AllUTXOsDict[coin_code];
          bBalance += refLocInfo.m_amount;
          floorish += refLocInfo.m_amount;
          prepareARow(
            {{"document_type", the_doc.value("dType").toString()},
            {"block_creation_date", block_creation_date},
            {"account", refLocInfo.m_hash},
            {"input", QVariant::fromValue(refLocInfo.m_amount)},
            {"block_balance", QVariant::fromValue(bBalance)},
            {"desc", "coin code input block(" + CUtils::hash8c(block_hash) + ")"},
            {"coin_code", coin_code}});

        }
      }

      if (the_doc.keys().contains("outputs"))
      {
        if (the_doc.value("dType").toString() == CConsts::DOC_TYPES::DPCostPay)
        {
          QJsonArray outputs = the_doc.value("outputs").toArray();
          for (COutputIndexT outInx = 0; outInx < outputs.size(); outInx++)
          {
             QJsonArray anOut = outputs[outInx].toArray();
             CMPAISValueT outValue = anOut[1].toDouble();
             CAddressT out_address = anOut[0].toString();
             AllUTXOsDict[CUtils::packCoinCode(doc_hash, outInx)] = {out_address, outValue};

             bBalance -= outValue;
             floorish -= outValue;
             if (out_address == "TP_DP")
             {
               waitedTreasury_ByBlockClaim += outValue;
               prepareARow({{"document_type", the_doc.value("dType").toString()},
                 {"block_creation_date", block_creation_date},
                 {"account", out_address},
                 {"output", QVariant::fromValue(outValue)},
                 {"block_balance", QVariant::fromValue(bBalance)},
                 {"desc", "TP_DP payment block(" + CUtils::hash8c(block_hash) + ")"},
                 {"newRef", CUtils::packCoinCode(doc_hash, outInx)}});

             } else {
                 waitedUTXOs_ByBlockClaim += outValue;
                 prepareARow(
                   {{"document_type", the_doc.value("dType").toString()},
                   {"block_creation_date", block_creation_date},
                   {"account", out_address},
                   {"output", QVariant::fromValue(outValue)},
                   {"block_balance", QVariant::fromValue(bBalance)},
                   {"desc", "Backer fee payment block(" + CUtils::hash8c(block_hash) + ")"},
                   {"newRef", CUtils::packCoinCode(doc_hash, outInx)}});
             }
          }
        } else if (QStringList{CConsts::DOC_TYPES::BasicTx, CConsts::DOC_TYPES::RpDoc}.contains(the_doc.value("dType").toString()))
        {
          QJsonArray outputs = the_doc.value("outputs").toArray();
          for (COutputIndexT outInx = 0; outInx < outputs.size(); outInx++)
          {
            QJsonArray anOut = outputs[outInx].toArray();
            CMPAISValueT outValue = anOut[1].toDouble();
            CAddressT out_address = anOut[0].toString();

            AllUTXOsDict[CUtils::packCoinCode(doc_hash, outInx)] = {out_address, outValue};

            // not calculating backer fee, becaue it is in TP_DP transaaction
            if (the_doc.value("dPIs").toArray().contains(outInx))
            {
              prepareARow(
                {{"document_type", the_doc.value("dType").toString()},
                {"block_creation_date", block_creation_date},
                {"account", out_address},
                {"output", QVariant::fromValue(-1 * outValue)},
                {"desc", "dPIs cost (" + CUtils::microPAIToPAI6(outValue) + ")"},
                {"newRef", CUtils::packCoinCode(doc_hash, outInx)}});
              continue;
            }

            if (block_creation_date < a_cycle_before)
            {
              // do somrthing
            } else {
              waitedUTXOs_ByBlockClaim += outValue;
            }
            bBalance -= outValue;
            floorish -= outValue;
            prepareARow(
              {{"document_type", the_doc.value("dType").toString()},
              {"block_creation_date", block_creation_date},
              {"account", out_address},
              {"output", QVariant::fromValue(-1 * outValue)},
              {"block_balance", QVariant::fromValue(bBalance)},
              {"desc", "trx paying block(" + CUtils::hash8c(block_hash) + ")"},
              {"newRef", CUtils::packCoinCode(doc_hash, outInx)}});
          }
        }
      }
      prepareARow({});
    }
  }

    bBalance -= floorish;
    if (floorish != 0)
    {
      QJsonObject the_doc = block.value("docs").toArray()[0].toObject();
      prepareARow(
        {{"document_type", the_doc.value("dType").toString()},
        {"block_creation_date", block_creation_date},
        {"account", "floorish"},
        {"output", QVariant::fromValue(-1 * floorish)},
        {"block_balance", QVariant::fromValue(bBalance)},
        {"desc", "lost by floorish block(" + CUtils::hash8c(block_hash) + ")"}});
    }


    // balance at the end of block
    // this.prepareARow({ bBalance, desc: 'Block Balance' });

    // gap between  blocks
    prepareARow({});
    prepareARow({});


  }


  // footer numbers
  prepareARow({
    {"date", CUtils::getNow()},
    {"account", "Spendasble Minted PAIs By Block Claim"},
    {"totalMinted", QVariant::fromValue(spendasbleMinted_ByBlockClaim)},
    {"minted", QVariant::fromValue(-1 * spendasbleMinted_ByBlockClaim)},
    {"bBalance", 0},
    {"desc", "Spendasble Minted PAIs By Block Claim"}});

  prepareARow({
    {"date", CUtils::getNow()},
    {"account", "Waited Minted PAIs By Block Claim"},
    {"totalMinted", QVariant::fromValue(waitedMinted_ByBlockClaim)},
    {"minted", QVariant::fromValue(-1 * waitedMinted_ByBlockClaim)},
    {"bBalance", 0},
    {"desc", "Waited Minted PAIs By Block Claim"}});

  prepareARow({
    {"date", CUtils::getNow()},
    {"account", "Waited UTXOs By block claim"},
    {"totalMinted", QVariant::fromValue(waitedUTXOs_ByBlockClaim)},
    {"minted", QVariant::fromValue(-1 * waitedUTXOs_ByBlockClaim)},
    {"bBalance", 0},
    {"desc", "Waited UTXOs By block claim"}});

  prepareARow({
    {"date", CUtils::getNow()},
    {"account", "Waited Treasury By block claim"},
    {"totalMinted", QVariant::fromValue(waitedTreasury_ByBlockClaim)},
    {"minted", QVariant::fromValue(-1 * waitedTreasury_ByBlockClaim)},
    {"bBalance", 0},
    {"desc", "Waited Treasury By block claim"}});

  auto[
    total_spendable_coins,
    utxos,
    utxos_dict] = UTXOHandler::getSpendablesInfo();

  prepareARow(
    {{"date", CUtils::getNow()},
    {"account", "Total spendables retrieved from trx_utxos table"},
    {"totalMinted", QVariant::fromValue(total_spendable_coins)},
    {"minted", QVariant::fromValue(-1 * total_spendable_coins)},
    {"bBalance", 0},
    {"desc", "Total spendables retrieved from trx_utxos table"}});

  prepareARow(
    {{"date", CUtils::getNow()},
    {"account", "Double spends Sum"},
    {"totalMinted", QVariant::fromValue(s_doublespend_sum)},
    {"minted", QVariant::fromValue(-1 * s_doublespend_sum)},
    {"bBalance", 0},
    {"desc", "Double spends Sum"}});


  // prepare a list of unique UTXOs in table trx_utxos
  prepareAUTXORow(
    {{"coin_code", "Coin Code"},
    {"outValue", 0},
    {"creationDate", "Creation Date"},
    {"visibleBy", "Visible By"}});

  int inx = 0;
  for (QVDicT aUT: utxos)
  {
    inx++;
    prepareAUTXORow(
      {{"inx", inx},
      {"refLoc", aUT.value("refLoc").toString()},
      {"outValue", aUT.value("outValue").toDouble()},
      {"creationDate", aUT.value("refLocCreationDate").toString()},
      //{"visibleBy", utxos_dict[aUT.value("refLoc").toString()].value("visibleBy").join(", ")}
       });
  }

  QString reports_path = CMachine::getReportsPath();
  QString file_name = CUtils::getNowSSS() + "_trx-bBalances.xlsx";
  QStringList rows {};
  for (std::vector<QString> a_row: s_rows)
  {
    QString a_row_str = "";
    for (QString a_cell: a_row)
    {
      a_row_str += a_cell + CELL_SEPERSATOR;
    }
    rows.append(a_row_str);
  }
  QString content = rows.join(END_LINE);

  CLog::log("############## excel reporter Done! ######### ");
  bool is_created = FileHandler::write(
    reports_path,
    file_name,
    content);
  CLog::log(content, "app", "trace");
  CLog::log("############## excel reporter Done! ######### ");
  if (!is_created)
    return {false, "Failed in creating CSAV file!"};

  return {true, "The CSV file is created " + file_name};
}
