#ifndef EXCELREPORTER_H
#define EXCELREPORTER_H


class ExcelReporter
{
public:
  ExcelReporter();

  static std::vector<std::vector<QString> > s_rows;
  static QStringList s_used_coins_codes;
  static CMPAISValueT s_doublespend_sum;

  static void addHeader();

  static std::vector<QString> fillTheEmptyCells(std::vector<QString> the_row);

  static void addABlockHeader1(
    uint64_t block_index,
    QString block_type,
    CDateT block_creation_date);

  static void addABlockHeader2(CBlockHashT block_hash);

  static void prepareARow(const QVDicT a_row);

  static std::tuple<bool, QString> downloadTransactionsList();

  static void prepareAUTXORow(const QVDicT a_row);

};

#endif // EXCELREPORTER_H
