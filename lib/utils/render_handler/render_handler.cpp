#include "stable.h"

#include "render_handler.h"

const QStringList RenderHandler::m_acceptable_extensions = {
  "png", "gif", "jpg", "jpeg", "jp2", "webp", "ppt", "pdf", "psd",
  "mp3", "xls", "xlsx", "swf", "doc", "docx", "odt", "odc", "odp",
  "odg", "mpp",
  "zip", "tar", "gz"};

const QStringList RenderHandler::m_image_extensions = {"jpg", "jpeg", "png", "webp", "gif", "svg", "tiff"};

RenderHandler::RenderHandler()
{

}

/**
 * @brief RenderHandler::renderImgAttrs
 * @param segments
 * @return {start_tag, end_tag, attr_string}
 */
ImgAttrs RenderHandler::renderImgAttrs(QStringList segments)
{
  ImgAttrs response {};
  for (QString aSeg: segments)
  {
    QStringList segDtl = aSeg.split("=");
    if (segDtl[0] == "link")
    {
      response.m_start_tag =  "<a href=\"" + segDtl[1] + "\">";
      response.m_end_tag =  "</a>";

    } else if (segDtl[0] == "alt") {
      response.m_attributes_string += " alt=\"" + CUtils::sanitizingContent(segDtl[1]) + "\" ";

    }
  }

  return response;
}


// e.g. [[File:accumulative-shares-of-alice.png|Accumulative shares of alice]]
// [[File:142c3bc70608df26ace66e287966b4953a96b427facbd5dc8d88bd8ac83a3310.jpg|nauture]]
//
QString RenderHandler::renderFile(QStringList segments)
{
  QString segZero = segments[0].midRef(5).toString(); // remove "File:" prefix
  segments.pop_front();

  QString caption = "";
  if (segments.size() > 0)
    caption = segments[segments.size() - 1];
  caption = CUtils::sanitizingContent(caption);

  QStringList segZeroDtl = segZero.split(".");
  QString extension = segZeroDtl[segZeroDtl.size() - 1];
  QString out = "";
  if (!m_acceptable_extensions.contains(extension))
    return "Invalid extension(" + CUtils::sanitizingContent(extension) + ") for file(" + caption + ")";

  if (m_image_extensions.contains(extension))
  {
    // egZero segZero  cache-files/cache/142c3bc70608df26ace66e287966b4953a96b427facbd5dc8d88bd8ac83a3310.jpg
    QString fileFinalPath = "";
    if (segZero.contains("/"))
    {
      fileFinalPath = segZero;
      // probably is an external link
    } else {
      fileFinalPath = CMachine::getCachePath() + "/" + segZero;
    }
    out = "<img src='" + fileFinalPath + "' ";
  }
  if (caption == "")
  {
    out += ">";
  } else {
    out += " title=" + caption + "'";
    if (segments.size() > 0)
    {
      ImgAttrs imgAttr = renderImgAttrs(segments);
      out += imgAttr.m_attributes_string;
      out += '>';
      if (imgAttr.m_start_tag != "")
          out = imgAttr.m_start_tag + out;
      if (imgAttr.m_end_tag != "")
          out = out + imgAttr.m_end_tag;
    } else {
        out += '>';
    }
  }
  return out;
}

QString RenderHandler::renderLink(QString link)
{
  link = link.replace("[[", "");
  link = link.replace("]]", "");
  QStringList segments = link.split('|');
  QString output = "";
  if (segments[0].midRef(0, 4).toString() == "File")
  {
    output = renderFile(segments);
  } else {
    output = "<a href=\"" + segments[0] + "\">";
    if (segments.size() > 1)
    {
      output += segments[1];
    } else {
      output += segments[0];
    }
    output += "</a>";

  }
  return output;
}



QStringList RenderHandler::extractLinks(QString content)
{
  // QString str = "d because of that proposals. [[imagine/wiki/contributors|Here]] is a graph of [[zimagine/ziki/zontributors|zere]]  contr";
  QStringList elements = content.split("[[");
  if (elements.size() == 0)
    return {};

  QStringList out {};
  int inx = 0;
  for (QString a_seg: elements)
  {
    inx++;
    if (inx == 1)
     continue;

    out.append("[[" + a_seg.split("]]")[0] + "]]");
  }

  return out;
}

QString RenderHandler::renderLinks(QString content)
{
  if (content == "")
    return "";

  QStringList links = extractLinks(content);
  if (links.size() == 0)
    return content;

  for (QString a_link: links)
  {
    QString new_link = renderLink(a_link);
    content = content.replace(a_link, new_link);
  }

  return content;
}

QString RenderHandler::renderEndLines(QString content)
{
//  content = content.replace("<br />", "\n");
//  content = content.replace("<br/>", "\n");
//  content = content.replace("<br>", "\n");
  content = content.replace("\n", "<br>");
  return content;
}


QString RenderHandler::renderToHTML(QString content)
{
  content = renderLinks(content);
  content = renderEndLines(content);
  return content;
}
