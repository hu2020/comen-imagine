#ifndef RENDERHANDLER_H
#define RENDERHANDLER_H



struct ImgAttrs
{
  QString m_start_tag = "";
  QString m_end_tag = "";
  QString m_attributes_string = "";
};


/**
 * @brief The RenderHandler class
 * this class converts a Mark Down text to something beautiful for GUI
 * it supports variety of arkDown languages (including mediawiki format and github MD implementation)
 * it needs heavy development
 */
class RenderHandler
{
public:
  RenderHandler();

  static const QStringList m_acceptable_extensions;
  static const QStringList m_image_extensions;

  static QString renderToHTML(QString content);
  static ImgAttrs renderImgAttrs(QStringList segments);
  static QString renderFile(QStringList segments);
  static QString renderLink(QString link);
  static QString renderLinks(QString content);
  static QString renderEndLines(QString content);
  static QStringList extractLinks(QString content);


};

#endif // RENDERHANDLER_H
