#include "stable.h"
#include "version_handler.h"

VersionHandler::VersionHandler()
{

}

/**
 * @brief VersionHandler::isOlderThan
 * @param v1
 * @param vRef
 * @return -1 on error, 1 on ture, 0 on false
 */
int VersionHandler::isOlderThan(const QString& v1, const QString& vRef)
{

  int64_t v1Value = convertVerToVal(v1);
  int64_t vRefValue = convertVerToVal(vRef);
  if ((v1Value < 0) || (vRefValue < 0))
    return -1;
  return (v1Value < vRefValue);
}

int64_t VersionHandler::convertVerToVal(const QString& version)
{
  QStringList v_seg = version.split(".");
  QVector<int> version_segments;
  for (auto a_seg: v_seg)
  {
    bool is_valid;
    int seg_int = a_seg.toInt(&is_valid);
    if (!is_valid || (seg_int < 0) || (a_seg != QString::number(seg_int)))
      return -1;
    version_segments.append(seg_int);
  }

  int64_t versionValue = 1000000 * version_segments[0] + 1000 * version_segments[1] + version_segments[2];
  return versionValue;
}

bool VersionHandler::isValid(const QString& version)
{
  QStringList v_seg = version.split(".");
  QVector<int> version_segments;
  for (auto a_seg: v_seg)
  {
    bool is_valid;
    int seg_int = a_seg.toInt(&is_valid);
    if (!is_valid || (seg_int < 0) || (a_seg != QString::number(seg_int)))
      return false;
  }
  return true;
}


bool VersionHandler::isNewerThan(
  const QString& v1,
  const QString& vRef)
{
  int64_t v1Value = convertVerToVal(v1);
  int64_t vRefValue = convertVerToVal(vRef);
  return (v1Value > vRefValue);
}
