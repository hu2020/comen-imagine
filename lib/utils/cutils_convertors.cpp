#include "cutils.h"


BlockLenT CUtils::convertPaddedStringToInt(QString s)
{
  while(s.midRef(0, 1).toString() == "0")
    s = s.midRef(1).toString();
  return s.toDouble();
}

QStringList CUtils::convertJSonArrayToQStringList(const QJsonArray& arr)
{
  QStringList out = {};
  for(QJsonValue an_elm: arr)
    out.append(an_elm.toString());
  return out;
}

QJsonArray CUtils::convertQStringListToJSonArray(const QStringList& arr)
{
  QJsonArray out {};
  for(QJsonValue an_elm: arr)
    out.append(an_elm);
  return out;
}
