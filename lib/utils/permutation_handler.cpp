#include "permutation_handler.h"



PermutationHandler::PermutationHandler(
    QStringList elements,
    uint32_t subset_count,
    bool shouldBeUnique,
    QList<QStringList> premutions,
    QStringList  premutionsStringify
    )
{
  m_elements = elements;
  m_shouldBeUnique = shouldBeUnique;
  m_permutations = premutions;
  m_permutationsStringify = premutionsStringify;

  if (subset_count>0)
    recursiveHeapP(m_elements, subset_count);

}

void PermutationHandler::heapP(QStringList values)
{
  if (values.size()==0)
    values = m_elements;

  recursiveHeapP(values, values.size());
}

void PermutationHandler::recursiveHeapP(QStringList values, uint32_t subset_count, int inner_size)
{

  // FIXME: implement automatic calculation START
  QString mod = QString("%1").arg(subset_count) + "/" + QString("%1").arg(m_elements.size());
  mod = mod.replace(QRegExp("[' ']{1,}"), "");

  if (mod == "1/1")
  {
    m_permutations = {
      {values[0]}
    };
    return;
  }

  if (mod == "1/2")
  {
    m_permutations = {
      {values[0]},
      {values[1]}
    };
    return;
  }
  if (mod == "2/2")
  {
    m_permutations = {
      {values[0], values[1]}
    };
    return;
  }

  if (mod == "1/3")
  {
    m_permutations = {
      {values[0]},
      {values[1]},
      {values[2]}
    };
    return;
  }
  if (mod == "2/3")
  {
    m_permutations = {
      {values[0], values[1]},
      {values[0], values[2]},
      {values[1], values[2]}
    };
    return;
  }
  if (mod == "3/3")
  {
    m_permutations = {
      {values[0], values[1], values[2]}
    };
    return;
  }

  if (mod == "1/4")
  {
    m_permutations = {
      {values[0]},
      {values[1]},
      {values[2]},
      {values[3]}
    };
    return;
  }
  if (mod == "2/4")
  {
    m_permutations = {
      {values[0], values[1]},
      {values[0], values[2]},
      {values[0], values[3]},
      {values[1], values[2]},
      {values[1], values[3]},
      {values[2], values[3]}
    };
    return;
  }
  if (mod == "3/4")
  {
    m_permutations = {
      {values[0], values[1], values[2]},
      {values[0], values[1], values[3]},
      {values[0], values[2], values[3]},
      {values[1], values[2], values[3]}
    };
    return;
  }
  if (mod == "4/4")
  {
    m_permutations = {
      {values[0], values[1], values[2], values[3]}
    };
    return;
  }

  if (mod == "1/5")
  {
    m_permutations = {
      {values[0]},
      {values[1]},
      {values[2]},
      {values[3]},
      {values[4]}
    };
    return;
  }
  if (mod == "2/5")
  {
    m_permutations = {
      {values[0], values[1]},
      {values[0], values[2]},
      {values[0], values[3]},
      {values[0], values[4]},
      {values[1], values[2]},
      {values[1], values[3]},
      {values[1], values[4]},
      {values[2], values[3]},
      {values[2], values[4]},
      {values[3], values[4]}
    };
    return;
  }
  if (mod == "3/5")
  {
    m_permutations = {
      {values[0], values[1], values[2]},
      {values[0], values[1], values[3]},
      {values[0], values[1], values[4]},
      {values[0], values[2], values[3]},
      {values[0], values[2], values[4]},
      {values[0], values[3], values[4]},
      {values[1], values[2], values[3]},
      {values[1], values[2], values[4]},
      {values[1], values[3], values[4]},
      {values[2], values[3], values[4]}
    };
    return;
  }
  if (mod == "4/5")
  {
    m_permutations = {
      {values[0], values[1], values[2], values[3]},
      {values[0], values[1], values[2], values[4]},
      {values[0], values[1], values[3], values[4]},
      {values[0], values[2], values[3], values[4]},
      {values[1], values[2], values[3], values[4]},
    };
    return;
  }
  if (mod == "5/5")
  {
    m_permutations = {
      {values[0], values[1], values[2], values[3], values[4]}
    };
    return;
  }

  // it is a custom 5/7. later implement a homogenous premutation
  if (mod == "5/7")
  {
    m_permutations = {
      {values[0], values[1], values[2], values[3], values[4]},  // 5,6
      {values[0], values[1], values[2], values[3], values[5]},  // 4,6
      {values[0], values[1], values[2], values[4], values[5]},  // 3,6
      {values[0], values[1], values[3], values[4], values[5]},  // 2,6
      {values[0], values[2], values[3], values[4], values[5]},  // 1,6
      {values[1], values[2], values[3], values[4], values[5]},  // 0,6
      {values[0], values[1], values[2], values[3], values[6]},  // 4,5
      {values[0], values[1], values[2], values[4], values[6]},  // 3,5
      {values[0], values[1], values[3], values[4], values[6]},  // 2,5
      {values[0], values[2], values[3], values[4], values[6]},  // 1,5
      {values[1], values[2], values[3], values[4], values[6]},  // 0,5
      {values[0], values[1], values[2], values[5], values[6]},  // 3,4
      {values[0], values[1], values[3], values[5], values[6]},  // 2,4
      {values[0], values[2], values[3], values[5], values[6]},  // 1,4
      {values[1], values[2], values[3], values[5], values[6]},  // 0,4
      {values[0], values[1], values[4], values[5], values[6]},  // 2,3
      {values[0], values[2], values[4], values[5], values[6]},  // 1,3
      {values[1], values[2], values[4], values[5], values[6]},  // 0,3
      {values[0], values[3], values[4], values[5], values[6]},  // 1,2
      {values[1], values[3], values[4], values[5], values[6]},  // 0,2
      {values[2], values[3], values[4], values[5], values[6]}  // 0,1
    };
    return;
  }

  CLog::log("not supported premutation " + mod, "app", "fatal");
  exit(99);

  // FIXME: implement automatic calculation END















  if (inner_size == -1)
  {
    inner_size = values.size();
  }
  // if inner_size becomes 1 then prints the obtained permutation

  if (inner_size == 1)
  {
    printArr(values, subset_count);
    // return;
  }

  for (int64_t i = 0; i < inner_size; i++)
  {
    recursiveHeapP(values, subset_count, inner_size - 1);

    // if inner_size is odd, swap first and last element
    if (inner_size % 2 == 1)
    {
        QString tmp = values[0];
        values[0] = values[inner_size - 1];
        values[inner_size - 1] = tmp;

        // swap(a[0], a[inner_size-1]);

        // If inner_size is even, swap ith and last
        // element
    }
    else
    {
        QString tmp = values[i];
        values[i] = values[inner_size - 1];
        values[inner_size - 1] = tmp;
        // swap(a[i], a[inner_size - 1]);
    }
  }
}

void PermutationHandler::printArr(QStringList values, uint32_t subset_count)
{
  QStringList out{};
  QString strOut = "";
  for (uint32_t i = 0; i < subset_count; i++)
  {
    out.append(values[i]);
  }
  QStringList tmp_out;
  tmp_out = out;
  tmp_out.sort();

  strOut = tmp_out.join(","); // JSON.stringify(_.sortBy(out));

    if (m_shouldBeUnique) {
      if (!m_permutationsStringify.contains(strOut)) {
          m_permutations.append(out);
          m_permutationsStringify.append(strOut);
      }

    } else {
      m_permutations.append(out);
      m_permutationsStringify.append(strOut);
    }
};


void PermutationHandler::testAnalyze(QList<QStringList> premutations)
{
  if (premutations.size() == 0)
  {
    premutations = m_permutations;
  }

  QStringList out;
  for (QStringList a_set: premutations)
  {
    QString an_not_ordered_key = a_set.join(",");
    a_set.sort();
    QString an_ordered_key = a_set.join(",");
    PremInfo a_prem_info = m_test_analyze.value(an_ordered_key, PremInfo{});
    if (a_prem_info.count == 0)
    {
      m_test_analyze.insert(an_ordered_key, a_prem_info);
    }
    a_prem_info.count++;
    a_prem_info.variety.append(an_not_ordered_key);
    m_test_analyze.insert(an_ordered_key, a_prem_info);

    out.push_back(a_set.join(","));
  }
//    const r = _.values(_.groupBy(out)).map(d => ({ name: d[0], count: d.length }));
//    let result = {}
//    r.forEach(elm => {
//        result[elm.name] = elm.count;
//    });
//    return result
}
