#ifndef CUtils_H
#define CUtils_H

#include <math.h>

#include <stdint.h>
#include <algorithm>
#include <string>
#include <regex>

#include <boost/config.hpp>
#include <boost/regex.hpp>

#include <QString>
#include <QVector>
#include <QStringList>
#include <QJsonObject>
#include <QJsonDocument>

#include "lib/vector.h"

//#include "vector.h"
#include <qdebug.h>
#include <qdatetime.h>

#include "constants.h"
#include "lib/clog.h"

class UnlockSet;
class IndividualSignature;
class UnlockDocument;
class CMachine;
class Document;
class SpendCoinsList;


#include "lib/database/db_model.h"
#include "lib/machine/machine_handler.h"
#include "lib/wallet/wallet.h"
#include "lib/dag/normal_block/normal_utxo_handler.h"
#include "lib/block/block_types/block_coinbase/coinbase_utxo_handler.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"

class TimeDiff
{
public:
  uint64_t asYears = 0;
  uint64_t years = 0;
  uint64_t asMonths = 0;
  uint64_t months = 0;
  uint64_t asDays = 0;
  uint64_t days = 0;
  uint64_t asHours = 0;
  uint64_t hours = 0;
  uint64_t asMinutes = 0;
  uint64_t minutes = 0;
  uint64_t asSeconds = 0;
  uint64_t seconds = 0;

  TimeDiff(
    const uint64_t& asYears_ = 0,
    const uint64_t& years_ = 0,
    const uint64_t& asMonths_ = 0,
    const uint64_t& months_ = 0,
    const uint64_t& asDays_ = 0,
    const uint64_t& days_ = 0,
    const uint64_t& asHours_ = 0,
    const uint64_t& hours_ = 0,
    const uint64_t& asMinutes_ = 0,
    const uint64_t& minutes_ = 0,
    const uint64_t& asSeconds_ = 0,
    const uint64_t& seconds_ = 0
  );
  bool operator ==(const TimeDiff& obj);
  bool operator !=(const TimeDiff& obj);

};

struct TimeRange
{
  QString from = "";
  QString to = "";
};

typedef double longDouble;

class CUtils
{
public:

  CUtils(){};

  static QString convertFloatToString(double num, uint8_t percision = 11);
  static double customFloorFloat(double number, uint8_t percision = 11);
  static double iFloorFloat(double number);
  static double CFloor(double v);

  static std::tuple <longDouble, longDouble, longDouble, longDouble> calcLog(
    longDouble xValue,
    uint64_t range,
    uint64_t exp = 1);
  static QString lightRandom(const int& max=100);


  static bool isGreaterThanNow(const QString& cDate);
  static void exitIfGreaterThanNow(const QString& cDate);

  static QString getFirstNWords(const QString& text, const uint32_t words_count = 20);

  static QString dumpIt(const bool& status);
  static QString dumpIt(const QStringList& values, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVector<QStringList>& rows, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVDicT& values, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const UI64DicT& values, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QList<QVDicT>& rows, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVDRecordsT& rows, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QHash<QString, QHash<QString, QVDRecordsT>>& groupped_rows, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVector<QVector<QVDicT> >& rows, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  template<typename T>
  QList<T> chunkIt(T values, uint64_t chunk_size=2);
  static QVector<QStringList> chunkQStringList(QStringList values, uint64_t chunk_size=2);
  static QVector<QStringList> chunkStringList(QStringList values, uint64_t chunk_size);
  static QStringList chunkString(const QString& content, const uint16_t& size);
  static QString dumpIt(const std::vector<CMPAIValueT>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const std::vector<QStringList>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const std::vector<double>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const UnlockSet& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVector<UnlockSet>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const UnlockDocument& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const IndividualSignature& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVector<IndividualSignature>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QV2DicT& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const CListListT& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QHash<QString, DNAShareCountT>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QHash<QString, QString>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QJsonValue& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QJsonObject& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const JORecordsT& rows, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QJsonArray& rows, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const GRecordsT& groupped_rows, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QHash<QString, Document*>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QHash<QString, QJsonObject>& custom_data, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const ClausesT&, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpDoubleSpends(const SpendCoinsList*, const QString& prefix_tabs = CConsts::DUMPER_INDENT);
  static QString paddingLengthValue(const int& value, const int& neededLen = 7);
  static QString paddingLengthValue(const QString& value, const int& neededLen = 7);

  static QString hash4c(const QString& s);
  static QString hash6c(const QString& s);
  static QString hash8c(const QString& s);
  static QString hash16c(const QString& s);
  static QString hash32c(const QString& s);
  static QString hash64c(const QString& s);
  static QString shortBech16(const QString& s);
  static QStringList shortStringsList(const QStringList& items, const int len = 8);

  static QStringList removeEmptyElements(const QStringList& elms);
  static QString removeNewLine(QString s);
  static QString replaceNewLineByBackN(QString s);
  static QString removeTab(QString s);
  static QString removeTabNL(const QString& s);
  static QString removeDblSpaces(QString s);
  static QString breakByBR(const QString& content, const uint16_t& size = 100);
  static QString stripBR(QString content);
  static QString stripOutputAddress(QString content);
  static QString stripNonAlphaNumeric(QString content);
  static QString stripNonInDateString(QString content);
  static QString stripNonNumerics(QString content);

  static QJsonObject parseToJsonObj(const QString& serialized = "{}");
  static QJsonObject parseToJsonObj(const QVariant& serialized);
  static QJsonArray parseToJsonArr(const QString& serialized);
  static QString serializeJson(const QJsonObject& values);
  static QString serializeJson(const QJsonArray& values);
  static QString serializeJson(const QStringList& values);
  static QString getANullStringifyedJsonObj();
  static QString sanitizingContent(const QString& str);
//  static QStringList convertJSonArrayToStringList(const QJsonArray& arr);

  static QString getNow();
  static QString getNowSSS();
  static QString getCurrentYear();
  static bool isValidDateForamt(const QString& cDate);

  static TimeDiff timeDiff(const QString& fromT, const QString& toT = getNow());
  //TimeDiff timeDiff(const QString& fromT, const QString& toT = QString::fromStdString(getNow()));
  static QString minutesBefore(const uint64_t& backInTimesByMinutes, const QString& cDate = "");
  static QString minutesAfter(const uint64_t& forwardInTimesByMinutes, const QString& cDate = "");
  static QString secondsAfter(const uint64_t& forward_in_time_by_seconds, const QString& cDate);
  static QString yearsBefore(const uint64_t& backInTimesByYears, const QString& cDate = "");
  static QString yearsAfter(const uint64_t& forwardInTimesByYears, const QString& cDate = "");
  static QString getCoinbaseCycleStamp(QString cDate = "");
  static uint getNowByMinutes();
  static QString getCoinbaseCycleNumber(QString cDate = getNow());
  static TimeByMinutesT getCoinbaseAgeByMinutes(const CDateT& cDate = getNow());
  static TimeBySecT getCoinbaseAgeBySecond(const CDateT& cDate = getNow());
  static QString isAmOrPm(const uint& minutes);
  static uint getCycleByMinutes();
  static TimeBySecT getCycleBySeconds();
  static uint getCycleCountPerDay();
  static QString convertMinutesToHHMM(uint minutes);
  static uint getCycleElapsedByMinutes(QString cDate = "");
  static TimeRange getACycleRange(
    QString cDate = getNow(),
    uint back_by_cycle = 0,
    uint forward_by_cycle = 0);
  static TimeRange getCoinbaseRange(const QString& cDate = getNow());
  static TimeRange getCbUTXOsDateRange(QString cDate = getNow());
  static TimeRange getCoinbaseRangeByCycleStamp(const QString& cycle);
  static std::tuple<QString, QString, QString, QString, QString> getCoinbaseInfo(
    QString cDate = "",
    QString cycle = "");
  static std::tuple<QString, QString, QString, QString, QString> getPrevCoinbaseInfo(const QString& cDate = getNow());
  static bool isYoungerThan2Cycle(const QString& cDate);

  static bool isValidHash(QString s);
  static bool isAValidEmailFormat(QString email);
  static QString stripNonHex(QString s);
  static QString sepNum(int64_t number);
  static QString microPAIToPAI6(CMPAISValueT number);
  static BlockLenT convertPaddedStringToInt(QString s);

  static QStringList convertJSonArrayToQStringList(const QJsonArray& arr);
  static QJsonArray convertQStringListToJSonArray(const QStringList& arr);

  static bool contains_(const std::vector<QString> v, const QString& x);

  static QStringList arrayDiff(const QStringList& superset, const QStringList& subset);
  static QStringList arrayAdd(const QStringList& arr1, const QStringList& arr2);
  static QStringList arrayUnique(QStringList arr);
  static QStringList arrayIntersection(const QStringList& set_one, const QStringList& set_two);
  static QStringList convertCommaSeperatedToArray(const QString& inp, const QString& seperator = ",");

  static std::vector<CMPAIValueT> uint64Unique(std::vector<CMPAIValueT> l);
  static std::vector<CMPAIValueT> intReverseSort(std::vector<CMPAIValueT> l);
  static QString normalizeCommaSeperatedStr(QString str) ;
  static QStringList unpackCommaSeperated(QString str);
  static QString packCommaSeperated(QStringList arr);
  static QString uniqueCommaSeperatedStr(QString str);
  static QStringList removeNullMembersFromCommaSeperated(QStringList arr);

//  -  -  -  math wrappers
  static CMPAIValueT truncMPAI(const float value);
  static CMPAIValueT truncMPAI(const double value);

// - - - - - - legacy iutils part - - - - -

  static int getAppCloneId();
  static bool isValidVersionNumber(const QString& ver);
  static void exiter(const QString& msg, const uint8_t& number = 0);
  static void exiter(const QString& msg, const int& number);
  static CCoinCodeT packCoinCode(const CDocHashT& ref_trx_hash, const COutputIndexT& output_index);
  static CCoinCodeT packCoinCode(const CDocHashT& ref_trx_hash, const QString& output_index);
  static QString textCleaner(QString inp);

  /**
   * @brief unpackCoinCode
   * @param coin
   * @return {doc_hash, ouput_index}
   */
  static std::tuple<QString, COutputIndexT> unpackCoinCode(const CCoinCodeT& coin);

  static QString shortCoinRef(const QString& coin);

  static QString packCoinSpendLoc(
    const QString& block_hash,
    const QString& transaction_hash);

  static std::tuple<QString, QString> unpackCoinSpendLoc(const QString& refLoc);

  static uint8_t getMatureCyclesCount(const QString& document_type);
  static bool isMatured(
    const QString& docType,
    const QString& coinCreationDate,
    const QString& cDate = CUtils::getNow());

  static bool isInCurrentCycle(const QString& cDate = getNow());

  static std::tuple<bool, QString> compressString(const QString& inp);
  static std::tuple<bool, QString> decompressString(const QString& inp);


};

#endif // namespace CUtils_H
