#include "stable.h"
#include "lib/ccrypto.h"
#include "cmerkle.h"


namespace CMerkle
{

QString dumpAProof(const MerkleNodeData& a_proof)
{
  QString out = "";
  out += "\n Parent: " + a_proof.m_parent;
  out += "\n Proof Keys: " + a_proof.m_proof_keys.join(", ");
  out += "\n Left Hash: " + a_proof.m_left_hash;
  out += "\n Merkle Proof: " + a_proof.m_merkle_proof.join(", ");
  return out;
}

QString dumpProofs(const MNodesMapT& proofs)
{
  QString out = "";
  for (QString a_leave_hash: proofs.keys())
  {
    out += "\n\n The Proof for leave hash: " + a_leave_hash;
    out += dumpAProof(proofs[a_leave_hash]);
  }
  return out;
}

//static merklePresenter(m) {
//    let out = `Root: ${m.root} \nLeaves: ${m.levels} \nLevels: ${m.leaves} \nProofs:`;

//    let inx = 0;
//    _.forOwn(m.proofs, function (value, key) {
//        out += `\n\tleave(${inx}): ${key} => ` + ((utils._notNil(value.m_left_hash)) ? `\n\t\tleftHsh: ${value.m_left_hash}` : ``)
//        out += `\n\t\thashes: `
//        value.m_merkle_proof.forEach(element => {
//            out += element + ` `
//        });
//        inx += 1;
//    });
//    return out;
//}

QString getRootByAProve(QString leave, QStringList proof_hashes, QString lHash, QString input_type, QString hash_algorithm)
{
  if (input_type == "string")
  {
    leave = doHashANode(leave, hash_algorithm);
  }
  QString proof = "";
  if (lHash != "")
  {
    proof = doHashANode(lHash + leave, hash_algorithm);
  }
  else
  {
    proof = leave;
  }

  if (proof_hashes.size() > 0)
  {
    for (QString element : proof_hashes)
    {
      QString pos = element.mid(0, 1);
      QString val = element.mid(2);
      if (pos == "r")
      {
        proof = doHashANode(proof + val, hash_algorithm);
      }
      else
      {
        proof = doHashANode(val + proof, hash_algorithm);
      }
    }
  }
  return proof;
}

QString doHashANode(const QString& node_value, const QString& hash_algorithm)
{
  if (hash_algorithm == "keccak256")
  {
    return CCrypto::keccak256(node_value);
  }
  if (hash_algorithm == "noHash")
  {
    return node_value;
  }
  if (hash_algorithm == "aliasHash")
  {
    return "h(" + node_value + ")";
  }

  CLog::log("Invalid hash for merkle!");
  exit(888);
}

std::tuple<QString, MNodesMapT, QString, int, int> generate(QStringList elms, QString input_type, QString hash_algorithm, QString version)
{
  MNodesMapT p;
  if (elms.size() == 0)
  {
    return {
      "", //root
      p, //proofs
      version,
      0,  //levels
      0  //leaves
    };
  }

  if (elms.size() == 1)
  {
    return {
      (input_type == "hashed") ? elms[0] : doHashANode(elms[0], hash_algorithm),  //root
      p, // proofs
      version,
      1,  //levels
      1  // leaves
    };
  }

  // fitting leaves conut
  int needed_leaves = 0;
  if ((2 < elms.size()) && (elms.size() < 4))
  {
    needed_leaves = 4;
  }else if ((4 < elms.size()) && (elms.size() < 8))
  {
    needed_leaves = 8;
  }else if ((8 < elms.size()) && (elms.size() < 16))
  {
    needed_leaves = 16;
  }else if ((16 < elms.size()) && (elms.size() < 32))
  {
    needed_leaves = 32;
  }else if ((32 < elms.size()) && (elms.size() < 64))
  {
    needed_leaves = 64;
  }else if ((64 < elms.size()) && (elms.size() < 128))
  {
    needed_leaves = 128;
  }else if ((128 < elms.size()) && (elms.size() < 256))
  {
    needed_leaves = 256;
  }else if ((256 < elms.size()) && (elms.size() < 512))
  {
    needed_leaves = 512;
  }else if ((512 < elms.size()) && (elms.size() < 1024))
  {
    needed_leaves = 1024;
  }else if ((1024 < elms.size()) && (elms.size() < 2048))
  {
    needed_leaves = 2048;
  }else if ((2048 < elms.size()) && (elms.size() < 4096))
  {
    needed_leaves = 4096;
  }
  for (int inx = elms.size(); inx < needed_leaves; inx++)
  {
    elms.append("leave_" + QString::number(inx+1));
  }
  auto[root, verifies, leaves, levels] = innerMerkle(elms, input_type, hash_algorithm);

  // let verifies;
  MNodesMapT final_verifies;
  QStringList keys = verifies.keys();
  keys.sort();  // FIXME: the right order is reverse sort, also consider the version 0.1.0 in order to solve "fake right leave" problem
  for (QString key : keys)
  {
    MerkleNodeData a_proof = verifies[key];
    final_verifies[key.midRef(2).toString()].m_merkle_proof = a_proof.m_proof_keys;
    final_verifies[key.midRef(2).toString()].m_left_hash = a_proof.m_left_hash;
  }
  return {
      root,
      final_verifies,
      version,
      levels,
      leaves
    };
};


std::tuple<QString, MNodesMapT, int, int> innerMerkle(QStringList elms, QString input_type, QString hash_algorithm, QString version)
{
  if (input_type == "string")
  {
    QStringList hashed_elements {};
    for (QString element : elms)
    {
      hashed_elements.push_back(doHashANode(element, hash_algorithm));
    }
    elms = hashed_elements;
  }

  MNodesMapT verifies {};
  int leaves = elms.size();
  int level = 0;
  QString parent;
  QString lKey, rKey;
  QString lChild, rChild;
  while (level < 1000000) {
    level += 1;
    if (elms.size() == 1)
      return
      {
        elms[0], //root:
        verifies,
        leaves,
        level
      };

    if (elms.size() % 2 == 1)
    {
      CLog::log("FATAL ERROR ON MERKLE GENERATING: " + CUtils::dumpIt(elms), "sec", "fatal");
      CUtils::exiter("FATAL ERROR ON MERKLE GENERATING");
      // adding parity right fake hash
      QString the_hash = elms[elms.size() - 1];

      if (version > "0.0.0")
        the_hash = CConsts::FAKE_RIGHT_HASH_PREFIX + the_hash;

      elms.push_back(the_hash);
    }

    QVector<QStringList> chunks = CUtils::chunkStringList(elms, 2);
    elms = QStringList {};
    for (QStringList chunk : chunks) {
      parent = doHashANode(chunk[0] + chunk[1], hash_algorithm);
      elms.push_back(parent);
      if (level == 1)
      {
        QString lKey = "l_" + chunk[0];
        QString rKey = "r_" + chunk[1];
//        QStringList proofKeys = QStringList {};
        verifies.insert(lKey, MerkleNodeData {
            QStringList {},   // proofKeys
            parent,
            ""    //  lHash
          }
        );
        verifies.insert(rKey, MerkleNodeData {
            QStringList {},   // proofKeys
            parent,
            chunk[0]  //  lHash
          }
        );
        verifies[lKey].m_proof_keys.append("r." + chunk[1]);

      }
      else
      {
        // find alter parent cild
        for (QString key: verifies.keys())
        {
          MerkleNodeData value = verifies.value(key);
            if (chunk[0] == value.m_parent) {
              verifies[key].m_proof_keys.append("r." + chunk[1]);
              verifies[key].m_parent = parent;
            }
            if (chunk[1] == value.m_parent) {
              verifies[key].m_proof_keys.append("l." + chunk[0]);
              verifies[key].m_parent = parent;
            }
//            lChild = _.find(verifies, { parent: chunk[0] })

        }
      }
    }
  }

  return
  {
    "", //root:
    MNodesMapT{},
    0,
    0
  };

}



}



