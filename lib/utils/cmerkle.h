#ifndef CMERKLE_H
#define CMERKLE_H


const QString MERKLE_VERSION = "0.1.0";

class MerkleNodeData
{
public:
  QStringList m_proof_keys = {};
  QString m_parent = "";
  QString m_left_hash = "";
  QStringList m_merkle_proof = {};
//  uint16_t leaves;
};

namespace CMerkle
{
  std::tuple<QString, MNodesMapT, QString, int, int> generate(QStringList elms, QString inputType = "hashed", QString hashAlgorithm = "keccak256", QString version = MERKLE_VERSION);

  QString doHashANode(const QString& node_value, const QString& hash_algorithm = "keccak256");

  std::tuple<QString, MNodesMapT, int, int> innerMerkle(
    QStringList elms,
    QString inputType = "hashed",
    QString hashAlgorithm = "keccak256",
    QString version = MERKLE_VERSION);

  QString getRootByAProve(QString leave, QStringList proveHashes, QString lHash, QString inputType = "hashed", QString hashAlgorithm = "keccak256");

  QString dumpAProof(const MerkleNodeData& a_proof);
  QString dumpProofs(const MNodesMapT& proofs);

};

#endif // CMERKLE_H
