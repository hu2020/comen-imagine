#include "stable.h"

#include "gui/c_gui.h"
#include "lib/dag/dag.h"
#include "lib/network/email/email.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/messaging_protocol/dag_message_handler.h"
#include "lib/file_buffer_handler/file_buffer_handler.h"
#include "lib/block/block_types/block_coinbase/coinbase_issuer.h"

#include "threads_handler.h"

ThreadsHandler::ThreadsHandler()
{

}

// 1. fetch email and download to local hard drive
void launchEmailPoper()
{
  CLog::log("Email Poper stuff..." );
  std::this_thread::sleep_for(std::chrono::seconds(CConsts::GAP_EMAIL_POP));
  EmailHandler::loopEmailPoper();
}

// 2. read emails/cPackets from hard disk, pre-parse and push to parsing q
void launchHardCopyReading()
{
  CLog::log("launch Hard Copy Reading..." );
  FileBufferHandler::loopReadAndParseHardDiskInbox();
}

// 3. read from parsing queue and start to actual process the block/CPacket, and record the block in DAG
void launchSmartPullFromParsingQ()
{
  CLog::log("Smart Pull From Parsing Queue..." );
  if (CConsts::DATABASAE_AGENT != "sqlite")
    ParsingQHandler::loopSmartPullFromParsingQ();
}

// 4. read from sending q and create output emails on local hard disk
void launchPullSendingQ()
{
  CLog::log("Pull sending Q ..." );
  SendingQHandler::loopPullSendingQ();
}

// 5. read from local hard disk and send via email to neighbors
void launchEmailSender()
{
  CLog::log("Read sending emails ..." );
  EmailHandler::loopEmailSender();
}


void launchImportUTXOsFromCoinbaseBlocks()
{
  CLog::log("Launching import UTXOs From Coinbase Blocks..." );
  CoinbaseUTXOHandler::loopImportCoinbaseUTXOs();
}

void launchImportUTXOsFromNormalBlocks()
{
  CLog::log("Launching import UTXOs From Normal Blocks..." );
  if (CConsts::DATABASAE_AGENT != "sqlite")
    NormalUTXOHandler::loopImportNormalUTXOs();
}

void launchCoinCleaner()
{
  CLog::log("Launching coins cleaner..." );
  UTXOHandler::loopCoinCleaner(); // FIXME: uncomment this line, when problem of database lock for sqlite solved and we can have real multi thread solution
}

void launchCoinbaseIssuer()
{
  CLog::log("Launching coinbase issuer ..." );
  CoinbaseIssuer::loopMaybeIssueACoinbaseBlock();
}

void launchMissedBlocksInvoker()
{
  CLog::log("Invoke mised Blocks..." );
  DAGMessageHandler::loopMissedBlocksInvoker();
}

void launchPrerequisitiesRemover()
{
  CLog::log("Prereauisities cleaner..." );
  DAG::loopPrerequisitiesRemover();
}

void launchConcludeTreatment()
{
  CLog::log("launch Conclude Treatment..." );
  if (CConsts::DATABASAE_AGENT != "sqlite")
    PollingHandler::loopConcludeTreatment();
}

void launchINamesSettlement()
{
  CLog::log("launch iNames Settlement..." );
  if (CConsts::DATABASAE_AGENT != "sqlite")
    INameHandler::loopINamesSettlement();
}

void launchMonitorRefresher()
{
  CLog::log("launch monitor refresher..." );
  // for the sake of performance every 60 second the monitor will be refresh
  while (CMachine::isInSyncProcess() && CMachine::shouldLoopThreads())
  {
    CGUI::refreshMonitor();
    std::this_thread::sleep_for(std::chrono::seconds(90));
  }
}

void launchLazyLoadings()
{
  // lazy loadings
  CLog::log("launch lazy loadings..." );
  CMachine::setCanStartLazyLoadings(true);
  CGUI::launchLazyLoadings();
}

void launchThreadsBunch(bool only_lazy_loadings = false)
{
//  std::this_thread::sleep_for(std::chrono::seconds(5));


  if (only_lazy_loadings)
  {
    std::thread(launchLazyLoadings).detach();
    return;
  }

  FileBufferHandler::maybeBootDAGFromBundle();

  {
    // coin importing

    // import new minted coins
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchImportUTXOsFromCoinbaseBlocks).detach();

    // import UTXOs
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::thread(launchImportUTXOsFromNormalBlocks).detach();

    // remove unusefull visibility in order to lightening coins table
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchCoinCleaner).detach();

    if (!CMachine::isInSyncProcess()){
      std::thread(launchCoinbaseIssuer).detach();
    }else{
      CLog::log("Since machine is in sync mode, so will not launch coinbase isuer thread");
    }
  }


  {
    // missed blocks
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchMissedBlocksInvoker).detach();

    // prerequisities cleaner
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchPrerequisitiesRemover).detach();

  }


  {
    // output cpackets

    // fetching sending queue
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchPullSendingQ).detach();

    // read from hard disk and send by email
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchEmailSender).detach();

  }


  {
    // control if should conclude pollings/proposal
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchConcludeTreatment).detach();

    // maybe iNames settelment
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchINamesSettlement).detach();

  }


  {
    // ingress cpackets and parsing
    if (CConsts::EMAIL_IS_ACTIVE)
      std::thread(launchEmailPoper).detach();

    // read messages from hard drive
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchHardCopyReading).detach();

    // read messages from database queue
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread(launchSmartPullFromParsingQ).detach();

  }


  std::thread(launchMonitorRefresher).detach();

  // lazy loadings
  std::thread(launchLazyLoadings).detach();

}


void ThreadsHandler::launchThreads()
{
  CLog::log("launch threads bunch" );
  std::thread(launchThreadsBunch, false).detach();

  //CoinbaseIssuer::tryCreateCoinbaseBlock();
}

