#include <mutex>

#include "db_model.h"
class CMachine;
#include "lib/machine/machine_handler.h"


std::mutex mu_db;

OrderModifier::OrderModifier(const QString& field, const QString& order)
{
  m_field = field;
  m_order = order;
}

ModelClause::ModelClause(const QString& field_name, const QVariant& field_value, const QString& clause_operand)
{
  m_field_name = field_name;
  m_field_single_value = field_value;
  m_clause_operand = clause_operand;
}

ModelClause::ModelClause(const QString& field_name, const QVariant& field_value)
{
  m_field_name = field_name;
  m_field_single_value = field_value;
}

ModelClause::ModelClause(const QString& field_name, const QStringList& field_values, const QString& clause_operand)
{
  m_field_name = field_name;
  m_field_single_value = "";
  m_field_multi_values = field_values;
  m_clause_operand = clause_operand;
}


//  -  -  -  DbModel part
const QSDicT DbModel::s_map_table_to_db = {
  {"c_blocks", "db_comen_blocks"},
  {"c_docs_blocks_map", "db_comen_blocks"},
  {"c_block_extinfos", "db_comen_block_ext_info"},
  {"c_trx_utxos", "db_comen_spendable_coins"},
  {"c_trx_spend", "db_comen_spent_coins"},
  {"c_machine_wallet_addresses", "db_comen_wallets"},
  {"c_machine_wallet_funds", "db_comen_wallets"},
};

QVector<QString> DbModel::s_single_operands = {"=", "<", ">", "<=", "=<", ">=", "like", "LIKE", "ilike", "ILIKE", "not like", "NOT LIKE", "not ilike", "NOT ILIKE"};

DbModel::DbModel()
{
}

QueryRes DbModel::select(
  const QString& table,
  const QStringList& fields,
  const ClausesT& clauses,
  const OrderT& order,
  const int& limit,
  const bool& is_transactional,
  const bool& do_log)
{
  QString database_name = s_map_table_to_db.keys().contains(table) ? s_map_table_to_db[table] : "db_comen_general";

  PTRRes r = prepareToSelect(table, fields, clauses, order, limit);
  return DbModel::execQuery(database_name, r.complete_query, clauses, fields, {}, is_transactional, do_log);//, lockDb, log);
}

PTRRes DbModel::prepareToSelect(
  const QString& table,
  const QStringList& fields,
  const ClausesT& clauses,
  const OrderT& order,
  const int& limit)
{
  QueryElements query_elements = preQueryGenerator(clauses, order, limit);
  QString complete_query = "SELECT " + fields.join(", ") + " FROM " + table + query_elements.m_clauses + query_elements.m_order + query_elements.m_limit;
  complete_query = complete_query.trimmed();
  complete_query = CUtils::removeDblSpaces(complete_query);
//    return {QString::fromUtf8(s.c_str()), query_elements};
  return {complete_query, query_elements};
}

QueryRes DbModel::dDelete(
  const QString& table,
  const ClausesT& clauses,
  const bool& is_transactional,
  const bool& do_log)
{
  QString database_name = s_map_table_to_db.keys().contains(table) ? s_map_table_to_db[table] : "db_comen_general";

  PTRRes r = prepareToDelete(table, clauses);
  return DbModel::execQuery(database_name, r.complete_query, clauses, {}, {}, is_transactional, do_log);
}

PTRRes DbModel::prepareToDelete(
  const QString& table,
  const ClausesT& clauses)
{
  QueryElements query_elements = preQueryGenerator(clauses);
  QString complete_query = "DELETE FROM " + table + query_elements.m_clauses + query_elements.m_order + query_elements.m_limit;
  complete_query = complete_query.trimmed();
  complete_query = CUtils::removeDblSpaces(complete_query);
  return {complete_query, query_elements};
}

std::tuple<QString, QVDicT> DbModel::clausesQueryGenerator(
  const ClausesT& clauses)
{
  QVDicT values_ = {};
  QString clauses_ = "";
  std::vector<QVariant> valArr;

  if (clauses.size()>0)
  {
    QStringList tmp;

    for (ModelClause a_clause_tuple: clauses)
    {
       QString key = a_clause_tuple.m_field_name;
       if (DbModel::s_single_operands.contains(a_clause_tuple.m_clause_operand))
       {
         if (CConsts::DATABASAE_AGENT == "psql")
         {
           tmp << " (" + key + " " + a_clause_tuple.m_clause_operand + " ? ) ";

         }else if (CConsts::DATABASAE_AGENT == "sqlite")
         {
           tmp << " (" + key + " " + a_clause_tuple.m_clause_operand + " :" + key + ") ";

         }
         values_.insert(key, a_clause_tuple.m_field_single_value);

       }
       else if ((a_clause_tuple.m_clause_operand == "IN") || (a_clause_tuple.m_clause_operand == "NOT IN"))
       {
         if (a_clause_tuple.m_field_multi_values.size() == 0)
         {
           CLog::log("Maleformed clauses: " + CUtils::dumpIt(clauses), "sql", "fatal");
           CUtils::exiter("SQL needed values for 'IN/NOT IN' clauses!", 90);
         }

         QStringList placeholders = {};
         for (int i=0; i < a_clause_tuple.m_field_multi_values.size(); i++)
         {
           if (CConsts::DATABASAE_AGENT == "psql")
           {
             placeholders.push_back("?");

           }else if (CConsts::DATABASAE_AGENT == "sqlite")
           {
             placeholders.push_back(":" + key + QString("%1").arg(i));

           }
           values_.insert(key + QString("%1").arg(i), a_clause_tuple.m_field_multi_values[i]);
         }
         tmp << " (" + key + " " + a_clause_tuple.m_clause_operand + " (" + placeholders.join(", ") + ")) ";

       }
       else if (a_clause_tuple.m_clause_operand == "LIKE:OR")
       {
         CUtils::exiter("LIKE:OR NOT IMPLEMENTED YET!", 90);
       }

    }

    if ((tmp.size() > 0) && (values_.keys().size() > 0)) {
      clauses_ = tmp.join(" AND ");
    }
  }

  return {clauses_, values_};
}

QueryElements DbModel::preQueryGenerator(
  const ClausesT& clauses,
  const OrderT& order,
  const int& limit)
{
  auto[clauses_, values_] = clausesQueryGenerator(clauses);
  if (clauses_.length() > 0)
    clauses_ = " WHERE " + clauses_;

  QString order_ = "";
  if (order.size() > 0)
  {
    QStringList orders;
    for (OrderModifier anOrder :  order)
    {
      orders.append(anOrder.m_field + " " + anOrder.m_order);
    }
    order_ = " ORDER BY " + orders.join(", ");
  }

  QString limit_ = "";
  if (limit > 0)
  {
    limit_ = " LIMIT " + QString("%1").arg(limit) + " ";
  }

  return {
    clauses_,
    values_,
    order_,
    limit_
  };
}

QueryRes DbModel::customQuery(
  const QString& database_name,
  const QString& complete_query,
  const QStringList& fields,
  const int& field_count,
  const QVDicT& to_bind_values,
  const bool& is_transactional,
  const bool& do_log)
{
  if (CConsts::DATABASAE_AGENT == "psql")
    return customPSQLQuery(database_name, complete_query, fields, field_count, to_bind_values, is_transactional, do_log);

  Q_UNUSED(is_transactional);
  QSqlQuery query(DbHandler::getDB(database_name));
  query.setForwardOnly(true);
  if (do_log)
    CLog::log(complete_query, "sql", "trace");

  query.prepare(complete_query);

  for (QString aKey : to_bind_values.keys())
    query.bindValue(":"+aKey, to_bind_values[aKey]);

  QMap<QString, QVariant> sqlIterator(query.boundValues());
  QStringList bound_list_for_log;
  for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
  {
    bound_list_for_log.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
  }
  if (do_log)
    CLog::log("Query Values: [" + bound_list_for_log.join(", ") + "]", "sql", "trace");

  QVDRecordsT records;
  if (!query.exec())
  {
    QString err_text = query.lastError().databaseText();
    if (uniqueConstraintFailed(err_text))
      return {true, records};

    CLog::log(err_text, "sql", "fatal");
    CLog::log("SQL failed query1: " + complete_query, "sql", "fatal");
    CLog::log("SQL failed values1: " + CUtils::dumpIt(to_bind_values), "sql", "fatal");
    QString executed_query = query.executedQuery();
    CLog::log("SQL failed executed_query: " + executed_query, "sql", "fatal");
    CLog::log("SQL failed clauses2: " + err_text, "sql", "fatal");

    bool res = handleMaybeDbLock(query);
    if (!res)
      return {false, records};
  }

  while (query.next())
  {
    QVDicT a_row;
    if (fields.size() > 0)
    {
      for ( QString a_filed: fields)
      {
        a_row[a_filed] = query.value(a_filed);
      }

    }else{
      // return results by position
      for(int i=0; i < field_count; i++)
        a_row[QString::number(i)] = query.value(i);

    }
    records.push_back(a_row);

    QMap<QString, QVariant> sqlIterator(query.boundValues());
    QStringList bound_list_for_log;
    for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
    {
      bound_list_for_log.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
    }
  }

  if (query.isSelect())
  {
    if (do_log)
      CLog::log("Results: [" + QString("%1").arg(records.size()) + " rows] returned", "sql", "trace");
  }
  else if (query.numRowsAffected() > 0)
  {
    CLog::log("Affected: [" + QString("%1").arg(query.numRowsAffected()) + " rows] afected", "sql", "trace");
  }


  query.finish();

  return {true, records};
}

QueryRes DbModel::customPSQLQuery(
  const QString& database_name,
  const QString& complete_query,
  const QStringList& fields,
  const int& field_count,
  const QVDicT& to_bind_values,
  const bool& is_transactional,
  const bool& do_log)
{
  Q_UNUSED(is_transactional);
  QSqlQuery* query = new QSqlQuery(*DbHandler::getPSQLDB(database_name));
  query->setForwardOnly(true);

  if (do_log)
    CLog::log(complete_query, "sql", "trace");

  QStringList bound_list_for_log;
  bool exec_res;
  if (to_bind_values.size() > 0)
  {
    query->prepare(complete_query);
    for (QString aKey : to_bind_values.keys())
      query->addBindValue(to_bind_values[aKey]);

    QMap<QString, QVariant> sqlIterator(query->boundValues());
    for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
    {
      bound_list_for_log.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
    }

    exec_res = query->exec();

  } else {
    exec_res = query->exec(complete_query);

  }


  if (do_log)
    CLog::log("Query Values: [" + bound_list_for_log.join(", ") + "]", "sql", "trace");

  QVDRecordsT records;
  if (!exec_res)
  {
    QString err_text = query->lastError().databaseText();
    if (uniqueConstraintFailed(err_text))
      return {true, records};

    CLog::log(err_text, "sql", "fatal");
    CLog::log("PSQL failed query1: " + complete_query, "sql", "fatal");
    CLog::log("PSQL failed values1: " + CUtils::dumpIt(to_bind_values), "sql", "fatal");
    QString executed_query = query->executedQuery();
    CLog::log("PSQL failed executed_query: " + executed_query, "sql", "fatal");
    CLog::log("PSQL failed clauses2: " + err_text, "sql", "fatal");
    return {false, records};
  }

  while (query->next())
  {
    QVDicT a_row;
    if (fields.size() > 0)
    {
      for ( QString a_filed: fields)
      {
        a_row[a_filed] = query->value(a_filed);
      }

    }else{
      // return results by position
      for(int i=0; i < field_count; i++)
        a_row[QString::number(i)] = query->value(i);

    }
    records.push_back(a_row);

    QMap<QString, QVariant> sqlIterator(query->boundValues());
//    QStringList bound_list_for_log;
//    for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
//      bound_list_for_log.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());

  }

  if (query->isSelect())
  {
    if (do_log)
      CLog::log("Results: [" + QString("%1").arg(records.size()) + " rows] returned", "sql", "trace");
  }
  else if (query->numRowsAffected() > 0)
  {
    CLog::log("Affected: [" + QString("%1").arg(query->numRowsAffected()) + " rows] afected", "sql", "trace");
  }

  query->finish();
  delete query;

  return {true, records};
}

QueryRes DbModel::execQuery(
  QString database_name,
  const QString& complete_query,
  const ClausesT& clauses,
  const QStringList& fields,
  const QVDicT& upd_values,
  const bool& is_transactional,
  const bool& do_log)
{
  if (CConsts::DATABASAE_AGENT == "psql")
    return execQuery_PSQL(database_name, complete_query, clauses, fields, upd_values, is_transactional, do_log);

  if (database_name == "")
    database_name = "db_comen_general";

  Q_UNUSED(is_transactional);
  QSqlQuery query(DbHandler::getDB(database_name));
  query.setForwardOnly(true);
  if (do_log)
    CLog::log(complete_query, "sql", "trace");

  query.prepare(complete_query);


  for (QString aKey : upd_values.keys())
  {
//    logValues.append(upd_values[aKey].toString());
    query.bindValue(":"+aKey, upd_values[aKey]);
  }


  for (ModelClause a_clause_tuple: clauses)
  {
    QString field_name = a_clause_tuple.m_field_name;
    QString field_value;
    if (DbModel::s_single_operands.contains(a_clause_tuple.m_clause_operand))
    {
      field_value = a_clause_tuple.m_field_single_value.toString();
      query.bindValue(":"+field_name, field_value);
    }
    else if ((a_clause_tuple.m_clause_operand == "IN") || (a_clause_tuple.m_clause_operand == "NOT IN"))
    {
      QStringList placeholders = {};
      for (int i=0; i < a_clause_tuple.m_field_multi_values.size(); i++)
      {
        field_value = a_clause_tuple.m_field_multi_values[i];
        query.bindValue(":"+field_name + QString("%1").arg(i), field_value);
      }

    }
    else if (a_clause_tuple.m_clause_operand == "LIKE:OR")
    {

    }

  }

  QMap<QString, QVariant> sqlIterator(query.boundValues());
  QStringList bound_list_for_log;
  for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
  {
    bound_list_for_log.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
  }
  if (do_log)
    CLog::log("Query Values: [" + bound_list_for_log.join(", ") + "]", "sql", "trace");

  QVDRecordsT records = {};
  if (!query.exec())
  {
    QString err_text = query.lastError().databaseText();
    if (uniqueConstraintFailed(err_text))
      return {true, records};

    QString thread_code = QString::number((quint64)QThread::currentThread(), 16);
    CLog::log("SQL failed Thread: " + CMachine::mapThreadCodeToPrefix(thread_code) + " "+ thread_code, "sql", "fatal");
    CLog::log("SQL failed query52: " + complete_query, "sql", "fatal");
    CLog::log("SQL failed clauses512: " + CUtils::dumpIt(clauses), "sql", "fatal");
    QString executed_query = query.executedQuery();
    CLog::log("SQL failed executed_query: " + executed_query, "sql", "fatal");
    CLog::log("SQL failed clauses522: " + err_text, "sql", "fatal");
    bool res = handleMaybeDbLock(query);
    if (!res)
      return {false, records};
  }

  while (query.next())
  {

    QVDicT a_row;
    for ( QString a_filed: fields)
    {
      a_row[a_filed] = query.value(a_filed);
    }
    records.push_back(a_row);
//        QString kv_value = query.value(1).toString();
//        QString kv_value2 = query.value("kv_value").toString();
  }

  //    std::cout << std::endl << "kv_value2: " << kv_value.toStdString();



  if (query.isSelect())
  {
    if (do_log)
      CLog::log("Results: [" + QString("%1").arg(records.size()) + " rows] returned", "sql", "trace");
  }
  else if (query.numRowsAffected() >0 )
  {
    if (do_log)
      CLog::log("Affected: [" + QString("%1").arg(query.numRowsAffected()) + " rows] afected", "sql", "trace");
  }

  query.finish();

  return {true, records};
}

QueryRes DbModel::execQuery_PSQL(
  QString database_name,
  const QString& complete_query,
  const ClausesT& clauses,
  const QStringList& fields,
  const QVDicT& upd_values,
  const bool& is_transactional,
  const bool& do_log)
{
  if (database_name == "")
    database_name = "db_comen_general";

  Q_UNUSED(is_transactional);

  QSqlQuery* query = new QSqlQuery(*DbHandler::getPSQLDB(database_name));
  query->setForwardOnly(true);
  if (do_log)
    CLog::log(complete_query, "sql", "trace");

  query->prepare(complete_query);

  for (QString aKey : upd_values.keys())
  {
//    logValues.append(upd_values[aKey].toString());
    query->addBindValue(upd_values[aKey]);
  }


  for (ModelClause a_clause_tuple: clauses)
  {
    QString field_name = a_clause_tuple.m_field_name;
    QString field_value;
    if (DbModel::s_single_operands.contains(a_clause_tuple.m_clause_operand))
    {
      field_value = a_clause_tuple.m_field_single_value.toString();
      query->addBindValue(field_value);
    }
    else if ((a_clause_tuple.m_clause_operand == "IN") || (a_clause_tuple.m_clause_operand == "NOT IN"))
    {
      QStringList placeholders = {};
      for (int i=0; i < a_clause_tuple.m_field_multi_values.size(); i++)
      {
        field_value = a_clause_tuple.m_field_multi_values[i];
        query->addBindValue(field_value);
      }

    }
    else if (a_clause_tuple.m_clause_operand == "LIKE:OR")
    {
      CLog::log("LIKE: is not implemented!", "sql", "fatal");
    }

  }

  QMap<QString, QVariant> sqlIterator(query->boundValues());
  QStringList bound_list_for_log;
  for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
  {
    bound_list_for_log.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
  }
  if (do_log)
    CLog::log("Query Values: [" + bound_list_for_log.join(", ") + "]", "sql", "trace");

  QVDRecordsT records = {};
  if (!query->exec())
  {
    QString err_text = query->lastError().text();
    QString thread_code = QString::number((quint64)QThread::currentThread(), 16);
    CLog::log("PSQL failed Thread: " + CMachine::mapThreadCodeToPrefix(thread_code) + " "+ thread_code, "sql", "fatal");
    CLog::log("PSQL failed query52: " + complete_query, "sql", "fatal");
    CLog::log("PSQL failed clauses532: " + CUtils::dumpIt(clauses), "sql", "fatal");
    CLog::log("PSQL failed clauses542: " + err_text, "sql", "fatal");
    return {false, records};
  }

  while (query->next())
  {

    QVDicT a_row;
    for ( QString a_filed: fields)
    {
      a_row[a_filed] = query->value(a_filed);
    }
    records.push_back(a_row);
//        QString kv_value = query.value(1).toString();
//        QString kv_value2 = query.value("kv_value").toString();
  }

  //    std::cout << std::endl << "kv_value2: " << kv_value.toStdString();



  if (query->isSelect())
  {
    if (do_log)
      CLog::log("Results: [" + QString("%1").arg(records.size()) + " rows] returned", "sql", "trace");
  }
  else if (query->numRowsAffected() >0 )
  {
    if (do_log)
      CLog::log("Affected: [" + QString("%1").arg(query->numRowsAffected()) + " rows] afected", "sql", "trace");
  }

  query->finish();
  delete query;

  return {true, records};
}

bool DbModel::handleMaybeDbLock(QSqlQuery& query)
{
  QString dummeyLockErrMsg = "database is locked";
  if (query.lastError().databaseText() != dummeyLockErrMsg)
    return false;

  while(true)
  {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    query.finish();
    if (query.exec())
    {
      return true;

    }else{
      CLog::log("handle Maybe Db Lock failed Thread: " + QString::number((quint64)QThread::currentThread(), 16), "sql", "fatal");
      CLog::log("handle Maybe Db Lock" + query.lastError().databaseText(), "sql", "fatal");
      QString executed_query = query.executedQuery();
      CLog::log("SQL failed executed_query: " + executed_query, "sql", "fatal");
      QString err_text = query.lastError().databaseText();
      CLog::log("SQL failed clauses2: " + err_text, "sql", "fatal");
      if (err_text != dummeyLockErrMsg)
        return false;
    }
  }

  CLog::log("even handle Maybe DbLock didn't work!", "app", "fatal");
  return false;
}

const QString UNIQUE_ERR_MSG_PREFIX1 = "UNIQUE constraint failed";
const QString UNIQUE_ERR_MSG_PREFIX2 = "ERROR:  duplicate key value violates unique constraint";

bool DbModel::uniqueConstraintFailed(const QString& msg)
{
  if (msg.midRef(0, UNIQUE_ERR_MSG_PREFIX1.length()).toString() == UNIQUE_ERR_MSG_PREFIX1)
    return true;
  if (msg.midRef(0, UNIQUE_ERR_MSG_PREFIX2.length()).toString() == UNIQUE_ERR_MSG_PREFIX2)
    return true;
  return false;
}


bool DbModel::insert(
  const QString& table,
  const QVDicT& values,
  const bool& is_transactional,
  const bool& do_log)
{
  if (CConsts::DATABASAE_AGENT == "psql")
    return insertToPSQL(table, values, is_transactional, do_log);

  Q_UNUSED(is_transactional);
  QList keys = values.keys();
  QString keys_str = keys.join(", ");
  QString keys_handlers;
  QString complete_query;

  keys_handlers = keys.join(", :");
  complete_query = "INSERT INTO " + table + " (" + keys_str + ") VALUES (:" + keys_handlers + "); "; //BEGIN; COMMIT;

  if (do_log)
    CLog::log(complete_query, "sql", "info");

  QString database_name = s_map_table_to_db.keys().contains(table) ? s_map_table_to_db[table] : "db_comen_general";
  QSqlQuery query(DbHandler::getDB(database_name));
  query.prepare(complete_query);
  QStringList logValues;
  for (QString aKey : keys)
  {
    query.bindValue(":"+aKey, values[aKey]);
//        logValues.push_back(values[aKey].toString());
  }

  if (!query.exec())
  {
    QString err_text = query.lastError().databaseText();
    if (uniqueConstraintFailed(err_text))
      return true;

    CLog::log("SQL failed clauses2: " + err_text, "sql", "fatal");
    CLog::log("SQL failed query3: " + complete_query, "sql", "fatal");
    CLog::log("SQL failed value3: " + CUtils::dumpIt(values), "sql", "fatal");
    QString executed_query = query.executedQuery();
    CLog::log("SQL failed executed_query: " + executed_query, "sql", "fatal");

    bool res = handleMaybeDbLock(query);
    if (!res)
      return false;
  }

  if (do_log)
  {
    QMap<QString, QVariant> sqlIterator(query.boundValues());
    QStringList bound_list_for_log;
    for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
      bound_list_for_log.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
    CLog::log("Query Values: [" + bound_list_for_log.join(", ") + "]", "sql", "trace");
  }

  query.finish();
  return true;
}

bool DbModel::insertToPSQL(
  const QString& table,
  const QVDicT& values,
  const bool& is_transactional,
  const bool& do_log)
{
  Q_UNUSED(is_transactional);
  QList keys = values.keys();
  QString keys_str = keys.join(", ");
  QString keys_handlers;
  QString complete_query;

  QStringList plh {};
  for (QString key: keys)
    plh.append("?");
  keys_handlers = plh.join(", ");
  complete_query = "INSERT INTO " + table + " (" + keys_str + ") VALUES (" + keys_handlers + "); "; //BEGIN; COMMIT;

  if (do_log)
    CLog::log(complete_query, "sql", "info");

  QString database_name = s_map_table_to_db.keys().contains(table) ? s_map_table_to_db[table] : "db_comen_general";
  QSqlQuery* query = new QSqlQuery(*DbHandler::getPSQLDB(database_name));
  query->setForwardOnly(true);

  query->prepare(complete_query);
  QStringList logValues;
  int position = 0;
  for (QString aKey : keys)
  {
    query->bindValue(position, values[aKey]);
    position++;
  }

  if (!query->exec())
  {
    QString err_text = query->lastError().text();
    if (uniqueConstraintFailed(err_text))
      return true;
    CLog::log("PSQL failed clauses 3 err_text: " + err_text, "sql", "fatal");
    CLog::log("PSQL failed query3: " + complete_query, "sql", "fatal");
    CLog::log("PSQL failed value3: " + CUtils::dumpIt(values), "sql", "fatal");
    return false;
  }

  if (do_log)
  {
    QMap<QString, QVariant> sqlIterator(query->boundValues());
    QStringList bound_list_for_log;
    for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
      bound_list_for_log.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
    CLog::log("Query Values: [" + bound_list_for_log.join(", ") + "]", "sql", "trace");
  }

  query->finish();
  delete query;
  return true;
}

bool DbModel::upsert(
  const QString& table,
  const QString& controled_field,
  const QString& controled_value,
  const QVDicT& values,
  const bool& is_transactional,
  const bool& do_log)
{

  const ClausesT clauses =
  {
    ModelClause(controled_field, controled_value)
  };

  // controll if the record already existed
  QueryRes existed = DbModel::select(
    table,
    QStringList {controled_field},     // fields
    clauses, //clauses
    {},   // order
    1,   // limit
    is_transactional,
    do_log
  );
  if (existed.records.size() > 0) {
    // update existed record
    DbModel::update(
      table,
      values, // update values
      clauses,  // update clauses
      is_transactional);

    return true;
  }

  // insert new entry
  QVDicT insert_values = QHash(values);
  insert_values[controled_field] = controled_value;
  return DbModel::insert(
    table,     // table
    insert_values, // values to insert
    true
  );

}

std::tuple<QString, QStringList> DbModel::prepareToUpdate(
  const QString& table,
  const QVDicT& upd_values,
  const ClausesT& clauses)
{
  QueryElements query_elements = preQueryGenerator(clauses);
  QStringList field_clauses = {};
  QStringList fields = upd_values.keys();
  for (QString aKey : fields)
  {
    if (CConsts::DATABASAE_AGENT == "psql")
    {
      field_clauses.append(aKey+"= ?");

    }else if (CConsts::DATABASAE_AGENT == "sqlite")
    {
      field_clauses.append(aKey+"=:" + aKey);
    }
  }
  QString set_fields = field_clauses.join(", ");
  QString complete_query = "UPDATE " + table + " SET " + set_fields + query_elements.m_clauses;
  complete_query = complete_query.trimmed();
  complete_query = CUtils::removeDblSpaces(complete_query);
//  complete_query = QString::fromUtf8(s.c_str());

  return {complete_query, fields};
}

bool DbModel::update(
  const QString& table,
  const QVDicT& update_values,
  const ClausesT& update_clauses,
  const bool& is_transactional,
  const bool& do_log)
{
  Q_UNUSED(is_transactional);

  QString database_name = s_map_table_to_db.keys().contains(table) ? s_map_table_to_db[table] : "db_comen_general";

  auto[complete_query, fields] = prepareToUpdate(table, update_values, update_clauses);
  Q_UNUSED(fields);
  QueryRes updRes = DbModel::execQuery(
    database_name,
    complete_query,
    update_clauses,
    {},
    update_values,
    is_transactional,
    do_log);

  return true;
}


bool DbModel::clauseContains(const ClausesT& clauses, const QString& filed_name)   //legacy queryHas
{
  for (ModelClause a_clause : clauses)
  {
    if (a_clause.m_field_name == filed_name)
      return true;
   }
   return false;
}









