#ifndef DBHANDLER_H
#define DBHANDLER_H

#include <QtSql>
#include <QSqlDriver>
#include <QCoreApplication>
#include <QHash>
#include <QDir>
#include <QString>
#include <QDebug>

#include "constants.h"
#include "db_model.h"
#include "lib/clog.h"
#include "init_sqlite.h"
#include "init_psql.h"

#include "lib/services/dna/dna_handler.h"

class DbHandler
{

public:
  DbHandler(const DbHandler&) = delete;
  static DbHandler &get(){return s_instance;};
  ~DbHandler();

  const static QStringList s_databases;
  QHash<QString, QSqlDatabase> m_sqlite_thread_connections;
  QHash<QString, QSqlDatabase*> m_psql_thread_connections;

  static std::tuple<bool, QString> initDb(){ return get().IinitDb(); };
  static bool closeConnections(){ return get().IcloseConnections(); };
  static std::tuple<bool, QString, QSqlDatabase> connectToSQLITE(const QString& database_name = ""){ return get().IconnectToSQLITE(database_name); }
  static std::tuple<bool, QSqlDatabase*> connectToPSQL(QString database_name = ""){ return get().IconnectToPSQL(database_name); }

//  static QSqlQuery getQ(){ return get().IgetQ(); };
  static QSqlDatabase getDB(const QString& database_name){ return get().IgetDB(database_name); };
  bool createTablesSQLITE();
  bool createTablesPSQL(QSqlDatabase* db);
  int initValues();
  static QSqlDatabase* getPSQLDB(const QString& database_name){ return get().IgetPSQLDB(database_name); };

private:
  DbHandler(){};
  static DbHandler s_instance;

  std::tuple<bool, QString, QSqlDatabase> IconnectToSQLITE(QString database_name = "");
  std::tuple<bool, QSqlDatabase*> IconnectToPSQL(QString database_name = "");
  std::tuple<bool, QString> IinitDb();
  bool IcloseConnections();

//  QSqlQuery IgetQ(){ return m_q; };
  QSqlDatabase IgetDB(const QString& database_name);
  QSqlDatabase* IgetPSQLDB(const QString& database_name);
};

#endif // DBHANDLER_H
