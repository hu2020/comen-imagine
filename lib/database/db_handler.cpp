#include "db_handler.h"


DbHandler DbHandler::s_instance;

const QStringList DbHandler::s_databases = {
  "db_comen_blocks",
  "db_comen_block_ext_info",
  "db_comen_spendable_coins",
  "db_comen_spent_coins",
  "db_comen_general",
  "db_comen_wallets"};

DbHandler::~DbHandler()
{
  closeConnections();
}

bool DbHandler::IcloseConnections()
{
  if ( CConsts::DATABASAE_AGENT == "sqlite")
  {
    for (QString connection_key: m_sqlite_thread_connections.keys())
    {
      m_sqlite_thread_connections[connection_key].close();
    }

  } else if ( CConsts::DATABASAE_AGENT == "psql")
  {
    for (QString connection_key: m_sqlite_thread_connections.keys())
    {
      m_psql_thread_connections[connection_key]->close();
      delete m_psql_thread_connections[connection_key];
    }
  }
  return true;
}

std::tuple<bool, QString, QSqlDatabase> DbHandler::IconnectToSQLITE(QString database_name)
{
  /**
   * @brief divided databases, in order to address scaleability
   * comen_blocks:
   * comen_block_ext_info:
   * comen_spendable_coins:
   * comen_spent_coins:
   * comen_general: // it will be devided to smaller databases (e.g wiki database, Demos database, version control database, inames database, signals database, logs databases, etc...
   * comen_wallets:
   *
   */
  auto thread_name = "CDB_" + QString::number((quint64)QThread::currentThread(), 16);
//  CLog::log("thread_name to check(" + thread_name + ")", "sql", "trace");

  if (database_name == "")
    database_name = "db_comen_general";

  QString db_connection_full_name = database_name + "_" + thread_name;
  if (m_sqlite_thread_connections.keys().contains(db_connection_full_name))
    return {true, db_connection_full_name + " Already connected to Database", m_sqlite_thread_connections[db_connection_full_name]};

  CLog::log("db_connection_full_name to create(" + db_connection_full_name + ")", "sql", "trace");

  QSqlDatabase the_database;

  const QString SQLITE_DRIVER {"QSQLITE"};
  if (!QSqlDatabase::isDriverAvailable(SQLITE_DRIVER))
    CUtils::exiter("SQLITE_DRIVER Failed to open database", 23);

  the_database = QSqlDatabase::addDatabase(SQLITE_DRIVER, db_connection_full_name);
  QSqlQuery thread_q = QSqlQuery(the_database);
  the_database.setDatabaseName(QCoreApplication::applicationDirPath() + QDir::separator() + database_name + ".dat");
  if (!the_database.open())
  {
    qDebug() << "Failed to open database" << the_database.lastError();
    CUtils::exiter("SQLITE_DRIVER Failed to open database", 23);
  }

  if (!CMachine::databasesAreCreated())
  {
    bool res = thread_q.exec("SELECT name FROM sqlite_master WHERE type='table' AND name='c_blocks'");
    if (!res || !thread_q.last())
      CMachine::setDatabasesAreCreated(createTablesSQLITE());
  }

  m_sqlite_thread_connections[db_connection_full_name] = the_database;

  return
  {
    true,
    "",
    m_sqlite_thread_connections[db_connection_full_name]
  };

}

std::tuple<bool, QSqlDatabase*> DbHandler::IconnectToPSQL(QString database_name)
{
  /**
   * @brief db_connection_full_name
   * for psql daabase we also put all tables in one database
   * TODO: divid tables to different databases in order to provid horizontal scaling and load balancing
   */

  database_name = CConsts::PSQL_DB::DB_NAME;
  auto clone_id = CMachine::getAppCloneId();
  if (clone_id > 0)
    database_name += QString::number(clone_id);

  QString thread_code = QString::number((quint64)QThread::currentThread(), 16);
  QString thread_name = "CDB_" + thread_code;
  QString db_connection_full_name = database_name + "_" + CMachine::mapThreadCodeToPrefix(thread_code) + "_"  + thread_name;
  if (m_psql_thread_connections.keys().contains(db_connection_full_name))
    return {true, m_psql_thread_connections[db_connection_full_name]};

  CLog::log("db_connection_full_name to create(" + db_connection_full_name + ")", "sql", "trace");

  const QString PSQL_DRIVER {"QPSQL"};
  if (!QSqlDatabase::isDriverAvailable(PSQL_DRIVER))
    CUtils::exiter("PSQL_DRIVER Failed to open database", 23);

  QSqlDatabase* db = new QSqlDatabase( QSqlDatabase::addDatabase(PSQL_DRIVER, db_connection_full_name) );
  db->setConnectOptions();
  db->setHostName(CConsts::PSQL_DB::DB_HOST);
  db->setDatabaseName(database_name);
  db->setUserName(CConsts::PSQL_DB::DB_USER);
  db->setPassword(CConsts::PSQL_DB::DB_PASS);

  if (!db->open())
  {
    qDebug() << "Failed to open psql database" << db->lastError();
    CUtils::exiter("PSQL_DRIVER Failed to open database", 23);
  }

  if (!CMachine::databasesAreCreated())
  {
    QSqlQuery* query = new QSqlQuery(*db);
    query->setForwardOnly(true);
    if( !query->prepare(QString("SELECT b_hash FROM c_blocks limit 1")) )
    {
      delete query;
      CMachine::setDatabasesAreCreated(createTablesPSQL(db));
    }
  }

  m_psql_thread_connections[db_connection_full_name] = db;

  return {true, m_psql_thread_connections[db_connection_full_name]};
}

bool DbHandler::createTablesPSQL(QSqlDatabase* db)
{
  QSqlQuery* query = new QSqlQuery(*db);
  for (std::string aQuery : psql_init_query)
  {
    QString qstr = QString::fromStdString(aQuery);
    CLog::log("Creating tables: " + qstr, "sql", "info");
//    qstr = CUtils::removeNewLine(qstr);
//    CLog::log("Creating tables: " + qstr, "sql", "info");

//    query->prepare(qstr);
    bool queryRes = query->exec(qstr);
    if (query->lastError().type() != QSqlError::NoError || !queryRes)
    {
      qDebug() << query->lastError().text();
      return false;
    }
  }

  // create developers tables
  for (std::string aQuery : psql_init_query_dev)
  {
    QString qstr = QString::fromStdString(aQuery);
    query->prepare(qstr);
    bool queryRes = query->exec();
    if (query->lastError().type() != QSqlError::NoError || !queryRes)
    {
      qDebug() << query->lastError().text();
      return false;
    }
  }

  delete query;
  return true;
}

bool DbHandler::createTablesSQLITE()
{
  const QString SQLITE_DRIVER {"QSQLITE"};
  for (QString a_db: s_databases)
  {
    CLog::log("Creating tables for database(" + a_db + ")", "sql");
    QSqlDatabase tmp_database = QSqlDatabase::addDatabase(SQLITE_DRIVER, a_db);
    QSqlQuery tmp_q = QSqlQuery(tmp_database);
    tmp_database.setDatabaseName(QCoreApplication::applicationDirPath() + QDir::separator() + a_db + ".dat");
    if (!tmp_database.open())
      CUtils::exiter("failed to open database(" + a_db + ")", 789);


    // FIXME: create only needed tables
    // create essential tables
    for (std::string aQuery : sqlite_init_query)
    {
      QString qstr = QString::fromStdString(aQuery);
      bool res = tmp_q.exec(qstr);
      if (!res){
          std::cout << std::endl << "Error! " << aQuery;
      }
      tmp_q.finish();
    }

    // create developers tables
    for (std::string aQuery : sqlite_init_query_dev)
    {
      QString qstr = QString::fromStdString(aQuery);
      bool res = tmp_q.exec(qstr);
      if (!res){
          std::cout << std::endl << "Error! " << aQuery;
      }
      tmp_q.finish();
    }
  }
  return true;
}

std::tuple<bool, QString> DbHandler::IinitDb()
{
  if (CConsts::DATABASAE_AGENT == "sqlite")
  {
    bool final_status = true;
    QString final_msg = "";
    for(QString a_database: s_databases)
    {
      auto[status, msg, db] = connectToSQLITE(a_database);
      Q_UNUSED(db);
      final_status &= status;
      final_msg += ", " + msg;
    }
    return {final_status, final_msg};

  }
  else if (CConsts::DATABASAE_AGENT == "psql")
  {
    auto[status, db] = connectToPSQL();
    return {status, "connected"};
  }
  return {false, "Unknown db agent!"};
}

QSqlDatabase DbHandler::IgetDB(const QString& database_name)
{
  auto[status, msg, db] = connectToSQLITE(database_name);
  Q_UNUSED(msg);
  if (status)
    return db;

  auto thread_name = "CDB_" + QString::number((quint64)QThread::currentThread(), 16);
  CUtils::exiter("Invalid thread database request sqlite" + thread_name, 14);
  return QSqlDatabase {};
};

QSqlDatabase* DbHandler::IgetPSQLDB(const QString& database_name)
{
  auto[status, db] = connectToPSQL(database_name);
  if (status)
    return db;

  auto thread_name = "CDB_" + QString::number((quint64)QThread::currentThread(), 16);
  CUtils::exiter("Invalid thread database request psql " + thread_name, 14);
  return db;
};

int DbHandler::initValues()
{
    return 1;
}










