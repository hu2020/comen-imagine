#include "stable.h"
#include "lib/ccrypto.h"
#include "address_handler.h"


std::tuple<bool, UnlockDocument> CAddress::createANewBasicAddress(
  const QString& signature_mod,
  const QString& signature_version)
{

  QString signature_type = CConsts::SIGNATURE_TYPES::Basic; // by default is two of three (m of n === m/n)
  CLog::log("creating a new signature_type:signature_mod " + signature_type + ":" + signature_mod, "app", "info");


  QVDicT map_pub_key_to_priv_key;
  uint16_t m_signatures_count = signature_mod.split("/")[0].toUInt();
  uint16_t n_signatures_count = signature_mod.split("/")[1].toUInt();
  QHash<QString, IndividualSignature> individuals_signing_sets;

  for (uint i = 0; i < n_signatures_count; i++)
  {
    auto[status, private_key, public_key] = CCrypto::ECDSAGenerateKeyPair();
    if (!status)
    {
      CLog::log("Couldn't create Strict ECDSA key pair", "app", "fatal");
      return { false, UnlockDocument()};
    }

    map_pub_key_to_priv_key[public_key] = private_key;

    IndividualSignature a_sign_set(public_key, CConsts::NO, CConsts::NO);
    a_sign_set.m_signer_id = CUtils::paddingLengthValue(QString::number(i));
    individuals_signing_sets[a_sign_set.m_signer_id] = a_sign_set;
  }

  UnlockDocument unlock_info = SignatureStructureHandler::createCompleteUnlockSets(
    individuals_signing_sets,
    m_signatures_count,
    QVDicT
    {
      {"signature_type", signature_type},
      {"signature_version", signature_version},
      {"customSalt", "PURE_LEAVE"}
    });

  for (UnlockSet an_unlocker_set: unlock_info.m_unlock_sets)
  {
    QStringList private_keys;
    for (IndividualSignature aSignSet: an_unlocker_set.m_signature_sets)
        private_keys.push_back(map_pub_key_to_priv_key[aSignSet.m_signature_key].toString());
    unlock_info.m_private_keys.insert(an_unlocker_set.m_salt, private_keys);

    // test unlock structure&  signature
    bool is_valid = SignatureStructureHandler::validateSigStruct(
      an_unlocker_set,
      unlock_info.m_account_address
    );
    if (is_valid) {
      CLog::log("The new address " + CUtils::shortBech16(unlock_info.m_account_address) + " created & tested successfully", "app", "info");
    } else {
      CLog::log("Curropted strict address created!?");
      CUtils::exiter(CUtils::dumpIt(unlock_info), 623);
    }
  }

  // console.log(\n unlock_info: ${utils.stringify(unlock_info)}\n);

  //TODO: FIXME: implement key signature potential ASAP
  // validate signature of new address
  QString message = CCrypto::convertTitleToHash("Imagine all the people living life in peace").midRef(0, CConsts::SIGN_MSG_LENGTH).toString();
  for (UnlockSet an_unlock_set: unlock_info.m_unlock_sets)
  {
    for (int inx = 0; inx < an_unlock_set.m_signature_sets.size(); inx++)
    {
      auto[status, signature_hex, signature] = CCrypto::ECDSAsignMessage(unlock_info.m_private_keys[an_unlock_set.m_salt][inx], message);
      if (!status)
      {
        CLog::log("Curropted strict address created signature status!?", "app", "fatal");
        CUtils::exiter(CUtils::dumpIt(unlock_info), 923);
      }
      Q_UNUSED(signature);
      bool verifyRes = CCrypto::ECDSAVerifysignature(an_unlock_set.m_signature_sets[inx].m_signature_key, message, QString::fromStdString(signature_hex));
      if (!verifyRes)
      {
        CLog::log("Curropted strict address created signature!?", "app", "fatal");
        CUtils::exiter(CUtils::dumpIt(unlock_info), 923);
      }
    }
  }

  return {true, unlock_info};

}
