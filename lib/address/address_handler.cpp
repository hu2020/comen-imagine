#include "stable.h"

#include "address_handler.h"

#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"


#include "basic_addresses.cpp"
#include "bitcoin_like_addresses.cpp"
#include "complex_addresses.cpp"
#include "indented_addresses.cpp"
#include "strict_addresses.cpp"


std::tuple<bool, UnlockDocument> CAddress::createANewAddress(
  const QString& signature_type,
  const QString& signature_mod, // m of n
  const QString& signature_version)
{
  if (signature_type == CConsts::SIGNATURE_TYPES::Basic)
  {
    if (signature_mod == "Complex")
    {
//      return complexAddressHandler.createANewComplexModAddress(args);
    }
    else
    {
      return createANewBasicAddress(signature_mod, signature_version);
    }

  }
  else if (signature_type == CConsts::SIGNATURE_TYPES::Strict)
  {
    return createANewStrictAddress(signature_mod, signature_version);
  }
  else if (signature_type == CConsts::SIGNATURE_TYPES::Bitcoin)
  {
//    return bitcoinAddressHandler.createANewBitcoinAddress(args);
  }

  QString msg = "Unknown address signatureType(" + signature_type + ") to create!";
  CLog::log(msg, "app", "fatal");
  return {false, UnlockDocument{}};
}










