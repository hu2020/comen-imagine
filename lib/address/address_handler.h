#ifndef CADDRESS_H
#define CADDRESS_H


class CAddress
{
public:
  static std::tuple<bool, UnlockDocument> createANewAddress(
    const QString& signature_type,
    const QString& signature_mod = CConsts::DEFAULT_SIGNATURE_MOD,
    const QString& signature_version = "0.0.0");

  static std::tuple<bool, UnlockDocument> createANewBasicAddress(
    const QString& signature_mod,
    const QString& signature_version = "0.0.0");

  static std::tuple<bool, UnlockDocument> createANewStrictAddress(
    const QString& signature_mod,
    const QString& signature_version = "0.0.1");



}; //  CAddress

#endif // CADDRESS_H
