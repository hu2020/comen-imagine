#include "stable.h"
#include "lib/file_handler/file_handler.h"
#include "lib/k_v_handler.h"
#include "lib/network/email/email.h"
#include "network_handler.h"

NetworkHandler::NetworkHandler()
{

}

bool NetworkHandler::iPush(
  const QString &title,
  const QString &message,
  const QString &sender,
  const QString &receiver)
{
  try {
    CLog::log("iPush args: title(" + title + ") sender(" + sender + ") receiver(" + receiver + ") message(" + message + ")", "app", "trace");
    //ZMQHook.doCallSync("SPSH_push_packet", args);// Synchronous Passive Server Hook

    QString email_hash, email_body;
    if (CConsts::DO_HARDCOPY_OUTPUT_EMAILS)
    {
      // create emails and write it on local hard drive
      // create an email copy in local hard drive
      // in such a way user can send email manually
      // she can sign some transactions and create a proper block, but not send it immideately
      // keeps it in safe place and when it need just send it manually to one or more backers
      auto[status, email, hash] = FileHandler::writeEmailAsFile(
        title,
        message,
        sender,
        receiver);
      email_hash = hash;
      email_body= email;
      CLog::log("write on HD res: " + CUtils::dumpIt(status));
    }

    if (!CConsts::EMAIL_IS_ACTIVE)
      return true;

    // avoid duplicate sending
    QJsonObject sent_emails_obj;
    QString sent_emails = KVHandler::getValue("SENT_EMAILS"); //sentEmails -> sent_emails & SENT_EMAILS
    if (sent_emails == "")
    {
      sent_emails_obj = QJsonObject {};
      KVHandler::upsertKValue("sent_emails", CUtils::serializeJson(sent_emails_obj));
    } else {
      sent_emails_obj = CUtils::parseToJsonObj(sent_emails);
    }
    if (sent_emails_obj.keys().contains(email_hash))
    {
      CLog::log("email already was sent TO: " + receiver + "(" + title + ") hash(" + email_hash + ")", "app", "trace") ;
//      return true;

    } else {
        sent_emails_obj[email_hash] = CUtils::getNow();
        KVHandler::upsertKValue("SENT_EMAILS", CUtils::serializeJson(sent_emails_obj));
    }

    QJsonObject refresh_sents = {};
    QString cDate = CUtils::minutesBefore(CMachine::getCycleByMinutes());
    for (QString aHash: sent_emails_obj.keys()) {
      if (sent_emails_obj[aHash].toString() > cDate)
        refresh_sents[aHash] = sent_emails_obj[aHash];
    }
    KVHandler::upsertKValue("SENT_EMAILS", CUtils::serializeJson(refresh_sents));

    // send email to via SMTP server
    bool email_status = EmailHandler::sendEmailWrapper(
      sender,
      title,
      email_body,
      receiver
    );
    return email_status;
  } catch (std::exception) {
    CLog::log("something goes wrong in i PushSync", "app", "fatal");
    return false;
  }
}
