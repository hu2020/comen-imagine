#include "broadcast_logger.h"

const QString BroadcastLogger::stbl_logs_broadcast = "c_logs_broadcast";

BroadcastLogger::BroadcastLogger()
{

}


QVDRecordsT BroadcastLogger::listSentBlocks(const QString &afterThat_, const QStringList& fields)
{
  try {
    QString afterThat = afterThat_;
    if (afterThat == "")
    {
      if (CMachine::isInSyncProcess())
      {
        afterThat = CUtils::minutesBefore(CMachine::getCycleByMinutes() / 40);
      }else{
        afterThat = CUtils::minutesBefore(CMachine::getCycleByMinutes() / 20);
      }
    }
    return DbModel::select(
      stbl_logs_broadcast,
      fields,
      {{"lb_send_date", afterThat, ">="}}).records;
  } catch (std::exception) {
    return {};
  }
}

QStringList BroadcastLogger::listSentBloksIds()
{
  QVDRecordsT rows = listSentBlocks();
  QStringList out;
  for (QVDicT a_row: rows)
  {
    out.append(QStringList
    {
       a_row.value("lb_type", "").toString(),
       a_row.value("lb_code", "").toString(),
       a_row.value("lb_sender", "").toString(),
       a_row.value("lb_receiver", "").toString()
    }.join(""));
  }
  return out;
}

bool BroadcastLogger::addSentBlock(QVDicT values)
{
  try {
     values["lb_send_date"] = values.value("lb_send_date", CUtils::getNow());
     CLog::log("add SentBlock: " + CUtils::dumpIt(values), "app", "trace");
     DbModel::insert(
       stbl_logs_broadcast,
       values);
   return true;
  } catch (std::exception) {
    return false;
  }
}
