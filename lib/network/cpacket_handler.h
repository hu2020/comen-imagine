#ifndef CPACKETHANDLER_H
#define CPACKETHANDLER_H

/**
 * @brief The CPacketHandler class
 * the CPacket is a packet that is transmitted between nodes. the packet can be a only a block, or n blocks or a combination of different document
 * a CPacket can be transmitted in one email or millions of small TCP or UDP packets
 */
class CPacketHandler
{
public:
  CPacketHandler();
  const static QString stbl_cpacket_ticketing;
  const static QStringList stbl_cpacket_ticketing_fields;

  static std::tuple<bool, QString, QJsonObject> decryptAndParsePacketSync(
    const QString &sender,
    const QString &receiver,
    const QString &file_name,
    const QString &message);

  static bool tLog(const QString &file_name);
  static QVDRecordsT iRead(const QString &file_name);
  static int getTry(const QString &file_name);
  static bool iUpdate(const QString &file_name);
  static bool iCreate(const QString &file_id);

};

#endif // CPACKETHANDLER_H
