#ifndef EMAIL_H
#define EMAIL_H





class EmailHandler
{
public:
  EmailHandler();

  static bool sendEmailWrapper(
    const QString& sender_,
    const QString& title,
    const QString& message,
    const QString& receiver);

  static bool sendMail(
    const QString& host,
    const QString& sender,
    const QString& pass,
    const QString& subject,
    const QString& message,
    const QString& receiver,
    uint16_t port);

  static void loopEmailPoper();
  static bool popPrivateEmail();
  static bool popPublicEmail();

  static void loopEmailSender();
  static bool sendPrivateEmail();
  static bool sendPublicEmail();

};

#endif // EMAIL_H
