#ifndef BROADCASTLOGGER_H
#define BROADCASTLOGGER_H

#include "lib/machine/machine_handler.h"

class BroadcastLogger
{
public:
  const static QString stbl_logs_broadcast;
  BroadcastLogger();

  static QVDRecordsT listSentBlocks(const QString &afterThat_ = "", const QStringList& fields = {"lb_type", "lb_code", "lb_sender", "lb_receiver"});
  static QStringList listSentBloksIds();
  static bool addSentBlock(QVDicT values);
};

#endif // BROADCASTLOGGER_H
