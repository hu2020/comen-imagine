#include "stable.h"
#include "lib/pgp/cpgp.h"
#include "lib/machine/machine_handler.h"
#include "cpacket_handler.h"

const QString CPacketHandler::stbl_cpacket_ticketing = "c_cpacket_ticketing";
const QStringList CPacketHandler::stbl_cpacket_ticketing_fields = {"msg_id", "msg_file_id", "msg_try_count", "msg_creation_date", "msg_last_modified"};

CPacketHandler::CPacketHandler()
{

}

struct TmpShake {
  QString n_id;
  QString connection_type;
};

void replyHandshakeNeighbor(const TmpShake &tsh)
{
  CLog::log("automatically handshake with Unknown email reply to neighbor's handshake... neighbor(" + tsh.n_id + ") connection_type(" + tsh.connection_type + ")" );
  std::this_thread::sleep_for (std::chrono::seconds(10));

  CMachine::handshakeNeighbor(tsh.n_id, tsh.connection_type);

}

std::tuple<bool, QString, QJsonObject> CPacketHandler::decryptAndParsePacketSync(
  const QString &sender,
  const QString &receiver,
  const QString &file_name,
  const QString &message
  )
{
  QString connection_type;
  QJsonObject message_obj;

  if (message == "")
  {
    CLog::log("CPacket has empty message! sender(" + sender + ") receiver(" + receiver + ") file_name(" + file_name + ")", "app", "error" );
    return {false, connection_type, message_obj};
  }

  // later again try this message or purge it after a certain try and fail. but not blocked on this message.
  tLog(file_name);

  // retreive sender's info
  QVDRecordsT sender_info = CMachine::getNeighbors("", "", "", "", sender);
  QString sender_public_Key = "";

  if (sender_info.size() > 0)
  {
    sender_public_Key = sender_info[0].value("n_pgp_public_key").toString();

  } else {
    /**
     * sender is not in my neighbors, so i add it as a new neighbor
     * TODO: probably security issue! machine must not add all new random emails.
     * instead must list them and user decides about that
     * so it must be implemented ASAP
     */
    CLog::log("Unknown email addresse sent msg, so add it automatically as a new neighbor(" + sender + ")", "sec", "info");
    CMachine::addANewNeighbor(sender, CConsts::PUBLIC);

    // retrieve id of newly inserted email
    QVDRecordsT newNeiInfo = CMachine::getNeighbors("", "", "", "", sender);
    if (newNeiInfo.size() == 0)
    {
      CLog::log("coludn't insert unknown email as a new neighbor(" + sender + ")");
      return {false, connection_type, message_obj};
    }

    // and now do handshake
    TmpShake tsh{newNeiInfo[0].value("n_id").toString(), CConsts::PUBLIC};
    std::thread(replyHandshakeNeighbor, tsh).detach();

  }
  // if (sender_public_Key == '')
  //     sender_public_Key = null;    // for new neighbors

  QString machine_private_PGP_key;
  EmailSettings machine_public_email = CMachine::getPubEmailInfo();
  EmailSettings machine_private_email = CMachine::getPrivEmailInfo();


  if (receiver == machine_private_email.m_address)
  {
    machine_private_PGP_key = machine_private_email.m_PGP_private_key;
    connection_type = CConsts::PRIVATE;

  }
  else if (receiver == machine_public_email.m_address)
  {
    machine_private_PGP_key = machine_public_email.m_PGP_private_key;
    connection_type = CConsts::PUBLIC;

  } else {
    CLog::log("in parse PacketSync unknown email cpacket: " + receiver, "sec", "error");
    return {false, connection_type, message_obj};
  }

  CPGPMessage decrypt_res;
  try {
    decrypt_res = CPGP::decryptPGP(message, machine_private_PGP_key, sender_public_Key);
    CLog::log(
      "decrypt PGP res: m_decryption_status(" + CUtils::dumpIt(decrypt_res.m_decryption_status) +
      ") m_decryption_status(" + CUtils::dumpIt(decrypt_res.m_decryption_status) +
      ") m_is_authenticated(" + CUtils::dumpIt(decrypt_res.m_is_authenticated) +
      ") m_is_signed(" + CUtils::dumpIt(decrypt_res.m_is_signed) +
      ") m_is_verified(" + CUtils::dumpIt(decrypt_res.m_is_verified) +
      ") m_is_compressed(" + CUtils::dumpIt(decrypt_res.m_is_compressed) +
      ") ",
      "app", "trace");

    if (!decrypt_res.m_decryption_status)
    {
      CLog::log("Failed decrypt msg1 !", "sec", "error");
      return {false, connection_type, message_obj};
    }

  } catch (std::exception) {
    CLog::log("Failed decrypt msg2 !", "sec", "error");
    return {false, connection_type, message_obj};
  }

  if (decrypt_res.m_is_signed && !decrypt_res.m_is_verified)
  {
    CLog::log("droped Invalid Or Insecure Message (Wrong signature)", "sec", "error");
//    this.dropInvalidOrInsecureMessage(packet)
    return {false, connection_type, message_obj};
  }

  if (decrypt_res.m_message == "")
  {
    CLog::log("droped Invalid Or Insecure Message (empty message body)", "sec", "error");
    return {false, connection_type, message_obj};
  }

  try {
    message_obj = CUtils::parseToJsonObj(decrypt_res.m_message);
  } catch (std::exception) {
    CLog::log("Failed parse msg ${utils.stringify(e)} ${decryptedMessage.message}", "sec", "error");
    return {false, connection_type, message_obj};
  }

  return {true, connection_type, message_obj};

}


//  -  -  -  -  -  CPacket ticketing
bool CPacketHandler::tLog(const QString &file_name)
{
  QVDRecordsT res = iRead(file_name);
  if (res.size() == 0)
  {
    iCreate(file_name);
  } else {
    iUpdate(file_name);
  }
  return true;
}

QVDRecordsT CPacketHandler::iRead(const QString &file_name)
{
  QueryRes res = DbModel::select(
    stbl_cpacket_ticketing,
    stbl_cpacket_ticketing_fields,
    {{"msg_file_id", file_name}}
  );
  return res.records;
}

int CPacketHandler::getTry(const QString &file_name)
{
  QueryRes res = DbModel::select(
    stbl_cpacket_ticketing,
    {"msg_try_count"},
    {{"msg_file_id", file_name}}
  );

//          let res = db.scustom('SELECT msg_try_count FROM i_message_ticketing WHERE =$1', [file_name]);
  if (res.records.size() > 0)
   return res.records[0]["msg_try_count"].toInt();

  return 0;
}

bool CPacketHandler::iUpdate(const QString &file_name)
{
  // TODO: implement it ASAP
//  DbModel::customQuery("",
//    "UPDATE " + stbl_cpacket_ticketing + " SET msg_last_modified=:msg_last_modified, msg_try_count=msg_try_count+1 WHERE msg_file_id=:msg_file_id",
//  {{}}
//  );
  return true;
}



bool CPacketHandler::iCreate(const QString &file_id)
{
  QString now_ = CUtils::getNow();
  DbModel::insert(
    stbl_cpacket_ticketing,
    {
      {"msg_file_id", file_id},
      {"msg_try_count", 0},
      {"msg_creation_date", now_},
      {"msg_last_modified", now_}
    });
  return true;
}
