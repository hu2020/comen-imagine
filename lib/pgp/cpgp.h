#ifndef CPGP_H
#define CPGP_H




class CPGPMessage
{
public:
  bool m_decryption_status = false;
  QString m_pgp_versaion = "0.0.1";
  QString m_secret_key = "";
  QString m_initialization_vector = "";
  QString m_decrypted_secret_key = "";
  QString m_decrypted_initialization_vector = "";
  QString m_message = "";
  QString m_aes_version = "";
  QString m_signature_version = "";
  bool m_is_signed = false;
  bool m_is_compressed = false;
  bool m_is_verified = false;
  bool m_is_authenticated = false;
  QString m_zip_version = "";
  QString m_zip_algorithm = "";

};

class CPGP
{
public:
  CPGP();
  static std::tuple<bool, QString> encryptPGP(
    const QString& message,
    const QString& sender_priv_key,
    const QString& receiver_pub_key,
    QString secret_key = "",
    QString initialization_vector = "",
    const bool& should_compress = true,
    const bool& should_sign = true);

  static CPGPMessage decryptPGP(
    const QString& message_,
    const QString& priv_key,
    const QString& sender_pub_key);

  static QString wrapPGPEnvelope(const QString& content);

  static QString stripPGPEnvelope(const QString& content);

  static std::tuple<QString, QString> generateSecretKeyIV();

};

#endif // CPGP_H
