#include "stable.h"
#include "lib/ccrypto.h"

#include "cpgp.h"

CPGP::CPGP()
{

}

std::tuple<bool, QString> CPGP::encryptPGP(
  const QString& message,
  const QString& sender_priv_key,
  const QString& receiver_pub_key,
  QString secret_key,
  QString initialization_vector,
  const bool& should_compress,
  const bool& should_sign)
{
  QString pgp_versaion = "0.0.1";

  // base64 encoding
  QString base64Encoded = CCrypto::base64Encode(message);

  QString signature = "";
  if (should_sign) {
    signature = CCrypto::nativeSign(sender_priv_key, CCrypto::keccak256(base64Encoded));
  }
  QJsonObject Jmsg_plus_signature {
    {"sigVersion", "0.0.2"},
    {"isSigned", (should_sign ? QString::fromStdString("true") : QString::fromStdString("false"))}, //  // this version denotes to used hashing and signature
    {"message", base64Encoded},
    {"signature", signature }};

  QString msg_plus_signature = CUtils::serializeJson(Jmsg_plus_signature);
  CLog::log("msg_plus_signature: " + msg_plus_signature, "app", "trace");

  QString compressed;
  if (should_compress)
  {
    QJsonObject Jcompressed {
      {"zipVersion", "0.0.0"},
      {"zipAlgorithm", "zip..."},
      {"isCompressed", "true"},
      {"message", Compressor::compress(msg_plus_signature)}}; // TODO: implementing compress function
    compressed = CUtils::serializeJson(Jcompressed);
  }
  else
  {
    QJsonObject Jcompressed {
      {"isCompressed", "false"},
    {"message", msg_plus_signature}};
    compressed = CUtils::serializeJson(Jcompressed);
  }
  CLog::log("compressed1: " + compressed, "app", "trace");

  QString pgpRes = "";

  if (receiver_pub_key == "")
  {
    // there is no pub key, so ther is no sence to encryption
    QJsonObject JpgpRes {
      {"iPGPVersion", pgp_versaion},
      {"secretKey", CConsts::MESSAGE_TAGS::NO_ENCRYPTION},
      {"message", compressed}, // to support javascript nodes, maybe need to use \"compresses\"
      {"isAuthenticated", "false"}};

    pgpRes = CUtils::serializeJson(JpgpRes);

  } else {
    if (secret_key == "")
      secret_key = CCrypto::getRandomNumber(32);
    if (initialization_vector == "")
      initialization_vector = CCrypto::getRandomNumber(16);

    secret_key = secret_key.midRef(0, 32).toString();
    initialization_vector = initialization_vector.midRef(0, 16).toString();

    // conventional symmetric encryption
    auto[aes_status, aesEncoded] = CCrypto::AESencrypt(
      compressed,
      secret_key,
      initialization_vector);

    if (!aes_status)
      return {false, "failed in AESencrypt"};
    QString pgp_encrypted_secret_key, pgp_encrypted_iv;
    if (CConsts::CURRENT_AES_VERSION == "0.0.0")
    {
      pgp_encrypted_secret_key = CConsts::MESSAGE_TAGS::NO_ENCRYPTION;
      pgp_encrypted_iv = "";
    }else{
      pgp_encrypted_secret_key = CCrypto::encryptStringWithPublicKey(
        receiver_pub_key,
        secret_key);

      pgp_encrypted_iv = CCrypto::encryptStringWithPublicKey(
        receiver_pub_key,
        initialization_vector);
    }
    QJsonObject JaesEncoded {
      {"aesVersion", CConsts::CURRENT_AES_VERSION},
      {"encrypted", aesEncoded}};

    QJsonObject JpgpRes {
      {"iPGPVersion", pgp_versaion},
      {"secretKey", pgp_encrypted_secret_key},
      {"iv", pgp_encrypted_iv},
      {"message", JaesEncoded},
      {"isAuthenticated", "true"}};

    pgpRes = CUtils::serializeJson(JpgpRes);
  }
  CLog::log("pgpRes: " + pgpRes, "app", "trace");

  // base64 encoding
  return {true, CCrypto::base64Encode(pgpRes)};
}

CPGPMessage CPGP::decryptPGP(
  const QString& message_,
  const QString& priv_key,
  const QString& sender_pub_key)
{
  CPGPMessage finalDec;
  QString message = stripPGPEnvelope(message_);


  // decode base64
  QString base64Decoded = CCrypto::base64Decode(message);
  CLog::log("base64Decoded" + base64Decoded);
  QJsonObject obj = CUtils::parseToJsonObj(base64Decoded);
  finalDec.m_is_authenticated = obj.value("isAuthenticated").toString() == "true";
  finalDec.m_pgp_versaion = obj.value("iPGPVersion").toString();
  finalDec.m_secret_key = obj.value("secretKey").toString();
  finalDec.m_initialization_vector = obj.value("iv").toString();

  QJsonObject AESdecrypted;

  if (finalDec.m_secret_key == CConsts::MESSAGE_TAGS::NO_ENCRYPTION) {
    CLog::log("AESdecrypted = " + obj.value("message").toString(), "app", "trace");
    auto tmpAESdecrypted = obj.value("message");
    if (tmpAESdecrypted.isObject())
    {
      AESdecrypted = tmpAESdecrypted.toObject();
    } else{
      // backward compatibility with Javascrit nodes
      AESdecrypted = CUtils::parseToJsonObj(tmpAESdecrypted.toString());
      AESdecrypted["message"] = CUtils::parseToJsonObj(AESdecrypted.value("message").toString());
    }
  }
  else
  {
    QJsonObject aesEncoded = obj.value("message").toObject();
    finalDec.m_aes_version = aesEncoded.value("aesVersion").toString(); //, "Unknown AES Version!"

    QString AESencryptedMsg = aesEncoded.value("encrypted").toString();

    // decrypt secret key
    QString  decryptedSecretKey;
    try {
      finalDec.m_decrypted_secret_key = CCrypto::decryptStringWithPrivateKey(priv_key, finalDec.m_secret_key);
      finalDec.m_decrypted_initialization_vector = CCrypto::decryptStringWithPrivateKey(priv_key, finalDec.m_initialization_vector);
    } catch (std::exception) {
      finalDec.m_decryption_status = false;
      finalDec.m_is_verified = false;
      finalDec.m_message = "wrong priv_key or encrypted_secret_key";
      return finalDec;
    }

    // decrypt message body
    try {
        auto [status_aes_dec, aes_dec] = CCrypto::AESdecrypt(
          AESencryptedMsg,
          finalDec.m_decrypted_secret_key,
          finalDec.m_decrypted_initialization_vector,
          finalDec.m_aes_version);
        Q_UNUSED(status_aes_dec);
        AESdecrypted = CUtils::parseToJsonObj(aes_dec);

    } catch (std::exception) {
      finalDec.m_decryption_status = false;
      finalDec.m_is_verified = false;
      finalDec.m_message = "AESdecrypt failed";
      return finalDec;
    }
  }

  finalDec.m_is_compressed = AESdecrypted.value("isCompressed").toString() == "true";
  finalDec.m_zip_version = AESdecrypted.keys().contains("zipVersion") ? AESdecrypted.value("isCompressed").toString() : "Unknown zip version!";
  finalDec.m_zip_algorithm = AESdecrypted.keys().contains("algorithm") ? AESdecrypted.value("algorithm").toString() : "Unknown zip algorithm!";

  QJsonObject msg_plus_signature;
  if (finalDec.m_secret_key == CConsts::MESSAGE_TAGS::NO_ENCRYPTION)
  {
    msg_plus_signature = AESdecrypted.value("message").toObject();
  } else {
    msg_plus_signature = CUtils::parseToJsonObj(AESdecrypted.value("message").toString());
  }

  // decompress it if it is compressed
  if (finalDec.m_is_compressed)
  {
    // decompress message
    message = Compressor::decompress(msg_plus_signature.value("message").toString());
  }

  // control signature if is signed
  finalDec.m_is_signed = msg_plus_signature.value("isSigned").toString() == "true";
  finalDec.m_is_verified = false;
  if (finalDec.m_is_signed)
  {
    if (sender_pub_key == "")
    {
      finalDec.m_decryption_status = false;
      finalDec.m_is_verified = false;
      finalDec.m_message = "missed sender_pub_key";
      return finalDec;
    }
    finalDec.m_signature_version = msg_plus_signature.value("sigVersion").toString();
    QString signature = msg_plus_signature.value("signature").toString();
    QString hash = CCrypto::keccak256(msg_plus_signature.value("message").toString());
    finalDec.m_is_verified = CCrypto::nativeVerifySignature(sender_pub_key, hash, signature);
  }
  finalDec.m_decryption_status = true;
  finalDec.m_message = CCrypto::base64Decode(msg_plus_signature.value("message").toString());
  return finalDec;
}


QString CPGP::wrapPGPEnvelope(const QString& content)
{
    return CConsts::MESSAGE_TAGS::iPGPStartEnvelope + content + CConsts::MESSAGE_TAGS::iPGPEndEnvelope;
}

QString CPGP::stripPGPEnvelope(const QString& content_)
{
  QString content = content_.simplified(); // remove extra spaces
  if (content.contains(CConsts::MESSAGE_TAGS::iPGPStartEnvelope))
  {
    content = content.split(CConsts::MESSAGE_TAGS::iPGPStartEnvelope)[1];
  }
  if (content.contains(CConsts::MESSAGE_TAGS::iPGPEndEnvelope))
  {
    content = content.split(CConsts::MESSAGE_TAGS::iPGPEndEnvelope)[0];
  }
  return content;
}

std::tuple<QString, QString> CPGP::generateSecretKeyIV()
{
  QString secret_key = CCrypto::getRandomNumber();
  secret_key = secret_key.midRef(0, 32).toString();

  QString initialization_vector = CCrypto::getRandomNumber();
  initialization_vector = initialization_vector.midRef(0, 16).toString();

  return {secret_key, initialization_vector};
}
