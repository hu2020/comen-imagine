#ifndef COLLISIONSHANDLER_H
#define COLLISIONSHANDLER_H

struct TransientCollision
{
  CDocHashT collision_doc_hash = "";
  CDateT collision_receive_date = "";
  DNASharePercentT collision_voter_shares = 0.0;
};

class CollisionsHandler
{
public:
  CollisionsHandler();

  static const QString stbl_collisions;
  static const QStringList stbl_collisions_fields;

  static std::tuple<bool, QVDRecordsT> hasCollision(
    const QString& collision_ref);

  static QVDRecordsT getLoggedDocs(const CDocHashT& doc_hash);

  static bool logACollision(
    const CAddressT& cl_voter,
    const CDocHashT& cl_collision_ref,
    const CBlockHashT& cl_block_hash,
    const CDocHashT& cl_doc_hash,
    const CDateT& cl_creation_date,
    const CDateT& cl_receive_date);

  static QJsonObject convertQVDRecordsToJson(const QVDRecordsT& collisions);

  static QVDRecordsT searchInCollisionLogs(
    const ClausesT& clauses,
    QStringList fields = stbl_collisions_fields,
    const OrderT& order = {},
    const int limit = 0);

  static std::tuple<bool, CDocHashT, int64_t> makeCollisionReport(
    const CDocHashT& collision_ref);


};

#endif // COLLISIONSHANDLER_H
