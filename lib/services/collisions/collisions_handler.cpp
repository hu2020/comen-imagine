#include "stable.h"
#include "collisions_handler.h"

const QString CollisionsHandler::stbl_collisions = "c_collisions";
const QStringList CollisionsHandler::stbl_collisions_fields = {"cl_id", "cl_voter", "cl_collision_ref","cl_block_hash", "cl_doc_hash", "cl_creation_date","cl_receive_date"};

CollisionsHandler::CollisionsHandler()
{

}

/**
 * @brief CollisionsHandler::hasCollision
 * @param collision_ref
 * @return {has_collision, the_collisioned_records}
 */
std::tuple<bool, QVDRecordsT> CollisionsHandler::hasCollision(
  const QString& collision_ref)
{
  QueryRes res = DbModel::select(
    stbl_collisions,
    {"cl_voter", "cl_collision_ref", "cl_block_hash", "cl_doc_hash", "cl_creation_date", "cl_receive_date"},
    {{"cl_collision_ref", collision_ref}},
    {{"cl_receive_date", "ASC"}});

  return {res.records.size() > 0, res.records};
}


QVDRecordsT CollisionsHandler::getLoggedDocs(const CDocHashT& doc_hash)
{
  QueryRes res = DbModel::select(
    stbl_collisions,
    {"cl_doc_hash"},
    {{"cl_doc_hash", doc_hash}});
  return res.records;
}

bool CollisionsHandler::logACollision(
  const CAddressT& cl_voter,
  const CDocHashT& cl_collision_ref,
  const CBlockHashT& cl_block_hash,
  const CDocHashT& cl_doc_hash,
  const CDateT& cl_creation_date,
  const CDateT& cl_receive_date)
{
  QVDicT values {
    {"cl_voter", cl_voter},
    {"cl_collision_ref", cl_collision_ref},
    {"cl_block_hash", cl_block_hash},
    {"cl_doc_hash", cl_doc_hash},
    {"cl_creation_date", cl_creation_date},
    {"cl_receive_date", cl_receive_date}};

  DbModel::insert(
    stbl_collisions,
    values);

  return true;
}

QVDRecordsT CollisionsHandler::searchInCollisionLogs(
  const ClausesT& clauses,
  QStringList fields,
  const OrderT& order,
  const int limit)
{
  QueryRes res = DbModel::select(
    stbl_collisions,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}

QJsonObject CollisionsHandler::convertQVDRecordsToJson(const QVDRecordsT& collisions)
{
  QJsonArray Jcollisions {};
  for(QVDicT a_coll: collisions)
  {
    QJsonObject a_j_coll {
      {"cl_voter", a_coll.value("cl_voter").toString()},
      {"cl_collision_ref", a_coll.value("cl_collision_ref").toString()},
      {"cl_block_hash", a_coll.value("cl_block_hash").toString()},
      {"cl_doc_hash", a_coll.value("cl_doc_hash").toString()},
      {"cl_creation_date", a_coll.value("cl_creation_date").toString()},
      {"cl_receive_date", a_coll.value("cl_receive_date").toString()}
    };
    Jcollisions.push_back(a_j_coll);
  }
  return QJsonObject {{"collisions", Jcollisions}};
}

/**
 * @brief CollisionsHandler::makeCollisionReport
 * @param collision_ref
 * @return [has_collision, winer_doc, winer_score]
 */
std::tuple<bool, CDocHashT, int64_t> CollisionsHandler::makeCollisionReport(
  const CDocHashT& collision_ref)
{
  // TODO: probably need to improve to more robust (e.g. like transaction doublespend solution)
  // do it ASAP
  QVDRecordsT records = searchInCollisionLogs(
    {{"cl_collision_ref", collision_ref}},
    stbl_collisions_fields,
    {{"cl_receive_date", "DESC"}});    // it is DESC to fore over-write early received on later arrived blocks

  if (records.size() == 0)
    return {false, "", 0};

  QHash<CAddressT, QHash<CDocHashT, TransientCollision> > votesDict {}; // a dummy dictionary to make uniq voter uniq docHash results
  for (QVDicT aVote: records)
  {
    CAddressT cl_voter = aVote.value("cl_voter").toString();
    if (!votesDict.keys().contains(cl_voter))
        votesDict[cl_voter] = QHash<CDocHashT, TransientCollision> {};


    auto[shares, shares_percentage] = DNAHandler::getAnAddressShares(cl_voter, aVote.value("cl_receive_date").toString()); // dna handler to calculate
    if (shares_percentage == 0)
    {
      CLog::log("collision voter shares == 0 for (" + CUtils::shortBech16(cl_voter) + ")", "app", "warning");
      shares_percentage = CUtils::CFloor(CConsts::MINIMUM_SHARES_IF_IS_NOT_SHAREHOLDER);
      CLog::log("collision voter shares == 0 for (" + CUtils::shortBech16(cl_voter) + ") tribute: " + QString::number(shares_percentage), "app", "warning");
    }

    votesDict[cl_voter][aVote.value("cl_doc_hash").toString()] = {
      aVote.value("cl_doc_hash").toString(),
      aVote.value("cl_receive_date").toString(),
      shares_percentage};

  }

  QHash<CAddressT, TransientCollision> unifiedRecordsForAVoter = {};
  for (CAddressT aVoter: votesDict.keys())
  {
    QHash<QString, TransientCollision> aVoterVotes {};
    for (CDocHashT aDocHash: votesDict[aVoter].keys())
    {
      TransientCollision aRecord = votesDict[aVoter][aDocHash];
      QString orderKey = aRecord.collision_receive_date + aDocHash;
      aVoterVotes[orderKey] = aRecord;
    }
    if (aVoterVotes.keys().size() > 0)
    {
        QStringList keys = aVoterVotes.keys();
        keys.sort();
        //let firstDocForVoter = utils.objKeys(aVoterVotes).sort()[0];
        unifiedRecordsForAVoter[aVoter] = aVoterVotes[keys[0]];  // first doc for this voter
    }
  }

  // score dict
  QHash<CDocHashT, int64_t> scoresDict = {};
  for (CAddressT aVoter: unifiedRecordsForAVoter.keys())
  {
    if (!scoresDict.keys().contains(unifiedRecordsForAVoter[aVoter].collision_doc_hash))
        scoresDict[unifiedRecordsForAVoter[aVoter].collision_doc_hash] = 0;
    scoresDict[unifiedRecordsForAVoter[aVoter].collision_doc_hash] += unifiedRecordsForAVoter[aVoter].collision_voter_shares;
  }

  // find winer
  CDocHashT winer_doc = "";
  int64_t winer_score = 0;
  for (CDocHashT aDocHash: scoresDict.keys())
  {
    if (scoresDict[aDocHash] > winer_score)
    {
      winer_doc = aDocHash;
      winer_score = scoresDict[aDocHash];
    }
  }

  CLog::log("collision winer is: " + winer_doc + " scores: " + CUtils::sepNum(winer_score), "app", "trace");
  return {true, winer_doc, winer_score};
}
