#ifndef BALLOTHANDLER_H
#define BALLOTHANDLER_H

class BallotDocument;

class BallotHandler
{
public:
  BallotHandler();

  static const QString stbl_ballots;
  static const QStringList stbl_ballots_fields;

  static const QString stbl_machine_ballots;
  static const QStringList stbl_machine_ballots_fields;

  static bool alreadyVoted(
    const CAddressT& voter,
    const CDocHashT& vote_ref);

  static bool removeBallot(const CDocHashT& ballot_hash);

  static bool recordBallotInDB(
    const CDocHashT& ba_hash,
    const CDocHashT& ba_polling_ref,
    const CDateT& ba_creation_date,
    const CDateT& ba_receive_date,
    const CAddressT& ba_voter,
    const DNAShareCountT& ba_voter_shares,
    const int8_t ba_vote,  // between -100, 0 , +100
    const QString& ba_comment,
    const uint64_t ba_vote_c_diff,
    const uint64_t ba_vote_r_diff);

  static QVDRecordsT searchInBallots(
    const ClausesT& clauses = {},
    const QStringList& fields = stbl_ballots_fields,
    const OrderT order = {},
    const uint64_t limit = 0);

  static QVDRecordsT searchInLocalBallots(
    const ClausesT& clauses = {},
    const QStringList& fields = stbl_machine_ballots_fields,
    const OrderT order = {},
    const uint64_t limit = 0);

  static QVDRecordsT searchInOnchainBallots(
    const ClausesT& clauses,
    const QStringList& fields = stbl_ballots_fields,
    const OrderT order = {},
    const uint64_t limit = 0);

  static std::tuple<bool, QString, BallotDocument*> prepareABallotDoc(
    const CDocHashT polling_hash,
    int32_t vote_amount,
    QString vote_comment = "",
    const CInputIndexT unlock_index = 0,
    const CAddressT& voter = CMachine::getBackerAddress(),
    const CDateT& vote_date = CUtils::getNow());

  static std::tuple<bool, QString> insertALocalBallot(
    const BallotDocument* ballot,
    const QString& mp_code = CMachine::getSelectedMProfile());

  static std::tuple<bool, QString> doVoteForProposal(
    const CDocHashT& proposal_hash,
    int32_t vote_amount,
    QString vote_comment = "");

  static std::tuple<bool, QString> doVoteForAdmPolling(
    const CDocHashT& adm_polling_hash,
    int32_t vote_amount,
    QString vote_comment);



};

#endif // BALLOTHANDLER_H
