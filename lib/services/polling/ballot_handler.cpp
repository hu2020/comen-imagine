#include "stable.h"

#include "polling_handler.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/ballot_document.h"
#include "lib/block/document_types/basic_tx_document.h"
#include "lib/transactions/basic_transactions/basic_transaction_handler.h"

#include "ballot_handler.h"

const QString BallotHandler::stbl_ballots = "c_ballots";
const QStringList BallotHandler::stbl_ballots_fields = {"ba_hash", "ba_pll_hash", "ba_creation_date", "ba_receive_date", "ba_voter", "ba_voter_shares", "ba_vote", "ba_vote_c_diff", "ba_vote_r_diff", "ba_comment"};

const QString BallotHandler::stbl_machine_ballots = "c_machine_ballots";
const QStringList BallotHandler::stbl_machine_ballots_fields = {"lbt_id", "lbt_mp_code", "lbt_hash", "lbt_pll_hash", "lbt_creation_date", "lbt_voter", "lbt_voter_shares", "lbt_voter_percent", "lbt_vote"};

BallotHandler::BallotHandler()
{

}

bool BallotHandler::alreadyVoted(
  const CAddressT& voter,
  const CDocHashT& vote_ref)
{
  QueryRes dbl = DbModel::select(
    stbl_ballots,
    {"ba_voter", "ba_pll_hash"},
    {{"ba_voter", voter}, {"ba_pll_hash", vote_ref}});
  if (dbl.records.size() > 0)
    return true;
  return false;
}

bool BallotHandler::removeBallot(const CDocHashT& ballot_hash)
{
  DbModel::dDelete(
    stbl_ballots,
    {{"ba_hash", ballot_hash}});

  return true;
}

bool BallotHandler::recordBallotInDB(
  const CDocHashT& ba_hash,
  const CDocHashT& ba_polling_ref,
  const CDateT& ba_creation_date,
  const CDateT& ba_receive_date,
  const CAddressT& ba_voter,
  const DNAShareCountT& ba_voter_shares,
  const int8_t ba_vote,  // between -100, 0 , +100
  const QString& ba_comment,
  const uint64_t ba_vote_c_diff,
  const uint64_t ba_vote_r_diff)
{
  // duplicate check
  bool already_voted = alreadyVoted(
    ba_voter,
    ba_polling_ref);
  QString vote_amendment_allowed = CConsts::NO;   // TODO: it must be retreived from dynamc POLLING_PROFILE_CLASSES. implement it!
  if ((already_voted) && (vote_amendment_allowed == CConsts::NO))
  {
    // TODO: make a black list and put all duplicated ballot in it and not counting in final response
    CLog::log("The Voter lready voted for polling(" + CUtils::hash8c(ba_polling_ref) + ") voter(" + CUtils::shortBech16(ba_voter) + ") ", "app", "warning");
    return false;
  }

  QVDicT values {
    {"ba_hash", ba_hash},
    {"ba_pll_hash", ba_polling_ref},
    {"ba_creation_date", ba_creation_date},    // ballot creation date, extracted from container block.creation Date
    {"ba_receive_date", ba_receive_date},   // machine local time
    {"ba_voter", ba_voter},
    {"ba_voter_shares", ba_voter_shares},
    {"ba_vote", ba_vote},
    {"ba_comment", ba_comment},
    {"ba_vote_c_diff", QVariant::fromValue(ba_vote_c_diff)},
    {"ba_vote_r_diff", QVariant::fromValue(ba_vote_r_diff)}
  };

  CLog::log("Inserting a Ballot: " + CUtils::dumpIt(values), "app", "trace");
  DbModel::insert(
    stbl_ballots,
    values);

  return true;
}

QVDRecordsT BallotHandler::searchInBallots(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_ballots,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}

QVDRecordsT BallotHandler::searchInLocalBallots(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_machine_ballots,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}

QVDRecordsT BallotHandler::searchInOnchainBallots(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_ballots,
    fields,
    clauses,
    order,
    limit);
    return res.records;
}


std::tuple<bool, QString, BallotDocument*> BallotHandler::prepareABallotDoc(
  const CDocHashT polling_hash,
  int32_t vote_amount,
  QString vote_comment,
  const CInputIndexT unlock_index,
  const CAddressT& voter,
  const CDateT& vote_date)
{
  QString msg;
  vote_comment = CUtils::sanitizingContent(vote_comment);
  vote_comment = CUtils::stripNonAlphaNumeric(vote_comment); // TODO remove this line ASAP in order to be able comment in multi-language

  QJsonObject blt_json {
    {"dType", CConsts::DOC_TYPES::Ballot},
    {"dClass", CConsts::BALLOT_CLASSES::Basic},
    {"dComment", vote_comment},
    {"dVer", "0.0.2"},
    {"dRef", polling_hash},      // reference pollingHash
    {"dCDate", vote_date},
    {"voter", voter},            // the bech32 address of voter
    {"vote", vote_amount}};      // a number between 100 to -100

  BallotDocument* ballot = new BallotDocument(blt_json);

  QString sign_message = ballot->getDocSignMsg();
  auto[sign_status, signer, uSet, signatures] = CMachine::signByMachineKey(sign_message, unlock_index);
  if (!sign_status){
    msg = "Failed in signing Ballot for polling(" + CUtils::hash8c(polling_hash) + ")";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }
  ballot->m_doc_ext_info = SignatureStructureHandler::compactUnlockersArray(QJsonArray {QJsonObject {
    {"uSet", uSet.exportToJson()},
    {"signatures", CUtils::convertQStringListToJSonArray(signatures)}}});
  ballot->setDExtHash();

  ballot->setDocLength();
  ballot->setDocHash();

  return {true, "", ballot};
}

std::tuple<bool, QString> BallotHandler::doVoteForProposal(
  const CDocHashT& proposal_hash,
  int32_t vote_amount,
  QString vote_comment)
{

  QString msg;
  // in order to send vote to network, must create a documetn of type vote and pay vote fee too

  GenRes polling_res = PollingHandler::retrievePollingByProposalHash(proposal_hash);
  if (!polling_res.status)
    return {false, polling_res.msg};
  CDocHashT polling_hash = polling_res.msg;

  // create ballot doc
  auto[prepare_status, res_msg, ballot] = prepareABallotDoc(
    polling_hash,
    vote_amount,
    vote_comment);
  if (!prepare_status)
  {
    msg = "Failed in prepare Bllot for proposal(" + CUtils::hash8c(proposal_hash) + ")";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  CLog::log("prepared ballot to send " + ballot->safeStringifyDoc(true), "app", "info");

  // calculate ballot cost
  auto[cost_status, ballot_dp_cost] = ballot->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    CUtils::getNow());
  if (!cost_status)
  {
    msg = "Failed in bllot cost calculation for proposal(" + CUtils::hash8c(proposal_hash) + ")";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[coins_status, coins_msg, spendable_coins, spendable_amount] = Wallet::getSomeCoins(
    CUtils::CFloor(ballot_dp_cost * 1.3),  // an small portion bigger to support DPCosts
    CConsts::COIN_SELECTING_METHOD::PRECISE);
  if (!coins_status)
  {
    msg = "Failed in finding coin to spend for voting on proposal(" + CUtils::hash8c(proposal_hash) + ")";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // create a transaction for payment
  auto changeback_res = Wallet::getAnOutputAddress(true);
  if (!changeback_res.status)
  {
    msg = "Failed in create changeback address for ballot!";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  CAddressT change_back_address = changeback_res.msg;
  std::vector<TOutput> outputs {
    TOutput{change_back_address, 1, CConsts::OUTPUT_CHANGEBACK},
    TOutput{"TP_BALLOT", ballot_dp_cost, CConsts::OUTPUT_TREASURY}};

  auto trx_template1 = BasicTransactionTemplate {
    spendable_coins,
    outputs,
    static_cast<CMPAIValueT>(ballot_dp_cost * 1.3),  // max DPCost
    0,    // pre calculated dDPCost
    "Payed for ballot cost",
    ballot->getDocHash()};
  auto[tx_status, res_msg1, ballot_payer_trx, dp_cost1] = BasicTransactionHandler::makeATransaction(trx_template1);
  if (!tx_status)
    return {false, res_msg1};

  CLog::log("Signed trx for proposal-vote cost: " + ballot_payer_trx->safeStringifyDoc(true), "app", "info");

  // mark UTXOs as used in local machine
  Wallet::locallyMarkUTXOAsUsed(ballot_payer_trx);

  // inser ballot in local db (as a voted polling);
  auto[insert_res, insert_msg] = insertALocalBallot(ballot);
  if (!insert_res)
    return {false, insert_msg};

  // push trx & vote-ballot to Block buffer
  auto[ballot_push_res, ballot_push_msg] = CMachine::pushToBlockBuffer(ballot, ballot_dp_cost);
  if (!ballot_push_res)
  {
    msg = "Failed in push ballot to block buffer polling(" + CUtils::hash8c(ballot->getDocRef()) + ") " + ballot_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[trx_push_res, trx_push_msg] = CMachine::pushToBlockBuffer(ballot_payer_trx, ballot_payer_trx->getDocCosts());
  if (!trx_push_res)
  {
    msg = "Failed in push trx to block buffer polling(" + CUtils::hash8c(ballot_payer_trx->getDocRef()) + ") " + trx_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  return {true, "Your Vote was pushed to Block Buffer"};
}

std::tuple<bool, QString> BallotHandler::insertALocalBallot(
  const BallotDocument* ballot,
  const QString& mp_code)
{
  QString msg;
  QVDRecordsT dbl = searchInLocalBallots(
    {{"lbt_mp_code", mp_code},
    {"lbt_pll_hash", ballot->getDocRef()},
    {"lbt_voter", ballot->m_voter}},
    {"lbt_id"});
  if (dbl.size() > 0)
  {
    msg = "account(" + CUtils::shortBech16(ballot->m_voter) + ") already voted for polling(" + CUtils::hash8c(ballot->getDocRef()) + ")";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[shares, issuer_shares_percentage] = DNAHandler::getAnAddressShares(ballot->m_voter, ballot->m_doc_creation_date);

  QVDicT values {
    {"lbt_mp_code", mp_code},
    {"lbt_hash", ballot->getDocHash()},
    {"lbt_pll_hash", ballot->getDocRef()},
    {"lbt_creation_date", ballot->m_doc_creation_date},
    {"lbt_vote", ballot->m_vote},
    {"lbt_voter", ballot->m_voter},
    {"lbt_voter_shares", shares},
    {"lbt_voter_percent", issuer_shares_percentage}};
  CLog::log("insert a local ballot: " + CUtils::dumpIt(values), "app", "info");
  bool res = DbModel::insert(
    stbl_machine_ballots,
    values);
  if (!res)
    return {false, "Fail in inserting Ballot in local db"};

  return {true, "Ballot was recorded in local database"};
}

// js name was doVote
std::tuple<bool, QString> BallotHandler::doVoteForAdmPolling(
  const CDocHashT& adm_polling_hash,
  int32_t vote_amount,
  QString vote_comment)
{

  QString msg;

  GenRes polling_res = PollingHandler::retrievePollingByAdmPollingHash(adm_polling_hash);
  if (!polling_res.status)
    return {false, polling_res.msg};
  CDocHashT polling_hash = polling_res.msg;

  // create ballot doc
  auto[prepare_status, res_msg, ballot] = prepareABallotDoc(
    polling_hash,
    vote_amount,
    vote_comment);
  if (!prepare_status)
  {
    msg = "Failed in prepare Bllot for adm-polling(" + CUtils::hash8c(adm_polling_hash) + ")";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  CLog::log("prepared ballot to send " + ballot->safeStringifyDoc(true), "app", "info");


  // calculate ballot cost
  auto[cost_status, ballot_dp_cost] = ballot->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    CUtils::getNow());
  if (!cost_status)
  {
    msg = "Failed in bllot cost calculation for adm-polling(" + CUtils::hash8c(adm_polling_hash) + ")";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[coins_status, coins_msg, spendable_coins, spendable_amount] = Wallet::getSomeCoins(
    CUtils::CFloor(ballot_dp_cost * 1.3),  // an small portion bigger to support DPCosts
    CConsts::COIN_SELECTING_METHOD::PRECISE);
  if (!coins_status)
  {
    msg = "Failed in finding coin to spend for voting on adm-pooling(" + CUtils::hash8c(adm_polling_hash) + ")";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // create a transaction for payment
  auto changeback_res = Wallet::getAnOutputAddress(true);
  if (!changeback_res.status)
  {
    msg = "Failed in create changeback address for adm-polling-ballot!";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  CAddressT change_back_address = changeback_res.msg;
  std::vector<TOutput> outputs {
    TOutput{change_back_address, 1, CConsts::OUTPUT_CHANGEBACK},
    TOutput{"TP_BALLOT", ballot_dp_cost, CConsts::OUTPUT_TREASURY}};

  auto trx_template1 = BasicTransactionTemplate {
    spendable_coins,
    outputs,
    static_cast<CMPAIValueT>(ballot_dp_cost * 1.3),  // max DPCost
    0,    // pre calculated dDPCost
    "Payed for adm-pooling ballot cost",
    ballot->getDocHash()};
  auto[tx_status, res_msg1, ballot_payer_trx, dp_cost1] = BasicTransactionHandler::makeATransaction(trx_template1);
  if (!tx_status)
    return {false, res_msg1};

  CLog::log("Signed trx for proposal cost: " + ballot_payer_trx->safeStringifyDoc(true), "app", "info");

  // mark UTXOs as used in local machine
  Wallet::locallyMarkUTXOAsUsed(ballot_payer_trx);
  //  walletHandlerLocalUTXOs.locallyMarkUTXOAsUsed(trxDtl.trx);

  // inser ballot in local db (as a voted polling);
  auto[insert_res, insert_msg] = insertALocalBallot(ballot);
  if (!insert_res)
    return {false, insert_msg};

  // push trx & vote-ballot to Block buffer
  auto[ballot_push_res, ballot_push_msg] = CMachine::pushToBlockBuffer(ballot, ballot_dp_cost);
  if (!ballot_push_res)
  {
    msg = "Failed in push ballot to block buffer adm-polling(" + CUtils::hash8c(ballot->getDocRef()) + ") " + ballot_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[trx_push_res, trx_push_msg] = CMachine::pushToBlockBuffer(ballot_payer_trx, ballot_payer_trx->getDocCosts());
  if (!trx_push_res)
  {
    msg = "Failed in push trx to block buffer adm-polling(" + CUtils::hash8c(ballot_payer_trx->getDocRef()) + ") " + trx_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  return {true, "Your Vote was pushed to Block Buffer"};
}
