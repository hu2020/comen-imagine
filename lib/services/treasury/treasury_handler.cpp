#include "stable.h"

#include "treasury_handler.h"

const QString TreasuryHandler::stbl_treasury = "c_treasury";
const QStringList TreasuryHandler::stbl_treasury_fields = {"tr_id", "tr_cat", "tr_title", "tr_descriptions", "tr_creation_date", "tr_block_hash", "tr_coin", "tr_value"};

TreasuryHandler::TreasuryHandler()
{

}

TimeRange TreasuryHandler::getTreasureIncomesDateRange(const QString cDate)
{
  return CUtils::getACycleRange(cDate, CConsts::TREASURY_MATURATION_CYCLES);
}

std::tuple<QString, QString, CMPAIValueT> TreasuryHandler::calcTreasuryIncomes(
  const QString cDate)
{

  // retrieve the total treasury incomes in last cycle (same as share cycle calculation)
  auto the_range = getTreasureIncomesDateRange(cDate);

  QString complete_query = "";
  if (CConsts::DATABASAE_AGENT == "psql")
  {
    complete_query = "SELECT SUM(tr_value) incomes_ FROM " + stbl_treasury + " WHERE tr_creation_date between '" + the_range.from + "' AND '" + the_range.to + "' ";
  }
  else if (CConsts::DATABASAE_AGENT == "sqlite")
  {
    complete_query = "SELECT SUM(tr_value) incomes_ FROM " + stbl_treasury + " WHERE tr_creation_date between \"" + the_range.from + "\" AND \"" + the_range.to + "\" ";
  }

  QueryRes incomes = DbModel::customQuery(
    "db_comen_general",
    complete_query,
    {"incomes_"});
  CLog::log("calc Treasury Incomes WHERE creation_date between (" + the_range.from + "," + the_range.to + ") -> incomes: " + CUtils::dumpIt(incomes.records));
  CMPAIValueT income_value = 0;
  if (incomes.records[0]["incomes_"].toDouble() > 0)
    income_value = incomes.records[0]["incomes_"].toDouble();

  return {
    the_range.from,
    the_range.to,
    income_value};

}


void TreasuryHandler::insertIncome(
  QString title,
  QString category,
  QString descriptions,
  QString creation_date,
  CMPAIValueT value,
  QString block_hash,
  CCoinCodeT coin)
{
  QueryRes dbl = DbModel::select(
    stbl_treasury,
    {"tr_coin"},
    {{"tr_coin", coin}},
    {{"tr_id", "ASC"}});
  if (dbl.records.size() > 0)
  {
    CLog::log("duplicated treasury insertion: block(" + CUtils::hash8c(block_hash)+ ") title(" + title + ")", "trx", "warning");
    // update the descriptions
    DbModel::update(
      stbl_treasury,
      {{"tr_descriptions", dbl.records[0].value("tr_descriptions").toString() + " " + descriptions}},
      {{"tr_coin", coin}});
    return;
  }

  QVDicT values {
    {"tr_cat", category},
    {"tr_title", title},
    {"tr_descriptions", descriptions},
    {"tr_creation_date", creation_date},
    {"tr_block_hash", block_hash},
    {"tr_coin", coin},
    {"tr_value", QVariant::fromValue(value)}};

  CLog::log("Treasury income(" + CUtils::microPAIToPAI6(value) + " PAIs) because of block(" + CUtils::hash8c(block_hash)+ ") title(" + title + ") values :" + CUtils::dumpIt(values ), "trx", "info");

  DbModel::insert(
    stbl_treasury,
    values);

  return;
}

void TreasuryHandler::donateTransactionInput(
  QString title,
  QString category,
  QString descriptions,
  QString creation_date,
  CMPAIValueT value,
  QString block_hash,
  CCoinCodeT coin)
{

  if (title == "")
    title = "No Title!";

  if (category == "")
    category = "No category!";

  if (descriptions == "")
    descriptions = "No descriptions!";

  if (creation_date == "")
    creation_date = "No creation_date!";

  // retrieve location refLoc is generated
  // let blocks = dagHandler.retrieveBlocksInWhichARefLocHaveBeenProduced(args.refLoc);
  // clog.trx.info(`donate Transaction Input. blocks by refLoc: ${JSON.stringify(blocks)}`);
  // let block = blocks[0];

  // big FIXME: for cloning transactions issue
  insertIncome(
    title,
    category,
    descriptions,
    creation_date,
    value,
    block_hash,
    coin);
}

CMPAIValueT TreasuryHandler::getWaitedIncomes(CDateT cDate)
{
  if (cDate == "")
    cDate = CUtils::getNow();

  cDate = getTreasureIncomesDateRange(cDate).to;

  QueryRes res = DbModel::select(
    stbl_treasury,
    {"tr_value"},
    {{"tr_creation_date", cDate, ">"}});

  CMPAIValueT sum = 0;
  for (QVDicT income: res.records)
    sum += income.value("tr_value").toDouble();

  return sum;
}

QVDRecordsT TreasuryHandler::searchInTreasury(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_treasury,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}
