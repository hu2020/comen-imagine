#ifndef TREASURYHANDLER_H
#define TREASURYHANDLER_H


class TreasuryHandler
{
public:
  TreasuryHandler();

  const static QString stbl_treasury;
  const static QStringList stbl_treasury_fields;

  static TimeRange getTreasureIncomesDateRange(const QString cDate = CUtils::getNow());

  static std::tuple<QString, QString, CMPAIValueT> calcTreasuryIncomes(
    const QString cDate = CUtils::getNow());

  static void donateTransactionInput(
      QString title,
      QString category,
      QString descriptions,
      QString creation_date,
      CMPAIValueT value,
      QString block_hash,
      CCoinCodeT coin);

  static void insertIncome(
    QString title,
    QString category,
    QString descriptions,
    QString creation_date,
    CMPAIValueT value,
    QString block_hash,
    CCoinCodeT coin);

  static CMPAIValueT getWaitedIncomes(CDateT cDate = "");

  static QVDRecordsT searchInTreasury(
    const ClausesT& clauses,
    const QStringList& fields = stbl_treasury_fields,
    const OrderT order = {},
    const uint64_t limit = 0);

};

#endif // TREASURYHANDLER_H
