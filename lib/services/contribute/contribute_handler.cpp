#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/wallet/wallet.h"
#include "lib/block/document_types/document.h"
#include "lib/services/polling/ballot_handler.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/messaging_protocol/graphql_handler.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/document_types/basic_tx_document.h"
#include "lib/block/document_types/dna_proposal_document.h"
#include "lib/services/contracts/loan/loan_contract_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/transactions/basic_transactions/basic_transaction_handler.h"

#include "contribute_handler.h"

ContributeHandler::ContributeHandler()
{

}

QHash<uint32_t, QVDicT> ContributeHandler::getOnchainProposalsList(
  const CAddressT& voter)
{
  // retrieve machine votes
  QVDRecordsT votes = BallotHandler::searchInLocalBallots();
  QV2DicT local_votes_dict {};
  for (QVDicT a_vote: votes)
    local_votes_dict[a_vote.value("lbt_pll_hash").toString()] = a_vote;

  QString complete_query = R"(
    SELECT ppr.ppr_name, ppr.ppr_perform_type, ppr.ppr_votes_counting_method,

    pr.pr_hash, pr.pr_title, pr.pr_descriptions, pr.pr_tags, pr.pr_help_hours, pr.pr_help_level,
    pr.pr_contributor_account, pr.pr_conclude_date,

    pll.pll_hash, pll.pll_ref, pll.pll_start_date, pll.pll_end_date, pll.pll_timeframe, pll.pll_status, pll.pll_ct_done,
    pll.pll_y_count, pll.pll_n_count, pll.pll_a_count,
    pll.pll_y_shares, pll.pll_n_shares, pll.pll_a_shares,
    pll.pll_y_gain, pll.pll_n_gain, pll.pll_a_gain,
    pll.pll_y_value, pll.pll_n_value, pll.pll_a_value


    FROM c_pollings pll
    JOIN c_polling_profiles ppr ON ppr.ppr_name=pll.pll_class
    JOIN c_proposals pr ON pr.pr_hash = pll.pll_ref
  )";

  if (CConsts::DATABASAE_AGENT == "psql")
  {
    complete_query += "WHERE pll.pll_ref_type='Proposal' AND ppr.ppr_name='Basic' ORDER BY pll.pll_start_date ";
  }
  else if (CConsts::DATABASAE_AGENT == "sqlite")
  {
    complete_query += "WHERE pll.pll_ref_type=\"Proposal\" AND ppr.ppr_name=\"Basic\" ORDER BY pll.pll_start_date ";
  }

  QueryRes res = DbModel::customQuery(
    "db_comen_general",
    complete_query,
    {"ppr_name", "ppr_perform_type", "ppr_votes_counting_method",
      "pr_hash", "pr_title", "pr_descriptions", "pr_tags", "pr_help_hours", "pr_help_level",
      "pr_contributor_account", "pr_conclude_date",
      "pll_hash", "pll_ref", "pll_start_date", "pll_end_date", "pll_timeframe", "pll_status", "pll_ct_done",
      "pll_y_count", "pll_n_count", "pll_a_count",
      "pll_y_shares", "pll_n_shares", "pll_a_shares",
      "pll_y_gain", "pll_n_gain", "pll_a_gain",
      "pll_y_value", "pll_n_value", "pll_a_value"},
    0,
    {},
    false,
    false);

  QHash<uint32_t, QVDicT> final_result {};
  uint32_t result_number = 0;
  for (QVDicT a_proposal: res.records)
  {
    uint32_t row_inx = result_number * 10; // 10 rows for each proposal are needed

    // calc potentiasl voter gains
    if (voter != "")
    {
      uint64_t diff = CUtils::timeDiff(a_proposal.value("pll_start_date").toString()).asMinutes;
      auto[yes_gain, no_abstain_gain, latenancy_] = PollingHandler::calculateVoteGain(
        diff,
        diff,
        a_proposal.value("pll_timeframe").toDouble() * 60.0);
      Q_UNUSED(latenancy_);

//      let vGain = pollHandler.calculateVoteGain(diff, diff, a_proposal.pll_timeframe * 60);
      a_proposal["your_yes_gain"] = CUtils::customFloorFloat(yes_gain * 100, 2);
      a_proposal["your_abstain_gain"] = CUtils::customFloorFloat(no_abstain_gain * 100, 2);
      a_proposal["your_no_gain"] = CUtils::customFloorFloat(no_abstain_gain * 100, 2);

    } else {
      a_proposal["your_yes_gain"] = 0.0;
      a_proposal["your_abstain_gain"] = 0.0;
      a_proposal["your_no_gain"] = 0.0;
    }

    CDateT conclude_date = a_proposal.value("pr_conclude_date").toString();
    if (conclude_date == "")
    {
      CDateT polling_end_date = a_proposal.value("pll_end_date").toString();
      CDateT approve_date_ = CUtils::minutesAfter(CMachine::getCycleByMinutes() * 2, polling_end_date);
      conclude_date = CUtils::getCoinbaseRange(approve_date_).from;
    }

    QString win_complementary_text = "";
    QString win_complementary_tip = "";
    if (a_proposal.value("pll_ct_done").toString() == CConsts::YES)
    {
      QVDRecordsT dna_records = DNAHandler::searchInDNA(
        {{"dn_doc_hash", a_proposal.value("pll_ref").toString()}});
      if (dna_records.size() > 0)
      {
        win_complementary_text = " Shares created on " + dna_records[0].value("dn_creation_date").toString();
        win_complementary_tip = "First income on " + CUtils::minutesAfter(
          CConsts::SHARE_MATURITY_CYCLE * CMachine::getCycleByMinutes(),
          dna_records[0].value("dn_creation_date").toString());
      }
    } else{
      win_complementary_tip = "First income (if win) on " + CUtils::minutesAfter(
        CConsts::SHARE_MATURITY_CYCLE * CMachine::getCycleByMinutes(), conclude_date);
    }

    final_result[row_inx] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"proposal_number", result_number + 1},
      {"pr_contributor_account", a_proposal.value("pr_contributor_account")},
      {"pll_status", CConsts::STATUS_TO_LABEL[a_proposal.value("pll_status").toString()]},
      {"ppr_name", a_proposal.value("ppr_name")},
    };


    final_result[row_inx + 1] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"pr_title", a_proposal.value("pr_title")},
      {"pll_ct_done", CConsts::STATUS_TO_LABEL[a_proposal.value("pll_ct_done").toString()]},
      {"the_conclude_date", conclude_date},
      {"ppr_perform_type", a_proposal.value("ppr_perform_type")},
    };

    final_result[row_inx + 2] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"pll_hash", a_proposal.value("pll_hash")},
      {"ppr_votes_counting_method", a_proposal.value("ppr_votes_counting_method")},
    };

    final_result[row_inx + 3] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"pr_descriptions", a_proposal.value("pr_descriptions")},
      {"", a_proposal.value("")},
    };

    CDateT end_y_date;
    if (CConsts::TIME_GAIN == 1)
    {
      end_y_date = CUtils::minutesAfter(a_proposal.value("pll_timeframe").toDouble() * 60, a_proposal.value("pll_start_date").toString());

    } else{
      // test ambient
      TimeByHoursT yes_timeframe_by_minutes = static_cast<uint64_t>(a_proposal.value("pll_timeframe").toDouble() * 60.0);
      CLog::log("yes_timeframe_by_minutes____pll_timeframe" + QString::number(a_proposal.value("pll_timeframe").toDouble()));
      CLog::log("yes_timeframe_by_minutes____" + QString::number(yes_timeframe_by_minutes));
      end_y_date = CUtils::minutesAfter(yes_timeframe_by_minutes, a_proposal.value("pll_start_date").toString());

    }


    final_result[row_inx + 4] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"pr_tags", a_proposal.value("pr_tags")},
      {"pll_start_date", a_proposal.value("pll_start_date")},
      {"pll_y_count", a_proposal.value("pll_y_count")},
      {"pll_y_shares", a_proposal.value("pll_y_shares")},
      {"pll_y_gain", a_proposal.value("pll_y_gain")},
      {"pll_y_value", a_proposal.value("pll_y_value")},
      {"your_yes_gain", a_proposal.value("your_yes_gain")},
    };

    final_result[row_inx + 5] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"pr_help_hours", a_proposal.value("pr_help_hours")},
      {"end_y", end_y_date}, //CUtils::minutesAfter(yes_timeframe * 60, a_proposal.value("pll_start_date").toString())},
      {"pll_a_count", a_proposal.value("pll_a_count")},
      {"pll_a_shares", a_proposal.value("pll_a_shares")},
      {"pll_a_gain", a_proposal.value("pll_a_gain")},
      {"pll_a_value", a_proposal.value("pll_a_value")},
      {"your_abstain_gain", a_proposal.value("your_abstain_gain")},
    };

    final_result[row_inx + 6] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"pr_help_level", a_proposal.value("pr_help_level")},
      {"end_n", a_proposal.value("pll_end_date")},
      {"pll_n_count", a_proposal.value("pll_n_count")},
      {"pll_n_shares", a_proposal.value("pll_n_shares")},
      {"pll_n_gain", a_proposal.value("pll_n_gain")},
      {"pll_n_value", a_proposal.value("pll_n_value")},
      {"your_no_gain", a_proposal.value("your_no_gain")},
    };

    final_result[row_inx + 7] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"proposed_shares", a_proposal.value("pr_help_level").toInt() * a_proposal.value("pr_help_hours").toInt()},
      {"", a_proposal.value("")},
      {"", a_proposal.value("")},
    };

    QString final_status_color, final_status_text;
    if (a_proposal.value("pll_y_value").toUInt() >= a_proposal.value("pll_n_value").toUInt())
    {
      final_status_color = "00ff00";
      final_status_text = "Won (";

    } else {
      final_status_color = "ff0000";
      final_status_text = "Missed (";

    }
    final_status_text += CUtils::sepNum(a_proposal.value("pll_y_value").toUInt() - a_proposal.value("pll_n_value").toUInt()) +" points)";
    final_status_text += win_complementary_text;

    final_result[row_inx + 8] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
      {"final_status_color", final_status_color},
      {"final_status_text", final_status_text},
      {"win_complementary_tip", win_complementary_tip},
    };

    final_result[row_inx + 9] = QVDicT {
      {"pr_hash", a_proposal.value("pr_hash")},
    };

//    a_proposal.pllEndDateAbstainOrNo = utils.minutesAfter(utils.floor(a_proposal.pll_timeframe * 60 * 1.5), a_proposal.pll_start_date);
//    a_proposal.machineBallot = _.has(local_votes_dict, a_proposal.pll_hash) ? local_votes_dict[a_proposal.pll_hash] : null;


    result_number++;
  }

  return final_result;
}


bool ContributeHandler::bindProposalLoanPledge(
  const CDocHashT& loan_ref, // the proposal for which loan is requested
  const CAddressT& the_pledger,
  const CAddressT& the_pledgee,
  const CMPAIValueT loan_principal,
  const double annual_interest,
  const CMPAIValueT repayment_amount,
  const CMPAIValueT repayment_schedule,
  const CAddressT& the_arbiter,
  const CDateT& creation_date)
{
  QString msg;

  if (loan_ref == "")
  {
    msg = "The proposal ref for loan is invalid!";
    CLog::log(msg, "app", "warning");
    return false;
  }

  if (the_pledger == "")
  {
    msg = "The pledger Address " + the_pledger + " is not a valid Bech32 address!";
    CLog::log(msg, "app", "warning");
    return false;
  }
  if (!CCrypto::isValidBech32(the_pledger))
  {
    msg = "The Pledger Address " + the_pledger + " is not a valid Bech32 address!";
    CLog::log(msg, "app", "warning");
    return false;
  }

  if (the_pledgee == "")
  {
    msg = "The Pledgee Address " + the_pledgee + " is empty";
    CLog::log(msg, "app", "warning");
    return false;
  }

  if (!CCrypto::isValidBech32(the_pledgee))
  {
    msg = "The Pledgee Address " + the_pledgee + " is not a valid Bech32 address!";
    CLog::log(msg, "app", "warning");
    return false;
  }

  if (the_arbiter != "")
  {
    if (!CCrypto::isValidBech32(the_arbiter))
    {
      msg = "The arbiter Address " + the_arbiter + " is not a valid Bech32 address!";
      CLog::log(msg, "app", "warning");
      return false;
    }
  }


  // control if signer has permitted to pledge?
  QVDRecordsT addInfo = Wallet::getAddressesInfo({the_pledger});
  if (addInfo.size() != 1)
  {
    msg = "The Invoice Address " + the_pledger + " is not controlled by your wallet!";
    CLog::log(msg, "app", "warning");
    return false;
  }

  QJsonObject addrDtl = CUtils::parseToJsonObj(addInfo[0].value("wa_detail").toString());
  QString pPledge = CConsts::NO;
  for (auto anUnlockerSet: addrDtl.value("uSets").toArray())
    for (auto aSign: anUnlockerSet.toObject().value("sSets").toArray())
      if (aSign.toObject().value("pPledge") == CConsts::YES)
        pPledge = CConsts::YES;

  if (pPledge == CConsts::NO)
  {
    msg = "The Invoice Address(" + the_pledger + ") doesn't permitted to Pledge!";
    CLog::log(msg, "app", "warning");
    return false;
  }

  // validate repayment clauses
  LoanDetails loan_details = LoanContractHandler::calcLoanRepayments(
    loan_principal,
    annual_interest,
    repayment_amount,
    repayment_schedule);
  CLog::log("Loan calculation for proposal in func bindProposalLoanPledge. proposal(" + loan_ref + ")! " + loan_details.dumpMe(), "app", "trace");

  if (!loan_details.m_calculation_status)
  {
    msg = "failed in loan calculation for proposal(" + loan_ref + ")! " + loan_details.dumpMe();
    CLog::log(msg, "app", "warning");
    return false;
  }

  // pledge account
  auto [status, pledged_contract] = GeneralPledgeHandler::doPledgeAddress(
    the_pledger,
    the_pledgee,
    loan_ref, // proposal ref
    loan_principal,
    annual_interest,
    repayment_amount,
    loan_details.m_repayments.size(),
    repayment_schedule,
    0,  // TODO: implement offset
    the_arbiter);

  if (!status) {
    msg = "failed in loan pledge for proposal(" + loan_ref + ")! " + loan_details.dumpMe();
    CLog::log(msg, "app", "warning");
    return false;
  }

  // save pledged contract in db
  return GeneralPledgeHandler::saveDraftPledgedProposal(pledged_contract);
}

std::tuple<bool, QString> ContributeHandler::sendLoanRequest(
  const uint64_t draft_pledge_id,
  const QString pledgee_neighbor_id)
{
  QString msg;

  // control if neighbor exist in db
  QVDRecordsT neightbors = CMachine::getNeighbors("", "", "", pledgee_neighbor_id);
  if (neightbors.size() != 1)
  {
    msg = "The pledgee email " + pledgee_neighbor_id + " doesn't exist in your neighbors email";
    return {false, msg};
  }
  QString neighbor_email = neightbors[0].value("n_email").toString();

  // retrieve draft loan request
  QVDRecordsT draftPledges = GeneralPledgeHandler::searchInDraftPledges({{"dpl_id", QVariant::fromValue(draft_pledge_id).toInt()}});
  if (draftPledges.size() != 1)
  {
    msg = "The loan request " + QString::number(draft_pledge_id) + " doesn't exist in your draft loan requests";
    return {false, msg};
  }
  QVDicT pledge_record = draftPledges[0];

  //retrieve draft proposal
  QVDRecordsT proposals = ProposalHandler::searchInDraftProposals({{"pd_hash", pledge_record.value("dpl_doc_ref").toString()}});
  if (proposals.size()!= 1)
  {
    msg = "The draft proposal " + CUtils::hash8c(pledge_record.value("dpl_doc_ref").toString()) + " doesn't exist in your draft loan requests";
    return {false, msg};
  }
  QVDicT proposal = proposals[0];


  QJsonObject card {
    {"cdType", CConsts::CARD_TYPES::ProposalLoanRequest},
    {"cdVer", "0.0.4"},
    {"proposal", CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(proposal.value("pd_body").toString()).content)},
    {"pledgerSignedPledge", CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(pledge_record.value("dpl_body").toString()).content)},
  };
  CLog::log("About to send loan request to neighbor(" + neighbor_email + ") request details:" + CUtils::serializeJson(card));

  auto[code, body] = GraphQLHandler::makeAPacket({card});

  bool push_res = SendingQHandler::pushIntoSendingQ(
    CConsts::GQL, //sqType
    code,
    body,
    "GQL Loan Request packet(" + CUtils::hash16c(code) + ")",
    {neighbor_email});  //sqReceivers

  if (push_res)
    return {true, "Proposal Loan Request was sent to pledgee"};

  return {false, "Failed in Proposal Loan Request sending to pledgee"};
}



std::tuple<bool, QString> ContributeHandler::signAndPayProposalCosts(
  const CDocHashT& draft_proposal_hash)
{
  QString msg;
  QString stage = CConsts::STAGES::Creating;

  QVDRecordsT drafts = ProposalHandler::searchInDraftProposals(
    {{"pd_hash", draft_proposal_hash}});
  CLog::log("Draft Proposal(s): " + CUtils::dumpIt(drafts), "app", "info");
  if (drafts.size() != 1)
  {
    msg = "The proposal draft(" + draft_proposal_hash + ") does not exist in Draft proposals or over exists!";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  QJsonObject proposal_js = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(drafts[0].value("pd_body").toString()).content);
  CLog::log("Proposal JS: " + CUtils::serializeJson(proposal_js), "app", "info");
  DNAProposalDocument* proposal = new DNAProposalDocument(proposal_js);

  if (proposal->m_doc_length != proposal->safeStringifyDoc(true).length())
  {
    msg = "The proposal length("+CUtils::sepNum(proposal->m_doc_length)+"), is not same as proposal("+CUtils::sepNum(proposal->safeStringifyDoc(true).length())+")";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[cost_status, proposal_dp_cost] = proposal->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    CUtils::getNow());
  if (!cost_status)
    return {false, "failed in proposal doc cost calculation"};


  auto[one_cycle_income, apply_cost] = ProposalHandler::calcProposalApplyCost(
    proposal->m_help_hours,
    proposal->m_help_level,
    CUtils::getNow());
  CMPAIValueT proposalFinalCost = proposal_dp_cost + apply_cost;
  CMPAIValueT needed_PAIs = CUtils::CFloor(proposalFinalCost * 1.7) + proposalFinalCost;

  // create a transaction for payment
  auto changeback_res = Wallet::getAnOutputAddress(true);
  if (!changeback_res.status)
  {
    msg = "Failed in create changeback address for Proposal payment!";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  CAddressT change_back_address = changeback_res.msg;
  std::vector<TOutput> outputs {
    TOutput{change_back_address, 1, CConsts::OUTPUT_CHANGEBACK},
    TOutput{"TP_PROPOSAL", proposalFinalCost, CConsts::OUTPUT_TREASURY}};

  auto[coins_status, coins_msg, spendable_coins, spendable_amount] = Wallet::getSomeCoins(
    needed_PAIs,  // an small portion bigger to support DPCosts
    CConsts::COIN_SELECTING_METHOD::PRECISE);
  if (!coins_status)
  {
    msg = "Failed in finding coin to spend for creating a proposal";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto trx_template = BasicTransactionTemplate {
    spendable_coins,
    outputs,
    static_cast<CMPAIValueT>(needed_PAIs),  // max DPCost
    0,    // pre calculated dDPCost
    "Payed for iPGP binding cost",
    proposal->getDocHash()};

  auto[tx_status, tx_res_msg, cost_payer_trx, dp_cost] = BasicTransactionHandler::makeATransaction(trx_template);
  if (!tx_status)
    return {false, tx_res_msg};

  CLog::log("Signed trx for proposing a DNAproposal: " + cost_payer_trx->safeStringifyDoc(true), "app", "info");



  // push trx & proposal Req to Block buffer
  auto[prop_push_res, prop_push_msg] = CMachine::pushToBlockBuffer(proposal, proposalFinalCost);
  if (!prop_push_res)
  {
    msg = "Failed in push proposal doc to block buffer proposal(" + CUtils::hash8c(proposal->getDocHash()) + ") " + prop_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[trx_push_res, trx_push_msg] = CMachine::pushToBlockBuffer(cost_payer_trx, cost_payer_trx->getDocCosts());
  if (!trx_push_res)
  {
    msg = "Failed in push proposal payer trx to block buffer trx(" + CUtils::hash8c(cost_payer_trx->getDocHash()) + ") " + trx_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // mark UTXOs as used in local machine
  Wallet::locallyMarkUTXOAsUsed(cost_payer_trx);

  return {true, "Your proposal request is pushed to Block buffer"};
}
