#ifndef CONTRIBUTEHANDLER_H
#define CONTRIBUTEHANDLER_H


class ContributeHandler
{
public:
  ContributeHandler();

  static QHash<uint32_t, QVDicT> getOnchainProposalsList(
    const CAddressT& voter = "");

  static bool bindProposalLoanPledge(
    const CDocHashT& loan_ref, // the proposal for which loan is requested
    const CAddressT& the_pledger,
    const CAddressT& the_pledgee,
    const CMPAIValueT loan_principal,
    const double annual_interest,
    const CMPAIValueT repayment_amount,
    const CMPAIValueT repayment_schedule = CConsts::DEFAULT_REPAYMENT_SCHEDULE,
    const CAddressT& the_arbiter = "",
    const CDateT& creation_date = CUtils::getNow());

  static std::tuple<bool, QString> sendLoanRequest(
    const uint64_t draft_pledge_id,
    const QString pledgee_neighbor_id);

  static std::tuple<bool, QString> signAndPayProposalCosts(
    const CDocHashT& draft_proposal_hash);

};

#endif // CONTRIBUTEHANDLER_H
