#include "mainwindow.h"
#include "stable.h"

#include "lib/services/society_rules/society_rules.h"

#include "initialize_node.h"

MainWindow* InitializeNode::s_mw;

InitializeNode::InitializeNode()
{

}

void InitializeNode::setMW(MainWindow* mw)
{
  s_mw = mw;
}

MainWindow* InitializeNode::getMW()
{
  return s_mw;
}

bool InitializeNode::refreshGUI()
{
//  s_mw->refreshGUI();
  return true;
}

bool InitializeNode::maybeInitDAG()
{

  // check if genesis block is created?
  QueryRes existedGenesisBlock = DbModel::select(
    "c_blocks",
    QStringList {"b_id", "b_hash"},     // fields
    {ModelClause("b_type", CConsts::BLOCK_TYPES::Genesis)}, //clauses
    OrderT {{"b_creation_date", "ASC"}, {"b_id", "ASC"}},   // order
    1   // limit
  );
  if (existedGenesisBlock.records.size() > 0)
    return true;

  CMachine::setDAGIsInitialized(false);

  bool status;

  // create Genisis Block
  GenesisBlock genesisBlock = GenesisBlock();
  genesisBlock.initGenesisBlock();

  // init Administrative Configurations History
  SocietyRules::initAdministrativeConfigurationsHistory();

  // init Polling Profiles
  PollingHandler::initPollingProfiles();

  // Initialize Agoras content
  status = AgoraInitializer::initAgoras();
  if (!status)
    CUtils::exiter("AgoraInitializer::initAgoras failed!", 908);

  // initialize registerd iNames
  status = INameHandler::initINames();
  if (!status)
    CUtils::exiter("INameHandler initINames failed!", 908);


  // initialize wiki pages
  WikiHandler::initTmpWikiContent();

  return doesSafelyInitialized();

}

bool InitializeNode::doesSafelyInitialized()
{
  // TODO implement it to controll if all intial document are inserted properly?
  CMachine::setDAGIsInitialized(true);

  // long list of controlls



  KVHandler::setValue("MACHINE_AND_DAG_ARE_SAFELY_INITIALIZED", CConsts::YES);

  return true;
}
