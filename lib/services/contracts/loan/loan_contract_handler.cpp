#include "stable.h"

#include "loan_contract_handler.h"


QString LoanDetails::dumpMe()
{
  QString out = "";
  out += "\n Calculation status: " + CUtils::dumpIt(m_calculation_status);
  out += "\n Interest gain: " + QString::number(m_interest_gain);
  out += "\n Repayments count: " + QString::number(m_repayments.size());
  out += "\n Loan principal: " + CUtils::microPAIToPAI6(m_loan_principal);
  out += "\n Total repayment amount(principlas + interest): " + CUtils::microPAIToPAI6(m_total_repayment_amount);
  return out;
};


LoanContractHandler::LoanContractHandler()
{

}

/**
 * @brief LoanContractHandler::calcLoanRepayments
 * @param principal
 * @param annual_interest
 * @param repayment_amount
 * @param repayment_schedule
 * @param max_repayments_count
 * @param precision
 * return LoanDetails
 */
LoanDetails LoanContractHandler::calcLoanRepayments(
  CMPAIValueT principal,
  float annual_interest,
  CMPAIValueT repayment_amount,
  uint32_t repayment_schedule,
  uint32_t max_repayments_count,
  uint32_t precision)
{
  std::vector<RepaymentDetails> repayments {};
  float interest_gain = 0.0;
  CMPAIValueT total_repayment_amount = 0;

  CMPAIValueT org_principal = principal;    // the PAIs are loaned
  if (org_principal < 1)
  {
    CLog::log("The loan principal is invalid! interst(" + QString::number(org_principal) + ")", "app", "error");
    return {false, repayments, interest_gain, org_principal, total_repayment_amount};
  }

  annual_interest = annual_interest / 100;
  if (!(annual_interest > 0)) {
    CLog::log("The loan interest is invalid! interst(" + QString::number(annual_interest) + ")", "app", "error");
    return {false, repayments, interest_gain, org_principal, total_repayment_amount};
  }

  if (repayment_amount < 1)
  {
    CLog::log("The loan repayment amount is invalid! repayment amount(" + QString::number(repayment_amount) + ")", "app", "error");
    return {false, repayments, interest_gain, org_principal, total_repayment_amount};
  }

  // how many times do repayment in a year in imagine = 365*2 which means 2 time in day
  if (repayment_schedule < 1)
  {
    CLog::log("The loan repayment schedule is invalid! repayment schedule(" + QString::number(repayment_schedule) + ")", "app", "error");
    return {false, repayments, interest_gain, org_principal, total_repayment_amount};
  }

  if (max_repayments_count < 1)
  {
    CLog::log("The loan max Repayments Count is invalid! repayment count(" + QString::number(max_repayments_count) + ")", "app", "error");
    return {false, repayments, interest_gain, org_principal, total_repayment_amount};
  }

  uint32_t inx = 1;


  principal = org_principal;
  CMPAISValueT new_principal;
  CMPAIValueT period_interest;

  interest_gain = (annual_interest / repayment_schedule);
  while ((principal > repayment_amount) && (inx < max_repayments_count))
  {
    period_interest = CUtils::customFloorFloat(interest_gain * principal, precision);
    new_principal = CUtils::customFloorFloat(principal - repayment_amount + period_interest, precision);
    total_repayment_amount += repayment_amount;
    repayments.emplace_back(RepaymentDetails{
      inx,
      principal,
      repayment_amount,
      period_interest,
      repayment_amount - period_interest,
      new_principal});
    principal = new_principal;
    inx++;
  }

  if (principal > repayment_amount)
  {
    CLog::log("By stated Repayment per Cycle, you can not repay back your dept in " +
      QString::number(CConsts::DEFAULT_MAX_REPAYMENTS) + " Repayments! Repayment(" + CUtils::microPAIToPAI6(repayment_amount) +
      ") org_principal(" + CUtils::microPAIToPAI6(org_principal) + ") ", "app", "error");
    return {false, repayments, interest_gain, org_principal, total_repayment_amount};
  }

  // add the last repayment
  period_interest = CUtils::customFloorFloat(interest_gain * principal, precision);
  CMPAIValueT Lastrepayment_amount = principal + period_interest;
  total_repayment_amount += Lastrepayment_amount;
  new_principal = CUtils::customFloorFloat(principal - Lastrepayment_amount + period_interest);
  if (new_principal < 0)
    new_principal = 0;
  repayments.emplace_back(RepaymentDetails{
    inx,
    principal,
    Lastrepayment_amount,
    period_interest,
    Lastrepayment_amount - period_interest,
    new_principal});

  return {true, repayments, interest_gain, org_principal, total_repayment_amount};
}
