#ifndef LOANCONTRACTHANDLER_H
#define LOANCONTRACTHANDLER_H

class RepaymentDetails
{
public:
  uint32_t m_repayment_number = 0;
  CMPAIValueT m_balance = 0;
  CMPAIValueT m_repayment_amount = 0;
  CMPAIValueT m_paid_interest = 0;
  CMPAIValueT m_paid_principal = 0;
  CMPAISValueT m_new_balance = 0;
};

class LoanDetails
{
public:
  bool m_calculation_status = false;
  std::vector<RepaymentDetails> m_repayments {};
  double m_interest_gain = 0.05;
  CMPAIValueT m_loan_principal = 0;
  CMPAIValueT m_total_repayment_amount = 0; // principlas + interest

  QString dumpMe();
};

class LoanContractHandler
{
public:
  LoanContractHandler();

  static LoanDetails calcLoanRepayments(
    CMPAIValueT principal,
    float annual_interest,
    CMPAIValueT repayment_amount,
    uint32_t repayment_schedule = CConsts::DEFAULT_REPAYMENT_SCHEDULE,
    uint32_t max_repayments_count = CConsts::DEFAULT_MAX_REPAYMENTS,
    uint32_t precision = 0);

};

#endif // LOANCONTRACTHANDLER_H
