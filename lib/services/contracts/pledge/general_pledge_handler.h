#ifndef GENERALPLEDGEHANDLER_H
#define GENERALPLEDGEHANDLER_H

class DNAProposalDocument;
class PledgeDocument;
class ClosePledgeDocument;

class GeneralPledgeHandler
{
public:
  GeneralPledgeHandler();

  static const QString stbl_pledged_accounts;
  static const QStringList stbl_pledged_accounts_fields;

  static const QString stbl_machine_draft_pledges;
  static const QStringList stbl_machine_draft_pledges_fields;

  static const QString stbl_machine_onchain_contracts;

  static GRecordsT getPledgedAccounts(
    QString cDate = CUtils::getNow(),
    const bool& onlyActives = false);

  static QVDRecordsT searchInPledgedAccounts(
    const ClausesT& clauses,
    const QStringList& fields = stbl_pledged_accounts_fields,
    const OrderT& order = {},
    const int& limit = 0);

  static QVDRecordsT searchInDraftPledges(
    const ClausesT& clauses,
    const QStringList& fields = stbl_machine_draft_pledges_fields,
    const OrderT& order = {},
    const int& limit = 0);

  static bool deleteDraftPledge(
    const ClausesT& clauses);

  static std::tuple<bool, uint32_t, QString> validatePledgerSignedRequest(
    const DNAProposalDocument* proposal,
    const PledgeDocument* pledge,
    QString stage,
    QString cDate);

  static std::tuple<bool, QString, QString> recognizeSignerTypeInfo(
    const PledgeDocument* pledge,
    const CAddressT& signer_address = "");

  static std::tuple<bool, QString, QString> recognizeSignerTypeInfo(
    Document* pledge,
    const CAddressT& signer_address = "");

  static bool addContractToDb(
    const QString& live_contract_type,
    const QString& live_contract_class,
    const QString& live_contract_ref_hash,
    const QString& live_contract_descriptions,
    const QString& live_contract_body);

  static bool insertAPledge(const Block& block, const PledgeDocument* pledge);
  static bool activatePledge(const Block& block, const PledgeDocument* pledge);
  static bool removePledgeBecauseOfPaymentsFail(const CDocHashT& pledge_hash);
  static bool reOpenPledgeBecauseOfPaymentsFail(const CDocHashT& pledge_hash);

  static bool pledgerSignsPledge(PledgeDocument* pledge_document);

  static std::tuple<bool, PledgeDocument*> doPledgeAddress(
    const CAddressT& pledger_address,
    const CAddressT& pledgee_address,
    const CDocHashT& proposal_ref,
    const CMPAIValueT principal,
    const double annual_interest,
    const CMPAIValueT repayment_amount,
    const uint64_t repayments_number,

    const uint64_t repayment_schedule = CConsts::DEFAULT_REPAYMENT_SCHEDULE,
    const uint64_t repayment_offset = 0,
    const CAddressT& arbiter_address = "",
    const CAddressT& document_type = CConsts::DOC_TYPES::Pledge,
    const CAddressT& document_class = CConsts::PLEDGE_CLASSES::PledgeP,
    const CDateT& pledger_sign_date = CUtils::getNow(),
    const CDateT& creation_date = CUtils::getNow());

  static bool saveDraftPledgedProposal(
    PledgeDocument* pledge_document,
    const QString& mp_code = CMachine::getSelectedMProfile());

  static std::tuple<bool, bool> handleReceivedProposalLoanRequest(
    const QString& sender,
    const QJsonObject& payload,
    const QString& connection_type,
    const CDateT& receive_date);

  static std::tuple<bool, QString> pledgeeSignsPledge(PledgeDocument* pledge);

  static bool createAndRecordPPTBundle(
    DNAProposalDocument* proposal,
    PledgeDocument* pledge,
    BasicTxDocument* proposalPayerTrx,
    BasicTxDocument* pledgeDocPayerTrx,
    const CDateT& creation_date = CUtils::getNow());

  static std::tuple<bool, QString> pledgeeSignsProposalLoanRequestBundle(
    DNAProposalDocument* proposal,
    PledgeDocument* pledge);




  //  -  -  -  close pledge part
  static bool doApplyClosingPledge(
    const Block& block,
    const ClosePledgeDocument* pledge);

  static QString renderPledgeDocumentToHTML(const QJsonObject& Jpledge);

};

#endif // GENERALPLEDGEHANDLER_H
