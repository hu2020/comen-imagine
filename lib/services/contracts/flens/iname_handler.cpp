#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/document_types/iname_documents/iname_reg_document.h"
#include "lib/block/document_types/iname_documents/iname_bind_document.h"
#include "lib/block/block_types/block_floating_vote/floating_vote_block.h"
#include "reserved_inames.h"
#include "lib/wallet/wallet.h"
#include "lib/services/collisions/collisions_handler.h"

#include "iname_register_handler.cpp"
#include "iname_binder.cpp"
#include "iname_messaging.cpp"

#include "iname_handler.h"

bool FleNS::setByDict(const QVDicT &values)
{
  if (values.value("doc_hash", "").toString() != "")
    m_doc_hash = values.value("doc_hash", "").toString();

  if (values.value("iname", "").toString() != "")
    m_iname = values.value("iname", "").toString();

  if (values.value("owner", "").toString() != "")
    m_owner = values.value("owner", "").toString();
  if (values.value("doc_creation_date", "").toString() != "")
    m_doc_creation_date = values.value("doc_creation_date", "").toString();
  if (values.value("block_creation_date", "").toString() != "")
    m_block_creation_date = values.value("block_creation_date", "").toString();
  if (values.value("iname_is_setteled", "").toString() != "")
    m_iname_is_setteled = values.value("iname_is_setteled", false).toBool();
  return true;
}





const QString INameHandler::stbl_iname_records = "c_iname_records";
const QStringList INameHandler::stbl_iname_records_fields = {"in_doc_hash", "in_name", "in_hash", "in_owner", "in_is_settled", "in_register_date"};

const QString INameHandler::stbl_machine_iname_records = "c_machine_iname_records";
const QStringList INameHandler::stbl_machine_iname_records_fields = {"imn_id", "imn_mp_code", "imn_doc_hash", "imn_name", "imn_hash", "imn_owner", "imn_info", "imn_register_date"};

const QString INameHandler::stbl_iname_bindings = "c_iname_bindings";
const QStringList INameHandler::stbl_iname_bindings_fields = {"nb_doc_hash", "nb_in_hash", "nb_bind_type", "nb_conf_info", "nb_title", "nb_comment", "nb_status", "nb_creation_date"};

const QString INameHandler::stbl_machine_iname_bindings = "c_machine_iname_bindings";


QString INameHandler::normalizeIName(const QString inp)
{
   // it is tooo strict. later can be a more free even unicode and smiley domains
  //    return inp.replace(/([^a-z0-9-_@]+)/gi, '-').toLowerCase();
  QRegularExpression rx("([^a-z0-9-_@]+)");
  inp.toLower().replace(rx, "");
  return inp;
}

QString INameHandler::generateINameHash(const QString& iname)
{
  QString hash = CCrypto::keccak256(normalizeIName(iname));
  return hash;
}


bool INameHandler::initINames()
{

  // register iName imagine & hu
//  std::make_unique(reserved_inames);

  std::vector<std::string>::iterator it;
  auto names = ReservedINames::s_reserved_inames;
  std::sort (names.begin(), names.end());
  it = std::unique(names.begin(), names.end());
  names.resize(std::distance(names.begin(), it));

  for (std::string aName : names) {
    QString iname_hash = generateINameHash(QString::fromStdString(aName));
    FleNS flens(QVDicT{
      {"doc_hash", iname_hash}, // dummy document hash
      {"iname_hash", iname_hash},
      {"iname", QString::fromStdString(aName)},
      {"owner", CConsts::HU_INAME_OWNER_ADDRESS},
      {"block_creation_date", CMachine::getLaunchDate()},
      {"iname_is_setteled", true}
    });
    recordINameInDAG(flens);
  }

  return true;
}


BindingInfo INameHandler::searchForINamesControlledByWallet(
  QString mp_code,
  const bool& need_bindings_info_too)
{
  if (mp_code == "")
    mp_code = CMachine::getSelectedMProfile();

  auto[wallet_addresses, details] = Wallet::getAddressesList(mp_code, {"wa_address", "wa_mp_code"});
  Q_UNUSED(details);
  QStringList addresses;
  QVDicT map_address_to_profile = {};
  for (QVDicT an_address_info : wallet_addresses) {
      addresses.append(an_address_info.value("wa_address").toString());
      map_address_to_profile.insert(an_address_info.value("wa_address").toString(), an_address_info.value("wa_mp_code").toString());
  }

  BindingInfo res = searchRegisteredINames(
    {{"in_owner", addresses, "IN"}},
    stbl_iname_records_fields,
    {{"in_name", "ASC"}},
    0,
    need_bindings_info_too);

  res.m_map_address_to_profile = map_address_to_profile;
  return res;
}

bool INameHandler::removeINameByHash(const QString& iName_hash)
{
  //sceptical test
  QueryRes exist = DbModel::select(
    stbl_iname_records,
    {"in_hash"},
    {{"in_hash", iName_hash}});

  if (exist.records.size() != 1)
  {
    CLog::log("Try to delete iName strange result! " + CUtils::dumpIt(exist.records), "sec", "error");
    return false;
  }

  CLog::log("Removing iName(" + CUtils::hash8c(iName_hash) + " / " + exist.records[0].value("in_name").toString() + ") because of Failed in payment" , "app", "warning");
  DbModel::dDelete(
    stbl_iname_records,
    {{"in_hash", iName_hash}});

  return true;
}

bool INameHandler::removeINameByName(const QString& iName)
{
  return removeINameByHash(generateINameHash(iName));
}

std::tuple<bool, QString, QVDRecordsT, BindingInfo> INameHandler::getMachineControlledINames(
  ClausesT clauses,
  const QStringList& fields,
  const OrderT& order,
  const bool& need_bindings_info_too,
  const bool& needs_pgp_key,
  const QString& mp_code)
{
  QString msg;
  clauses.push_back({"imn_mp_code", mp_code});

  QueryRes res = DbModel::select(
    stbl_machine_iname_records,
    fields,
    clauses,
    order);
  if (res.records.size() == 0)
  {
    msg = "There is no iName-Addresses controlled by this machine";
    CLog::log(msg, "app", "warning");
    return {false, msg, {}, {}};
  }

  if (!need_bindings_info_too)
    return {true, "", res.records, {}};

  BindingInfo bindings_info = prepareBindingsDict(
    {},
    needs_pgp_key);
  if (!bindings_info.m_status)
  {
    msg = "Failed in preparing PGP bindings dic";
    CLog::log(msg, "app", "error");
    return {false, msg, {}, {}};
  }

  return {true, "", res.records, bindings_info};
}
