#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/services/society_rules/society_rules.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/document_types/basic_tx_document.h"
#include "lib/services/collisions/collisions_handler.h"
#include "lib/block/document_types/iname_documents/iname_reg_document.h"
#include "lib/block/block_types/block_floating_vote/floating_vote_block.h"
#include "lib/transactions/basic_transactions/basic_transaction_handler.h"

#include "iname_handler.h"



bool INameHandler::loopINamesSettlement()
{
  QString thread_prefix = "iname_settlement_treatment_";
  QString thread_code = QString::number((quint64)QThread::currentThread(), 16);

  while (CMachine::shouldLoopThreads())
  {
    CMachine::reportThreadStatus(thread_prefix, thread_code, CConsts::THREAD_STATE::RUNNING);
    maybeINameSettlement();

    CMachine::reportThreadStatus(thread_prefix, thread_code, CConsts::THREAD_STATE::SLEEPING);
    std::this_thread::sleep_for(std::chrono::seconds(CMachine::getINamesSettlementGap()));
  }

  CMachine::reportThreadStatus(thread_prefix, thread_code, CConsts::THREAD_STATE::STOPPED);
  CLog::log("Gracefully stopped thread(" + thread_prefix + thread_code + ") of loop iName Settlement");
  return true;
}

bool INameHandler::maybeINameSettlement()
{
  QString msg;

  // find unsettled iNames which is registered more than 12 hours ago
  BindingInfo toBeSetteled = searchRegisteredINames(
    {{"in_is_settled", CConsts::NO},
    {"in_register_date", CUtils::minutesBefore(CMachine::getCycleByMinutes()), "<"}});

  if (!toBeSetteled.m_status)
    return false;

  if (toBeSetteled.m_records.size() > 0)
  {
    for (QVDicT aRec: toBeSetteled.m_records)
    {
      auto[has_collision, winer_doc, winer_score] = CollisionsHandler::makeCollisionReport(aRec.value("in_hash").toString());
      if (!has_collision)
        winer_doc = aRec.value("in_doc_hash").toString();

      if (winer_doc != "")
      {
        // do settle

        // retrieve winer doc proper info
        auto[status, document, doc_index, merkle_verify, doc_ext_info] = DAG::retrieveDocByDocHash(winer_doc);
        if (!status)
        {
          msg = "iNameSettlement couldn't find doc(" + CUtils::hash8c(winer_doc) + ")";
          CLog::log(msg, "sec", "error");
          continue;
        }

        if (document.keys().size() == 0)
        {
          msg = "iNameSettlement the doc(" + CUtils::hash8c(winer_doc) + ") was empty";
          CLog::log(msg, "sec", "error");
          continue;
        }

        QVDicT updates {
          {"in_is_settled", CConsts::YES},
          {"in_owner", document.value("iNOwner").toString()},
          {"in_doc_hash", document.value("dHash").toString()}};
        CLog::log("iNameSettlement update values " + CUtils::dumpIt(updates), "app", "trace");

        DbModel::update(
          stbl_iname_records,
          updates,
          {{"in_name", document.value("iName").toString()},
          {"in_hash", generateINameHash(document.value("iName").toString())}});
      }
    }
  }
}

bool INameHandler::recordINameInDAG_(
  const Block& block,
  const INameRegDocument* flens,
  const bool inIsSettled)
{
  QString iName = normalizeIName(flens->m_iname_string);
  CDocHashT iNameHash = generateINameHash(iName);
  QVDicT values {
    {"in_doc_hash", flens->getDocHash()},
    {"in_name", flens->m_iname_string},
    {"in_hash", iNameHash},
    {"in_owner", flens->m_iname_owner},
    {"in_register_date", block.m_block_creation_date},
    {"in_is_settled", (inIsSettled ? "Y" : "N")}
  };
  CLog::log("Registering iName(" + iName + ") in iName DAG values: " + CUtils::dumpIt(values), "app", "trace");
  DbModel::insert(
    stbl_iname_records,
    values);

  return maybeAddToMachineControlled(
    iName,
    flens->getDocHash(),
    flens->m_iname_owner,
    block.m_block_creation_date);

}


bool INameHandler::recordINameInDAG(
  const Block& block,
  const INameRegDocument* flens)
{

  auto[already_registerd, record_info] = alreadyRegistered(flens->m_iname_string);
  if (!already_registerd)
    return recordINameInDAG_(block, flens);

  CDocHashT clCollisionRef = generateINameHash(flens->m_iname_string);
  CLog::log("collision the iName(" + flens->m_iname_string + "), iNameHash(" + CUtils::hash8c(clCollisionRef) + ") already placed in DAG! " + CUtils::dumpIt(record_info), "app", "error");

  CAddressT backer_address = CMachine::getBackerAddress();

  // first controls if the already recorded iName in table iname_records is logged too?
  QVDRecordsT dbl = CollisionsHandler::getLoggedDocs(record_info.value("in_doc_hash").toString());
  if (dbl.size() == 0)
  {
    CLog::log("logging collision an recorded iName(" + flens->m_iname_string + ")", "app", "trace");
    // retrieve block info to logging it
    auto[existed_blocks, map_] = DAG::getWBlocksByDocHash({record_info.value("in_doc_hash").toString()});
    Q_UNUSED(map_);
    for (QVDicT a_block_record: existed_blocks)
    {
      // record the log of existed block. later if there will collisions in next 12 hours, machine will use these l
      CollisionsHandler::logACollision(
        backer_address,
        clCollisionRef,
        a_block_record.value("b_hash").toString(),
        record_info.value("in_doc_hash").toString(),
        a_block_record.value("b_creation_date").toString(),
        a_block_record.value("b_receive_date").toString());
    }
  }

  // then logging newly received doc too
  dbl = CollisionsHandler::getLoggedDocs(flens->getDocHash());
  if (dbl.size() == 0)
  {
    // record the log of newly received block. later if there will collisions in next 12 hours, machine will use these l
    CollisionsHandler::logACollision(
      backer_address,
      clCollisionRef,
      block.getBlockHash(),
      flens->getDocHash(),
      block.m_block_creation_date,
      CUtils::getNow());
  }

  // then preparing floating vote for iName collision & broadcasting to neighbors
  if (DAG::isDAGUptodated() && CUtils::isYoungerThan2Cycle(block.m_block_creation_date))
  {
    auto[status, floating_vote_block] = INameHandler::createFleNSVoteBlock(
      clCollisionRef,
      block.getBlockHash());  // as an uplink, sice floating vote must be attached to block which are voting for.

    CLog::log("Create FleNS Vote Block res: " + CUtils::dumpIt(floating_vote_block), "app", "trace");
    if (!status)
    {
      if (floating_vote_block != nullptr)
        delete floating_vote_block;
      return false;
    }


    if (floating_vote_block == nullptr)
    {
      CLog::log("error in create FleNSVoteBlock: " + CUtils::dumpIt(floating_vote_block), "app", "error");
      return false;
    }

    // send floating vote for iName collision
    QString msg = "Broadcasting fVote on FleNS collision iName(" + flens->m_iname_string + ") block(" + CUtils::hash8c(floating_vote_block->getBlockHash()) +")";
    CLog::log(msg, "app", "trace");

    bool pushRes = SendingQHandler::pushIntoSendingQ(
      CConsts::BLOCK_TYPES::FVote,
      floating_vote_block->getBlockHash(),
      floating_vote_block->safeStringifyBlock(false),
      msg);

    CLog::log("iName fVote block pushRes(" + CUtils::dumpIt(pushRes) + ")");

    return true;
  }

  return false;
}


bool INameHandler::recordINameInDAG(const FleNS &flens)
{
  QString in_hash = generateINameHash(flens.m_iname);
  QVDicT values {
    {"in_doc_hash", flens.getDocHash()},
    {"in_name", flens.m_iname},
    {"in_owner", flens.m_owner},
    {"in_register_date", flens.m_block_creation_date},
    {"in_is_settled", flens.m_iname_is_setteled ? CConsts::YES : CConsts::NO}
  };
  CLog::log("Registering iName(" + flens.m_iname + ") in DAG values:" + CUtils::dumpIt(values));
  DbModel::insert(
    stbl_iname_records,
    values
  );

  return maybeAddToMachineControlled(
    flens.m_iname,
    flens.getDocHash(),
    flens.m_owner,
    CUtils::getNow());
}






bool INameHandler::maybeAddToMachineControlled(
  const QString& iname,
  const CDocHashT& registering_doc_hash,
  const CAddressT& iname_owner,
  const CDateT& register_date)
{

  // if the iName is controlled by machine-wallet mProfiles, add it to local db too
  BindingInfo machine_controlled_inames = searchForINamesControlledByWallet(CConsts::ALL);
  CLog::log("searchFor INamesControlledByWallet.records" + CUtils::dumpIt(machine_controlled_inames.m_records), "app", "trace");
  CLog::log("searchFor INamesControlledByWallet.mapAddressToProfile" + CUtils::dumpIt(machine_controlled_inames.m_map_address_to_profile), "app", "trace");

  CDocHashT iname_hash = CCrypto::convertTitleToHash(iname);
  QStringList iname_hashes;
  for (QVDicT a_record: machine_controlled_inames.m_records)
    iname_hashes.append(a_record.value("in_hash").toString());

  if (iname_hashes.contains(iname_hash))
  {
    QueryRes dbl = DbModel::select(
      stbl_machine_iname_records,
      {"imn_hash"},
      ClausesT {{"imn_hash", iname_hash}});
    if (dbl.records.size() == 0)
    {
      // insert
      QVDicT values {
        {"imn_mp_code", machine_controlled_inames.m_map_address_to_profile.value(iname_owner)},
        {"imn_doc_hash", registering_doc_hash},
        {"imn_name", iname},
        {"imn_hash", iname_hash},
        {"imn_owner", iname_owner},
        {"imn_info", CUtils::getANullStringifyedJsonObj()},
        {"imn_register_date", register_date}
      };
      CLog::log("insert c_machine_iname_records.values" + CUtils::dumpIt(values), "app", "trace");
      DbModel::insert(
        stbl_machine_iname_records,
        values);
    }
  }

  return true;
}




BindingInfo INameHandler::searchRegisteredINames(
  const ClausesT& clauses,
  QStringList fields,
  const OrderT& order,
  const int limit,
  const bool &need_bindings_info_too)
{
  BindingInfo bInfo;
  QueryRes res = DbModel::select(
    stbl_iname_records,
    fields,
    clauses,
    order,
    limit);
  if (res.records.size() == 0)
  {
    bInfo.m_status = true;
    bInfo.m_msg = "There is no registered iName!";
    return bInfo;
  }
  bInfo.m_records = res.records;

  if (!need_bindings_info_too)
  {
    bInfo.m_status = true;
    bInfo.m_msg = QString("There are %1 registered iName!").arg(res.records.size());
    return bInfo;
  }

  return prepareBindingsDict(clauses, false);
}

/**
 *
 * @param {*} address
 * takes address and based on holder shares and the number of already registered iNames returns result
 * return {shares, allowed_inames, already_used, available}
 */
std::tuple<DNAShareCountT, uint32_t, uint32_t, uint32_t> INameHandler::ownerRegisteredRecords(const CAddressT& address)
{
  auto[total_shares_, share_amount_per_holder, holdersOrderByShares_] = DNAHandler::getSharesInfo();
  Q_UNUSED(total_shares_);
  Q_UNUSED(holdersOrderByShares_);
  if (!share_amount_per_holder.contains(address))
  {
    CLog::log("The account has no shares at all! address(" + address + ")", "app", "trace");
    return {0, 0, 0, 0};
  }

  DNAShareCountT shares = share_amount_per_holder[address];

  QueryRes res = DbModel::select(
    stbl_iname_records,
    stbl_iname_records_fields,
    {{"in_owner", address}});
  uint32_t already_used = res.records.size();
  // this formula will not work well after 7 years when the share going to amortization!
  // TODO: enhance the formul, or even better after 7 years we do not need at all this limitation
  uint32_t allowed = CUtils::CFloor(shares / CConsts::SHARES_PER_INAME);

  return {
    shares,
    allowed,
    already_used,
    allowed - already_used
  };
}

/**
 * @brief alreadyRegistered
 * @param iname
 * @return {already_registerd, record_info}
 */
std::tuple<bool, QVDicT> INameHandler::alreadyRegistered(const QString& iname)
{

  QueryRes res = DbModel::select(
    stbl_iname_records,
    {"in_name"},
    {{"in_name", normalizeIName(iname), "ILIKE"}});

  if (res.records.size() == 0)
    return {false, QVDicT{}};

  return {true, res.records[0]};
}

bool INameHandler::isAcceptableIName(const QString& iname)
{
  /**
   * dummy simple way to avoid registering general domain names (e.g. google.com)
   * later, after finding a way to authorize canonical domain name owner can activate it
   * the ":" strictly prohobited, because used for port number seperating also / \ and |
   */

  if (iname.contains(":") || (iname.contains(".") && !iname.contains("@")))
    return false;

  return true;
}

std::tuple<bool, Block*> INameHandler::createFleNSVoteBlock(
  const CDocHashT& collision_ref,
  const CBlockHashT& block_hash)
{
  auto[has_collision, the_collisioned_records] = CollisionsHandler::hasCollision(collision_ref);
//  let collisions = collisionsHandler.hasCollision({ collision_ref });
  CLog::log("collisions Handler has Collision(" + CUtils::dumpIt(has_collision) + "), collisions: " + CUtils::dumpIt(the_collisioned_records), "app", "warning");
  if (!has_collision)
  {
    CLog::log("here is no collision for FleNS(" + CUtils::hash8c(collision_ref) + ")" , "app", "warning");
    return {true, nullptr};
  }

  auto[status_sus, tmp_block] = FloatingVoteBlock::createFVoteBlock(
    block_hash,  // uplink
    CConsts::FLOAT_BLOCKS_CATEGORIES::FleNS,  // bCat
    CollisionsHandler::convertQVDRecordsToJson(the_collisioned_records), // vote Data
    CUtils::getNow());
  ;

  CLog::log("Creating FleNS fV Block res: " + CUtils::dumpIt(tmp_block), "app", "trace");
  return {status_sus, tmp_block};
}

CMPAIValueT INameHandler::getPureINameRegCost(const QString& iname)
{
  CMPAIValueT the_cost = 0;
  if (iname.contains("@") && (CUtils::isAValidEmailFormat(iname)))
  {
    // the email addresses are most cheaper than simple names
    the_cost += CConsts::INAME_UNIT_PRICE_EMAIL;
  } else {
    the_cost += CConsts::INAME_UNIT_PRICE_NO_EMAIL;
  }

  auto[x_, y_, len_factor, rev_gain_] = CUtils::calcLog(iname.length(), CConsts::INAME_THRESHOLD_LENGTH, 1);
  Q_UNUSED(x_);
  Q_UNUSED(y_);
  Q_UNUSED(rev_gain_);
  if (len_factor < 1)
    len_factor = 1;
  the_cost *= len_factor;

  return CUtils::CFloor(the_cost);
}

std::tuple<bool, CMPAIValueT, QString> INameHandler::estimateINameRegReqCost(
  const QString& iname,
  const QString& stage,
  const CDateT& cDate)
{
  if (!isAcceptableIName(iname))
  {
    return {false, 0, "The iName(" + iname + ") is not acceptable" };
  }

  DocLenT dLen = 2000; // adummy price for document itself
  CMPAIValueT pure_cost = INameHandler::getPureINameRegCost(iname);
  CMPAIValueT the_cost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(
      CConsts::DOC_TYPES::INameReg,
      dLen,
      CConsts::INAME_CLASESS::NoDomain,
      cDate);


  the_cost += pure_cost;

  if (stage == CConsts::STAGES::Creating)
    the_cost = the_cost * CMachine::getMachineServiceInterests(
      CConsts::DOC_TYPES::INameReg,
      CConsts::INAME_CLASESS::NoDomain,
      dLen);

  return {true, CUtils::CFloor(the_cost), "Done"};
}

std::tuple<bool, QString> INameHandler::addToPreRecordedINames(QString iname)
{
  QString pre_recorded_inames = KVHandler::getValue("PRE_RECORDED_INAMES");
  QStringList pre_recorded_inames_list {};
  if (pre_recorded_inames != "")
    pre_recorded_inames_list = pre_recorded_inames.split(",");
  if (pre_recorded_inames_list.contains(iname))
  {
    return {false, "iName (" + iname + ") already registerd by you & placed in buffer to broadcast!"};
  }

  pre_recorded_inames_list.push_back(iname);
  KVHandler::upsertKValue("PRE_RECORDED_INAMES", pre_recorded_inames_list.join(","));
  return {true, "iName (" + iname + ") was added to records!"};
}

std::tuple<bool, QString, INameRegDocument*> INameHandler::prepareINameRegRequest(
  QString iname,
  const CAddressT& iname_owner,
  const CDateT& cDate)
{
  QString msg;
  iname = normalizeIName(iname);   // TODO: improve it to accept unicode domain

  auto[shares, allowed_inames, already_used, availables_count] = ownerRegisteredRecords(iname_owner);
  if (availables_count < 1)
  {
    msg = "Account (" + CUtils::shortBech16(iname_owner) + ") has " + CUtils::sepNum(shares)+ " shares, ";
    msg += "\nso can register " + CUtils::sepNum(allowed_inames)+ " iName(s). \n(each iName needs " + CUtils::sepNum(CConsts::SHARES_PER_INAME)+ " shares)";
    if (already_used > 0)
        msg += "you alresdy registered " + CUtils::sepNum(already_used)+ " iName(s)";
    return {false, msg, nullptr};
  }
  auto[already_registerd, record_info] = alreadyRegistered(iname);
  if (already_registerd)
  {
    msg = "iName (" + iname + ") is already registerd!";
    CLog::log(msg + CUtils::dumpIt(record_info), "app", "warning");
    return {false, "iName (" + iname + ") is already registerd!", nullptr};
  }

  QString pre_recorded_inames = KVHandler::getValue("PRE_RECORDED_INAMES");
  QStringList pre_recorded_inames_list {};
  if (pre_recorded_inames != "")
    pre_recorded_inames_list = pre_recorded_inames.split(",");
  if (pre_recorded_inames_list.contains(iname))
  {
    return {false, "iName (" + iname + ") already registerd by you & placed in buffer to broadcast!", nullptr};
  }

  QJsonObject flens_json {
    {"dType", CConsts::DOC_TYPES::INameReg},
    {"dClass", CConsts::INAME_CLASESS::NoDomain},
    {"dVer", "0.0.0"},
    {"iName", iname},
    {"iNOwner", iname_owner},
    {"dCDate", cDate},
    {"dVer", "0.0.0"},
    {"dVer", "0.0.0"},
  };

  auto flens = new INameRegDocument(flens_json);
  flens->signINameRegReq();
  flens->setDExtHash();
  flens->setDocLength();
  flens->setDocHash();

  return {true, "", flens};
}

std::tuple<bool, QString> INameHandler::iNameRegReq(
  const QString& iname,
  const CAddressT& iname_owner)
{
  QString msg;
  // in order to send a FLENS() request to network, must create a documetn of type flens and pay flens fee as wewll

  // create flens doc
  auto[flens_status, flens_msg, flens] = prepareINameRegRequest(
    iname,
    iname_owner);
  if (!flens_status)
  {
    CLog::log("failed in creating a new FleNS doc. " + flens_msg, "app", "error");
    if (flens)
      CLog::log("FleNS: " + flens->safeStringifyDoc(true), "app", "error");

    return {false, flens_msg};
  }

  CLog::log("new FleNS: " + flens->safeStringifyDoc(true), "app", "info");

  // calculate flens cost
  auto[cost_status, flens_dp_cost] = flens->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    CUtils::getNow());
  if (!cost_status)
  {
    msg = "Failed in calculating cost of new FleNS doc. ";
    CLog::log(msg + flens->safeStringifyDoc(true), "app", "error");
    return {false, msg};
  }

  // create a transaction for payment
  auto changeback_res = Wallet::getAnOutputAddress(true);
  if (!changeback_res.status)
  {
    msg = "Failed in create changeback address for FleNS!";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  CAddressT change_back_address = changeback_res.msg;
  std::vector<TOutput> outputs {
    TOutput{change_back_address, 1, CConsts::OUTPUT_CHANGEBACK},
    TOutput{"TP_INAME_REG", flens_dp_cost, CConsts::OUTPUT_TREASURY}};

  auto[coins_status, coins_msg, spendable_coins, spendable_amount] = Wallet::getSomeCoins(
    CUtils::CFloor(flens_dp_cost * 1.3),  // an small portion bigger to support DPCosts
    CConsts::COIN_SELECTING_METHOD::PRECISE);
  if (!coins_status)
  {
    msg = "Failed in finding coin to spend for creating FleNS doc";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto trx_template = BasicTransactionTemplate {
    spendable_coins,
    outputs,
    static_cast<CMPAIValueT>(flens_dp_cost * 1.3),  // max DPCost
    0,    // pre calculated dDPCost
    "Payed for FleNS cost",
    flens->getDocHash()};

  auto[tx_status, tx_res_msg, cost_payer_trx, dp_cost] = BasicTransactionHandler::makeATransaction(trx_template);
  if (!tx_status)
    return {false, tx_res_msg};

  CLog::log("Signed trx for FleNS cost: " + cost_payer_trx->safeStringifyDoc(true), "app", "info");

  // push trx & FleNS to Block buffer
  auto[flens_push_res, flens_push_msg] = CMachine::pushToBlockBuffer(flens, flens_dp_cost);
  if (!flens_push_res)
  {
    msg = "Failed in push FleNS to block buffer FleNS(" + CUtils::hash8c(flens->getDocHash()) + ") " + flens_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[trx_push_res, trx_push_msg] = CMachine::pushToBlockBuffer(cost_payer_trx, cost_payer_trx->getDocCosts());
  if (!trx_push_res)
  {
    msg = "Failed in push trx to block buffer trx(" + CUtils::hash8c(cost_payer_trx->getDocHash()) + ") " + trx_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // mark UTXOs as used in local machine
  Wallet::locallyMarkUTXOAsUsed(cost_payer_trx);

  // also binding a default iPGP pair key to domain name
  auto[bind_status, bind_msg] = INameHandler::createAndBindIPGP(
    iname_owner,  // hot_owner
    generateINameHash(iname), // iname_hash:
    CConsts::DEFAULT, // bind_label
    true, // isHotIName
    "Always PGP"); // bind_comment

  if (!bind_status)
  {
    msg = "Failed in register an iName or Binding iPGP!";
    CLog::log(msg + bind_msg, "app", "error");
    return {false, msg};
  }

  return {true, "Your FleNs(iname Register Request) is pushed to Block buffer"};
}
