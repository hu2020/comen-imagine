#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/document_types/basic_tx_document.h"
#include "lib/block/document_types/iname_documents/iname_bind_document.h"
#include "lib/transactions/basic_transactions/basic_transaction_handler.h"

#include "iname_handler.h"




BindingInfo INameHandler::prepareBindingsDict(
  const ClausesT& clauses,
  const bool& needs_pgp_key)
{
  BindingInfo bInfo;
  QueryRes bindings = DbModel::select(
    stbl_iname_bindings,
    {"nb_doc_hash", "nb_in_hash", "nb_bind_type", "nb_conf_info", "nb_title", "nb_comment", "nb_status", "nb_creation_date"},
    clauses
  );
  if (bindings.records.size() == 0)
  {
    bInfo.m_status = true;
    bInfo.m_msg = "No binded pgp to iname exist";
    return bInfo;
  }

  bInfo.m_binding_dict = {};

  for (QVDicT aBind : bindings.records)
  {
    QJsonObject nb_conf_info = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(aBind.value("nb_conf_info").toString()).content);


    // lightening response
    if (aBind.value("nb_bind_type") == CConsts::BINDING_TYPES::IPGP) {
//      QString PGPHandle = CCrypto::keccak256(aBind.value("nb_conf_info").value("pBLabel").toString() + aBind.value("nb_creation_date").toString());
//      aBind.insert("PGPHandle", PGPHandle);

      if (!needs_pgp_key)
        nb_conf_info.remove("pBPubKey");

//      aBind.insert("nb_conf_info", nb_conf_info);
    }

    QString nb_in_hash = aBind.value("nb_in_hash").toString();
    if (!bInfo.m_binding_dict.keys().contains(nb_in_hash))
      bInfo.m_binding_dict.insert(nb_in_hash, QHash<QString, QJsonArray> {});

    QString nb_bind_type = aBind.value("nb_bind_type").toString();
    if (!bInfo.m_binding_dict.value(nb_in_hash).keys().contains(nb_bind_type))
      bInfo.m_binding_dict[nb_in_hash].insert(nb_bind_type, QJsonArray {});

    for (QString a_key: aBind.keys())
      nb_conf_info[a_key] = aBind.value(a_key).toString();
    bInfo.m_binding_dict[nb_in_hash][nb_bind_type].push_back(nb_conf_info);
  }

  bInfo.m_status = true;
  return bInfo;
}

bool INameHandler::removeBind(const CDocHashT& bind_doc_hash)
{
  //sceptical test
  QueryRes exist = DbModel::select(
    stbl_iname_records,
    {"nb_doc_hash"},
    {{"nb_doc_hash", bind_doc_hash}});

  if (exist.records.size() != 1)
  {
    CLog::log("Try to delete iName strange result! " + CUtils::dumpIt(exist.records), "sec", "error");
    return false;
  }

  CLog::log("Removing iName(" + CUtils::hash8c(bind_doc_hash) + " / " + exist.records[0].value("nb_doc_hash").toString() + ") because of Failed in payment" , "app", "warning");

  DbModel::dDelete(
    stbl_iname_records,
    {{"nb_doc_hash", bind_doc_hash}});

  return true;
}


/**
 *
 * @param {*} args
 * records an onchain binded entity
 */
bool INameHandler::recordABindDoc(
  const Block& block,
  const INameBindDocument* binding_doc)
{
  QueryRes dbl = DbModel::select(
    stbl_iname_bindings,
    {"nb_doc_hash"},
    {{"nb_doc_hash", binding_doc->getDocHash()},
    {"nb_in_hash", binding_doc->m_pgp_binding_iname_hash}});
  if (dbl.records.size() > 0)
  {
    CLog::log("the binding doc already recorded in DAG! doc(" + CUtils::hash8c(binding_doc->getDocHash()) + ") label: " + CUtils::hash6c(binding_doc->m_iname_binding_title), "app", "warning");
    return true;
  }

  QVDicT values {
    {"nb_doc_hash", binding_doc->getDocHash()},
    {"nb_in_hash", binding_doc->m_pgp_binding_iname_hash},
    {"nb_bind_type", binding_doc->m_iname_binding_type},
    {"nb_conf_info",  BlockUtils::wrapSafeContentForDB(CUtils::serializeJson(binding_doc->exportBindInfoToJSon())).content},
    {"nb_title", binding_doc->m_iname_binding_title},
    {"nb_comment", binding_doc->m_iname_binding_comment},
    {"nb_status", CConsts::VALID},
    {"nb_creation_date", block.m_block_creation_date}};
  CLog::log("Create A Binding pgp key to iName: " + CUtils::dumpIt(values));

  dbl = DbModel::select(
    stbl_iname_bindings,
    {"nb_title", "nb_in_hash"},
    {{"nb_title", binding_doc->m_iname_binding_title},
    {"nb_in_hash", binding_doc->m_pgp_binding_iname_hash}});
  if (dbl.records.size() > 0)
  {
    DbModel::update(
      stbl_iname_bindings,
      values,
      {{"nb_title", binding_doc->m_iname_binding_title},
      {"nb_in_hash", binding_doc->m_pgp_binding_iname_hash}});

  } else {
    DbModel::insert(
      stbl_iname_bindings,
      values);

  }

  return true;
}

QVDRecordsT INameHandler::searchINameBindings(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int limit)
{
  QueryRes posts = DbModel::select(
    stbl_iname_bindings,
    fields,
    clauses,
    order,
    limit);

  return posts.records;
}
/**
 *
 * @param {*} args
 * records a newly created binding in local machine user for current profile
 */
// js name was createABinding
std::tuple<bool, QString> INameHandler::insertBindingInfoInLocalDB(
  const CDocHashT& mcb_in_hash,
  const QString& mcb_bind_type,
  const QString& mcb_conf_info,
  const QString& mcb_label,
  const QString& mcb_comment,
  const QString& mp_code,
  const CDateT& mcb_creation_date)
{
  QString msg;
  QueryRes dbl = DbModel::select(
    stbl_machine_iname_bindings,
    {"mcb_mp_code", "mcb_in_hash", "mcb_bind_type", "mcb_label"},
    {{"mcb_mp_code", mp_code},
    {"mcb_in_hash", mcb_in_hash},
    {"mcb_bind_type", mcb_bind_type},
    {"mcb_label", mcb_label}});
  if (dbl.records.size() > 0)
  {
    msg = "The binding label(" + mcb_label + ") for(" + CUtils::hash8c(mcb_in_hash) + ") already exist!";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  QVDicT values {
    {"mcb_mp_code", mp_code},
    {"mcb_in_hash", mcb_in_hash},
    {"mcb_bind_type", mcb_bind_type},
    {"mcb_conf_info", mcb_conf_info},
    {"mcb_comment", mcb_comment},
    {"mcb_label", mcb_label},
    {"mcb_creation_date", mcb_creation_date}};
  CLog::log("Create A PGP Binding: " + CUtils::dumpIt(values), "app", "info");
  bool ins_res = DbModel::insert(
    stbl_machine_iname_bindings,
    values);
  if (!ins_res)
    return {false, "Failed in insert binding info"};

  return {true, "done"};
}

std::tuple<bool, QString, INameBindDocument*> INameHandler::prepareReqToBinding(
  const QJsonObject& pgp_binging_info,
  const QString& binding_type,
  const QString& binding_label,
  const QString& binding_comment,
  const CDateT& creation_date)
{
  QString msg;
  QJsonObject bindToINameReqTpl {
    {"dCDate", creation_date},
    {"dType", CConsts::DOC_TYPES::INameBind},
    {"dClass", CConsts::GENERAL_CLASESS::Basic},
    {"nbComment", binding_comment},
    {"nbTitle", binding_label},
    {"nbType", binding_type},
    {"nbInfo", pgp_binging_info},
    {"dVer", "0.0.0"}};
  INameBindDocument* binding_request_doc = new INameBindDocument(bindToINameReqTpl);

  // do sign doc by proper key
  QString sign_message = binding_request_doc->getDocSignMsg();
  auto[sign_status, res_msg, sign_signatures, sign_unlock_set] = Wallet::signByAnAddress(
    pgp_binging_info.value("inameOwner").toString(),
    sign_message);
  if (!sign_status)
    return {false, res_msg, nullptr};

  binding_request_doc->m_doc_ext_info = QJsonArray{QJsonObject {
    {"uSet", sign_unlock_set},
    {"signatures", CUtils::convertQStringListToJSonArray(sign_signatures)}}};

  binding_request_doc->setDExtHash();
  binding_request_doc->setDocLength();
  binding_request_doc->setDocHash();
  CLog::log("Created PGP Binding document: " + binding_request_doc->safeStringifyDoc(true), "app", "info");

  return {true, "", binding_request_doc};
}

std::tuple<bool, QString> INameHandler::createAndBindIPGP(
  const CAddressT& iname_owner,
  const CDocHashT& iname_hash,
  const QString& bind_label,
  const bool is_hot_iname, // the Hot IName is the iName which is currently created by user, bus still is not setteled
  const CAddressT& hot_owner,
  const QString& bind_comment)
{
  QString msg;

  // retrieve proper iName
  CAddressT the_owner;
  if (is_hot_iname) {
    the_owner = hot_owner;
  } else {
    auto[res_status, res_msg, inames, bindings_info] = getMachineControlledINames({{"imn_hash", iname_hash}});
    if (inames.size() != 1)
    {
      msg = "The iNameHash(" + CUtils::hash8c(iname_hash) + ") does not exist or not controlled by your Wallet/Profile";
      CLog::log(msg + res_msg, "app", "error");
      return {false, msg};
    }
    the_owner = inames[0].value("imn_owner").toString();
  }

  // prepare iPGP pair key
  auto[status, private_PGP, public_PGP] = CCrypto::nativeGenerateKeyPair();
  if (!status)
  {
    CLog::log("Couldn't create iPGP key pair to Bind", "app", "fatal");
    return { false, ""};
  }

  QJsonObject pgp_binging_info = {
    {"inameHash", iname_hash},  // binded iname hash
    {"inameOwner", iname_owner},    // binded iname owner
    {"pBLabel", bind_label},
    {"pBComment", bind_comment},
    {"pBVersion", "0.0.0"},
    {"pBPubKey", public_PGP}};
  CLog::log("perepared pgp_binging_info: " + CUtils::serializeJson(pgp_binging_info), "app", "info");
  pgp_binging_info["pBPrvKey"] = private_PGP;

  // TODO: implement a draft table and mechanisem to avoid misconception of accepted bindings
  // for now directy insert to machine name_bindings table
  auto[bind_Status, bind_msg] = insertBindingInfoInLocalDB(
    iname_hash,
    CConsts::BINDING_TYPES::IPGP,
    CUtils::serializeJson(pgp_binging_info), // mcb_conf_info
    bind_label,
    bind_comment);
  if (!bind_Status)
  {
    msg = "failed in insert Binding Info In Local DB" + bind_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // prepare request document
  pgp_binging_info.remove("pBPrvKey");
  auto[bind_status, bind_res_msg, binding_doc] = prepareReqToBinding(
    pgp_binging_info,
    CConsts::BINDING_TYPES::IPGP,
    bind_label,
    bind_comment);
  if (!bind_status)
  {
    msg = "failed in create Binding doc " + bind_res_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // prepare payment doc too
  // calculate bind cost
  auto[binding_cost_status, binding_dp_cost] = binding_doc->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating);
  if (!binding_cost_status)
  {
    msg = "failed in calculating the binding cost";
    CLog::log(msg + binding_doc->safeStringifyDoc(true), "app", "error");
    return {false, msg};
  }

  // create a transaction for payment
  auto changeback_res = Wallet::getAnOutputAddress(true);
  if (!changeback_res.status)
  {
    msg = "Failed in create changeback address for FleNS!";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  CAddressT change_back_address = changeback_res.msg;
  std::vector<TOutput> outputs {
    TOutput{change_back_address, 1, CConsts::OUTPUT_CHANGEBACK},
    TOutput{"TP_INAME_BIND", binding_dp_cost, CConsts::OUTPUT_TREASURY}};

  auto[coins_status, coins_msg, spendable_coins, spendable_amount] = Wallet::getSomeCoins(
    CUtils::CFloor(binding_dp_cost * 2),  // an small portion bigger to support DPCosts
    CConsts::COIN_SELECTING_METHOD::PRECISE);
  if (!coins_status)
  {
    msg = "Failed in finding coin to spend for creating binding iname to PGP key";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto trx_template = BasicTransactionTemplate {
    spendable_coins,
    outputs,
    static_cast<CMPAIValueT>(binding_dp_cost * 2),  // max DPCost
    0,    // pre calculated dDPCost
    "Payed for iPGP binding cost",
    binding_doc->getDocHash()};

  auto[tx_status, tx_res_msg, cost_payer_trx, dp_cost] = BasicTransactionHandler::makeATransaction(trx_template);
  if (!tx_status)
    return {false, tx_res_msg};

  CLog::log("Signed trx for iPGP binding cost: " + cost_payer_trx->safeStringifyDoc(true), "app", "info");

  // push trx & bind doc to Block buffer
  auto[bind_push_res, bind_push_msg] = CMachine::pushToBlockBuffer(binding_doc, binding_dp_cost);
  if (!bind_push_res)
  {
    msg = "Failed in push iPGP bind doc to block buffer bind(" + CUtils::hash8c(binding_doc->getDocHash()) + ") " + bind_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[trx_push_res, trx_push_msg] = CMachine::pushToBlockBuffer(cost_payer_trx, cost_payer_trx->getDocCosts());
  if (!trx_push_res)
  {
    msg = "Failed in push binding payer trx to block buffer trx(" + CUtils::hash8c(cost_payer_trx->getDocHash()) + ") " + trx_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // mark UTXOs as used in local machine
  Wallet::locallyMarkUTXOAsUsed(cost_payer_trx);

  return {true, "Your Binding request is pushed to Block buffer"};
}
