#include <vector>
#include "reserved_inames.h"

ReservedINames::ReservedINames()
{

}


std::vector<std::string> ReservedINames::s_reserved_inames =
{
  "about",
  "example",
  "chat",
  "contact",
  "demos",
  "faq",
  "home",
  "hu",
  "imagine",
  "index",
  "localhost",
  "messenger",
  "security",
  "settings",
  "wiki",
  "web3",
  "web",
  "misc",
  "contract",
  "contracts",
  "plugin",
  "plugins",
  "iname",
  "inames",
  "forum",
  "forums",
  "samples",
  "home",
  "wallet",
  "wallets",
  "contributes",
  "contribute",
  "monitor",
  "contributors",
  "contributor",
  "satoshi",
  "nakamoto",
  "satoshinakamoto",
  
};
