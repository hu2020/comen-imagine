#ifndef RESERVEDINAMES_H
#define RESERVEDINAMES_H

#include <list>
#include <string>


class ReservedINames
{
public:
  static std::vector<std::string> s_reserved_inames;
  ReservedINames();
};

#endif // RESERVEDINAMES_H
