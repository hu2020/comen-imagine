#ifndef INAMEHANDLER_H
#define INAMEHANDLER_H

class INameRegDocument;
class INameBindDocument;

class FleNS
{
public:


  QString m_doc_hash = "";
  QString m_iname = "";
  QString m_owner = "";
  QString m_doc_creation_date = "";
  QString m_block_creation_date = "";
  bool m_iname_is_setteled = false;

  FleNS(const QVDicT& values = {}){
    if (values.keys().size() > 0)
      setByDict(values);
  };
  bool setByDict(const QVDicT& values = {});

  QString getDocHash() const { return m_doc_hash; };
};


class BindingInfo {
public:
  bool m_status;
  QHash<QString, QHash<QString, QJsonArray> > m_binding_dict;
  QVDRecordsT m_records;
  QString m_msg;
  QVDicT m_map_address_to_profile = {};

  BindingInfo(
    const bool& status = false,
    const QHash<QString, QHash<QString, QJsonArray> >& binding_dict = {},
    const QVDRecordsT& records = {},
    const QString& msg = "",
    const QVDicT& map_address_to_profile = {}
    ): m_status(status), m_binding_dict(binding_dict), m_records(records),
  m_msg(msg), m_map_address_to_profile(map_address_to_profile){};

};


class INameHandler
{
public:
  static const QString stbl_iname_records;
  static const QStringList stbl_iname_records_fields;

  static const QString stbl_machine_iname_records;
  static const QStringList stbl_machine_iname_records_fields;

  static const QString stbl_iname_bindings;
  static const QStringList stbl_iname_bindings_fields;

  static const QString stbl_machine_iname_bindings;

  static bool initINames();
  static QString normalizeIName(const QString inp);
  static QString generateINameHash(const QString& iname);
  static BindingInfo searchForINamesControlledByWallet(QString mp_code = "", const bool& needBindingsInfoToo = false);

  static std::tuple<bool, QString, QVDRecordsT, BindingInfo> getMachineControlledINames(
    ClausesT clauses,
    const QStringList& fields = stbl_machine_iname_records_fields,
    const OrderT& order = {{"imn_register_date", "ASC"}},
    const bool& need_bindings_info_too = false,
    const bool& needs_pgp_key = false,
    const QString& mp_code = CMachine::getSelectedMProfile());

  //  -  -  -  iname_register_handler.cpp
  static bool recordINameInDAG(const FleNS& flens);
  static bool recordINameInDAG(
    const Block& block,
    const INameRegDocument* polling);
  static bool recordINameInDAG_(
    const Block& block,
    const INameRegDocument* flens,
    const bool inIsSettled = false);

  static BindingInfo searchRegisteredINames(
    const ClausesT& clauses,
    QStringList fields = stbl_iname_records_fields,
    const OrderT& order = {{"in_name", "ASC"}},
    const int limit = 0,
    const bool& need_bindings_info_too = false);

  static bool maybeAddToMachineControlled(
    const QString& iname,
    const CDocHashT& registering_doc_hash,
    const CAddressT& iname_owner,
    const CDateT& register_date);

  static std::tuple<DNAShareCountT, uint32_t, uint32_t, uint32_t> ownerRegisteredRecords(const CAddressT& address);
  static std::tuple<bool, QVDicT> alreadyRegistered(const QString& iName);
  static bool removeINameByHash(const QString& iName_hash);
  static bool removeINameByName(const QString& iName);
  static std::tuple<bool, Block*> createFleNSVoteBlock(
    const CDocHashT& collision_ref,
    const CBlockHashT& block_hash);

  static bool isAcceptableIName(const QString& iname);

  static CMPAIValueT getPureINameRegCost(const QString& iname);

  static std::tuple<bool, CMPAIValueT, QString> estimateINameRegReqCost(
    const QString& iname,
    const QString& stage,
    const CDateT& cDate = CUtils::getNow());

  static std::tuple<bool, QString, INameRegDocument*> prepareINameRegRequest(
    QString iname,
    const CAddressT& iname_owner = CMachine::getBackerAddress(),
    const CDateT& cDate = CUtils::getNow());

  static std::tuple<bool, QString> iNameRegReq(
    const QString& iname,
    const CAddressT& iname_owner = CMachine::getBackerAddress());

  static std::tuple<bool, QString> addToPreRecordedINames(QString iname);

  static bool loopINamesSettlement();

  static bool maybeINameSettlement();




  //  -  -  -  iname_binder.cpp
  static BindingInfo prepareBindingsDict(
    const ClausesT& clauses,
    const bool& needs_pgp_key = false);

  static bool removeBind(const CDocHashT& bind_doc_hash);
  static bool recordABindDoc(
    const Block& block,
    const INameBindDocument* bindDoc);

  static QVDRecordsT searchINameBindings(
    const ClausesT& clauses = {},
    const QStringList& fields = stbl_iname_bindings_fields,
    const OrderT& order = {},
    const int limit = 0);

  static std::tuple<bool, QString, INameBindDocument*> prepareReqToBinding(
    const QJsonObject& pgp_binging_info,
    const QString& binding_type,
    const QString& binding_label,
    const QString& binding_comment,
    const CDateT& creation_date = CUtils::getNow());

  static std::tuple<bool, QString> createAndBindIPGP(
    const CAddressT& iname_owner,
    const CDocHashT& iname_hash,
    const QString& bind_label = CConsts::DEFAULT,
    const bool is_hot_iname = true, // the Hot IName is the iName which is currently created by user, bus still is not setteled
    const CAddressT& hot_owner = "",
    const QString& bind_comment = "Always PGP");

  static std::tuple<bool, QString> insertBindingInfoInLocalDB(
    const CDocHashT& mcb_in_hash,
    const QString& mcb_bind_type,
    const QString& mcb_conf_info,
    const QString& mcb_label,
    const QString& mcb_comment,
    const QString& mp_code = CMachine::getSelectedMProfile(),
    const CDateT& mcb_creation_date = CUtils::getNow());


  //  -  -  -  iname_messaging.cpp

};

#endif // INAMEHANDLER_H
