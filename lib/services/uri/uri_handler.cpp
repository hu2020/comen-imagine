#include "stable.h"

#include "uri_handler.h"

const QStringList URIHandler::s_modules = {"wiki", "demos"};

URIHandler::URIHandler()
{

}

CURI URIHandler::ParseURI(const QString& uri)
{
  CURI curi;
  QStringList sements = uri.split("/");

  if (sements.size() == 0)
    return curi;

  // the first segment should be domain name. if not, it MUST be be module name
  int offset = 0;
  if (s_modules.contains(sements[0]))
  {
    curi.m_domain = "imagine";
    curi.m_module = sements[0];

  } else {
    curi.m_domain = sements[0];
    curi.m_module = sements[1];
    offset = 1;
  }

  curi.m_module = curi.m_module.toUtf8().toLower(); // TODO: improve it to support unicode and local languages (either for domain name or parameters)

  if (curi.m_module == "demos")
  {
    if (sements.size() > offset)
      curi.m_page = sements[offset + 1];

  } else if (curi.m_module == "wiki")
  {

  }

  return curi;
}

