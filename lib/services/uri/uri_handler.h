#ifndef URIHANDLER_H
#define URIHANDLER_H


/**
 * @brief The CURI class
 * curi format = domain/module/page/ ... other segments ... /?parameters
 */
class CURI
{
public:
  QString m_domain = "imagine";
  QString m_module = "";  // e.g. wiki, demos, weblog, ...
  QString m_page = "";
  QVDicT m_params = {};

};

class URIHandler
{
public:
  URIHandler();

  static const QStringList s_modules;


  static CURI ParseURI(const QString& uri);
};

#endif // URIHANDLER_H
