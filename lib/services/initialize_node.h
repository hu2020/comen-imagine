#ifndef INITIALIZENODE_H
#define INITIALIZENODE_H


#include "lib/machine/machine_handler.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/block/block_types/block_genesis/genesis_block.h"
#include "lib/services/free_docs/demos/initialize_agoras.h"
#include "lib/services/contracts/flens/iname_handler.h"
#include "lib/services/free_docs/wiki/wiki_handler.h"
#include "gui/c_gui.h"


class InitializeNode
{
public:
  InitializeNode();

  static MainWindow* s_mw;
  static void setMW(MainWindow* mw);
  static MainWindow* getMW();
  static bool refreshGUI();

  static bool bootMachine();
  static bool maybeInitDAG();
  static bool doesSafelyInitialized();
};

#endif // INITIALIZENODE_H
