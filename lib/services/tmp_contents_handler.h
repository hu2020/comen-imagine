#ifndef TMPCONTENTSHANDLER_H
#define TMPCONTENTSHANDLER_H


class TmpContentsHandler
{
public:
  TmpContentsHandler();
  const static QString stbl_machine_tmp_contents;
  const static QStringList stbl_machine_tmp_contents_fields;

  static QVDRecordsT searchTmpContents(
    const ClausesT& clauses,
    const QStringList& fields = stbl_machine_tmp_contents_fields,
    const OrderT& order = {},
    const int& limit = 0);

  static bool insertTmpContent(
    const QString& content_type,
    const QString& content_class,
    const CDocHashT& content_hash,
    const QString& payload,
    const QString& content_status = CConsts::NEW,
    const CDateT& creation_date = CUtils::getNow(),
    const QString& mp_code = CMachine::getSelectedMProfile());

  static bool deleteTmpContent(
    const ClausesT& clauses);

  static std::tuple<bool, QString> unBundleAndSignPLR(const uint64_t tc_id);

  static std::tuple<bool, QString> unBundleAndPushToBlockBufferPPT(const uint64_t tc_id);


};

#endif // TMPCONTENTSHANDLER_H
