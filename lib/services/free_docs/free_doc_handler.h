#ifndef FREEDOCHANDLER_H
#define FREEDOCHANDLER_H

class FreeDocument;

class FreeDocHandler
{
public:
  FreeDocHandler();

  static bool removeFDoc(const CDocHashT& doc_hash);

  static bool customTreatments(
    const Block& block,
    const FreeDocument* fDoc);

  static std::tuple<bool, QString> payForFreeDocAndPushToBuffer(
    const FreeDocument* free_doc,
    const CDateT& creation_date = CUtils::getNow());

};

#endif // FREEDOCHANDLER_H
