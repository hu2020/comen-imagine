#include "stable.h"

#include "gui/c_gui.h"
#include "lib/block/document_types/document.h"
#include "lib/services/free_docs/wiki/wiki_handler.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/document_types/basic_tx_document.h"
#include "lib/services/free_docs/demos/demos_handler.h"
#include "lib/services/free_docs/file/file_handler_in_dag.h"
#include "lib/block/document_types/free_documents/free_document.h"
#include "lib/transactions/basic_transactions/basic_transaction_handler.h"

#include "free_doc_handler.h"

FreeDocHandler::FreeDocHandler()
{

}




bool FreeDocHandler::removeFDoc(const CDocHashT& doc_hash)
{
  // TODO: implement it ASAP
//  //sceptical test
//  QueryRes exist = DbModel::select(
//    stbl_,
//    {"nb_doc_hash"},
//    {{"nb_doc_hash", doc_hash}});

//  if (exist.records.size() != 1)
//  {
//    CLog::log("Try to delete iName strange result! " + CUtils::dumpIt(exist.records), "sec", "error");
//    return false;
//  }

//  CLog::log("Removing iName(" + CUtils::hash8c(doc_hash) + " / " + exist.records[0].value("nb_doc_hash").toString() + ") because of Failed in payment" , "app", "warning");

//  DbModel::dDelete(
//    stbl_,
//    {{"nb_doc_hash", doc_hash}});

  return true;
}


bool FreeDocHandler::customTreatments(
  const Block& block,
  const FreeDocument* free_doc)
{
  // TODO: implemet a good factory design pattern and get rid of these switches
  if (free_doc->m_doc_class == CConsts::FPOST_CLASSES::File)
  {
    return FileHandlerInDAG::maybeWriteFileInLocal(block, free_doc);

  }
  else if (free_doc->m_doc_class == CConsts::FPOST_CLASSES::DMS_Post)
  {
    return DemosHandler::insertDemosDocPost(block, free_doc);

  }
  else if (free_doc->m_doc_class == CConsts::FPOST_CLASSES::DMS_RegAgora)
  {
    bool ins_res = DemosHandler::insertAgoraInDB(block, free_doc);
    if (ins_res && !CMachine::isInSyncProcess())
    {
      CGUI::signalUpdateAgoraTitles();
      auto ttt = CGUI::get().m_ui->comboBox_agora_category;
      CGUI::initAgorasCategoriesCombo(
        ttt,
        CGUI::get().m_ui->comboBox_agora_domain->currentData().toString());
    }
    return ins_res;

  }
  else if (QStringList{CConsts::FPOST_CLASSES::WK_CreatePage, CConsts::FPOST_CLASSES::WK_EditPage}.contains(free_doc->m_doc_class))
  {
    bool ins_res = WikiHandler::recordInWikiDB(block, free_doc);
    if (ins_res && !CMachine::isInSyncProcess())
      CGUI::signalUpdateWikiTitles();
    return ins_res;

  }
  else
  {
    // return listener.doCallSync('SASH_custom_treat_FreeDocs', block, free_doc);
    return true;
  }
}

std::tuple<bool, QString> FreeDocHandler::payForFreeDocAndPushToBuffer(
  const FreeDocument* free_doc,
  const CDateT& creation_date)
{
  QString msg;
  // calculate post cost
  auto[status, doc_dp_cost] = free_doc->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    creation_date);
  if (!status)
    return {false, "Failed in calculating free doc cost"};

  // create a transaction for payment
  auto changeback_res = Wallet::getAnOutputAddress(true);
  if (!changeback_res.status)
  {
    msg = "Failed in create changeback address for FDoc!";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }
  CAddressT change_back_address = changeback_res.msg;
  std::vector<TOutput> outputs {
    TOutput{change_back_address, 1, CConsts::OUTPUT_CHANGEBACK},
    TOutput{"TP_FDOC", doc_dp_cost, CConsts::OUTPUT_TREASURY}};

  auto[coins_status, coins_msg, spendable_coins, spendable_amount] = Wallet::getSomeCoins(
    CUtils::CFloor(doc_dp_cost * 1.5),  // an small portion bigger to support DPCosts
    CConsts::COIN_SELECTING_METHOD::PRECISE);
  if (!coins_status)
  {
    msg = "Failed in finding coin to spend for creating FDoc";
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto trx_template = BasicTransactionTemplate {
    spendable_coins,
    outputs,
    static_cast<CMPAIValueT>(doc_dp_cost * 1.5),  // max DPCost
    0,    // pre calculated dDPCost
    "Payed for FDoc cost",
    free_doc->getDocHash()};

  auto[tx_status, tx_res_msg, cost_payer_trx, dp_cost] = BasicTransactionHandler::makeATransaction(trx_template);
  if (!tx_status)
    return {false, tx_res_msg};

  CLog::log("Signed trx for FDoc cost: " + cost_payer_trx->safeStringifyDoc(true), "app", "info");

  // push trx & FDoc to Block buffer
  auto[flens_push_res, flens_push_msg] = CMachine::pushToBlockBuffer(free_doc, doc_dp_cost);
  if (!flens_push_res)
  {
    msg = "Failed in push FDoc to block buffer FDoc(" + CUtils::hash8c(free_doc->getDocHash()) + ") " + flens_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  auto[trx_push_res, trx_push_msg] = CMachine::pushToBlockBuffer(cost_payer_trx, cost_payer_trx->getDocCosts());
  if (!trx_push_res)
  {
    msg = "Failed in push FDoc payer trx to block buffer trx(" + CUtils::hash8c(cost_payer_trx->getDocHash()) + ") " + trx_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // mark UTXOs as used in local machine
  Wallet::locallyMarkUTXOAsUsed(cost_payer_trx);

  return {true, "Your free document(" + free_doc->m_doc_type + ") was pushed to Block Buffer"};
}
