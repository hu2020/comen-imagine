#include "stable.h"

#include "lib/machine/machine_handler.h"
#include "lib/file_handler/file_handler.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/free_documents/free_document.h"

#include "file_handler_in_dag.h"

FileHandlerInDAG::FileHandlerInDAG()
{

}

bool FileHandlerInDAG::maybeWriteFileInLocal(
  const Block& block,
  const FreeDocument* file_doc)
{
  // depends on machine security level & file type and sender/signer shares in imagine DNA
  // each machine can decide to decrypt file or not
  // TODO: improve it to support more comprehensive reputation system beside the file controll by Oracles and society ranking (like, dislike etc..)

  if (CConsts::DECODE_ALL_FREE_POST_FILES)
  {
    QString cPFileContent = ""; //file_doc.cPFileContent; FIXME: implement uploading file in c++

    // write file on hard
    FileHandler::write(
      CMachine::getCachePath(),
      file_doc->m_file_name,
      file_doc->m_file_content
          // overWrite: true
      );


    if (file_doc->m_doc_signer != "")
    {
      QString mp_code = CMachine::getSelectedMProfile();
      // insert in DB too(if its mine), it will be used to administrator reasons
//      let walletControlledAddresses = walletAddressHandler.searchWalletAdress({ query: [['wa_address', file_doc.signer]] });
//      if (walletControlledAddresses.length > 0) {
//          let exist = model.sRead({
//              table,
//              query: [
//                  ['mpf_mp_code', mp_code],
//                  ['mpf_name', file_doc.cPFileName],
//              ]
//          });
//          if (exist.length == 0) {
//              let values = {
//                  mpf_mp_code: mp_code,
//                  mpf_name: file_doc.cPFileName,
//                  mpf_doc_hash: file_doc.hash,
//                  mpf_mime: file_doc.cPFileMimetype,
//                  mpf_creation_date: file_doc.creation Date,
//                  mpf_signer: file_doc.signer
//              }
//              clog.app.info(`inserting a free file ${utils.stringify(values)}`);
//              model.sCreate({
//                  table,
//                  values
//              });
//          }
//      }

    }
  }

  return true;
}
