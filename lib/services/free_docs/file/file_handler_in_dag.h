#ifndef FILEHANDLERINDAG_H
#define FILEHANDLERINDAG_H

class FreeDocument;

class FileHandlerInDAG
{
public:
  FileHandlerInDAG();

  static bool maybeWriteFileInLocal(
    const Block& block,
    const FreeDocument* free_doc);
};

#endif // FILEHANDLERINDAG_H
