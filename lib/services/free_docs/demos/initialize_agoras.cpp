#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/block/document_types/document.h"
#include "lib/services/free_docs/demos/demos_handler.h"
#include "lib/block/document_types/free_documents/free_document.h"
#include "lib/services/free_docs/demos/init_contents/agoras_init_contents.h"

#include "initialize_agoras.h"

class DemosHandler;

using std::vector;

AgoraInitializer::AgoraInitializer()
{

}

QSDicT AgoraInitializer::map_title_to_ag_hash = {};
bool AgoraInitializer::initAgoras()
{
  bool status = false;
  vector<InitAgora> def_agoras =
  {
    InitAgora("Beginners"),
    InitAgora("Development"),
    InitAgora("Economy"),
    InitAgora("Translation")
  };

  QString iname = "imagine";
  QString creation_date = CMachine::getLaunchDate();

  Block tmp_block {};
  tmp_block.m_block_creation_date = creation_date;

  for (InitAgora an_agora : def_agoras) {
    // in order to create agora hash for root categories we just hash(iname / agoraTitle)
    // wheras for sub cats we use hash(parentHash / agoraTitle)
    QString agora_hash = CCrypto::keccak256(iname + "/" + an_agora.m_title);
    FreeDocument* tmp_doc = new FreeDocument(QJsonObject{
      {"dType", CConsts::DOC_TYPES::FPost},
      {"dClass", CConsts::FPOST_CLASSES::DMS_RegAgora},
      {"dTitle", an_agora.m_title},
      {"dHash", agora_hash},  // a dummy doc-hash for initializing
      {"iName", iname},
      {"agHash", agora_hash},
      {"parent", ""},
      {"cLang", CConsts::DEFAULT_LANG},
      {"dComment", an_agora.m_description},
      {"dTags", an_agora.m_tags},
      {"dCDate", creation_date},
      {"dSigner", CConsts::HU_INAME_OWNER_ADDRESS}});

    bool status = DemosHandler::insertAgoraInDB(tmp_block, tmp_doc);
    delete tmp_doc;
    map_title_to_ag_hash.insert(an_agora.m_title, agora_hash);
    CLog::log(">>>>>>>>>>>>>>>>>>>>> map_title_to_ag_hash " + agora_hash + " <- " + an_agora.m_title);
    if (!status)
    {
      return false;
    }
  }


  // insert sub-category
  def_agoras =
  {
    InitAgora("Before Start"),
    InitAgora("Contribute, Proposals, Earn And Shares")
  };
  status = insertSubCat(
    iname,
    "Beginners",
    def_agoras
  );

  // insert sub-category
  def_agoras =
  {
    InitAgora("Before Start"),
    InitAgora("Pre Proposals and Discussions"),
    InitAgora("Security"),
    InitAgora("Core develop"),
    InitAgora("Interface Develop (Angular)"),
    InitAgora("Mobile Wallet"),
    InitAgora("3rd Party development")
  };
  status = insertSubCat(
    iname,
    "Development",
    def_agoras);
  if (!status)
  {
    CLog::log("insert Sub Cat failed", "app", "error");
    return false;
  }

  def_agoras =
  {
    InitAgora("Machine Buffer watcher")
  };
  status = insertSubCat(
    iname,
    "3rd Party development",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insert Sub Cat failed", "app", "error");
    return false;
  }



  def_agoras =
  {
    InitAgora("Inflationary or Deflationary Money"),
    InitAgora("How to determine coins value")
  };
  status = insertSubCat(
    iname,
    "Economy",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }



  def_agoras =
  {
    InitAgora("Comunication & Encryption Enhancement"),
    InitAgora("Plugins"),
    InitAgora("Wallet API"),
    InitAgora("Messenger improvement")
  };
  status = insertSubCat(
    iname,
    "Core develop",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }


  def_agoras =
  {
    InitAgora("Plugin handler improvements"),
    InitAgora("Distributed Version Control Plugin implementation"),
    InitAgora("Distributed Wiki Plugin implementation"),
  };
  status = insertSubCat(
    iname,
    "Plugins",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }


  def_agoras =
  {
    InitAgora("Design & Theme Improvement"),
    InitAgora("Multilingual"),
    InitAgora("Wallet Enhancement"),
  };
  status = insertSubCat(
    iname,
    "Interface Develop (Angular)",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }



  def_agoras =
  {
    InitAgora("Logos, either imagine or PAI")
  };
  status = insertSubCat(
    iname,
    "Design & Theme Improvement",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }


  def_agoras =
  {
    InitAgora("iPGP improvement")
  };
  status = insertSubCat(
    iname,
    "Comunication & Encryption Enhancement",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }


  // insert initial content of Agoras
  return initPosts();
}


bool AgoraInitializer::insertSubCat(
  const QString& iname,
  const QString& parent_name,
  std::vector<InitAgora>& def_agoras)
{

  const QString creation_date = CMachine::getLaunchDate();

  QVDRecordsT parent_info = DemosHandler::searchInAgoras(
    {{"ag_iname", iname},
    {"ag_title", parent_name}});

  Block tmp_block {};
  tmp_block.m_block_creation_date = creation_date;

  QString parent_hash = parent_info[0].value("ag_hash").toString();
  for (InitAgora an_agora : def_agoras)
  {
    QString agora_hash = CCrypto::keccak256(parent_hash + "/" + an_agora.m_title);
    QString ag_doc_hash = agora_hash;   // a dummy doc-hash for initializing

    FreeDocument* tmp_doc = new FreeDocument(QJsonObject{
      {"dClass", CConsts::FPOST_CLASSES::DMS_RegAgora},
      {"dTitle", an_agora.m_title},
      {"dHash", ag_doc_hash},
      {"agHash", agora_hash},
      {"iName", iname},
      {"parent", parent_hash},
      {"dComment", an_agora.m_description},
      {"dTags", an_agora.m_tags},
      {"dCDate", creation_date},
      {"dSigner", CConsts::HU_INAME_OWNER_ADDRESS}});

    bool status = DemosHandler::insertAgoraInDB(tmp_block, tmp_doc);
    delete tmp_doc;
    if (!status)
    {
      CLog::log("insert Sub Cat Failed" + parent_name + "-" + an_agora.m_title, "app", "fatal");
      return false;
    }
    map_title_to_ag_hash.insert(parent_info[0].value("ag_title").toString() + "-" + an_agora.m_title, agora_hash);
    CLog::log(">>>>>>>>>>>>>>>>>>>>> map_title_to_ag_hash " + agora_hash + " <- " + an_agora.m_title);
  }
  return true;
}

bool AgoraInitializer::initPosts()
{
  QString creation_date = CMachine::getLaunchDate();
  // read all default post contents and insert in proper Agora
  QStringList topics = db_init_agoras_content.keys();
  topics.sort();
  CLog::log(">>>>>>>>>>>>>>>>>>>>usssseeee> map_title_to_ag_hash " + CUtils::dumpIt(map_title_to_ag_hash));
  Block tmp_block {};
  tmp_block.m_block_creation_date = creation_date;
  for (QString a_topic : topics) {
    QString ap_opinion = BlockUtils::wrapSafeContentForDB(db_init_agoras_content.value(a_topic)).content;
    FreeDocument* tmp_doc = new FreeDocument(QJsonObject{
      {"dClass", CConsts::FPOST_CLASSES::DMS_Post},
      {"agHash", map_title_to_ag_hash.value(a_topic)}, // map_title_to_ag_hash.value(a_topic)},
      {"dHash", CCrypto::keccak256(ap_opinion)},
      {"opinion", ap_opinion},
      {"reply", ""},
      {"replyPoint", 0},
      {"dCDate", creation_date},
      {"dSigner", CConsts::HU_INAME_OWNER_ADDRESS}
    });
    DemosHandler::insertDemosDocPost(tmp_block, tmp_doc);
    delete tmp_doc;
  }

  return true;
}
