#include "stable.h"

#include "lib/clog.h"
#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/wallet/wallet.h"
#include "lib/machine/machine_handler.h"
#include "lib/block/document_types/document.h"
#include "lib/utils/render_handler/render_handler.h"
#include "lib/services/free_docs/free_doc_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/block/document_types/free_documents/free_document.h"

#include "demos_handler.h"

using std::string;

DemosHandler::DemosHandler()
{

}

const QString DemosHandler::stbl_agoras = "c_agoras";
const QStringList DemosHandler::stbl_agoras_fields = {"ag_id", "ag_title", "ag_parent", "ag_iname", "ag_hash", "ag_doc_hash", "ag_language", "ag_description", "ag_content_format_version", "ag_tags", "ag_creation_date", "ag_last_modified", "ag_creator", "ag_controlled_by_machine", "ag_mp_code"};

const QString DemosHandler::stbl_agoras_posts = "c_agoras_posts";
const QStringList DemosHandler::stbl_agoras_posts_fields = {"ap_id", "ap_doc_hash", "ap_ag_hash", "ap_opinion", "ap_attrs", "ap_format_version", "ap_reply", "ap_reply_point", "ap_creation_date", "ap_creator"};

bool DemosHandler::insertAgoraInDB(
  const Block& block,
  const FreeDocument* free_doc)  // const Agora& ago
{

  if (free_doc->m_doc_title == "")
  {
    CLog::log("Agora title is empty", "app", "error");
    return false;
  }

  if (free_doc->m_doc_signer == "")
  {
    CLog::log("ag_creator is empty", "app", "error");
    return false;
  }

  QueryRes exist = DbModel::select(
    stbl_agoras,
    {"ag_hash"},
    {{"ag_hash", free_doc->m_agora_hash}});

  if (exist.records.size() > 0)
    return true;

  QString controlled_by_machine = CConsts::NO;
  QString mp_code = "";
  auto[status, wallet_controlled_addresses] = Wallet::searchWalletAdress(
    QStringList(free_doc->m_doc_signer),
    CMachine::getSelectedMProfile());
  Q_UNUSED(status);

  if (wallet_controlled_addresses.size() > 0)
  {
    controlled_by_machine = CConsts::YES;
    mp_code = wallet_controlled_addresses[0].value("wa_mp_code").toString();
  }

  QString full_category;
  if (free_doc->m_parent_agora == "")
  {
    full_category = free_doc->m_doc_title;
  } else {
    full_category = getFullCategory(free_doc->m_parent_agora) + "/" + free_doc->m_doc_title;

  }

  QVDicT values {
    {"ag_title", free_doc->m_doc_title},
    {"ag_doc_hash", free_doc->getDocHash()},
    {"ag_iname", free_doc->m_iname}, // the related domain
    {"ag_hash", free_doc->m_agora_hash},
    {"ag_full_category", full_category},
    {"ag_parent", free_doc->m_parent_agora},
    {"ag_language", free_doc->m_content_language},
    {"ag_description", free_doc->m_doc_comment},
    {"ag_content_format_version", free_doc->m_free_doc_version},
    {"ag_tags", free_doc->m_doc_tags},
    {"ag_creation_date", block.m_block_creation_date},
    {"ag_last_modified", block.m_block_creation_date},
    {"ag_creator", free_doc->m_doc_signer},
    {"ag_controlled_by_machine", controlled_by_machine},
    {"ag_mp_code", mp_code},
  };
  CLog::log("Insert a new Agora: " + CUtils::dumpIt(values), "app", "trace");
  DbModel::insert(
    stbl_agoras,
    values
  );

  return true;
}

QString DemosHandler::getFullCategory(
  const CDocHashT& agora_hash,
  QString path)
{
  while (agora_hash != "")
  {
    QVDRecordsT res = searchInAgoras(
      {{"ag_hash", agora_hash}},
      {"ag_title", "ag_parent"});
    if (res.size() != 1)
      return path;

    QVDicT agora = res[0];
    if(path == "")
    {
      path = agora.value("ag_title").toString();
    }else{
      path = agora.value("ag_title").toString() + "/" + path;
    }

    if (agora.value("ag_parent").toString() == "")
      return path;

    return getFullCategory(agora.value("ag_parent").toString(), path);
  }
  return path;
}

bool DemosHandler::insertDemosDocPost(
  const Block& block,
  const FreeDocument* free_doc)
{

  QString opinion = BlockUtils::unwrapSafeContentForDB(free_doc->m_agora_opinion).content;
  if (free_doc->m_doc_attributes.keys().size() > 0)
  {
    // it is normal html post
    opinion = CUtils::sanitizingContent(opinion);

  } else {
    // maybe need special treatment befor insert to db
    if (free_doc->m_doc_attributes.keys().contains("isPreProposal") && (free_doc->m_doc_attributes["isPreProposal"].toString() == CConsts::YES))
    {
      // convert opinion to base64 and save it in db
      opinion = BlockUtils::wrapSafeContentForDB(opinion).content;
    }

  }

  QueryRes exist = DbModel::select(
    stbl_agoras_posts,
    {"ap_doc_hash"},
    {{"ap_doc_hash", free_doc->getDocHash()}});
  if (exist.records.size() > 0)
    return true;


  QVDicT values {
    {"ap_ag_hash", free_doc->m_agora_hash},
    {"ap_doc_hash", free_doc->getDocHash()},
    {"ap_opinion", opinion},
    {"ap_format_version", free_doc->m_free_doc_version},
    {"ap_reply", free_doc->m_agora_reply},
    {"ap_reply_point", free_doc->m_agora_reply_point},
    {"ap_attrs", CUtils::serializeJson(free_doc->m_doc_attributes)},
    {"ap_creation_date", block.m_block_creation_date},
    {"ap_creator", free_doc->m_doc_signer}};

  DbModel::insert(
    stbl_agoras_posts,
    values
  );

  return true;
}

QVDRecordsT DemosHandler::searchInAgoras(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int& limit)
{
    QueryRes res = DbModel::select(
      stbl_agoras,
      fields,
      clauses,
      order,
      limit);

     return res.records;
}

uint32_t DemosHandler::getParentIndex(
  const QVDRecordsT& indented_agoras,
  QString parent_hash)
{
  for (int64_t i = 0; i < indented_agoras.size(); i++)
    if (indented_agoras[i].value("ag_hash").toString() == parent_hash)
      return i;
  return 0;
}

QVDRecordsT DemosHandler::makeTitlesTree(const QString& selected_iname)
{
  QString indet_gap = "     ";
  ClausesT clauses {};
  if (selected_iname != "")
    clauses.append({"ag_iname", selected_iname});
  QVDRecordsT agora_titles = searchInAgoras(
    clauses,
    {"ag_iname", "ag_title", "ag_last_modified", "ag_parent", "ag_hash"},
    {{"ag_iname", "ASC"}, {"ag_title", "ASC"}});

  // re-generating titles: TODO: Agora view must be a tree and not a table. fix it ASAP
  QVDRecordsT indented_agoras {};

  QStringList sub1 = {};

  for (QVDicT anAgo: agora_titles)
  {
    QVDicT anAg = anAgo;
    if (anAg.value("ag_parent").toString() == "")
    {
      anAg["class"] = 0;
      anAg["ag_title"] = anAg.value("ag_title").toString();
      indented_agoras.push_back(anAg);
      sub1.append(anAg.value("ag_hash").toString());
    }
  }

  // injecting indented layers
  for (uint32_t level = 1; level < 10; level++)
  {
    QStringList subs = {};
    for (QVDicT anAgo: agora_titles)
    {
      QVDicT an_agora = anAgo;
      if (sub1.contains(an_agora.value("ag_parent").toString()))
      {
        uint32_t parent_index = getParentIndex(indented_agoras, an_agora.value("ag_parent").toString());
        an_agora["class"] = level;
        QString indent_spaces = "";
        for(uint32_t j=0; j<level; j++)
          indent_spaces += indet_gap;

        an_agora["ag_title"] = indent_spaces + an_agora.value("ag_title").toString();
        indented_agoras.insert(parent_index + 1, 1, an_agora);
        subs.append(an_agora.value("ag_hash").toString());
      }
    }
    sub1 = subs;
  }

  return indented_agoras;
}

QString DemosHandler::renderPage(const CDocHashT& ag_hash)
{
  // FIXME: add iname_hash sub-domain to clauses also
  auto[status, agora_info, final_posts] = retrieveAgorasInfo({{"ag_hash", ag_hash}}, true);
  if (!status)
    return "";

  // retrieve posts of this Agora

  QString out = RenderHandler::renderToHTML(agora_info.value("ag_description").toString());
  out += "\n<br>";

  QString a_post_string;
  for(QVDicT a_post: final_posts)
  {
    CAddressT the_creator = a_post.value("ap_creator").toString();
    auto[shares, shares_percentage] = DNAHandler::getAnAddressShares(the_creator);
    CLog::log("The author("+the_creator+") has " +QString::number(shares_percentage)+ "% of total shares =" +QString::number(shares)+ "shares ", "app", "info");

    a_post_string = "\n<br>" + a_post.value("ap_opinion").toString();
    if(a_post.keys().contains("attr_actions"))
    {
//      a_post_string+= "\nFIXME! the post need botton to click";
    }
    out += "<table>";
    out += "<tr>";
    out += "<td><span style=\"background-color:#" + CUtils::hash6c(a_post.value("ap_doc_hash").toString()) + ";\" >Date: </span></td><td><span style=\"background-color:#" + CUtils::hash6c(a_post.value("ap_doc_hash").toString()) + ";\" >" + a_post.value("ap_creation_date").toString() + "</span></td>";
    out += "</tr>";

    out += "<tr>";
    out += "<td>Author:</td><td><span style=\"background-color:#" + CUtils::hash6c(CCrypto::keccak256(the_creator)) + ";\" >" + CUtils::shortBech16(the_creator) + " (" + QString::number(shares_percentage)+ " % Shares)</span></td>";
    if (a_post.value("ap_reply").toString() !="")
    {
      out += "<td>Reply to:</td>";
      out += "<td><span style=\"background-color:#" + CUtils::hash6c(a_post.value("ap_reply").toString()) + ";\" >" + CUtils::hash8c(a_post.value("ap_reply").toString()) + "</span></td>";
      int reply_point = a_post.value("ap_reply_point").toInt();
      QString reply_color = "#ffffff";
      if (reply_point > 0)
      {
        reply_color = "#00ff00";
      } else if (reply_point < 0) {
        reply_color = "#ff0000";
      }
      out += "<td><span style=\"background-color:" + reply_color + ";\" >" + QString::number(reply_point) + "</span></td>";

    }
    out += "</tr>";

    out += "</table>";
    out += RenderHandler::renderToHTML(a_post_string);
    out += "\n<br>" + addPostBottomLinks(a_post) + "\n";
    out += "\n<br>  -  -  -  -  -  -  -  -  -  -  -  -  -  \n<br>\n<br>";
  }

  return out;
}

QString DemosHandler::addPostBottomLinks(
  const QVDicT& post)
{
  QString ap_doc_hash = post.value("ap_doc_hash").toString();
  return " <a href=\"replay/"+ ap_doc_hash + "\">Reply</a>";

  QString out = "";
  for (int score = -10; score <= 10; score++)
  {
    if (score == 0)
    {
      out += " <a href=\"replay/"+ ap_doc_hash + "/0\">Reply</a>";

    }else{
      out += " <a href=\"replay/"+ ap_doc_hash + "/" + QString::number(score*10) + "\">" + QString::number(score*10) + "</a>";

    }
  }
  return out;
}

QVDRecordsT DemosHandler::searchInPosts(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int& limit)
{
  QueryRes posts = DbModel::select(
    stbl_agoras_posts,
    fields,
    clauses,
    order,
    limit);

  return posts.records;
}

std::tuple<bool, QVDicT, QVDRecordsT> DemosHandler::retrieveAgorasInfo(
  const ClausesT& clauses,
  const bool retrieve_posts)
{
  QVDRecordsT agoras = searchInAgoras(
    clauses,
    stbl_agoras_fields);
  if (agoras.size() ==0)
  {
    CLog::log("There is no Agora for ${args.iName}/" + CUtils::dumpIt(clauses), "app", "warning");
    return {false, QVDicT{}, QVDRecordsT{}};
  }

  QVDicT agora_info = agoras[0];
  if (!retrieve_posts)
    return {true, agora_info, QVDRecordsT{}};

  QVDRecordsT posts = searchInPosts(
    {{"ap_ag_hash", agora_info.value("ag_hash").toString()}},
    {"ap_opinion", "ap_attrs", "ap_doc_hash", "ap_reply", "ap_reply_point", "ap_creation_date", "ap_creator"},
    {{"ap_creation_date", "ASC"}});

  QVDRecordsT final_posts = {};
  for (QVDicT a_post: posts)
  {
    QJsonObject ap_attrs = a_post.value("ap_attrs").toJsonObject();
    QString ap_opinion = a_post.value("ap_opinion").toString();
    QJsonArray attr_actions = {};
    if (ap_attrs.keys().size() == 0)
    {
      // it is a normal post, so render
      // ap_opinion = RenderHandler::renderToHTML(ap_opinion);

    } else {
      // mabe need special treatment befor displying
      if (ap_attrs.keys().contains("isPreProposal") && (ap_attrs.value("isPreProposal").toString() == CConsts::YES))
      {
        ap_opinion = CCrypto::base64Decode(ap_opinion);
        QJsonObject Jap_opinion = CUtils::parseToJsonObj(ap_opinion);
        QString proposal_HTML = ProposalHandler::renderProposalDocumentToHTML(Jap_opinion.value("proposal").toObject());
        QJsonObject pledge = Jap_opinion.value("pledge").toObject();
        QString pledge_HTML = GeneralPledgeHandler::renderPledgeDocumentToHTML(pledge);
        QVDRecordsT address_details = Wallet::getAddressesInfo({pledge.value("pledgee").toString()});

        // present attr buttons too!

        if (address_details.size() == 1)
        {
          attr_actions.push_back(QJsonObject {
            {"aType", "Button"},
            {"aCaption", " Confirm "},
            {"aAct", "convertCPostToPLRBundle"},
            {"aParams", a_post.value("ap_doc_hash").toString()}});

        } else {
          // attr_actions.push({ aType: 'Button', aCaption: ` Confirm `, aAct: `alert`, aParams: 'You are not the desired pledgee! but you can be, try offer a better loan to user' });
          attr_actions.push_back(QJsonObject {
            {"aType", "Button"},
            {"aCaption", " I can offer better loan! "},
            {"aAct", "alert"},
            {"aParams", "It is not implemented yet!"}});

        }
        ap_opinion = proposal_HTML + "\n" + pledge_HTML;
      }
    }

    a_post["attr_actions"] = attr_actions;
    a_post["ap_opinion"] = ap_opinion;

    final_posts.push_back(a_post);
  }

  return {true, agora_info, final_posts};
}


std::tuple<bool, QString, FreeDocument*> DemosHandler::prepareNewAgoraRegisterDoc(
  const CAddressT& agora_creator,
  const CDocHashT& the_parent,
  QString agora_title,
  QString agora_comment,
  QString agora_tags,
  const QString& agora_lang,
  const QString& agora_iname,
  const CDateT& creation_date)
{
  QString msg;

  agora_title = CUtils::sanitizingContent(agora_title);
  agora_comment = CUtils::sanitizingContent(agora_comment);
  agora_tags = CUtils::sanitizingContent(agora_tags);


  if (agora_iname == "")
  {
    msg = "Invalid/Empty Agora Domain!";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }

  // validate signer (if exist)
  if (agora_creator == "")
  {
    msg = "Invalid/Empty Agor signer(" + CUtils::shortBech16(agora_creator) + ")!";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }
  if (!CCrypto::isValidBech32(agora_creator))
  {
    msg = "Invalid Agora signer(" + agora_creator + ")!";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }

  CDocHashT agora_hash;
  if (the_parent == "")
  {
    agora_hash = CCrypto::keccak256(agora_iname + "/" + agora_title);
  }else{
    agora_hash = CCrypto::keccak256(the_parent + "/" + agora_title);
  }
  QueryRes exist = DbModel::select(
    stbl_agoras,
    {"ag_hash"},
    {{"ag_hash", agora_hash}});
  if (exist.records.size() > 0)
  {
    msg = "The title(" + agora_title + ") for desired domain(" + agora_iname + ") is already registerd!";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }


  QJsonObject doc_json = {
    {"dType", CConsts::DOC_TYPES::FPost},
    {"dClass", CConsts::FPOST_CLASSES::DMS_RegAgora},
    {"dVer", CConsts::DEFAULT_VERSION},
    {"fVer", CConsts::DEFAULT_VERSION},
    {"dCDate", creation_date},
    {"parent", the_parent},   // super category(if exist)
    {"agHash", agora_hash},
    {"dSigner", agora_creator},
    {"dTitle", agora_title},
    {"cLang", agora_lang},
    {"dComment", agora_comment},
    {"dTags", agora_tags},
    {"iName", agora_iname}};
  // TODO: implement a language detection sub-module to automatically sujjest the language or adjust it
  FreeDocument* document = new FreeDocument(doc_json);

  // do sign doc by proper key
  QString sign_message = document->getDocSignMsg();
  auto[sign_status, res_msg, sign_signatures, sign_unlock_set] = Wallet::signByAnAddress(
    agora_creator,
    sign_message);
  if (!sign_status)
    return {false, res_msg, nullptr};

  document->m_doc_ext_info = QJsonArray { QJsonObject {
    {"signatures", CUtils::convertQStringListToJSonArray(sign_signatures)},
    {"uSet", sign_unlock_set}}};

  document->setDExtHash();
  document->setDocLength();
  document->setDocHash();

  Block* tmp_block = new Block(QJsonObject {
    {"bCDate", CUtils::getNow()},
    {"bType", "futureBlockDemos2"},
    {"bHash", "futureHashDemos2"}});
  GenRes full_validate = document->fullValidate(tmp_block);
  if (!full_validate.status)
  {
    msg = "Failed in full validate creat an Agora, " +full_validate.msg+ " (" + agora_title + ")";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }

  return {true, "", document};
}

std::tuple<bool, QString> DemosHandler::buildANewAgora(
  const CAddressT& agora_creator,
  const CDocHashT& the_parent,
  QString agora_title,
  QString agora_comment,
  QString agora_tags,
  const QString& agora_lang,
  const QString& agora_iname,
  const CDateT& creation_date)
{
  // create proper doc and send it to network
  auto[create_status, create_msg, agora] = prepareNewAgoraRegisterDoc(
    agora_creator,
    the_parent,
    agora_title,
    agora_comment,
    agora_tags,
    agora_lang,
    agora_iname,
    creation_date);
  if (!create_status)
    return {false, create_msg};
  CLog::log("New Agora just created: " + agora->safeStringifyDoc(true), "app", "info");

  // prepare payment doc too
  return FreeDocHandler::payForFreeDocAndPushToBuffer(
    agora,
    creation_date);

}

std::tuple<bool, QString, FreeDocument*> DemosHandler::prepareNewPost(
  const CDocHashT& related_agora,
  const QString& post_content,
  const CDocHashT& replyed_to,
  const int8_t reply_point,
  CAddressT post_signer,
  const CDateT& creation_date,
  QJsonArray dAttrs,
  const QString& cost_pay_mode) // also could be "byPoW"
{
  QString msg;

  if ((related_agora == ""))
  {
    msg = "invalid Agora hash";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }

  if (post_signer == "")
    post_signer = CMachine::getBackerAddress();

  /**
  * if post has special attributes to consoder. e.g. {isPreProposal:'Y'} means the opinion is a preproposal
  * plugins can use this feature to customizing post and adding new features to system
  */


  QJsonObject doc_json = {
    {"dType", CConsts::DOC_TYPES::FPost},
    {"dClass", CConsts::FPOST_CLASSES::DMS_Post},
    {"dVer", CConsts::DEFAULT_VERSION},
    {"fVer", CConsts::DEFAULT_VERSION},  // formating version
    {"dCDate", creation_date},
    {"dSigner", post_signer},
    {"agHash", related_agora}, // hash of proper Agora
    {"opinion", post_content}};

  if (dAttrs.size() > 0)
    doc_json["dAttrs"] = dAttrs;

  if (replyed_to != "")
  {
    doc_json["reply"] = replyed_to;
    doc_json["replyPoint"] = reply_point;
  }

  FreeDocument* document = new FreeDocument(doc_json);
  // do sign doc by proper key
  QString sign_message = document->getDocSignMsg();
  auto[sign_status, res_msg, sign_signatures, sign_unlock_set] = Wallet::signByAnAddress(
    post_signer,
    sign_message);
  if (!sign_status)
    return {false, res_msg, nullptr};

  document->m_doc_ext_info = QJsonArray { QJsonObject {
    {"signatures", CUtils::convertQStringListToJSonArray(sign_signatures)},
    {"uSet", sign_unlock_set}}};

  document->setDExtHash();
  document->setDocLength();
  document->setDocHash();

  Block* tmp_block = new Block(QJsonObject {
    {"bCDate", CUtils::getNow()},
    {"bType", "futureBlockDemos1"},
    {"bHash", "futureHashDemos1"}});
  GenRes full_validate = document->fullValidate(tmp_block);
  if (!full_validate.status)
  {
    msg = "Failed in full validate Demos/Agora, " + full_validate.msg + " reply (" + post_content + ")";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }

  return {true, "", document};
}

std::tuple<bool, QString> DemosHandler::sendNewPost(
  const CDocHashT& related_agora,
  QString the_opinion,
  const CDocHashT& replyed_to,
  const int8_t reply_point,
  const CDateT& creation_date,
  const QString& cost_pay_mode)
{

  QString msg;

  // create proper doc and send it to network
  the_opinion = CUtils::sanitizingContent(the_opinion);
  the_opinion = BlockUtils::wrapSafeContentForDB(the_opinion).content;

  auto[prepare_status, prepare_msg, agora_post] = prepareNewPost(
    related_agora,
    the_opinion,
    replyed_to,
    reply_point,
    "",
    creation_date);

  if (!prepare_status)
  {
    msg = "Failed in prepare Agora post document! " + prepare_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  CLog::log("Newly preapared Agora post: " + agora_post->safeStringifyDoc(true), "app", "info");

  if (cost_pay_mode == "normal") {
    // prepare payment doc too
    return FreeDocHandler::payForFreeDocAndPushToBuffer(
      agora_post,
      creation_date);

  } else if (cost_pay_mode == "byPoW") {
    //// push to Block buffer and publish immediately
    //let res = docBufferHandler.pushInSync({ doc: agora_post.fDoc, DPCost: 0 });
    //if (res.err != false)
    //return res;
    //const wBroadcastBlock = require('../../web-interface/utils/broadcast-block');
    //let r = wBroadcastBlock.broadcastBlock({ costPayMode: 'byPoW' });
    //return r;
  }

  return {false, "Unknown payment method!"};
}
