#ifndef DEMOSHANDLER_H
#define DEMOSHANDLER_H

class FreeDocument;

class DemosHandler
{
public:
  static const QString stbl_agoras;
  static const QStringList stbl_agoras_fields;
  static const QString stbl_agoras_posts;
  static const QStringList stbl_agoras_posts_fields;

  DemosHandler();
  static bool insertAgoraInDB(
    const Block& block,
    const FreeDocument* free_doc);

  static QVDRecordsT searchInAgoras(
    const ClausesT& clauses,
    const QStringList& fields = stbl_agoras_fields,
    const OrderT& order = {},
    const int& limit = 0);

  static bool insertDemosDocPost(
    const Block& block,
    const FreeDocument* free_doc);

  static uint32_t getParentIndex(
    const QVDRecordsT& indented_agoras,
    QString parent_hash);

  static QVDRecordsT makeTitlesTree(const QString& selected_iname = "imagine");

  static QString renderPage(const CDocHashT& ag_hash);

  static QVDRecordsT searchInPosts(
    const ClausesT& clauses,
    const QStringList& fields,
    const OrderT& order = {},
    const int& limit = 0);

  static std::tuple<bool, QVDicT, QVDRecordsT> retrieveAgorasInfo(
    const ClausesT& clauses,
    const bool retrieve_posts = false);

  static QString addPostBottomLinks(const QVDicT& post);

  static QString getFullCategory(
    const CDocHashT& agora_hash,
    QString path = "");

  static std::tuple<bool, QString, FreeDocument*> prepareNewAgoraRegisterDoc(
    const CAddressT& agora_creator,
    const CDocHashT& the_parent,
    QString agora_title,
    QString agora_comment,
    QString agora_tags,
    const QString& agora_lang = CConsts::DEFAULT_LANG,
    const QString& agora_iname = "imagine",
    const CDateT& creation_date = CUtils::getNow());

  static std::tuple<bool, QString> buildANewAgora(
    const CAddressT& agora_creator,
    const CDocHashT& the_parent,
    QString agora_title,
    QString agora_comment,
    QString agora_tags,
    const QString& agora_lang = CConsts::DEFAULT_LANG,
    const QString& agora_iname = "imagine",
    const CDateT& creation_date = CUtils::getNow());

  static std::tuple<bool, QString, FreeDocument*> prepareNewPost(
    const CDocHashT& related_agora,
    const QString& post_content,
    const CDocHashT& replyed_to,
    const int8_t reply_point,
    CAddressT post_signer = "",
    const CDateT& creation_date = CUtils::getNow(),
    QJsonArray dAttrs = {},
    const QString& cost_pay_mode = "normal"); // also could be "byPoW"

  static std::tuple<bool, QString> sendNewPost(
    const CDocHashT& related_agora,
    QString the_opinion,
    const CDocHashT& replyed_to = "",
    const int8_t reply_point = 0,
    const CDateT& creation_date = CUtils::getNow(),
    const QString& cost_pay_mode = "normal");


};

#endif // DEMOSHANDLER_H
