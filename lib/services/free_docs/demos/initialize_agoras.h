#ifndef INITIALIZEAGORAS_H
#define INITIALIZEAGORAS_H


class InitAgora
{
public:
  QString m_title = "";
  QString m_description = "";
  QString m_tags = "";
  InitAgora(const QString& title, const QString& description = "", const QString& tags = ""):
  m_title(title), m_description(description), m_tags(tags) {};
};

class AgoraInitializer
{
public:
  static QHash<QString, QString> map_title_to_ag_hash;

  AgoraInitializer();
  static bool initAgoras();
  static bool insertSubCat(
      const QString& in_hash,
      const QString& parent_name,
      std::vector<InitAgora>& def_agoras
      );
  static bool initPosts();

};

#endif // INITIALIZEAGORAS_H
