#include "agoras_init_contents.h"

#include <iostream>
#include <vector>
#include <QHash>


QHash<QString, QString> db_init_agoras_content = {
  {
  QString::fromStdString("3rd Party development-Machine Buffer watcher"),
    R"(
        the documents which will be included in next block are recorded in "i_machine_block_buffer" table.
        we need a plugin to list document and potentially relation between documents (e.g.each non-trx doc needs a trx-doc
        to pay for it)
        in order to have an estimate of buffer situation, befor pushing block to network.
    )"
  },

  {
  QString::fromStdString("Beginners"),
R"(Dear Beginers,
    There are some good place to delve in! Please check the general [[imagine/wiki/faq|FAQ]] and write your questions here.
    )"
  },

  {
  QString::fromStdString("Beginners-Before Start"),
R"(
    Do what you LOVE to do. it is the main standard of imagine. I love help people and also coding, so I do it disregarding getting paid or not.
    this is my full-time mission, and probably can not be your full-time job. but you can dedicate some hours in week too. although if you think deeply about the idea behind it you will be a full-time supporter too.
    please read [[demos/Contribute, Proposals, Earn And Shares|Contribute, Proposals, Earn And Shares]]
)"
  },

  {
  QString::fromStdString("Beginners-Contribute, Proposals, Earn And Shares"),
R"(<div>
    First of all we need to explain some words.
    Contributor/Community member: who is participate in the imagine ecosystem. whether run a full-node or not.
    Shareholder: how has some approved proposals and get paid because of that proposals.
    [[imagine/wiki/contributors|Here]] is a graph of contributor shares longevity.

</div>)"
  },

  {
  QString::fromStdString("Comunication & Encryption Enhancement-iPGP improvement"),
R"(Always PGP)"
  },

  {
  QString::fromStdString("Core develop-Comunication & Encryption Enhancement"),
R"(Core develop-Comunication & Encryption Enhancement)"
  },

  {
  QString::fromStdString("Core develop-Messenger improvement"),
R"(Core develop-Messenger improvement)"
  },

  {
  QString::fromStdString("Core develop-Plugins"),
R"(Core develop-Plugins)"
  },

  {
  QString::fromStdString("Core develop-Wallet API"),
R"(Core develop-Wallet API)"
  },

  {
  QString::fromStdString("Design & Theme Improvement-Logos, either imagine or PAI"),
R"(Design and Theme Improvement-Logos, either imagine or PAI)"
  },

  {
  QString::fromStdString("Development"),
R"(
    There are tons of stuff to do, just check the [[imagine/wiki/todos|TODOs]] list.
    if you know Nodejs, expressjs, angular, most probably you can find something to do. and most important is you can
    help some non-technical friends and install imagine on their computer.
    YES, we need to help together. the friend can be an active contributor, if you dedicate a couple of hours to
    her/him.
    in addition if you already contributed and coded in some of these projects wikipedia, SMF or other foruom softeware,
    Jira, Git, theme & template designing
    then definitely you will participate in one or more projects of imagine.
    and if you are expert in blockchain you do even further. maybe start and preparing some documention and educational
    stuff, and later improving transaction & contracts, and adding planed features.
    or even beter, having brilliant ideas. let's implement it. you can implement new plugins. and if it enough good, can
    be added to core-code.
    and if your experties domain is mobile-app so start to make a nice powerfull wallet.
    probably the software architecture and database design need heavily refactor, definitely we will do it and always
    considering backward-compatibility.
    in such a way no one lose her/his coins and no (intentionally)fork will happend.
    but if someone wants to fork, it is ok too! althougt we suggest to add what she/he desired to existed imagine, or if
    you insist on fork, atleast wait until have a great robust software to relay on.
    if you are cryptographer or security expert, absolutely can help imagine. just take a look at code and make an
    improvment proposal and will be considered in high priority.
    depend on the case the proposal can be public or private. obviousley critical bugs or security issues MUST be
    reported privately to iName "security".
    if you are not sure, just write a short message in related Agora and we will keep in touch.
)"
  },

  {
  QString::fromStdString("Development-3rd Party development"),
R"(Development-3rd Party development)"
  },

  {
  QString::fromStdString("Development-Core develop"),
R"(Development-Core develop)"
  },

  {
  QString::fromStdString("Development-Interface Develop (Angular)"),
R"(Development-Interface Develop (Angular))"
  },

  {
  QString::fromStdString("Development-Mobile Wallet"),
R"(The moblie wallet(AKA mobile client), is one of system pillars. either in UX or system architecture)"
  },

  {
  QString::fromStdString("Development-Pre Proposals and Discussions"),
R"(Here you can post your pre proposals and discust about it.
    also can support some proposals and provide loan)"
  },

  {
  QString::fromStdString("Development-Security"),
R"(Security issues and ...


    in imagine MUST NOT use external resourcees (css, images, trackers, social network icons, ...)
)"
  },

  {
  QString::fromStdString("Economy"),
R"(Economy)"
  },

  {
  QString::fromStdString("Economy-How to determine coins value"),
R"(how to determine Imagine’s PAIs value?

    it has a simple straight forward formula. what you earn becasue of one hour of contribution, more or less should be
    equal to cost of buying one hour service from someone else.
    super clear. you do something and earn X PAIs and in return you supposed to spend x Pais and buy one hour services
    from the other one.
    you must be able to save this amount of value for later, and consequently you must be able to purchase same services
    even after decades.
    lets explain by numbers. currently in imagine every 12 hours 45,035,996 Pais are minted and for a month will be
    almost 2,702,159,760 Pai.
    if you contribute one hour in level 6 you will earn 16,212 Pais per month and it will lasts for 7 years. so it could
    be equal to 16,212 * 12 * 7.
    and for each block (which creates every 12 hours) will be 4 reserve coins with same coins & sahre that can be
    released by block's shareholder magiority consent.
    so you can multiple 4 the coins. the final coins will be 4 * (16,212 * 12 * 7). so you probably earn totaly
    5,447,232 Pais in 7 years because of only one hour helping.
    and if you consider your salary for one hour work (say 10$) so every 1 million Pais is equal 1.83$.
    but it is not the right value. because your are not the only one who contributes to project.
    in fact every cycle the contributors number (and consequently the contribute hours) increases and because of that
    fact Imagine will have more shareholders wherase there are fixed minted coins per cycle.
    it means as long as new contributors join the project your dividend will reduce and can not be monthly 16,212 for
    entire seven years. the decrease rate can not be calculate exactly but can be predict with a good aprox.
    there is a visual tool in [[contributes|Contributes]]. go and change the parameters and you will find some good insight of how dividend
    works.
    but there are some good news! you can continuously contribute to project and accumulate more DNA-shares. in suach a
    way you also increase your voting power.
    and more important fact is as long as more contributors join to project it means the project is more robust and
    complete and comunity is greater, and Pai is more welcomed by people.
    and last but not least ...
    lets explain it later, we need some good experince and best practice on this model of economy. again, it is not
    something designated only for imagine improvement as a giant software company.
    what we are creating is a calture and not a software. the software and its rights all are open and free and will be
    open and free for ever.
    in fact the software, the shares and the coins without community worth nothing.
    Imagine cares to create a community to save the world.)"
  },

  {
  QString::fromStdString("Economy-Inflationary or Deflationary Money"),
R"(Economy-Inflationary or Deflationary Money.html)"
  },

  {
  QString::fromStdString("Interface Develop (Angular)-Design & Theme Improvement"),
R"()"
  },

  {
  QString::fromStdString("Interface Develop (Angular)-Multilingual"),
R"(<div>
    Multilingual content & page
    todo list:
    - changing entire interface to support multilang either static contents or dynamic contents
    - all forms and inputs must accept utf-8 texts
    - the utf-8 text must be recorded in DB in proper format ...
</div>)"
  },

  {
  QString::fromStdString("Interface Develop (Angular)-Wallet Enhancement"),
R"(Wallet Enhancement)"
  },

  {
  QString::fromStdString("Plugins-Distributed Version Control Plugin implementation"),
R"(Plugins-Distributed Version Control Plugin implementation.html)"
  },

  {
  QString::fromStdString("Plugins-Distributed Wiki Plugin implementation"),
R"(Plugins-Distributed Wiki Plugin implementation.html)"
  },

  {
  QString::fromStdString("Plugins-Plugin handler improvements"),
R"(Plugins-Plugin handler improvements.html)"
  },

  {
  QString::fromStdString("Translation"),
R"(<div>
    since spreding crypto-calture is the most important, therfore the translation is important too. imagine intensively
    supports translation(from/to) every languages in the world.
    if you are developer(most probably you are) and know some non-english speaker which are enough good in english and
    local languages, please install a full node for her/him and eager her/him to join the comunity.
    there is no limit for translate proposals & shares as long as the quality is acceptable.
    the translate can be done for imagine software and its content and giudes. aslo can translate educational matterials,
    and also translate discussions and wiki pages.
    so there is endless content to translate (from/to) local languages.
</div>)"
  }


};
