#ifndef WIKIHANDLER_H
#define WIKIHANDLER_H

class FreeDocument;

class WikiHandler
{
public:
static QString stbl_wiki_pages;
static QStringList stbl_wiki_pages_fields;
static QString stbl_wiki_contents;
static QStringList stbl_wiki_contents_fields;

  WikiHandler();
  static bool initTmpWikiContent();

  static QVDRecordsT searchInWikiContents(
    const ClausesT& clauses,
    QStringList fields = stbl_wiki_contents_fields,
    const OrderT& order = {},
    const int limit = 0);

  static QVDRecordsT searchInWikiPages(
    const bool need_contents_too = false,
    const ClausesT& clases = {},
    QStringList fields = stbl_wiki_pages_fields,
    const OrderT& order = {{"wkp_title", "ASC"}},
    const int limit = 0);

  static bool insertPage(
    const Block& block,
    const FreeDocument* free_doc);

  static bool recordInWikiDB(
    const Block& block,
    const FreeDocument* free_doc);

  static QVDRecordsT getOnchainWkPages();
  static QString renderPage(const QString& page_title);

  //  -  -  -  wiki pages
  static bool updateWkDoc(
    const Block& block,
    const FreeDocument* free_doc);

  static std::tuple<bool, QString, FreeDocument*> prepareNewWikiPageRegReq(
    CAddressT creator_address,
    const CDocHashT& wiki_page_hash,
    const QString& wiki_page_title,
    const QString& wiki_page_content,     // the page content, formatted as mediawiki standards
    const QString& page_language = CConsts::DEFAULT_LANG,
    QString wiki_iname = "",  // the domain name of wiki
    const CDateT& creation_date = CUtils::getNow());

  static std::tuple<bool, QString> createNewWikiPage(
    const QString& wiki_page_title,
    const QString& wiki_page_content,
    const CDocHashT& wiki_page_hash,
    const QString& page_language = CConsts::DEFAULT_LANG,
    const QString& wiki_iname = "imagine");


};

#endif // WIKIHANDLER_H
