#include "wikis_init_contents.h"

QHash<QString, QString> InitWikiContent::s_wiki_initial_pages = {

  {
    "contributors",
    R"(
     in imagine, the contributor is who helped the system. either technical or non technical.
     who interested in imagine can propose her/his help and run a polling and the comunity decide to accept the proposal or not.
     if contributor's proposal be accepted the contributor became a share holder too and by this the contrinbutor have the voting rights for next 7 years
     and also get divivedent every 12 hours (one in 00:00:00 AM and the other in 12:00:00 PM) for next 7 years.
     contributor's share amount is calculated based a simlple stright forward formula.
     h = how many hours the contributed spend(or will spend) for that activity?
     l = what was the usefullness level of that activity?
     new shares = h * l
     and this share will be add to imagine's DNA for ever. means this user helped world this certain amount of shares.
     the help level can be between 1-12 and for now we assum all help in level 6.
     this document will be cmpleted soon. but for now the below graph will help you much.
     <br>
     [[File:accumulative-shares-of-alice.png|Accumulative shares of alice]]
     <br>
     [[File:accumulative-shares-of-contributors.png|Accumulative shares of all contributors]]
     <br>
    )"
  },


  {
    "development",
    R"(
    <div>
        there is a [[imagine/wiki/todos|big room]] for developers in evry aspect to join the project.
        and will be exponentially increase where your creativity add to on top of what already exist.
        just check the [[imagine/wiki/todos|TODOs list]] regularly.

        if you know Nodejs, expressjs, angular, most probably you can find something to do. and most important is you can
        help some non-technical friends and install imagine on their computer.
        YES, we need to help together. the friend can be an active contributor, if you dedicate a couple of hours to
        her/him.
        in addition if you already contributed and coded in some of these projects wikipedia, SMF or other foruom software,
        Jira, Git, theme & template designing then definitely you will participate in one or more projects of imagine.
        and if you are expert in blockchain you do even further. maybe start and preparing some documention and educational
        stuff, and later improving transaction & contracts, and adding planed features.
        or even beter, having brilliant ideas. let's implement it. you can implement new plugins. and if it enough good, can
        be added to core-code.
        and if your experties domain is mobile-app so start to make a nice powerfull wallet.
        probably the software architecture and database design need heavily refactor, definitely we will do it and always
        considering backward-compatibility.
        in such a way no one lose her/his coins and no (intentionally)fork will happend.
        but if someone wants to fork, it is ok too! althougt we suggest to add what she/he desired to existed imagine, or if
        you insist on fork, atleast wait until have a great robust software to relay on.
        if you are cryptographer or security expert, absolutely can help imagine. just take a look at code and make an
        improvment proposal and will be considered in high priority.
        depend on the case the proposal can be public or private. obviousley critical bugs or security issues MUST be
        reported privately to iName [[/messenger|"security"]].
        if you are not sure, just write a short message in [[/demos/development/security|related Agora]] and we will keep in touch.
    </div>
    <div>
        plugins: the most common way to develope project without touching core-code!
        if you know wordpress, must probably you can write some awesome plugins for imagine too.
        and lack of document is the big problem.
        there are some good hooks to start e.g. SASH_before_validate_normal_block, APSH_control_if_missed_block ...
        and more in code (imagine-server/plugin-handler/hooks-list.js)
        and obviously tons of feature request hooks will be add to imagine-core.
        feel free to request your needs in [[imagine/demos/Plugins|plugins general discussions]]
    </div>
    )"
  },


  {
    "faq",
    R"(
    FAQ:

        - So you made another sh**tcoin?
        - It looks like a scam. Why you have not any Twitter, YouTube, Facebook account? Why do you want to be totally anonymous?
        - Do you believe in some ideologies or biased some economic doctrine or some particular school of though?
        - Does the scheme provide a basis for distributing new currency?
        - How are new coins distributed?
        - Who can own stocks?
        - measuring in works hours is difficult, and system can be fooled, and juniors get paid better than seniors who will work less hours to produce better work.
        - What are the benefits of having network stock?
        - How will the stocks be divided?
        - Why you get one million initial shares of “imagine” community?
        - This one million initial shares for you, makes any election pointless, since finally you are the person who can accept or reject any proposals.
        - This one million initial shares for you makes you billionaire while the others get a nut.
        - This one million initial shares for you makes you point of failure of system.
        - This one million initial shares for you makes you a good target for bad actors or/and criminals. either because of your voting power or your coins!
        - Why contributors get paid 7 years? Is it arbitrary number? Can it be one year or vice versa 100 years?
        - It is unlikely and also impractical that everyone would evaluate daily the work done by every contributor.
        - How would decision making work for a decentralized network with numerous shareholders?
        - People do not like everyone evaluate their work.
        - There is a perverse incentive for shareholders to undervalue the work by other people (up to zero).
        - If the good shareholders can remove corrupt shareholders, so could corrupt shareholders remove honest ones. In fact, they would probably be more motivated for doing so.
        - What are the consensus mechanism and data structure?
        - You cannot “immediately record the transactions in the blockgraph“
        - That delay could be a serious limitation though. Imagine a exchange transaction, funds must move from A to B, then C, then D, ... 12 hours on every step would make these types of transactions not viable.
        - Why you set 12 hours for each cycle? Couldn't you set it 10 minutes or less?
        - Who handles regular merge of branches? Isn't that making the system centralized?
        - Which node has the privilege over the rest to get the subsidy? How do the rest agree?
        - Since the data structure is DAG, Can the chain be forked without this being noticed?
        - Rather than waiting for 12 hours, you probably want instead to make it work with confirmations from 51% of the shareholders. That could be almost immediate or take several days (worst-case, more than 50% shareholders no longer have active nodes, bringing the currency to a halt).
        - What do you mean by “real decentralized” blockchain?
        - How you can guaranty the security and privacy of Comen's messenger?
        - If everyone can record everything on blockgraph, so very fast the blockgraph will bloat and incapacitate!
        - What languages or platform you used for software and why?
        - Why you do not put the source code on Github?
        - How the network shares and the coins get value in first place?
        - Why “non believers” should want our money in first place?
        - Scarcity doesn't give value to the coin.
        - How the system creates value (in economic term)?
        - Is its money a kind of utility token?
        - How many maximum coin supply will be?
        - What is different between imagine monetary and MMT (Modern Monetary Theory)?
        - How would the system resist against Sybil attacks?
        - It would be nice if the network recorded the status of its DAG in the Bitcoin blockchain and vice versa.
        - PoS is energy savvy, but still is biased in terms of making richer the rich and poorer the poor.
        - The system needs to high-speed internet be accessible everywhere, otherwise not works.
        - why you use email as transporter and just do not use TCP/IP or other decentralized messaging protocols for node communication?
        - Is it possible to have a technology that adversaries can not shout it down, block or seize it?
        - Can the adversaries shout-down internet?
        - Can the adversaries cut electricity?
        - Can the adversaries acquire nodes or key member persons through physical action or cyberattacks.
        - Will this technology will be vastly used by mass?
        - Will these currencies will be vastly used by mass and they will have real usability and applicability?
        - Can these currencies being used in tax evasion?
        - Can these currencies being used in criminal?
        - If a good and useful human community is to be built, it has to have a way to exclude bad actors. The software, built around hard anonymity as it is, has not.
        - The bandwidth shortcomings (TPS) and need to interact with real world, have forced cryptocurrencies users to reinvent (via exchanges) all the financial infrastructure they still believe they have freed themselves from.
        - What features the Comen software has?
        - Does Comen support Smart Contracts?
        - What type of transactions system support?
        - What level of privacy the transactions have in Comen system?
        - What are Comen solutions for privacy of transactions and coins in traceability sense?
        - Will it be really easy to create a new cryptocurrency?
        - Having thousands of different currencies will raise arbitrage and all kinds of other schemes of speculation and manipulation.
        - The market-capital of new monies will not be enough big to influence the real world's market.
        - will get around the power and centrality of the largest economic units the financial and industrial capitals?
        - Some of ideologies essentially do not believed in money at all. So the Comen approach makes no sense to them.
        - What is the software license?
        - Is software a fork? And if not, why not?
        - Why not just use some mix of Tor, VPNs, Orchid, LokiNet, Berty, IPFS, Matrix, etc?
        - What are your main observations to create this software?



    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

        - So you made another sh**tcoin?
    No, I did not just create another coin. Indeed I made an infrastructure to everyone be able to create a new community and run its new coin in a couple of hours, in order to ease small decentralized communities have their voices. without censorship or banning. It is a great survival for Blockchain technology that is currently being held hostage by giant companies, exchanges and greedy traders, who want to be millionaire overnight.
    The software called Comen stands for “community maker engine”. So I developed the alpha version of software and launch it.
    Then as a favorite community, I established a community called “imagine”. I set some rules and power/wealth distribution mechanisms for this community. The most important rule in imagine is “every decision in imagine community must be made by polling between all community members, in proportion to their shares of network.”. imagine is an umbrella community that supports projects that are good for world. The community rules and mechanisms have been explained in different parts of this Q & A.


        - It looks like a scam. Why you have not any Twitter, YouTube, Facebook account? Why do you want to be totally anonymous?
    I strongly believe that Blockchain technology is the greatest innovation which could be used by people to enhance democracy. But in current situation, no one practically used it for this mean. They may not have been allowed to use this technology for common good! What do you think?
    If you read the proposal, immediately you will find why I shouldn't compromise my privacy. I am a weak individual. I have an idea to help people and I have to spread it safely. So let focus on what the software is, since when you want to deal with another person, the source of trust could be one or both of reputation or contract itself. If you have both is good, but even if you know the person, doesn't mean you will go to sign whatever the contract is! Do you? Obviously you read entire contract and every details. Here is the same. You don't know me. So I could be evil or the most trustfulness person in the world. In any case you must concentrate on software and its intention and real behaviors of software and protocol, and not on who created it. Let me know what you do not trust in software and protocol instead.
    The project is not scam, simply because no where I tried to sell you new coins. I clearly state, “YOU SHOULD NOT BUY COINS” from me or the other. It is really important. No one must not buy coins unless being sure about coin community principals and community governance rules and software safety and consensus mechanism and network stability, and so on. This learning curve deserves a long journey, and in your journey you will find it will be much easier to earn coins by contribution in project development, rather than pay paper money to buy coins.
    About why I have not Facebook, google, twitter, reddit… I refer you to some informative posts like:
    Reasons not to be used by Facebook: https://stallman.org/facebook.html,
    https://stallman.org/google.html


        - Do you believe in some ideologies or biased some economic doctrine or some particular school of though?
    I do not want to fit myself or the project in a particular “ism“ or a certain ideology, doctrine, regime or school of thought, simply because the project is a comprehensive infrastructure of human knowledge and experiences. It belongs to whole humankind, since my intention of that strive is human prosperity and I hope your intention is human prosperity as well.
    Indeed it is “the first time in human history” that people are encouraged to do good job and record it in a global transparent ledger and be rewarded by doing this good job.
    It is a practical altruism. It is what we all were looking for from being human in first place and IMHO it is goal the the all ideologies till now.
    Don't take my word for it: Join to netwrok and you'll see.


        - Does the scheme provide a basis for distributing new currency?
    Yes, but the distribution logic has nothing with network or computer power. Instead it based on member's stocks which is an external parameter. That is, members do some tasks, and claim their wage by stating the hours that they dedicate to a particular task. Their claim will be judged by polling through current stakeholders. In case of acceptance of their claim, the stated hours become stocks. Therefor the contributor will get paid in proportion to her/his stocks.


        - How are new coins distributed?
    The treasury income consist of 2 sources and distributed 2 time per day (once 00:00:00 and another 12:00:00).
    Every 12 hours the treasury incomes will be divided between network share holders in proportion to their shares.


        - Who can own stocks?

    Everyone can get shares and coins of imagine community by these A, B, C plans:
    A: They (Skilled people) can directly involve in software development, testing, documenting, translation... which their contribution will be compensate by shares in proportion to the hours they worked for system.
    B: They (entrepreneurs) can hire developers/translators,… and pay them fiat money as their salary and propose that accomplished task to community in exchange of the shares of community network.
    C: They (investors or normal people with small capital) can buy the coins or shares from other early adapters who did A or B.
    In all these 3 cases the early adapters pay less and get more. Today they work less and get more shares/coins comparing tomorrow. Today they can buy coins and shares cheaper then tomorrow.
    Since Plan B and C are enough clear, I go into detail for plan A.
    In order to get network shares, an individual can propose what she/he can do for changing the world to a better situation, and the community decide to accept the proposal or not.
    In every stage of software development which is correlated and paralleled with society development, brand new contributors will/can join to the community. That is, society starts with software developers but definitely does not stop over there. In early days only technicals can run a node, and if you don't have those skills you need to ask a friend or a professional - who knows Nodejs, Angular and a little PSQL - to install software on your laptop, but after releasing mobile app the community will embrace huge number of non technicals. The imagine community approach always is finding more and more and more reasons to join new people to community and providing a chance to them to do something useful for world, and earn shares/coins because of their participation and their goodwill.
    We are not living in a world of limited resources. We are living in a world of mismanaged resources. There is more than enough for everyone. Note that this program is not UBI, since I strongly believe in, no one is totally useless for community that makes her/him a frail consumer. Since in our community,  everybody participates in every decisions, surely we can find great solutions to benefit more and more people, which is where the community gets its strength. We can assign single, simple, “useful” tasks to Incapable people and pay them the network shares in exchange of their real works. If still there will be a very narrow small set of people which they really can not offer anything to community. we can plan a semi UBI as a complementary program beside the imagine's standard monetary.
    Furthermore you (as contributor) are the only one that will assess/claim your job in sense of “usefulness“ for society and not “how skilled/graduated are you“ and the society has the rights to accept your proposal or deny. Pretty much like businesses but in big scale like “commons-based peer production”. For a business what is important is “how much value you create for the business” and likely, for a society what is important is “how much value you create for the society”. Can we have a society that ALL world population are its members? IMHO, Yes. Indeed we already have this society and it is called earth. We just need proper rules and mechanisms to re-organize it.
    Return back to work assessment, in technical point of view, some kind of jobs like programmer, translators, content providers, editors, designers, multimedia providers, and all digital product providers, can easily (somehow) be evaluated in sense of the usefulness for society, the quality of content and quantity of hours she/he/organization spend for that special task.
    In other hand, since our business in imagine has no limit to being a huge multidimensional world business, participants may suggest anything to do (e.g. feeding an elephant in Africa, which cause to sustainable economic growth in that ares, which cause to save purchase power of the population of that area, which cause to being able to buy Zeppelin tickets from our company.), we need to find a way to assess the quality and the quantity of accomplished works. We need trust able “Oracle”s.
    As long as emerging blockchain concept always there was a problem of “Oracle”. That is, how we can correlate the real world information with blockchain information and vice versa. In our case is how the community can evaluate the quality and quantity of a task? Since we embrace all activities that aimed for “common goodness”, there will be a long list of immense different activities like feeding an elephant in Africa or providing a service for handicaps in NY or... How we can evaluate that wide range of activities? Who is qualified for this kind of evaluations? And too many questions like these.
    The “oracles” are people or organizations that we trust them. If they claim person X did Y hours of work we should accept their claim. But how we can trust the oracle itself? There are solutions for that. I will explain it in a pure technical post later, but as a short answer I foreseen a kind of “reputation sub-system” in afotware. That is, people gradually improve their trustfulness level in system. There are tons of algorithms to implement this feature, but for now I explain it in shortest possible way. Each people/organization in system is an entity that has an id (e.g. im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl). Each entity has a coefficient of trustfulness and time by time the entity can improve the trustfulness or lose it. Since in blockchain everything is transparent and everything is registered forever and no one can tamper it, this trustfulness coefficient is an essential value in algorithms. Everyone in imagine community can track the history of activities of an entity and decide to trust this entity or not. In addition, there are some self-regulated mechanism to enhance efficiency and quality of contributor's task over time. In long run imagine coins will be distributed fairly and because of real helping the world. Obviously there are different tools to automate this process and suggest you some probability, graphs, numbers and etc. It is a long technical stuff I will explain it in a technical post. There is also privacy consideration that needs to detailed explanation too. But for now we got a rough idea of imagine's road-map. Careers must be decided based on which problem people want to work on, and People will work out of love. Passion for their work and with a responsibility feeling towards the community.
    Hope it was clear and made you aware of non technical participation.


        - Measuring in works hours is difficult, and system can be fooled, and juniors get paid better than seniors who will work less hours to produce better work.
    Measuring in works hours is a little difficult, but since (almost) every stock holders are going to measure, judge and vote for all proposals, the process will be more professional and anti-cheating. Maybe one can cheat one, but one can not cheat all. For every single proposal, every one can/will vote, so cheater has no chance to propose a low quality job and claim for high hours rewards, and simply community reject her/his proposal. In addition, in long run every contributor makes its own reputation history as well, which will be used for assessing the tasks.
    About juniors vs seniors, there is a coefficient in formula that nominated for “level of usefulness“ of the task. By default all kind of works and all skill levels are equal, over time the community will decide to set different number for different level of skills or different type of jobs. Obviously these decisions must be done after debates and convincing the community. BTW I strongly believe this number must be an integer number between 1-12 due the fact that “No one should earn more in a month than someone else earns in a year”.
    On the other hand  This “1-12“ coefficient is designed to both recognize and encourage reciprocal community service, resist inflation, avoid hoarding, enable trade, and encourage cooperation among contributors.
    Professionals are entitled to charge multiple hours per hour or higher coefficient than 12, but this approach will not remedy any pain in the world, instead they should reduce their rate in the spirit of equity.


        - What are the benefits of having network stock?
    Having these stocks users will get paid every 12 hours in proportion to their stocks. This income lasts 7 years. In other word, users one time participate in network tasks and accomplish a task, but they will get paid for next 7 years. The users also have voting rights in proportion to their stocks for next 7 years. Users can spend their coins in exchange of goods or services. They also are free to sell their stocks as well.


        - How will the stocks be divided?
    It is pretty straightforward. Lets start from first day. I developed a software called Comen. Now it is ready, so I install it and establish a community called imagine. I am the only member of community. I have one million initiative shares. You want to join to network, so you find an interesting task to do (say tweak some css files). You dedicated say 5 hours of your time to accomplish the task. You send a proposal about what you did and run a polling on blockgraph. All community members (currently only me) vote for your proposal in proportion to their shares. They can accept your effort or reject it. If you get approved, the new total shares of project will be 1000000 + 5 shares for you. In next cycle the treasury incomes will be divided between 1000005 shares. 5 shares for you and 1000000 for me. Once your proposal be approved you have voting rights too. That is, you have 5 shares and can vote for new proposals (including your future proposals). As long as you (and others) contribute in project development you get more stocks and relatively my stocks power decrease.
    To making it more interesting for contributors, every shares last 7 years, means you will earn coins for each approved proposals for next 7 years and you have voting power for next 7 years too.
    I started with a “Benevolent dictator for life” BDFL and hopefully very soon community became a real community driven management, and all my shares will be amortized.


        - Why you get one million initial shares of “imagine” community?
    It is because of the breed the idea, dedicating 3 years full time to work on it, writing MVP “Comen” software, and still striving to present it to professionals and convince them to join project. And I set these shares Only for imagine Community. The other communities can run Comen software freely, and form their community. They can decide about their money, treasury mechanisms, shares or initial shares, rules and governing mechanisms in the way they prefer.
    In imagine community I have the mission to be sure the Comen software will be developed in most secure and high quality process. Therefore I needed this shares to guaranty that software is following the principles (to know principles please read the “What are your main observations to create this software?“).
    It could be any number but I intentionally chose one million to make an offset in shares calculation and reduce the contributors impact exponentially, since the shares are accumulative for 7 years for each single contribution. Please read also the 4 another questions in sequence.


        - This one million initial shares for you, makes any election pointless, since finally you are the person who can accept or reject any proposals.
    Please read the FAQ: “There is a perverse incentive for shareholders to undervalue the work by other people (up to zero)“.
    Short answer: I have to vote honestly, like other small share holders. otherwise our community never take place.


        - This one million initial shares for you makes you billionaire while the others get a nut.
    Do you really think I can spend even 0.000001 percent of this coins without compromise myself or putting myself in danger? I have to spend almost ALL of my coins for “common good“, “society benefits”, organizations and charities, and I will do that definitely. Indeed 99.99 percent of my coins will be injected to circulation by alternative mechanism. That is, “limited board member” decision of funding projects.


        - This one million initial shares for you makes you point of failure of system.
    That's true, because of that we must implement a kind of revocation system by which if the community “feels“ I am not the honest, goodwill one who started the project (for any reason), they will be able to kick out me and burn my shares at all.
    In this way I will have a safety margin as well, and I will not be the person of interest of some bad actors who wants to ruin the ecosystem.


        - This one million initial shares for you makes you a good target for bad doers, either because of your voting power or your coins!
    Same answer as above.



        - Why contributors get paid 7 years? Is it arbitrary number? Can it be one year or vice versa 100 years?
    The 7 years is not an arbitrary number. I am not the fan of speed, since the speed causes to centralization. it is intentionally long to give people a sensation of serenity and wipe out the fear and stress from life. It is a kind of insurance but by changing the Point of view. People do not paying in advance for an uncertain future, but they will earn in future unconditionally. it is a huge improvement in life experience. just imagine this year you work enough, and for next 7 years you will not only survive but also enjoy your life.


        - It is unlikely and also impractical that everyone would evaluate daily the work done by every contributor.
    Every new proposals by default are approved. So if no one go to vote, the proposal will be approved after polling span date. The network members already knew that if new proposals approved, a part of income of current members will be cut in favor of new member. Because treasury income is FIX. So new members means dividing the same coins between more people. Therefore the community members have enough incentive to participate in voting and rejecting the new proposals.
    In old classical human interaction this kind of decision making was too costly and nearly impossible, but with blockchain technology it is absolutely possible, even for entire world population.
    Although it could be more efficient if we implement a kind of delegation “sub-system”. I intentionally name sub-system, because what we are going to implement is a liquid-democracy, in which everyone can vote individually or delegate it to parties or NGOs or a group of specialists. Adding ZKP (Zero Knowledge Proof) on top of it, we have capacity for running thousand secure and private polling per day.


        - How would decision making work for a decentralized network with numerous shareholders?
    The answer is voting for every single decision in proportion to the shareholders shares. Is it feasible? Absolutely yes. We need just to consider an offset time. That is, if we run a polling for 10 million participants in one hour time span, most probably entire system will be crashed. But if we run it in a 12 hour time span, system can handle it successfully. By this reasoning we can estimate that ruing a pooling for 8 billion entire world population is feasible yet. We just need to consider enough long time span for voting allowed date.
    By the way, there are proposals to improve polling mechanisms and in time we will implement those as well. For example, a kind of delegated voting can be implemented, in which every person can vote either directly or delegate her/his vote to a group of professionals or an NGO or organization. In this way, the person do not need vote every time for everything, but when she/he desired can vote individually and the network prioritized individual votes on delegated votes.


        - People do not like everyone evaluate their work.
    - It is only a mental barrier. If I know some professionals going to judge my stuff, I won't close myself, instead I learn and improve myself.
    - not necessarily ALL HAVE TO VOTE or ALL WILL VOTE, only 51% confirmation will be enough for approving a proposal.
    - not necessarily ALL WILL vote, since voting is not free (like every activities in this network). The voter must pay a little ballot fee.
    - after all, over time we can improve polling system to some more efficient alternative systems. For example we can implement a liquid democracy model in which Voters can either vote directly or delegate their vote to other professionals or we can use some quadratic voting where individuals allocate votes to express the degree of their preferences, rather than just the direction of their preferences.


        - There is a perverse incentive for shareholders to undervalue the work by other people (up to zero).
    Let imagine the network consist of me myself only as initiative shareholder. Obviously I get all new money, but what is the point? If I do not find somebody whom accepts my money in exchange of her/his goods or services, the money worth nothing forever.
    Because of human nature, we prefer to keep more coins for ourselves. If it is not possible so we will divide coins between as less as possible people, and if this is not possible too, we will divide it between our clan.
    But none of these 3 approaches will help our network -particularly the value of our money-, because “The money's value come from the exception of future purchasing power”. people accept the coin in exchange of their good or services, because they believe in tomorrow can pay that money and buy another goods or services.
    Therefore the network has to be expanded and embraces new members to raise up the money usability and applicability in order to advance market range. Bigger community means less daily coins for each, But also means more usability and applicability for the community's money.
    Hence community has to be grown and every decisions are made by polling, the community has to add more and more honest person and not corrupted one. Otherwise community dies in early steps and their money will never be a valuable asset. Implicitly there will be no reason for developer to participate in development any more.


        - If the good shareholders can remove corrupt shareholders, so could corrupt shareholders remove honest ones. In fact, they would probably be more motivated for doing so.
    It is possible for both groups, but we should consider the motives of establish a network. If I start a community, my motives are all about “common good”, and I strongly stand for it. In sequence who join the network and I approve her/him participation, will have same intentions or at least covers large part of my intentions. So over time the community which formed around certain principals, contains the members that have almost same mindset. BTW “this network” and its “rules” and “value of its money” all are governed by majority of community via polling for every single decision. If majority are good, the entire community and its money will be good, otherwise community and its money will collapse -let alone the fact that this kind of corrupted community can not be formed in first place-.
    After all, the software is and will be free(libre) and open source. In worst case, if the 51 percent bad people can dominant the network, the 49% good member of community can fork new coin and resume the network and history, and chain info and every single details of network in new graph. They only dismiss the shares of 51 % corrupted and everything will be fine. Note that, the good community do not need to dismiss money of bad actors. They just need to reform the community with almost no cost. we are talking about a community that governing their own money and proper rules in order to expand community members, rise up community reputation and strengthen their money.


        - What is different between imagine monetary and MMT (Modern Monetary Theory)?
    In MMT the government issues easy cheap fiat money out of thin air with no limitation and spends it in tribalism, dissipating, and almost not democratic way, but in imagine, the CODE decides how much money should be issued (regulation), whereas the COMMUNITY decides what activity should be done (economy planing), who should accomplish activity (Social Progress), and how much money for that activity should be paid (wealth distribution).


        - What are the consensus mechanism and data structure?
    The consensus mechanism is PoS which I prefer interpret it to Proof of Solidarity - Solidarity to system - , means shareholders guaranty the network health. The shareholders solely are person who did something to improve ecosystem in whatever form of contribution. There is no ICO, no exchanges, even no fiat money! And the data structure is DAG, so it is better to call the project as a block-graph and not a blockchain.


        - You cannot “immediately record the transactions in the blockgraph“, since there is no one single blockgraph. Remember you are working with several lines that are then merged. That's why you have that settlement time of 12 hours (it could be any other amount as well, like 5 minutes). That's an upper limit on which you expect any issue would be solved. It's not clear how you would solve conflicts, either. Remember, it is *distributed*.
    It is very complex to explain it by I'll try to do my best, hopefully I will create an animation to explain it well, but for now:
    Indeed we record the block and its transactions immediately on graph(DAG), and it will not reversible at all, but recording a block in graph doesn't mean all of its transactions are valid. So after 12 hours a transaction will be matured and its outputs will be spendable.
    After 12 hours if we find there are two or more transactions which are using same inputs but spending it for different addresses, it is a double-spend attack and we have to resolve it. Either we determine the first spend as a valid one and reject the rests, or we detect ALL transactions as invalid and seize the inputs in favor of network treasury to penalize the attacker. So lets delve into how we determine valid transactions.
    Most likely after 12 hours, all leave blocks in last 12 hours are merged at least one time and now we can see entire blocks in history of all current leaves by moving back in leave ancestors.
    For simplicity we assume two transaction x, y that are using same input and spending in different outputs. Assuming transaction x take placed in block X and transaction y take placed in block Y. The problem is either blocks X or Y can claim they are created before the other one (since the nodes creates these blocks and attacker can run million cheater nodes). So we have XcreationTime and YcreationTime. Since we have a network and inevitably we will face propagate delay as well. So, for each node there will be XreceiveTime and YreceiveTime, which can/will be different for each node. Each node has to decide the order of receiving blocks and broadcast it to all neighbors. The nodes have 12 hours to this “informative phase”. Of course the node do not this “informative activity” for all blocks (avoid overwhelming network). They will start it immediately after founding double-spend occurrence and only for that particular blocks. So after 12 hours every nodes have votes of every other nodes about the receive order of blocks X & Y. Each node has a certain amount of network shares and a vote about order. These information is already distributed in entire network and now each node can decide about which transaction is valid and which not. The result for entire nodes will be same. Each node uses the (vote * voterShares) to evaluate the blocks order. Note that, for each node the order is important and the node do not consider to the percentage of this agreement. So a node can figure out the X is before Y by 10 percent agreement and the other node reasons for same result X is before Y but with 99 percent agreement. Both are ok and X is valid transaction. In this formula each node uses only XreceiveTime and YreceiveTime as the unique fact that a node can trust independently.
    There are some way to cheating, but for the sake of brevity I stop these conflict-resolution-process explanation and hopefully later in that animation I'll explain all possible attack scenarios and the solutions as well. But as a last important part of puzzle I would like to note about two important timestamps that limits attackers ability. The XcreationTime & YcreationTime are the parameters that the attacker can set freely. The only control the nodes doing is “if the date is not in future”. But after 12 hours, in “import matured transactions” if there is double-spending and the gap between XcreationTime & YcreationTime is less that 6 hours, definitely both transactions will determined as invalid transactions.


        - That delay could be a serious limitation though. Imagine a exchange transaction, funds must move from A to B, then C, then D, ... 12 hours on every step would make these types of transactions not viable.
    Exactly, I do not like exchanges, so do not care about this kind of fake speed and money transfer.
    This emphasize on speed and technology adds nothing to our real life quality, but rather changes the constant factors of speed and marginal cost, and raises the barrier for participation arbitrarily high. Emphasize on speed cause to more centralization and as always giant actors are winner. If the system is designed for people (whom some of them do not get paid after months of “forced labor”) so 12 hours is good enough. This is not a money for speculation at all. Meanwhile there are some mechanisms to improve user experience. As an example users can hold their money in a kind of 10, 100, 1000, 10000 PAI coins. In such a way they can spend partially and still have money for next spend (before 12 hours which their change will be spendable).


        - Why you set 12 hours for each cycle? Couldn't you set it 10 minutes or less?
    First of all the system in whole is far beyond a only money transfer rail. Even in a money transporter functionality I designed a system that can work even in 3rd world countries (because of slow and limited internet connection) and covers propagation delay. This system is intended to prepare services for real no-banked people.
    Meanwhile system must be resilience for any kind of sabotage. One most sabotage is slowing down the internet or breaking the connections at all. Again, for most general financial activities, 12 hour is pretty good time span.


        - Who handles regular merge of branches? Isn't that making the system centralized?
    When a node wants to create a new block, it has to put the hash of all current leave blocks in header of new block. So the node will includes all leave blocks it knows and they are already existed in its local machine DAG. It is a merge, and each node do it several times in day, and all nodes doing it as well. Then the node broadcasts new block to network. The neighbors will receive this new block and validate it and add it to their local DAG as a new leave block. There is no obligation for a node to link to all leaves in new block and there is no obligation for nodes to not rejecting some particular leaves as well. If a node do not link to a block, probably another node will link to that block and broadcast the new leave block. So at a certain time there will be different nodes with different leaves and definitely in that certain time we can consensus on DAG members older than 12 hours.
    Most probably, the blocks with age older than 12 hours are “synchronized” in all nodes, with same ancestors and same descendants. There will be a catastrophic situation in which a group of nodes do not accept the block of certain group of computers or the group of computers for any reason do not accept transactions of certain accounts. Maybe because of conspiracy or political decision. In these cases we will face fork, and like other blockchains it must be resolved or will cause two or two hundred forks.
    It looks like a tragedy, but for me it is ok as well, since my proposed network is formed around the mindset of its population. That is, the network has some “principals and rules” that they are coded in software. The participants of network primarily accepted the rules, otherwise they never would join to network. The “network”, the “network's rules” and the “network's money” are essentially “social contract”. If there is conflict in any of these issues, either we can solve it or a part of community will leave it and create a new community with new money, pretty much like a democracy, but with “no cost”. Nothing is bad. I guarantee we will never face this kind of problems in our network, due to the nature of this network and its “community based” money.


        - Which node has the privilege over the rest to get the subsidy? How do the rest agree?
    Comen has not “miner”s. we have two different entity in network, the share holders which earned shares by participating in software development (or another kind of develop e.g. design, test, translate, tutorials, contents…) and we have the backers that dedicate their machine to network and run full-nodes and maintain the blockgraph, validate transactions and so on. Since we have not PoW and there is no mining, the only incentive for backers is services cost (e.g. transaction fees, domain register costs, message cost, etc...).
    In early days the shareholders and backers are same person. The backer also have to create the coinbase block every 12 hours, it is not important which backer create the coinbase block since there is no reward or subsidy for block creator. It is possible in a cycle we have 2 or more coinbase block simultaneously which are created by different backers. That's ok too. Since the shares are already known for all backers (full nodes), the money supply is fix and the treasury income is clear, so every backers can create the same coin base block with same information and same hash. So it really doesn't mater which backer create it. There is a pseudo random mechanism to select a backer to create coinbase and propagate it. The system could run without this random selection as well.



        - Since the data structure is DAG, Can the chain be forked without this being noticed?
    Since the used structure is a DAG and not link-list, therefor in any given time for any machine we will have different graph state with different leaves. Therefore theoretically there will be maximum n forks in which n=number of nodes. But it is not important, because we always working on the blocks with the age older than 12 hours. Therefore the graph members which are older than 12 hours must be same for all nodes.



        - Rather than waiting for 12 hours, you probably want instead to make it work with confirmations from 51% of the shareholders. That could be almost immediate or take several days (worst-case, more than 50% shareholders no longer have active nodes, bringing the currency to a halt).
    As I explained before, to resolve the conflict it is just enough to after 12 hours determine the order of blocks by each node. So there is no need for wait for 51% percent, but definitely there is a threshold for minimum confirmation. If the network can not provide even this minimum threshold it is not safe at all.
    But there is a threat for network in which the shareholder do not run nodes. That is, we have two different entity in network, the share holders which earned shares by participating in software development (or another kind of develop e.g. design, test, translate, tutorials, contents…) and we have the backers that dedicate their machine to network and run full-nodes and maintain the blockgraph, validate transactions and so on. Since we have not PoW and there is no mining, the only incentive backers is transactions fees. In early days the shareholders and backers are same person, but over time the backers will be a group of people that they have small portion of network's share or they have no shares at all. It could be a thread in case of conflict resolving. We can implement a kind of delegate sub-system in which the share holders rent their shares to backers and give them delegation to judge “ONLY in double-spend-resolving” and no more grants. BTW it is an open issue to consider in future.


        - What do you mean by “real decentralized” blockchain?
    According a post from Vitalik (https://medium.com/@VitalikButerin/the-meaning-of-decentralization-a0c92b76a274) There are actually three separate axes of centralization/decentralization.
    - Architectural (de)centralization - how many physical computers is a system made up of? How many of those computers can it tolerate breaking down at any single time?
    - Political (de)centralization - how many individuals or organizations ultimately control the computers that the system is made up of?
    - Logical (de)centralization - does the interface and data structures that the system presents and maintains look more like a single monolithic object, or an amorphous swarm? One simple heuristic is: if you cut the system in half, including both providers and users, will both halves continue to fully operate as independent units?

    And I am going to add another more important axes to them.
    - Protocol (de)centralization - who and how decide about the protocol?
    In decentralized systems “the PROTOCOL is all”. It is not important what language you are using to develop software or what medium you are using to transfer data between nodes.
    The most important definition of a decentralized system is laid off in its “protocol.” It defines consensus rules, validation rules, governing rules and practically ALL decision making processes.
    To discover if a product is decentralized or not, you just need to ask who and how decide about protocol of project? Is it a company that registered in a jurisdiction? If the answer is yes, the product is centralized, even if they didn't aim to be.
    And the sad story is even Bitcoin couldn't resist against centralization, when the core development limited to a particular company and an small set of developers (https://www.reddit.com/r/btc/comments/7lio87/debunking_blockstream_is_3_or_4_developers_out_of/) . In Bitcoin's case, there are also other point of centralization which are miners and exchanges that for the sake of brevity I am not delve into it now.
    How we can prevent it in our Comen software? We MUST implement mechanisms by which not only developers and developer's employer decide about Protocol but also individuals – either developers or non-developers - can impact on future development of the “protocol”. For detail read the https://community.hackernoon.com/t/why-i-had-to-create-a-new-crypto-value-and-how-it-finished “network architecture” please.


        - How you can guaranty the security and privacy of Comen's messenger?
    What people are really missing with Lavabit/Skype/Signal/Zoom or whatever email/messenger in future is the fact that “End to End” encryption makes no difference in privacy level, if the service is ONLY ACCESSIBLE FROM A SINGLE APP provided by the service provider who can force an automatic update. In other word no messenger of no company will be secure and private, since sooner or later they HAVE TO implement at least one backdoor for governments.
    The only solution is an “OPEN STANDARD” for communication that can be used by different clients, developed by different companies. Such an open protocol can be implemented only in decentralized way, and maintained only in decentralized way too, as long as there exist a decentralized decision making mechanism about “protocol”.
    We can implement this standard in our software (Comen), because we are not bind to any jurisdiction and we have not single point of failure, we are not a company that doing this for profit and particularly we already have a real “decentralized decision making system”. That is, every single new user of software – despite the fact that is she/he a developer or a normal user - is a new shareholder, in proportion to her/his help to system. Each new user means a new person for decision making.
    It is possible because we are not implementing the software for making profit -unlike the all other companies -, but we are making an infrastructure that on top of this infrastructure there is the possibility of earning benefit which has not effect of the “protocol” functionality.
    Although implementing a protocol is the easiest part and the hardest part is convincing other companies to use it in their software. We start to use it in our software and make good reputation for our protocol. And as always when I talk about “we” I mean all technical and non technical individuals from all over the glob (with no discrimination on age, sex, race, nationality, religious or political view,... ) that are participated in ecosystem development and have shares in system.
    In short run we can use our software to encrypt messages, then passing encrypted messages via API to a commercial service (say Telegram messenger). The telegram carries it to receiver and receiver's Telegram will deliver it to receiver's Comen. At the end the receiver's Comen will decrypt the message for user.
    In such a way we do not need to trust Telegram, Gmail, Facebook or whatever abusive giant company and use them ONLY as a transporter for messages.
    In long run we will implement our full-fledged routing protocols and algorithms and we will not need these greedy companies at all.
    The road-map of privacy enhancement is:
    - reducing passive eavesdropping by using encryption always and everywhere.
    - hardened end-to-end to avoid active man in the Middle (whether known apps such as Whatsapp messenger or hidden actors).


        - If everyone can record everything on blockgraph, so very fast the blockgraph will bloat and incapacitate!
    First of all, users have to pay for recording data on blockgraph, and nodes get this money in their wallet. The cost of different data type are different. e.g. transaction, wiki page, weblog post or video stream have different prices.
    Even if users pay for record data, the blockgraph will bloat fast, but there are solutions too. We can solve it by simple “Demand and supply“ market rule.
    In Comen design, data is divided in 2 classes.
    A: Essential data
    B: optional data
    The essential data are the core data about transactions which are compressed and small. They are necessary and each node has to record and maintain these data (either full history of transactions or pruned version is ok), whereas the optional data are cumbersome and each node may record it or not.
    BTW if a given node needs some optional data which doesn't exist on local memory, it can purchase this information from other nodes. The mentioned node can also sell this data to other nodes -if there is a demand for it-. In such a mechanism some nodes may prefer to act like a Long Term Data Backers and making a passive income by selling data, and the others just maintain the necessary data.
    It is a free market for data. As we know, the storage nowadays are very cheap, so most probably major percent of nodes prefer to store entire blockgraph (including transactions, wiki pages, weblog posts, even video podcasts) on their passive hard drives and earn money by selling those information.
    At this point we can also use CDNs or better calling BDNs to provide fast, reliable, distributed storage over the glob. It will be easy to write a plugin to connect Comen to any commercial CDN company and vice versa.
    Recorded data can have expire date as well, so the recorder may renew the data rent regularly.
    Nodes can manage what kind of data to be maintained or not. In addition, some customized application can be implemented as a plugin for Comen. So this app will use blockgraph space only as a proof of existence and they share data in between in form of big blocks of data. For example a supply chain software can be a plugin on top of Comen. So this software(plugin) will be installed by business partners and they just record the hash of goods allocations or stats on block chain and real big data will be transferred via FTP or what else between partners.
    At the end of the day the nodes use UBL either for trade real goods and services in smart contracts or for trade recorded data (either encrypted or not) on blockgraphs.


        - What languages or platform you used for software and why?
    The software comprise 2 different apps.
    1. imagine-server: which in crypto-currency space means “client” code.
    2. imagine-client: which is the machine's GUI (via web browser) or simply our interface.
    I intentionally choose JavaScript. It is fast for implementing, and easy to reading and understanding protocol's logic. By using JS we can expand developers member, since the project approach is to involving more and more people in project.
    Because of this design, every middle developer can use the core “hooks” (like Wordpress implementation) and create innovative products on top of Comen core code. It creates a huge chance to people participation and flourish the product.

    These are all technologies I used in Comen software.
    JavaScript, NodeJs, ExpressJs, Angular, Postgresql
    Since we have not limited budget, we easily can implement client in different languages such as c++, Rust and also different interface for different devices such as mobile app.
    We can have a full-node pre-installed on Raspberry Pi with full blockgraph history.


        - Why you do not put the source code on Github?
    I do not like Github policies. They discriminating users and filter some users. https://techcrunch.com/2019/07/29/github-ban-sanctioned-countries/ and there is no guaranty to not extending the list in future, or even worst, targeting and banning individuals. In order to address this flaws I planned to add version control software as a plugin on top of Comen blockchain. This software must be completed, but for now, the Comen developers can download the commits by exploring the committed blocks in blockchain and apply this commits on local machine with whatever source control version they preferred. Over time this plugin will be full-fledged decentralized VCS, that will be used to versioning not only the Comen software but also as a decentralized service would be used for every project.


        - How the network shares and the coins get value in first place?
    In early days the network shares and the coins are absolutely worthless. No one want these shares and coins, except the “believers”. Who are the believers?
    The core value of software is networking and establish a community of minded people (AKA “believers“). These person can be technology enthusiasts or human right activists or both.
    When I talk about “technology enthusiasts” I am talking about nerds, gigs and/or Open source software participants that are curious for new technologies, and when I talk about “human right activists” I am talking about all “freedom of expression” proponents and/or “privacy right” defenders and/or “free society” fighters etc.
    In order to know who can be a believer please read FAQ: “What are your main observations to create this software?”
    We are planing to gather all these person in our network. They will collaborate together in order to empower the software and/or its community. These kind of people already are doing same activities volunteer and for free. There is enough room for ALL to contribute in project and being compensated by network shares and coins.  Each single participation in project increases the qualify of software and/or the quality of its community, and this increment will increase the value of the money of the community.
    The outcome of this collaboration is a robust software and an excellent community of software developers and/or users.
    This community has its money which is worthy for them and they pay/accept this money in their trades in exchange of goods (either virtual or physical) and services in between.
    Over time the community get bigger and bigger. There are incentives for community to embrace more and more new people. For detail look at the FAQ: “There is a perverse incentive for shareholders to undervalue the work by other people (up to zero)”.
    The bigger community will find more and more applicability and usability (markets) for their money. These new usability of money increases the demand for this money. Since the money supply is fix, the value of the money will be rise up.
    Sooner or later we will touch “pizza day“ for our coins, the day one will buy a real pizza in exchange of our coins (let say 1 million coin for one pizza). And if we do well this day will be next year, and very soon we will touch “Equality Day“ in which one coin worth one Dollar.
    We start with “believers” but we do not stop over, since not “believers” can do one of these 3 plans of “Who can own stocks?” to get our money:
    In all cases:
    Today you work less and earn more compare to tomorrow.
    Today you pay less and buy more shares/coins compare to tomorrow.



        - Why “non believers” should want our money in first place?
    Our money is a kind of safe for the people who are under threat by numerous powerful hostiles -e.g. minorities -.
    It is investment for normal people who are worried about devaluation of inflationary national currencies.
    It is a job/side job for skilled, semi-skilled people by which they can earn cryptocurrency.
    In addition, Our money has all features of a good money in contrast of cheap governmental paper money.
    You will find more on FAQ in the answer of:
    “Scarcity doesn't give value to the coin.”
    “How the system creates value”





        - Scarcity doesn't give value to the coin.
    The early value of coins is grounded in demand for pay services cost. That is, network users have to pay cost for network services(e.g. register a domain name, send message, run a website, a video channel or a forum on top of blockchain, record a file -text, media, BLOB- on blockchain and so on). The need for using these services drives a demand for coins.
    On the other hand, the scarcity is one of the features of a “good money”. I state the features of a “good money” here, and I believe in money of our network will be a good money.
    Generally, the good money must have intrinsic value, Something like “labor theory of value“ but do not stick that too much. The good money must appreciates its value over time. That is, doesn't lose its purchasing power even after decades. It must be regulated and do not fluctuating too much. It must be scarce and has utility value, meanwhile it must has optimal granularity. The good money must support “financial sovereignty” and must not be seizeable, also must be cheaply transferable and fungible. The good money definitely must have 3 classical factor of money. Unit of account, Medium of exchange, Store of value.
    If you issue a good money, definitely people will buy “good money” in exchange the bad, cheap, paper fiat money.


        - How the system creates value (in economic term)?
    “The software” and “its network” and “people who formed this network” in whole, they are creating value, in various ways. The primarily value they are creating is the software itself, although the software is free and no one pay for it, but people use the software and its services (e.g. send encrypted messages, publish un-stoppable contents, , transfer money... ) and pay service costs that goes to network maintainer pocket as a kind of income. By this cycle, we created a money exchange ecosystem in which there are producers and consumers.
    The second goal for community's developers is adding more and more useful features to software, in order to eliminate all Bitcoin shortcomings and make the money more popular and user friendly. Every small improvement increases the value of total ecosystem. There are immense features and functionalities we can create on top of this software and its network.


        - Is its money a kind of utility token?
    No, the tokens are indeed the coins which are earned because of working for system and accumulating network shares, and coin owner can sell coins or spend it in order to use network services. No one can buy shares by paying to me or someone else. Although everyone can pay fiat money to someone to work for him and develop Comen software and grab the software shares in exchange of fiat money.
    If you mean a kind of “non-fungible token” Of course we can implement this kind of tokens (like Ethereum ERC721) on top of our network in order to implement a tokenized economy for shared assets management.
    It is one of million possibility of our future development of the software.


        - How many maximum coin supply will be?
    imagine has a cyclic coinbase which takes place every 12 hours (one in 00:00:00 and the other in 12:00:00). Minted coins for each cycle is not fix, it could be Maximum 2 power 52 microPAIs, but actually it is 1 percent of that number. As long as increasing the number of contributors (and in sequence hours of contribute) this percentage also grown until reaching 100 % of all 2 power 52 microPAIs.
    The exact formula of “inflation rate” is situated in source code, to find it just look for WORLD_POPULATION.
    In additional for each block there are another 4 blocks with same amount of minted coins which act as “anti-deflation reserve”. Which means the block can/will be released in case of share holders of that particular block, voted to release it and reserve-time-lock is exceeded. For all 4 blocks there are different time-lock. First block, is releasable after 1 year of minting related coinbase block, second after 3 years, third after 6 years and forth after 11 years.
    The Halving period is 20 years. The numbers are intentionally big and periods are long to calming down and give hope to future to ALL PEOPLE in the world.
    So as a response for question the max coin supply will be
    (2 power 53) * 2 * 365 * 20 * 5 = 657525545596092400000 micro PAI = 657,525,545,596,092 PAI in next 1040 years. But real coin supply will be far less.
    You may ask why the maximum supply must be known in advance and why it must be limited? The answer has two different aspect. In economy the scarcity is an important factor for a thing to be able to be a medium of exchange. There is another important rule. As we can not assume there will be unlimited water, forest, oil, oxygen, insects, stars and… for ever, Subsequently we can not assume that there will be unlimited money unless we are a trickster. The economy cake will grow up continuously, but it means more product by less effort (time or work force), and this economy growth has nothing with printing money.




        - How would the system resist against Sybil attacks?
    Our consensus mechanism is PoS means shareholders guaranty the network health. Since the only way of getting network shares is working for ecosystem, the Sybil attack will not happened at all. Every bit of critical data must be validate and signed by majority of shareholders in proportion to their shares. So million suck poppets have no effect on network health. Additional security level in Sybil attack prevention is implemented by neighborhood restriction. That is, unlike the other peer to peer networks, the peers are not connecting randomly and they are not equal in reputation score and trustworthiness. Each node have some selected public neighbors and a set of selected private neighbors (in regarding of the neighbor's public connection history and reputation score and trustworthiness). Even in case of compromising public neighbors and flooding or DoS attack, the node simply can disable public neighbors and only communicate with private neighbors.


        - It would be nice if the network recorded the status of its DAG in the Bitcoin blockchain and vice versa.
    The idea of using Bitcoin(or some other blockchains) as a proof of state for weaker chains and vice versa is good, and we can bind TWO chain together, but this also has a flaw. What if the cheater group starts to do same binding beforehand? I mean imagine the cheaters start to bind the corrupted chain parallel and put the Bitcoin hash in corrupted chain, eventually after some days they attack to network and claim their chain is the right one? How the nodes can determine which chain is true? They see both chains are recorded on Bitcoin chain and both are linked to Bitcoin blocks as well! The only solution is nodes shares. The nodes have to trust the majority of network on every block's creationTime. That is, for every time span, the nodes will control “for this time range, what blocks are signed by majority, and registered in Bitcoin chain?”. So the nodes always choose the branch which signed by majority and recorded in Bitcoin chain. Indeed I like the idea of recording weaker chain (specially PoS-type chains) in stronger chains in one way. This adds up an external-entity to chain which is useful. By this kind of binding in one way we secure our chain will not re-organize graph, if the majority (with today's shares) wants to change the history of graph.
    Your second idea of binding n different pairs is more interesting and as you mentioned before “it is almost impossible to persuade all chains to defect”. It could be implement as a plugin on top of our software. In such a way we can add some auto-snapshoot-recorder that records our chain's status in any given time(e.g every 12 hours) on another blockchain.


        - PoS is energy savvy, but still is biased in terms of making richer the rich and poorer the poor.
    Agree, we need a mechanism to create more equality, and in other side we have human nature which always looks for more (either food, relation, money, power, etc...). That's why I tried to remedy the system. We can not totally remove “money” and greed for hoarding power, but we can reining those.
    First of all in imagine the only way to own shares is working for system. Although one can pay cheap fiat money to people to working for him and developing Comen software. And at the end he claims the job which they did and grab all shares. But it is different than he pays directly to me (or my company) and buy shares. Considering that the software is free(libre), in the end, the result of all efforts (either by spending money or by working directly) goes back to the people and common good. So our product (Comen software) is “common good”.
    One of the important reasons that cause richer getting more rich is that they have the “means of production”. The Comen(imagine) software is the product, but also is the “means of productions” for entire people of the world. We start to develop the software itself and pay people for that. In next steps we need to translate the software and make it multi-lingual, later we need to create educational stuff and translate, and so on. By every single contribution the shares will be increase. It causes more democratic community. I am pretty sure the new shareholders are the best defenders of the system in every aspect. They will decide “what to produce, how to produce, for whom to produce, and why”. They will decide about real world and not only in cyberspace. There is a long essay about interaction between cyberspace and real world “Redefining exchange rates to an indication in democracy term” in Comen ecosystem. Please read it.
    Another flaws of our current economy is banking systems (either governmental banking or commercial banking). I addressed these two problem by introduce a “ ”.
    Another drawback of current economy is the “Cantillon effect” that I addressed by an steady and predictable money supply (like Bitcoin) and a well-designed wealth distribution (unlike Bitcoin) and an smart enhancement to add regulation on money supply (anti inflation and anti hyper deflation). In such a way the money in a just distribution first arrives to the hand of system helpers and not government's clan. I explained it partially in “An alternative monetary and wealth distribution model”.


        - The system needs to high-speed internet be accessible everywhere, otherwise not works.
    No, This solution need a normal (even slow) internet to work, although there are areas with no internet at all, system will not work for them. As we succeed in our mission we have to bring internet for poor, tyranny and dictatorship controlled area as well.


        - why you use email as transporter and just do not use TCP/IP or other decentralized messaging protocols for node communication?
    - Everyone can obtain one or one million email address with no cost, and governments can not stop individuals from using email. -Thanks free speech defenders we still can have anonymous email without compromising our identity -, whereas for all other alternate solutions user need to obtain an IP or some kind of identification or membership processes.
    - Email infrastructure is well-established and is accessible all over the glob. Even in dictatorship countries with high level of censorship and oppression, IP banning, low speed internet, and all other barriers for commercial, high speed services we are using freely in Europe and US.
    - The email is the only free (non proprietary) and open protocol/technology for communication.
    Comen infrastructure is not good for “online gaming” or funny kitty “video watching”, but it perfectly works for serious issues about privacy and against censorship. it stops global passive eavesdropping.
    Keeping it simple results No spying, No information exploitation, No hidden 3rd party IP connection.

        - Is it possible to have a technology that adversaries can not shout it down, block or seize it?
    Yes, It is possible and I already use it. As far as more people join to network and run their nodes or create their money, it will be even harder to decelerate it.


        - Can the adversaries shout-down internet?
    Of course they can shout down internet, but no government can shot-down internet forever. The cost of shouting down internet is too high to be affordable by any government. Even in this case there will be mesh-networks or offline solutions too. Although These two concept still are impractical but shouting down internet is impractical as well (at least in 90 percent of the countries).


        - Can the adversaries cut electricity?
    Of course they can cut, but I am not sure they seriously cut electricity and how long they can continue? Even in this case there will be generators as well. No one can continue without electricity. If you provide electricity for your refrigerator so you can use it for your laptop, and radio-wireless-mesh-network-equipment as well.


        - Can the adversaries acquire nodes or key member persons through physical action or cyber-attacks.
    Probably they can acquire some nodes through physical action or cyber-attacks, but it will be very high cost action. Since there will be thousands of backer-nodes and millions of mobile wallets in each country that are working perfectly decentralized. They may find the major shareholders or key persons in a society and try to bribe them or forced them to not participate or even eliminate them, but since establishing a new community costs zero, immediately new community will be emerged. And they can bring all coins and shares and entire society's history(of course the valid history and shares) from old community to new one.
    Indeed all community members already have entire information about society and software. These are essential features of blockchain technology where every body know everything. It is open public ledger. Actually in the Comen software, I already foreseen this kind of immigration, so I already equipped it with revocation of big shareholders or key members, and forking new community – all based on election surely. The community simply can re-organize entire structures, rules and members. At the end of the day brand new community has its members, ideologies and its new coined money with a change-rate by old-money. There is no single point of failure, unlike all revolutionary movements in history. That's all. It is an unstoppable infrastructure for ALL.


        - Will this technology will be vastly used by mass?
    Yes, although right now it looks like a fancy idea that is not accessible by even 0.0003 % of world population yet, but having great economical incentives and/or ideological incentives accelerates learning and using it and expands the networks exponentially.


        - Will these currencies will be vastly used by mass and they will have real usability and applicability?
    Yes, although in early days the community coins worth nothing, and you can do nothing except using it in community network to pay some network costs (e.g. posting to forums, or message sending, etc...). But time by time the community member can exchange coins between community members, in return of goods or services. Surly the community market range is too limited, thus they should invite more people to their community and convince them to accept their money in return of their goods or services. Maybe a community member pays 10 million coins to buy a real pizza! As it happened in history. Or the community member should change community's money with maybe another community's money to get different goods or services, and this exchanges prepare a wider range of goods and services. Right after the first exchanging a currency with something else the currency denominates the exchange-rate. Each single transaction remarks the exchange-rate. This is a recursive mechanism in which every transaction increases the value of money of one particular society or group and decrease the money of other group's or a fiat currency. This mechanism by itself accelerate competitive aspect of currencies which leads all groups to expand their money's usability and applicability, popularity. In order to increase their money's value.


        - Can these currencies being used in tax evasion?
    Taxation could be social contract, moral obligation, state-sanctioned, legal theft or not. It is up to you and your jurisdiction. Everything could be used legally or illegally as well as these currencies.


        - Can these currencies being used in criminal?
    There is a severe dichotomy and philosophical issue in everything which is related to freedom. If freedom is granted to all, both good and bad actors will use new freedom in good or bad way, and if it is limited, both good and bad will be limited. As we have all experienced before, it is not exactly a linear equation. More freedom causes to accomplish “much more good acts” by good actors, and “less more bad acts” by bad actors, and more limitation cause to “much more limitation on good actors” in favor of “less limitation on bad actors”.
    In overall more freedom cause to more good acts. Based on this fact, “Our mission is always to spread and extend freedom for all with no discremination”.
    On the other hand there is common sense, in which majority are good actors -otherwise we are already living in hell- and system must tolerate the malfunctions as well as normal function.
    The survival key for such system is fair allocation. That is, the product (in our case freedom in all of its aspects, whether financial freedom or speech freedom) must be distributed as fairly as possible. It is the core value of software and this feature must be anticipated and implemented by design, in advance. That is exactly what I designed in the software, indeed “no matters how good are the intentions or behavior of the creators, founders, or early community”, since a corrupted community absolutely will fast fail in early stage and they will never have a chance to grow. As a result only “good behavior community” will survive. Therefore the system can partially “protect itself from criminal abuse”. Ref:”Redefining exchange rates to an indication in democracy term”.


        - If a good and useful human community is to be built, it has to have a way to exclude bad actors. The software, built around hard anonymity as it is, has not.
    The software intended to have both “hard anonymity” - indeed pseudo anonymous - and “reputation-based trustworthiness” simultaneously, since both “privacy” and “trust” are important and necessary for any modern society. A small portion of actors and transactions of these kind of systems will involve in maleficent. In my opinion, the reason for this defect is in the way of distributing wealth. I tried to reduce these side effects by well distributing the wealth and increasing the overall benefits of the system, and I still need help to improve it more and more.


        - The bandwidth shortcomings (TPS) and need to interact with real world, have forced cryptocurrencies users to reinvent (via exchanges) all the financial infrastructure they still believe they have freed themselves from.
    By implementing a good design (DAG), the software has no limit in TPS. In addition because of the wealth distribution model of software, at the end of the day we do not need any exchanges at all. People will use native money directly in exchange of goods and services, and will do many of DeFi's activities without the need for exchanges, brokers, fiduciary, credit funds, brokers, and bankers.


        - What features the Comen software has?
    Up to now Comen has a complete protocol & web-interfaced desktop wallet to manage coins and send/receive coins to/from others. See the “What type of transactions system support?” for details.
    Comen has a really distributed Flexible Name Service (FleNS), by that people can register a user name (e.g. Nakamoto, 2Bfree, or 胡). These usernames are controlled by user itself and absolutely can not be seized or filtered.
    The registered names can be used as an address to send or receive encrypted and secured messages through iPGP (a user friendly implementation of PGP in which public keys are recorded on blockgraph).
    By having this usernames, people can run their own unstoppable website, weblog, videolog or video channel or podcast.
    The registered names can be used as an address to send or receive coins (through wallets communicating and generating account addresses without revealing which address belongs to which username).
    Furthermore Comen has a bunch of hard-coded smart contracts which are handy in DeFi. See “Does Comen support Smart Contracts?” for details.
    imagine has a basic polling system which is enough good to start, and in the next versions bring more option for polling and even ZKP voting.
    In order to being a real decentralized system, imagine has a real distributed wiki. Either for imagine's contents itself or any one who needs this feature (e.g. someone/wiki/page...).
    imagine has a decentralized Demos (forum). Either for imagine discussions or who need this feature (e.g. demos/someone/someTitle).
    And again to being a real distributed system, imagine also planned to have its own Distributed Version Control System. Since imagine accepts every type of document (transaction, contract, ballots, file, free-document ...) in it's graph for now the version-commits will be uploaded to graph as patch files and very soon we re-implementing a new fully fledged Version Control System; customized for distributed environments.
    imagine is more than just money. It is a secure time-stamping ledger, payment rail, smart contract platform, DeFi and more.
    imagine accepts any type of document in its DAG and as a node developers/companies can write some plugins that uses core functionality. In such case there will be unlimited creativity on top of a secure decentralized ecosystem.


        - Does Comen support Smart Contracts?
    Yes and No; Well, the smart money concept is an undeniable feature for any cryptocurrency. Although it is clear the most functionality of smart contracts is not too smart and it is not even a contract. Mostly they are about if x signed something and y not signed or z signed after a certain time then some money from j will go to k and so on…
    OK, almost all of these functionalities are implemented and embedded in Comen's multi-signature feature. So we have all, even need to name it smart contract!
    In particular, the imagine has more powerful feature which no other cryptocurrencies has! And it is the ability of having steady fixed (almost), guaranteed, long term income. Having this 7 years income give you the power of use it as a guaranty for loan & lending. You can buy some good and services and in return sign an “Generic Smart Contract” to schedule the paybacks (installment purchases). And after putting this document on chain, your cyclic income will be cut in order to doing repay backs. I emphasis on the word “Generic”, because the smart contract concept in imagine is a kind of hard coded program which is enough Generic to be used by more and more people. In fact they are not personalize-able at all. They are hard coded features which can be parameterized to fit to user's need. I think this is an efficient and good approach to solve the real-world-problem by some real solution. Having fancy features is good but not necessarily solves the real-world-problems.
    The main slogan in smart contract concept in Comen is “if there is a generic problem, write the parametric-able generic code and put it in core code”. It comes form the motto for this approach is “If something is enough good for one business, it will be good for all people, so let make it for all”. In such a way one time prepare one feature for ALL users and serve ALL users. So I hard coded some real handy tools in order to be used by ALL, and it doesn't mean we will not add more hard coded pieces in future.
    BTW if some one needs these fancy features it is easily feasible to collateral Comen coins in an Ethereum contract - or any other sophisticated platform - and do something over there and after a certain situation return back to imagine chain and redeems the coins.
    It is good but short comes in most of business which the model is pull. Say you want to be a subscriber of an online magazine or a video channel. How can you do it with bitcoin where you have to pay monthly to the company and company need this guaranty of your payment in order to service you a cheaper cost service? Having this regular income empowers you to sign a “Generic Contract” to pay x coins on each cycle to the company. And now the company will get payed automatically for each cycle.
    And as I mentioned before, imagine has an special transaction in which the signature until a certain time in future can not be considered as a valid signature. Telling that means if you sign the subscription contract, you can not cancel it before x months (or days or even hours) in features. Means you can subscribe a service for a couple of hours! Isn't amazing?


        - What type of transactions system support?
    Software supports native multi-signature addresses (m of n Bech32), time lock for income or/and outgoing coins.
    In addition, coin owners can even define who and how (in terms of who could be the receiver and how much is daily/weekly, ... spend limits) spend the coins for a certain address (say parents for child accounts or CEO for company's accountant). These all prepare an immense flexibility and usability to Comen's payment system.
    Since imagine is the only blockgraph with a predictable and steady income (for next coming 7 years) it is most powerful ecosystem for loaning, lending, pledge, crowd funding, installment purchase and “pull model” business (any kind of regular and automated payment for subscription a service). The wealth distribution mechanism of imagine provides a kind of real “credit“ for shareholders. This credit is real definition of credit and it is not like the fractional-reserve definition of credit. The credit in imagine means, you have steady unconditional income for next 7 years, despite the all changes in world (sanction, stagnation, rescission, pandemic). It works even better than all insurance you can buy. You do not have to cut a part of your income in advanced, for an uncertain future, and there is no condition to get your save back. It works perfectly in case of losing job or even passing away. It just needed to save private keys in a safe for the heirs.
    In addition, because of the way Comen designed, it is quite feasible having different type of transactions (Transparent, confidential, Mimblewimble, zk-snark, minimal format, IoT, RGB, BitcoinLike, …) in one Blockchain. So, in case of quantum computer threat, overtime system can immigrate to new transaction classes smoothly.


        - What level of privacy the transactions have in Comen system?
    The transaction privacy by now is as safe as Bitcoin - practically nothing – and transactions and coin transfers can be traced by going back in transaction history. This approach is not bad by itself. And I intentionally chose this for newly minted coins to avoid hidden coin creation. The threat of hidden inflation exist around the all privacy protected coins (such as Monero, Zcash and probably MimbleWimble) and to address this I separated the process of issuing new coins and the process of transferring coins.
    So newly created coins are always clear and transparently audit-able by entire network, and every one know exactly how many coin is created, but for having some levels of privacy we implement different type of transactions, and users can decide to spend their coins in whatever type of transactions they like.
    We have pure transparent transactions, and also different confidential transaction types, and also different techniques of coin join, coin swap and “backward spending”. all of these different type of transactions and techniques are take place on same blockchain and structure, and users can transfers their coins between and by different type of transactions.
    We can develop all kind of confidential transactions e.g. MimbleWimble, zk-snark, Monero circle signatures, etc in Comen as a customized transaction all in one place.
    So users always have transparent minted coins in first place, then they can use their coins say in a MW transaction as inputs, and after that the coins will be live in series of MW transactions for ever with no traceability. User can also move these coins from a MW transaction to a ZKP transaction or to a normal transaction and so on so forth.
    There fore it is always clear how many coin is created, and it is clear also how many coins are transferred to MB type or ZKP type or other confidential transaction types. So no one can create hidden coins.
    The road-map of transactions privacy enhancement is:
    - reducing passive eavesdropping by using encryption always and everywhere.
    - facilitate and do by default different techniques to wipe out the coins transfers footprint.


        - What are Comen solutions for privacy of transactions and coins in traceability sense?
    For now, the Comen transactions are pseudo anonymous (like vanilla Bitcoin), But as I mentioned before, the way software is designed allows to have various type of transactions on same blockchain. That is, users can accept a money from normal transaction and transfer it by MimbleWimble transaction and then transfer new coins to another coin swap transaction and at the end give that coins to a coin join/mixer and get new coins. These sequence can be repeated multiple time to strengthen more privacy, and make transactions untraceable.


        - Will it be really easy to create a new cryptocurrency?
    Yes, it is. A middle-skilled software developer can configure software and establish a new network (society, community or simple a new group) in couple of hours.


        - Having thousands of different currencies will raise arbitrage and all kinds of other schemes of speculation and manipulation.
    Arbitrage and all kinds of other schemes of speculation and manipulation are not welcomed, but we have to deal with. In beginning absolutely will be chaos and starts a curve which at the end, there will be some stable currencies supporting by matured community. The survived currencies have very low fluctuate (unlike Bitcoin and all other altcoins) and will be used for some real commodity exchange and not only speculating. Remember that we are talking about a community that governs their own money and proper rules in order to expand community members, rise up community reputation and strengthen their money.


        - Having thousands of different currencies will not crash modern system of production and exchange and organizations.
    No, won't. Indeed all of these currencies cannot grow enough to affect the mainstream economy. A bunch of real powerful monies which are supported by major ideologies will participate in most part of market. It is the instant democracy. People will support the community that covers more of their opinions so will support that money. I would wonder if we have more than ten really distinct-able different ideology or school of thought. Of course there will be tons of small communities currencies and proper exchange rates. BTW right now for making a just simple pen, we must manage materials and means of production and labor, allocation, and final shop, involving ten or more different countries and different currencies. we already solved these kind of problem. There is a software that calculates the final price of a pen, and for a software calculating the exchange-rate for 10 or 100 or 1000 different currencies isn't too different.


        - The market-capital of new monies will not be enough big to influence the real world's market.
    No, since I am not talking about tomorrow or even next years. As currencies prove themselves as a unit of accounting, medium of exchange and real store of value, their market caps will be grown. There will be two options only. One particular money can be raise up or collapse and fail. It is up to community acting and governance rules and wealth distribution/redistribution policies. Also knowing the fact that some of these currencies can be inflationary and some other deflationary. So people hold some type of currencies and spend another type of currencies. By each transaction the market cap of each currency changes. Finally there will be some good money having bigger portion of market cap, even bigger than fiat currencies.


        - will get around the power and centrality of the largest economic units - the financial and industrial capitals?
    In long run, yes. Although I am not planing to postpone every benefit of the software, and promise you some day in future (maybe after century) we will make the paradise on earth. let's come back to reality. Our pillars in this software are community and its money. To be clear, the people of community try to earn their community's money, so preferably accept their money. If we accept this assumption -That seems to be the case, Otherwise society would never have been formed-, society members use their money inside society as far as possible. Imagine if a general member of society can make 10 percent of its monthly turnover inside the society. It is a huge improvement in quality of life of society members. They can have their “mutual banks” and all benefits. They can have fund and credit inside the society, running by whatever Rules they want. They can take advantage of all “financial sovereignty“. As they internal turnovers increase they get more benefit. In hundred percent internally turnover they absolutely do not need any “outsider financial institutes”.
    And about “industrial capitals“, it is another story for another day. There are practical solutions for that as well, and I'll write a separate post for that.


        - Some of ideologies essentially do not believed in money at all. So the Comen approach makes no sense to them.
    No, this software will help them as well. Money simply can be considered as a contract like I Owe You. So we need some type of voucher/receipt/bill that represents this liability between two entities, in a kind of barter system or mutualism or reciprocity. All are Ok, just rename the currency and name it whatever you like. You do something and earn an special type of token denotes and proves you did that. Later you can spend this token in exchange of special goods or services. There are unlimited token types for unlimited goods or services. You will get payed back exactly by what you delivered before or something almost equal to what you did before. It is up to you both (seller & buyer).
    Some may even have problem with IOU concept and believed in the concept of money-less, gift-economy (one where people give and receive freely instead of expecting for compensation). That is ok too. In this society they can modify software's money concept to fit their own definition as well, to manage “access to necessity of life” for every person of society, and also other sort of automation.
    The software is designed to be highly flexible to support all needs of all groups of people.


        - What is the software license?
    Software is free(libre) and will be free(libre). The source code is open and the license for now is GPL. Since we are creating a software that in entire WORLDWIDE, EVERY BODY with NO CONDITION must be able to take benefit, maybe we need even more free licenses. It is an open issue.


        - Is software a fork? And if not, why not?
    No, the software idea is genuine and it is developed from scratch. Because the mechanisms (consensus algorithm and power/wealth distribution and protocol development) I needed are not same as any other cryptocurrencies. These mechanisms are the core value of our system.
    We do not pay coin because of burning electricity and solving a useless problem, we have not a business plan of selling useless coins to in cognizant people. A our coins are not speculation tool. Our plan and mechanisms are radically different from what you see in whole cryptocurrency ambient.


        - Why not just use some mix of Tor, VPNs, Orchid, LokiNet, Berty, IPFS, Matrix, etc?
    These technologies/platforms/protocols/softwares are all grate and good. But they are not decentralized at all. Please read the answer of “What do you mean by real decentralized blockchain?”
    Having these great technologies in parallel and as an “optional”, efficient, low cost feature in our ecosystem is pretty good idea, but relaying on external entities and having strong dependency to companies that can refuse to service our system is absolutely a bad decision.
    Comen must be standalone and continue to working in any situation.
    If we are doing some with serious impact on the world - which I strongly believe we are doing -, obviously we will have too many adversaries and they will do whatever they can to stop us.
    Our app must resist against all enemies.
    Since we are not limited in fund, we can have ALL these solutions simultaneously, if the community decide.
    The network community can decide to develop plan A or B or C or all A and B and C together. all is ok and community decides “what to produce, how to produce, for whom to produce, and why”.



        - What are your main observations to create this software?
    A: The blockchain concept is misunderstood too much! particularly exaggerated emphasize on speed & PoW & hash-rate. Most implementations didn't get the point and just copied the Bitcoin.
    B: The internet was developed & expanded very fast thus we couldn't evaluate truly the way and span of sharing our private information. And now, we are stuck in a situation that there is no privacy, EVERYTHING about our habits, interests, believes, financial history, ... are known by governments and the companies (Banks, Search engines, Social networks) that always stand for their benefit even if it is against  their customers' wishes (societies and people). They distort and play with our time, our expectations and, even more fundamentally, with our perception of reality.
    C: Internet in early days was defined based on peer to peer communication and not a centralized structure. This centralization and monopolization of the certification issuers and DNS, SSL,... is the one of the biggest flaw of current internet.
    D: Money by itself is a tool to store the value and using this value in trading, to change some goods or services with something else. It is only a tool. It must appreciate it's value over time, and guaranty the purchase power of it's owner even after decades.
    E: The good sides of societies are forced to be separated islands with poor power and weak voice! They must have power and a glue to tightening and amplify each other's efforts.
    Blockgraph for ALL!
    On top of current classic internet infrastructure, having blockgraph technology + Savor of cryptography we can rebuild the STANDARD internet which has not those drawbacks.
    The standard internet respects the users privacy and security, has no bias or discrimination on nations, race, sex or religious or political believes, is truly decentralized and can not be censored, and in overall the standard internet is “means of production” of society good and common good.
    Please read “Why Political Liberty Depends on Software Freedom More Than Ever” at https://softwarefreedom.org/events/2011/fosdem/moglen-fosdem-keynote.html too.

    )"
  },


  {
    "home",
    R"(
    <div>
        imagine follows the [https://mediawiki.org MediaWiki] formatting.
        surly it must be implementd completely! [[imagine/wiki/TODOs|here]] is a list of what we can do.


        [[142c3bc70608df26ace66e287966b4953a96b427facbd5dc8d88bd8ac83a3310.jpg|nauture]]


    </div>

    UPDATE i_wiki_contents SET wkc_content ='<div>
        imagine follows the [https://mediawiki.org MediaWiki] formatting.
        surly it must be implementd completely! [[imagine/wiki/TODOs|here]] is a list of what we can do.


        [[File:142c3bc70608df26ace66e287966b4953a96b427facbd5dc8d88bd8ac83a3310.jpg|nauture]]

        [[File:60f2d0d7c59f5828947ce2b905f6ae90fc7e71795a451552d5f6180637338bdc.jpg|future]]


    </div>' WHERE wkc_wkp_hash='20128937d67143a76812dfbfe2d9cbb137dd04895d488fa9e8572d252ef16c3d';

    )"
  },


  {
    "CIPs",
    R"(
    <div>
        Comen's Improvment Proposals, the idea comes from PIBs, same concept same policy.
    </div>
    )"
  },


  {
    "imagine",
    R"(
    <div>
        John Lennon
        Imagine there's no heaven
        It's easy if you try
        No hell below us
        Above us, only sky
        Imagine all the people living for today
        Ah


        Imagine there's no countries
        It isn't hard to do
        Nothing to kill or die for
        And no religion too
        Imagine all the people living life in peace


        You may say I'm a dreamer
        But I'm not the only one
        I hope some day you'll join us
        And the world will be as one

        Imagine no possessions
        I wonder if you can
        No need for greed or hunger
        A brotherhood of man
        Imagine all the people sharing all the world

        You may say I'm a dreamer
        But I'm not the only one
        I hope some day you'll join us
        And the world will live as one
    </div>
    )"
  },


  {
    "Comen network",
    R"(
    imagine's network in early days consists of one type element which are nodes. the laptops or personal computers whith a normal CPU power and HD capacity
    in mid time we will develope the mobile wallets and for long term we will have backing pools and also massive data backers.
    for know there are only light nodes which have all blockgraph's history.
    [[File:imagine-network.png|imagine network]]
    mobile wallets will be able to chose to connect whatever nodes she want. the mobile wallet can connect to more than one node in same time.
    the wallet find nodes by their [[imagine/wiki/inames|iName]]s
    eache node has different power, connectivity,... thus different transaction fee.
    mobile wallet can send same transaction to multiple nodes (in order to spread as fast as possible), obviousley paying multiple transactions fee too.
    )"
  },


  {
    "mail FAQ",
    R"(
    if you are here means you couldn't configure your email setting well or probably your email service provider
    (for example gmail) does not support your moachine connection.
    it could be also because of not fully implementation of imagine mailing sub system.
    hope fully we upgrade the code in order to have more reliability in system functionality.
    your machine may have problem in sending email or in receiving or both!
    you can do it manually. and since in early day of launching project there are few emails it will not a frustrating job!
    1. if your machine can not send email you can find emails to be sended in path imagine-server/temp-mailbox/outbox/
    the file name indicates the receiver. you just need to open the file, copy the content and past it in email and send to proper receiver.
    2. if your machine can not receive email, you need to copy the email body in a text file in imagine-server/temp-mailbox/inbox/. and the name of file isn't important.
    )"
  },


  {
    "mobile wallet",
    R"(
    <div>
        <div>mobile-wallet</div>
        <div>
            no inputs can not be spendable without manually confirmation by wallet owner, to defend against dust-attack or any type of privaicy & security issues.
        </div>

        <div>
            privacy & security is a MUST and DEFAULT feature in imagine Wallets
        </div>

        <div>
            the imagine mobile-wallet must have these features:
            - NATIVE coin-join(wasabi/samurai like) or even Knapsack(unequal inputs/outputs)
            - full support of imagine Demos features (e.g. rooms, pollings...)
            - Onion supported by default
            - because of the native multiSig and input-time-lock support in imagine addresses, user can easily add a time-locked address in every addresses which controls by wallet.
            and saves this time-locked key in a not so secure place(e.g. mailbox). since this special key can sign the funds but the network didn't accept thiese trnasactions until a certain time passed.
            so compromise this address has not dangerous.
            by the way in case of user losses his key, he always can use this time-locked-key to restoration funds(of course after expiring time-lock)
        </div>

        <div>
            bitcoin hd wallet improvment:

            - schnore signature to cover new features

            - instead sending some outputs to one input address, send each output to a seperate input:
            imagine wants to send 9 to another, currently it could be like

            inputs    output  change
                2-------|--4      1
                3-------|  3

            it will convert to

            inputs    output  change
                2---------2       1
                3---------3

            more details on BIP32 Reclaiming Financial Privacy With HD Wallets
            http://bitcoinism.blogspot.com/2013/07/reclaiming-financial-privacy-with-hd.html

            - coinjoin (wasabi like)
                https://wasabiwallet.io/
                https://bitcointalk.org/index.php?topic=279249.0
                https://bitcoin.stackexchange.com/questions/1823/what-is-the-maximum-size-of-a-transaction/35882#35882
                https://bitcoin.stackexchange.com/questions/57073/what-is-the-maximum-anonimity-set-of-a-coinjoin-transaction/57091
                https://github.com/nopara73/ZeroLink


        </div>

    </div>
    )"
  },


  {
    "page formatting",
    R"(
    imagine's wiki follows the exact rules of [[https://www.mediawiki.org/wiki/Help:Formatting|media wiki]].
    currently it has  implemente only [ [ ] ] for links and files.
    and very soon we (contributors) will complete it.
    if you know Javascript you can [[contributors|join to contribute]].

    )"
  },


  {
    "pledging and unpledging",
    R"(
    since in imagine every contributor has a (almost)regulare income for next 7 years, it could be a good fund to pledge it
    in order to get loan
    or buying some services and paying gradualy and so on.
    currently imagine supports peldeP which is use ful when you ask a loan to pay proposal's cost.

    the orther pledges will come in soon are:

    <div>
        <b>PledgeP</b>
        <br>Most common type of pledge in early launching imagine days, in which the new contributors do not have any PAI to
        apply a proposal. so they have to loan some PAIs, they can ask a loan and in return pledge the account in order to
        payback the loan. for detail about proposal & contribute check also [[wiki/imagine/faq, faq]]
        <br>disregarding proposal approved or not, the loan repayments will be cut from pledged account.

        <br>there are 3 ways to un-pledge an account
        <br>1. ByPledgee: The pledgee redeems account (pledgee Closes Pledge). it is most common way supposed to close
        pledge and redeem account.
        <br> this is most efficient way to avoid entire network engaging unnecessary calculation.
        <br>2. ByArbiter: in redeem contract can be add a BECH32 address as a person(or a group of persons) as arbiter. the
        contract can be closed by arbiter signatures too.
        <br> the arbiters inetercept in 2 cases. in case of denial of closing contract by pledgee or for the sake of useless
        process.
        <br> this method is also cost-less(in sence of network process load) and is a GOOD SAMPLE of trusting and not
        over-processing by entirenetwork
        <br> in other word, by giving rights to someone to be arbiter, and givin small wage, and involving them in a
        contract,
        <br> the arbiters MUST run a full node (and maybe a full-stack-virtual-machine to evaluate a contract) and the rest
        of networks doing nothing about this particulare contract
        <br> in this implementation even an infinit loop problem doesnot collapse entire network, and more over the
        contracts can be run as an OPTIONAL-PLUGIN on SOME machines
        <br> and for the rest of network, what is important is the final signature of arbiters, adn they only validate
        signatures in order to confirm the PAIs transformation/contract-closing...
        <br>3. ByPledger: in case of pledgee refuse to redeem account, pledger requests network to validate redeem clauses by
        calculate repayments and...
        <br> and finally vote to redeem (or not redeem) the account. this proces has cost, and must be paid by pledger.
        <br> althout this has cost for pledger, also has negative effect on pledgee(and potentially arbiters) reputation(if network redeem the
        account).
        <br>4. ByNetwork, every time the coinbase dividend toke place, every machines calculate automatically. this method
        is most costly till implementing an efficient way
        <br>either pledgee or arbiter can close contract before head and remit repayments
        <br>Note: there will be a reputation sub-system which calculate the risk of different pledgee and arbiters(which can be a group of persons)
        and at the end the pledger can manage the risk & pros & cons of each contract and also ceil & floor of the contract value.
        PledgeP Relations with other documents
        [[File:pledgep-relations.png|DNAProposal, Loan request & PledgeP Relations]]
    </div>

    <div>
        <b>PledgeSA</b>
        <br>self authorized pledge
        <br>in this class the pledger signs a contract to cut regularely(every 12 hours) from mentioned address(usually the
        shareholder
        address by which received incomes from share dvidend).
        <br>the diference is in this class all 3 parties(pledger, pledgee, arbiter) can close contract whenever they want.
        <br>this service is usefull for all type of push-money, subscription services e.g. monthly payments for T.V.
        streaming or even
        better DAILY payment for using services
        <br>by activating field "applyableAfter" the contract can be realy closed after certain minute of concluding it.
        <br>e.g. customer must use at least 3 month of service(in order to get discount), so pledger signs it with an extra
        tag which present the minutes of closing offset, and in time of closing contract the real close happened after
        certain minute which are mentioned befor.
        or can be calceled immediately if passed this certain time from signing time.
        <br>TODO: implement it since it costs couple of hours to implementing

    </div>

    <div>
        <b>PledgeG</b>
        <br>gneric pledge in which pledger can pledge an address(either has sufficent income from DNA or not) and get a loan
        <br>and by time payback the loan to same account, and pledgee takes from pledged account
        <br>of course there is no garanty to payback PAIs, excep reputation of pledger or her bussiness account which is
        pledged
        <br>TODO: implement it ASAP
    </div>

    <div>
        <b>PledgeL</b>
        <br>lending PAIs by pledging an account with SUFFICENT income
    </div>

    <div>

    </div>
    )"
  },


  {
    "pollings",
    R"(
    <div>
        there are too many different Voting systems in the world, non of them is designed for distributed systems!
        all are designed to perform in a certain and limited window-time(max 12 hours) and in certain places.
        the basci polling process in imagine is designed to run a distributed ellection and in long period.
        in order to reduce latenancy effect on the final result of polling basic profile works in 2 step.
        first positive, negative and abstain votes. and second phasde accepts only abstain and negative votes.
        I complete this later...
        [[File:basic-polling-phases.png| basic polling phases]]
    </div>
    )"
  },


  {
    "prepare a proposal",
    R"(
    the proposals are documents in which a contributor states what she/he can do for improvement of the imagine.
    or what she already did.
    and simply writes how many hours toke time that job and how important/usefull was that job?
    and at the end sets a time-window for polling about proposal. after finish the polling time,
    if the majority of imagine comunity approve the proposal,
    the proposer become a share holder or if the address already has some shares, the shares will be increased.
    the share value is equal to hours of contribute * usefulness of contribute.



    <div>

        There is a long list of [[imagine/wiki/todos| TO DO]]s. take a look and find if there is something you can do?
        hopefully you will find somethig interset to do!
        after that you need to prepare a proposal and send it to network, in order to run a polling and all shareholders of
        imagine(including you) decide about your proposal.
        the proposal form is in address [[contributes| Contributes]], and you first must complete form 1 (gray one)
        <br>
        [[File:create-a-draft-proposal.png|1. Create a Draft Proposal]].
        <br>
        and click on button "Prepare proposal". by clicking, the proposal will be add to your draft proposals.
        there is list of all your Draft proposals 2.(Blue one)
        <br>
        [[File:my-draft-proposals.png|2.my draft proposals]].
        <br>

        to send a proposal you need to spend some PAIs(in order to reduce spaming, this is a little high value).
        obviousely you have not this amount before head, so can ask a loan.
        if you have some PAIs you can "sign and pay cost..." and pay by yourself, otherwise click on button
        "Prepare Loan Request" and the form 3 will be displayed(green one)
        <br>
        [[File:request-loan-to-apply-for-polling.png|3. Request loan & apply for Polling]].
        <br>


        here you can change repayments count(by changing repayment value)
        and put inside pledgee the address of who you wana ask loan. by default it is filed by Hu's Address.
        by changing the numbers the repayments will be changed and you can see all in long list (green/gray one)
        <br>
        [[File:laon-repayments-details.png|4. Loan Repayment Details]].
        <br>

        <br>
        in form 3, at bottom you can click on "Sign Request" and now you have the signed loan request on your machine.
        the draft list presents the signe loan request(s)
        <b>
            [[File:my-draft-proposals-signed.png|2.my draft proposals]].
        </b>

        <br> now you need to send it whom you want to ask loan, there are 2 solution.
        first is sending directly by email address via form "2.my draft proposals" the default is Hu's email address but you
        can
        change it to who you want getting loan. this is absolutely private email.
        in case of losting email possibility, there is a [[imagine/demos/Pre Proposals and Discussions|Proposals and
        Discussions Room(Agora)]] you
        can publicly send your proposal over there
        and everybody can read your proposal but only pledgee that you signed can complete the request and send it to
        network.
        to sending this proposal you still need some small amount of PAIS.
        and if you do not have it you can send it by POW. in imagine POW is just a way to send messages and you can not earn
        PAIs by POW.

        <br>
        sending your proposal to pledgee means, she will sign your proposal and pay the proposal costs and push it to
        network.
        after placing proposal onchaine, the polling period starts immediately, and lasts for what you requested in form 1 (minimum 24 hours + 12 hour for negative votes).
        deatils about [[imagine/wiki/pollings|how basic polling works in imagine?]]
        and at the end if your proposal approved by majority of imagine comunity you will be a shareholder(or your share
        will be increased).


    </div>
    )"
  },


  {
    "reserved coins",
    R"(
    <div>
        The reserved coins are the coins which are issued at the time of creation a coinbase, but they are not released thus
        they are not spendable.
        for each coinbase block there are another 4 reserve blocks whith the same amount of "minted" coins. mintes is quoted
        because in coinbase class there are also treasury coins which are collected in treasury becase of diverse incomes.
        and the treasury income is not cloning to reserve blocks.
        so there are 4 reserved coins block corresponding to each coinbase block (which creates every 12 hours).
        These “anti-deflation reserve” blocks can/will be released in case of share holders of that particular
        block, voted to release it and reserve-time-lock is exceeded. For all 4 blocks there are different time-lock.
        First block, is releasable after 1 year of minting related coinbase block, second after 3 years, third after 6 years and
        forth after 11 years.
        to release a reserved block must run a polling and after that the votes indicate release should happend or not!
        this note is incomplete and will be more complete soon. maybe you can help to finish it.


        Request for Release Reserved Coins (reqRelRes) Relations
        [[File:reqrelres-relations.png|The ReqRelRes, Pollin & payer transactions]]
    </div>
    )"
  },


  {
    "support languages",
    R"(
    <div>
        Currently imagine supports some alnguages. a list of supported languages placed in file
        "imagine-server/web-interface/settings/languages.js". if you want help you can add more languages as mentioned in
        [[https://iso639-3.sil.org/code_tables/639/data/all|iso639-3]]. and send it back with a [[contributes|Proposal]].
        in such a way you will be a contributor and the languages list will be more complete.

    </div>
    )"
  },


  {
    "todos",
    R"(
    trx

    <div>
        <div>
            There is a big list of TODOs, some of them are specialized for cryptographers and some of them can be done by a
            simple-computer user (e.g. translate). and there are a wide range in between you can fit in.
            please check the list, surly you will find(or can suggest) something to do to bring happiness to the world.
        </div>

        <div>
            must complete BEFORE RELEASE
            - Cloned Trx and P4P trx, duoble-spending TESTs
            - TO TEST: GQL, invoke for entire DAG (for newly joined node)
            - TOTEST: proposer pays for proposal
            - TOTEST: release resereved coins
            - TOTEST: bind PGP to iName
            - maybe, bind by default the default key to iname(when it registered)
            - TOTEST: send msg to iName
            - TOTEST: iName(registering, collision handling, sending content, even multimeida, and presenting in personal
            page)
            - TOTEST: iname collision solving
            - TOTEST: Messenger (specialy sending encrypt message to iName which toke place on blockchain, improve it to not
            recording msg in blockchain space but relayed by entire network until reaching the propoer machine)
            - TOTEST: Demos & Agora (forum)

            -* TOTEST: booting a machine and sync
            -* TOTEST: too many tests for double-spend. clone, p4p tests

            ------------------------
            remove am pm from cycle
            utf8Decode
            - by default a PGP key must be bind to an iName
            - improve POW block to be able send msgs from Bech to Bech address
            - possiblity to send to iName by and bech32 address, betterpossibility to send msg between bech32 addresses
            getDocumentMerkleProof({blockHash:, docHash:});
            verifyDocumentMerkleProof({blockHash:, docHash:});
            - set a limit of POW blocks per hour.

            - notify version updating
            - arbitrary voting, can be created easily, low cost, for everything. e.g. imagine Logo, development planing,
            political polling... (SASH_custom_validate_polling)
            - implemet pay to iName which takes place on blockchain.
            - make a Black Holes to shoot some inappropriate iNames(e.g. racism, violence...) to be inaccessible forever
        </div>

        <div>
            bind ip to iName
        </div>

        <div>
            bind receive cost to iName. the sender must pay to you in order to be delivered her mail to you.
        </div>

        <div>
            improve content rendering/formatting based on media wiki formattings
            full support of all media wiki conventions
        </div>

        <div>
            Implementing Real Distributed Wiki based on mediawiki implementation
            todo: adding sign feature to wiki pages. in such a way viewers can find the most reliable pages(in case of
            adversary over-writes the qualified version, since everybody permited to edit every public pages).
        </div>

        <div>
            improvinge Demo & Agoras, in order to have nice graphic, folding for long texts,
            and most important re-order and restructure opinions in different ways e.g. most replyed(either positive or
            negative),
            extracting discussion main thread by folloing both cronological and reply.
            alongside ther discussions a nice pie chart statistics and sumup the discussion.
            all possible by a kind of plugin on top of existed structure and data
        </div>

        <div>
            adding optional new salt(or alter existed salt)
            in order to account owner add her name to address, and in such a way
            sender(by knowing the recepient name/nick nale/alias name... and hashing it to check)
            can be sure the address isn't type or transfer incorectly
        </div>

        <div>
            forum demos agora enhancing
            https://blog.discourse.org/2013/03/the-universal-rules-of-civilized-discourse/
        </div>
        <div>
            implement a bind-bitcoin-address to FleNS contract.
            it can accept also an HD-key to generate new pub-keys beside the wallet owner(which is iName owner too)
        </div>

        <div>
            adding discussion tab to wiki, in order to have an Agora(forum | chatroom | comment place... which are all same)
            for each page|topic in a certain language
        </div>

        <div>
            add pinedPost posibility to Agoras, to change first posts to better ones
        </div>

        <div>
            improve pay to iName in order to be interactive and
            payer ask from payee address to pay
            payee generates some fresh address(by HD Wallet) and sends to payer. The payer transfers PAIs to payee
            all process and comunications done in iPGP and encrypted and not visible for others and in background
        </div>

        <div>
            improve pay-to-iName to accept bitcoin payment(and maybe some other real usefull alt-coins).
            imagine node accepts a bitcoin payment to an iName, and returns the proper bitcoin address of an iName(even
            generates a bunch of address based on HD-wallets)
            and return to client wallet a new transaction in which the receiver addresses are replaced by real bitcoin
            address of recipient.
        </div>

        <div>
            TOO many security issue to not allowing upload harmful files, during send block to network, validating received
            blocks, forwarding them and also presenting the file content in browsers
        </div>

        <div>
            improve iName in which the owner can set a customized profile
            e.g. picture, webite, social network...
            ALL profile pieces MUST be optional and manageable by owner, in order to decide to share which parts with whom
            and be able to hide it again.
        </div>

        <div>
            educational plan to teach more tschnicals
        </div>

        <div>
            Optimizing plugin-based architecture for both server(node) and client(Angular & maybe React)
            more accurate hooks in all Active/Passive Synchronous/Asynchronous different modes
        </div>

        <div>
            adding alternate media-layer. e.g. SOCKET & IP
        </div>

        <div>
            improve iName regisre & Bind, in order to by registering an iName, automatically create a default iPGP and bind
            it to iName be label Default.
        </div>

        <div>
            improving server security and efficiency in order to each node can be connected by mobile wallet freely
            and earn a litle PAI by provide service to mobile wallets.
            each mobile wallet has to have a couple of different fullnode-sites url and optionally casn send transactions to
            some of them.
        </div>

        <div>
            Generalize imagine-DNA in order to make it available for every one who wants start an activity in distributed
            mode and give shares and dividend of that to contributors just like imagine-DNA.
        </div>

        <div>
            although multi-sig is native feature of imagine,
            BTW in some cases payee wants to be sure the payment MUST sign with a certain key(kSet).
            e.g if the payee accpet a pledged account as a payer of a service regulare subscribtion, and want to to
            subscriber not be able to cancel service in advance
            and he must use service for a shile(for example at least 6 month abounment).
            in this case payee accepts the only signature-pledged in which has an 6 month outputTimeLock.
            to solve this issue in this special type of SAPledge the transactioon includes also a certain uSet
        </div>

        <div>
            implementing customizable theme style css for entire site
        </div>

        <div>
            Too many security checks
            OWASP
        </div>

        <div>
            complete language lists, file: imagine-server/web-interface/settings/languages.js
        </div>

        <div>
            improve some method to save & backup private/public keys easily, even backup it onchain
            or encrypt all bunch of keys in a file and put on chain and divide the password between some trusted iNames(such
            as firends, partner, relatives, lawyer...)
            in case of lost key, user can re-genererate pwd from friends and decrypt the onchain file.
        </div>

        <div>
            Tons of tests (uniit, integrity & e2e)
        </div>

        <div>
            clients(full node or wallets) can comunicate eachother through iNames, specially they interactively can share
            paublic-keys in order to create multi-sig addresses.
            for example user1 and user2 send one public key each to user3, and she by adding her public-key, creates a 2 of
            3 signature address.
            and share it with 2 other. now all 3 have a same bech32 address in their wallet which the funds can be spend by
            2 signature of 3.
            obviously to signing the funds, this 3 users needs to pre-sign trx comunication and so forth. and finally
            sending to network.
            all these can be done simply and in background by iName infostructure. without users panic of all complexity.
        </div>

        <div>
            unittest, and efficiency for inputTimelock controls in BasicTx
        </div>

        <div>
            implementing outputTimelock control in BasicTx
        </div>

        <div>
            implementing pleger redeem account.(if pledgee refused doing so)
        </div>

        <div>
            improve neighbor-reputation plugin
        </div>

        <div>
            plugin for polling system, in order to watch a polling result instantly & online, and also promot to vote(accept
            or reject)
            <br>also tons of tests for polling
        </div>

        <div>
            implementing native block-explorer(DAG monitor) plugin
        </div>

        <div>
            implementing market prediction, Options, future contracts plugin
            since imagine is the only blockchain in which the backers have steady and predictable incomes for 7
            years(including reserved coins until 7+1+2+3+5=18 years),
            this income could be a very good security to many different type of reputation systems and Oracles.
            all aspect of smart contracts can be implemented easyly and bind to real-world with high degree of guaranty of
            good-treatment of actors in sistem.
            BTW, for more dynamic conditions it is possible to block imagine fund in an ethereum contract and use solidity
            to treat.
            in mid-term a bridge between imagine and ethreum to lock imagine incomes on imagine DAG and run smart-contracts
            on ethereum.
            in long-term, if it really needed impement our smart-contract platform(language, VM etc)
        </div>

        <div>
            implementing distributed issue tracking and project manager (Jira like) plugin for imagine eccosystem
        </div>

        <div>
            Later, improving IOT transactions to be more efficient than noraml transactions ( in sence of needed space &
            process)
        </div>

        <div>
            Bitcoin-like account Address & Transaction, in order to implement atomic swap between imagine & Bitcoin
        </div>

        <div>
            selecting an oen-source [[mobile-wallet|mobile-wallet]] project and customize it for imagine.
        </div>

        <div>
            since everything in imagine is designed to be modulare, so we can easily aggregate different behaviors in one
            eccosystem.
            in such a way the coins, can be used in BasicTx or can being transformed to different mode for example
            mimblewimble transactions or ZKP.
            in this case the same amount of PAIs are trading in mimble-mode and have the mimble charachteristic and
            everytime the owner of coins can transform it
            to Basic form or ZKP or ...
        </div>

        <div>
            boosting nodemailer to support more email client/servers
        </div>

        <div>
            adding new transfer medium as an optional plugin. in this case the plugin can be activated in order to broadcast
            the blocks(and GQL packets) in a efficient and alternate transformers
            such as direct IP/socket connections, or onion routing, maybe Dandelio...
            the plugin do not need to touch core code. it is enough to listen to hooks (SPSH_push_packet) to broadcast
            and in case of receiving new packet, calling hooks (SPSH_packet_ingress) will bring the packet to imagine core
            to parse & process it
        </div>

        <div>
            improve graphQL
        </div>

        <div>
            study for implementing other polling systems. either the way of voting or counting & presenting the result
        </div>

        <div>
            improve this version-patching system to be a real distributed versioning system with some handsfull commands
            (like commit, push...)
        </div>

        <div>
            fisibility study on RGB(colored coin) new generation implementing in imagine
            specialy offline transactions
        </div>

        <div>
            to support:
            The use of TLS in Censorship Circumvention
            uTLS as a wrapper for all censorship circumvention tools
        </div>

        <div>
            an app to automatically convert some old but gold mailing-lists (e.g. cryptography, cypherpunks, activists...)
            to new our Demos/Agoras in a modern form and presentation, in order to use the veteran's experiences and discussions.
            and also translate ALL to ALL languages.
        </div>

        <div>
            study on real-world needed contracts to implementing in imagine-core.
        </div>

        <div>
            develope PledgeG & PledgeL contracts as like as implemented PledgeP
            and later on developing PledgePZKP in respect of privacy
        </div>

        <div>
            the pledgeL is a loan system, user can pledge her income(which is essentialy an imagine Bech32 Address that has
            shares) in fron of get lon by PAIs
        </div>

        <div>
            improve pledgeL to transfer the PAIs immidiately to vendor's account, in this case pledgeL acts like a "pay over
            time" system in which user can buy a service/good/product
            and pay the cost over time with/without interest/late fees, penalties, and compounding interest,...
        </div>

        <div>
            I order to improve privacy in eccosystem, it needed to [[iPGP|improve PGP comunication between nodes]]. to
            support existed legacy GPG and web of trust
        </div>

        <div>
            adjust code to support multi language i18n
            then translate menu, label, content... ALL to ALL languages
        </div>

        <div>
            a class(dClass=VePoll Verify by Polling Result) of Basic transactions in which instead verify the signature of
            input owners, machines use the result of a generall polling
            and based on the result decide to transfer PAIs or reject the entire transaction.
            1. owner of funds will be used in trx signs the fund through a contract in which binds a polling.
            2. 3 documents(trx, polling & VePollContract) put on chain, and because of that the fund will be lock until end
            of polling period.
            3. after finish polling depends on the results the transaction will be valid or invalid

            P.S. same principals cn be implemented to do a Pledge/Unpledge(obviously for all class of pledges e.g. PledgeP,
            PledgeSA...)
            based on a polling.
            the polling participane can be ALL or be mentioned in VePollContract
        </div>

        <div>
            Implementing data-selling plugin(code, protocol...), in order to backers can be payed because of backing up mass
            data.
            using Universal Business Language UBL for nodes comunications
        </div>
        <div>
            Enabling Clone and P4P transactions
        </div>

        <div>
            refactor Routing in angular in order to having http://localhost:4200/imagine/... for all parts related to
            imagine
            and http://localhost:4200/iName1/... for all parts related to an hypothetical iName1
        </div>
    </div>
    )"
  },


  {
    "version control",
    R"(
    <div>
        In order to have a real distriuted system imagine has to have it's own distributed version control system.
        it not implementes yet, but the imagine updates are released by imagine's DAG and in free-document format.
        the update, essentially are patches you can download and add to your code regardles of what version control system yu are using (e.g. Fossil, Git, Subversion, ...)
        BTW we will imlement a new version control system which is customized for distributed environment soon.
        look at [[/imagine/demos/Distributed Version Control Plugin implementation|Distributed Version Control Plugin implementation]] for discussiona and planing
    </div>
    )"
  },

  {
    "Redefining “exchange rates” to “excellence indice” in “democracy term”",
    R"(

    Redefining “exchange rates” to “excellence indice” in “democracy term”


    Let me draw a future in which you vote hundred times in a single day, and your vote impacts immediately. You are not limited to vote only for a person or for a vague plan of a party, instead you can vote for every single changes and upvote it or defiance it immediately. And most importantly this voting doesn’t take your time or energy.

    Hello dear reader,
    It is Hu, a freedom enthusiast software developer, who believes in Privacy, digital rights and human equality as well. I implemented an innovative mixed technology to cover some drawbacks in internet privacy and center-oriented services as well as wipe out some flaws in economy, hoping more people feel happiness. I do not want to fit project in a particular “ism“ or a certain ideology, doctrine, regime or school of thought, simply because the project is a comprehensive infrastructure of human knowledge and experiences. It belongs to whole humankind, since my intention of that strive is human prosperity and I hope your intention is human prosperity as well.

    The project synthesized different concepts such as blockchain, free(libre) software movement, and some economic doctrine, so formed a software in a genuine ecosystem.
    In simple words, I created a software that lets every group of people establish an online community. It is “Community Maker Engine“ software. The community is practically decentralized and censorship resilience. So no one can stop the community, or cut its voice. These  features are core values of the software. Furthermore the software helps communities to run their customized rules and having their money and monetary system. Software also supports uncensorable decentralized domain name, messenger, forums, video channels and wikis. The software called “Comen”.

    You may ask “What is the use of all these?”.
    Let me draw a future, in which you vote hundred times in a single day, and your vote impacts immediately. You are not limited to vote only for a person or for a vague plan of a party, instead you can vote for every single decision and upvote it or defiance it immediately. And most importantly this voting doesn’t take your time or energy.
    Nowadays we vote for some political party or economic change promises every 4 or 5 years -if we believed in election yet-. After election, generally we can not do much in order to control what are doing the congress men or the president or the parliament -despite the fact that it is not an affair for mass, and most people simply do not interested in-.
    Even if -for any reason- an individual (or a group of people) monitors politicians or government decisions and find something wrong, practically she/he/they can not do something effective, unless waiting for next election, to not vote for that party or person and select another one. Something like selecting one kilo potato from one kilo potato! How can people express, “we prefer apples to potatoes”?
    Lets go back to our software.
    Assume different group of people, have installed the “Comen software” and established their online communities. Each community will governed by different set of rules, some will be pure democratic and some other communities may have totalitarian rules. Each community has its customized economic systems for monetary, interest rate, inflation rate, wealth distribution and taxation. Eventually we will have different communities. Some communities follow market economy, whereas other communities are pure socialist and have some kind of coupons for managing “necessity of life”, and some another communities seeking gift-economy model and they do not have any form of money at all. By our software all of these different types of communities can be created and established in couple of hours. Even by a newbie software developer.
    Inevitably, communities need to trade with each other, since It is almost impossible for one society not to need the goods or services of another. This need for trade between communities (More precisely, the trade between a person who has the money of community 1 and a person who has the money of community 2) creates the concept of exchange rate between currencies of communities. Two individual can directly change their different coins with agreement on a change rate between themselves.
    Take a look at schema:



    “For any reason” there will be communities that their money is more valuable than other communities. Later we will go into details “Why the money of a community is valuable than other ones”.
    Now suppose the exchange-rates is fairly adjusted and not manipulated by speculators or conspiracy activities. We will discuss this in detail later.

    Recap:
    - They are coexisting different communities with different currencies, different governing rules and economic system, different order class and different structures.
    - There are exchange rates between currencies  of communities.

    The exchange rates between different communities generally are flat, and are determined by supply and demand on the open market between communities. There are several technical and fundamental factors that determine what people perceive is a fair exchange rate and alter their supply and demand accordingly. Despite the classical factors such as interest rate, inflation rate, commodities, etc… the value of a community-based currency comes form  “the community’s tendency of supporting community’s money”.  The community regularly attempts to keep its currency price favorable for inter-community trades. We should accept this assumption, Otherwise community would never have been formed. These mechanisms together provide a concept of “exchange rate as  excellence measurement”.
    We can see this measurement -in a very loosely form- already exist in our real world exchange rates. The big difference between currencies of communities and fiat currencies laid in how the currencies are managed. In one side we have currencies, managed by governments and most often in non democratic ways, and the other side the currencies that are powered by people.
    The way that current exchange rates work, “exchange rate does not reflect fundamental economic conditions” or at least governments try to uncouple those (through FX intervention), but in our new “community-based arrangement” of world population, the exchange rate reflects both the economic conditions and the political governance of each community.
    Reasonably people will demand currency of some communities that they believe in the community’s principals, and refuse or devaluate the currency of other communities. People can be somewhat socialist and a bit capitalist, as well as naturalist and so on. It is our human nature and nothing is one hundred percent true. So we should pick the best part of each. People are free to create a portfolio of different currencies and also be a member of some different communities as well. Whenever they feel that a community is no longer following their wishes, they simply "leave the community at no cost", unlike the current communities, ideologies, political regimes or countries and territories. They get rid of the community’s money and that’s all. No need to argue, quarrel or fight. No need to war.

    “Every time you spend money, you're casting a vote for the kind of world
    you want.”
          ― Anna Lappé
    We buy and sell million times per day. By each transaction we make an impact on the exchange rate (supply and demand rule). These are our votes. We can vote every day million times for what we stand for, whether it’s a new economic policy or a welfare program in a community or a cultural movement in another community. This is a kind of instant democracy. We express our opinion and impact on exchange rates immediately. In such a way the corrupted community will lose value of its money and its population very quickly -let alone the fact that this kind of corrupted community can not be formed in first place-.
    Arbitrage and all kinds of other schemes of speculation and manipulation are not welcomed, but the communities have to deal with. There will probably be chaos in the beginning, but a stabilization curve will begin soon, and at the end there will be some stable currencies, that are supported by matured community members. The remaining currencies fluctuate very little (unlike Bitcoin and all other altcoins) and will be used for some real trading and not only speculation. Remember that we are talking about a community that governing their own money and have proper rules in order to expand community members, rise up community reputation and strengthen their money. In this new paradigm not only “competition in monies is good, and there is no problem with private money” but also it is necessary that different group of people – despite the geolocations – can issue their money.


    So far, I've roughly explained the mission of the software and how it works. You may encounter some of these questions.

    - Why would people want to join to a community and why would they want money of community in the first place?
    Individuals would like to join community for various reasons, such as supporting a mindset or some principals, participating in an “open source accelerator project” or just economic incentives. As mentioned earlier, like-minded people may establish an online community, and set some rules and economic system that they believe in. Obviously they will try to expand community and invite more people to it. Depending on how well the community works -respecting the principals and values of the community-, individuals may join or leave it. The people are the best judges. They can interact with community at different levels. They can use the money of a community as a "medium of exchange" or as a "store of value" and no more. Or they can involve in community activities and decision making and so on.
    There will be people who benefit from exchange rate fluctuations. They may be persuaded to buy that money as well.
    In addition, these coins also can be used to pay for network services. That is, community can set some price for services such as domain name registering, weblog/videolog cost, secure messaging, and so on.


    - There are some online communities and some people joined to this or that community. And they have some kind of tokens (you call it money), and they exchange these tokens for each other. Isn't it like online forums, that already exist hundreds of them? What is special in this idea?
    The more important feature of this software is decentralization, which means the community (any group of people) can be established and continue to live, while no authority, no central-service or no third party can stop them or censor their voice and contents. The community governs its territory and is standalone.
    About the money of community (token), this is a community-based money that gains its value from community, and not from authorities.


    - I am still unconvinced! Where does the value of these currencies come from?
    Lets ask “where does the value of fiat currencies come from?”. The value of “fiat currencies” is grounded in government taxes. The state creates money to pay people to do stuff it wants done, and everyone else must trade to acquire that money to pay taxes or the state will use violence, whereas the value of a community’s money is created by the free choice of community members whom – by the regard of their own interest – judge whether the services community’s money provide is useful to them or not. If  I believe in my community’s money I expect to buy some good or services from my community’s member. Say ten million coin for one pizza (as it happened before). It is the zero day for set value of the money of the community. The money’s value come from the exception of future purchasing power. If community acts according to its principles, we expect that the purchasing power of money of community will remain same or even increase day by day. Because the community proved its consistency for some principles. This consistency drives more demand for that money from who believes in those principles.

    - The community members -by the any reason- believed in money of their community, why should the outsiders believe that money worth to accept?
    It depends on how the community has introduced itself to the public. The functionality of that particular money is also important. I will describe what I mean by “money functionality” in next few lines when I explain the features of money of my favorite community (named “imagine”).

    - How these online communities will affect on real world? How they can embody their community?
    That is really up to community members. Some communities may prefer to continue in cyberspace. Some other may use this system to handle a “collective or cooperative” activity*, or some will use it as a potential infrastructure for a social movement. There are many use cases for this system in real world.
    I personally use this software to accelerate open source movement and also implement a kind of “good money” and put it in practice as well. I am a free(libre) software proponent (someone know it as FOSS or FLOSS). Therefore I established a community to accelerate free software movement and develop more privacy tools for mass as a serious issue in internet age. I set some rules and monetary system aiming to create some incentives to involve more and more people -despite the fact that most of free(libre) developers doing that voluntarily-. The approach is invite as possible as people -and not only software developers- to community. The community called “imagine”, inspired by the Beatles famous song you may know.
    By the way, the imagine members are get paid from imagine’s treasury, because of accomplish the tasks that community considered as a common good for the world, or as a good activity for community. The imagine community runs polling for every single decision, and all community members can vote in proportion to their shares of community. Eventually the imagine’s community will develop/improve many useful software for mass (including “Comen” software itself). The imagine’s community decide “what to produce, how to produce, for whom to produce, and why”. They do real work to produce real product to solve real world problem. The Comen software by itself is the “product” and “means of production” simultaneously.
    So for now the imagine community established one of its blockchain real-world practices. There are many more different types of activities that imagine members can do in order to solve real world problems. The jobs are not limited to cyberspace or digital activities. In imagine we have a long term plan to accomplish physical activities by virtual money. Later I will send an independent post only for covering this chapter.


    - How do these online communities affect their members in the real world economically?
    It depends on community’s policy. The Comen software has embedded DeFi features (e.g. send & receive money, loan & lending, interest rate setting, and smart contracts to manage all these stuff in a secure way, with almost zero cost). If community members decide to trade inside community (even an small portion of daily turnovers), community will take benefit of these trades aiming to improve community’s members welfare. The community can also run a kind of “mutual banking system”, in order to provide funds for community members. All of these improvements depends on community’s willingness to do so. The software and its infrastructure are designed in a highly flexible mode and supports all these optional features. More members convinced to use DeFi, more “financial sovereignty”, more wealthy community.
    As an example:
    The “imagine” community has customized monetary and wealth distribution mechanism, in which it makes a “good money” for its needs. The monetary works like that:
    - The community’s money called PAI.
    - Every 12 hours 45 million PAI are minting. (numbers are not random numbers)
    - The new PAIs right after creating are divided between community members in proportion to their shares.
    - The shares of each member are calculated based on how many hours that member dedicated to accomplish communities task. In early days tasks are all activities about software development. Such as coding, testing, documenting, presentations, design, educational stuff, transcript and translation, reviewing, etc… later, community accepts more types of not-online activities, such as common good activities of all kind of NGOs or non profit organizations, etc…
    - Once a member accomplish a task, she/he/organization will be paid for 7 years for that single contribute. Two times per day, directly to contributor’s wallet.
    - Because of that contribution, the member has voting rights as well, for next 7 years.
    - Every 20 years the amount of minting coins will be half (halving), so eventually the total amount of money supply will be a FIX number. This is the one of the features of a “good money”. “imagine” doesn’t allowed to print money out of thin air.
    Because of these mechanisms the imagine’s community creates a “good money”. One key feature of a “good money” is “appreciating purchasing power over time” and imagine PAI has this feature. So people willingly accept this money in return of their goods or services, while people can earn this money by participating in community activities as well.
    Having fix income for next 7 years, adds up another useful feature to imagine PAI, which is “credit”. This credit is real definition of credit and it is not like the fractional-reserve definition of credit. The credit in imagine means, you have steady, unconditional income for next 7 years, despite the all changes in world (stagnation, crisis, pandemic,...). It works even better than all insurance you can buy. You do not have to cut a part of your income in advanced, for an uncertain future. We just changing our point of view. People get paid regularly. There is no condition. It works perfectly in case of losing job or even passing away. It just needed to save a copy of private keys in a safe for the heirs.
    You can benefit this steady income for installment purchase as well. There are more benefits for this steady income but for the sake of brevity I stop here.
    In these lines I explained the economic effect of one particular community. Different communities are likely to emerge that will have different economic impacts on their members.


    - What are the advantages of these new currencies over government fiat currencies?
    Not all new currencies will necessarily have more advantage than Fiat currencies, but certainly low-quality currencies will not survive, and "good money" will continue to grow over time. Here is listed what is “good money”.
    Generally, the good money must have intrinsic value, Something like "labor theory of value" but do not stick that too much. The good money must appreciates its value over time. That is, doesn't lose its purchasing power even after decades. It must be regulated and do not fluctuating too much. It must be scarce and has utility value, meanwhile it must has optimal granularity. The good money must support “financial sovereignty” and must not be seizeable, also must be cheaply transferable. The good money definitely must have 3 classical factor of money. Unit of account, Medium of exchange, Store of value.
    imagine PAI is designed to cover all of these features. It has mechanisms and procedures to make it self-regulated, anti-fluctuation and not-inflationary. These features will be explained in an independent post.


    - Even if it's the best money, its market is very thin, so there is no utility or applicability in these new currencies.
    True, the primary market is thin, but that doesn't mean the market won’t grow forever! The new currencies, as a good money, will gradually prove themselves against the cheap fiat paper currencies, specially when they are accessible for mass with low cost or zero cost and in return of some useful tasks. The imagine PAI is designed to be easily acquirable to people who want to help others. In long run this money will be distributed fairly and because of real helping the world, which provides huge liquidity as well.





    - what is the imagine’s perspective?
    These are objectives of imagine’s community.
    - Write and release a software as a “community maker engine“ under free(libre), open source software license.
    - The endless development of the software, in order to reform the standard internet based on current classic internet. Particularly removing centralism flaws and improving privacy issues.
    - Introducing a kind of “good money” and put it in practice meanwhile plan to distribute it fairly.
    - Accelerating “open movement”. That includes open software, open hardware, open data, open science, open technology, open money and open communities.
    - Interacting philanthropists in order to amplifying and resonating their efforts.

    Each of the above goals is a significant change towards improving human society, and I hope we achieve all of these for all people of the world.


    Recap:
    - They are different communities with different currencies and different governing rules and economic system.
    - There are exchange rates between currencies of communities.
    - The greater the excellence of community, the greater the demand for that its money, and this in turn increases the value of that money.
    - People simply exchange the different community’s currencies to appreciate their portfolio's value, meanwhile they express their opinions as well.
    - These exchange rates express the people feelings about the communities and impact on communities decisions as well.
    - The communities correlate  to real day to day life of their members economically. Indeed the community and real life impact on each other.
    - People vote for or against every single changes, frequently, by minimum cost, and effect immediately.


    * since you do not need classic internet domain registration and host renting and all costs of create a website and the maintenance, and the users have “data sovereignty” plus “financial sovereignty”, the software is a great alternative for current internet websites. You will not lose domain name and your data of your website will be always accessible for you and your network, without paying one penny.




    Tags: Comen, imagine, cryptocurrency, blockchain, cryptovalue, blockgraph, cryptography, free (libre) software, FOSS, FLOSS, open source, privacy and privacy tools, decentralization, liquid democracy, prosperity

    )"
  },



  {
    "Open Money, a glue for accelerate",
    R"(

    Imagine the open software developers have a common value-presenter to share together all through the world.
    This value-presenter presents the fact that the owner was dedicated X amount of time to design/develop/test... an open source software, and because of that good job she/he/organization earned a certain amount of special type of coins as a receipt. So these coins an be spent in order to purchase goods or services or …
    If you insist on voluntary contribution that’s OK too. What if you donate whole the coins you earned for your contribution to a charity or organization? It is a good incentive for all parties. Whether contributor, the software or charity or organization to resonate each other. Take a look at this flowchart:

    Hopefully everything should be clear, except the treasury concept. The thing is our imaginary coins must be created beforehand, to be distributable and spendable as well. So we launch a treasury that regularly mints fix amount of coins every day. Now we have coins and we can distribute it between people who did some improvement on an Open Source software, or even better, we can distribute coins between who did something good for world improvement. In such a way we can expand all of goodness we are strive for in open source, all over the glob.

    You may ask Who define what is good for world and how we can qualify the issue?
    Actually there is not a single entity or organization to be able to answer to this question. There is also the problem of “what you think is good, is not what I think is good”. The only solution is voting. Voting for every single decision. Is it feasible? In old classical human interaction it was too costly and nearly impossible, but with blockchain technology it is possible. Although it could be more efficient if we implement a kind of delegation “sub-system”. I intentionally name sub-system, because what we are going to implement is a liquid-democracy, in which everyone can vote individually or delegate it to parties or organizations or a group of specialists. Adding ZKP (Zero Knowledge Proof) on top of it, we have capacity for running thousand polling per day for entire world population with strong privacy protection.
    Is it really & technically possible? Short answer, Yes.
    Recap:
    - Volunteers join to network
    - There is a long TODO list of software developments, tests, designs, surveys, documenting, translating…
    - If you are agree & can & like to do some of them simply do it (or dedicate small amount of time to carry out, even small step) and be a shareholder
    - If you think there is something more important to do (e.g. another software), simply propose it and hopefully community accept it and someone will help you to accomplish the job.

    Once a task is done, contributor(s) became a shareholder and regularly get paid for next 7 years.
    Powerful things happen when like-minded people connect, so at the end of the day we will integrate all goodness of open-software, privacy tools, social networks,... together in a single open product. Accessible for everyone, in an easy-find manner.
    No single small project will be abandoned any more. All will be forever on blockchain and time by time, the brand-new contributors pushing it forward continuously.
    It is all about sharing good faith to make the world a better place, as it deserves. It is deployed for the common good, as open source philosophy is.
    This project expands Open source’s frontiers to Open hardware, Open science and Open technology? Where every single paper of researches is accessible for everyone, and most important technologies can be used openly by everyone and every where. And eventually Open communities.


    P.S.
    I. For the sake of brevity I wrote the rough idea. In next posts I will send different aspects of project.
    II. It is not a fanciful proposal, But a pragmatic one which is absolutely feasible.
    III. This idea is not a complete & closed recipe. It is a pure booting infrastructure, to build a huge freedom building on top of it.

    )"
  },



  {
    "An alternative monetary and power/wealth distribution model",
    R"(


    Why we need to change monetary system?
    The answer is reining in the power of the banking system.
    The most corruption in today's life is because failed banking system -either governmental or commercial -. In next lines I try to explain an alternate economic model in order to start modifying the things that are not working properly. gradually and progressively.
    At the end of day we will have a new power and wealth distribution model, which will be more just than what exist now.

    Notes:
    I. For the sake of brevity I wrote the rough idea. You can find more details and answer of all questions in FAQ.
    II. It is not a fanciful proposal, But a pragmatic one which is absolutely feasible.
    III. This idea is not a complete & closed recipe. It is a pure booting infrastructure, to build a huge freedom building on top of it.

    In next few lines I try to explain an alternate economic model.
    Most economists say, the main point of failure in current financial system are 2 facts.
        • Failed monetary system (governmental banking):
          - Is not auditable nor accountable
          - Is not democratic membership nor democratic  decision making

        • Failed fractional reserve (commercial banking)
          - Dept-based economy growth, which is not based on real productivity growth

    These critics are obvious for almost all of us which had seen what happened in 2008 and what is going on nowadays all over the world. so instead of argue about that facts, for sake of brevity lets go to proposed solution. It could be a good solution as well as worst one. By the way we already know the old solutions, and how they will finish, don’t we?

    “We cannot solve our problems with the same thinking we used when we created them.”
    Albert Einstein

    First of all we have to think globally, although we wrongly used to think nationwide or even worst local-focused, now we have to think globally, simply because something is local and something is global and economy is one of extremely global issues. If we create inflation and export it, definitely it comes back to us. It is how economy works.

    lets start with creating a new money. We assume an imaginary monetary system which day by day mints x amount of coins. Because we need these coins in our wallets to spend it to buy goods and services. You may ask how these coins will be distributed?  The answer is straightforward. The coins will be distributed between people who worked for the network or did something good for the world. It is too wide and vague! For more details please read the FAQ: “Who can own stocks?” and “How are new coins distributed?”
    Till now, we have a monetary system with a fix amount of daily minted coins. The coins are dividing between system shareholders, based on their shares amount. The shares are created by proposals of new members. They made a proposal, stating how many hours worked for “common good” and “world benefit”, and the current shareholders accept or negate the proposal. If the current shareholders accept proposal, the proposer will be a new shareholder.
    Being shareholder means you will be get paid from treasury and you can vote for future proposals (including your future proposals). Your vote power or what you get as a shareholder, depends on your shares amount. You dedicate more hours to system and being rewarded by more shares.
    Your shares lasts for 7 years. That is you do something results you will get paid 7 years instead of one time. It is a huge improvement in all aspect of contractual works. You know, this 7 year income works better than most expensive insurance you can buy! You will get paid unconditionally for next 7 years. It works perfectly if you lose your job or in case of passing away. You just need to save a copy of your private keys in a safe for the heirs.
    Now we have a self-modifier game! Old shareholders do not want to add new members. Instead they prefer to increase their shares. But they have to embrace new shareholders. Otherwise how the money popularity, applicability and usability will be expanded? What value has the money if no one use it? For more details look at FAQ: “There is a perverse incentive for shareholders to undervalue the work by other people (up to zero).”

    Due the fact that shareholders have to accept new shareholders, it will be better to accept who really did something good for this  money ecosystem. Since ecosystem is global, therefore community accept proposal of people who did something good , really good for world.

    These coins in early days worth nothing. They can be used for paying network costs (e.g. record data on blockchain, send encrypted messages, running smart contracts etc). Over time, need for using network services drives demand for network coins, and it cause to raise up the value of network’s money.

    Lets recap.
    - x coins daily minted.
    - coins divided between shareholders.
    - shareholders have to wisely accept new shareholders.
    - no one forced to use this coin. There is no military or legal enforcement.
    - as shareholders act well the coin value increases, and as corrupted community is coin value decline.
    - new shareholders means less dividends and less coin for each shareholder, demand and supply rule => increasing coin value.
    - demand for using  network services, will increase the the demand for network coins, means raise up the coin value.
    - this money has intrinsic value too, because are payed in return of real work, which cheap fiat central bank printed monies haven’t.
    - there is also halving (the process in which every 20 years, daily minted coins will be half), means more scarcity

    as a result, the money is  deflationary. Means it’s values appreciated by time. can this money be subject of hyper-deflation? Probably yes, so we need to regulate it, in order to avoid hyper-deflation , market fluctuate and Pump/Dump conspiracy. lets imagine we have reserved coins as well. That is, every day our monetary system mints x coins, and 4x reserved coins. The reserved coins as it names defined are reserved and can not be spend easily! The reserved coins can be released if a certain time elapsed and the share holders are agree to release it. Again remember “as shareholders act well the coin value increased”. This self-modifier mechanism works pretty well. A cyclic incentive process.

    Right now we eliminated the government banking problem. No money out of thin air. No easy money based nothing. No unpredictable money supply.
    We also distributed the money in a fair mechanism. You may ask “what about who can not participate in project”? The short answer is “There is enough space for ALL to get paid from treasury”. For more details please look as these in FAQ: “Who can own stocks?”

    Lets go to commercial banking problem which is fractional reserve banking system. Once upon a time the fractional reserve banking was a form of embezzlement or financial fraud, but now it is legalized! The real economy growth comes from real productivity growth and not using the others money to lend and earn interest. This fake growth is like going up on the ladder more rungs to be fallen more harder!
    How we can stop bankers from fractional reserve? We can not! We have to have our money, separated from banks, and stand alone. It was the Bitcoin promises. “financial sovereignty”. If you have your private keys, you control your money, if you don’t have, the bank/exchange/institute… controls your money. That is easy simple. You do not need Banks, so control your money.
    You may ask about “usability” and “applicability” of new money and being in doubt why someone has to accept this money? Please read FAQ: “Scarcity doesn't give value to the coin.”, “How the system creates value (in economic term)?”, “Is its money a kind of utility token?”, “Will these currencies will be vastly used by mass and they will have real usability and applicability?” and “will get around the power and centrality of the largest economic units - the financial and industrial capitals?”

    You may want your money works for you (e.g. putting your money in your bank account and get interest). Or you may want to get loan from bank. Or you may want to put your home as collateral and get a loan.
    The good news is you can do almost all of your financial needs by new money as well, and even more efficient and zero fraudulent.
    You lend real money and borrower gets real money, not “fake credit/dept”. In such system we will not face the credit/dept cycles and the economy growth gradually. We have an strong DeFi in our system that has absolutely no fraudulent risk. In addition there is a reputation sub-system by which the parties can leverage the trades with less risk.. the system in whole freed individuals from fiduciary media’s dominance on financial market, specially cases where clients need a kind of credit or collateral for lending or borrowing. For more details look at the project wiki on DeFi. (the project wiki is a fully decentralized wiki, that you can access to it by install the software on your laptop)

    hopefully this new monetary system will help us to reach human prosperity.

    )"
  }


};

