#ifndef WIKISINITCONTENT_H
#define WIKISINITCONTENT_H

#include <QHash>

class InitWikiContent
{
public:
  InitWikiContent(){};
  static QHash<QString, QString> s_wiki_initial_pages;
};


#endif // WIKISINITCONTENT_H
