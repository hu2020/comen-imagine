#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/wallet/wallet.h"
#include "init_contents/wikis_init_contents.h"
#include "lib/block/document_types/document.h"
#include "lib/services/free_docs/free_doc_handler.h"
#include "lib/block/document_types/free_documents/free_document.h"

#include "wiki_handler.h"


// same as insertRegDoc
bool WikiHandler::insertPage(
  const Block& block,
  const FreeDocument* wiki)
{
  if (
  (wiki->m_doc_title == "") ||
  (wiki->m_iname == "") ||
  (wiki->getDocHash() == "") ||
  (wiki->m_doc_signer == "") ||
  (wiki->m_doc_content == ""))
  {
    CLog::log("invalid wiki page info: " + wiki->m_doc_title, "app", "fatal");
    return false;
  }

  QString wkp_title = CUtils::sanitizingContent(wiki->m_doc_title);
  QString wkc_content = CUtils::sanitizingContent(wiki->m_doc_content);

  QString title_hash = wkp_title;
  if (title_hash == "")
    title_hash = CCrypto::convertTitleToHash(title_hash);

  QString wiki_page_hash = wiki->m_wiki_page_hash;
  if (wiki_page_hash == "")
    wiki_page_hash = CCrypto::keccak256(wiki->m_iname + "/" + wiki->m_doc_title);

  // check if exist newest version
  QueryRes exist = DbModel::select(
    stbl_wiki_pages,
    {"wkp_hash"},
    {{"wkp_hash", wiki_page_hash},
    {"wkp_last_modified", block.m_block_creation_date, ">="}});
  if (exist.records.size() > 0)
  {
    CLog::log("already a newer version of wikipage(" + CUtils::hash16c(wiki_page_hash) + ") exist in db");
    return true;
  }

  // check if same doc alredy recorded
  exist = DbModel::select(
    stbl_wiki_pages,
    {"wkp_hash"},
    {{"wkp_doc_hash", wiki->getDocHash()}});
  if (exist.records.size() > 0)
  {
    CLog::log("wiki doc already recorded(" + CUtils::hash16c(wiki->getDocHash()) + ") exist in db");
    return true;
  }

  QVDicT values {
    {"wkp_title", wiki->m_doc_title},
    {"wkp_iname", wiki->m_iname},
    {"wkp_doc_hash", wiki->getDocHash()},
    {"wkp_hash", wiki_page_hash},
    {"wkp_language", wiki->m_content_language},
    {"wkp_format_version", wiki->m_free_doc_version},
    {"wkp_creation_date", block.m_block_creation_date},
    {"wkp_last_modified", block.m_block_creation_date},
    {"wkp_creator", wiki->m_doc_signer}};
  CLog::log("inserting a wiki page:" + CUtils::dumpIt(values));
  DbModel::insert(
    stbl_wiki_pages,
    values);

  // insert contents too
  values = {
    {"wkc_wkp_hash", wiki_page_hash},
    {"wkc_content", wkc_content}};
  CLog::log("inserting a wiki page content: " + CUtils::dumpIt(values));
  DbModel::insert(
    stbl_wiki_contents,
    values);

  if (CMachine::getDAGIsInitialized())
    CGUI::signalUpdateWikiTitles();

  return true;
}

// js name was updatePage
bool WikiHandler::updateWkDoc(
  const Block& block,
  const FreeDocument* free_doc)
{
  auto[status1, title] = CUtils::decompressString(free_doc->m_doc_title);
  auto[status2, content] = CUtils::decompressString(free_doc->m_doc_content);
  if (!status1 || !status2)
  {
    CLog::log("Invalid decompress wiki page info: " + free_doc->safeStringifyDoc(), "app", "warning");
    return false;
  }

  if (
  (title == "") ||
  (content == "") ||
  (free_doc->getDocHash() == "") ||
  (free_doc->m_doc_signer == "") ||
  (free_doc->m_iname == "")
  ) {
    CLog::log("Invalid update wiki page info: " + free_doc->safeStringifyDoc(), "app", "warning");
    return false;
  }

  title = CUtils::sanitizingContent(title);

//  if ((wkp_hash))
//      wkp_hash = iutils.convertTitleToHash(wkp_title);

  QString wiki_page_hash = free_doc->m_wiki_page_hash;
  if (wiki_page_hash == "")
    wiki_page_hash = CCrypto::keccak256(free_doc->m_iname + "/" + free_doc->m_doc_title);

  // check if exist newest version
  QueryRes exist = DbModel::select(
    stbl_wiki_pages,
    {"wkp_hash"},
    {{"wkp_hash", wiki_page_hash},
    {"wkp_last_modified", block.m_block_creation_date, ">="}});
  if (exist.records.size() > 0)
  {
    CLog::log("already a newer version of wikipage exist in db! page(" + CUtils::hash8c(wiki_page_hash) + " / " + free_doc->getDocHash() + ")", "app", "warning");
    return true;
  }

  // check if same doc alredy recorded
  exist = DbModel::select(
    stbl_wiki_pages,
    {"wkp_doc_hash"},
    {{"wkp_doc_hash", free_doc->getDocHash()}});
  if (exist.records.size() > 0)
  {
    CLog::log("The wikipage already recorded! page(" + CUtils::hash8c(wiki_page_hash) + " / " + free_doc->getDocHash() + ")", "app", "warning");
    return true;
  }

  QVDicT updates {
    {"wkp_doc_hash", free_doc->getDocHash()},
    {"wkp_language", free_doc->m_content_language},
    {"wkp_format_version", free_doc->m_free_doc_version},
    {"wkp_last_modified", block.m_block_creation_date},
    {"wkp_creator", free_doc->m_doc_signer}
  };
  CLog::log("updating wiki page: " + CUtils::dumpIt(updates), "app", "trace");

  DbModel::update(
    stbl_wiki_pages,
    updates,
    {{"wkp_hash", wiki_page_hash}});

  // update contents too
  updates = {{"wkc_content", content}};
  CLog::log("updating wiki page content: " + CUtils::dumpIt(updates), "app", "trace");

  DbModel::update(
    stbl_wiki_contents,
    updates,
    {{"wkc_wkp_hash", wiki_page_hash}});

  return true;
}

std::tuple<bool, QString, FreeDocument*> WikiHandler::prepareNewWikiPageRegReq(
  CAddressT creator_address,
  const CDocHashT& wiki_page_hash,
  const QString& wiki_page_title,
  const QString& wiki_page_content,     // the page content, formatted as mediawiki standards
  const QString& page_language,
  QString wiki_iname,  // the domain name of wiki
  const CDateT& creation_date)
{
  QString msg;
  /**
   * wkTitle is the name of page e.g. imagine/wiki/fruits or imagine/wiki/apple
   * NOTE: wiki does not support more than one layer slashes e.g. imagine/wiki/fruits/apple is not a valid wiki URI
   */

  QString san_wiki_page_title = CUtils::sanitizingContent(wiki_page_title);

  QString san_wiki_page_content = CUtils::sanitizingContent(wiki_page_content);    // TODO: implement string compression as well
  san_wiki_page_content = BlockUtils::wrapSafeContentForDB(san_wiki_page_content).content;

  if (wiki_iname == "")
  {
    msg = "Invalid/Empty wiki URI Address!";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }

  if ((creator_address == "") || !CCrypto::isValidBech32(creator_address))
  {
    msg = "Invalid wiki signer(" + creator_address + ")!";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }

  QString recalc_wiki_page_hash = CCrypto::keccak256(wiki_iname + "/" + san_wiki_page_title);

  QString document_class;
  if (wiki_page_hash == "")
  {
    document_class = CConsts::FPOST_CLASSES::WK_CreatePage;
    QueryRes exist = DbModel::select(
      stbl_wiki_pages,
      {"wkp_hash"},
      {{"wkp_hash", recalc_wiki_page_hash}});
    if (exist.records.size() > 0)
    {
      msg = "The title(" + san_wiki_page_title + ") for desired address(" + (wiki_iname + "/" + san_wiki_page_title) + ") wiki is already registerd!";
      CLog::log(msg, "app", "error");
      return {false, msg, nullptr};
    }

  } else {
    document_class = CConsts::FPOST_CLASSES::WK_EditPage;
    if (recalc_wiki_page_hash != wiki_page_hash)
    {
      msg = "Desired wiki to update has not valid uniq hash, respect it's title & iName!";
      CLog::log(msg, "sec", "error");
      return {false, msg, nullptr};
    }

  }

  if (creator_address == "")
    creator_address = CMachine::getBackerAddress();

  QJsonObject free_doc_json {
    {"dType", CConsts::DOC_TYPES::FPost},
    {"dClass", document_class},
    {"cLang", page_language},
    {"dVer", CConsts::DEFAULT_VERSION},
    {"fVer", CConsts::DEFAULT_VERSION},
    {"dCDate", creation_date},
    {"dSigner", creator_address},
    {"dTitle", san_wiki_page_title},
    {"dContent", san_wiki_page_content},
    {"iName", wiki_iname}};
  FreeDocument* document = new FreeDocument(free_doc_json);

  // do sign doc by proper key
  QString sign_message = document->getDocSignMsg();
  auto[sign_status, res_msg, sign_signatures, sign_unlock_set] = Wallet::signByAnAddress(
    creator_address,
    sign_message);
  if (!sign_status)
    return {false, res_msg, nullptr};

  document->m_doc_ext_info = QJsonArray { QJsonObject {
    {"signatures", CUtils::convertQStringListToJSonArray(sign_signatures)},
    {"uSet", sign_unlock_set}}};

  document->setDExtHash();
  document->setDocLength();
  document->setDocHash();

  Block* tmp_block = new Block(QJsonObject {
    {"bCDate", CUtils::getNow()},
    {"bType", "futureBlockWiki"},
    {"bHash", "futureHashWiki"}});
  GenRes full_validate = document->fullValidate(tmp_block);
  if (!full_validate.status)
  {
    msg = "Failed in full validate wiki page, " +full_validate.msg+ " title(" + san_wiki_page_title + ")";
    CLog::log(msg, "app", "error");
    return {false, msg, nullptr};
  }

  return {true, "", document};
}

std::tuple<bool, QString> WikiHandler::createNewWikiPage(
  const QString& wiki_page_title,
  const QString& wiki_page_content,
  const CDocHashT& wiki_page_hash,
  const QString& page_language,
  const QString& wiki_iname)
{
  CAddressT creator_address = CMachine::getBackerAddress();

  // create proper doc and send it to network
  //QString iname_hash = CCrypto::convertTitleToHash("imagine");
  auto[create_status, create_msg, free_doc] = prepareNewWikiPageRegReq(
    creator_address,
    wiki_page_hash,
    wiki_page_title,
    wiki_page_content,
    page_language,
    wiki_iname);  // the domain name of wiki

  if (!create_status)
    return {false, create_msg};

  // prepare payment doc too
  return FreeDocHandler::payForFreeDocAndPushToBuffer(free_doc);
}
