#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/wallet/wallet.h"
#include "init_contents/wikis_init_contents.h"
#include "lib/block/document_types/document.h"
#include "lib/utils/render_handler/render_handler.h"
#include "lib/services/contracts/flens/iname_handler.h"
#include "lib/block/document_types/free_documents/free_document.h"

#include "wiki_handler.h"
#include "wiki_handler_pages.cpp"

// -  -  -  -  -  -  -  -  WikiHandler

QString WikiHandler::stbl_wiki_pages = "c_wiki_pages";
QStringList WikiHandler::stbl_wiki_pages_fields = {"wkp_id", "wkp_iname", "wkp_title", "wkp_doc_hash", "wkp_hash", "wkp_language", "wkp_format_version", "wkp_creation_date", "wkp_last_modified", "wkp_creator"};

QString WikiHandler::stbl_wiki_contents = "c_wiki_contents";
QStringList WikiHandler::stbl_wiki_contents_fields = {"wkc_wkp_hash", "wkc_content"};

WikiHandler::WikiHandler()
{

}

bool WikiHandler::initTmpWikiContent()
{
  CLog::log("Insert initial wiki pages", "app", "trace");

  QString creation_date = CMachine::getLaunchDate();
  QString iname_hash = CCrypto::convertTitleToHash("imagine");

  Block tmp_block {};
  tmp_block.m_block_creation_date = creation_date;

  for (QString a_page_title: InitWikiContent::s_wiki_initial_pages.keys())
  {
    QString wiki_page_hash = CCrypto::keccak256("imagine/" + a_page_title);
    CLog::log("Insert wiki page " + a_page_title + "(" + CUtils::hash8c(wiki_page_hash) + ").", "app", "trace");
    QJsonObject document_json {
      {"dType", CConsts::DOC_TYPES::FPost},
      {"dClass", CConsts::FPOST_CLASSES::WK_CreatePage},
      {"dTitle", a_page_title},
      {"iName", "imagine"},
      {"dHash", wiki_page_hash}, // dummy hash assigning
      {"language", CConsts::DEFAULT_LANG},
      {"fVer", CConsts::DEFAULT_CONTENT_VERSION},
      {"dCDate", creation_date},
      {"dSigner", CConsts::HU_INAME_OWNER_ADDRESS},
      {"dContent", BlockUtils::wrapSafeContentForDB(InitWikiContent::s_wiki_initial_pages[a_page_title]).content}};
    FreeDocument* tmp_doc = new FreeDocument(document_json);
    insertPage(tmp_block, tmp_doc);
    delete tmp_doc;
  }

  // delegate permission to ALL to post on imagine domain
  // TODO:
  return true;
}



bool WikiHandler::recordInWikiDB(
  const Block& block,
  const FreeDocument* free_doc)
{
  if (free_doc->m_doc_class == CConsts::FPOST_CLASSES::WK_CreatePage)
  {
    return insertPage(block, free_doc);

  }
  else if (free_doc->m_doc_class == CConsts::FPOST_CLASSES::WK_EditPage)
  {
    return updateWkDoc(block, free_doc);
  }

  return true;

}

QVDRecordsT WikiHandler::searchInWikiContents(
  const ClausesT& clauses,
  QStringList fields,
  const OrderT& order,
  const int limit)
{
  QueryRes contents = DbModel::select(
    stbl_wiki_contents,
    fields,
    clauses,
    order,
    limit);
  return contents.records;
}


QVDRecordsT WikiHandler::searchInWikiPages(
  const bool need_contents_too,
  const ClausesT& clauses,
  QStringList fields,
  const OrderT& order,
  const int limit)
{
  QueryRes posts = DbModel::select(
    stbl_wiki_pages,
    fields,
    clauses,
    order,
    limit);

  if (!need_contents_too)
    return posts.records;

  // retrieve contents
  QStringList page_ids;
  for(QVDicT a_page: posts.records)
    page_ids.append(a_page.value("wkp_hash").toString());

  if (page_ids.size() == 0)
    return {};

  QVDRecordsT pages_content = searchInWikiContents({{"wkc_wkp_hash", page_ids, "IN"}});
  QSDicT content_dict {};
  for (QVDicT aContent: pages_content)
    content_dict[aContent.value("wkc_wkp_hash").toString()] = aContent.value("wkc_content").toString();

  // add content to records
  QVDRecordsT out {};
  for (QVDicT aPost: posts.records)
  {
    aPost["content"] = content_dict[aPost.value("wkp_hash").toString()];
    out.push_back(aPost);
  }

  return out;
}

QVDRecordsT WikiHandler::getOnchainWkPages()
{
  QVDRecordsT records = searchInWikiPages(false, {}, {"wkp_hash", "wkp_title"});

  // retrieve wiki iNames info
  QStringList iname_hashes;
  for(QVDicT a_record: records)
    iname_hashes.append(a_record.value("wkp_iname").toString());

  BindingInfo iNames = INameHandler::searchRegisteredINames(
    {{"in_hash", CUtils::arrayUnique(iname_hashes), "IN"}});
//  let iNames = flensHandler.register.searchRegisteredINames({
//      needBindingsInfoToo: false,
//      query: [
//          ['
//      ]
//  });
  QSDicT iNamesDict {};
  for (QVDicT anIName: iNames.m_records)
    iNamesDict[anIName.value("in_hash").toString()] = anIName.value("iName").toString();

  QVDRecordsT final_records = {};
  for (QVDicT aRec: records)
  {
    aRec["iName"] = iNamesDict.keys().contains(aRec.value("wkp_iname").toString()) ? iNamesDict[aRec.value("wkp_iname").toString()] : "-";
    aRec["url"] = aRec.value("iName").toString() + "/wiki/" + aRec.value("wkp_title").toString();
    aRec["content"] = RenderHandler::renderToHTML(aRec.value("content").toString());
    final_records.push_back(aRec);
  }

  return final_records;
}

QString WikiHandler::renderPage(const QString& page_title)
{
  QVDRecordsT pages = searchInWikiPages(true, {{"wkp_title", page_title}});
  if (pages.size() ==0)
    return "";

  QString out = pages[0].value("content").toString();
  out = RenderHandler::renderToHTML(out);

  return out;
}
