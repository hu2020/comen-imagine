#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/block/document_types/dna_proposal_document.h"
#include "lib/block/block_types/block_coinbase/coinbase_issuer.h"

#include "proposal_handler.h"

const QString ProposalHandler::stbl_proposals = "c_proposals";
const QStringList ProposalHandler::stbl_proposals_fields = {"pr_id", "pr_hash", "pr_type", "pr_class", "pr_version", "pr_title", "pr_descriptions", "pr_tags", "pr_project_id", "pr_help_hours", "pr_help_level", "pr_voting_timeframe", "pr_polling_profile", "pr_contributor_account", "pr_start_voting_date", "pr_conclude_date", "pr_approved"};

const QString ProposalHandler::stbl_machine_draft_proposals = "c_machine_draft_proposals";
const QStringList ProposalHandler::stbl_machine_draft_proposals_fields = {"pd_id", "pd_mp_code", "pd_hash", "pd_type", "pd_class", "pd_version", "pd_title", "pd_comment", "pd_tags", "pd_project_hash", "pd_help_hours", "pd_help_level", "pd_voting_timeframe", "pd_polling_profile", "pd_contributor_account","pd_body", "pd_creation_date"};

ProposalHandler::ProposalHandler()
{

}

/**
 * @brief ProposalHandler::calcProposalApplyCost
 * @param help_hours
 * @param help_level
 * @param cDate
 * return {one_cycle_income, apply_cost}
 */
std::tuple<CMPAIValueT, CMPAIValueT> ProposalHandler::calcProposalApplyCost(
  const uint32_t& help_hours,
  const uint8_t& help_level,
  const QString& cDate)
{
  //cDate is the creation date of the block in which contribute is recorded (start date)

  uint64_t the_contribute = help_hours * help_level;
  FutureIncomes future_incomes = CoinbaseIssuer::predictFutureIncomes(
    the_contribute,
    cDate,
    1);
  CLog::log("predicted Future definite incomes(" + CUtils::microPAIToPAI6(future_incomes.m_definite_incomes) + ") ", "app", "info");

  CMPAIValueT apply_cost = future_incomes.m_monthly_incomes[0].m_income_per_month * CConsts::PROPOSAL_APPLY_COST_SCALE; // 3 * first month incom
  return {future_incomes.m_monthly_incomes[0].m_one_cycle_income, apply_cost};
}

bool ProposalHandler::removeProposal(const QString& proposal_hash)
{
  //sceptical test
  QueryRes exist = DbModel::select(
    stbl_proposals,
    {"pr_hash"},
    {{"pr_hash", proposal_hash}});

  if (exist.records.size() != 1)
  {
    CLog::log("Try to delete proposal strange result! " + CUtils::dumpIt(exist.records), "sec", "error");
    return false;
  }

  DbModel::dDelete(
    stbl_proposals,
    {{"pr_hash", proposal_hash}});

  return true;
}

QVDRecordsT ProposalHandler::searchInProposals(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_proposals,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}

QVDRecordsT ProposalHandler::searchInDraftProposals(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_machine_draft_proposals,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}

bool ProposalHandler::transformApprovedProposalToDNAShares(
  const QVDicT& polling,
  const CDateT& approveDate,
  CDocHashT dnProjectHash)
{
  if (dnProjectHash == "")
    dnProjectHash = CCrypto::convertTitleToHash("imagine");

  CDocHashT proposalHash = polling.value("pll_ref").toString();

  QVDRecordsT proposals = searchInProposals({{"pr_hash", proposalHash}});
  if (proposals.size() == 0)
  {
    CLog::log("proposal(" + CUtils::hash8c(proposalHash) + ") doesn't exisat in recorded proposals", "app", "error");
    return false;
  }
  QVDicT proposal = proposals[0];

  CDateT nowT = CUtils::getNow();

  proposal["pr_help_level"] = trunc(proposal.value("pr_help_level").toDouble());
  // insert in DNA

  DNAProposalDocument* dnaDoc = new DNAProposalDocument();
  dnaDoc->m_doc_hash = proposal.value("pr_hash").toString();
  dnaDoc->m_contributor_account = proposal.value("pr_contributor_account").toString();
  dnaDoc->m_project_hash = dnProjectHash;
  dnaDoc->m_help_hours = proposal.value("pr_help_hours").toDouble();
  dnaDoc->m_help_level = proposal.value("pr_help_level").toDouble();
  dnaDoc->m_shares = dnaDoc->m_help_hours * dnaDoc->m_help_level;
  dnaDoc->m_doc_title = proposal.value("pr_title").toString();
  dnaDoc->m_doc_comment = proposal.value("pr_descriptions").toString();
  dnaDoc->m_doc_tags = proposal.value("pr_tags").toString();
  dnaDoc->m_votes_yes = polling.value("pll_y_value").toDouble();
  dnaDoc->m_votes_abstain = polling.value("pll_a_value").toDouble();
  dnaDoc->m_votes_no= polling.value("pll_n_value").toDouble();
  dnaDoc->m_doc_creation_date = approveDate;

  CLog::log("Adding shares because of proposal(" + CUtils::hash8c(dnaDoc->m_doc_hash) + "): " + dnaDoc->safeStringifyDoc(), "app", "info");

  if (CMachine::isInSyncProcess())
  {
    QVDRecordsT exist = DNAHandler::searchInDNA({{"dn_doc_hash", proposal.value("pr_hash")}});
    if (exist.size() == 0)
    {
      DNAHandler::insertAShare(dnaDoc);
    } else {
        // update votes
        DNAHandler::updateDNAVotes(
          {{"dn_doc_hash", proposal.value("pr_hash")}},
          {{"dn_votes_y", polling.value("pll_y_value")},
          {"dn_votes_a", polling.value("pll_a_value")},
          {"dn_votes_n", polling.value("pll_n_value")}});
    }

  } else {
    DNAHandler::insertAShare(dnaDoc);
  }

  delete dnaDoc;

  // update proposal too
  DbModel::update(
    stbl_proposals,
    {{"pr_conclude_date", approveDate},
    {"pr_approved", CConsts::YES}},
    {{"pr_hash", proposal.value("pr_hash")}});

  return true;
}

bool ProposalHandler::concludeProposal(
  const QVDicT& polling,
  const CDateT& approveDate)
{
  CDocHashT proposalHash = polling.value("pll_ref").toString();

  // update proposal
  DbModel::update(
    stbl_proposals,
    {{"pr_conclude_date",  approveDate},
    {"pr_approved", CConsts::NO}},
    {{"pr_hash", proposalHash}});

  return true;
}

/**
 * it used in Demos to present a PoW/Normal proposal in Agora
 */
QString ProposalHandler::renderProposalDocumentToHTML(const QJsonObject& proposal)
{
  QString out = "";
  out += "\nProposal Title: " + proposal.value("dTitle").toString();
  out += "\nProposal Description: " + proposal.value("dComment").toString();
  out += "\nProposal Tags: " + proposal.value("dTags").toString();
  out += "\nProposal Hash: " + proposal.value("dHash").toString();
  out += "\nContributor account: " + proposal.value("contributor").toString();
  out += "\nHelp Hours: " + proposal.value("helpHours").toString();
  out += "\nHelp Level: " + proposal.value("helpLevel").toString();
  out += "\nGained Shares: " + QString::number(proposal.value("helpHours").toInt() * proposal.value("helpLevel").toInt());
  out += "\nCreation Date: " + proposal.value("dCDate").toString();
  return out;
}

QJsonObject ProposalHandler::getDraftTpl()
{
  QJsonObject dna_draft_proposal {
    {"dType", CConsts::DOC_TYPES::DNAProposal},
    {"dClass", CConsts::PROPOSAL_CLASESS::General},
    {"dLen", "0000000"},
    {"dVer", "0.0.8"},
    {"dHash", "0000000000000000000000000000000000000000000000000000000000000000"},
    {"dTitle", ""},
    {"dComment", ""},
    {"dTags", ""},
    {"projectHash", CCrypto::convertTitleToHash("imagine")}, // it must be sha3(imagine) intended for imagine main dna, later we can use this ground for public use
    {"dCDate", ""}, // this is proposal creation date, and not the approvment date
    {"helpHours", 0},
    {"helpLevel", 0}, // how important/usefull was this help? 1-12
    {"contributor", ""},
    {"pTimeframe", CMachine::getMinVotingTimeframe()},
    {"pollingProfile", "Basic"},
    {"dExtInfo", QJsonArray {}},
    {"dExtHash", CConsts::NO_EXT_HASH}
  };
  return dna_draft_proposal;
}

std::tuple<bool, QString> ProposalHandler::prepareDraftProposal(
  const CAddressT& pd_contributor_account,
  const QString& pd_title,
  const QString& pd_comment,
  const QString& pd_tags,
  const uint64_t pd_help_hours,
  const uint64_t pd_help_level,
  double pd_voting_timeframe,
  const CDateT& pd_creation_date,
  const QString& pd_polling_profile,
  QString pd_project_hash,
  const QString& mp_code,
  const QString& pd_type,
  const QString& pd_class,
  const QString& pd_version)
{

  CLog::log("pd_voting_timeframe before" + QString::number(pd_voting_timeframe));
  if (CConsts::TIME_GAIN != 1)
    pd_voting_timeframe = ((pd_voting_timeframe/12.0) * CConsts::TIME_GAIN) / 60.0;
  CLog::log("pd_voting_timeframe after" + QString::number(pd_voting_timeframe));

  if (pd_project_hash == "")
    pd_project_hash = CCrypto::convertTitleToHash("imagine");

  QJsonObject draft = getDraftTpl();
  draft["dType"] = pd_type;
  draft["dClass"] = pd_class;
  draft["dVer"] = pd_version;
  draft["dTitle"] = CUtils::sanitizingContent(pd_title);
  draft["dComment"] = CUtils::sanitizingContent(pd_comment);
  draft["dTags"] = CUtils::sanitizingContent(pd_tags);
  draft["projectHash"] = pd_project_hash;
  draft["dCDate"] = pd_creation_date;
  draft["helpHours"] = QVariant::fromValue(pd_help_hours).toDouble();
  draft["helpLevel"] = QVariant::fromValue(pd_help_level).toDouble();
  draft["pTimeframe"] = pd_voting_timeframe;
  draft["pollingProfile"] = pd_polling_profile;
  draft["pollingVersion"] = "0.0.8";
  draft["contributor"] = pd_contributor_account;
  draft["dLen"] = CUtils::paddingLengthValue(CUtils::serializeJson(draft).length());

  DNAProposalDocument* proposal = new DNAProposalDocument(draft);
  proposal->setDocHash();


  Block* tmp_block = new Block(QJsonObject {
    {"bCDate", CUtils::getNow()},
    {"bType", "futureBlockproposal0"},
    {"bHash", "futureHashproposal0"}});
  GenRes final_validate_proposal = proposal->fullValidate(tmp_block);
  delete tmp_block;
  if (!final_validate_proposal.status)
    return {false, "Failed in full validation of proposal. " + final_validate_proposal.msg};

  QVDicT values {
    {"pd_mp_code", mp_code},
    {"pd_hash", proposal->getDocHash()},
    {"pd_type", proposal->m_doc_type},
    {"pd_class", proposal->m_doc_class},
    {"pd_version", proposal->m_doc_version},
    {"pd_title", proposal->m_doc_title},
    {"pd_comment", proposal->m_doc_comment},
    {"pd_tags", proposal->m_doc_tags},
    {"pd_project_hash", proposal->m_project_hash},
    {"pd_help_hours", QVariant::fromValue(proposal->m_help_hours)},
    {"pd_help_level", QVariant::fromValue(proposal->m_help_level)},
    {"pd_polling_profile", proposal->m_polling_profile},
    {"pd_voting_timeframe", proposal->m_voting_timeframe},
    {"pd_contributor_account", proposal->m_contributor_account},
    {"pd_body", BlockUtils::wrapSafeContentForDB(proposal->safeStringifyDoc()).content},
    {"pd_creation_date", proposal->m_doc_creation_date}};
  CLog::log("About to insert new draft proposal: " + CUtils::dumpIt(values));
  bool res = DbModel::insert(
    stbl_machine_draft_proposals,
    values);
  delete proposal;
  if (res)
    return {true, "New proposal(" + CUtils::hash8c(values["pd_hash"].toString()) + ") created and inserted in drafts"};

  return {false, "Something went wrong in creation new proposal"};
}

QHash<uint32_t, QVDicT> ProposalHandler::loadMyDraftProposals(
  const QString& mp_code)
{
  QueryRes res = DbModel::select(
    stbl_machine_draft_proposals,
    stbl_machine_draft_proposals_fields,
    {{"pd_mp_code", mp_code}},
    {{"pd_creation_date", "ASC"}});
  if (res.records.size() == 0)
    return {};

  auto[total_shares, share_amount_per_holder, holdersOrderByShares_] = DNAHandler::getSharesInfo();
  //let { sumShares, holdersByKey } = DNAHandler.getSharesInfo();

  QHash<uint32_t, QVDicT> final_result {};
  uint32_t result_number = 0;
  for (QVDicT a_proposal: res.records)
  {
    uint32_t row_inx = result_number * 10; // 10 row

    final_result[row_inx] = QVDicT {
      {"pd_hash", a_proposal.value("pd_hash")},
      {"proposal_number", result_number + 1},
      {"pd_title", a_proposal.value("pd_title")},
    };
    final_result[row_inx + 1] = QVDicT {
    {"pd_hash", a_proposal.value("pd_hash")},
    {"pd_creation_date", a_proposal.value("pd_creation_date")}};

    final_result[row_inx + 2] = QVDicT {
    {"pd_hash", a_proposal.value("pd_hash")},
    {"pd_comment", a_proposal.value("pd_comment")}};

    final_result[row_inx + 3] = QVDicT {
    {"pd_hash", a_proposal.value("pd_hash")},
    {"pd_tags", a_proposal.value("pd_tags")}};

    final_result[row_inx + 4] = QVDicT {
      {"pd_hash", a_proposal.value("pd_hash")},
      {"pd_help_level", a_proposal.value("pd_help_level")},
      {"pd_help_hours", a_proposal.value("pd_help_hours")},
    };

    final_result[row_inx + 5] = QVDicT {
      {"pd_hash", a_proposal.value("pd_hash")},
      {"pd_voting_timeframe", a_proposal.value("pd_voting_timeframe")}};

    final_result[row_inx + 6] = QVDicT {
      {"pd_hash", a_proposal.value("pd_hash")},
      {"pd_contributor_account", a_proposal.value("pd_contributor_account")},
      {"total_shares", a_proposal.value("total_shares")},
      {"contributor_current_shares", share_amount_per_holder[a_proposal.value("pd_contributor_account").toString()]},
    };

    final_result[row_inx + 7] = QVDicT {
      {"pd_hash", a_proposal.value("pd_hash")},
    };

    final_result[row_inx + 8] = QVDicT {
      {"pd_hash", a_proposal.value("pd_hash")},
    };

    final_result[row_inx + 9] = QVDicT {
      {"pd_hash", a_proposal.value("pd_hash")},
    };

//    let draftPledges = pledgeHandler.searchInDraftPledges({
//        dpl_doc_ref: aPropos.pd_hash
//    });
//    if (draftPledges.length > 0)
//        for (let aPlg of draftPledges) {
//          aPlg.type = 'pledgeReq';
//          aPlg.dplBody = utils.parse(aPlg.dplBody);
//          // console.log('aPlg.dplPledgee', aPlg.dplPledgee, `#${utils.hash6c(crypto.keccak256(aPlg.dplPledgee))}`);
//          aPlg.dplPledgeeColor = `#${utils.hash6c(crypto.keccak256(aPlg.dplPledgee))}`;
//          finalRows.push(aPlg);
//        }

    result_number++;
  }

  return final_result;
}

bool ProposalHandler::deleteDraftProposal(const CDocHashT& proposal_hash)
{
  DbModel::dDelete(
    stbl_machine_draft_proposals,
    {{"pd_hash", proposal_hash}});
  return true;
}
