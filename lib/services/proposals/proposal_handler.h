#ifndef PROPOSALHANDLER_H
#define PROPOSALHANDLER_H


class ProposalHandler
{
public:
  ProposalHandler();

  static const QString stbl_proposals;
  static const QStringList stbl_proposals_fields;

  static const QString stbl_machine_draft_proposals;
  static const QStringList stbl_machine_draft_proposals_fields;


  static std::tuple<CMPAIValueT, CMPAIValueT> calcProposalApplyCost(
    const uint32_t& help_hours,
    const uint8_t& help_level,
    const QString& cDate);

  static bool removeProposal(const QString& proposal_hash);

  static QVDRecordsT searchInProposals(
    const ClausesT& clauses,
    const QStringList& fields = stbl_proposals_fields,
    const OrderT order = {},
    const uint64_t limit = 0);

  static bool transformApprovedProposalToDNAShares(
    const QVDicT& polling,
    const CDateT& approveDate,
    CDocHashT dnProjectHash = "");

  static bool concludeProposal(
    const QVDicT& polling,
    const CDateT& approveDate);

  static QString renderProposalDocumentToHTML(const QJsonObject& proposal);

  static QJsonObject getDraftTpl();

  static std::tuple<bool, QString> prepareDraftProposal(
    const CAddressT& pd_contributor_account,
    const QString& pd_title,
    const QString& pd_comment,
    const QString& pd_tags,
    const uint64_t pd_help_hours,
    const uint64_t pd_help_level,
    double pd_voting_timeframe,
    const CDateT& pd_creation_date = CUtils::getNow(),
    const QString& pd_polling_profile = "Basic",
    QString pd_project_hash = "",
    const QString& mp_code = CMachine::getSelectedMProfile(),
    const QString& pd_type = CConsts::DOC_TYPES::DNAProposal,
    const QString& pd_class = CConsts::PROPOSAL_CLASESS::General,
    const QString& pd_version = "0.0.8");

  static QHash<uint32_t, QVDicT> loadMyDraftProposals(
    const QString& mp_code = CMachine::getSelectedMProfile());

  static bool deleteDraftProposal(const CDocHashT& proposal_hash);

  static QVDRecordsT searchInDraftProposals(
    const ClausesT& clauses,
    const QStringList& fields = stbl_machine_draft_proposals_fields,
    const OrderT order = {},
    const uint64_t limit = 0);


};

#endif // PROPOSALHANDLER_H
