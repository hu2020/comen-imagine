#ifndef SOCIETYRULES_H
#define SOCIETYRULES_H

namespace POLLING_TYPES
{
  const QString RFRlRsCoins = "RFRlRsCoins";
  const QString RFRfBasePrice = "RFRfBasePrice";                // char base price
  const QString RFRfTxBPrice = "RFRfTxBPrice";
  const QString RFRfBlockFixCost = "RFRfBlockFixCost";
  const QString RFRfPollingPrice = "RFRfPollingPrice";
  const QString RFRfPLedgePrice = "RFRfPLedgePrice";
  const QString RFRfClPLedgePrice = "RFRfClPLedgePrice";
  const QString RFRfDNAPropPrice = "RFRfDNAPropPrice";
  const QString RFRfBallotPrice = "RFRfBallotPrice";
  const QString RFRfINameRegPrice = "RFRfINameRegPrice";
  const QString RFRfINameBndPGPPrice= "RFRfINameBndPGPPrice";
  const QString RFRfINameMsgPrice = "RFRfINameMsgPrice";
  const QString RFRfFPostPrice = "RFRfFPostPrice";
  const QString RFRfMinS2Wk = "RFRfMinS2Wk";
  const QString RFRfMinS2DA = "RFRfMinS2DA";
  const QString RFRfMinS2V = "RFRfMinS2V";
  const QString RFRfMinFSign = "RFRfMinFSign";
  const QString RFRfMinFVote = "RFRfMinFVote";
};


class AdministrativePollingDocument;

class SocietyRules
{
public:
  SocietyRules();
  static const QString stbl_administrative_refines_history;
  static const QStringList stbl_administrative_refines_history_fields;

  static const QString stbl_administrative_pollings;
  static const QStringList stbl_administrative_pollings_fields;

  static const QStringList POLLING_TYPES;

  static QHash<QString, double> getAdmDefaultValues();
  static void initAdministrativeConfigurationsHistory();

  static QVariant getAdmValue(
    const QString& pollingKey,
    QString cDate = CUtils::getNow());

  static CMPAIValueT getSingleIntegerValue(
    const QString& pollingKey,
    const CDateT& cDate = CUtils::getNow());

  static CMPAIValueT getBasePricePerChar(const CDateT& cDate = CUtils::getNow());

  static CMPAIValueT getBasicTxDPCost(
    const DocLenT& dLen,
    const CDateT& cDate = CUtils::getNow());

  static CMPAIValueT getPollingDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getPledgeDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getClosePledgeDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getCloseDNAProposalDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getBallotDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getINameRegDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getINameBindDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getINameMsgDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getCPostDPCost(const CDateT& cDate = CUtils::getNow());
  static CMPAIValueT getBlockFixCost(const CDateT& cDate = CUtils::getNow());
  static DNASharePercentT getMinShareToAllowedWiki(const CDateT& cDate = CUtils::getNow());
  static DNASharePercentT getMinShareToAllowedDemos(const CDateT& cDate = CUtils::getNow());

  static QVDicT prepareDocExpenseDict(
    const CDateT& cDate = CUtils::getNow(),
    const DocLenT& dLen = 0);

  static CMPAIValueT getDocExpense(
    const QString& dType,
    const DocLenT& dLen,
    const QString& dClass = "",
    const CDateT& cDate = CUtils::getNow());

  static CMPAIValueT getTransactionMinimumFee(const CDateT& cDate = CUtils::getNow());

  static uint8_t getPoWDifficulty(const CDateT& cDate = CUtils::getNow());

  static BlockLenT getMaxBlockSize(const QString& block_type);

  static double getSingleFloatValue(
    const QString& pollingKey,
    const CDateT& cDate = CUtils::getNow());

  static double getMinShareToAllowedIssueFVote(const CDateT& cDate = CUtils::getNow());
  static double getMinShareToAllowedVoting(const CDateT& cDate = CUtils::getNow());
  static double getMinShareToAllowedSignCoinbase(const CDateT& cDate = CUtils::getNow());

  static bool logRefineDetail(
    const CDocHashT& arh_hash,
    const QString& arh_subject,
    const double arh_value,
    const QString& arh_apply_date);

  static bool treatPollingWon(
    const QVDicT& polling,
    const CDateT& approveDate);

  static bool concludeAdmPolling(
    const QVDicT& polling,
    const CDateT& approveDate);

  static bool recordAnAdministrativePollingInDB(
    const Block& block,
    const AdministrativePollingDocument* polling);

  static bool removeAdmPolling(const CDocHashT& doc_hash);

  static QVDRecordsT searchInAdmPollings(
    const ClausesT& clauses,
    const QStringList& fields = stbl_administrative_pollings_fields,
    const OrderT& order = {},
    const int limit = 0);

  static QVDRecordsT searchInAdmRefineHistory(
    const ClausesT& clauses = {},
    const QStringList& fields = stbl_administrative_refines_history_fields,
    const OrderT& order = {},
    const int limit = 0);

  static QVDicT readAdministrativeCurrentValues();

  static QJsonArray loadAdmPollings(
    const CDateT& cDate = CUtils::getNow());

  static std::tuple<bool, QString> createAPollingFor(
    const QString& polling_subject,
    TimeByHoursT voting_timeframe,
    double the_value); // it can be fee or shares

  static QHash<uint32_t, QVDicT> getOnchainSocietyPollings(
    const CAddressT& voter);


};

#endif // SOCIETYRULES_H
