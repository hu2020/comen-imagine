#include "stable.h"

#include "lib/block_utils.h"
#include "lib/ccrypto.h"
#include "lib/block/document_types/document.h"
#include "lib/services/polling/ballot_handler.h"
#include "lib/services/polling/polling_handler.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/document_types/administrative_polling_document.h"

#include "society_rules.h"

const QString SocietyRules::stbl_administrative_refines_history = "c_administrative_refines_history"; // tblAdmRefinesHist
const QStringList SocietyRules::stbl_administrative_refines_history_fields = {"arh_id", "arh_hash", "arh_subject", "arh_value", "arh_apply_date"};

const QString SocietyRules::stbl_administrative_pollings = "c_administrative_pollings"; // table
const QStringList SocietyRules::stbl_administrative_pollings_fields = {"apr_id", "apr_hash", "apr_creator", "apr_subject", "apr_values", "apr_comment", "apr_creation_date", "apr_conclude_date", "apr_approved", "apr_conclude_info"};


SocietyRules::SocietyRules()
{

}


QHash<QString, double> SocietyRules::getAdmDefaultValues()
{
  QHash<QString, double> admCostParams =
  {
    {POLLING_TYPES::RFRfPLedgePrice, 37.0},
    {POLLING_TYPES::RFRfPollingPrice, 37.0},
    {POLLING_TYPES::RFRfTxBPrice, 11.0},
    {POLLING_TYPES::RFRfClPLedgePrice, 37.0},
    {POLLING_TYPES::RFRfDNAPropPrice, 37.0},
    {POLLING_TYPES::RFRfBallotPrice, 57.0},
    {POLLING_TYPES::RFRfINameRegPrice, 71.0},
    {POLLING_TYPES::RFRfINameBndPGPPrice, 41.0},
    {POLLING_TYPES::RFRfINameMsgPrice, 11.0},
    {POLLING_TYPES::RFRfFPostPrice, 17.0},
    {POLLING_TYPES::RFRfBasePrice, 683.0},
    {POLLING_TYPES::RFRfBlockFixCost, (CConsts::TIME_GAIN == 1) ? (100 * 2 * 1000000) : (1 * 1000)}, //minimum cost(100 trx * 2 PAI per trx * 1000000 micropPAI) for atleast 100 simple/light transaction
    {POLLING_TYPES::RFRfMinS2Wk,  0.0001},        // create/edit wiki page
    {POLLING_TYPES::RFRfMinS2DA,  0.00001},       // create/edit Demos
    {POLLING_TYPES::RFRfMinS2V,   0.0000000001},  // participate in pollings and vote
    {POLLING_TYPES::RFRfMinFSign, 0.0000000001},
    {POLLING_TYPES::RFRfMinFVote, 0.000000002}
    // ['RFRlRsCoins', {}],
  };

  return admCostParams;
}

void SocietyRules::initAdministrativeConfigurationsHistory()
{
  QHash<QString, double> admCostParams = getAdmDefaultValues();

  for (QString aKey : admCostParams.keys())
  {
    QString arh_hash = CCrypto::keccak256(CMachine::getLaunchDate() + "-" + aKey);
    DbModel::insert(
      SocietyRules::stbl_administrative_refines_history,
      QVDicT
      {
        {"arh_hash", arh_hash},
        {"arh_subject", QVariant(aKey)},
        {"arh_value", admCostParams[aKey]},
        {"arh_apply_date", CMachine::getLaunchDate()}
      }
    );
  }
}

/**
* func retrieves the last date before given cDate in which the value is refined
* @param {*} args
*/
QVariant SocietyRules::getAdmValue(
  const QString& pollingKey,
  QString cDate)
{
  QueryRes res = DbModel::select(
    stbl_administrative_refines_history,
    {"arh_value"},
    {{"arh_subject", pollingKey},
    {"arh_apply_date", cDate, "<="}},
    {{"arh_apply_date", "DESC"}},
    1);

  if (res.records.size() == 0)
    CUtils::exiter("invalid arh_apply_date for (" + pollingKey + ") on date(" + cDate + ")", 1125);

  return res.records[0].value("arh_value");
}


CMPAIValueT SocietyRules::getSingleIntegerValue(
  const QString& pollingKey,
  const CDateT& cDate)
{
  // fetch from DB the price for calculation Date
  QVariant value = getAdmValue(pollingKey, cDate);
  CLog::log("Retrieveing RFRf for pollingKey(" + pollingKey + ") on date(" + cDate + ") -> value(" + value.toString() + ")", "app", "trace");
  return value.toDouble();
}

CMPAIValueT SocietyRules::getBasicTxDPCost(
  const DocLenT& dLen,
  const CDateT& cDate)
{
  CMPAIValueT TxBasePrice = getSingleIntegerValue(POLLING_TYPES::RFRfTxBPrice, cDate);

  /**
   * TODO: maybe can be modified in next version of transaction to be more fair
   * specially after implementing the indented bach32 unlockers(recursively unlimited unlockers which have another bech32 as an unlocker)
   */
  auto[x, y, gain, revGain] = CUtils::calcLog(dLen, CConsts::MAX_DOC_LENGTH_BY_CHAR, 1);//(dLen * 1) / (iConsts.MAX_DOC_LENGTH_BY_CHAR * 1);
  Q_UNUSED(x);
  Q_UNUSED(y);
  Q_UNUSED(gain);
  CMPAIValueT cost = CUtils::iFloorFloat(TxBasePrice * pow(100 * revGain, 20));
  return cost;
}

CMPAIValueT SocietyRules::getBasePricePerChar(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfBasePrice, cDate);
}

CMPAIValueT SocietyRules::getPollingDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfPollingPrice, cDate);
}

CMPAIValueT SocietyRules::getPledgeDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfPLedgePrice, cDate);
}

CMPAIValueT SocietyRules::getClosePledgeDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfClPLedgePrice, cDate);
}

CMPAIValueT SocietyRules::getCloseDNAProposalDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfDNAPropPrice, cDate);
}

CMPAIValueT SocietyRules::getBallotDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfBallotPrice, cDate);
}

CMPAIValueT SocietyRules::getINameRegDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfINameRegPrice, cDate);
}

CMPAIValueT SocietyRules::getINameBindDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfINameBndPGPPrice, cDate);
}

CMPAIValueT SocietyRules::getINameMsgDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfINameMsgPrice, cDate);
}

CMPAIValueT SocietyRules::getCPostDPCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfFPostPrice, cDate);
}

CMPAIValueT SocietyRules::getBlockFixCost(const CDateT& cDate)
{
  return getSingleIntegerValue(POLLING_TYPES::RFRfBlockFixCost, cDate);
}

// TODO: optimize it to catch price-set and fetch it once in every 12 hours
QVDicT SocietyRules::prepareDocExpenseDict(
  const CDateT& cDate,
  const DocLenT& dLen)
{
  QVDicT servicePrices {
    {CConsts::DOC_TYPES::BasicTx, QVariant::fromValue(getBasicTxDPCost(dLen, cDate))},
    {CConsts::DOC_TYPES::AdmPolling, QVariant::fromValue(getPollingDPCost(cDate))},
    {CConsts::DOC_TYPES::Polling, QVariant::fromValue(getPollingDPCost(cDate))},
    {CConsts::DOC_TYPES::Pledge, QVariant::fromValue(getPledgeDPCost(cDate))},
    {CConsts::DOC_TYPES::ClosePledge, QVariant::fromValue(getClosePledgeDPCost(cDate))},
    {CConsts::DOC_TYPES::DNAProposal, QVariant::fromValue(getCloseDNAProposalDPCost(cDate))},
    {CConsts::DOC_TYPES::Ballot, QVariant::fromValue(getBallotDPCost(cDate))},
    {CConsts::DOC_TYPES::INameReg, QVariant::fromValue(getINameRegDPCost(cDate))},
    {CConsts::DOC_TYPES::INameBind, QVariant::fromValue(getINameBindDPCost(cDate))},
    {CConsts::DOC_TYPES::INameMsgTo, QVariant::fromValue(getINameMsgDPCost(cDate))},
    {CConsts::DOC_TYPES::FPost, QVariant::fromValue(getCPostDPCost(cDate))}};

  return servicePrices;
}

CMPAIValueT SocietyRules::getDocExpense(
  const QString& doc_type,
  const DocLenT& doc_len,
  const QString& doc_lass,
  const CDateT& cDate)
{
  Q_UNUSED(doc_lass);
  if (doc_len > CConsts::MAX_DOC_LENGTH_BY_CHAR)
    return 0;

  QVDicT servicePrices = prepareDocExpenseDict(cDate, doc_len);

  if (servicePrices.keys().contains(doc_type))
    return servicePrices[doc_type].toUInt();

  return 0;

  //TODO: implement plugin price
  // if type of documents is not defined, so accept it as a base feePerByte
//  let pluginPrice = listener.doCallSync('SASH_calc_service_price', args);
//  if (_.has(pluginPrice, 'err')&& pluginPrice.err != false) {
//      utils.exiter("wrong plugin price calc for ${utils.stringify(args)}", 434);
//  }
//  if (!_.has(pluginPrice, 'fee')) {
//    utils.exiter("missed price fee for doc_type(${doc_type})", 34)
//  }
//  return pluginPrice.fee;
}


/**
 * returns minimum transaction fee by microPAI
 */
CMPAIValueT SocietyRules::getTransactionMinimumFee(const CDateT& cDate)
{
  return CUtils::truncMPAI(static_cast<double>(
    CConsts::TRANSACTION_MINIMUM_LENGTH *
    getBasePricePerChar(cDate) *
    getDocExpense(
      CConsts::DOC_TYPES::BasicTx,
      CConsts::TRANSACTION_MINIMUM_LENGTH,
      cDate)
  ));
}


uint8_t SocietyRules::getPoWDifficulty(const CDateT& cDate)
{
  if (cDate < "2020-10-01 00:00:00") {
    return 4;
  } else if (("2020-10-01 00:00:00" <= cDate) &&  (cDate < "2021-00-01 00:00:00")) {
    return 6;
  } else if (("2021-00-01 00:00:00" <= cDate) && (cDate < "2021-06-01 00:00:00")) {
    return 10;
  } else {
    return 18;
  }
}

BlockLenT SocietyRules::getMaxBlockSize(const QString& block_type)
{
  // TODO: implement it to retrieve max number from db, by voting process

  BlockLenT DEFAULT_MAX = 10000000 * 10; //MAX_BLOCK_LENGTH_BY_CHAR
  QHash<QString, BlockLenT> max_block_size = {
    {CConsts::BLOCK_TYPES::Normal, DEFAULT_MAX},
    {CConsts::BLOCK_TYPES::Coinbase, DEFAULT_MAX},
    {CConsts::BLOCK_TYPES::RpBlock, DEFAULT_MAX},
    {CConsts::BLOCK_TYPES::FSign, DEFAULT_MAX},
    {CConsts::BLOCK_TYPES::SusBlock, DEFAULT_MAX},
    {CConsts::BLOCK_TYPES::FVote, DEFAULT_MAX},
    {CConsts::BLOCK_TYPES::POW, 7100}};

  if (max_block_size.keys().contains(block_type))
    return max_block_size[block_type];

  CLog::log("Invalid block type! for length block_type(" + block_type + ")", "sec", "error");
  return 0;

}


double SocietyRules::getSingleFloatValue(
  const QString& pollingKey,
  const CDateT& cDate)
{
  if (!CUtils::isValidDateForamt(cDate))
    CUtils::exiter("invalid cDate for get Single Float Value for pollingKey(" + pollingKey + ") cDate:(" + cDate + ") ", 812);

  // fetch from DB the price for calculation Date
  QVariant value_ = getAdmValue(
    pollingKey,
    cDate);

  double value = CUtils::iFloorFloat(value_.toDouble());
  CLog::log("RFRf(float) for pollingKey(" + pollingKey + ") cDate:(" + cDate + ") => value(" + QString::number(value) + ")", "app", "trace");
  return value;
}

//  -  -  -  shares parameters settings
DNASharePercentT SocietyRules::getMinShareToAllowedIssueFVote(const CDateT& cDate)
{
  return getSingleFloatValue(POLLING_TYPES::RFRfMinFVote, cDate);
}

DNASharePercentT SocietyRules::getMinShareToAllowedVoting(const CDateT& cDate)
{
  return getSingleFloatValue(POLLING_TYPES::RFRfMinS2V, cDate);
}

DNASharePercentT SocietyRules::getMinShareToAllowedSignCoinbase(const CDateT& cDate)
{
  return getSingleFloatValue(POLLING_TYPES::RFRfMinFSign, cDate);
}

DNASharePercentT SocietyRules::getMinShareToAllowedWiki(const CDateT& cDate)
{
  return getSingleFloatValue(POLLING_TYPES::RFRfMinS2Wk, cDate);
}

DNASharePercentT SocietyRules::getMinShareToAllowedDemos(const CDateT& cDate)
{
  return getSingleFloatValue(POLLING_TYPES::RFRfMinS2DA, cDate);
}

bool SocietyRules::logRefineDetail(
  const CDocHashT& arh_hash,
  const QString& arh_subject,
  const double arh_value,
  const QString& arh_apply_date)
{
  /**
  * FIXME: since "arh_apply_date" is calculted based on container block.creation Date
  * it is possible for a same pSubject having 2 or more different polling which the arh_apply_date
  * will be sam(e.g they reside in same block or 2 different block by same creation date which is heigly possible
  * an adversor create them)
  * and at the end we have different result with same date to apply!
  * it must be fixed ASAP, meanwhile the comunity can simply futile the duplicated polling by negative votes
  */

  QueryRes exist = DbModel::select(
    stbl_administrative_refines_history,
    {"arh_hash"},
    {{"arh_hash", arh_hash}});
  if (exist.records.size() > 0)
  {
    CLog::log("duplicate refine hist (" + CUtils::hash8c(arh_hash) + ")", "app", "error");
    return false;
  }
  DbModel::insert(
    stbl_administrative_refines_history,
    {{"arh_hash", arh_hash},
    {"arh_subject", arh_subject},
    {"arh_value", arh_value},
    {"arh_apply_date", arh_apply_date}});

  return true;
}

bool SocietyRules::treatPollingWon(
  const QVDicT& polling,
  const CDateT& approveDate)
{
  CDocHashT admPollingHash = polling.value("pll_ref").toString();
  QueryRes admPollings = DbModel::select(
    stbl_administrative_pollings,
    stbl_administrative_pollings_fields,
    {{"apr_hash", admPollingHash}});
  if (admPollings.records.size() != 1)
  {
    CLog::log("Invalid winner adm olling! admPolling(" + CUtils::hash8c(admPollingHash) + ")", "app", "error");
    return false;
  }
  QVDicT admPolling = admPollings.records[0];
  QJsonObject the_values = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(admPolling.value("apr_values").toString()).content);
  CLog::log("Treat Polling Won adm Polling: " + CUtils::dumpIt(admPolling), "app", "trace");
  CDateT arh_apply_date = CUtils::getACycleRange(
    approveDate,
    0,
    2).from;

  if (QStringList {"RFRfMinS2V", "RFRfMinFSign", "RFRfMinFVote"}.contains(admPolling.value("apr_subject").toString()))
  {
    logRefineDetail(
      admPollingHash,
      admPolling.value("apr_subject").toString(),
      the_values.value("pShare").toDouble(), //.share,
      arh_apply_date);

  } else if (QStringList {
    "RFRfBasePrice",
    "RFRfTxBPrice",
    "RFRfBlockFixCost",
    "RFRfPollingPrice",
    "RFRfPLedgePrice",
    "RFRfClPLedgePrice",
    "RFRfDNAPropPrice",
    "RFRfBallotPrice",
    "RFRfINameRegPrice",
    "RFRfINameBndPGPPrice",
    "RFRfINameMsgPrice",
    "RFRfFPostPrice"}.contains(admPolling.value("apr_subject").toString()))
  {
    logRefineDetail(
      admPollingHash,
      admPolling.value("apr_subject").toString(),
      the_values.value("pFee").toDouble(),  //.pFee,
      arh_apply_date);

  } else {
    CLog::log("Unknown apr_subject in 'treat Polling Won' " + admPolling.value("apr_subject").toString(), "sec", "error");
    return false;
  }

  // update proposal
  DbModel::update(
    stbl_administrative_pollings,
    {{"apr_conclude_date", approveDate},
    {"apr_approved", CConsts::YES}},
    {{"apr_hash", admPollingHash}});

  return true;
}


bool SocietyRules::concludeAdmPolling(
  const QVDicT& polling,
  const CDateT& approveDate)
{
  CDocHashT admPollingHash = polling.value("pll_ref").toString();

  // update proposal
  DbModel::update(
    stbl_administrative_pollings,
    {{"apr_conclude_date", approveDate},
    {"apr_approved", CConsts::NO}},
    {{"apr_hash", admPollingHash}});

  return true;
}


bool SocietyRules::recordAnAdministrativePollingInDB(
  const Block& block,
  const AdministrativePollingDocument* polling)
{
  CLog::log("record A Society administrative polling document: " + CUtils::hash8c(polling->getDocHash()), "app", "trace");


  QueryRes dbl = DbModel::select(
    stbl_administrative_pollings,
    {"apr_hash"},
    {{"apr_hash", polling->m_doc_hash}});
  if (dbl.records.size() > 0)
  {
    CLog::log("Try to double insert existed adm polling(" + CUtils::hash8c(polling->m_doc_hash) + ")", "sec", "error");
    return true;  // by the way, it is ok and the block must be recorded in DAG
  }

  QVDicT values {
    {"apr_hash", polling->getDocHash()},
    {"apr_creator", polling->m_polling_creator},
    {"apr_subject", polling->m_polling_subject},
    {"apr_values", BlockUtils::wrapSafeContentForDB(CUtils::serializeJson(polling->m_polling_values)).content},
    {"apr_comment", polling->m_doc_comment},
    {"apr_creation_date", block.m_block_creation_date},
    {"apr_conclude_date", ""},
    {"apr_approved", CConsts::NO},
    {"apr_conclude_info", BlockUtils::wrapSafeContentForDB(CUtils::getANullStringifyedJsonObj()).content}
  };
  CLog::log("New admPolling is creates: " + CUtils::dumpIt(values), "app", "trace");
  bool res = DbModel::insert(
    stbl_administrative_pollings,
    values);
  return res;
}

bool SocietyRules::removeAdmPolling(const CDocHashT& doc_hash)
{
  DbModel::dDelete(
    stbl_administrative_pollings,
    {{"apr_hash", doc_hash}});

  return true;
}

QVDRecordsT SocietyRules::searchInAdmPollings(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int limit)
{
  QueryRes posts = DbModel::select(
    stbl_administrative_pollings,
    fields,
    clauses,
    order,
    limit);

  return posts.records;
}

QVDRecordsT SocietyRules::searchInAdmRefineHistory(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int limit)
{
  QueryRes posts = DbModel::select(
    stbl_administrative_refines_history,
    fields,
    clauses,
    order,
    limit);

  return posts.records;
}

QVDicT SocietyRules::readAdministrativeCurrentValues()
{
  /**
  * instead of fetching from a single table, tha values are retrived from proper functions in ConfParamsHandler
  */
  CDateT cDate = CUtils::getNow();
  CDateT cycleStartDate = CUtils::getACycleRange().from;

  QVDicT res {
    {"cycleStartDate", cycleStartDate},
    {"transactionMinimumFee", QVariant::fromValue(getTransactionMinimumFee(cDate))},
    {"docExpenseDict", prepareDocExpenseDict(cDate, CConsts::TRANSACTION_MINIMUM_LENGTH)},
    {"basePricePerChar", QVariant::fromValue(getBasePricePerChar(cDate))},
    {"blockFixCost", QVariant::fromValue(getBlockFixCost(cDate))},
    {"minShareToAllowedIssueFVote", getMinShareToAllowedIssueFVote(cDate)},
    {"minShareToAllowedVoting", getMinShareToAllowedVoting(cDate)},
    {"minShareToAllowedSignCoinbase", getMinShareToAllowedSignCoinbase(cDate)}
  };

  return res;
}

QJsonArray SocietyRules::loadAdmPollings(
  const CDateT& cDate)
{
  QJsonArray admPollings = {
    QJsonObject {
      {"key", POLLING_TYPES::RFRfBasePrice},
      {"label", "Request for Refine charachter base pice of Data & Process costs(DPCost), currently is " + CUtils::microPAIToPAI6(getBasePricePerChar(cDate)) + " PAI per Char"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getBasePricePerChar(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}
      }}
    },

    QJsonObject {
      {"key", POLLING_TYPES::RFRfTxBPrice},
      {"label", "Request for Refine Transaction DPCost, currently is " + CUtils::microPAIToPAI6(CUtils::CFloor(getBasicTxDPCost(CConsts::TRANSACTION_MINIMUM_LENGTH, cDate))) + " micro PAI per Char"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getBasicTxDPCost(CConsts::TRANSACTION_MINIMUM_LENGTH, cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },

    QJsonObject {
      {"key", POLLING_TYPES::RFRfBlockFixCost},
      {"label", "Request for Refine Block Fix Cost, currently is " + CUtils::microPAIToPAI6(getBlockFixCost(cDate)) + " micro PAI per Block"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getBlockFixCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfPollingPrice},
      {"label", "Request for Refine DPCost of Polling Document, currently is " + CUtils::microPAIToPAI6(getPollingDPCost(cDate)) + " PAIs per char"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getPollingDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfPLedgePrice},
      {"label", "Request for Refine DPCost of Pledge Document, currently is " + CUtils::microPAIToPAI6(getPledgeDPCost(cDate)) + " PAIs per char"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getPledgeDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfClPLedgePrice},
      {"label", "Request for Refine DPCost of Close a Pledged Account, currently is " + CUtils::microPAIToPAI6(getClosePledgeDPCost(cDate)) + " PAIs per char"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getClosePledgeDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfDNAPropPrice},
      {"label", "Request for Refine DPCost of offer a DNAProposal, currently is " + CUtils::microPAIToPAI6(getCloseDNAProposalDPCost(cDate)) + " PAIs per char"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getCloseDNAProposalDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfBallotPrice},
      {"label", "Request for Refine DPCost of Ballot, currently is " + CUtils::microPAIToPAI6(getBallotDPCost(cDate)) + " PAIs per char"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getBallotDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfINameRegPrice},
      {"label", "Request for Refine DPCost of register an iName, currently is " + CUtils::microPAIToPAI6(getINameRegDPCost(cDate)) + " PAIs per unit "},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getINameRegDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfINameBndPGPPrice},
      {"label", "Request for Refine DPCost of Binding an iPGP key to an iName, currently is " + CUtils::microPAIToPAI6(getINameBindDPCost(cDate)) + " PAIs per a pair-key "},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getINameBindDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfINameMsgPrice},
      {"label", "Request for Refine DPCost of a message via DAG, currently is " + CUtils::microPAIToPAI6(getINameMsgDPCost(cDate)) + " PAIs per char. it refers to entire encrypted message and head & tail & ..."},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getINameMsgDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfFPostPrice},
      {"label", "Request for Refine DPCost of a Free Post (including text, file, media...), currently is " + CUtils::microPAIToPAI6(getCPostDPCost(cDate)) + " PAIs per char"},
      {"pValues", QJsonObject {
        {"pFee", QVariant::fromValue(getCPostDPCost(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfMinS2Wk},
      {"label", "Request for Refine Minimum Shares to be Allowed to participate in Wiki Activities, currently is " + QString::number(getMinShareToAllowedWiki(cDate)) + " Percent"},
      {"pValues", QJsonObject {
        {"pShare", QVariant::fromValue(getMinShareToAllowedWiki(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfMinS2DA},
      {"label", "Request for Refine Minimum Shares to be Allowed to participate in Demos Discussions, currently is " + QString::number(getMinShareToAllowedDemos(cDate)) + " Percent"},
      {"pValues", QJsonObject {
        {"pShare", QVariant::fromValue(getMinShareToAllowedDemos(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfMinS2V},
      {"label", "Request for Refine Minimum Shares to be Allowed to participate in ellections, currently is " + QString::number(getMinShareToAllowedVoting(cDate)) + " Percent"},
      {"pValues", QJsonObject {
        {"pShare", QVariant::fromValue(getMinShareToAllowedVoting(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfMinFSign},
      {"label", "Request for Refine Minimum Shares to be Allowed to Sign a Coinbase block, currently is " + QString::number(getMinShareToAllowedSignCoinbase(cDate)) + " Percent"},
      {"pValues", QJsonObject {
        {"pShare", QVariant::fromValue(getMinShareToAllowedSignCoinbase(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRfMinFVote},
      {"label", "Request for Refine Minimum Shares to be Allowed to Issue a Floating Vote (either a block or an entry), currently is " + QString::number(getMinShareToAllowedIssueFVote(cDate)) + " Percent"},
      {"pValues", QJsonObject {
        {"pShare", QVariant::fromValue(getMinShareToAllowedIssueFVote(cDate)).toDouble()},
        {"pTimeframe", QVariant::fromValue(CMachine::getMinPollingTimeframeByHour()).toDouble()}}
      }
    },
    QJsonObject {
      {"key", POLLING_TYPES::RFRlRsCoins},
      {"label", "Request for release a Reserved Block"},
    },
  };

  return admPollings;
}

std::tuple<bool, QString> SocietyRules::createAPollingFor(
  const QString& polling_subject,
  TimeByHoursT voting_timeframe,
  double the_value) // it can be fee or shares
{
  auto[total_shares_, share_amount_per_holder, holdersOrderByShares_] = DNAHandler::getSharesInfo();
  uint64_t voters_count = share_amount_per_holder.keys().size();
  voting_timeframe = PollingHandler::normalizeVotingTimeframe(voting_timeframe);
  auto[status, msg] = PollingHandler::makeReqForAdmPolling(
    polling_subject,
    voting_timeframe,
    the_value,
    voters_count);
  return {status, msg};
}

QHash<uint32_t, QVDicT> SocietyRules::getOnchainSocietyPollings(
  const CAddressT& voter)
{
  // retrieve machine votes
  QVDRecordsT votes = BallotHandler::searchInLocalBallots();
  QV2DicT local_votes_dict {};
  for (QVDicT a_vote: votes)
    local_votes_dict[a_vote.value("lbt_pll_hash").toString()] = a_vote;

  QString complete_query = R"(
    SELECT ppr.ppr_name, ppr.ppr_perform_type, ppr.ppr_votes_counting_method,

    apr.apr_hash, apr.apr_creator, apr.apr_subject, apr.apr_values, apr.apr_comment, apr.apr_creation_date,
    apr.apr_conclude_date, apr.apr_approved, apr.apr_conclude_info,

    pll.pll_hash, pll.pll_ref, pll.pll_start_date, pll.pll_end_date, pll.pll_timeframe, pll.pll_status, pll.pll_ct_done,
    pll.pll_y_count, pll.pll_n_count, pll.pll_a_count,
    pll.pll_y_shares, pll.pll_n_shares, pll.pll_a_shares,
    pll.pll_y_gain, pll.pll_n_gain, pll.pll_a_gain,
    pll.pll_y_value, pll.pll_n_value, pll.pll_a_value

    FROM c_pollings pll
    JOIN c_polling_profiles ppr ON ppr.ppr_name=pll.pll_class
    JOIN c_administrative_pollings apr ON apr.apr_hash = pll.pll_ref
  )";

  if (CConsts::DATABASAE_AGENT == "psql")
  {
    complete_query += "WHERE pll.pll_ref_type='AdmPolling' ORDER BY pll.pll_start_date ";
  }
  else if (CConsts::DATABASAE_AGENT == "sqlite")
  {
    complete_query += "WHERE pll.pll_ref_type=\"AdmPolling\" ORDER BY pll.pll_start_date ";
  }

  QueryRes res = DbModel::customQuery(
    "db_comen_general",
    complete_query,
    {"ppr_name", "ppr_perform_type", "ppr_votes_counting_method",
      "apr_hash", "apr_creator", "apr_subject", "apr_values", "apr_comment",
      "apr_conclude_date", "apr_approved", "apr_conclude_info",
      "pll_hash", "pll_ref", "pll_start_date", "pll_end_date", "pll_timeframe", "pll_status", "pll_ct_done",
      "pll_y_count", "pll_n_count", "pll_a_count",
      "pll_y_shares", "pll_n_shares", "pll_a_shares",
      "pll_y_gain", "pll_n_gain", "pll_a_gain",
      "pll_y_value", "pll_n_value", "pll_a_value"},
    0,
    {},
    false,
    false);

  QHash<uint32_t, QVDicT> final_result {};
  uint32_t result_number = 0;
  for (QVDicT a_society_polling: res.records)
  {
    uint32_t row_inx = result_number * 10; // 10 rows for each proposal are needed

    // calc potentiasl voter gains
    if (voter != "")
    {
      uint64_t diff = CUtils::timeDiff(a_society_polling.value("pll_start_date").toString()).asMinutes;
      auto[yes_gain, no_abstain_gain, latenancy_] = PollingHandler::calculateVoteGain(
        diff,
        diff,
        a_society_polling.value("pll_timeframe").toDouble() * 60.0);
      Q_UNUSED(latenancy_);

//      let vGain = pollHandler.calculateVoteGain(diff, diff, a_society_polling.pll_timeframe * 60);
      a_society_polling["your_yes_gain"] = CUtils::customFloorFloat(yes_gain * 100, 2);
      a_society_polling["your_abstain_gain"] = CUtils::customFloorFloat(no_abstain_gain * 100, 2);
      a_society_polling["your_no_gain"] = CUtils::customFloorFloat(no_abstain_gain * 100, 2);

    } else {
      a_society_polling["your_yes_gain"] = 0.0;
      a_society_polling["your_abstain_gain"] = 0.0;
      a_society_polling["your_no_gain"] = 0.0;
    }

    CDateT conclude_date = a_society_polling.value("pr_conclude_date").toString();
    if (conclude_date == "")
    {
      CDateT polling_end_date = a_society_polling.value("pll_end_date").toString();
      CDateT approve_date_ = CUtils::minutesAfter(CMachine::getCycleByMinutes() * 2, polling_end_date);
      conclude_date = CUtils::getCoinbaseRange(approve_date_).from;
    }

    QString win_complementary_text = "";
    QString win_complementary_tip = "";
    if (a_society_polling.value("pll_ct_done").toString() == CConsts::YES)
    {
      QVDRecordsT dna_records = DNAHandler::searchInDNA(
        {{"dn_doc_hash", a_society_polling.value("pll_ref").toString()}});
      if (dna_records.size() > 0)
      {
        win_complementary_text = " Shares created on " + dna_records[0].value("dn_creation_date").toString();
        win_complementary_tip = "First income on " + CUtils::minutesAfter(
          CConsts::SHARE_MATURITY_CYCLE * CMachine::getCycleByMinutes(),
          dna_records[0].value("dn_creation_date").toString());
      }
    } else{
      win_complementary_tip = "First income (if win) on " + CUtils::minutesAfter(
        CConsts::SHARE_MATURITY_CYCLE * CMachine::getCycleByMinutes(), conclude_date);
    }

    final_result[row_inx] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"apr_comment", a_society_polling.value("apr_comment")},
      {"polling_number", result_number + 1},
      {"pr_contributor_account", a_society_polling.value("pr_contributor_account")},
      {"pll_status", CConsts::STATUS_TO_LABEL[a_society_polling.value("pll_status").toString()]},
      {"ppr_name", a_society_polling.value("ppr_name")},
    };


    final_result[row_inx + 1] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"apr_creator", a_society_polling.value("apr_creator")},
      {"pll_ct_done", CConsts::STATUS_TO_LABEL[a_society_polling.value("pll_ct_done").toString()]},
      {"the_conclude_date", conclude_date},
      {"ppr_perform_type", a_society_polling.value("ppr_perform_type")},
    };

    final_result[row_inx + 2] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"pll_hash", a_society_polling.value("pll_hash")},
      {"ppr_votes_counting_method", a_society_polling.value("ppr_votes_counting_method")},
    };

    final_result[row_inx + 3] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"pr_descriptions", a_society_polling.value("pr_descriptions")},
      {"", a_society_polling.value("")},
    };

    CDateT end_y_date;
    if (CConsts::TIME_GAIN == 1)
    {
      end_y_date = CUtils::minutesAfter(a_society_polling.value("pll_timeframe").toDouble() * 60, a_society_polling.value("pll_start_date").toString());

    } else{
      // test ambient
      TimeByHoursT yes_timeframe_by_minutes = static_cast<uint64_t>(a_society_polling.value("pll_timeframe").toDouble() * 60.0);
      CLog::log("yes_timeframe_by_minutes____pll_timeframe" + QString::number(a_society_polling.value("pll_timeframe").toDouble()));
      CLog::log("yes_timeframe_by_minutes____" + QString::number(yes_timeframe_by_minutes));
      end_y_date = CUtils::minutesAfter(yes_timeframe_by_minutes, a_society_polling.value("pll_start_date").toString());

    }


    final_result[row_inx + 4] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"pr_tags", a_society_polling.value("pr_tags")},
      {"pll_start_date", a_society_polling.value("pll_start_date")},
      {"pll_y_count", a_society_polling.value("pll_y_count")},
      {"pll_y_shares", a_society_polling.value("pll_y_shares")},
      {"pll_y_gain", a_society_polling.value("pll_y_gain")},
      {"pll_y_value", a_society_polling.value("pll_y_value")},
      {"your_yes_gain", a_society_polling.value("your_yes_gain")},
    };

    final_result[row_inx + 5] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"pr_help_hours", a_society_polling.value("pr_help_hours")},
      {"end_y", end_y_date}, //CUtils::minutesAfter(yes_timeframe * 60, a_society_polling.value("pll_start_date").toString())},
      {"pll_a_count", a_society_polling.value("pll_a_count")},
      {"pll_a_shares", a_society_polling.value("pll_a_shares")},
      {"pll_a_gain", a_society_polling.value("pll_a_gain")},
      {"pll_a_value", a_society_polling.value("pll_a_value")},
      {"your_abstain_gain", a_society_polling.value("your_abstain_gain")},
    };

    final_result[row_inx + 6] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"pr_help_level", a_society_polling.value("pr_help_level")},
      {"end_n", a_society_polling.value("pll_end_date")},
      {"pll_n_count", a_society_polling.value("pll_n_count")},
      {"pll_n_shares", a_society_polling.value("pll_n_shares")},
      {"pll_n_gain", a_society_polling.value("pll_n_gain")},
      {"pll_n_value", a_society_polling.value("pll_n_value")},
      {"your_no_gain", a_society_polling.value("your_no_gain")},
    };

    final_result[row_inx + 7] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"proposed_shares", a_society_polling.value("pr_help_level").toInt() * a_society_polling.value("pr_help_hours").toInt()},
      {"", a_society_polling.value("")},
      {"", a_society_polling.value("")},
    };

    QString final_status_color, final_status_text;
    if (a_society_polling.value("pll_y_value").toUInt() >= a_society_polling.value("pll_n_value").toUInt())
    {
      final_status_color = "00ff00";
      final_status_text = "Approved (";

    } else {
      final_status_color = "ff0000";
      final_status_text = "Missed (";

    }
    final_status_text += CUtils::sepNum(a_society_polling.value("pll_y_value").toUInt() - a_society_polling.value("pll_n_value").toUInt()) +" points)";
    final_status_text += win_complementary_text;

    final_result[row_inx + 8] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
      {"final_status_color", final_status_color},
      {"final_status_text", final_status_text},
      {"win_complementary_tip", win_complementary_tip},
    };

    final_result[row_inx + 9] = QVDicT {
      {"apr_hash", a_society_polling.value("apr_hash")},
    };

//    a_society_polling.pllEndDateAbstainOrNo = utils.minutesAfter(utils.floor(a_society_polling.pll_timeframe * 60 * 1.5), a_society_polling.pll_start_date);
//    a_society_polling.machineBallot = _.has(local_votes_dict, a_society_polling.pll_hash) ? local_votes_dict[a_society_polling.pll_hash] : null;


    result_number++;
  }

  return final_result;
}
