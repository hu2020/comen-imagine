#include "stable.h"

#include "gui/c_gui.h"
#include "lib/block_utils.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/document_types/dna_proposal_document.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"
#include "lib/block/document_types/pledge_documents/pledge_document.h"

#include "tmp_contents_handler.h"

const QString TmpContentsHandler::stbl_machine_tmp_contents = "c_machine_tmp_contents";
const QStringList TmpContentsHandler::stbl_machine_tmp_contents_fields = {"tc_id", "tc_mp_code", "tc_insert_date", "tc_content_status", "tc_content_hash", "tc_content_type", "tc_content_class", "tc_payload"};


TmpContentsHandler::TmpContentsHandler()
{

}

QVDRecordsT TmpContentsHandler::searchTmpContents(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT& order,
  const int& limit)
{
  QueryRes res = DbModel::select(
    stbl_machine_tmp_contents,
    fields,
    clauses,
    order,
    limit);

  return res.records;
}

bool TmpContentsHandler::deleteTmpContent(
  const ClausesT& clauses)
{
  QueryRes res = DbModel::dDelete(
    stbl_machine_tmp_contents,
    clauses);

  return true;
}


// js name was insertTmpDoc
bool TmpContentsHandler::insertTmpContent(
  const QString& content_type,
  const QString& content_class,
  const CDocHashT& content_hash,
  const QString& payload,
  const QString& content_status,
  const CDateT& creation_date,
  const QString& mp_code)
{
  //listener.doCallAsync('APSH_before_push_doc_to_tmp_sync', args);

  QVDRecordsT dblChk = searchTmpContents(
    {{"tc_mp_code", mp_code},
    {"tc_content_hash", content_hash}});
  if (dblChk.size() > 0)
  {
    CLog::log("tried to insert in tmp duplicated trx " + content_hash, "sec", "error");
    return false;
  }

  QVDicT values {
    {"tc_mp_code", mp_code},
    {"tc_insert_date", creation_date},
    {"tc_content_status", content_status},
    {"tc_content_hash", content_hash},
    {"tc_content_type", content_type},
    {"tc_content_class", content_class},
    {"tc_payload", payload}};
  CLog::log("Insert into table _machine_tmp_contents: " + CUtils::dumpIt(values));

  bool res = DbModel::insert(
    stbl_machine_tmp_contents,
    values);
  //listener.doCallAsync('APSH_after_push_doc_to_tmp_sync', args);
  return res;
}

std::tuple<bool, QString> TmpContentsHandler::unBundleAndSignPLR(const uint64_t tc_id)
{
  QString msg;
  QVDRecordsT bundles = searchTmpContents(
    {{"tc_mp_code", CMachine::getSelectedMProfile()},
    {"tc_id", QVariant::fromValue(tc_id).toDouble()},
    {"tc_content_status", CConsts::NEW}});
  if (bundles.size() != 1)
  {
    msg = "bundles (" + QString::number(tc_id) + " is not new or ..." + CUtils::dumpIt(bundles);
    CLog::log(msg, "app", "warning");
    return {false, msg};
  }

  QVDicT bundle = bundles[0];
  CLog::log("going to sign bundles: " + CUtils::dumpIt(bundle));

  QJsonObject loan_request_json = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(bundle.value("tc_payload").toString()).content);
  QJsonObject proposal_json = loan_request_json.value("proposal").toObject();
  QJsonObject pledge_json = loan_request_json.value("pledgerSignedPledge").toObject();

  DNAProposalDocument* proposal = new DNAProposalDocument(proposal_json);
  PledgeDocument* pledge = new PledgeDocument(pledge_json);
  pledge->m_pledgee_sign_date = CUtils::getNow();

  auto[status, sing_res_msg] = GeneralPledgeHandler::pledgeeSignsProposalLoanRequestBundle(
    proposal,
    pledge);
  if (!status)
    return {false, sing_res_msg};

  // update bundle status
  DbModel::update(
    stbl_machine_tmp_contents,
    {{"tc_content_status", CConsts::SIGNED}},
    {{"tc_id", QVariant::fromValue(tc_id)}});

  if (status)
  {
    CGUI::signalUpdateWalletCoins();
    CGUI::signalUpdateWalletAccounts();
  }

  return {true, "The Loan/Pledge request was signed"};
}

// js name was unBundleAndBroadcastPPT
std::tuple<bool, QString> TmpContentsHandler::unBundleAndPushToBlockBufferPPT(const uint64_t tc_id)
{
  CLog::log("Pushing to block buffer bundle(" + QString::number(tc_id) + ")", "app", "info");
  QString msg;

  QVDRecordsT bundles = searchTmpContents(
    {{"tc_mp_code", CMachine::getSelectedMProfile()},
    {"tc_id", QVariant::fromValue(tc_id)}});
  CLog::log("Pushing to block buffer bundle(s): " + CUtils::dumpIt(bundles), "app", "info");
  if (bundles.size() != 1)
  {
    msg = "bundles PPT(" + QString::number(tc_id) + " doesn't exist or ..." + CUtils::dumpIt(bundles);
    CLog::log(msg, "app", "warning");
    return {false, msg};
  }

  QVDicT bundle = bundles[0];
  if (bundle.value("tc_content_status").toString() != CConsts::NEW)
  {
    msg = "bundles PPT(" + QString::number(tc_id) + " bundle already sent!";
    CLog::log(msg, "app", "warning");
    return {false, msg};
  }

  QJsonObject body = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(bundle.value("tc_payload").toString()).content);
  QJsonObject proposal_json = body.value("proposal").toObject();
  QJsonObject pledge_json = body.value("pledgeeSignedPledge").toObject();
  QJsonObject proposal_payer_json = body.value("proposalPayerTrx").toObject();
  QJsonObject pledge_payer_json = body.value("pledgeDocPayerTrx").toObject();

  // push proposal to Block buffer
  Document* proposal = DocumentFactory::create(proposal_json);
  auto[proposal_push_res, proposal_push_msg] = CMachine::pushToBlockBuffer(proposal, pledge_json.value("redeemTerms").toObject().value("principal").toDouble());
  if (!proposal_push_res)
  {
    msg = "Failed in proposal push PPT(" + QString::number(tc_id) + ") " + proposal_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // push pledge to Block buffer
  Document* pledge = DocumentFactory::create(pledge_json);
  auto[pledge_push_res, pledge_push_msg] = CMachine::pushToBlockBuffer(pledge, 1);
  if (!pledge_push_res)
  {
    msg = "Failed in pledge push PPT(" + QString::number(tc_id) + ") " + pledge_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // push proposalPayerTrx to Block buffer
  Document* proposal_payer = DocumentFactory::create(proposal_payer_json);
  auto[proposal_payer_push_res, proposal_payer_push_msg] = CMachine::pushToBlockBuffer(
    proposal_payer,
    proposal_payer->getOutputs()[proposal_payer->getDPIs()[0]]->m_amount);
  if (!proposal_payer_push_res)
  {
    msg = "Failed in proposal payer trx push PPT(" + QString::number(tc_id) + ") " + proposal_payer_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // push proposalPayerTrx to Block buffer
  Document* pledge_payer = DocumentFactory::create(pledge_payer_json);
  auto[pledge_payer_push_res, pledge_payer_push_msg] = CMachine::pushToBlockBuffer(
    pledge_payer,
    pledge_payer->getOutputs()[pledge_payer->getDPIs()[0]]->m_amount);
  if (!pledge_payer_push_res)
  {
    msg = "Failed in pledge payer trx push PPT(" + QString::number(tc_id) + ") " + pledge_payer_push_msg;
    CLog::log(msg, "app", "error");
    return {false, msg};
  }

  // update bundle status
  DbModel::update(
    stbl_machine_tmp_contents,
    {{"tc_content_status", CConsts::PushedToDocBuffer}},
    {{"tc_id", QVariant::fromValue(tc_id)}});

  return {true, "Bundle pushed to block buffer"};
}
