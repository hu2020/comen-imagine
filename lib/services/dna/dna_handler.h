#ifndef DNAHANDLER_H
#define DNAHANDLER_H

class DNAProposalDocument;

struct Shareholder {
  CAddressT account = "";
  DNAShareCountT shares = 0;
};


class DNAHandler
{
public:
  DNAHandler();

  static const QString stbl_dna_shares;
  static const QStringList stbl_dna_shares_fields;

  CDocHashT m_project_hash = "";
  CAddressT m_shareholder = "";
  uint64_t m_help_hours = 0;
  uint64_t m_help_level = 0;
  DNAShareCountT m_shares = 0;
  uint64_t m_votes_yes = 0;
  uint64_t m_votes_abstain = 0;
  uint64_t m_votes_no = 0;

  static std::tuple<QString, QString> getDNAActiveDateRange(QString cDate = "");
  static std::tuple<DNAShareCountT, QHash<QString, DNAShareCountT>, QVector<Shareholder> > getSharesInfo(QString cDate = "");

  static GenRes insertAShare(QJsonObject& params);
  static GenRes insertAShare(DNAProposalDocument* doc);

  static std::tuple<DNAShareCountT, DNASharePercentT> getAnAddressShares(
    const CAddressT& address,
    CDateT cDate = "");

  static std::tuple<QString, DNAShareCountT, DNASharePercentT> getMachineShares(QString cDate = "");

  static QVDRecordsT searchInDNA(
    const ClausesT& clauses,
    const QStringList& fields = stbl_dna_shares_fields,
    const OrderT order = {},
    const uint64_t limit = 0);

  static void updateDNAVotes(
    const ClausesT& clauses,
    const QVDicT& updates);


};

#endif // DNAHANDLER_H
