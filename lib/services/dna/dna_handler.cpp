#include "stable.h"

#include "lib/block/document_types/dna_proposal_document.h"

#include "dna_handler.h"

DNAHandler::DNAHandler()
{
}

const QString DNAHandler::stbl_dna_shares = "c_dna_shares";
const QStringList DNAHandler::stbl_dna_shares_fields = {"dn_id", "dn_doc_hash", "dn_shareholder", "dn_project_hash", "dn_help_hours", "dn_help_level", "dn_shares", "dn_title", "dn_descriptions", "dn_tags", "dn_votes_y", "dn_votes_n", "dn_votes_a", "dn_creation_date"};


GenRes DNAHandler::insertAShare(DNAProposalDocument* doc)
{
  QueryRes exist = DbModel::select(
    DNAHandler::stbl_dna_shares,
    {"dn_doc_hash"},
    {{"dn_doc_hash", doc->getDocHash()}});
  if (exist.records.size() > 0)
  {
    // maybe some updates
    return {false, "share already exist!"}; // "The DNA document (${utils.hash6c(dna.hash)}) is already recorded"};
  }

//  CUtils::exitIfGreaterThanNow(doc->m_doc_creation_date);

  QVDicT values{
    {"dn_doc_hash", doc->m_doc_hash},
    {"dn_shareholder", doc->m_contributor_account},
    {"dn_project_hash", doc->m_project_hash},
    {"dn_help_hours", QVariant::fromValue(doc->m_help_hours)},
    {"dn_help_level", QVariant::fromValue(doc->m_help_level)},
    {"dn_shares", QVariant::fromValue(doc->m_shares)},
    {"dn_title", doc->m_doc_title},
    {"dn_descriptions", doc->m_doc_comment},
    {"dn_tags", doc->m_doc_tags},
    {"dn_votes_y", QVariant::fromValue(doc->m_votes_yes)},
    {"dn_votes_a", QVariant::fromValue(doc->m_votes_abstain)},
    {"dn_votes_n", QVariant::fromValue(doc->m_votes_no)},
    {"dn_creation_date", doc->m_doc_creation_date}};
  CLog::log("Inserting a DNA share: " + CUtils::dumpIt(values), "app", "trace");
  DbModel::insert(
    stbl_dna_shares,    // table
    values, // values to insert
    true
  );

  return {true, "the share was inserted"};
}

GenRes DNAHandler::insertAShare(QJsonObject& params)
{
  QueryRes exist = DbModel::select(
    DNAHandler::stbl_dna_shares,
    QStringList {"dn_doc_hash"},     // fields
    {ModelClause("dn_doc_hash", params.value("dn_doc_hash").toString())}
    );
  if (exist.records.size() > 0)
  {
    // maybe some updates
    return {false, "The DNA document (${utils.hash6c(dna.hash)}) is already recorded"};
  }

//  CUtils::exitIfGreaterThanNow(params.value("m_doc_creation_date").toString());

  QVDicT values{
    {"dn_doc_hash", params.value("m_doc_hash").toString()},
    {"dn_shareholder", params.value("m_shareholder").toString()},
    {"dn_project_hash", params.value("m_project_hash").toString()},
    {"dn_help_hours", params.value("m_help_hours").toString()},
    {"dn_help_level", params.value("m_help_level").toString()},
    {"dn_shares", params.value("m_shares").toString()},
    {"dn_title", params.value("m_doc_title").toString()},
    {"dn_descriptions", params.value("m_doc_comment").toString()},
    {"dn_tags", params.value("m_doc_tags").toString()},
    {"dn_votes_y", params.value("m_votes_yes").toString()},
    {"dn_votes_a", params.value("m_votes_abstain").toString()},
    {"dn_votes_n", params.value("m_votes_no").toString()},
    {"dn_creation_date", params.value("m_block_creation_date").toString()}
  };

  DbModel::insert(
    stbl_dna_shares,    // table
    values, // values to insert
    true
  );

  return {true, ""};
}

/**
 *
 * @param {*} _t
 * given time(DNA proposal approing time), it returns the range in which a share is valid
 * the active period starts from 7 years ago and finishes right at the end of previous cycle time
 * it means if your proposal have been approved in 2017-01-01 00:00:00, the owner can gain from 2017-01-01 12:00:00 to 2024-01-01 00:00:00
 */
std::tuple<QString, QString> DNAHandler::getDNAActiveDateRange(QString cDate)
{
  if (cDate == "")
    cDate = CUtils::getNow();

  auto the_range = CUtils::getACycleRange(
    cDate,
    CConsts::SHARE_MATURITY_CYCLE);

  if (CConsts::TIME_GAIN == 1)
  {
    the_range.from = CUtils::yearsBefore(CConsts::CONTRIBUTION_APPRECIATING_PERIOD, the_range.from);
  } else {
    the_range.from = CUtils::minutesBefore(100 * CMachine::getCycleByMinutes(), the_range.from);
  }
  return { the_range.from, the_range.to };
}

// TODO: since shares are counting for before 2 last cycles, so implementing a caching system will be much helpfull where we have millions of shareholders
std::tuple<DNAShareCountT, QHash<QString, DNAShareCountT>, QVector<Shareholder> > DNAHandler::getSharesInfo(QString cDate)
{
  if (cDate == "")
    cDate = CUtils::getNow();

  // retrieve the total shares in last 24 hours, means -36 to -24 based on greenwich time
  // (Note: it is not the machine local time)
  // for examplie if a node runs this command on 7 May at 14 (in greenwich time)
  // the result will be the final state of DNA at 11:59:59 of 6 May.
  // it means the node calculate all shares which the creation date are less than 11:59:59  of 6 May
  // -------------< 11:59 of 6 May |         --- 24 hours ---        |12:00 of 7 May     --- 2 hours ---     14:00 of 7 May

  CLog::log("get Share info: calc shares for date(" + cDate + ")");

  auto[minCreationDate, maxCreationDate] = getDNAActiveDateRange(cDate);

  QString query = "";
  if (CConsts::DATABASAE_AGENT == "psql")
  {
    query = "SELECT dn_shareholder, SUM(dn_shares) sum_ FROM " + stbl_dna_shares;
    query += " WHERE dn_creation_date between '" + minCreationDate + "' AND '" + maxCreationDate + "' GROUP BY dn_shareholder ORDER BY sum_ DESC";

  } else if (CConsts::DATABASAE_AGENT == "sqlite")
  {
    query = "SELECT dn_shareholder, SUM(dn_shares) sum_ FROM " + stbl_dna_shares;
    query += " WHERE dn_creation_date between \"" + minCreationDate + "\" AND \"" + maxCreationDate + "\" GROUP BY dn_shareholder ORDER BY sum_ DESC";

  }

      CLog::log("Retrieve shares for range cDate(" + cDate + ") -> (" + minCreationDate + " " + maxCreationDate + ")");
  // let msg = `Retrieve shares: SELECT shareholder _shareholder, SUM(shares) _share FROM i_dna_shares WHERE creation_date between '${minCreationDate}' AND '${maxCreationDate}' GROUP BY _shareholder ORDER BY _share DESC `;
  QueryRes shares = DbModel::customQuery(
    "db_comen_general",
    query,
    {"dn_shareholder", "sum_"},
    0);

  DNAShareCountT sum_shares = 0;
  QVector<Shareholder> holders_order_by_shares {};
  QHash<QString, DNAShareCountT> share_amount_per_holder {};
  for (QVDicT a_share: shares.records)
  {
    sum_shares += a_share.value("sum_").toDouble();

    CAddressT owner = a_share.value("dn_shareholder").toString();
    share_amount_per_holder[owner] = a_share.value("sum_").toDouble();
    holders_order_by_shares.push_back(Shareholder {
      owner,
      share_amount_per_holder[owner]
    });
  }
  return { sum_shares, share_amount_per_holder, holders_order_by_shares };
}

std::tuple<DNAShareCountT, DNASharePercentT> DNAHandler::getAnAddressShares(
  const CAddressT& address,
  CDateT cDate)
{
  if(cDate == "")
    cDate = CUtils::getNow();

  auto[sum_shares, share_amount_per_holder, tmp_] = getSharesInfo(cDate);
  Q_UNUSED(tmp_);
  DNAShareCountT shares = 0.0;
  double percentage = 0.0;
  if (share_amount_per_holder.keys().contains(address))
  {
    shares = share_amount_per_holder[address];
    percentage = ((shares * 100) / sum_shares);
  }
  percentage = CUtils::iFloorFloat(percentage);
  return {shares, percentage};
}


std::tuple<QString, DNAShareCountT, DNASharePercentT> DNAHandler::getMachineShares(QString cDate)
{
  if(cDate == "")
    cDate = CUtils::getNow();

  auto[sum_shares, share_amount_per_holder, tmp_] = getSharesInfo(cDate);
  Q_UNUSED(tmp_);
  QString backer_address = CMachine::getBackerAddress();
  DNAShareCountT shares = share_amount_per_holder.keys().contains(backer_address) ? share_amount_per_holder[backer_address] : 0;
  DNASharePercentT percentage = CUtils::iFloorFloat((shares / sum_shares) * 100);
  return {backer_address, shares, percentage};
}

QVDRecordsT DNAHandler::searchInDNA(
  const ClausesT& clauses,
  const QStringList& fields,
  const OrderT order,
  const uint64_t limit)
{
  QueryRes res = DbModel::select(
    stbl_dna_shares,
    fields,
    clauses,
    order,
    limit);
  return res.records;
}

void DNAHandler::updateDNAVotes(
  const ClausesT& clauses,
  const QVDicT& updates)
{
  DbModel::update(
    stbl_dna_shares,
    updates,
    clauses);
}
