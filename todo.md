
trx

<div>
    <div>
        There is a big list of TODOs, some of them are specialized for cryptographers and some of them can be done by a
        simple-computer user (e.g. translate). and there are a wide range in between you can fit in.
        please check the list, surly you will find(or can suggest) something to do to bring happiness to the world.
    </div>

    <div>
        must complete BEFORE RELEASE
        - Cloned Trx and P4P trx, duoble-spending TESTs
        - TO TEST: GQL, invoke for entire DAG (for newly joined node)
        - TOTEST: proposer pays for proposal
        - TOTEST: release resereved coins
        - TOTEST: bind PGP to iName
        - maybe, bind by default the default key to iname(when it registered)
        - TOTEST: send msg to iName
        - TOTEST: iName(registering, collision handling, sending content, even multimeida, and presenting in personal
        page)
        - TOTEST: iname collision solving
        - TOTEST: Messenger (specialy sending encrypt message to iName which toke place on blockchain, improve it to not
        recording msg in blockchain space but relayed by entire network until reaching the propoer machine)
        - TOTEST: Demos & Agora (forum)

        -* TOTEST: booting a machine and sync
        -* TOTEST: too many tests for double-spend. clone, p4p tests

        ------------------------
        remove am pm from cycle
        utf8Decode
        - by default a PGP key must be bind to an iName
        - improve POW block to be able send msgs from Bech to Bech address
        - possiblity to send to iName by and bech32 address, better possibility to send msg between bech32 addresses
        getDocumentMerkleProof({blockHash:, docHash:});
        verifyDocumentMerkleProof({blockHash:, docHash:});
        - set a limit of POW blocks per hour.

        - notify version updating
        - arbitrary voting, can be created easily, low cost, for everything. e.g. imagine Logo, development planing,
        political polling... (SASH_custom_validate_polling)
        - implemet pay to iName which takes place on blockchain.
        - make a Black Holes to shoot some inappropriate iNames(e.g. racism, violence...) to be inaccessible forever
    </div>

    <div>
        bind ip to iName
    </div>

    <div>
        bind receive cost to iName. the sender must pay to you in order to be delivered her mail to you.
    </div>

    <div>
        improve content rendering/formatting based on media wiki formattings
        full support of all media wiki conventions
    </div>

    <div>
        Improving Distributed Wiki based on mediawiki implementation
        todo: adding sign feature to wiki pages. in such a way viewers can find the most reliable pages(in case of
        adversary over-writes the qualified version, since everybody permited to edit every public pages).
    </div>

    <div>
        improvinge Demo & Agoras, in order to have nice graphic, folding for long texts,
        and most important re-order and restructure opinions in different ways e.g. most replyed(either positive or
        negative),
        extracting discussion main thread by folloing both cronological and reply.
        alongside ther discussions a nice pie chart statistics and sumup the discussion.
        all possible by a kind of plugin on top of existed structure and data
    </div>

    <div>
        adding optional new salt(or alter existed salt)
        in order to account owner add her name to address, and in such a way 
        sender(by knowing the recepient name/nick nale/alias name... and hashing it to check) 
        can be sure the address isn't type or transfer incorectly
    </div>

    <div>
        forum demos agora enhancing
        https://blog.discourse.org/2013/03/the-universal-rules-of-civilized-discourse/
	https://cryptome.org/2012/07/gent-forum-spies.htm
    </div>
    <div>
        making optional content record mode for social networks: record on blockchain or just distribute in one time broadcast style (like personal message or group chat contents)
    </div>
    <div>
        wallet implementing BIP32 - Hierarchical Deterministic Wallets
        implement a bind-bitcoin-address to FleNS contract.
        it can accept also an HD-key to generate new pub-keys beside the wallet owner(which is iName owner too)
	Bind BIP 0047 to iName in order to make easy and practical send to iName 
	since wallets can communicate vis iNames, we can implement PayJoin (pay-to-end-point or P2EP) as a default payment
    </div>

    <div>
        adding discussion tab to wiki, in order to have an Agora(forum | chatroom | comment place... which are all same)
        for each page|topic in a certain language
    </div>

    <div>
        add pinedPost posibility to Agoras, to change first posts to better ones
    </div>

    <div>
        improve pay to iName in order to be interactive and
        payer ask from payee address to pay
        payee generates some fresh address(by HD Wallet) and sends to payer. The payer transfers PAIs to payee
        all process and comunications done in iPGP and encrypted and not visible for others and in background
    </div>

    <div>
        improve pay-to-iName to accept bitcoin payment(and maybe some other real usefull alt-coins).
        imagine node accepts a bitcoin payment to an iName, and returns the proper bitcoin address of an iName(even
        generates a bunch of address based on HD-wallets)
        and return to client wallet a new transaction in which the receiver addresses are replaced by real bitcoin
        address of recipient.
    </div>

    <div>
        TOO many security issue to not allowing upload harmful files, during send block to network, validating received
        blocks, forwarding them and also presenting the file content in browsers
    </div>

    <div>
        improve iName in which the owner can set a customized profile
        e.g. picture, webite, social network...
        ALL profile pieces MUST be optional and manageable by owner, in order to decide to share which parts with whom
        and be able to hide it again.
    </div>

    <div>
        educational plan to teach more technicals
    </div>

    <div>
        Optimizing plugin-based architecture for both server(node) and client(Angular & maybe React)
        more accurate hooks in all Active/Passive Synchronous/Asynchronous different modes
    </div>

    <div>
        adding alternate media-layer. e.g. SOCKET & IP
    </div>

    <div>
        improve iName regisre & Bind, in order to by registering an iName, automatically create a default iPGP and bind
        it to iName be label Default.
    </div>

    <div>
        improving server security and efficiency in order to each node can be connected by mobile wallet freely
        and earn a litle PAI by provide service to mobile wallets.
        each mobile wallet has to have a couple of different fullnode-sites url and optionally casn send transactions to
        some of them.
    </div>

    <div>
        Generalize imagine-DNA in order to make it available for every one who wants start an activity in distributed
        mode and give shares and dividend of that to contributors just like imagine-DNA.
    </div>

    <div>
        although multi-sig is native feature of imagine,
        BTW in some cases payee wants to be sure the payment MUST sign with a certain key(kSet).
        e.g if the payee accpet a pledged account as a payer of a service regulare subscribtion, and want to to
        subscriber not be able to cancel service in advance
        and he must use service for a shile(for example at least 6 month abounment).
        in this case payee accepts the only signature-pledged in which has an 6 month outputTimeLock.
        to solve this issue in this special type of SAPledge the transactioon includes also a certain uSet
    </div>

    <div>
        implementing customizable theme style css for entire site
    </div>

    <div>
        Too many security checks
        OWASP
    </div>

    <div>
        complete language lists, file: imagine-server/web-interface/settings/languages.js
    </div>

    <div>
        improve some method to save & backup private/public keys easily, even backup it onchain
        or encrypt all bunch of keys in a file and put on chain and divide the password between some trusted iNames(such
        as firends, partner, relatives, lawyer...)
        in case of lost key, user can re-genererate pwd from friends and decrypt the onchain file.
    </div>

    <div>
        Tons of tests (uniit, integrity & e2e)
    </div>

    <div>
        clients(full node or wallets) can comunicate eachother through iNames, specially they interactively can share
        paublic-keys in order to create multi-sig addresses.
        for example user1 and user2 send one public key each to user3, and she by adding her public-key, creates a 2 of
        3 signature address.
        and share it with 2 other. now all 3 have a same bech32 address in their wallet which the funds can be spend by
        2 signature of 3.
        obviously to signing the funds, this 3 users needs to pre-sign trx comunication and so forth. and finally
        sending to network.
        all these can be done simply and in background by iName infostructure. without users panic of all complexity.
    </div>

    <div>
        implement strict input time lock
    </div>

    <div>
        unittest, and efficiency for inputTimelock controls in BasicTx
    </div>

    <div>
        implementing outputTimelock control in BasicTx
    </div>

    <div>
        implementing pleger redeem account.(if pledgee refused doing so)
    </div>

    <div>
        improve neighbor-reputation plugin
    </div>

    <div>
        plugin for polling system, in order to watch a polling result instantly & online, and also promot to vote(accept
        or reject)
        <br>also tons of tests for polling
    </div>

    <div>
        a kind of lock time in which a certain sSet can sign and spend fund until a certain taime, 
        and after that date the signature is invalid even if it is correct.
        
    </div>

    <div>
        implementing native block-explorer(DAG monitor) plugin
    </div>

    <div>
        implementing market prediction, Options, future contracts plugin
        since imagine is the only blockchain in which the backers have steady and predictable incomes for 7
        years(including reserved coins until 7+1+2+3+5=18 years),
        this income could be a very good security to many different type of reputation systems and Oracles.
        all aspect of smart contracts can be implemented easyly and bind to real-world with high degree of guaranty of
        good-treatment of actors in sistem.
        BTW, for more dynamic conditions it is possible to block imagine fund in an ethereum contract and use solidity
        to treat.
        in mid-term a bridge between imagine and ethreum to lock imagine incomes on imagine DAG and run smart-contracts
        on ethereum.
        in long-term, if it really needed impement our smart-contract platform(language, VM etc)
    </div>

    <div>
        implementing distributed issue tracking and project manager (Jira like) plugin for imagine eccosystem
    </div>

    <div>
        Later, improving IOT transactions to be more efficient than noraml transactions ( in sence of needed space &
        process)
    </div>

    <div>
        Later, improving in-jar transactions 
    </div>

    <div>
        /**
	 * the PAIs participated in these kind of transations will be transparently traceable until certain time(by minutes or changes between parties)
	 * e.g. after 5 time changing between people or/and 3 years 
	 * this kind of transparency is NECESSARY for paying Taxes, or donations,... in order to trace the actual use of the payments
	 * 
	 * output of this transactions, can not be used as input in any kind of transactions without dAttr included "TTT". 
	 * specially can not be used as input in any privacy protected transactions
	 * in sequence the outputs always must be spent in another Transparent Tx(in which the dAttr includes iConsts.TRX_ATTRS.TTT)
	 * 
	 * the coins come in this kind of transaction will be tagged as TTT until a certain change or/and time to be able to return to normal coins
	 * if sBM(survival By Minutes) is setted, the longivity of transparency will be started/renewd from transaction creationDate(in fact block.creationDate) 
	 * if sBC(survival By Change) is setted, the longivity of transparency will be started/renewd from transaction and will be counted from container transaction(the container is index zero)
	 * sCC(survival Conditions Combination) can be OR: for "or", AD: for "and" between sBM and sBC
	 * 
	 * if non of those 3(sBM, sBC, sCC) not mentioned in transaction but the trxType is "TTT" the 3 parameteres must be retieved from inputs.
	 * and the STRICTEST rolls MUST be applied on new transaction
	 * it means AND is prior than OR
	 * heigher sBM or sBC will be based of calculating
	 * 
	 * efter expiring sBM, sBC the new transaction can nulled the dAttr
	 * 
	 * 
	 * TODO: implementation & tests should take 16 hours, 
    </div>

    <div>
	implementing homomorphic one way functions and proper transaction type which support it(curve25519 is not quite homomorphic.  Ristretto25519 is however homomorphic)
    </div>

    <div>
        Bitcoin-like account Address & Transaction, in order to implement atomic swap between imagine & Bitcoin
    </div>

    <div>
        selecting an oen-source [[mobile-wallet|mobile-wallet]] project and customize it for imagine.
    </div>

    <div>
        since everything in imagine is designed to be modulare, so we can easily aggregate different behaviors in one
        eccosystem.
        in such a way the coins, can be used in BasicTx or can being transformed to different mode for example
        mimblewimble transactions or ZKP.
        in this case the same amount of PAIs are trading in mimble-mode and have the mimble charachteristic and
        everytime the owner of coins can transform it
        to Basic form or ZKP or ...
    </div>

    <div>
        boosting nodemailer to support more email client/servers
    </div>

    <div>
        adding new transfer medium as an optional plugin. in this case the plugin can be activated in order to broadcast
        the blocks(and GQL packets) in a efficient and alternate transformers
        such as direct IP/socket connections, or onion routing, maybe Dandelio...
        the plugin do not need to touch core code. it is enough to listen to hooks (SPSH_push_packet) to broadcast
        and in case of receiving new packet, calling hooks (SPSH_packet_ingress) will bring the packet to imagine core
        to parse & process it
    </div>

    <div>
        improve graphQL
    </div>

    <div>
        study for implementing other polling systems. either the way of voting or counting & presenting the result
    </div>

    <div>
        improve this version-patching system to be a real distributed versioning system with some handsfull commands
        (like commit, push...)
    </div>

    <div>
        fisibility study on RGB(colored coin) new generation implementing in imagine
        specialy offline transactions
    </div>

    <div>
        to support:
        The use of TLS in Censorship Circumvention
        uTLS as a wrapper for all censorship circumvention tools
    </div>

    <div>
        an app to automatically convert some old but gold mailing-lists (e.g. cryptography, cypherpunks, activists...)
        to new our Demos/Agoras in a modern form and presentation, in order to use the veteran's experiences and discussions.
        and also translate ALL to ALL languages.
    </div>

    <div>
        study on real-world needed contracts to implementing in imagine-core.
    </div>

    <div>
        develope PledgeG & PledgeL contracts as like as implemented PledgeP
        and later on developing PledgePZKP in respect of privacy
    </div>

    <div>
        the pledgeL is a loan system, user can pledge her income(which is essentialy an imagine Bech32 Address that has
        shares) in fron of get lon by PAIs
    </div>

    <div>
        C++: remove all calcDocDataAndProcessCost to Block::blockGeneralControls()
    </div>

    <div>
        implement sequence signature in order to keep your money if you compromise your first private key, so you will use second private key and
        from the moent you are using new private key, the older private key will be invalid.
        {
            uSet{
                sequenceNumber:0
                sSets:{}
            }
            uSet{
                sequenceNumber:1
                sSets:{}
            }
            ...
        }
    </div>

    <div>
        improve pledgeL to transfer the PAIs immidiately to vendor's account, in this case pledgeL acts like a "pay over
        time" system in which user can buy a service/good/product
        and pay the cost over time with/without interest/late fees, penalties, and compounding interest,...
    </div>

    <div>
        I order to improve privacy in eccosystem, it needed to [[iPGP|improve PGP comunication between nodes]]. to
        support existed legacy GPG and web of trust
    </div>

    <div>
        refactor interface in order to accept template. maybe immigrate to React or Vue!
    </div>

    <div>
        adjust code to support multi language i18n
        then translate menu, label, content... ALL to ALL languages
    </div>

    <div>
        a class(dClass=VePoll Verify by Polling Result) of Basic transactions in which instead verify the signature of
        input owners, machines use the result of a generall polling
        and based on the result decide to transfer PAIs or reject the entire transaction.
        1. owner of funds will be used in trx signs the fund through a contract in which binds a polling.
        2. 3 documents(trx, polling & VePollContract) put on chain, and because of that the fund will be lock until end
        of polling period.
        3. after finish polling depends on the results the transaction will be valid or invalid

        P.S. same principals cn be implemented to do a Pledge/Unpledge(obviously for all class of pledges e.g. PledgeP,
        PledgeSA...)
        based on a polling.
        the polling participane can be ALL or be mentioned in VePollContract
    </div>

    <div>
        Implementing data-selling plugin(code, protocol...), in order to backers can be payed because of backing up mass
        data.
        using Universal Business Language UBL for nodes comunications
    </div>
    <div>
        Enabling Clone and P4P transactions
    </div>

    <div>
        refactor Routing in angular in order to having http://localhost:4200/imagine/... for all parts related to
        imagine
        and http://localhost:4200/iName1/... for all parts related to an hypothetical iName1
    </div>
</div>

embedding magma (lavabit protocol) in Comen ans an optional mail server.
nodes can earn revenue by providing email service and/or mail deliver.

embedding cjdns to Comen core
	Networking Reinvented
	https://github.com/cjdelisle/cjdns

