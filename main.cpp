#include "mainwindow.h"
#include "stable.h"

#include "lib/ccrypto.h"
#include "global_funcs.hpp"


int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  MainWindow w;
  w.show();

  int manual_clone_id = 0;

  QObject::connect(&app, &QCoreApplication::aboutToQuit, [&]()
  {
    CMachine::onAboutToQuit(&w);
  });
  CMachine::parseArgs(argc, argv, manual_clone_id);
  InitCCrypto::init();

  CMachine::setLaunchDateAndCloneId("2021-03-02 00:20:00", manual_clone_id);

  w.initMachineEnvironment();

  if (true)
  {
    dummyTestsHandler();
  }

  return app.exec();
}
