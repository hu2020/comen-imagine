#ifndef CGUI_H
#define CGUI_H

class MainWindow;

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <thread>
#include <future>

#include <QFileDialog>
#include <QApplication>
#include <QTableView>
#include <QItemDelegate>
#include <QIdentityProxyModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QPixmap>
#include <QObject>

#include "lib/machine/machine_handler.h"
#include "lib/dag/state/dag_state_handler.h"
#include "lib/services/free_docs/demos/demos_handler.h"

//  -  -  -  monitor
#include "gui/models/model_blocks.h"
#include "gui/models/model_neighbors.h"
#include "gui/models/model_parsing_q.h"
#include "gui/models/model_sending_q.h"
#include "gui/models/model_snapshots.h"
#include "gui/models/model_dag_leaves.h"
#include "gui/models/model_contributes.h"
#include "gui/models/model_wiki_titles.h"
#include "gui/models/model_agora_titles.h"
#include "gui/models/model_block_buffer.h"
#include "gui/models/model_adm_pollings.h"
#include "gui/models/model_snap_compare.h"
#include "gui/models/model_missed_blocks.h"
#include "gui/models/model_draft_pledges.h"
#include "gui/models/model_draft_proposals.h"
#include "gui/models/model_society_pollings.h"
#include "gui/models/model_coins_visbility.h"
#include "gui/models/model_machine_balances.h"
#include "gui/models/model_machine_contracts.h"
#include "gui/models/model_registered_inames.h"
#include "gui/models/model_send_loan_request.h"
#include "gui/models/model_received_loan_requests.h"
#include "gui/models/model_machine_controlled_inames.h"
#include "gui/models/model_proposal_pledge_transactions.h"

//  -  -  -  wallet
#include "gui/models/model_wallet_coins.h"
#include "gui/models/model_wallet_accounts.h"


class CGUI: public QObject
{
  Q_OBJECT
public:

  Ui::MainWindow* m_ui;
  bool m_checkBox_ToS;

  ModelContributes* m_model_contributes;
  ModelDraftProposals* m_model_draft_proposals;
  ModelDraftPledges* m_model_draft_pledges;
  ModelSendLoanRequest* m_model_send_loan_request;
  ModelReceivedLoanRequests* m_model_received_loan_requests;
  ModelProposalPledgeTransactions* m_model_proposal_pledge_transactions;
  ModelMachineContracts* m_model_machine_contracts;
  ModelRegisteredINames* m_model_registered_inames;
  ModelMachineControlledINames* m_model_machine_controlled_inames;
  ModelBlocks* m_model_blocks;
  ModelParsingQ* m_model_parsing_q;
  ModelSendingQ* m_model_sending_q;
  ModelNeighbors* m_modelNeighbors;
  ModelMissedBlocks* m_model_missed_blocks;
  ModelBlockBuffer* m_model_block_buffer;
  ModelAdmPollings* m_model_adm_pollings;
  ModelSocietyPollings* m_model_society_pollings;
  ModelCoinsVisbility* m_model_coins_visbility;
  ModelSnapshots* m_model_snapshots;
  ModelSnapCompare* m_model_snap_compare;
  ModelMachineBalances* m_model_machine_balances;
  ModelWalletCoins* m_model_wallet_coins;
  ModelWalletAccounts* m_model_wallet_accounts;
  ModelWikiTitles* m_model_wiki_titles;
  ModelAgoraTitles* m_model_agora_titles;


  CGUI(const CGUI&) = delete;
  static CGUI& get();
  static void initGUI(Ui::MainWindow* ui);

  static QHash<QString, QBrush> getBlocksBrushes();
  static QHash<QString, QBrush> getCPacketBrushes();
  static QBrush getBrushByColorCode(const QString& color = "000000");
  static QBrush getBrushByDocType(const QString& doc_type = "");
  static QPalette getErrorPalette();
  static QPalette getNormalPalette();
  void static displayMsg(
    QLabel* obj,
    QString msg,
    bool status = true);
//  void static displayMsg(QLabel* obj, std::string msg, bool status) { displayMsg(obj, QString::fromStdString(msg), status); };


  static void connectSignalSlots(){ get().IconnectSignalSlots(); };
  static void launchLazyLoadings(){ get().IlaunchLazyLoadings(); };

  static void setModelBlocks(ModelBlocks* model){ get().IsetModelBlocks(model); };
  static void signalUpdateBlocks(){ get().m_model_blocks->signalUpdate(); };

  static void setModelParsingQ(ModelParsingQ* model){ get().IsetModelParsingQ(model); };
  static ModelParsingQ* getModelParsingQ(){ return get().m_model_parsing_q; };
  static void signalUpdateParsingQ(){ get().m_model_parsing_q->signalUpdate(); };

  static void setModelMachineBalances(ModelMachineBalances* model){ get().IsetModelMachineBalances(model); };
  static void signalUpdateMachineBalances(){ get().m_model_machine_balances->signalUpdate(); };

  static void setModelSendingQ(ModelSendingQ* model){ get().IsetModelSendingQ(model); };
  static void signalUpdateSendingQ(){ get().m_model_sending_q->signalUpdate(); };

  static void setModelNeighbors(ModelNeighbors* model){ get().IsetModelNeighbors(model); };
  static void signalUpdateNeighbors(){ get().m_modelNeighbors->signalUpdate(); };

  static void setModelMissedBlocks(ModelMissedBlocks* model){ get().IsetModelMissedBlocks(model); };
  static void signalUpdateMissedBlocks(){ get().m_model_missed_blocks->signalUpdate(); };

  //  -  -  - Misc
  static void setModelBlockBuffer(ModelBlockBuffer* model){ get().IsetModelBlockBuffer(model); };
  static void signalUpdateBlockBuffer();


  //  -  -  - Society
  static void setModelAdmPollings(ModelAdmPollings* model){ get().IsetModelAdmPollings(model); };
  static ModelAdmPollings* getModelAdmPollings(){ return get().IgetModelAdmPollings(); };
  static void signalUpdateAdmPollings();
  static void hideAdmPollingVoteForm();

  //  -  -  - Society
  static void setModelSocietyPollings(ModelSocietyPollings* model){ get().IsetModelSocietyPollings(model); };
  static ModelSocietyPollings* getModelSocietyPollings(){ return get().IgetModelSocietyPollings(); };
  static void signalUpdateSocietyPollings();
  static void toggleSocietyVoteForm(const CDocHashT& doc_hash);

  //  -  -  - DAG state
  static void setModelCoinsVisbility(ModelCoinsVisbility* model){ get().IsetModelCoinsVisbility(model); };
  static void signalUpdateCoinsVisbility(){ get().m_model_coins_visbility->signalUpdate(); };

  static void setModelSnapshots(ModelSnapshots* model){ get().IsetModelSnapshots(model); };
  static ModelSnapshots* getModelSnapshots(){ return get().IgetModelSnapshots(); };
  static void signalUpdateSnapshots(){ get().m_model_snapshots->signalUpdate(); };
  static void setModelSnapCompare(ModelSnapCompare* model){ get().IsetModelSnapCompare(model); };
  static ModelSnapCompare* getModelSnapCompare(){ return get().IgetModelSnapCompare(); };
  static void compareSnapshots(const int snapshot_id){ get().IcompareSnapshots(snapshot_id); };


  static void setModelDAGLeaves(ModelDAGLeaves* model){ get().IsetModelDAGLeaves(model); };
  static void signalUpdateDAGLeaves(){
    get().m_model_DAG_leaves[ModelDAGLeaves::ActionType::LEAVES_BY_KVALUE]->signalUpdate();
    get().m_model_DAG_leaves[ModelDAGLeaves::ActionType::LEAVES_BY_DAG]->signalUpdate();
    get().m_model_DAG_leaves[ModelDAGLeaves::ActionType::FRESH_LEAVES]->signalUpdate();
  };

  static void refreshMonitor(){ get().IrefreshMonitor(); };

  //  -  -  -  wallet part

  static void setModelWalletCoins(ModelWalletCoins* model){ get().IsetModelWalletCoins(model); };
  static ModelWalletCoins* getModelWalletCoins(){ return get().IgetModelWalletCoins(); };
  static void resetSelectedWalletCoins(){ get().m_model_wallet_coins->resetSelectedWalletCoins(); };
  static void signalUpdateWalletCoins(){ get().m_model_wallet_coins->signalUpdate(); };
  static void refreshTransactionFormPresentation(){ get().IrefreshTransactionFormPresentation(); };

  static void setModelWalletAccounts(ModelWalletAccounts* model){ get().IsetModelWalletAccounts(model); };
  static ModelWalletAccounts* getModelWalletAccounts(){ return get().m_model_wallet_accounts; };
  static void signalUpdateWalletAccounts(){ get().m_model_wallet_accounts->signalUpdate(); };

  //  -  -  -  wiki part
  static void setModelWikiTitles(ModelWikiTitles* model){ get().IsetModelWikiTitles(model); };
  static ModelWikiTitles* getModelWikiTitles(){ return get().m_model_wiki_titles; };
  static void signalUpdateWikiTitles(){ get().m_model_wiki_titles->signalUpdate(); };
  static void wikiPageSelected(const QModelIndex& index);
  static void initLanguagesCombo(QComboBox* obj);
  static void showWikiRegForm();
  static void hideWikiRegForm();
  static void showWikiEditForm();
  static void hideWikiEditForm();


  //  -  -  -  Demos/Agora part
  static void setModelAgoraTitles(ModelAgoraTitles* model){ get().IsetModelAgoraTitles(model); };
  static ModelAgoraTitles* getModelAgoraTitles(){ return get().m_model_agora_titles; };
  static void signalUpdateAgoraTitles(){ get().m_model_agora_titles->signalUpdate(); };
  static void agoraSelected(const QModelIndex& index);
  static void initWalletControlledAddressesCombo(QComboBox* obj);
  static void initINamesCombo(QComboBox* obj, bool include_imagine = true);
  static void initAgorasCategoriesCombo(QComboBox* obj, const QString& selected_iname = "");
  static void showAgoraReplyForm();
  static void hideAgoraReplyForm();
  static void showAgoraRegForm();
  static void hideAgoraRegForm();


  //  -  -  -  contribute part
  static void setModelContributes(ModelContributes* model){ get().IsetModelContributes(model); };
  static void toggleContributeVoteForm(const CDocHashT& doc_hash);
  static void hideContributeVoteForm();
  static ModelContributes* getModelContributes(){ return get().m_model_contributes; };
  static void signalUpdateContributes(){ get().m_model_contributes->signalUpdate(); };
  static void setModelDraftProposals(ModelDraftProposals* model){ get().IsetModelDraftProposals(model); };
  static ModelDraftProposals* getModelDraftProposal(){ return get().m_model_draft_proposals; };
  static void signalUpdateDraftProposals(){ get().m_model_draft_proposals->signalUpdate(); };
  static void contreibuteDblClicked(const QModelIndex& index);
  static void toggleLoanRequestsList(const CDocHashT& doc_hash);
  static void toggleLoanRequestForm(const CDocHashT& doc_hash);
  static void initDraftProposalFormsDisplay();
  static void refreshLoanReqSummary();
  static void setModelDraftPledges(ModelDraftPledges* model){ get().IsetModelDraftPledges(model); };
  static ModelDraftPledges* getModelDraftPledges(){ return get().m_model_draft_pledges; };
  static void signalUpdateDraftPledges(){ get().m_model_draft_pledges->signalUpdate(); };
  static void setModelSendLoanRequest(ModelSendLoanRequest* model){ get().IsetModelSendLoanRequest(model); };
  static ModelSendLoanRequest* getModelSendLoanRequest(){ return get().m_model_send_loan_request; };
  static void signalUpdateSendLoanRequest(){ get().m_model_send_loan_request->signalUpdate(); };
  static void showSendLoanRequest();

  //  -  -  -  contracts part
  static void setModelReceivedLoanRequests(ModelReceivedLoanRequests* model){ get().IsetModelReceivedLoanRequests(model); };
  static ModelReceivedLoanRequests* getModelReceivedLoanRequests(){ return get().m_model_received_loan_requests; };
  static void signalUpdateReceivedLoanRequests(){ get().m_model_received_loan_requests->signalUpdate(); };
  static void setModelProposalPledgeTransactions(ModelProposalPledgeTransactions* model){ get().IsetModelProposalPledgeTransactions(model); };
  static ModelProposalPledgeTransactions* getModelProposalPledgeTransactions(){ return get().m_model_proposal_pledge_transactions; };
  static void signalUpdateProposalPledgeTransactions(){ get().m_model_proposal_pledge_transactions->signalUpdate(); };

  static void setModelMachineContracts(ModelMachineContracts* model){ get().IsetModelMachineContracts(model); };
  static ModelMachineContracts* getModelMachineContracts(){ return get().m_model_machine_contracts; };
  static void signalUpdateMachineContracts(){ get().m_model_machine_contracts->signalUpdate(); };

  //  -  -  -  iNames part
  static void setModelRegisteredINames(ModelRegisteredINames* model){ get().IsetModelRegisteredINames(model); };
  static ModelRegisteredINames* getModelRegisteredINames(){ return get().m_model_registered_inames; };
  static void signalUpdateRegisteredINames(){ get().m_model_registered_inames->signalUpdate(); };
  static void initMachineControlledINames();
  static void setModelMachineControlledINames(ModelMachineControlledINames* model){ get().IsetModelMachineControlledINames(model); };
  static ModelMachineControlledINames* getModelMachineControlledINames(){ return get().m_model_machine_controlled_inames; };
  static void signalUpdateMachineControlledINames(){ get().m_model_machine_controlled_inames->signalUpdate(); };
  static void refresh_inames_info();

signals:

private slots:





private:
  explicit CGUI(QObject *parent = nullptr);
//  CGUI(){};
  static CGUI s_instance;
  void IinitGUI(Ui::MainWindow* ui);

  void IconnectSignalSlots();
  void IlaunchLazyLoadings();

  void IsetModelBlocks(ModelBlocks* model){
    m_model_blocks = model;
    m_model_blocks->m_brushes = getBlocksBrushes();
  };

  void IsetModelParsingQ(ModelParsingQ* model){
    m_model_parsing_q = model;
    m_model_parsing_q->m_brushes = getCPacketBrushes();
  };

  MachineTransientBalances* m_machine_transient_balances;
  void IsetMachineBalances(MachineTransientBalances* model){
    m_machine_transient_balances = model;
  };

  void IsetModelSendingQ(ModelSendingQ* model){
    m_model_sending_q = model;
    m_model_sending_q->m_brushes = getCPacketBrushes();
  };

  void IsetModelNeighbors(ModelNeighbors* model){
    m_modelNeighbors = model;
    m_model_blocks->m_brushes = getBlocksBrushes();
  };

  void IsetModelMissedBlocks(ModelMissedBlocks* model){
    m_model_missed_blocks = model;
    m_model_missed_blocks->m_brushes = getBlocksBrushes();
  };

  void IsetModelBlockBuffer(ModelBlockBuffer* model)
  {
    m_model_block_buffer = model;
  };

  ModelAdmPollings* IgetModelAdmPollings()
  {
    return m_model_adm_pollings;
  }
  void IsetModelAdmPollings(ModelAdmPollings* model)
  {
    m_model_adm_pollings = model;
  };
  ModelSocietyPollings* IgetModelSocietyPollings()
  {
    return m_model_society_pollings;
  }
  void IsetModelSocietyPollings(ModelSocietyPollings* model)
  {
    m_model_society_pollings = model;
  };

  void IsetModelCoinsVisbility(ModelCoinsVisbility* model){
    m_model_coins_visbility = model;
    m_model_coins_visbility->m_brushes = getBlocksBrushes();
  };

  void IsetModelSnapshots(ModelSnapshots* model){
    m_model_snapshots = model;
  };
  ModelSnapshots* IgetModelSnapshots()
  {
    return m_model_snapshots;
  };

  void IsetModelSnapCompare(ModelSnapCompare* model){
    m_model_snap_compare = model;
  };
  ModelSnapCompare* IgetModelSnapCompare()
  {
    return m_model_snap_compare;
  };

  QHash<int, ModelDAGLeaves*> m_model_DAG_leaves;
  void IsetModelDAGLeaves(ModelDAGLeaves* model)
  {
    m_model_DAG_leaves[model->m_action_type] = model;
//    m_model_DAG_leaves->m_brushes = getBlocksBrushes();
  };

  void IsetModelMachineBalances(ModelMachineBalances* model){
    m_model_machine_balances = model;
  };

  void IrefreshMonitor();

  //  -  -  -  wallet part
  void IsetModelWalletCoins(ModelWalletCoins* model){
    m_model_wallet_coins = model;
  };
  ModelWalletCoins* IgetModelWalletCoins(){
    return m_model_wallet_coins;
  };
  void IsetModelWalletAccounts(ModelWalletAccounts* model){
    m_model_wallet_accounts = model;
  };
  void IrefreshTransactionFormPresentation();

  //  -  -  -  wiki part
  void IsetModelWikiTitles(ModelWikiTitles* model){
    m_model_wiki_titles = model;
  };

  //  -  -  -  contributes part
  void IsetModelContributes(ModelContributes* model){
    m_model_contributes = model;
  };
  void IsetModelDraftProposals(ModelDraftProposals* model){
    m_model_draft_proposals = model;
  };
  void IsetModelDraftPledges(ModelDraftPledges* model){
    m_model_draft_pledges = model;
  };
  void IsetModelSendLoanRequest(ModelSendLoanRequest* model){
    m_model_send_loan_request = model;
  };

  //  -  -  -  iNames
  void IsetModelRegisteredINames(ModelRegisteredINames* model)
  {
    m_model_registered_inames = model;
  };
  void IsetModelMachineControlledINames(ModelMachineControlledINames* model)
  {
    m_model_machine_controlled_inames = model;
  };

  //  -  -  -  Contracts
  void IsetModelReceivedLoanRequests(ModelReceivedLoanRequests* model)
  {
    m_model_received_loan_requests = model;
  };
  void IsetModelProposalPledgeTransactions(ModelProposalPledgeTransactions* model)
  {
    m_model_proposal_pledge_transactions = model;
  };
  void IsetModelMachineContracts(ModelMachineContracts* model)
  {
    m_model_machine_contracts = model;
  };

  //  -  -  -  Demos/Agora part
  void IsetModelAgoraTitles(ModelAgoraTitles* model){
    m_model_agora_titles = model;
  };

  void IcompareSnapshots(const int snapshot_id);
};


#endif // CGUI_H
