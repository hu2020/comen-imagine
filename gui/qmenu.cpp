#include "c_gui.h"


void MainWindow::on_actionBackup_Machine_triggered()
{
    CLog::log("on actionBackup_Machine_triggered");
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_pushButton_restoreMachineFromBackup_clicked()
{
  CLog::log("on_pushButton_restoreMachineFromBackup_clicked*************");
  QString filter = "All file (*.*) ;; Text file (*.txt)";
  QString file_name = QFileDialog::getOpenFileName(this, "Open a file", "/");
  QFile file(file_name);
  if (!file.open(QFile::ReadOnly))
  {
    warningMessage("Failed to open file!", "Restore Backup");
  }
  QTextStream in(&file);
  QString content = in.readAll();
  CLog::log(content);

  // FIXME: implement restore
  informationMessage("Backeup restored sucessfully.", "Restore Backup");
}
