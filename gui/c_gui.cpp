#include "stable.h"

#include "mainwindow.h"
#include "lib/ccrypto.h"
#include "ui_mainwindow.h"
#include "lib/services/free_docs/wiki/wiki_handler.h"

#include "c_gui.h"

CGUI::CGUI(QObject *parent) : QObject(parent)
{
}


CGUI CGUI::s_instance;

CGUI& CGUI::get()
{
  return s_instance;
}

void CGUI::initGUI(Ui::MainWindow* ui){
  get().IinitGUI(ui);
};

void CGUI::IinitGUI(Ui::MainWindow* ui)
{
  m_ui = ui;
  QPixmap c_logo(":/assets/comen_logo.jpg");
  m_ui->label_logo_comen->setPixmap(c_logo.scaled(333, 299, Qt::KeepAspectRatio));

}

void CGUI::IconnectSignalSlots()
{
  // connect model / app signal slots
//  connect(
//    m_model_blocks,
//  &  ModelBlocks::signalData,
//    m_model_blocks,
//  &  ModelBlocks::populateData);

  CMachine::setIsGUIConnected(true);
}

void CGUI::IlaunchLazyLoadings()
{

  //  -  -  -  monitor loadings
  signalUpdateMissedBlocks();
  signalUpdateParsingQ();
  signalUpdateSendingQ();
  signalUpdateBlocks();
  signalUpdateDAGLeaves();
  signalUpdateMachineBalances();

  //  -  -  -  neighbors loadings
  signalUpdateNeighbors();

  //  -  -  -  contributes loadings
  signalUpdateContributes();
  signalUpdateDraftProposals();

  //  -  -  -  contracts loadings
  signalUpdateReceivedLoanRequests();
  signalUpdateProposalPledgeTransactions();
  signalUpdateMachineContracts();

  //  -  -  -  Demos/Agora loadings
  signalUpdateAgoraTitles();
  initAgorasCategoriesCombo(get().m_ui->comboBox_agora_category, "imagine");

  //  -  -  -  Wiki loadings
  signalUpdateWikiTitles();

  //  -  -  -  wallet loadings
  resetSelectedWalletCoins();
  signalUpdateWalletCoins();
  signalUpdateWalletAccounts();

  //  -  -  -  wallet loadings
  signalUpdateCoinsVisbility();
  signalUpdateSnapshots();

  //  -  -  -  misc loadings
  signalUpdateBlockBuffer();

  //  -  -  -  society loadings
  signalUpdateAdmPollings();
  signalUpdateSocietyPollings();

  //  -  -  -  inames
  signalUpdateRegisteredINames();
  signalUpdateMachineControlledINames();

}

void CGUI::displayMsg(
  QLabel* obj,
  QString msg,
  bool status)
{
  obj->setAutoFillBackground(true);
  if (status)
  {
    obj->setPalette(getNormalPalette());
  }else{
    obj->setPalette(getErrorPalette());
  }
  obj->setText(msg);

}


//  -  -  -  MainWindow

void MainWindow::connectGUIDataModels()
{
  QString title = "Comen App";
  if (CMachine::getAppCloneId() > 0)
    title += " (clone " + QString::number(CMachine::getAppCloneId()) + ")";
  this->setWindowTitle(title);


  // (maybe init machin) and then read machine configs
  bool m_checkBox_ToS = (CMachine::getProfile().m_mp_settings.m_term_of_services == CConsts::YES);
  ui->checkBox_ToS->setChecked(m_checkBox_ToS);

  EmailSettings public_email = CMachine::getProfile().m_mp_settings.m_public_email;
  ui->lineEdit_publicEmailAddress->setText(public_email.m_address);
  ui->lineEdit_publicEmailInterval->setText(public_email.m_fetching_interval_by_minute);
  ui->lineEdit_publicEmailIncomeHost->setText(public_email.m_incoming_mail_server);
  ui->lineEdit_publicEmailPassword->setText(public_email.m_password);
  ui->lineEdit_lineEdit_publicEmailIncomeIMAP->setText(public_email.m_income_IMAP);
  ui->lineEdit_lineEdit_publicEmailIncomePOP->setText(public_email.m_income_POP3);
  ui->lineEdit_lineEdit_publicEmailOutgoingSMTP->setText(public_email.m_outgoing_SMTP);
  ui->lineEdit_publicEmailOutgoingHost->setText(public_email.m_outgoing_mail_server);

  auto[backer_address, shares_, percentage] = DNAHandler::getMachineShares();
  Q_UNUSED(shares_);
  ui->label_backerAddress->setText("Backer Address: " + backer_address + " " + CUtils::convertFloatToString(percentage) + " % shares");

  QString iname_noti = "The iNames are super flexible concept in imagine, they are using to make a personal website,";
  iname_noti += "\nsending/receiving messages, participating in pollings, send/receiving coins(either imagine PAIs or Bitcoin ...) ";
  iname_noti += "\nyou can register your own iName here. in order to register an iName you should have atleast 30 hours of contribute. ";
  iname_noti += "\nThe iNames can not contain dot \".\" unless beeing a valid email. ";
  iname_noti += "\nfor now iNames are case insensitive and accept ONLY ASCII characters. ";
  iname_noti += "\nIn future upgrades will be add unicode supports too.";
  ui->label_iname_reg_form_noti_msg->setText(iname_noti);

  auto[shares, allowed_inames, already_used, availables_count] = INameHandler::ownerRegisteredRecords(CMachine::getBackerAddress());
  QString allowed_iname_msg = "You already registered " + CUtils::sepNum(already_used) + " iNames from your " + CUtils::sepNum(allowed_inames) + " allowed.";
  allowed_iname_msg += "\nIn order to register one iName you need to participate in project by 150 shares = \n5 days * 5 hours (which will be normal working hours for a human prosper life) * average level(6)";
  ui->label_allowed_iname_count->setText(allowed_iname_msg);

  // load neighbors list
  QVDRecordsT neighbors = CMachine::getNeighbors();
    for(QVDicT a_nei: neighbors)
      ui->comboBox_neighbors_list->addItem(a_nei.value("n_email").toString() + "(" + a_nei.value("n_connection_type").toString() + ")", a_nei.value("n_id").toInt());

  // load machine controlled iNames
  CGUI::initMachineControlledINames();

  // bind signal slots
  connectDAGHistoryToModel();
  connectInboxFilesToModel();
  connectOutboxFilesToModel();
  connectParsingQToModel();
  connectSendingQToModel();
  connectNeighborsToModel();
  connectMissedBlocksToModel();
  connectDAGLeavesModel();
  connectMachineBalances();

  // bind wallet signals
  connectWalletCoinsToModel();
  connectWalletAccountsToModel();

  // bind wiki signals
  connectWikiTitlesToModel();
  CGUI::initLanguagesCombo(CGUI::get().m_ui->comboBox_wiki_languages);
  CGUI::initINamesCombo(CGUI::get().m_ui->comboBox_wiki_domain);

  // bind misc signals
  connectBlockBufferToModel();


  // bind society signals
  connectAdmPollingsToModel();
  connectSocietyPollingsToModel();
  CGUI::hideAdmPollingVoteForm();

  // bind Demos signals
  connectAgoraTitlesToModel();
  CGUI::initLanguagesCombo(CGUI::get().m_ui->comboBox_agora_languages);
  CGUI::initWalletControlledAddressesCombo(CGUI::get().m_ui->comboBox_agora_author);
  CGUI::initINamesCombo(CGUI::get().m_ui->comboBox_agora_domain);
  CGUI::hideAgoraRegForm();
  CGUI::hideAgoraReplyForm();

  // contributes
  connectContributesToModel();
  connectDraftProposalsToModel();
  connectDraftPledgesToModel();
  connectSendLoanRequestToModel();
  ui->lineEdit_loanRequestPledgee->setText(CConsts::HU_DNA_SHARE_ADDRESS);
  CGUI::initDraftProposalFormsDisplay();
  CGUI::hideContributeVoteForm();

  // -  -  -  iNames
  connectRegisteredINamesToModel();
  connectMachineControlledINamesToModel();

  // -  -  -  Contracts
  connectReceivedLoanRequestsToModel();
  connectProposalPledgeTransactionsToModel();
  connectMachineContractsToModel();

  // DAG state
  connectSnapshotModel();
  connectSnapComapreModel();
  connectCoinsVisbilityModel();

}

void MainWindow::informationMessage(const QString& msg, const QString& title)
{
  CLog::log(msg);
  QMessageBox::information(this, title, msg);
}

void MainWindow::warningMessage(const QString& msg, const QString& title)
{
  CLog::log(msg);
  QMessageBox::warning(this, title, msg);
}
QBrush CGUI::getBrushByColorCode(const QString& color)
{
  QColor col = QColor(
    color.midRef(0, 2).toInt(0, 16),
    color.midRef(2, 2).toInt(0, 16),
    color.midRef(4, 2).toInt(0, 16),
    127);
  QBrush brush(col, Qt::SolidPattern);
  return brush;
}

QHash<QString, QBrush> CGUI::getCPacketBrushes()
{
  return QHash<QString, QBrush> {
    {CConsts::BLOCK_TYPES::Genesis, QBrush(QColor(47, 254, 44, 91), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::Coinbase, QBrush(QColor(247, 74, 44, 91), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::RpBlock, QBrush(QColor(223, 131, 118, 91), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::RlBlock, QBrush(QColor(17,225,238, 51), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::FSign, QBrush(QColor(94, 94, 255, 255), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::Normal, QBrush(QColor(97, 195, 238, 176), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::POW, QBrush(QColor(205, 204, 93, 99), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::SusBlock, QBrush(QColor(197,95,238, 75), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::FVote, QBrush(QColor(151, 101, 197, 91), Qt::SolidPattern)}};
}

QHash<QString, QBrush> CGUI::getBlocksBrushes()
{
  return QHash<QString, QBrush> {
    {CConsts::BLOCK_TYPES::Genesis, QBrush(QColor(47, 254, 44, 91), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::Coinbase, QBrush(QColor(247, 74, 44, 91), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::RpBlock, QBrush(QColor(223, 131, 118, 91), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::RlBlock, QBrush(QColor(17,225,238, 51), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::FSign, QBrush(QColor(94, 94, 255, 255), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::Normal, QBrush(QColor(97, 195, 238, 175), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::POW, QBrush(QColor(205, 204, 93, 99), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::SusBlock, QBrush(QColor(197,95,238, 5), Qt::SolidPattern)},
    {CConsts::BLOCK_TYPES::FVote, QBrush(QColor(151, 101, 197, 91), Qt::SolidPattern)}};
}

const QHash<QString, QBrush> DOCUMENTS_COLOR {
  {CConsts::DOCUMENT_TYPES::DNAProposal, QBrush(QColor(47, 254, 44, 91), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::Coinbase, QBrush(QColor(247, 74, 44, 91), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::RpDoc, QBrush(QColor(223, 131, 118, 91), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::FPost, QBrush(QColor(17,225,238, 51), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::Pledge, QBrush(QColor(94, 94, 255, 255), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::ClosePledge, QBrush(QColor(94, 94, 255, 255), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::BasicTx, QBrush(QColor(97, 195, 238, 175), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::INameReg, QBrush(QColor(205, 204, 93, 99), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::INameBind, QBrush(QColor(205, 204, 93, 99), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::INameMsgTo, QBrush(QColor(205, 204, 93, 99), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::AdmPolling, QBrush(QColor(197,95,238, 5), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::Polling, QBrush(QColor(197,95,238, 5), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::Ballot, QBrush(QColor(197,95,238, 5), Qt::SolidPattern)},
  {CConsts::DOCUMENT_TYPES::customDoc, QBrush(QColor(151, 101, 197, 91), Qt::SolidPattern)}};


QBrush CGUI::getBrushByDocType(const QString& doc_type)
{
  if (DOCUMENTS_COLOR.keys().contains(doc_type))
    return DOCUMENTS_COLOR[doc_type];
  return QBrush();
}

void CGUI::IrefreshMonitor()
{
  signalUpdateBlocks();
  signalUpdateParsingQ();
  signalUpdateSendingQ();
  signalUpdateNeighbors();
  signalUpdateDAGLeaves();
  signalUpdateMissedBlocks();
  signalUpdateMachineBalances();

  //  -  -  -  DAG state
  signalUpdateCoinsVisbility();
  signalUpdateSnapshots();

}

void CGUI::IcompareSnapshots(const int snapshot_id)
{
  auto [status, report] = DAGStateHandler::compareSnapshots(snapshot_id);
  m_model_snap_compare->signalUpdate2(report);
};


void CGUI::IrefreshTransactionFormPresentation()
{
  m_ui->label_changebackAmount->setText("Change back Amount: " + CUtils::microPAIToPAI6(getModelWalletCoins()->m_trx_change_back));
  m_ui->label_spendableAmount->setText("Spendable Amount: " + CUtils::microPAIToPAI6(getModelWalletCoins()->m_trx_spendable_amount));
}

QPalette CGUI::getErrorPalette()
{
  QPalette sample_palette;
  sample_palette.setColor(QPalette::Window, Qt::red);
  sample_palette.setColor(QPalette::WindowText, Qt::black);
  return sample_palette;
}

QPalette CGUI::getNormalPalette()
{
  QPalette sample_palette;
  sample_palette.setColor(QPalette::Window, Qt::green);
  sample_palette.setColor(QPalette::WindowText, Qt::black);
  return sample_palette;
}

void CGUI::hideContributeVoteForm()
{
  get().m_model_contributes->m_is_visible_vote_form = false;
  get().m_ui->groupBox_voting_form->setFixedHeight(0);
  get().m_ui->groupBox_voting_form->setFixedWidth(0);
}

void CGUI::signalUpdateBlockBuffer(){
  get().m_model_block_buffer->signalUpdate();
}


void CGUI::signalUpdateAdmPollings(){
  get().m_model_adm_pollings->signalUpdate();
}

void CGUI::signalUpdateSocietyPollings(){
  get().m_model_society_pollings->signalUpdate();
}

void CGUI::initAgorasCategoriesCombo(QComboBox* obj, const QString& selected_iname)
{
  obj->clear();

  if (selected_iname == "")
  {
    return;
  }

  obj->addItem("Root", "");

  QVDRecordsT indented_agoras = DemosHandler::makeTitlesTree(selected_iname);
  for (QVDicT an_agora: indented_agoras)
    obj->addItem(an_agora.value("ag_title").toString(), an_agora.value("ag_hash").toString());

}

void CGUI::initINamesCombo(QComboBox* obj, bool include_imagine)
{
  obj->clear();

  if (include_imagine)
    obj->addItem("imagine", "imagine");

  auto[res_status, res_msg, inames, bindings_info] = INameHandler::getMachineControlledINames(
    {},
    {"imn_owner", "imn_name", "imn_hash", "imn_register_date"},
    {{"imn_name", "ASC"}},
    false);
  if (res_status)
    for (QVDicT an_iname: inames)
      obj->addItem(an_iname["imn_name"].toString(), an_iname["imn_name"].toString());
}

void CGUI::initWalletControlledAddressesCombo(QComboBox* obj)
{
  auto[wallet_controlled_accounts, details] = Wallet::getAddressesList(
    CMachine::getSelectedMProfile(),
    Wallet::stbl_machine_wallet_addresses_fields,
    false);

  for (QVDicT an_address: wallet_controlled_accounts)
    obj->addItem(an_address.value("wa_title").toString() + " (" + CUtils::shortBech16(an_address.value("wa_address").toString()) + ")", an_address.value("wa_address").toString());
}

