#include "stable.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lib/services/polling/ballot_handler.h"

#include "lib/services/society_rules/society_rules.h"

void societyPollingsDblClicked(const QModelIndex& idx)
{
  CGUI::getModelSocietyPollings()->societyPollingsDblClicked(idx);
}

void MainWindow::connectSocietyPollingsToModel()
{
  ModelSocietyPollings* mdl = new ModelSocietyPollings(this);
  CGUI::setModelSocietyPollings(mdl);
  mdl->populateData();
  ui->tableView_society_pollings->setModel(mdl);
  ui->tableView_society_pollings->setColumnWidth(0, 1);
  ui->tableView_society_pollings->setColumnWidth(1, 21);
  ui->tableView_society_pollings->resizeColumnsToContents();
  connect(ui->tableView_society_pollings, &QListView::doubleClicked, societyPollingsDblClicked);
}

void MainWindow::connectAdmPollingsToModel()
{
  ModelAdmPollings* mdl = new ModelAdmPollings(this);
  CGUI::setModelAdmPollings(mdl);
  mdl->populateData();
  ui->tableView_adm_pollings->setModel(mdl);
  ui->tableView_adm_pollings->setColumnWidth(0, 1);
  ui->tableView_adm_pollings->setColumnWidth(1, 21);
  ui->tableView_adm_pollings->resizeColumnsToContents();

  connect(
    ui->tableView_adm_pollings,
    SIGNAL(doubleClicked(const QModelIndex &)),
    this,
    SLOT(dspAdmPollings(const QModelIndex &)));

}

void MainWindow::dspAdmPollings(const QModelIndex &index)
{
  if (index.isValid())
  {
    ui->widget_admpolling_form->setFixedHeight(155);
    QJsonObject the_adm = CGUI::getModelAdmPollings()->m_adm_pollings[index.row()].toObject();
    QString adm_polling_type = the_adm.value("key").toString();
    CGUI::getModelAdmPollings()->m_selected_adm_polling_type = adm_polling_type;
    ui->label_adm_pollig_noti_msg->setText("Start a Polling For " + the_adm.value("label").toString());

    QJsonObject pollin_values = the_adm.value("pValues").toObject();
    ui->lineEdit_admpolling_timeframe->setText(QString::number(pollin_values.value("pTimeframe").toDouble()));

    if (QStringList{POLLING_TYPES::RFRfPLedgePrice,
    POLLING_TYPES::RFRfPollingPrice,
    POLLING_TYPES::RFRfTxBPrice,
    POLLING_TYPES::RFRfClPLedgePrice,
    POLLING_TYPES::RFRfDNAPropPrice,
    POLLING_TYPES::RFRfBallotPrice,
    POLLING_TYPES::RFRfINameRegPrice,
    POLLING_TYPES::RFRfINameBndPGPPrice,
    POLLING_TYPES::RFRfINameMsgPrice,
    POLLING_TYPES::RFRfFPostPrice,
    POLLING_TYPES::RFRfBasePrice,
    POLLING_TYPES::RFRfBlockFixCost}.contains(adm_polling_type))
    {
      ui->label_admpolling_value_type1->setText("Fee: ");
      ui->label_admpolling_value_type2->setText(" micro PAIs");
      double pFee = pollin_values.value("pFee").toDouble();
      ui->lineEdit_admpolling_value->setText(QString::number(pFee));
    } else if (QStringList{POLLING_TYPES::RFRfMinS2Wk,
      POLLING_TYPES::RFRfMinS2DA,
      POLLING_TYPES::RFRfMinS2V,
      POLLING_TYPES::RFRfMinFSign,
      POLLING_TYPES::RFRfMinFVote}.contains(adm_polling_type))
    {
      ui->label_admpolling_value_type1->setText("Minimum Shares: ");
      ui->label_admpolling_value_type2->setText(" Percent");
      double min_shares = pollin_values.value("pShare").toDouble();
      ui->lineEdit_admpolling_value->setText(CUtils::convertFloatToString(min_shares));

    }
  }
}


void MainWindow::on_pushButton_pushAdmPolling_clicked()
{
  if (CGUI::getModelAdmPollings()->m_selected_adm_polling_type == "")
  {
    CGUI::displayMsg(ui->label_adm_pollig_noti_msg, "No Administrative polling is selected", false);
    return;
  }

  QString adm_polling_type = ui->comboBox_wiki_domain->currentData().toString();
  double admpolling_timeframe = QVariant::fromValue(ui->lineEdit_admpolling_timeframe->text()).toDouble();
  double admpolling_value = QVariant::fromValue(ui->lineEdit_admpolling_value->text()).toDouble();
  auto[status, msg] = SocietyRules::createAPollingFor(
    CGUI::getModelAdmPollings()->m_selected_adm_polling_type,
    admpolling_timeframe,
    admpolling_value);
 CGUI::displayMsg(ui->label_adm_pollig_noti_msg, msg, status);

 if (status)
   CGUI::signalUpdateBlockBuffer();
}


void MainWindow::on_pushButton_refreshSocietyPollings_clicked()
{
  CGUI::signalUpdateSocietyPollings();
  CGUI::signalUpdateAdmPollings();
}

void CGUI::toggleSocietyVoteForm(const CDocHashT& adm_polling_hash)
{
  // hide Ballot details view


  if (getModelSocietyPollings()->m_selected_adm_polling == adm_polling_hash)
  {
    get().m_model_society_pollings->m_is_visible_adm_vote_form = !get().m_model_society_pollings->m_is_visible_adm_vote_form;
  }else{
    get().m_model_society_pollings->m_is_visible_adm_vote_form = true;
  }

  getModelSocietyPollings()->m_selected_adm_polling = adm_polling_hash;


  if (get().m_model_society_pollings->m_is_visible_adm_vote_form )
  {
    get().m_ui->widget_adm_vote_form->setFixedWidth(311);
    get().m_ui->widget_adm_vote_form->setFixedHeight(555);
    QVDRecordsT adm_pollings = SocietyRules::searchInAdmPollings(
      {{"apr_hash", getModelSocietyPollings()->m_selected_adm_polling}});
    if (adm_pollings.size() == 1)
    {
      displayMsg(get().m_ui->label_admPollVoteNotiMsg, adm_pollings[0].value("apr_comment").toString(), true);
    }else{
      displayMsg(get().m_ui->label_admPollVoteNotiMsg, "Society polling does not exist!", false);
    }

  }else{
    hideAdmPollingVoteForm();
  }
}

void CGUI::hideAdmPollingVoteForm()
{
  get().m_model_society_pollings->m_is_visible_adm_vote_form = false;
  get().m_ui->widget_adm_vote_form->setFixedHeight(0);
  get().m_ui->widget_adm_vote_form->setFixedWidth(0);
}

void MainWindow::on_pushButton_vote_adm_polling_clicked()
{
  QString post_content = ui->lineEdit_admPollingVoteComment->text();

  auto[vote_status, mote_msg] = BallotHandler::doVoteForAdmPolling(
    CGUI::getModelSocietyPollings()->m_selected_adm_polling,
    CGUI::getModelSocietyPollings()->m_adm_polling_score,
    post_content);
  CGUI::displayMsg(ui->label_admPollVoteNotiMsg, mote_msg, vote_status);
  if (vote_status)
    CGUI::signalUpdateBlockBuffer();

}

void MainWindow::on_horizontalSlider_adm_polling_valueChanged(int value)
{
  CGUI::getModelSocietyPollings()->m_adm_polling_score = value;
  if ( value == 0)
  {
    ui->pushButton_vote_adm_polling->setText("Send (Abstain)");
  }else if ( value > 0) {
    ui->pushButton_vote_adm_polling->setText("Send (" + QString::number(CGUI::getModelSocietyPollings()->m_adm_polling_score) + "% Agree)");
  }else{
    ui->pushButton_vote_adm_polling->setText("Send (" + QString::number(-CGUI::getModelSocietyPollings()->m_adm_polling_score) + "% Disagree)");
  }
}
