#include "stable.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contribute/contribute_handler.h"
#include "lib/services/contracts/loan/loan_contract_handler.h"

void receivedLoanRequestDblClicked(const QModelIndex& idx)
{
  CGUI::getModelReceivedLoanRequests()->receivedLoanRequestDblClicked(idx);
}

void MainWindow::connectReceivedLoanRequestsToModel()
{
  ModelReceivedLoanRequests* mdl = new ModelReceivedLoanRequests(this);
  CGUI::setModelReceivedLoanRequests(mdl);
  mdl->populateData();
  ui->tableView_receivedLoanRequests->setModel(mdl);
  ui->tableView_receivedLoanRequests->setColumnWidth(0, 1);
  ui->tableView_receivedLoanRequests->setColumnWidth(1, 21);
  ui->tableView_receivedLoanRequests->resizeColumnsToContents();
  connect(ui->tableView_receivedLoanRequests, &QListView::doubleClicked, receivedLoanRequestDblClicked);
}

void proposalPledgeTransactionsDblClicked(const QModelIndex& idx)
{
  CGUI::getModelProposalPledgeTransactions()->proposalPledgeTransactionsDblClicked(idx);
}

void MainWindow::connectProposalPledgeTransactionsToModel()
{
  ModelProposalPledgeTransactions* mdl = new ModelProposalPledgeTransactions(this);
  CGUI::setModelProposalPledgeTransactions(mdl);
  mdl->populateData();
  ui->tableView_pppBundles->setModel(mdl);
  ui->tableView_pppBundles->setColumnWidth(0, 1);
  ui->tableView_pppBundles->setColumnWidth(1, 21);
  ui->tableView_pppBundles->resizeColumnsToContents();
  connect(ui->tableView_pppBundles, &QListView::doubleClicked, proposalPledgeTransactionsDblClicked);
}

void machineContractsDblClicked(const QModelIndex& idx)
{
  CGUI::getModelMachineContracts()->machineContractsDblClicked(idx);
}

void MainWindow::connectMachineContractsToModel()
{
  ModelMachineContracts* mdl = new ModelMachineContracts(this);
  CGUI::setModelMachineContracts(mdl);
  mdl->populateData();
  ui->tableView_machinesOnchainContracts->setModel(mdl);
  ui->tableView_machinesOnchainContracts->setColumnWidth(0, 1);
  ui->tableView_machinesOnchainContracts->setColumnWidth(1, 21);
  ui->tableView_machinesOnchainContracts->resizeColumnsToContents();
  connect(ui->tableView_machinesOnchainContracts, &QListView::doubleClicked, machineContractsDblClicked);
}

void MainWindow::on_pushButton_contractsInfo_clicked()
{
  CGUI::signalUpdateReceivedLoanRequests();
  CGUI::signalUpdateProposalPledgeTransactions();
  CGUI::signalUpdateMachineContracts();
}
