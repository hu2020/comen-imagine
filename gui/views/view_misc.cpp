#include "stable.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lib/utils/excel_reporter/excel_reporter.h"

void MainWindow::connectBlockBufferToModel()
{
  ModelBlockBuffer* mdl = new ModelBlockBuffer(this);
  CGUI::setModelBlockBuffer(mdl);
  mdl->populateData();
  ui->tableView_machineBlockBuffer->setModel(mdl);
  ui->tableView_machineBlockBuffer->setColumnWidth(0, 1);
  ui->tableView_machineBlockBuffer->setColumnWidth(1, 21);
  ui->tableView_machineBlockBuffer->resizeColumnsToContents();

  connect(
    ui->tableView_machineBlockBuffer,
    SIGNAL(doubleClicked(const QModelIndex &)),
    this,
    SLOT(removeFromBuffer(const QModelIndex &)));

}


void MainWindow::removeFromBuffer(const QModelIndex &index)
{
  if (index.isValid())
  {
    auto m = index.model();
    CDocHashT hash = m->index(index.row(), 2).data().toString();

    if (index.column() == 6)
    {
      CMachine::removeFromBuffer({{"bd_doc_hash", hash + "%", "LIKE"}});
      CGUI::signalUpdateBlockBuffer();
    }
  }
}

void MainWindow::on_pushButton_refreshBlockBuffer_clicked()
{
  CGUI::signalUpdateBlockBuffer();
}

void MainWindow::on_pushButton_flushBlockBuffer_clicked()
{
  auto[status, msg] = CMachine::broadcastBlock();
  CGUI::displayMsg(ui->label_miscNotiMsg, msg, status);
  CGUI::signalUpdateBlockBuffer();
}

void MainWindow::on_pushButton_generate_transactions_list_clicked()
{
  auto[status, msg] = ExcelReporter::downloadTransactionsList();
}

