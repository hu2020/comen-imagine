#include "stable.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"

void wikiPageSelected(const QModelIndex& index)
{
  CGUI::wikiPageSelected(index);
}

void MainWindow::connectWikiTitlesToModel()
{
  ModelWikiTitles* mdl = new ModelWikiTitles(this);
  CGUI::setModelWikiTitles(mdl);
  mdl->populateData();
  ui->tableView_wikiTitles->setModel(mdl);
  ui->tableView_wikiTitles->setColumnWidth(0, 1);
  ui->tableView_wikiTitles->setColumnWidth(1, 21);
  ui->tableView_wikiTitles->resizeColumnsToContents();
  connect(ui->tableView_wikiTitles, &QListView::doubleClicked, wikiPageSelected);

  CGUI::hideWikiEditForm();
  CGUI::hideWikiRegForm();
}

void CGUI::showWikiEditForm()
{
  CGUI::getModelWikiTitles()->m_is_visible_wiki_edit_form = true;
  get().m_ui->widget_wiki_edit_form->setFixedHeight(370);
}

void CGUI::hideWikiEditForm()
{
  CGUI::getModelWikiTitles()->m_is_visible_wiki_edit_form = false;
  get().m_ui->widget_wiki_edit_form->setFixedHeight(0);
}

void CGUI::showWikiRegForm()
{
  CGUI::getModelWikiTitles()->m_is_visible_wiki_reg_form = true;
  get().m_ui->widget_wiki_reg_form->setFixedHeight(370);
}

void CGUI::hideWikiRegForm()
{
  CGUI::getModelWikiTitles()->m_is_visible_wiki_reg_form = false;
  get().m_ui->widget_wiki_reg_form->setFixedHeight(0);
}

void CGUI::initLanguagesCombo(QComboBox* obj)
{
  obj->addItem("", "");
  for (QSDicT a_lang: MultiLanguage::s_languages)
    obj->addItem(a_lang.value("enName") + "(" + a_lang.value("lName") + ")", a_lang.value("iso"));
}

void MainWindow::on_pushButton_edit_wiki_page_clicked()
{
  if (!CGUI::getModelWikiTitles()->m_is_visible_wiki_edit_form)
  {
    CGUI::hideWikiRegForm();
    if (CGUI::getModelWikiTitles()->m_selected_wiki.keys().size() > 0)
    {
      CGUI::showWikiEditForm();
      QVDicT selected_wiki = CGUI::getModelWikiTitles()->m_selected_wiki;
      QString page_title = selected_wiki.value("wkp_title").toString();
      ui->label_wiki_edit_noti_msg->setText("Editing wiki page \"" + page_title + "\"");
      ui->label_wiki_domain->setText("iName (domain): " + selected_wiki.value("wkp_iname").toString());
      QSDicT lang_info = MultiLanguage::mapLangCodeToLangName(selected_wiki.value("wkp_language").toString());
      ui->label_wiki_language->setText("Language: " + lang_info.value("enName") + "(" + lang_info.value("lName") + ")");
      QVDRecordsT pages = WikiHandler::searchInWikiContents({{"wkc_wkp_hash", selected_wiki.value("wkp_hash").toString()}});
      if (pages.size() != 1)
      {
        CGUI::displayMsg(ui->label_wiki_edit_noti_msg, "This wiki page doesn't exist!", false);
        return;
      }
      ui->textEdit_wiki_edit_page_content->setText(pages[0].value("wkc_content").toString());

    }

  } else {
    CGUI::hideWikiEditForm();
  }

}

void MainWindow::on_pushButton_submit_edited_wiki_page_clicked()
{
  QVDicT selected_wiki = CGUI::getModelWikiTitles()->m_selected_wiki;
  CDocHashT wiki_page_hash = selected_wiki.value("wkp_hash").toString();
  QString page_title = selected_wiki.value("wkp_title").toString();
  QString wiki_iname = selected_wiki.value("wkp_iname").toString();
  QString wiki_lang = selected_wiki.value("wkp_language").toString();
  QString page_content = ui->textEdit_wiki_edit_page_content->toPlainText();

  if (wiki_page_hash == "")
  {
    CGUI::displayMsg(ui->label_wiki_edit_noti_msg, "Missed wiki page hash to edit!", false);
    return;
  }

  auto[status, msg] = WikiHandler::createNewWikiPage(
    page_title,
    page_content,
    wiki_page_hash,
    wiki_lang,
    wiki_iname);
  CGUI::displayMsg(ui->label_wiki_edit_noti_msg, msg, status);

  if (status)
  {
    CGUI::signalUpdateBlockBuffer();
    CGUI::hideWikiEditForm();
  }

}


void MainWindow::on_pushButton_toggle_wiki_form_clicked()
{
  // deselect wiki page (if already selected one)
  CGUI::getModelWikiTitles()->m_selected_wiki = {};

  if (!CGUI::getModelWikiTitles()->m_is_visible_wiki_reg_form)
  {
    CGUI::hideWikiEditForm();
    CGUI::showWikiRegForm();
  } else {
    CGUI::hideWikiRegForm();
  }
}

void MainWindow::on_pushButton_submit_wiki_page_clicked()
{
  QString wiki_iname = ui->comboBox_wiki_domain->currentData().toString();
  QString lang = ui->comboBox_wiki_languages->currentData().toString();
  QString page_title = ui->lineEdit_wiki_page_title->text();
  QString page_content = ui->textEdit_wiki_page_content->toPlainText();

  if (lang == "")
  {
    CGUI::displayMsg(ui->label_wiki_page_noti_msg, "Missed wiki language", false);
    return;
  }
  if (lang != "eng")
  {
    CGUI::displayMsg(ui->label_wiki_page_noti_msg, "At the moment only English language is supported", false);
    return;
  }

  if (page_title == "")
  {
    CGUI::displayMsg(ui->label_wiki_page_noti_msg, "Missed wiki page title", false);
    return;
  }

  if (page_content == "")
  {
    CGUI::displayMsg(ui->label_wiki_page_noti_msg, "Missed wiki page content", false);
    return;
  }

  auto[status, msg] = WikiHandler::createNewWikiPage(
    page_title,
    page_content,
    "",
    lang,
    wiki_iname);
  CGUI::displayMsg(ui->label_wiki_page_noti_msg, msg, status);

  if (status)
  {
    CGUI::signalUpdateBlockBuffer();
    ui->lineEdit_wiki_page_title->setText("");
    ui->textEdit_wiki_page_content->setText("");
  }
}

void CGUI::wikiPageSelected(const QModelIndex& index)
{
  getModelWikiTitles()->m_selected_wiki = getModelWikiTitles()->m_wiki_titles[index.row()];
  QString page_title = getModelWikiTitles()->m_selected_wiki.value("wkp_title").toString();
  QString rendered_content = WikiHandler::renderPage(page_title);
  CLog::log("Rendered wiki page content: " + rendered_content);
  get().m_ui->textBrowser_wikiPage->setHtml(rendered_content);
}
