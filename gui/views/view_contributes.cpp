#include "stable.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lib/services/polling/ballot_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contribute/contribute_handler.h"
#include "lib/services/contracts/loan/loan_contract_handler.h"

void CGUI::toggleContributeVoteForm(const CDocHashT& proposal_hash)
{
  // hide Ballot details view


  if (getModelContributes()->m_selected_proposal == proposal_hash)
  {
    get().m_model_contributes->m_is_visible_vote_form = !get().m_model_contributes->m_is_visible_vote_form;
  }else{
    get().m_model_contributes->m_is_visible_vote_form = true;
  }

  getModelContributes()->m_selected_proposal = proposal_hash;


  if (get().m_model_contributes->m_is_visible_vote_form )
  {
    get().m_ui->groupBox_voting_form->setFixedWidth(155);
    get().m_ui->groupBox_voting_form->setFixedHeight(333);
    QVDRecordsT proposals = ProposalHandler::searchInProposals({{"pr_hash", getModelContributes()->m_selected_proposal}});
    if (proposals.size() == 1)
    {
      displayMsg(get().m_ui->label_voteNotiMsg, proposals[0].value("pr_title").toString(), true);
    }else{
      displayMsg(get().m_ui->label_voteNotiMsg, "Proposal/Contribution does not exist! ", false);
    }


  }else{
    hideContributeVoteForm();

  }
}

void MainWindow::on_pushButton_voteProposal_clicked()
{
  QString vote_comment = ui->lineEdit_proposalVoteComment->text();
  double the_vote_value = CGUI::getModelContributes()->m_polling_score;

  auto[res, msg] = BallotHandler::doVoteForProposal(
    CGUI::getModelContributes()->m_selected_proposal,
    the_vote_value,
    vote_comment);

  CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, msg, res);

  if (res)
  {
    CGUI::signalUpdateContributes();
    CGUI::signalUpdateBlockBuffer();
    CGUI::signalUpdateWalletCoins();
    CGUI::signalUpdateWalletAccounts();
  }
}

void hideLoanRequests()
{
  CGUI::get().m_ui->tableView_loanRequests->setFixedHeight(0);
  CGUI::get().m_model_draft_proposals->m_is_visible_loan_requests_list = false;
}

void hideLoanResuestForm()
{
  CGUI::get().m_ui->scrollAreaWidgetContents_loanRequestForm->setFixedHeight(0);
  CGUI::get().m_ui->scrollAreaWidgetContents_loanRequestForm->setMinimumHeight(0);
  CGUI::get().m_ui->scrollAreaWidgetContents_loanRequestForm->setMaximumHeight(0);
  CGUI::get().m_model_draft_proposals->m_is_visible_loan_request_form = false;
}

void hideSendLoanRequest()
{
  CGUI::get().m_ui->tableView_sendLoanRequest->setFixedHeight(0);
  CGUI::get().m_model_draft_proposals->m_is_visible_send_loan_request = false;
}

void CGUI::showSendLoanRequest()
{
  hideLoanRequests();
  hideLoanResuestForm();

  get().m_ui->tableView_sendLoanRequest->setFixedHeight(99);
  get().m_model_draft_proposals->m_is_visible_send_loan_request = true;
  // get().getModelSendLoanRequest()->
  signalUpdateSendLoanRequest();
}


void contreibuteDblClicked(const QModelIndex& idx)
{
  CGUI::getModelContributes()->contreibuteDblClicked(idx);
}

void draftProposalDblClicked(const QModelIndex& idx)
{
  CGUI::getModelDraftProposal()->draftProposalDblClicked(idx);
}

void MainWindow::on_pushButton_refreshContributesInfo_clicked()
{
  CGUI::signalUpdateContributes();
}

void MainWindow::connectDraftProposalsToModel()
{
  ModelDraftProposals* mdl = new ModelDraftProposals(this);
  CGUI::setModelDraftProposals(mdl);
  mdl->populateData();
  ui->tableView_draftProposals->setModel(mdl);
  ui->tableView_draftProposals->setColumnWidth(0, 1);
  ui->tableView_draftProposals->setColumnWidth(1, 21);
  ui->tableView_draftProposals->resizeColumnsToContents();
  connect(ui->tableView_draftProposals, &QListView::doubleClicked, draftProposalDblClicked);
}

void draftPledgeDblClicked(const QModelIndex& idx)
{
  CGUI::getModelDraftPledges()->draftPledgeDblClicked(idx);
}


void MainWindow::connectDraftPledgesToModel()
{
  ModelDraftPledges* mdl = new ModelDraftPledges(this);
  CGUI::setModelDraftPledges(mdl);
  mdl->populateData();
  ui->tableView_loanRequests->setModel(mdl);
  ui->tableView_loanRequests->setColumnWidth(0, 1);
  ui->tableView_loanRequests->setColumnWidth(1, 21);
  ui->tableView_loanRequests->resizeColumnsToContents();
  connect(ui->tableView_loanRequests, &QListView::doubleClicked, draftPledgeDblClicked);
}

void sendLoanRequestDblClicked(const QModelIndex& idx)
{
  CGUI::getModelSendLoanRequest()->sendLoanRequestDblClicked(idx);
}

void MainWindow::connectSendLoanRequestToModel()
{
  ModelSendLoanRequest* mdl = new ModelSendLoanRequest(this);
  CGUI::setModelSendLoanRequest(mdl);
  mdl->populateData();
  ui->tableView_sendLoanRequest->setModel(mdl);
  ui->tableView_sendLoanRequest->setColumnWidth(0, 1);
  ui->tableView_sendLoanRequest->setColumnWidth(1, 21);
  ui->tableView_sendLoanRequest->resizeColumnsToContents();
  connect(ui->tableView_sendLoanRequest, &QListView::doubleClicked, sendLoanRequestDblClicked);
}

void MainWindow::connectContributesToModel()
{
  ModelContributes* mdl = new ModelContributes(this);
  CGUI::setModelContributes(mdl);
  mdl->populateData();
  ui->tableView_contributes->setModel(mdl);
  ui->tableView_contributes->setColumnWidth(0, 1);
  ui->tableView_contributes->setColumnWidth(1, 21);
  ui->tableView_contributes->resizeColumnsToContents();
  connect(ui->tableView_contributes, &QListView::doubleClicked, contreibuteDblClicked);


  on_pushButton_resetProposalForm_clicked();

  // contribute usefullness level
  for (int i=1; i<13; i++)
    ui->comboBox_contributeUsefulness->addItem("level " + QString::number(i), i);
  // ui->comboBox_contributeUsefulness->setCurrentIndex(6);

  // contribute dedicated hours
  for (int i=1; i<31; i++)
    ui->comboBox_contributeHours->addItem(QString::number(i) + " Hours", i);
  for (int i: CGUI::getModelContributes()->m_contribute_hours_combo)
    ui->comboBox_contributeHours->addItem(QString::number(i) + " Hours", i);

  // polling timeframe
  std::vector<QStringList> polling_time_span {
    {"24 Hours", "24"},
    {"48 Hours (2 days)", "48"},
    {"3 Days (72 Hours)", "72"},
    {"4 Days (96 Hours)", "96"},
    {"5 Days (120 Hours)", "120"},
    {"6 Days (144 Hours)", "144"},
    {"7 Days (168 Hours)", "168"},
    {"10 Days (240 Hours)", "240"},
    {"14 Days (336 Hours)", "336"},
    {"15 Days (360 Hours)", "360"},
    {"20 Days (480 Hours)", "480"},
    {"1 Month (30 Days)", "720"},
    {"2 Monthes (60 Days)", "1440"},
    {"3 Monthes (90 Days)", "2160"},
    {"4 Monthes (120 Days)", "2880"},
    {"6 Monthes (180 Days)", "4320"},
    {"1 Year (365 Days)", "8760"},
    {"2 Years", "17520"},
  };
  for (QStringList tuple: polling_time_span)
    ui->comboBox_proposalPollingTimeframe->addItem(tuple[0], tuple[1]);

}

void MainWindow::on_comboBox_contributeHours_currentIndexChanged(int index)
{
  if (index < 30)
  {
    CGUI::getModelContributes()->m_contribute_hours = index + 1;

  }else{
    CGUI::getModelContributes()->m_contribute_hours = CGUI::getModelContributes()->m_contribute_hours_combo[index-30];
  }
  CLog::log("============CGUI::getModelContributes()->m_contribute_hours =>" + QString::number(CGUI::getModelContributes()->m_contribute_hours));

  refreshProposalCost();
}

void MainWindow::on_comboBox_contributeUsefulness_currentIndexChanged(int index)
{
  CLog::log("USefulness level => " + QString::number(index));
  CGUI::getModelContributes()->m_contribute_level = index+1;

  refreshProposalCost();
}

void MainWindow::refreshProposalCost()
{
  int64_t the_contribute = CGUI::getModelContributes()->m_contribute_level * CGUI::getModelContributes()->m_contribute_hours;
  auto[one_cycle_income, apply_cost] = ProposalHandler::calcProposalApplyCost(
    CGUI::getModelContributes()->m_contribute_hours,
    CGUI::getModelContributes()->m_contribute_level,
    CUtils::getNow());

//  FutureIncomes future_incomes = CoinbaseIssuer::predictFutureIncomes(
//    the_contribute,
//    CUtils::getNow(),
//    1);
  ui->label_proposalCost->setText("Proposal cost for (" + QString::number(CGUI::getModelContributes()->m_contribute_hours) + " Hour) * (Level " + QString::number(CGUI::getModelContributes()->m_contribute_level) + ") = (" + QString::number(the_contribute) + " Shares) => " + CUtils::microPAIToPAI6(apply_cost) + " PAIs");
}

void MainWindow::on_lineEdit_loanRequestIntrestRate_textChanged(const QString& amount)
{
  CGUI::get().m_model_draft_proposals->m_interest_rate = QVariant::fromValue(amount).toDouble();
  CGUI::refreshLoanReqSummary();
}

void MainWindow::on_lineEdit_loanRepaymentAmount_textChanged(const QString& amount)
{
  CGUI::get().m_model_draft_proposals->m_repayment_amount = QVariant::fromValue(amount).toUInt();
  CGUI::refreshLoanReqSummary();
}


void MainWindow::on_pushButton_prepareProposal_clicked()
{
  QString proposal_title = ui->lineEdit_proposalTitle->text();
  if (proposal_title == "")
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Missed proposal title!", false);
    return;
  }

  QString proposal_comment = ui->lineEdit_proposalComment->text();
  if (proposal_comment == "")
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Missed proposal comment!", false);
    return;
  }

  QString proposal_tags = ui->lineEdit_proposalTags->text();
  if (proposal_tags == "")
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Missed proposal tags!", false);
    return;
  }

  CAddressT contributer_address = ui->lineEdit_proposalContributer->text();
  if (contributer_address == "")
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Missed contributor's address!", false);
    return;
  }

  uint64_t contribute_hours = CGUI::getModelContributes()->m_contribute_hours;
  uint64_t contribute_level = CGUI::getModelContributes()->m_contribute_level;
  double voting_timeframe = ui->comboBox_proposalPollingTimeframe->currentData().toDouble();

  auto[res, msg] = ProposalHandler::prepareDraftProposal(
    contributer_address,
    proposal_title,
    proposal_comment,
    proposal_tags,
    contribute_hours,
    contribute_level,
    voting_timeframe);

  CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, msg, res);

  CGUI::signalUpdateDraftProposals();
}

void MainWindow::on_pushButton_resetProposalForm_clicked()
{
  ui->lineEdit_proposalTitle->setText("");
  ui->lineEdit_proposalComment->setText("");
  ui->lineEdit_proposalTags->setText("");
  ui->label_proposalCost->setText("Proposal Cost: 0 PAIs");
  ui->lineEdit_proposalContributer->setText(CMachine::getBackerAddress());

}

void MainWindow::on_pushButton_closeLoanRequestForm_clicked()
{
  hideLoanResuestForm();
}

void MainWindow::on_pushButton_signLoanRequest_clicked()
{
  CDocHashT proposal_hash = CGUI::getModelDraftProposal()->m_selected_proposal;

  CAddressT pledgee_address = ui->lineEdit_loanRequestPledgee->text();
  if (pledgee_address == "")
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Missed pledgee address for loan request proposal(" + proposal_hash + ")", false);
    return;
  }
  CAddressT arbiter_address = ui->lineEdit_loanRequestArbiter->text();

  double annual_interst = QVariant::fromValue(ui->lineEdit_loanRequestIntrestRate->text()).toDouble();
  if (annual_interst == 0.0)
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Annual interst can not be zero for loan request proposal(" + proposal_hash + ")", false);
    return;
  }

  double repayment_amount = QVariant::fromValue(ui->lineEdit_loanRepaymentAmount->text()).toDouble();
  if (repayment_amount < 1)
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Repayment amount can not be zero for loan request proposal(" + proposal_hash + ")", false);
    return;
  }

  QVDRecordsT proposals = ProposalHandler::searchInDraftProposals({{"pd_hash", proposal_hash}});
  if (proposals.size() == 0)
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Invalid proposal requested for loan proposal(" + proposal_hash + ")", false);
    return;
  }

  CAddressT contributer_address = proposals[0].value("pd_contributor_account").toString();
  uint64_t contributed_hours = proposals[0].value("pd_help_hours").toInt();
  uint64_t contributed_level = proposals[0].value("pd_help_level").toInt();

  auto[one_cycle_income, apply_cost] = ProposalHandler::calcProposalApplyCost(
    contributed_hours,
    contributed_level,
    CUtils::getNow());

  DNAProposalDocument* proposal = new DNAProposalDocument(CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(proposals[0].value("pd_body").toString()).content));
  auto[status, proposal_dp_cost] = proposal->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    CUtils::getNow());
  if (!status)
  {

  }
  delete proposal;

  CMPAIValueT full_costs = apply_cost + (proposal_dp_cost * 2);

  LoanDetails loan_details = LoanContractHandler::calcLoanRepayments(
    full_costs,
    annual_interst,
    repayment_amount);
  if (!loan_details.m_calculation_status)
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Failed in loan details calculation for loan request proposal(" + CUtils::hash8c(proposal_hash) + ")", false);
    return;
  }

  bool res = ContributeHandler::bindProposalLoanPledge(
    proposal_hash,
    contributer_address,
    pledgee_address,
    full_costs,
    annual_interst,
    repayment_amount,
    CConsts::DEFAULT_REPAYMENT_SCHEDULE,
    arbiter_address);

  if (!res)
  {
    CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, "Failed in bind Proposal Loan Pledge for loan request proposal(" + CUtils::hash8c(proposal_hash) + ")", false);
    return;
  }

  CGUI::displayMsg(
    CGUI::get().m_ui->label_proposalNotifyMsg,
    "Loan request is created and recorded in draft. proposal(" + CUtils::hash8c(proposal_hash) + ")",
    true);
  CGUI::signalUpdateDraftPledges();
}



void CGUI::initDraftProposalFormsDisplay()
{
  // hiding loan requests List
  hideLoanRequests();
  hideSendLoanRequest();
  hideLoanResuestForm();

  // hiding loan request form
  get().m_ui->label_loanReqNotifyMsg->setAlignment(Qt::AlignCenter);
}

void CGUI::toggleLoanRequestForm(const CDocHashT& doc_hash)
{

  // hide loan requests
  hideLoanRequests();
  hideSendLoanRequest();

  if (CGUI::getModelDraftProposal()->m_selected_proposal == doc_hash)
  {
    get().m_model_draft_proposals->m_is_visible_loan_request_form = !get().m_model_draft_proposals->m_is_visible_loan_request_form;
  }else{
    get().m_model_draft_proposals->m_is_visible_loan_request_form = true;
  }

  CGUI::getModelDraftProposal()->m_selected_proposal = doc_hash;
  QVDRecordsT proposals = ProposalHandler::searchInDraftProposals({{"pd_hash", doc_hash}});
  if (proposals.size() > 0)
  {
    CGUI::getModelDraftProposal()->m_contributed_hours = proposals[0].value("pd_help_hours").toInt();
    CGUI::getModelDraftProposal()->m_contributed_level = proposals[0].value("pd_help_level").toInt();
    CGUI::getModelDraftProposal()->m_selected_proposal_title = proposals[0].value("pd_title").toString();
  }else{
    CGUI::getModelDraftProposal()->m_contributed_hours = 0;
    CGUI::getModelDraftProposal()->m_contributed_level = 0;
  }

  get().m_ui->lineEdit_loanRequestIntrestRate->setText(QString::number(CGUI::get().m_model_draft_proposals->m_interest_rate));
  get().m_ui->lineEdit_loanRepaymentAmount->setText(QString::number(CGUI::get().m_model_draft_proposals->m_repayment_amount));

  DNAProposalDocument* proposal = new DNAProposalDocument(CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(proposals[0].value("pd_body").toString()).content));
  auto[status, proposal_dp_cost] = proposal->calcDocDataAndProcessCost(
    CConsts::STAGES::Creating,
    CUtils::getNow());
  if (!status)
  {

  }
  delete proposal;

  auto[one_cycle_income, apply_cost] = ProposalHandler::calcProposalApplyCost(
    CGUI::getModelDraftProposal()->m_contributed_hours,
    CGUI::getModelDraftProposal()->m_contributed_level,
    CUtils::getNow());
  CGUI::getModelDraftProposal()->m_loan_principals = apply_cost + (proposal_dp_cost * 2);

  // roughly set repayments amount
  CGUI::getModelDraftProposal()->m_repayment_amount = static_cast<CMPAIValueT>(apply_cost/CConsts::DEFAULT_REPAYMENT_SCHEDULE);
  get().m_ui->lineEdit_loanRepaymentAmount->setText(QString::number(CGUI::get().m_model_draft_proposals->m_repayment_amount));



  if (get().m_model_draft_proposals->m_is_visible_loan_request_form)
  {
    get().m_ui->scrollAreaWidgetContents_loanRequestForm->setFixedHeight(199);
    get().m_ui->scrollAreaWidgetContents_loanRequestForm->setMinimumHeight(199);
    get().m_ui->scrollAreaWidgetContents_loanRequestForm->setMaximumHeight(199);

  }else{
    hideLoanResuestForm();

  }
}

void CGUI::refreshLoanReqSummary()
{

  LoanDetails loan_details = LoanContractHandler::calcLoanRepayments(
    CGUI::getModelDraftProposal()->m_loan_principals,
    CGUI::getModelDraftProposal()->m_interest_rate,
    CGUI::getModelDraftProposal()->m_repayment_amount);



  auto[one_cycle_income, apply_cost] = ProposalHandler::calcProposalApplyCost(
    CGUI::getModelDraftProposal()->m_contributed_hours,
    CGUI::getModelDraftProposal()->m_contributed_level,
    CUtils::getNow()); // the creation date of the block in which contribute is recorded (start date)

  if (CGUI::getModelDraftProposal()->m_repayment_amount > one_cycle_income)
  {
    QString msg = "Repayment value is bigger than one cycle income! repay(" + CUtils::microPAIToPAI6(CGUI::getModelDraftProposal()->m_repayment_amount) + ") one cycle(" + CUtils::microPAIToPAI6(one_cycle_income) + ") proposal(" + CUtils::hash8c(CGUI::getModelDraftProposal()->m_selected_proposal) + ")";
    CLog::log(msg, "sec", "error");
    displayMsg(get().m_ui->label_loanReqSummery, msg, false);
    return;
  }

  QString out = "";
  out = "for proposal (" + CUtils::hash8c(CGUI::getModelDraftProposal()->m_selected_proposal) + ") ";
  out += "\nContributing (" + QString::number(CGUI::getModelDraftProposal()->m_contributed_hours) + " Hours of level " + QString::number(CGUI::getModelDraftProposal()->m_contributed_level) + ") " + CGUI::getModelDraftProposal()->m_selected_proposal_title + " ";
  out += "\nTotal repayment (Principles + Interest): " + CUtils::microPAIToPAI6(CGUI::getModelDraftProposal()->m_loan_principals) + " + " + CUtils::microPAIToPAI6(loan_details.m_total_repayment_amount-loan_details.m_loan_principal) + " = " + CUtils::microPAIToPAI6(loan_details.m_total_repayment_amount) + "  Annual interst rate: " + QString::number(CGUI::getModelDraftProposal()->m_interest_rate) + " %";
  out += "\nRepayback in " + CUtils::sepNum(loan_details.m_repayments.size()) + " payments";

  if (loan_details.m_calculation_status)
  {
    out = "Request loan " + out;
    QString tool_tip = "";
    for (auto a_repay: loan_details.m_repayments)
      tool_tip += "\n" + QString::number(a_repay.m_repayment_number) + ". " + CUtils::microPAIToPAI6(a_repay.m_balance) + ", " + CUtils::microPAIToPAI6(a_repay.m_paid_principal) + "+" + CUtils::microPAIToPAI6(a_repay.m_paid_interest) + ", " + CUtils::microPAIToPAI6(a_repay.m_new_balance);
    get().m_ui->label_loanReqSummery->setToolTip(tool_tip);
  }else{
    out = "Failed on loan calculation " + out;
  }

  displayMsg(get().m_ui->label_loanReqSummery, out, loan_details.m_calculation_status);
}

void CGUI::toggleLoanRequestsList(const CDocHashT& doc_hash)
{
  // hide loan resuest form
  hideLoanResuestForm();
  hideSendLoanRequest();

  if (CGUI::getModelDraftProposal()->m_selected_proposal == doc_hash)
  {
    get().m_model_draft_proposals->m_is_visible_loan_requests_list = !get().m_model_draft_proposals->m_is_visible_loan_requests_list;
  }else{
    get().m_model_draft_proposals->m_is_visible_loan_requests_list = true;
  }

  CGUI::getModelDraftProposal()->m_selected_proposal = doc_hash;


  if (get().m_model_draft_proposals->m_is_visible_loan_requests_list)
  {
    get().m_ui->tableView_loanRequests->setFixedHeight(333);
    CGUI::getModelDraftPledges()->signalUpdate();

  }else{
    hideLoanRequests();

  }
}

void MainWindow::on_horizontalSlider_vote_valueChanged(int value)
{
  CGUI::getModelContributes()->m_polling_score = value;
  if ( value == 0)
  {
    ui->pushButton_voteProposal->setText("Send Abstain");
    ui->pushButton_voteProposal->setStyleSheet("background-color: rgb(15, 210, 22);");
  }else if ( value > 0) {
    ui->pushButton_voteProposal->setStyleSheet("background-color: rgb(15, 210, 22);");
    ui->pushButton_voteProposal->setText(QString::number(CGUI::getModelContributes()->m_polling_score) + "% Agree");
  }else{
    ui->pushButton_voteProposal->setStyleSheet("background-color: rgb(215, 10, 22);");
    ui->pushButton_voteProposal->setText(QString::number(-CGUI::getModelContributes()->m_polling_score) + "% Disagree");
  }
}
