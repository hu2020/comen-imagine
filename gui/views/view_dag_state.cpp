#include "stable.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lib/dag/full_dag_handler.h"
#include "lib/dag/state/dag_state_handler.h"
#include "lib/messaging_protocol/dag_message_handler.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>


void MainWindow::connectCoinsVisbilityModel()
{
  ModelCoinsVisbility *mdl = new ModelCoinsVisbility(this);
  CGUI::setModelCoinsVisbility(mdl);
  mdl->populateData();
  ui->tableView_coinsVisbility->setModel(mdl);
  ui->pushButton_updateCoinsVisibility->setCursor(QCursor(Qt::PointingHandCursor));
}

void compareSnapshots(const QModelIndex& idx)
{
  CGUI::compareSnapshots(CGUI::getModelSnapshots()->itemData(idx)[Qt::ToolTipRole].toInt());
}

void MainWindow::connectSnapshotModel()
{
  ModelSnapshots *mdl = new ModelSnapshots(this);
  CGUI::setModelSnapshots(mdl);
  mdl->populateData();
  ui->tableView_snapshots->setModel(mdl);
  connect(ui->tableView_snapshots, &QListView::doubleClicked, compareSnapshots);
}

void MainWindow::connectSnapComapreModel()
{
  ModelSnapCompare *mdl = new ModelSnapCompare(this);
  CGUI::setModelSnapCompare(mdl);
  mdl->populateData();
  ui->tableView_snapsCompare->setModel(mdl);
//  connect(ui->tableView_snapshots, &QListView::doubleClicked, );
}




bool MainWindow::on_pushButton_downloadDAGSnapshot_clicked()
{
  return DAGStateHandler::sendNodeSnapshot("toHard");
}

bool MainWindow::on_pushButton_sendDAGSnapshot_clicked()
{
  return DAGStateHandler::sendNodeSnapshot("toNeighbor", "");
}

void MainWindow::on_pushButton_controllDAGHealth_clicked()
{
  ui->pushButton_controllDAGHealth->setCursor(QCursor(Qt::PointingHandCursor));
  auto [status, msg] = DAG::controllDAGHealth();
  QString status_color = status ? "00ff00" : "ff0000";
//  ui->pushButton_controllDAGHealth->setBackgrounColor(status_color);

}

void MainWindow::on_pushButton_updateCoinsVisibility_clicked()
{
  CGUI::signalUpdateCoinsVisbility();
  CGUI::signalUpdateSnapshots();
}

void MainWindow::on_pushButton_loadSnapshot_clicked()
{
  CLog::log("on pushButton loadSnapshot clicked*************");
  QString filter = "All file (*.*) ;; Text file (*.txt)";
  QString file_name = QFileDialog::getOpenFileName(this, "Open a file", "/");
  QFile file(file_name);
  if (!file.open(QFile::ReadOnly))
  {
//    warningMessage("Failed to open file!", "Restore Backup");
  }
  QTextStream in(&file);
  QString content = in.readAll();
  if (content != "")
  {
    QStringList seg = file_name.split("/");
    DAGStateHandler::RecordNodeSnapshotInDb(content, CUtils::getNow() + " " + seg[seg.size()-1]);
  }else{
    warningMessage("The file was empty!", "Restore Backup");
  }
}
