#include "stable.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lib/services/contracts/flens/iname_handler.h"


void inameDblClicked(const QModelIndex& idx)
{
  CGUI::getModelRegisteredINames()->inameDblClicked(idx);
}

void MainWindow::connectRegisteredINamesToModel()
{
  ModelRegisteredINames* mdl = new ModelRegisteredINames(this);
  CGUI::setModelRegisteredINames(mdl);
  mdl->populateData();
  ui->tableView_registered_inames->setModel(mdl);
  ui->tableView_registered_inames->setColumnWidth(0, 1);
  ui->tableView_registered_inames->setColumnWidth(1, 21);
  ui->tableView_registered_inames->resizeColumnsToContents();
  connect(ui->tableView_registered_inames, &QListView::doubleClicked, inameDblClicked);
}

void machineControlledINameDblClicked(const QModelIndex& idx)
{
  CGUI::getModelMachineControlledINames()->machineControlledINameDblClicked(idx);
}

void MainWindow::connectMachineControlledINamesToModel()
{
  ModelMachineControlledINames* mdl = new ModelMachineControlledINames(this);
  CGUI::setModelMachineControlledINames(mdl);
  mdl->populateData();
  ui->tableView_machine_controlled_inames->setModel(mdl);
  ui->tableView_machine_controlled_inames->setColumnWidth(0, 1);
  ui->tableView_machine_controlled_inames->setColumnWidth(1, 21);
  ui->tableView_machine_controlled_inames->resizeColumnsToContents();
  connect(ui->tableView_machine_controlled_inames, &QListView::doubleClicked, machineControlledINameDblClicked);
}

void MainWindow::on_pushButton_update_registered_inames_clicked()
{
  CGUI::refresh_inames_info();
}

void CGUI::refresh_inames_info()
{
  signalUpdateRegisteredINames();
  signalUpdateMachineControlledINames();
  initMachineControlledINames();
}

void CGUI::initMachineControlledINames()
{
  auto[res_status, res_msg, inames, bindings_info] = INameHandler::getMachineControlledINames(
  {},
  {"imn_name", "imn_hash"},
  {{"imn_name", "ASC"}});
  CLog::log("kkkkkkkkk " + CUtils::dumpIt(inames));

  for (QVDicT an_iname: inames)
    get().m_ui->comboBox_machine_controlled_inames->addItem(an_iname.value("imn_name").toString(), an_iname.value("imn_hash").toString());

}

void MainWindow::on_pushButton_create_and_bind_iPGP_clicked()
{

  QString binding_iname = ui->comboBox_machine_controlled_inames->currentText();
  QString binding_iname_hash = ui->comboBox_machine_controlled_inames->currentData().toString();
  CLog::log("pppppppppppppp binding_iname " + binding_iname);
  CLog::log("pppppppppppppp binding_iname_hash " + binding_iname_hash);
  QString binding_label = ui->lineEdit_iPGP_binding_label->text();
  QString binding_comment = ui->textEdit_iPGP_binding_comment->toPlainText();

  if (binding_label == "")
  {
    CGUI::displayMsg(ui->label_bind_iPGP_noti_msg, "Missed binding label!", false);
    return;
  }

  auto[res_status, res_msg, inames, bindings_info] = INameHandler::getMachineControlledINames(
    {{"imn_hash", binding_iname_hash}},
    {"imn_owner", "imn_name", "imn_register_date"}
  );
  if (inames.size() != 1)
  {
    CGUI::displayMsg(ui->label_bind_iPGP_noti_msg, "There is no registere iName(" + binding_iname + ") hash(" + binding_iname_hash + ")", false);
    return;
  }

  QVDicT the_iname = inames[0];
  CLog::log("pppppppppppppp the_iname " + CUtils::dumpIt(the_iname));
  auto[bind_status, bind_msg] = INameHandler::createAndBindIPGP(
    the_iname.value("imn_owner").toString(),  // hot_owner
    binding_iname_hash, // iname_hash:
    binding_label, // bind_label
    false, // isHotIName
    ((binding_comment=="")?"Always PGP":binding_comment)); // bind_comment
  CGUI::displayMsg(ui->label_bind_iPGP_noti_msg, bind_msg, bind_status);

  if(bind_status)
  {
    CGUI::refresh_inames_info();
    CGUI::signalUpdateBlockBuffer();
  }

}

void MainWindow::on_pushButton_registerIName_clicked()
{
  if (!ui->checkBox_iname_responsability->isChecked())
  {
    CGUI::displayMsg(ui->label_iname_reg_form_noti_msg, "In order to register an iName, please check your responsibility \nabout the iName and all future usage of it.", false);
    return;
  }


  QString iname = ui->lineEdit_iname_to_reg->text();

  if (iname == "")
  {
    CGUI::displayMsg(ui->label_iname_reg_form_noti_msg, "iName is empty!", false);
    return;
  }

  auto[reg_status, reg_msg] = INameHandler::iNameRegReq(iname);
  CGUI::displayMsg(ui->label_iname_reg_form_noti_msg, reg_msg, reg_status);

  if (reg_status)
  {
    // update PRE_RECORDED_INAMES
    INameHandler::addToPreRecordedINames(iname);
    CGUI::signalUpdateBlockBuffer();
  }
}

void MainWindow::on_lineEdit_iname_to_reg_textChanged(const QString& iname)
{
  auto[status, reg_cost, est_msg] = INameHandler::estimateINameRegReqCost(iname, CConsts::STAGES::Creating);
  if (status)
  {
    CGUI::displayMsg(ui->label_iname_reg_form_noti_msg, "Cost for registering \"" + iname + "\" \nis about " + CUtils::microPAIToPAI6(reg_cost) + " PAIs", status);
  }else{
    CGUI::displayMsg(ui->label_iname_reg_form_noti_msg, est_msg, status);
  }
}
