#include "mainwindow.h"
#include "ui_mainwindow.h"



enum ITEM_DATA{
  N_ID = 1,
  CONNECTION_TYPE = 3,
  EMAIL = 4
};

bool MainWindow::on_pushButton_addANewNeighbor_clicked()
{
  QString public_neighbor_email = ui->lineEdit_publicNeighborEmail->text();
  QString public_neighbor_public_key = ui->textEdit_publicNeighborPubKey->toPlainText();
  CLog::log("public_neighbor_email: " + public_neighbor_email);
  CLog::log("public_neighbor_public_key: " + public_neighbor_public_key);
  if (public_neighbor_email == "")
  {
    warningMessage("The neighbor email is needed!", "Adding a new neighbor");
    return false;
  }
  auto[staut, msg] = CMachine::addANewNeighbor(public_neighbor_email, CConsts::PUBLIC, public_neighbor_public_key);
  if (!staut)
  {
    warningMessage(msg, "Adding a new neighbor");
    return false;
  }

  // refresh the neighbors list
  CGUI::signalUpdateNeighbors();
  informationMessage("New neighbor added", "Adding a new neighbor");

  return true;
}


void MainWindow::connectNeighborsToModel()
{
  ModelNeighbors *mdl = new ModelNeighbors(this);
  CGUI::setModelNeighbors(mdl);
  mdl->populateData();
  ui->tableView_pulicNeighbors->setModel(mdl);

  connect(
    ui->tableView_pulicNeighbors,
    SIGNAL(doubleClicked(const QModelIndex &)),
    this,
    SLOT(handshakeDblClicked(const QModelIndex &)));

}

void MainWindow::handshakeDblClicked(const QModelIndex &index)
{
  if (index.isValid()) {
    auto m = index.model();
    QString email = m->index(index.row(), 0).data().toString();

    if (index.column() == 2)
    {
      // hand shake
      QString connection_type = CConsts::PUBLIC;
      QString neighbor_id = index.data(Qt::StatusTipRole).toString();
      bool res = CMachine::handshakeNeighbor(neighbor_id, connection_type);
      if (res)
      {
        informationMessage("Handshake to " + email +  " sent");
      }else{
        warningMessage("Handshake to " + email +  " was failed!");
      }
    }

    else if (index.column() == 5)
    {
      // delete neighbor
      QString connection_type = CConsts::PUBLIC;
      QString neighbor_id = index.data(Qt::StatusTipRole).toString();
      bool res = CMachine::deleteNeighbors(neighbor_id, connection_type);
      if (res)
      {
        CGUI::signalUpdateNeighbors();
//        informationMessage("Delete reqauest to " + email +  " sent");
      }else{
        warningMessage("Delete reqauest to " + email +  " was failed!");
      }


    }
  }
}

