#include "stable.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"



void MainWindow::on_pushButton_refreshWallet_clicked()
{
  Wallet::getCoinsList(true);
  CGUI::signalUpdateWalletCoins();
  CGUI::signalUpdateWalletAccounts();
}

void MainWindow::connectWalletCoinsToModel()
{
  ModelWalletCoins* mdl = new ModelWalletCoins(this);
  CGUI::setModelWalletCoins(mdl);
//  mdl->populateData();
  ui->tableView_walletCoins->setModel(mdl);
  ui->tableView_walletCoins->setColumnWidth(0, 1);
  ui->tableView_walletCoins->setColumnWidth(1, 21);
  ui->tableView_walletCoins->resizeColumnsToContents();

  ui->pushButton_createBasic1of1Address->setCursor(QCursor(Qt::PointingHandCursor));
  ui->pushButton_createBasic2of3Address->setCursor(QCursor(Qt::PointingHandCursor));
  ui->pushButton_createStrict2of3Address->setCursor(QCursor(Qt::PointingHandCursor));
  ui->pushButton_createStrictInputTimeLockedAddress->setCursor(QCursor(Qt::PointingHandCursor));

}

void MainWindow::on_pushButton_createBasic1of1Address_clicked()
{
  auto[status, unlock_doc] = CAddress::createANewAddress(
    CConsts::SIGNATURE_TYPES::Basic,
    "1/1",
    "0.0.1");
    if (!status)
    {
      CGUI::displayMsg(ui->label_createNewAddressNoti, "Couldn't creat ECDSA 1of1 key pairs", false);
      return;
    } else {
      CGUI::displayMsg(ui->label_createNewAddressNoti, "New Address created: " + unlock_doc.m_account_address, true);
    }

  Wallet::insertAddress( WalletAddress (
    &unlock_doc,
    CMachine::getSelectedMProfile(),   // mp code
    "Basic address (1/1 signature) ver(0.0.1)",
    CUtils::getNow()));

  CGUI::signalUpdateWalletCoins();
  CGUI::signalUpdateWalletAccounts();
  return;
}

void MainWindow::on_pushButton_signTransaction_clicked()
{
  CAddressT trx_recipient = ui->lineEdit_trx_recipient->text();
  QString trx_amount = ui->lineEdit_trx_amount->text();
  QString trx_outputs_bill_amount = ui->lineEdit_trx_outputs_amount->text(); // the amount of output(s). zero means one output for all amount, while 1000000 means each output must be a million mPAI
  QString trx_fee = ui->lineEdit_trx_fee->text();
  CAddressT trx_change_back_address = ui->lineEdit_trx_change_back_address->text();

  // TODO: add some control on input fields

  CMPAIValueT bill_size;
  if ((trx_outputs_bill_amount == "") || (trx_outputs_bill_amount == "0"))
  {
    bill_size = 0;
  }else{
    bill_size = static_cast<CMPAIValueT>(QVariant::fromValue(trx_outputs_bill_amount).toDouble());
  }

  QStringList selected_coins {};
  auto coins_chck_status = CGUI::getModelWalletCoins()->m_trx_selected_coins_to_spend;
  for (QString a_coin: coins_chck_status.keys())
    if (coins_chck_status[a_coin])
      selected_coins.append(a_coin);

  auto[sign_res, sign_msg] = Wallet::walletSigner(
    selected_coins,
    static_cast<CMPAIValueT>(QVariant::fromValue(trx_amount).toDouble()) * 1000000,
    static_cast<CMPAIValueT>(QVariant::fromValue(trx_fee).toDouble()) * 1000000,
    trx_recipient,
    trx_change_back_address,
    bill_size);
  ui->label_spendableAmount->setText(sign_msg);
  if(sign_res)
  {
    CGUI::signalUpdateBlockBuffer();
    CGUI::resetSelectedWalletCoins();
    CGUI::signalUpdateWalletCoins();
    CGUI::signalUpdateWalletAccounts();
  }
}

void MainWindow::on_pushButton_createStrict2of3Address_clicked()
{
  auto[status, unlock_doc] = CAddress::createANewAddress(
    CConsts::SIGNATURE_TYPES::Strict,
    "2/3",
    "0.0.1");
  if (!status)
  {
    CGUI::displayMsg(ui->label_createNewAddressNoti, "Couldn't creat ECDSA Strict 2of3 key pairs", false);
    return;// {false, " (for public channel)"};
  } else {
    CGUI::displayMsg(ui->label_createNewAddressNoti, "New Address created: " + unlock_doc.m_account_address, true);
  }


  Wallet::insertAddress( WalletAddress (
    &unlock_doc,
    CMachine::getSelectedMProfile(),   // mp code
    "Strict address (2/3 signatures) ver(0.0.1)",
    CUtils::getNow()));

  CGUI::signalUpdateWalletCoins();
  CGUI::signalUpdateWalletAccounts();
  return;
}

void MainWindow::on_pushButton_createBasic2of3Address_clicked()
{
  auto[status, unlock_doc] = CAddress::createANewAddress(
    CConsts::SIGNATURE_TYPES::Basic,
    "2/3",
    "0.0.1");
    if (!status)
    {
      CGUI::displayMsg(ui->label_createNewAddressNoti, "Couldn't creat ECDSA 2of3 key pairs", false);
      return;// {false, " (for public channel)"};
    } else {
      CGUI::displayMsg(ui->label_createNewAddressNoti, "New Address created: " + unlock_doc.m_account_address, true);
    }

  Wallet::insertAddress( WalletAddress (
    &unlock_doc,
    CMachine::getSelectedMProfile(),   // mp code
    "Basic address (2/3 signatures) ver(0.0.1)",
    CUtils::getNow()));

  CGUI::signalUpdateWalletCoins();
  CGUI::signalUpdateWalletAccounts();
  return;
}

void MainWindow::connectWalletAccountsToModel()
{
  ModelWalletAccounts* mdl = new ModelWalletAccounts(this);
  CGUI::setModelWalletAccounts(mdl);
//  mdl->populateData();
  ui->tableView_walletAccounts->setModel(mdl);
  ui->tableView_walletAccounts->setColumnWidth(0, 1);
  ui->tableView_walletAccounts->setColumnWidth(1, 21);
  ui->tableView_walletAccounts->resizeColumnsToContents();
}

void MainWindow::on_lineEdit_trx_amount_textEdited(const QString& amount)
{
  CGUI::getModelWalletCoins()->m_trx_sending_amount = QVariant::fromValue(amount).toDouble();
  CGUI::getModelWalletCoins()->recalcTransactionForm();
  CGUI::refreshTransactionFormPresentation();
}

void MainWindow::on_lineEdit_trx_fee_textEdited(const QString& amount)
{
  CGUI::getModelWalletCoins()->m_trx_fee_amount = QVariant::fromValue(amount).toDouble();
  CGUI::getModelWalletCoins()->recalcTransactionForm();
  CGUI::refreshTransactionFormPresentation();
}

void MainWindow::on_pushButton_resetSpendingForm_clicked()
{
  CGUI::resetSelectedWalletCoins();
  ui->lineEdit_trx_recipient->setText("");
  ui->lineEdit_trx_amount->setText("");
  ui->lineEdit_trx_outputs_amount->setText("");
  ui->lineEdit_trx_change_back_address->setText("");
  ui->lineEdit_trx_fee->setText("1000");
  CGUI::getModelWalletCoins()->m_trx_spendable_amount = 0;
  CGUI::getModelWalletCoins()->m_trx_sending_amount = 0;
  CGUI::getModelWalletCoins()->recalcTransactionForm();
}

void MainWindow::on_pushButton_unmark_not_really_used_coins_clicked()
{
  Wallet::restorUnUsedUTXOs();

  Wallet::getCoinsList(true);
  CGUI::signalUpdateWalletCoins();
  CGUI::signalUpdateWalletAccounts();
}
