#include "stable.h"

#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "lib/services/uri/uri_handler.h"

void agoraSelected(const QModelIndex& index)
{
  CGUI::agoraSelected(index);
}

void CGUI::showAgoraReplyForm()
{
  get().m_ui->widget_reply_post_agora->setFixedHeight(611);
}

void CGUI::hideAgoraReplyForm()
{
  get().m_ui->widget_reply_post_agora->setFixedHeight(0);
}

void CGUI::showAgoraRegForm()
{
  CGUI::getModelAgoraTitles()->m_is_visible_agora_creation_form = true;
  get().m_ui->widget_create_new_agora->setFixedHeight(333);
}

void CGUI::hideAgoraRegForm()
{
  CGUI::getModelAgoraTitles()->m_is_visible_agora_creation_form = false;
  get().m_ui->widget_create_new_agora->setFixedHeight(0);
}

void MainWindow::on_pushButton_toggle_agora_creation_form_clicked()
{
  CGUI::getModelAgoraTitles()->m_is_visible_agora_creation_form = !CGUI::getModelAgoraTitles()->m_is_visible_agora_creation_form;
  if(CGUI::getModelAgoraTitles()->m_is_visible_agora_creation_form)
  {
    CGUI::hideAgoraReplyForm();
    CGUI::showAgoraRegForm();
  }else{
    CGUI::hideAgoraRegForm();
  }
}

void MainWindow::on_pushButton_reply_agora_post_clicked()
{
  CDocHashT ag_hash = CGUI::getModelAgoraTitles()->m_selected_agora.value("ag_hash").toString();
  QString post_content = ui->textEdit_agora_post_content->toPlainText();
  auto reply_point = CGUI::getModelAgoraTitles()->m_reply_point;
  auto [status, msg] = DemosHandler::sendNewPost(
    ag_hash,
    post_content,
    CGUI::getModelAgoraTitles()->m_selected_post_to_reply,
    reply_point);

  CGUI::displayMsg(ui->label_agora_reply_noti_msg, msg, status);

  if (status)
  {
    CGUI::signalUpdateBlockBuffer();
    ui->textEdit_agora_post_content->setText("");
  }

}

void MainWindow::on_pushButton_new_post_on_agora_clicked()
{
  CGUI::hideAgoraRegForm();
  CGUI::showAgoraReplyForm();
  CGUI::getModelAgoraTitles()->m_selected_post_to_reply = "";
}

void MainWindow::clickedOnDemosLinks(const QUrl& link)
{
  //  QDesktopServices::openUrl();  // open link in default browser
  QString link_str = link.toString();
  CLog::log("link_str: "+ link_str);
  QStringList uri_segments = link_str.split("/");

  if (uri_segments[0] == "replay")
  {
    CGUI::getModelAgoraTitles()->m_selected_post_to_reply = uri_segments[1];
    CLog::log("goint to reply agora post: " + CGUI::getModelAgoraTitles()->m_selected_post_to_reply);
    QVDRecordsT posts = DemosHandler::searchInPosts(
      {{"ap_doc_hash", CGUI::getModelAgoraTitles()->m_selected_post_to_reply}},
      {"ap_ag_hash", "ap_opinion"});
    if (posts.size() != 1)
    {
      ui->label_agora_reply_header->setText("Invalide post to reply");
      return;
    }
    CDocHashT related_agora = posts[0].value("ap_ag_hash").toString();

    ui->label_agora_reply_header->setText("Reply to: " + CUtils::getFirstNWords(posts[0].value("ap_opinion").toString(), 8));

    // display replay form
    CGUI::hideAgoraRegForm();
    CGUI::showAgoraReplyForm();
    return;
  }

  CURI curi = URIHandler::ParseURI(link_str);
  if (curi.m_module == "demos")
  {
    ui->textBrowser_agoraPage->setHtml(DemosHandler::renderPage(curi.m_page));

  } else if (curi.m_module == "wiki")
  {
    CUtils::exiter("click on wiki link is not implemented yet", 66);

  } else if (curi.m_module == "contribute")
  {
    CUtils::exiter("click on contribute link is not implemented yet", 66);
  }
}

void MainWindow::connectAgoraTitlesToModel()
{
  ModelAgoraTitles* mdl = new ModelAgoraTitles(this);
  CGUI::setModelAgoraTitles(mdl);
  mdl->populateData();
  ui->treeView_agoraTitles->setModel(mdl);
  ui->treeView_agoraTitles->setColumnWidth(0, 1);
  connect(ui->treeView_agoraTitles, &QListView::doubleClicked, agoraSelected);

  connect(ui->textBrowser_agoraPage, &QTextBrowser::anchorClicked, this, &MainWindow::clickedOnDemosLinks);
  ui->textBrowser_agoraPage->setOpenLinks(false);
  ui->textBrowser_agoraPage->setOpenExternalLinks(false);

}

void MainWindow::on_comboBox_agora_category_currentIndexChanged(const QString& arg1)
{
  on_lineEdit_agora_title_textChanged(ui->lineEdit_agora_title->text());
}

void MainWindow::on_lineEdit_agora_title_textChanged(const QString& the_text)
{
  QString agora_hash = ui->comboBox_agora_category->currentData().toString();

  QString complete_uri;
  if (agora_hash != "")
  {
    QVDRecordsT agoras = DemosHandler::searchInAgoras(
      {{"ag_hash", agora_hash}},
      {"ag_full_category"});

    complete_uri = "/demos/" + agoras[0].value("ag_full_category").toString() + "/" + the_text;
  } else {
    complete_uri = "/demos/" + the_text;
  }

  ui->label_agora_title->setText(complete_uri);
}



void MainWindow::on_horizontalSlider_agora_reply_valueChanged(int value)
{
  CGUI::getModelAgoraTitles()->m_reply_point = value;
  if ( value == 0)
  {
    ui->pushButton_reply_agora_post->setText("Send (Abstain)");
  }else if ( value > 0) {
    ui->pushButton_reply_agora_post->setText("Send (" + QString::number(CGUI::getModelAgoraTitles()->m_reply_point) + "% Agree)");
  }else{
    ui->pushButton_reply_agora_post->setText("Send (" + QString::number(-CGUI::getModelAgoraTitles()->m_reply_point) + "% Disagree)");
  }
}

void MainWindow::on_pushButton_create_agora_clicked()
{
  QString agora_author = ui->comboBox_agora_author->currentData().toString();
  QString agora_iname = ui->comboBox_agora_domain->currentData().toString();
  QString the_parent = ui->comboBox_agora_category->currentData().toString();
  QString agora_lang = ui->comboBox_agora_languages->currentData().toString();
  QString agora_title = ui->lineEdit_agora_title->text();
  QString agora_comment = ui->lineEdit_agora_comment->text();
  QString agora_tags = ui->lineEdit_agora_tags->text();

  if (agora_lang == "")
  {
    CGUI::displayMsg(ui->label_agora_noti_msg, "Missed agora language", false);
    return;
  }
  if (agora_lang != "eng")
  {
    CGUI::displayMsg(ui->label_agora_noti_msg, "At the moment only English language is supported", false);
    return;
  }

  if (agora_title == "")
  {
    CGUI::displayMsg(ui->label_agora_noti_msg, "Missed agora title", false);
    return;
  }

  auto[status, msg] = DemosHandler::buildANewAgora(
    agora_author,
    the_parent,
    agora_title,
    agora_comment,
    agora_tags,
    agora_lang,
    agora_iname);
  CGUI::displayMsg(ui->label_agora_noti_msg, msg, status);

  if (status)
    CGUI::signalUpdateBlockBuffer();

}

void MainWindow::on_comboBox_agora_domain_currentIndexChanged(const QString& selected_iname)
{
  CGUI::initAgorasCategoriesCombo(ui->comboBox_agora_category, selected_iname);
}

void CGUI::agoraSelected(const QModelIndex& index)
{
  getModelAgoraTitles()->m_selected_agora = getModelAgoraTitles()->m_agora_titles[index.row()];
  QString out = DemosHandler::renderPage(getModelAgoraTitles()->m_selected_agora.value("ag_hash").toString());
  CLog::log("Agora content: \n" + out);
  get().m_ui->textBrowser_agoraPage->setText(out);
};

