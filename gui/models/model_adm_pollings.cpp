#include "stable.h"

#include "gui/c_gui.h"
#include "lib/services/society_rules/society_rules.h"

#include "model_adm_pollings.h"
ModelAdmPollings::ModelAdmPollings(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelAdmPollings::rowCount(const QModelIndex & /*parent*/) const
{
  return m_adm_pollings.size();
}

int ModelAdmPollings::columnCount(const QModelIndex & /*parent*/) const
{
  return 2;
}

QVariant ModelAdmPollings::data(const QModelIndex &index, int role) const
{
  QJsonObject the_adm = m_adm_pollings[index.row()].toObject();

  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;

        case 1:
          return the_adm.value("label").toString();

      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        default:
        return QBrush {};

      }
      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return the_adm.value("key").toString();
          break;


        default:
          return "";
          break;
      }
      break;



    }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelAdmPollings::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Title");

      }
    }
  }
  return QVariant();
}

void ModelAdmPollings::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_adm_pollings = SocietyRules::loadAdmPollings();
}


void ModelAdmPollings::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}
