#include "stable.h"

#include "gui/c_gui.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"

#include "model_draft_pledges.h"


ModelDraftPledges::ModelDraftPledges(QObject *parent)
{
Q_UNUSED(parent);
}

int ModelDraftPledges::rowCount(const QModelIndex & /*parent*/) const
{
  return m_draft_pledges.size();
}

int ModelDraftPledges::columnCount(const QModelIndex & /*parent*/) const
{
  return 9;
}

QVariant ModelDraftPledges::getCellData(const QModelIndex &index) const
{
  QVDicT pledge_record = m_draft_pledges[index.row()];
  QJsonObject pldege_doc = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(pledge_record.value("dpl_body").toString()).content);

  QVDRecordsT proposals = ProposalHandler::searchInDraftProposals({{"pd_hash", pledge_record.value("dpl_doc_ref").toString()}});
  if (proposals.size() == 0)
  {
    CGUI::get().m_ui->label_proposalNotifyMsg->setText("Theres is no draft proposal for this draft pledge! proposal(" +CUtils::hash8c(pledge_record.value("dpl_doc_ref").toString())+ ") pledge-id(" + pledge_record.value("dpl_id").toString()+ ") ");
    return QVariant();
  }

  QVDicT proposal_record = proposals[0];

  QString repayment_details = "Should pay " + QString::number(pldege_doc.value("redeemTerms").toObject().value("repaymentsNumber").toDouble()) + " repayments of ";
  repayment_details += CUtils::microPAIToPAI6(pldege_doc.value("redeemTerms").toObject().value("repaymentAmount").toDouble()) + " PAIs";

  switch(index.column())
  {
    case 0:
      return index.row() + 1;

    case 1:
      return proposal_record.value("pd_title").toString() + " " + proposal_record.value("pd_comment").toString();

    case 2:
      return pledge_record.value("dpl_pledger").toString();

    case 3:
      return pledge_record.value("dpl_pledgee").toString();

    case 4:
      return CUtils::microPAIToPAI6(pldege_doc.value("redeemTerms").toObject().value("principal").toDouble());

    case 5:
      return pldege_doc.value("redeemTerms").toObject().value("annualInterest").toDouble();

    case 6:
      return repayment_details;

    case 7:
      return "Send Request to neighbor(pledgee)";

    case 8:
      return "Delete";

  }

  return "";
}


QVariant ModelDraftPledges::data(const QModelIndex &index, int role) const
{
  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  QVDicT proposal = m_draft_pledges[index.row()];

  QString row_color_proposal = (index.row() % 2 == 0) ? "ffffff" : "eaebec";
  QString row_color_polling = (index.row() % 2 == 0) ? "afffff" : "aaebec";
  QString row_color_votes = (index.row() % 2 == 0) ? "8fffdf" : "8aabec";

  uint32_t mod_ = index.row() % 10;

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);
      break;

    case Qt::BackgroundRole:

      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(CUtils::hash6c(proposal.value("dpl_doc_ref").toString()));
          break;
      case 7:
        return CGUI::getBrushByColorCode("00ff00");
      case 8:
        return CGUI::getBrushByColorCode("ff0000");

//        case 1:
//          switch (mod_)
//          {
//            case 6:
//              return CGUI::getBrushByColorCode("00aabb");
//            case 7:
//              return CGUI::getBrushByColorCode("55aabb");
//          }
//        case 2:
//          switch (mod_)
//          {
//            case 6:
//              return CGUI::getBrushByColorCode("00ff00");
//            case 7:
//              return CGUI::getBrushByColorCode("ff0000");
//          }



      default:
        return CGUI::getBrushByColorCode(row_color_proposal);

      }


    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return "";
          break;

        default:
          return "";
          break;
      }
      break;



  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}


QVariant ModelDraftPledges::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Proposal Title");
      case 2:
        return QString("Pledged Account");
      case 3:
        return QString("Pledgee");
      case 4:
        return QString("Principal");
      case 5:
        return QString("Annual Interest");
      case 6:
        return QString("Repayment details");
      case 7:
        return QString("Send Request");
      case 8:
        return QString("Delete");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("");
      case 1:
        return QString("");
      }
    }
  }
  return QVariant();
}


void ModelDraftPledges::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  auto tmp = GeneralPledgeHandler::searchInDraftPledges(
    {{"dpl_class", CConsts::PLEDGE_CLASSES::PledgeP},
     {"dpl_doc_ref", CGUI::getModelDraftProposal()->m_selected_proposal}});
  m_draft_pledges = tmp;
}


void ModelDraftPledges::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelDraftPledges::draftPledgeDblClicked(const QModelIndex& index)
{
  QVDicT pldege = m_draft_pledges[index.row()];

  switch(index.column())
  {
    case 0:
      return;

    case 7:
      // send
      CGUI::getModelDraftProposal()->m_selected_draft_pledge = pldege.value("dpl_id").toDouble();
      CLog::log("Show neighbors list as pledgee for pledge-id(" + pldege.value("dpl_id").toString());
      CGUI::showSendLoanRequest();
      return;

      case 8:
        // delete
        GeneralPledgeHandler::deleteDraftPledge({{"dpl_id", pldege.value("dpl_id")}});
        signalUpdate();
        return;

      case 9:
        return;

      default:
        return;
  }

}
