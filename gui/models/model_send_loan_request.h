#ifndef MODELSENDLOANREQUEST_H
#define MODELSENDLOANREQUEST_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"



class ModelSendLoanRequest: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelSendLoanRequest(QObject *parent);
  virtual ~ModelSendLoanRequest(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_neighbors {};

  void populateData();
  void signalUpdate();

  void sendLoanRequestDblClicked(const QModelIndex& idx);

};

#endif // MODELSENDLOANREQUEST_H
