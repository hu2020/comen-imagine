#include "stable.h"

#include "gui/c_gui.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contribute/contribute_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"

#include "model_send_loan_request.h"

ModelSendLoanRequest::ModelSendLoanRequest(QObject *parent)
{
Q_UNUSED(parent);
}

int ModelSendLoanRequest::rowCount(const QModelIndex & /*parent*/) const
{
  return m_neighbors.size();
}

int ModelSendLoanRequest::columnCount(const QModelIndex & /*parent*/) const
{
  return 5;
}

QVariant ModelSendLoanRequest::getCellData(const QModelIndex &index) const
{
  QVDicT the_neighbor = m_neighbors[index.row()];

  switch(index.column())
  {
    case 0:
      return index.row() + 1;

    case 1:
      return the_neighbor.value("n_email").toString();

    case 2:
      return the_neighbor.value("n_is_active").toString();

    case 3:
      return the_neighbor.value("n_connection_type").toString();

    case 4:
      return "Send";

  }

  return "";
}


QVariant ModelSendLoanRequest::data(const QModelIndex &index, int role) const
{
  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);

  case Qt::BackgroundRole:
    switch (index.column())
    {
      case 4:
        return CGUI::getBrushByColorCode("00ff00");
    }
  }

  return QVariant();
}


QVariant ModelSendLoanRequest::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Email");
      case 2:
        return QString("Is Active");
      case 3:
        return QString("Connection Type");
      case 4:
        return QString("Send");
      }
    }

  } else if (role == Qt::BackgroundRole) {
    switch (section)
    {
      case 4:
        return CGUI::getBrushByColorCode("00ff00");
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("");
      case 1:
        return QString("");
      }
    }

  }

  return QVariant();
}


void ModelSendLoanRequest::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  auto tmp = CMachine::getNeighbors();
  m_neighbors = tmp;
}


void ModelSendLoanRequest::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelSendLoanRequest::sendLoanRequestDblClicked(const QModelIndex& index)
{
  QVDicT the_neighbor = m_neighbors[index.row()];

  switch(index.column())
  {
    case 4:
      // sending
      CLog::log("Sending loan request for pledge-id(" + the_neighbor.value("n_id").toString() + ") to neighbor("+the_neighbor.value("n_email").toString()+")");
      auto[status, msg] = ContributeHandler::sendLoanRequest(
        CGUI::getModelDraftProposal()->m_selected_draft_pledge,
        the_neighbor.value("n_id").toString());
      CGUI::get().m_ui->label_proposalNotifyMsg->setText(msg);
      return;
  }

}
