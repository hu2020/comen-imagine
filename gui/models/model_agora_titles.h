#ifndef MODELAGORATITLES_H
#define MODELAGORATITLES_H


#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"


class ModelAgoraTitles: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelAgoraTitles(QObject *parent);
  virtual ~ModelAgoraTitles(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDicT m_selected_agora;
  int m_reply_point = 0;
  QVDRecordsT m_agora_titles = {};

  bool m_is_visible_agora_creation_form = false;
  CDocHashT m_selected_post_to_reply = "";

  void populateData();
  void signalUpdate();
};

#endif // MODELAGORATITLES_H
