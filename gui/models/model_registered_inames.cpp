#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"

#include "model_registered_inames.h"

ModelRegisteredINames::ModelRegisteredINames(QObject *parent)
{
Q_UNUSED(parent);
}

int ModelRegisteredINames::rowCount(const QModelIndex & /*parent*/) const
{
  return m_registered_inames.size();
}

int ModelRegisteredINames::columnCount(const QModelIndex & /*parent*/) const
{
  return 5;
}

QVariant ModelRegisteredINames::getCellToolTip(const QModelIndex &index) const
{
  QString tooltip = "";
  return tooltip;
}

QVariant ModelRegisteredINames::getCellData(const QModelIndex &index) const
{
  QVDicT the_iname = m_registered_inames[index.row()];
  // CLog::log("xxxxx m_registered_inames xxxxxxx " + CUtils::dumpIt(the_iname));

  switch(index.column())
  {
    case 0:
      return index.row() + 1;

    case 1:
      return the_iname.value("in_name").toString();
    case 2:
      return the_iname.value("in_register_date").toString();
    case 3:
      return the_iname.value("in_owner").toString();
    case 4:
      return the_iname.value("in_is_settled").toString();
  }

  return "";
}


QVariant ModelRegisteredINames::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("iName");
      case 2:
        return QString("Registered Date");
      case 3:
        return QString("Owner");
      case 4:
        return QString("Setteled");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("");
      case 1:
        return QString("");
      }
    }
  }
  return QVariant();
}


QVariant ModelRegisteredINames::data(const QModelIndex &index, int role) const
{
  QVDicT the_iname = m_registered_inames[index.row()];

  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);

    case Qt::ToolTipRole:
      return getCellToolTip(index);

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 3:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(the_iname.value("in_owner").toString()));
        case 4:
          return CGUI::getBrushByColorCode((the_iname.value("in_is_settled").toString()=="Y" ? "00ff00" : "ffff00"));

        return QBrush {};

      }

  }

  return QVariant();
}


void ModelRegisteredINames::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  auto inames = INameHandler::searchRegisteredINames({});
  m_registered_inames = inames.m_records;
}


void ModelRegisteredINames::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelRegisteredINames::inameDblClicked(const QModelIndex& index)
{
//  QJsonObject the_iname = m_registered_inames[index.row()].toObject();
//  CLog::log("Dbl Clicked on onchain contract: " + CUtils::serializeJson(the_iname), "app", "info");

//  switch(index.column())
//  {
//    case 16:
//      // deleting
//      TmpContentsHandler::deleteTmpContent({{"tc_id", the_iname.value("tc_id")}});
//      signalUpdate();
//      return;

//    case 17:
//      // accepting & signing
//      auto[status, msg] = TmpContentsHandler::unBundleAndPushToBlockBufferPPT(the_iname.value("tc_id").toDouble());
//      signalUpdate();
////      CGUI::signalUpdateBlockBuffer();
//      CGUI::displayMsg(CGUI::get().m_ui->label_contractsNotiMsg, msg, status);
//      return;
//  }

}
