#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/machine/machine_handler.h"

#include "model_machine_balances.h"


ModelMachineBalances::ModelMachineBalances(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelMachineBalances::rowCount(const QModelIndex & /*parent*/) const
{
  return 8;
}

int ModelMachineBalances::columnCount(const QModelIndex & /*parent*/) const
{
  return 4;
}

QVariant ModelMachineBalances::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:


      switch(index.row())
      {
        case 0:
          switch(index.column())
          {

            case 0:
              return "Minted Coins";
            case 1:
              return CUtils::microPAIToPAI6(m_machine_balances1.m_total_minted_coins);
            case 2:
              return CUtils::microPAIToPAI6(m_machine_balances2.m_total_minted_coins);
            case 3:
              return CUtils::microPAIToPAI6(m_machine_balances3.m_total_minted_coins);
          }
          break;

        case 1:
          switch(index.column())
          {

            case 0:
              return "Spendable Coins";
            case 1:
              return CUtils::microPAIToPAI6(m_machine_balances1.m_total_spendable_coins);
            case 2:
              return CUtils::microPAIToPAI6(m_machine_balances2.m_total_spendable_coins);
            case 3:
              return CUtils::microPAIToPAI6(m_machine_balances3.m_total_spendable_coins);
          }
          break;

        case 2:
          switch(index.column())
          {
            case 0:
              return "Waited Coinbase";
            case 1:
              return CUtils::microPAIToPAI6(m_machine_balances1.m_cb_not_imported_coinbase_value);
            case 2:
              return CUtils::microPAIToPAI6(m_machine_balances2.m_cb_not_imported_coinbase_value);
            case 3:
              return CUtils::microPAIToPAI6(m_machine_balances3.m_cb_not_imported_coinbase_value);
          }
          break;

        case 3:
          switch(index.column())
          {
            case 0:
              return "Waited Normal";
            case 1:
              return CUtils::microPAIToPAI6(m_machine_balances1.m_normal_not_imported_sum);
            case 2:
              return CUtils::microPAIToPAI6(m_machine_balances2.m_normal_not_imported_sum);
            case 3:
              return CUtils::microPAIToPAI6(m_machine_balances3.m_normal_not_imported_sum);
          }
          break;

        case 4:
          switch(index.column())
          {
            case 0:
              return "Waited Treasury";
            case 1:
              return CUtils::microPAIToPAI6(m_machine_balances1.m_waited_treasury_incomes);
            case 2:
              return CUtils::microPAIToPAI6(m_machine_balances2.m_waited_treasury_incomes);
            case 3:
              return CUtils::microPAIToPAI6(m_machine_balances3.m_waited_treasury_incomes);
          }
          break;

        case 5:
          switch(index.column())
          {
            case 0:
              return "Final Balance";
            case 1:
              return CUtils::microPAIToPAI6(m_machine_balances1.m_final_balance);
            case 2:
              return CUtils::microPAIToPAI6(m_machine_balances2.m_final_balance);
            case 3:
              return CUtils::microPAIToPAI6(m_machine_balances3.m_final_balance);
          }
          break;

        case 6:
          switch(index.column())
          {
            case 0:
              return "Double spend";
            case 1:
              return CUtils::microPAIToPAI6(0);
            case 2:
              return CUtils::microPAIToPAI6(0);
            case 3:
              return CUtils::microPAIToPAI6(0);
          }
          break;

        case 7:
          switch(index.column())
          {
            case 0:
              return "Wallet Wealth";
            case 1:
              return CUtils::microPAIToPAI6(m_machine_balances1.m_wallet_wealth_value);
            case 2:
              return CUtils::microPAIToPAI6(m_machine_balances2.m_wallet_wealth_value);
            case 3:
              return CUtils::microPAIToPAI6(m_machine_balances3.m_wallet_wealth_value);
          }
          break;


      }
      break;

    case Qt::BackgroundRole:
      switch(index.row())
      {

      case 0:
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode(CCrypto::keccak256(QString::number(m_machine_balances1.m_total_minted_coins)));
          case 1:
            return CGUI::getBrushByColorCode("f47444");
          case 2:
            return CGUI::getBrushByColorCode("f47444");
          case 3:
            return CGUI::getBrushByColorCode("f47444");
        }
        break;


      case 1:
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances1.m_total_spendable_coins));
          case 1:
            return CGUI::getBrushByColorCode("5454ff");
          case 2:
            return CGUI::getBrushByColorCode("5454ff");
          case 3:
            return CGUI::getBrushByColorCode("5454ff");
        }
        break;


      case 2:
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances1.m_cb_not_imported_coinbase_value));
          case 1:
            return CGUI::getBrushByColorCode("e46434");
          case 2:
            return CGUI::getBrushByColorCode("e46434");
          case 3:
            return CGUI::getBrushByColorCode("e46434");
        }
        break;


      case 3:
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode("ffffff");
          case 1:
            return ((m_machine_balances1.m_normal_not_imported_sum == 0) ? CGUI::getBrushByColorCode("ffffff") : CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances1.m_normal_not_imported_sum)));
          case 2:
            return ((m_machine_balances2.m_normal_not_imported_sum == 0) ? CGUI::getBrushByColorCode("ffffff") : CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances2.m_normal_not_imported_sum)));
          case 3:
            return ((m_machine_balances3.m_normal_not_imported_sum == 0) ? CGUI::getBrushByColorCode("ffffff") : CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances3.m_normal_not_imported_sum)));
        }
        break;


      case 4:
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances1.m_waited_treasury_incomes));
          case 1:
            return CGUI::getBrushByColorCode("f4998a");
          case 2:
            return CGUI::getBrushByColorCode("f4998a");
          case 3:
            return CGUI::getBrushByColorCode("f4998a");
        }
        break;


      case 5:
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode("ffffff");
          case 1:
            return ((m_machine_balances1.m_final_balance == 0) ? CGUI::getBrushByColorCode("ffffff") : CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances1.m_final_balance)));
          case 2:
            return ((m_machine_balances2.m_final_balance == 0) ? CGUI::getBrushByColorCode("ffffff") : CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances2.m_final_balance)));
          case 3:
            return ((m_machine_balances3.m_final_balance == 0) ? CGUI::getBrushByColorCode("ffffff") : CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances3.m_final_balance)));
        }
        break;


      case 6:
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode(CCrypto::keccak256(0));
          case 1:
            return CGUI::getBrushByColorCode("fefefe");
          case 2:
            return CGUI::getBrushByColorCode("fefefe");
          case 3:
            return CGUI::getBrushByColorCode("fefefe");
        }
        break;


      case 7:
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode(CCrypto::keccak256(m_machine_balances1.m_wallet_wealth_value));
          case 1:
            return CGUI::getBrushByColorCode("1da98a");
          case 2:
            return CGUI::getBrushByColorCode("1da98a");
          case 3:
            return CGUI::getBrushByColorCode("1da98a");
        }
        break;

      }
      break;

    case Qt::TextAlignmentRole:
      switch(index.column())
      {
        case 0:
        case 1:
        case 2:
        case 3:
          return Qt::AlignRight;

        default:
          return QVariant();
      }
      break;


    case Qt::ToolTipRole:
      break;


    default:
      break;
  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelMachineBalances::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Title");
      case 1:
        return QString("Current");
      case 2:
        return QString("Prev");
      case 3:
        return QString("Prev prev");
      }
    }

  } else if (role == Qt::ToolTipRole)
  {
//    if (orientation == Qt::Horizontal)
//    {
//      switch (section)
//      {
//      case 0:
//        return QString("Number");
//      case 1:
//        return QString("Prerequisities block");
//      case 2:
//        return QString("Parsing attemps");
//      case 3:
//        return QString("Block hash");
//      case 4:
//        return QString("Create Date");
//      }
//    }
  }

  return QVariant();

}

void ModelMachineBalances::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_machine_balances3 = m_machine_balances2;
  m_machine_balances2 = m_machine_balances1;
  m_machine_balances1 = CMachine::machineBalanceChk();
}


void ModelMachineBalances::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

