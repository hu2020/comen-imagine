#include "stable.h"

#include "lib/dag/state/dag_state_handler.h"
#include "gui/c_gui.h"

#include "model_snapshots.h"

ModelSnapshots::ModelSnapshots(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelSnapshots::rowCount(const QModelIndex & /*parent*/) const
{
  return m_snapshots.size();
}

int ModelSnapshots::columnCount(const QModelIndex & /*parent*/) const
{
  return 4;
}

QVariant ModelSnapshots::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return QString::number(index.row() + 1);

        case 1:
          return CUtils::hash8c(m_snapshots[index.row()].value("nss_label").toString());

        case 2:
          return m_snapshots[index.row()].value("nss_creation_date").toString();

        case 3:
          return "Compare";

      }
      break;

    case Qt::ToolTipRole:
    case Qt::UserRole:
      switch(index.column())
      {
        case 3:
          return m_snapshots[index.row()].value("nss_id").toString();
         break;
      }
      break;


    case Qt::DecorationRole:
      switch(index.column())
      {
        case 3:
          return QCursor(Qt::PointingHandCursor);
         break;
      }
      break;

  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }

  return QVariant();
}

QVariant ModelSnapshots::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Number");
      case 1:
        return QString("Label");
      case 2:
        return QString("Creation Date");
      case 3:
        return QString("Compare local with");

      }
    }
  }
  return QVariant();
}

void ModelSnapshots::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_snapshots = DAGStateHandler::listSnapshots();
}


void ModelSnapshots::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}
