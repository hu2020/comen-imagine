#ifndef MODELDRAFTPLEDGES_H
#define MODELDRAFTPLEDGES_H



#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"



class ModelDraftPledges: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelDraftPledges(QObject *parent);
  virtual ~ModelDraftPledges(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_draft_pledges {};

  void populateData();
  void signalUpdate();

  void draftPledgeDblClicked(const QModelIndex& idx);

};

#endif // MODELDRAFTPLEDGES_H
