#include "stable.h"

#include "lib/ccrypto.h"
#include "gui/c_gui.h"
#include "lib/wallet/wallet.h"

#include "model_wallet_accounts.h"



ModelWalletAccounts::ModelWalletAccounts(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelWalletAccounts::rowCount(const QModelIndex & /*parent*/) const
{
  return m_wallet_controlled_accounts.size();
}

int ModelWalletAccounts::columnCount(const QModelIndex & /*parent*/) const
{
  return 5;
}


QVariant ModelWalletAccounts::data(const QModelIndex &index, int role) const
{
  QVDicT the_coin = m_wallet_controlled_accounts[index.row()];
  QVDicT the_details = m_wallet_controlled_account_details[the_coin.value("wa_address").toString()];

  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;

        case 1:
          return the_coin.value("wa_title").toString();

        case 2:
          return the_coin.value("wa_address").toString();

        case 3:
          return CUtils::microPAIToPAI6(the_details.value("mat_sum").toDouble()) + "(" + QString::number(the_details.value("mat_count").toUInt()) + ")";

        case 4:
          return CUtils::microPAIToPAI6(the_details.value("unmat_sum").toDouble()) + "(" + QString::number(the_details.value("unmat_count").toUInt()) + ")";
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(the_coin.value("wa_address").toString()));
        case 1:
        case 2:
        case 3:
        case 4:
          return CGUI::getBrushByColorCode("f1f5fa");

      }
      break;

    case Qt::TextAlignmentRole:
      switch(index.column())
      {
        case 3:
        case 4:
          return Qt::AlignRight;

        default:
          return QVariant();
      }
      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return "";
          break;


      default:
        return "";
        break;
      }


  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelWalletAccounts::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Account Label");
      case 2:
        return QString("Account Address");
      case 3:
        return QString("Spendable Coins (by PAIs)");
      case 4:
        return QString("Unmatured Coins (by PAIs)");
      }
    }
  } else if (role == Qt::ToolTipRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Number");
      case 1:
        return QString("Prerequisities block");
      case 2:
        return QString("Parsing attemps");
      case 3:
        return QString("Block hash");
      case 4:
        return QString("Create Date");
      }
    }
  }
  return QVariant();
}


void ModelWalletAccounts::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  auto[wallet_controlled_accounts, details] = Wallet::getAddressesList(
    CMachine::getSelectedMProfile(),
    Wallet::stbl_machine_wallet_addresses_fields,
    true);
  m_wallet_controlled_accounts = wallet_controlled_accounts;
  m_wallet_controlled_account_details = details;
}


void ModelWalletAccounts::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}
