#ifndef MODELNEIGHBORS_H
#define MODELNEIGHBORS_H


#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelNeighbors : public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelNeighbors(QObject *parent);
  virtual ~ModelNeighbors(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;


  QVDRecordsT m_public_neighbors = {};
  QVDRecordsT m_private_neighbors = {};
  QHash<QString, QBrush> m_brushes;

  void populateData();
  void signalUpdate();

private slots:


};

#endif // MODELNEIGHBORS_H
