#include "stable.h"

#include "gui/c_gui.h"
#include "lib/services/tmp_contents_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contribute/contribute_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"

#include "model_received_loan_requests.h"

ModelReceivedLoanRequests::ModelReceivedLoanRequests(QObject *parent)
{
Q_UNUSED(parent);
}

int ModelReceivedLoanRequests::rowCount(const QModelIndex & /*parent*/) const
{
  return m_received_loan_requests.size();
}

int ModelReceivedLoanRequests::columnCount(const QModelIndex & /*parent*/) const
{
  return 16;
}

QVariant ModelReceivedLoanRequests::getCellData(const QModelIndex &index) const
{
  QVDicT the_loan_request = m_received_loan_requests[index.row()];
  QJsonObject loan_request_json = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(the_loan_request.value("tc_payload").toString()).content);
  QJsonObject proposal = loan_request_json.value("proposal").toObject();
  QJsonObject pledge = loan_request_json.value("pledgerSignedPledge").toObject();

//  CLog::log("xxxxxxxxxxxx"+ CUtils::serializeJson(loan_request_json));

  switch(index.column())
  {
    case 0:
      return index.row() + 1;

    case 1:
      return proposal.value("dTitle").toString() + " " + proposal.value("dComment").toString() + " " + proposal.value("dTags").toString();
    case 2:
      return proposal.value("contributor").toString();
    case 3:
      return proposal.value("dCDate").toString();
    case 4:
      return QString::number(proposal.value("helpHours").toInt()) + " hour(s) contribution in level " + QString::number(proposal.value("helpLevel").toInt()) + " = " + QString::number(proposal.value("helpHours").toDouble() * proposal.value("helpLevel").toDouble());
    case 5:
      return proposal.value("pollingProfile").toString() + " " + proposal.value("pollingVersion").toString() + " timespan " + QString::number(proposal.value("pTimeframe").toDouble()) + " Hours";

    case 6:
      return pledge.value("pledgerSignDate").toString();
    case 7:
      return CUtils::microPAIToPAI6(pledge.value("redeemTerms").toObject().value("principal").toDouble());
    case 8:
      return pledge.value("redeemTerms").toObject().value("annualInterest").toDouble();
    case 9:
      return pledge.value("redeemTerms").toObject().value("repaymentOffset").toDouble();
    case 10:
      return CUtils::microPAIToPAI6(pledge.value("redeemTerms").toObject().value("repaymentAmount").toDouble());
    case 11:
      return pledge.value("redeemTerms").toObject().value("repaymentSchedule").toDouble();
    case 12:
      return pledge.value("redeemTerms").toObject().value("repaymentsNumber").toDouble();
    case 13:
      return the_loan_request.value("tc_content_status").toString();
    case 14:
      return "X";
    case 15:
      return "Sign";

  }

  return "";
}


QVariant ModelReceivedLoanRequests::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Proposal Title, Comment, Tags");
      case 2:
        return QString("Contributor");
      case 3:
        return QString("Creation Date");
      case 4:
        return QString("Contribute Details");
      case 5:
        return QString("Polling Details");

      case 6:
        return QString("Pledge Date");
      case 7:
        return QString("Principal");
      case 8:
        return QString("Annual Interest");
      case 9:
        return QString("Repayment Offset");
      case 10:
        return QString("Repayment Amount");
      case 11:
        return QString("Repayment Schedule");
      case 12:
        return QString("Repaymenta Number");
      case 13:
        return QString("Status");
      case 14:
        return QString("Delete");
      case 15:
        return QString("Accept & Sign");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("");
      case 1:
        return QString("");
      }
    }
  }
  return QVariant();
}


QVariant ModelReceivedLoanRequests::data(const QModelIndex &index, int role) const
{
  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
          return CGUI::getBrushByColorCode("9ffe62");

        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
          return CGUI::getBrushByColorCode("7fee92");

        case 13:
          return CGUI::getBrushByColorCode("ffffff");

        case 14:
          return CGUI::getBrushByColorCode("ff0000");

        case 15:
          return CGUI::getBrushByColorCode("00ff00");

        return QBrush {};

      }

  }

  return QVariant();
}


void ModelReceivedLoanRequests::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  auto tmp = TmpContentsHandler::searchTmpContents(
    {{"tc_mp_code", CMachine::getSelectedMProfile()},
    {"tc_content_type", CConsts::receivedPLR}});
  m_received_loan_requests = tmp;
}


void ModelReceivedLoanRequests::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelReceivedLoanRequests::receivedLoanRequestDblClicked(const QModelIndex& index)
{
  QVDicT the_loan_request = m_received_loan_requests[index.row()];
  CLog::log("Action on received loan request: " + CUtils::dumpIt(the_loan_request), "app", "info");

  switch(index.column())
  {
    case 14:
      // deleting
      TmpContentsHandler::deleteTmpContent({{"tc_id", the_loan_request.value("tc_id")}});
      signalUpdate();
      return;

    case 15:
      // accepting & signing
      auto[status, msg] = TmpContentsHandler::unBundleAndSignPLR(the_loan_request.value("tc_id").toDouble());
      signalUpdate();
      CGUI::signalUpdateProposalPledgeTransactions();
      CGUI::displayMsg(CGUI::get().m_ui->label_contractsNotiMsg, msg, status);
      return;
  }

}
