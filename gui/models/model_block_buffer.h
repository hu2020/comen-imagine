#ifndef MODELBLOCKBUFFER_H
#define MODELBLOCKBUFFER_H


#include <QObject>
#include <QFileDialog>
#include <QApplication>
#include <QTableView>
#include <QItemDelegate>
#include <QIdentityProxyModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QAbstractTableModel>

#include "stable.h"

class ModelBlockBuffer: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelBlockBuffer(QObject *parent);
  virtual ~ModelBlockBuffer(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_buffered_documents = {};

  void populateData();
  void signalUpdate();
};

#endif // MODELBLOCKBUFFER_H
