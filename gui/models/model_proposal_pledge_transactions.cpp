#include "stable.h"

#include "gui/c_gui.h"
#include "lib/services/tmp_contents_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contribute/contribute_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"

#include "model_proposal_pledge_transactions.h"

ModelProposalPledgeTransactions::ModelProposalPledgeTransactions(QObject *parent)
{
Q_UNUSED(parent);
}

int ModelProposalPledgeTransactions::rowCount(const QModelIndex & /*parent*/) const
{
  return m_ppt_bundles.size();
}

int ModelProposalPledgeTransactions::columnCount(const QModelIndex & /*parent*/) const
{
  return 18;
}

QVariant ModelProposalPledgeTransactions::getCellData(const QModelIndex &index) const
{
  QVDicT the_ppt_bundle = m_ppt_bundles[index.row()];
  QJsonObject ppt_json = CUtils::parseToJsonObj(BlockUtils::unwrapSafeContentForDB(the_ppt_bundle.value("tc_payload").toString()).content);
  QJsonObject proposal = ppt_json.value("proposal").toObject();
  QJsonObject proposalPayerTrx = ppt_json.value("proposalPayerTrx").toObject();
  QJsonObject pledge = ppt_json.value("pledgeeSignedPledge").toObject();
  QJsonObject pledgeDocPayerTrx = ppt_json.value("pledgeDocPayerTrx").toObject();

  CMPAISValueT TP_PROPOSAL = 0;
  for (auto an_out: proposalPayerTrx.value("outputs").toArray())
    if (an_out.toArray()[0].toString() == "TP_PROPOSAL")
      TP_PROPOSAL = an_out.toArray()[1].toDouble();

  CMPAISValueT TP_PLEDGE = 0;
  for (auto an_out: pledgeDocPayerTrx.value("outputs").toArray())
    if (an_out.toArray()[0].toString() == "TP_PLEDGE")
      TP_PLEDGE = an_out.toArray()[1].toDouble();

  //CLog::log("xxxxxModelProposalPledgeTransactionsxxxxxxx"+ CUtils::serializeJson(ppt_json));

  switch(index.column())
  {
    case 0:
      return index.row() + 1;

    case 1:
      return proposal.value("dTitle").toString() + " " + proposal.value("dComment").toString() + " " + proposal.value("dTags").toString();
    case 2:
      return proposal.value("contributor").toString();
    case 3:
      return proposal.value("dCDate").toString();
    case 4:
      return QString::number(proposal.value("helpHours").toInt()) + " hour(s) contribution in level " + QString::number(proposal.value("helpLevel").toInt()) + " = " + QString::number(proposal.value("helpHours").toDouble() * proposal.value("helpLevel").toDouble());
    case 5:
      return proposal.value("pollingProfile").toString() + " " + proposal.value("pollingVersion").toString() + " timespan " + QString::number(proposal.value("pTimeframe").toDouble()) + " Hours";

    case 6:
      return pledge.value("pledgerSignDate").toString();
    case 7:
      return CUtils::microPAIToPAI6(pledge.value("redeemTerms").toObject().value("principal").toDouble());
    case 8:
      return pledge.value("redeemTerms").toObject().value("annualInterest").toDouble();
    case 9:
      return pledge.value("redeemTerms").toObject().value("repaymentOffset").toDouble();
    case 10:
      return CUtils::microPAIToPAI6(pledge.value("redeemTerms").toObject().value("repaymentAmount").toDouble());
    case 11:
      return pledge.value("redeemTerms").toObject().value("repaymentSchedule").toDouble();
    case 12:
      return pledge.value("redeemTerms").toObject().value("repaymentsNumber").toDouble();
    case 13:
      return (CUtils::microPAIToPAI6(TP_PROPOSAL) + " PAIs");
    case 14:
      return (CUtils::microPAIToPAI6(TP_PLEDGE) + " PAIs");
    case 15:
      return the_ppt_bundle.value("tc_content_status").toString();
    case 16:
      return "X";
    case 17:
      return "Push";

  }

  return "";
}


QVariant ModelProposalPledgeTransactions::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Proposal Title, Comment, Tags");
      case 2:
        return QString("Contributor");
      case 3:
        return QString("Creation Date");
      case 4:
        return QString("Contribute Details");
      case 5:
        return QString("Polling Details");

      case 6:
        return QString("Pledge Date");
      case 7:
        return QString("Principal");
      case 8:
        return QString("Annual Interest");
      case 9:
        return QString("Repayment Offset");
      case 10:
        return QString("Repayment Amount");
      case 11:
        return QString("Repayment Schedule");
      case 12:
        return QString("Repaymenta Number");
      case 13:
        return QString("Proposal cost Payer Transaction");
      case 14:
        return QString("Pledge cost Payer Transaction");
      case 15:
        return QString("Status");
      case 16:
        return QString("Delete");
      case 17:
        return QString("Unpack and Puch bundle to block-buffer");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("");
      case 1:
        return QString("");
      }
    }
  }
  return QVariant();
}


QVariant ModelProposalPledgeTransactions::data(const QModelIndex &index, int role) const
{
  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
          return CGUI::getBrushByColorCode("9ffe62");

        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
          return CGUI::getBrushByColorCode("7fee92");

        case 13:
        case 14:
          return CGUI::getBrushByColorCode("7f4ee2");

        case 15:
          return CGUI::getBrushByColorCode("ffffff");

        case 16:
          return CGUI::getBrushByColorCode("ff0000");

        case 17:
          return CGUI::getBrushByColorCode("00ff00");

        return QBrush {};

      }

  }

  return QVariant();
}


void ModelProposalPledgeTransactions::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  auto tmp = TmpContentsHandler::searchTmpContents(
    {{"tc_mp_code", CMachine::getSelectedMProfile()},
    {"tc_content_type", CConsts::BundlePPT}});
  m_ppt_bundles = tmp;
}


void ModelProposalPledgeTransactions::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelProposalPledgeTransactions::proposalPledgeTransactionsDblClicked(const QModelIndex& index)
{
  QVDicT the_ppt_bundle = m_ppt_bundles[index.row()];
  CLog::log("About to delete or Broadcast PPT bundle: " + CUtils::dumpIt(the_ppt_bundle), "app", "info");

  switch(index.column())
  {
    case 16:
      // deleting
      TmpContentsHandler::deleteTmpContent({{"tc_id", the_ppt_bundle.value("tc_id")}});
      signalUpdate();
      return;

    case 17:
      // accepting & signing
      auto[status, msg] = TmpContentsHandler::unBundleAndPushToBlockBufferPPT(the_ppt_bundle.value("tc_id").toDouble());
      signalUpdate();
      CGUI::signalUpdateBlockBuffer();
      CGUI::displayMsg(CGUI::get().m_ui->label_contractsNotiMsg, msg, status);
      return;
  }

}
