#include "model_missed_blocks.h"

#include "lib/dag/missed_blocks_handler.h"


ModelMissedBlocks::ModelMissedBlocks(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelMissedBlocks::rowCount(const QModelIndex & /*parent*/) const
{
  return m_missed_blocks.size();
}

int ModelMissedBlocks::columnCount(const QModelIndex & /*parent*/) const
{
  return 4;
}

QVariant ModelMissedBlocks::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
        return index.row() + 1;
        case 1:
          return m_missed_blocks[index.row()].value("mb_block_hash").toString();
        case 2:
          return CUtils::hash8c(m_missed_blocks[index.row()].value("mb_invoke_attempts").toString());
        case 3:
          return m_missed_blocks[index.row()].value("mb_insert_date").toString();
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
        case 1:
        case 2:
        case 3:
        return QBrush();
      }
      break;
  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelMissedBlocks::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Hash");
      case 2:
        return QString("Attemps");
      case 3:
        return QString("Request Date");
      }
    }
  }
  return QVariant();
}

void ModelMissedBlocks::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_missed_blocks = MissedBlocksHandler::listMissedBlocks({}, {}, {{"mb_invoke_attempts", "DESC"}});
}


void ModelMissedBlocks::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

