#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/machine/machine_handler.h"
#include "lib/services/tmp_contents_handler.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contribute/contribute_handler.h"
#include "lib/services/contracts/pledge/general_pledge_handler.h"

#include "model_machine_contracts.h"

ModelMachineContracts::ModelMachineContracts(QObject *parent)
{
Q_UNUSED(parent);
}

int ModelMachineContracts::rowCount(const QModelIndex & /*parent*/) const
{
  return m_machine_contacts.size();
}

int ModelMachineContracts::columnCount(const QModelIndex & /*parent*/) const
{
  return 18;
}

QVariant ModelMachineContracts::getCellToolTip(const QModelIndex &index) const
{
  QJsonObject the_contract = m_machine_contacts[index.row()].toObject();
  QJsonObject contract_body = the_contract.value("contract_body").toObject();
  QJsonObject complementary_info = the_contract.value("complementaryInfo").toObject();
  CDocHashT pledge_doc_hash = the_contract.value("lc_ref_hash").toString();
  QJsonObject redeem_terms = contract_body.value("redeemTerms").toObject();

  QString tooltip = "";
  tooltip += "\n Annual Interest: " + QString::number(redeem_terms.value("annualInterest").toDouble());
  tooltip += "\n Principal: " + CUtils::microPAIToPAI6(redeem_terms.value("principal").toDouble());
  tooltip += "\n Repayment Amount: " + CUtils::microPAIToPAI6(redeem_terms.value("repaymentAmount").toDouble());
  tooltip += "\n Repayment Offset: " + CUtils::sepNum(redeem_terms.value("repaymentOffset").toDouble());
  tooltip += "\n Repayment Schedule: " + CUtils::sepNum(redeem_terms.value("repaymentSchedule").toDouble());
  tooltip += "\n Repayments Number: " + CUtils::sepNum(redeem_terms.value("repaymentsNumber").toDouble());
  return tooltip;
}

QString generateActionLink(const QJsonArray& actions, int index)
{
  if (actions.size() < (index+1))
    return "";

  return actions[index].toObject().value("actTitle").toString();

  //"actCode": "ByPledgee",
  //"actSubjCode": 14,
  //"actSubjType": "Pledge",
  //"actTitle": "Unpledge By Pledgee"
}

QVariant ModelMachineContracts::getCellData(const QModelIndex &index) const
{
  QJsonObject the_contract = m_machine_contacts[index.row()].toObject();
  QJsonObject contract_body = the_contract.value("contract_body").toObject();
  QJsonObject complementary_info = the_contract.value("complementaryInfo").toObject();
  CDocHashT pledge_doc_hash = the_contract.value("lc_ref_hash").toString();
  QJsonArray actions = the_contract["actions"].toArray();
//  CLog::log("xxxxxModel Machine Contractsxxxxxxx"+ CUtils::serializeJson(the_contract));

  switch(index.column())
  {
    case 0:
      return index.row() + 1;

    case 1:
      return CUtils::hash8c(pledge_doc_hash);
    case 2:
      return the_contract.value("lc_type").toString() + " / " + the_contract.value("lc_class").toString();
    case 3:
      return CUtils::shortBech16(contract_body.value("pledger").toString());
    case 4:
      return contract_body.value("pledgerSignDate").toString();
    case 5:
      return CUtils::shortBech16(contract_body.value("pledgee").toString());
    case 6:
      return contract_body.value("pledgeeSignDate").toString();
    case 7:
      return CUtils::shortBech16(contract_body.value("arbiter").toString());
    case 8:
      return contract_body.value("arbiterSignDate").toString();

    case 9:
      return complementary_info.value("status").toString();
    case 10:
      return complementary_info.value("realActivateDate").toString();
    case 11:
      return complementary_info.value("realCloseDate").toString();

    case 12:
      return generateActionLink(actions, 0);
    case 13:
      return generateActionLink(actions, 1);
    case 14:
      return generateActionLink(actions, 2);
    case 15:
      return generateActionLink(actions, 3);
  }

  return "";
}


QVariant ModelMachineContracts::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Contract");
      case 2:
        return QString("Type");
      case 3:
        return QString("Pledger");
      case 4:
        return QString("Pledger Sign Date");
      case 5:
        return QString("Pledgee)");
      case 6:
        return QString("Pledgee Sign Date");
      case 7:
        return QString("Arbiter");
      case 8:
        return QString("Arbiter Sign Date");

      case 9:
        return QString("Status");
      case 10:
        return QString("Real Activate Date");
      case 11:
        return QString("Real Close Date");


      case 12:
      case 13:
      case 14:
      case 15:
        return QString("Action");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("");
      case 1:
        return QString("");
      }
    }
  }
  return QVariant();
}


QVariant ModelMachineContracts::data(const QModelIndex &index, int role) const
{
  QJsonObject the_contract = m_machine_contacts[index.row()].toObject();
  QJsonObject contract_body = the_contract.value("contract_body").toObject();

  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);

    case Qt::ToolTipRole:
      return getCellToolTip(index);

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 3:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(contract_body.value("pledger").toString()));
        case 5:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(contract_body.value("pledgee").toString()));
        case 7:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(contract_body.value("arbiter").toString()));
        case 0:
        case 1:
        case 2:
        case 4:
        case 6:
        case 8:
          return CGUI::getBrushByColorCode("4f6ea2");

        case 9:
        case 10:
        case 11:
          return CGUI::getBrushByColorCode("7f1e22");

        case 12:
        case 13:
        case 14:
        case 15:
          return CGUI::getBrushByColorCode("7f4ee2");

        return QBrush {};

      }

  }

  return QVariant();
}


void ModelMachineContracts::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  auto tmp = CMachine::searchInMyOnchainContracts({
    {"lc_type", CConsts::DOCUMENT_TYPES::Pledge},
    {"lc_class", CConsts::PLEDGE_CLASSES::PledgeP}});
  m_machine_contacts = tmp;
}


void ModelMachineContracts::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelMachineContracts::machineContractsDblClicked(const QModelIndex& index)
{
  QJsonObject the_contract = m_machine_contacts[index.row()].toObject();
  CLog::log("Dbl Clicked on onchain contract: " + CUtils::serializeJson(the_contract), "app", "info");

  switch(index.column())
  {
    case 16:
      // deleting
      TmpContentsHandler::deleteTmpContent({{"tc_id", the_contract.value("tc_id")}});
      signalUpdate();
      return;

    case 17:
      // push to buffer
      auto[status, msg] = TmpContentsHandler::unBundleAndPushToBlockBufferPPT(the_contract.value("tc_id").toDouble());
      signalUpdate();
      CGUI::displayMsg(CGUI::get().m_ui->label_contractsNotiMsg, msg, status);
      if (status)
        CGUI::signalUpdateBlockBuffer();
      return;
  }

}
