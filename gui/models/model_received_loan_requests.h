#ifndef MODELRECEIVEDLOANREQUESTS_H
#define MODELRECEIVEDLOANREQUESTS_H


#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"



class ModelReceivedLoanRequests: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelReceivedLoanRequests(QObject *parent);
  virtual ~ModelReceivedLoanRequests(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_received_loan_requests {};

  void populateData();
  void signalUpdate();

  void receivedLoanRequestDblClicked(const QModelIndex& idx);

};


#endif // MODELRECEIVEDLOANREQUESTS_H
