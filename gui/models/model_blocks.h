#ifndef MODELBLOCKS_H
#define MODELBLOCKS_H

#include <QObject>
#include <QFileDialog>
#include <QApplication>
#include <QTableView>
#include <QItemDelegate>
#include <QIdentityProxyModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QAbstractTableModel>

#include "stable.h"

class ModelBlocks : public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelBlocks(QObject *parent);
  virtual ~ModelBlocks(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

//  Qt::ItemFlags flags(const QModelIndex &index) const override;
  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const;


  QVDRecordsT m_blocks = {};
  QHash<QString, QBrush> m_brushes;

  void populateData();
  void signalUpdate();

  QString prepareInx(const QModelIndex &index) const;


//signals:
//  void signalData(QVDRecordsT&);

//public slots:
//  void populateData(const QVDRecordsT& blocks);


};

#endif // MODELBLOCKS_H
