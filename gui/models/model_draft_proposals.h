#ifndef MODELDRAFTPROPOSALS_H
#define MODELDRAFTPROPOSALS_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"



class ModelDraftProposals: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelDraftProposals(QObject *parent);
  virtual ~ModelDraftProposals(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  uint8_t m_contributed_level = 6;
  uint64_t m_contributed_hours = 1;
  bool m_is_visible_loan_requests_list = false;

  bool m_is_visible_loan_request_form = false;
  CMPAIValueT m_loan_principals = 0;
  CMPAIValueT m_repayment_amount  = 0;
  double m_interest_rate = 0.05;
  CDocHashT m_selected_proposal = "";
  QString m_selected_proposal_title = "";
  QHash<uint32_t, QVDicT> m_draft_proposals = {};

  bool m_is_visible_send_loan_request = false;
  uint64_t m_selected_draft_pledge = 0;


  void populateData();
  void signalUpdate();

  void draftProposalDblClicked(const QModelIndex& idx);

};

#endif // MODELDRAFTPROPOSALS_H
