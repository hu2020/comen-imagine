#ifndef MODELWIKITITLES_H
#define MODELWIKITITLES_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"


class ModelWikiTitles: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelWikiTitles(QObject *parent);
  virtual ~ModelWikiTitles(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDicT m_selected_wiki {};
  QVDRecordsT m_wiki_titles = {};
  bool m_is_visible_wiki_reg_form = false;
  bool m_is_visible_wiki_edit_form = false;

  void populateData();
  void signalUpdate();
};

#endif // MODELWIKITITLES_H
