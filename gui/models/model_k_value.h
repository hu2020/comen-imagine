#ifndef MODELKVALUE_H
#define MODELKVALUE_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelKValue : public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelKValue(QObject *parent);
  virtual ~ModelKValue(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_kvalue = {};
  QHash<QString, QBrush> m_brushes;

  void populateData();
  void signalUpdate();
};


#endif // MODELKVALUE_H
