#include "model_k_value.h"


#include "lib/k_v_handler.h"

#include "gui/c_gui.h"

ModelKValue::ModelKValue(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelKValue::rowCount(const QModelIndex & /*parent*/) const
{
  return m_kvalue.size();
}

int ModelKValue::columnCount(const QModelIndex & /*parent*/) const
{
  return 3;
}

QVariant ModelKValue::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return m_kvalue[index.row()].value("pq_type").toString();
        case 1:
          return CUtils::hash8c(m_kvalue[index.row()].value("pq_code").toString());
        case 2:
          return m_kvalue[index.row()].value("pq_creation_date").toString();
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(m_kvalue[index.row()].value("pq_code").toString());
        case 1:
        case 2:
          return m_brushes[m_kvalue[index.row()].value("pq_type").toString()];

      }
      break;


  }

  if (role == Qt::DisplayRole)
  {


    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelKValue::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Type");
      case 1:
        return QString("Hash");
      case 2:
        return QString("Create Date");
      }
    }
  }
  return QVariant();
}

void ModelKValue::populateData()
{
//  m_kvalue = KVHandler::
}


void ModelKValue::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}
