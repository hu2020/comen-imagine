#ifndef MODELWALLETACCOUNTS_H
#define MODELWALLETACCOUNTS_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelWalletAccounts: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelWalletAccounts(QObject *parent);
  virtual ~ModelWalletAccounts(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_wallet_controlled_accounts = {};
  QV2DicT m_wallet_controlled_account_details = {};

  void populateData();
  void signalUpdate();
};

#endif // MODELWALLETACCOUNTS_H
