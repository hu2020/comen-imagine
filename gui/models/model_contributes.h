#ifndef MODELCONTRIBUTES_H
#define MODELCONTRIBUTES_H



#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"


class ModelContributes: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelContributes(QObject *parent);
  virtual ~ModelContributes(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QHash<uint32_t, QVDicT> m_contributes = {};
  uint8_t m_contribute_level = 6;
  uint64_t m_contribute_hours = 1;
  std::vector<int> m_contribute_hours_combo {40, 50, 70, 80, 90, 100, 120, 130, 150, 170, 200, 300, 500, 1000, 2000};

  bool m_is_visible_vote_form = false;
  CDocHashT m_selected_proposal = "";
  int8_t m_polling_score = 0;

  void contreibuteDblClicked(const QModelIndex& index);

  void populateData();
  void signalUpdate();

};

#endif // MODELCONTRIBUTES_H
