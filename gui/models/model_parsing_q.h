#ifndef MODELPARSINGQ_H
#define MODELPARSINGQ_H
#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelParsingQ: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelParsingQ(QObject *parent);
  virtual ~ModelParsingQ(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_parsing_q = {};
  QHash<QString, QBrush> m_brushes;

  void populateData();
  void signalUpdate();
};

#endif // MODELPARSINGQ_H
