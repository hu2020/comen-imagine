#include "lib/parsing_q_handler/parsing_q_handler.h"

#include "model_parsing_q.h"
#include "gui/c_gui.h"

ModelParsingQ::ModelParsingQ(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelParsingQ::rowCount(const QModelIndex & /*parent*/) const
{
  return m_parsing_q.size();
}

int ModelParsingQ::columnCount(const QModelIndex & /*parent*/) const
{
  return 5;
}

QVariant ModelParsingQ::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;

        case 1:
          return m_parsing_q[index.row()].value("prerequisites_arr").toJsonArray().size();

        case 2:
          return m_parsing_q[index.row()].value("pq_parse_attempts").toInt();

        case 3:
          return CUtils::hash8c(m_parsing_q[index.row()].value("pq_code").toString());
        case 4:
          return m_parsing_q[index.row()].value("pq_creation_date").toString();
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(m_parsing_q[index.row()].value("pq_code").toString());
        case 1:
        case 2:
        case 3:
        case 4:
          return m_brushes[m_parsing_q[index.row()].value("pq_type").toString()];
      }
      break;

    case Qt::ToolTipRole:
    switch(index.column())
    {
      case 0:
      case 1:
        return CUtils::convertJSonArrayToQStringList(m_parsing_q[index.row()].value("prerequisites_arr").toJsonArray()).join(", ");
        break;


    default:
      return m_parsing_q[index.row()].value("pq_type").toString() + " " + CUtils::hash8c(m_parsing_q[index.row()].value("pq_code").toString()) + " " + m_parsing_q[index.row()].value("pq_creation_date").toString();
      break;
    }


  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelParsingQ::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("No.");
      case 1:
        return QString("Prereq");
      case 2:
        return QString("Attempt");
      case 3:
        return QString("Hash");
      case 4:
        return QString("Create Date");
      }
    }
  } else if (role == Qt::ToolTipRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Number");
      case 1:
        return QString("Prerequisities block");
      case 2:
        return QString("Parsing attemps");
      case 3:
        return QString("Block hash");
      case 4:
        return QString("Create Date");
      }
    }
  }
  return QVariant();
}

struct myclass {
  bool operator() (QVDicT i, QVDicT j)
  {
    return (i.value("prerequisites_arr").toJsonArray().size() < j.value("prerequisites_arr").toJsonArray().size());
  }
} myobject;

void ModelParsingQ::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_parsing_q = ParsingQHandler::searchParsingQ(
    {},
    {"pq_type", "pq_code", "pq_receive_date", "pq_prerequisites", "pq_parse_attempts", "pq_creation_date"},
    {{"pq_connection_type", "ASC"}, {"pq_creation_date", "ASC"}});

  QVDRecordsT parsing_q {};
  for(int32_t inx = 0; inx < m_parsing_q.size(); inx++)
  {
    QVDicT a_row = m_parsing_q[inx];

    QStringList prereqs = {};
    for(QString a_prereq: a_row.value("pq_prerequisites").toString().split(","))
      if (a_prereq != "")
        prereqs.append(a_prereq);

    a_row["prerequisites_arr"] = CUtils::convertQStringListToJSonArray(prereqs);

    parsing_q.push_back(a_row);
  }
  if (parsing_q.size() > 0)
    std::sort(parsing_q.begin(), parsing_q.end(), myobject);

  m_parsing_q = parsing_q;
}


void ModelParsingQ::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

