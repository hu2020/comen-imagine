#include "stable.h"

#include "lib/dag/state/dag_state_handler.h"
#include "gui/c_gui.h"

#include "model_snap_compare.h"

ModelSnapCompare::ModelSnapCompare(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelSnapCompare::rowCount(const QModelIndex & /*parent*/) const
{
  return m_compare_report.m_compared_tables.size() + 1;
}

int ModelSnapCompare::columnCount(const QModelIndex & /*parent*/) const
{
  return 4;
}

QVariant ModelSnapCompare::getTablesReport(const QModelIndex &index, int role) const
{
  int index_row = index.row();
  int index_col = index.column();

  QString key_ref = m_compare_report.m_compared_tables[index_row];
  QString red = "ff0000";
  QString green = "00ff00";

  QString extra_tables_in_local = "Extra tables in Local";
  if (m_compare_report.m_addition_keys_in_local.size()>0)
    extra_tables_in_local +=  " ("+QString::number(m_compare_report.m_addition_keys_in_local.size())+") " + m_compare_report.m_addition_keys_in_local.join(", ");

  QString extra_tables_in_remote = "Extra tables in Remote";
  if (m_compare_report.m_addition_keys_in_remote.size()>0)
    extra_tables_in_remote +=  " ("+QString::number(m_compare_report.m_addition_keys_in_remote.size())+") " + m_compare_report.m_addition_keys_in_remote.join(", ");

  int first_part_height = 1;
  QString bgc_have_same_keys = m_compare_report.m_have_same_keys ? green : red;
  QString bgc_addition_keys_in_local = m_compare_report.m_addition_keys_in_local.size() > 0 ? red : green;
  QString bgc_addition_keys_in_remote = m_compare_report.m_addition_keys_in_remote.size() > 0 ? red : green;

  switch(index_row)
  {
    case 0:
      switch (index_col)
      {
        case 0:
          switch(role)
          {
            case Qt::DisplayRole:
              return "Same tables";
            case Qt::ToolTipRole:
              return "Both local and remote have same key to comparing";
            case Qt::BackgroundRole:
              return CGUI::getBrushByColorCode(bgc_have_same_keys);
          }

        case 1:
          switch(role)
          {
            case Qt::DisplayRole:
              return "Extra tables in Local";
            case Qt::ToolTipRole:
              return extra_tables_in_local;
            case Qt::BackgroundRole:
              return CGUI::getBrushByColorCode(bgc_addition_keys_in_local);
          }

        case 2:
          switch(role)
          {
            case Qt::DisplayRole:
              return extra_tables_in_remote;
            break;
            case Qt::ToolTipRole:
              return "Remote report contains more tables to compare";
            case Qt::BackgroundRole:
              return CGUI::getBrushByColorCode(bgc_addition_keys_in_remote);
          }
      }

  }

  if ((0 < index_row) && (index_row < (m_compare_report.m_compared_tables.size() + 1)))
  {
    QString the_key_ref = m_compare_report.m_compared_tables[index_row - first_part_height];

    QString bgc_are_same_in_records_count = (m_compare_report.m_are_same_in_records_count[the_key_ref] == 0) ? green : red;
    QString title_are_same_in_records_count = "Same in records";
    if (m_compare_report.m_are_same_in_records_count[the_key_ref] != 0)
      title_are_same_in_records_count += " (" + QString::number(m_compare_report.m_are_same_in_records_count[the_key_ref]) + ")";

    QString title_are_same_in_content = "Same in contents";
    QString hint_are_same_in_content = "Diefferences: ";
    if (m_compare_report.m_discrepancy_in_content[the_key_ref].size() != 0)
    {
      title_are_same_in_content += " (" + QString::number(m_compare_report.m_discrepancy_in_content[the_key_ref].size()) + ")";
      hint_are_same_in_content += m_compare_report.m_discrepancy_in_content[the_key_ref].join(", ");
    }

      switch (index_col)
      {
        case 0:
          switch(role) {
            case Qt::DisplayRole:
              return the_key_ref;
            case Qt::ToolTipRole:
              return "";
            case Qt::BackgroundRole:
              return CGUI::getBrushByColorCode("e9e9e9");
          }

        case 1:
          switch(role) {
            case Qt::DisplayRole:
              return title_are_same_in_records_count;
            case Qt::ToolTipRole:
              return "";
            case Qt::BackgroundRole:
              return CGUI::getBrushByColorCode(bgc_are_same_in_records_count);
          }
        case 2:
          switch(role) {
            case Qt::DisplayRole:
              return title_are_same_in_content;
            case Qt::ToolTipRole:
              return hint_are_same_in_content;
            case Qt::BackgroundRole:
              return CGUI::getBrushByColorCode(m_compare_report.m_are_same_in_content[the_key_ref] ? green : red);
          }
      }
  }


  return QVariant();

}

QVariant ModelSnapCompare::data(const QModelIndex &index, int role) const
{
  if(index.row() < m_compare_report.m_compared_tables.size())
    return getTablesReport(index, role);

//  switch(role) {
//    case Qt::DisplayRole:
//      switch(index.column())
//      {
//        case 0:
//          return QString::number(index.row() + 1);

//        case 1:
//          return CUtils::hash8c(m_compare_report[index.row()].value("nss_label").toString());

//        case 2:
//          return m_compare_report[index.row()].value("nss_creation_date").toString();

//        case 3:
//          return "Compare";

//      }
//      break;

//    case Qt::ToolTipRole:
//    case Qt::UserRole:
//      switch(index.column())
//      {
//        case 3:
//          return m_compare_report[index.row()].value("nss_id").toString();
//         break;
//      }
//      break;


//    case Qt::DecorationRole:
//      switch(index.column())
//      {
//        case 3:
//          return QCursor(Qt::PointingHandCursor);
//         break;
//      }
//      break;

//  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }

  return QVariant();
}

QVariant ModelSnapCompare::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Table");
      case 1:
        return QString("Local");
      case 2:
        return QString("Remote");
      case 3:
        return QString("-");

      }
    }
  }
  return QVariant();
}

void ModelSnapCompare::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;
}


void ModelSnapCompare::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelSnapCompare::signalUpdate2(const CompareState& report)
{
  beginResetModel();
  m_compare_report = report;
  endResetModel();
  CGUI::get().m_ui->textBrowser_compare->setText(report.m_nodes_discrepancies.join("\n"));

}
