#include "stable.h"

#include "gui/c_gui.h"

#include "model_block_buffer.h"



ModelBlockBuffer::ModelBlockBuffer(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelBlockBuffer::rowCount(const QModelIndex & /*parent*/) const
{
  return m_buffered_documents.size();
}

int ModelBlockBuffer::columnCount(const QModelIndex & /*parent*/) const
{
  return 7;
}

QVariant ModelBlockBuffer::data(const QModelIndex &index, int role) const
{
  QVDicT the_doc = m_buffered_documents[index.row()];

  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;

        case 1:
          return the_doc.value("bd_doc_type").toString() + " / " + the_doc.value("bd_doc_class").toString();

        case 2:
          return CUtils::hash8c(the_doc.value("bd_doc_hash").toString());

        case 3:
          return CUtils::sepNum(the_doc.value("bd_doc_len").toDouble());

        case 4:
          return CUtils::microPAIToPAI6(the_doc.value("bd_dp_cost").toDouble());

        case 5:
          return the_doc.value("bd_insert_date").toString();

        case 6:
          return "X";

      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return CGUI::getBrushByDocType(the_doc.value("bd_doc_type").toString());
          break;
        default:
        return QBrush {};

      }
      break;

    case Qt::TextAlignmentRole:
      switch(index.column())
      {
        case 3:
        case 4:
          return Qt::AlignRight;

        default:
          return QVariant();
      }
      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return "";
          break;


        default:
          return "";
          break;
      }
      break;



    }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelBlockBuffer::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Doc Type / Class");
      case 2:
        return QString("Hash");
      case 3:
        return QString("Doc Length");
      case 4:
        return QString("Doc Cost");
      case 5:
        return QString("Insert Date");
      case 6:
        return QString("Delete");

      }
    }
  }
  return QVariant();
}

void ModelBlockBuffer::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_buffered_documents = CMachine::searchBufferedDocs(
    {},
    {"bd_id", "bd_insert_date", "bd_doc_hash", "bd_doc_type", "bd_doc_class", "bd_dp_cost", "bd_doc_len"},
    {{"bd_insert_date", "ASC"},
    {"bd_dp_cost", "DESC"}});
}


void ModelBlockBuffer::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}
