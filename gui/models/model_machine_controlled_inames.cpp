#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"

#include "model_machine_controlled_inames.h"

ModelMachineControlledINames::ModelMachineControlledINames(QObject *parent)
{
Q_UNUSED(parent);
}

int ModelMachineControlledINames::rowCount(const QModelIndex & /*parent*/) const
{
  return m_machine_controlled_inames.size();
}

int ModelMachineControlledINames::columnCount(const QModelIndex & /*parent*/) const
{
  return 7;
}

QVariant ModelMachineControlledINames::getCellToolTip(const QModelIndex &index) const
{
  QVDicT the_iname = m_machine_controlled_inames[index.row()];

  QString tooltip = the_iname.value("nb_comment").toString();
  return tooltip;
}

QVariant ModelMachineControlledINames::getCellData(const QModelIndex &index) const
{
  QVDicT the_iname = m_machine_controlled_inames[index.row()];

  switch(index.column())
  {
    case 0:
      return index.row() + 1;

    case 1:
      return the_iname.value("imn_name").toString();
    case 2:
      return the_iname.value("imn_owner").toString();
    case 3:
      return the_iname.value("imn_register_date").toString();
    case 4:
      return the_iname.value("nb_title").toString();
    case 5:
      return the_iname.value("nb_creation_date").toString();
    case 6:
      return the_iname.value("nb_status").toString();

  }

  return "";
}


QVariant ModelMachineControlledINames::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("iName");
      case 2:
        return QString("Owner");
      case 3:
        return QString("Register Date");
      case 4:
        return QString("iPGP Binding Label");
      case 5:
        return QString("iPGP Binding Date");
      case 6:
        return QString("iPGP Binding Status");

      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 3:
        return QString("");
      }
    }
  }
  return QVariant();
}


QVariant ModelMachineControlledINames::data(const QModelIndex &index, int role) const
{
  QVDicT the_iname = m_machine_controlled_inames[index.row()];

  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);

    case Qt::ToolTipRole:
      return getCellToolTip(index);

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(the_iname.value("imn_hash").toString());
        case 2:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(the_iname.value("imn_owner").toString()));

        return QBrush {};

      }

  }

  return QVariant();
}


void ModelMachineControlledINames::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_machine_controlled_inames = {};

  auto[res_status, res_msg, inames, bindings_info] = INameHandler::getMachineControlledINames(
  {},
  {"imn_owner", "imn_name", "imn_hash", "imn_register_date"},
  {{"imn_name", "ASC"}},
  true);

  for (QVDicT an_iname: inames)
  {
    CDocHashT imn_hash = an_iname["imn_hash"].toString();
    if (bindings_info.m_binding_dict.keys().contains(imn_hash))
    {
      if (bindings_info.m_binding_dict[imn_hash].keys().contains(CConsts::BINDING_TYPES::IPGP))
      {
        for (auto an_iPGP_binding_: bindings_info.m_binding_dict[imn_hash][CConsts::BINDING_TYPES::IPGP].toVariantList())
        {
          QJsonObject an_iPGP_binding = an_iPGP_binding_.toJsonObject();
          an_iname["nb_title"] = an_iPGP_binding["nb_title"].toString();
          an_iname["nb_comment"] = an_iPGP_binding["nb_comment"].toString();
          an_iname["nb_status"] = an_iPGP_binding["nb_status"].toString();
          an_iname["nb_creation_date"] = an_iPGP_binding["nb_creation_date"].toString();
          m_machine_controlled_inames.push_back(an_iname);
        }
      }
    }else{
      m_machine_controlled_inames.push_back(an_iname);
    }

  }

}


void ModelMachineControlledINames::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelMachineControlledINames::machineControlledINameDblClicked(const QModelIndex& index)
{
//  QJsonObject the_iname = m_machine_controlled_inames[index.row()].toObject();
//  CLog::log("Dbl Clicked on onchain contract: " + CUtils::serializeJson(the_iname), "app", "info");

//  switch(index.column())
//  {
//    case 16:
//      // deleting
//      TmpContentsHandler::deleteTmpContent({{"tc_id", the_iname.value("tc_id")}});
//      signalUpdate();
//      return;

//    case 17:
//      // accepting & signing
//      auto[status, msg] = TmpContentsHandler::unBundleAndPushToBlockBufferPPT(the_iname.value("tc_id").toDouble());
//      signalUpdate();
////      CGUI::signalUpdateBlockBuffer();
//      CGUI::displayMsg(CGUI::get().m_ui->label_contractsNotiMsg, msg, status);
//      return;
//  }

}
