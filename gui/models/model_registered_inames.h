#ifndef MODELREGISTEREDINAMES_H
#define MODELREGISTEREDINAMES_H


#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelRegisteredINames: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelRegisteredINames(QObject *parent);
  virtual ~ModelRegisteredINames(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;
  QVariant getCellToolTip(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_registered_inames {};

  void populateData();
  void signalUpdate();

  void inameDblClicked(const QModelIndex& idx);

};

#endif // MODELREGISTEREDINAMES_H
