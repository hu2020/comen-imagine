#ifndef MODELSNAPCOMPARE_H
#define MODELSNAPCOMPARE_H


#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"
#include "lib/dag/state/dag_state_handler.h"

class ModelSnapCompare: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelSnapCompare(QObject *parent);
  virtual ~ModelSnapCompare(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  CompareState m_compare_report = {};

  QVariant getTablesReport(const QModelIndex &index, int role) const;

  void populateData();
  void signalUpdate();
  void signalUpdate2(const CompareState&);
};

#endif // MODELSNAPCOMPARE_H
