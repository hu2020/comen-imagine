#include "stable.h"

#include "gui/c_gui.h"
#include "lib/services/contribute/contribute_handler.h"

#include "model_contributes.h"

ModelContributes::ModelContributes(QObject *parent)
{
Q_UNUSED(parent);
}


int ModelContributes::rowCount(const QModelIndex & /*parent*/) const
{
  return m_contributes.keys().size();
}

int ModelContributes::columnCount(const QModelIndex & /*parent*/) const
{
  return 11;
}


QVariant ModelContributes::getCellData(const QModelIndex &index) const
{
  QVDicT the_contribute = m_contributes[index.row()];
  uint32_t mod_ = index.row() % 10;
  switch(index.column())
  {
    case 0:
      if (mod_==0)
        return the_contribute.value("proposal_number").toInt();
      return "";

    case 1:
      switch(mod_)
      {
      case 0:
        return "Contributor: ";
      case 1:
        return "Title: ";
      case 2:
        return "Proposal hash: ";
      case 3:
        return "Description: ";
      case 4:
        return "Tags: ";
      case 5:
        return "Contribute Hours: ";
      case 6:
        return "Usefulness Level: ";
      case 7:
        return "Polling Shares: ";
      case 8:
        return "Final Status: ";
      case 9:
        return "";

      default:
        return "undef mode_";
      }
      break;

    case 2:
      switch(mod_)
      {
        case 0:
          return the_contribute.value("pr_contributor_account").toString();
        case 1:
          return the_contribute.value("pr_title").toString();
        case 2:
          return the_contribute.value("pr_hash").toString();
        case 3:
          return the_contribute.value("pr_descriptions").toString();
        case 4:
          return the_contribute.value("pr_tags").toString();
        case 5:
          return the_contribute.value("pr_help_hours").toInt();
        case 6:
          return the_contribute.value("pr_help_level").toInt();
        case 7:
          return the_contribute.value("proposed_shares").toInt();
        case 8:
          return the_contribute.value("final_status_text").toString();


        case 9:
          return "";
      }
      break;

    case 3:
      switch(mod_)
      {
      case 0:
        return "Voting Status: ";
      case 1:
        return "Is Concluded: ";
      case 2:
        return "Polling Hash: ";
      case 3:
        return "";
      case 4:
        return "Start Date: ";
      case 5:
        return "End Time(Yes): ";
      case 6:
        return "End Time(No): ";
      case 7:
        return "";
      case 8:
        return "";
      case 9:
        return "";

      default:
        return "undef mode_";
      }
      break;

    case 4:
      switch(mod_)
      {
        case 0:
          return the_contribute.value("pll_status").toString();
        case 1:
          return the_contribute.value("pll_ct_done").toString() + " date(" + the_contribute.value("the_conclude_date").toString() + ") ";
        case 2:
          return the_contribute.value("pll_hash").toString();
        case 3:
          return "";
        case 4:
          return the_contribute.value("pll_start_date").toString();
        case 5:
          return the_contribute.value("end_y").toString();
        case 6:
          return the_contribute.value("end_n").toString();
        case 7:
          return "";
        case 8:
          return "";
        case 9:
          return "";
      }
      break;

    case 5:
      switch(mod_)
      {
      case 0:
        return "Polling Profile: ";
      case 1:
        return "Perform Type: ";
      case 2:
        return "Vote Counting Method: ";
      case 3:
        return "";
      case 4:
        return "Yes: ";
      case 5:
        return "Abstin: ";
      case 6:
        return "No:";
      case 7:
        return "";
      case 8:
        return "Refresh Polling Result";
      case 9:
        return "";

      default:
        return "undef mode_";
      }
      break;

    case 6:
      switch(mod_)
      {
        case 0:
          return the_contribute.value("ppr_name").toString();
        case 1:
          return the_contribute.value("ppr_perform_type").toString();
        case 2:
          return the_contribute.value("ppr_votes_counting_method").toString();
        case 3:
          return "Voters";
        case 4:
          return CUtils::sepNum(the_contribute.value("pll_y_count").toInt());
        case 5:
          return CUtils::sepNum(the_contribute.value("pll_a_count").toInt());
        case 6:
          return CUtils::sepNum(the_contribute.value("pll_n_count").toInt());
        case 7:
          return "";
        case 8:
          return "See Ballots Details";
        case 9:
          return "";
      }
      break;

    case 7:
      switch(mod_)
      {
        case 0:
          return "";
        case 1:
          return "";
        case 2:
          return "";
        case 3:
          return "Voters Shares";
        case 4:
          return CUtils::sepNum(the_contribute.value("pll_y_shares").toInt());
        case 5:
          return CUtils::sepNum(the_contribute.value("pll_a_shares").toInt());
        case 6:
          return CUtils::sepNum(the_contribute.value("pll_n_shares").toInt());
        case 7:
          return "";
        case 8:
          return "";
        case 9:
          return "";
      }
      break;

    case 8:
      switch(mod_)
      {
        case 0:
          return "";
        case 1:
          return "";
        case 2:
          return "";
        case 3:
          return "Effective Vote";
        case 4:
          return CUtils::sepNum(the_contribute.value("pll_y_gain").toInt());
        case 5:
          return CUtils::sepNum(the_contribute.value("pll_a_gain").toInt());
        case 6:
          return CUtils::sepNum(the_contribute.value("pll_n_gain").toInt());
        case 7:
          return "";
        case 8:
          return "";
        case 9:
          return "";
      }
      break;

    case 9:
      switch(mod_)
      {
        case 0:
          return "";
        case 1:
          return "";
        case 2:
          return "";
        case 3:
          return "Votes Value";
        case 4:
          return CUtils::sepNum(the_contribute.value("pll_y_value").toInt());
        case 5:
          return CUtils::sepNum(the_contribute.value("pll_a_value").toInt());
        case 6:
          return CUtils::sepNum(the_contribute.value("pll_n_value").toInt());
        case 7:
          return "";
        case 8:
          return "";
        case 9:
          return "";
      }
      break;

    case 10:
      switch(mod_)
      {
        case 0:
          return "";
        case 1:
          return "";
        case 2:
          return "";
        case 3:
          return "Your Vote Coefficient";
        case 4:
          return QString::number(the_contribute.value("your_yes_gain").toInt()) + " %";
        case 5:
          return QString::number(the_contribute.value("your_abstain_gain").toInt()) + " %";
        case 6:
          return QString::number(the_contribute.value("your_no_gain").toInt()) + " %";
        case 7:
          return "";
        case 8:
          return "Vote";
        case 9:
          return "";
      }
      break;


  }

  return "";
}


QVariant ModelContributes::data(const QModelIndex &index, int role) const
{
  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  QVDicT the_contribute = m_contributes[index.row()];

  QString row_color_proposal = (index.row() % 2 == 0) ? "ffffff" : "eaebec";
  QString row_color_polling = (index.row() % 2 == 0) ? "afffff" : "aaebec";
  QString row_color_votes = (index.row() % 2 == 0) ? "8fffdf" : "8aabec";

  uint32_t mod_ = index.row() % 10;

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);
      break;

    case Qt::BackgroundRole:
      if (mod_ == 9 )
      {
        return CGUI::getBrushByColorCode("101010");

      }else{
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode(CUtils::hash6c(the_contribute.value("pr_hash").toString()));
            break;

          case 1:
          case 2:
            if (mod_==8)
              return CGUI::getBrushByColorCode(the_contribute.value("final_status_color").toString());
            return CGUI::getBrushByColorCode(row_color_proposal);
          break;

          case 3:
          case 4:
            return CGUI::getBrushByColorCode(row_color_polling);
          break;

          case 5:
            if ((2<mod_) && (mod_<8))
              return CGUI::getBrushByColorCode("e3a97b");
            if (mod_ == 8)
              return CGUI::getBrushByColorCode("cfd400");
            return CGUI::getBrushByColorCode(row_color_votes);
          case 6:
            if (mod_ == 8)
              return CGUI::getBrushByColorCode("049f6e");
          case 7:
          case 8:
          case 9:
          case 10:
            if (mod_== 4)
              return CGUI::getBrushByColorCode("00ee00");
            if (mod_== 5)
              return CGUI::getBrushByColorCode("ffffff");
            if (mod_== 6)
              return CGUI::getBrushByColorCode("ee0000");
            if ((2<mod_) && (mod_<8))
              return CGUI::getBrushByColorCode("e3a97b");
            if ((mod_ == 8) && (index.column()==10))
              return CGUI::getBrushByColorCode("049f6e");

            return CGUI::getBrushByColorCode(row_color_votes);
          break;

        default:
          return CGUI::getBrushByColorCode(row_color_proposal);

        }

      }

      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return "";
        case 2:
          if (mod_ == 8)
            return the_contribute.value("win_complementary_tip").toString();

        case 10:
          if (mod_ == 3)
            return "Your vote gain is based on your shares & the time passed of polling start";

          return "";
          break;


        default:
          return "";
          break;
      }
      break;

    case Qt::TextAlignmentRole:
      switch(index.column())
      {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
          return QVariant();
          break;
        case 5:
          if ((3<mod_) && (mod_<7))
            return Qt::AlignRight;
          return QVariant();
          break;
      }
      break;

  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelContributes::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Proposal");
      case 2:
        return QString("Info");
      case 3:
        return QString("Polling");
      case 4:
        return QString("Info");
      case 5:
        return QString("Votes");
      case 6:
        return QString("Details");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return "";
      }
    }
  }
  return QVariant();
}

void ModelContributes::contreibuteDblClicked(const QModelIndex& index)
{
  QVDicT the_contribute = m_contributes[index.row()];
  CDocHashT proposal_hash = the_contribute.value("pr_hash").toString();
  CLog::log("Clicked on the_contribute proposal hash(" + CUtils::hash8c(proposal_hash) + ")");

  uint32_t mod_ = index.row() % 10;

  if (mod_ == 8)
  {
    switch(index.column())
    {
      case 5:
        PollingHandler::maybeUpdatePollingStatByProposalHash(proposal_hash);
        return;

      case 10:
        CGUI::toggleContributeVoteForm(proposal_hash);
        return;
    }
  }
  switch(index.column())
  {
    case 0:
      if (mod_==0)
        return;
      return;

    case 1:
      switch(mod_)
      {
      case 0:
        return;
      case 1:
        return;
      case 2:
        return;
      case 3:
        return;
      case 4:
        return;
      case 5:
        return;
//      case 6:
//        CLog::log("Request loan for " + proposal.value("pd_hash").toString());
//        CGUI::toggleLoanRequestForm(proposal.value("pd_hash").toString());
//        return;
//      case 7:
//        return;
      case 8:
        return;
      case 9:
        return;
      case 10:
        return;

//      default:
//        return;
//      }
//      break;

//    case 2:
//      switch(mod_)
//      {
//        case 6:
//          CLog::log("Sign and pay Proposal Costs " + proposal.value("pd_hash").toString());
//          return;
//        case 7:
//          CLog::log("Delete proposal " + proposal.value("pd_hash").toString());
//          ProposalHandler::deleteDraftProposal(proposal.value("pd_hash").toString());
//          CGUI::signalUpdateDraftProposals();
//          return;

//        case 8:
//          return;
//        case 9:
//          return;
      }
      break;
  }

}


void ModelContributes::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_contributes = ContributeHandler::getOnchainProposalsList(CMachine::getBackerAddress());
}


void ModelContributes::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

