#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/services/free_docs/demos/demos_handler.h"

#include "model_agora_titles.h"


ModelAgoraTitles::ModelAgoraTitles(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelAgoraTitles::rowCount(const QModelIndex & /*parent*/) const
{
  return m_agora_titles.size();
}

int ModelAgoraTitles::columnCount(const QModelIndex & /*parent*/) const
{
  return 3;
}


QVariant ModelAgoraTitles::data(const QModelIndex &index, int role) const
{
  QVDicT agora_info = m_agora_titles[index.row()];

  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;

        case 1:
          return agora_info.value("ag_iname").toString();

        case 2:
          return agora_info.value("ag_title").toString();

      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(CUtils::hash6c(agora_info.value("ag_hash").toString()));

        case 1:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(agora_info.value("ag_iname").toString()));

        case 2:
          return QBrush {};

      }
      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return "";
          break;


        default:
          return "parent: " + m_agora_titles[index.row()].value("ag_parent").toString();
          break;
      }
      break;



    }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelAgoraTitles::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Agora Title");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("");
      case 1:
        return QString("");
      }
    }
  }
  return QVariant();
}


void ModelAgoraTitles::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_agora_titles = DemosHandler::makeTitlesTree("");
}


void ModelAgoraTitles::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

