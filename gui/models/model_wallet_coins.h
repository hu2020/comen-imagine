#ifndef MODELWALLETCOINS_H
#define MODELWALLETCOINS_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"


class ModelWalletCoins: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelWalletCoins(QObject *parent);
  virtual ~ModelWalletCoins(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  Qt::ItemFlags flags(const QModelIndex & index) const override;
  bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_wallet_controlled_coins = {};
  QHash<CCoinCodeT, bool> m_trx_selected_coins_to_spend = {};
  CMPAIValueT m_trx_sending_amount = 0;
  CMPAIValueT m_trx_fee_amount = 1000;
  CMPAISValueT m_trx_spendable_amount = 0;
  CMPAISValueT m_trx_change_back = 0;

  void populateData();
  void signalUpdate();

  void recalcTransactionForm();
  void resetSelectedWalletCoins();

};

#endif // MODELWALLETCOINS_H
