#ifndef MODELMACHINECONTRACTS_H
#define MODELMACHINECONTRACTS_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelMachineContracts: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelMachineContracts(QObject *parent);
  virtual ~ModelMachineContracts(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;
  QVariant getCellToolTip(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QJsonArray m_machine_contacts {};

  void populateData();
  void signalUpdate();

  void machineContractsDblClicked(const QModelIndex& idx);

};

#endif // MODELMACHINECONTRACTS_H
