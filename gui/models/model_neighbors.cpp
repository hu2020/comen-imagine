#include "lib/machine/machine_handler.h"

#include "model_neighbors.h"

ModelNeighbors::ModelNeighbors(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelNeighbors::rowCount(const QModelIndex & /*parent*/) const
{
  return m_public_neighbors.size();
}

int ModelNeighbors::columnCount(const QModelIndex & /*parent*/) const
{
  return 6;
}

QVariant ModelNeighbors::data(const QModelIndex &index, int role) const
{
  QVDicT the_row = m_public_neighbors[index.row()];
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return the_row.value("n_email").toString();
        case 1:
          return the_row.value("n_pgp_public_key").toString();
        case 2:
          return "Shakehand";
        case 3:
          return "";
        case 4:
          return "";
        case 5:
          return "Remove";
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return QBrush();
        case 1:
        case 2:
          return m_brushes[the_row.value("pq_type").toString()];

      }
      break;

    case Qt::StatusTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return "";
          break;
        case 2:
        case 5:
          return the_row.value("n_id").toString();
          break;

      }
      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return "";
        break;
        case 2:
          return "Doubleclick to send neighborhood request";

      }
      break;


  }

  if (role == Qt::DisplayRole)
  {


    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelNeighbors::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Email");
      case 1:
        return QString("Public Key");
      case 2:
        return QString("Do Shake hand");
      case 3:
        return QString("Discover");
      case 4:
        return QString("Connect People");
      case 5:
        return QString("Disconnect");
      }
    }
  }
  return QVariant();
}

void ModelNeighbors::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_public_neighbors = CMachine::getNeighbors(CConsts::PUBLIC);
  m_private_neighbors = CMachine::getNeighbors(CConsts::PRIVATE);
}


void ModelNeighbors::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}



