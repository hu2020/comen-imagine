#include "stable.h"

#include <QTimer>

#include "lib/dag/dag.h"
#include "gui/c_gui.h"

#include "model_blocks.h"

ModelBlocks::ModelBlocks(QObject *parent)
{
//  QTimer* timer = new QTimer(this);
//  timer->setInterval(3000);
//  connect(timer, SIGNAL(timeout()) , this, SLOT(timerHit()));
//  timer->start();
}

int ModelBlocks::rowCount(const QModelIndex&  /*parent*/) const
{
  return m_blocks.size();
}

int ModelBlocks::columnCount(const QModelIndex&  /*parent*/) const
{
  return 3;
}

QString ModelBlocks::prepareInx(const QModelIndex& index) const
{
  QString imported_coins = "";
  if (QStringList {
      CConsts::BLOCK_TYPES::Coinbase,
      CConsts::BLOCK_TYPES::Normal,
      CConsts::BLOCK_TYPES::RpBlock}.contains(m_blocks[index.row()].value("b_type").toString()) &&
      (m_blocks[index.row()].value("b_utxo_imported").toString() == CConsts::NO))
        imported_coins = " *";

  return QString::number(index.row() + 1) + imported_coins;
}

QVariant ModelBlocks::data(const QModelIndex& index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return prepareInx(index);
        case 1:
          return CUtils::hash8c(m_blocks[index.row()].value("b_hash").toString());

        case 2:
          return m_blocks[index.row()].value("b_creation_date").toString();
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(m_blocks[index.row()].value("b_hash").toString());
        case 1:
        case 2:
          return m_brushes[m_blocks[index.row()].value("b_type").toString()];

      }
      break;

  case Qt::ToolTipRole:
    return m_blocks[index.row()].value("b_type").toString() + " " + m_blocks[index.row()].value("b_hash").toString() + " " + m_blocks[index.row()].value("b_creation_date").toString();



  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelBlocks::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Hash");
      case 2:
        return QString("Create Date");
      }
    }
  }
  return QVariant();
}

void ModelBlocks::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_blocks = DAG::loadCachedBlocks();
}

void ModelBlocks::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}


//void ModelBlocks::signalUpdate()
//{
//  if (!CMachine::canStartLazyLoadings())
//    return;

//  QVDRecordsT blocks = DAG::searchInDAG(
//    {},
//    {"b_type", "b_hash", "b_creation_date", "b_utxo_imported"},
//    {{"b_creation_date", "ASC"},
//    {"b_type", "DESC"}});    // TODO: optimize it ASAP

//  emit signalData(blocks);
//}

//void ModelBlocks::populateData(const QVDRecordsT& blocks)
//{
//  beginResetModel();
//  m_blocks = blocks;
//  endResetModel();
//}
