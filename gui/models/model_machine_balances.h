#ifndef MODELMACHINEBALANCES_H
#define MODELMACHINEBALANCES_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "lib/machine/machine_handler.h"

class MachineTransientBalances;

class ModelMachineBalances : public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelMachineBalances(QObject *parent);
  virtual ~ModelMachineBalances(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  MachineTransientBalances m_machine_balances1;
  MachineTransientBalances m_machine_balances2;
  MachineTransientBalances m_machine_balances3;

  void populateData();
  void signalUpdate();
};
#endif // MODELMACHINEBALANCES_H
