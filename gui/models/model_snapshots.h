#ifndef MODELSNAPSHOTS_H
#define MODELSNAPSHOTS_H


#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelSnapshots: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelSnapshots(QObject *parent);
  virtual ~ModelSnapshots(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_snapshots = {};

  void populateData();
  void signalUpdate();
};

#endif // MODELSNAPSHOTS_H
