#ifndef MODELSENDINGQ_H
#define MODELSENDINGQ_H

#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelSendingQ : public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelSendingQ(QObject *parent);
  virtual ~ModelSendingQ(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const;


  QVDRecordsT m_sending_q = {};
  QHash<QString, QBrush> m_brushes;

  void populateData();
  void signalUpdate();

};


#endif // MODELSENDINGQ_H
