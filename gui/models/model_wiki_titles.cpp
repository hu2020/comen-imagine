#include "stable.h"

#include "gui/c_gui.h"
#include "lib/ccrypto.h"
#include "lib/services/free_docs/wiki/wiki_handler.h"

#include "model_wiki_titles.h"


ModelWikiTitles::ModelWikiTitles(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelWikiTitles::rowCount(const QModelIndex & /*parent*/) const
{
  return m_wiki_titles.size();
}

int ModelWikiTitles::columnCount(const QModelIndex & /*parent*/) const
{
  return 4;
}


QVariant ModelWikiTitles::data(const QModelIndex &index, int role) const
{
  QVDicT the_wiki_page = m_wiki_titles[index.row()];

  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;

        case 1:
          return the_wiki_page.value("wkp_iname").toString();

        case 2:
          return the_wiki_page.value("wkp_title").toString();

        case 3:
          return the_wiki_page.value("wkp_last_modified").toString();
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(CUtils::hash6c(the_wiki_page.value("wkp_hash").toString()));
        case 1:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(the_wiki_page.value("wkp_iname").toString()));
        case 2:
        case 3:
        return QBrush {};

      }
      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return CUtils::convertJSonArrayToQStringList(m_wiki_titles[index.row()].value("prerequisites_arr").toJsonArray()).join(", ");
          break;


        default:
          return "";
          break;
      }
      break;



  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelWikiTitles::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Title");
      case 2:
        return QString("Last Update");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Number");
      case 1:
        return QString("Prerequisities block");
      case 2:
        return QString("Parsing attemps");
      }
    }
  }
  return QVariant();
}


void ModelWikiTitles::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_wiki_titles = WikiHandler::searchInWikiPages(
    false,
    {},
    {"wkp_iname", "wkp_language", "wkp_hash", "wkp_title", "wkp_last_modified"},
    {{"wkp_iname", "ASC"}, {"wkp_title", "ASC"}});

}


void ModelWikiTitles::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

