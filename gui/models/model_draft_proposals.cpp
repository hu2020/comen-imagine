#include "stable.h"

#include "gui/c_gui.h"
#include "lib/services/proposals/proposal_handler.h"
#include "lib/services/contribute/contribute_handler.h"

#include "model_draft_proposals.h"

ModelDraftProposals::ModelDraftProposals(QObject *parent)
{
Q_UNUSED(parent);
}

int ModelDraftProposals::rowCount(const QModelIndex & /*parent*/) const
{
  return m_draft_proposals.keys().size();
}

int ModelDraftProposals::columnCount(const QModelIndex & /*parent*/) const
{
  return 3;
}


QVariant ModelDraftProposals::getCellData(const QModelIndex &index) const
{
  QVDicT proposal = m_draft_proposals[index.row()];
  uint32_t mod_ = index.row() % 10;
  CLog::log("11>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + QVariant::fromValue(proposal.value("pd_voting_timeframe").toDouble()).toString());
  CLog::log("22>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + CUtils::convertFloatToString(proposal.value("pd_voting_timeframe").toDouble()));

  switch(index.column())
  {
    case 0:
      if (mod_==0)
        return proposal.value("proposal_number").toInt();
      return "";

    case 1:
      switch(mod_)
      {
      case 0:
        return "Title: ";
      case 1:
        return "Proposal creation date & timeframe: ";
      case 2:
        return "Comment: ";
      case 3:
        return "Tags: ";
      case 4:
        return "Quantity: ";
      case 5:
        return "Contributor: ";
      case 6:
        return "Request loan";
      case 7:
        return "See loan Requests";
      case 8:
        return "";
      case 9:
        return "";

      default:
        return "undef mode_";
      }
      break;

    case 2:
      switch(mod_)
      {
        case 0:
          return (proposal.value("pd_title").toString() + " hash(" + CUtils::hash8c(proposal.value("pd_hash").toString()) + ")" );
        case 1:
          return proposal.value("pd_creation_date").toString() + " Polling lasts " + CUtils::convertFloatToString(proposal.value("pd_voting_timeframe").toDouble()) + " Hour(s)";
        case 2:
          return proposal.value("pd_comment").toString();
        case 3:
          return proposal.value("pd_tags").toString();
        case 4:
          return proposal.value("pd_help_hours").toString() + " hours of contribution in level " + proposal.value("pd_help_level").toString() + " usefulness";
        case 5:
          return proposal.value("pd_contributor_account").toString() + "has " + QString::number(proposal.value("contributor_current_shares").toDouble()) + " shares of " + QString::number(proposal.value("total_shares").toDouble()) + " total shares ";

        case 6:
          return "Sign and pay Proposal Costs";
        case 7:
          return "Delete proposal";
        case 8:
          return "";
        case 9:
          return "";
      }
      break;

  }

  return "";
}


QVariant ModelDraftProposals::data(const QModelIndex &index, int role) const
{
  if (!CMachine::canStartLazyLoadings())
    return QVariant();

  QVDicT proposal = m_draft_proposals[index.row()];

  QString row_color_proposal = (index.row() % 2 == 0) ? "ffffff" : "eaebec";
  QString row_color_polling = (index.row() % 2 == 0) ? "afffff" : "aaebec";
  QString row_color_votes = (index.row() % 2 == 0) ? "8fffdf" : "8aabec";

  uint32_t mod_ = index.row() % 10;

  switch(role){
    case Qt::DisplayRole:
      return getCellData(index);
      break;

    case Qt::BackgroundRole:
      if (mod_ == 9 )
      {
        return CGUI::getBrushByColorCode("101010");

      }else{
        switch(index.column())
        {
          case 0:
            return CGUI::getBrushByColorCode(CUtils::hash6c(proposal.value("pd_hash").toString()));
            break;

          case 1:
            switch (mod_)
            {
              case 6:
                return CGUI::getBrushByColorCode("00aabb");
              case 7:
                return CGUI::getBrushByColorCode("55aabb");
            }
          case 2:
            switch (mod_)
            {
              case 6:
                return CGUI::getBrushByColorCode("00ff00");
              case 7:
                return CGUI::getBrushByColorCode("ff0000");
            }


            return CGUI::getBrushByColorCode(row_color_proposal);
          break;

        default:
          return CGUI::getBrushByColorCode(row_color_proposal);

        }

      }

      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
        case 1:
          return "";
          break;

        default:
          return "";
          break;
      }
      break;



  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}


QVariant ModelDraftProposals::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("#");
      case 2:
        return QString("#");

      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Number");
      case 1:
        return QString("Prerequisities block");
      case 2:
        return QString("Parsing attemps");
      }
    }
  }
  return QVariant();
}


void ModelDraftProposals::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_draft_proposals = ProposalHandler::loadMyDraftProposals();
}


void ModelDraftProposals::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

bool signandpayProposalCosts(QVDicT proposal)
{
  CLog::log("Sign and pay Proposal Costs " + proposal.value("pd_hash").toString());
  auto[status, msg] = ContributeHandler::signAndPayProposalCosts(proposal.value("pd_hash").toString());
  CGUI::displayMsg(CGUI::get().m_ui->label_proposalNotifyMsg, msg, status);
  CGUI::signalUpdateBlockBuffer();
  return true;
}

void ModelDraftProposals::draftProposalDblClicked(const QModelIndex& index)
{
  QVDicT proposal = m_draft_proposals[index.row()];
  uint32_t mod_ = index.row() % 10;

  switch(index.column())
  {
    case 0:
      if (mod_==0)
        return;
      return;

    case 1:
      switch(mod_)
      {
      case 0:
        return;
      case 1:
        return;
      case 2:
        return;
      case 3:
        return;
      case 4:
        return;
      case 5:
        return;
      case 6:
        CLog::log("Request loan for " + proposal.value("pd_hash").toString());
        CGUI::toggleLoanRequestForm(proposal.value("pd_hash").toString());
        return;
      case 7:
        CLog::log("See loan Requests " + proposal.value("pd_hash").toString());
        CGUI::toggleLoanRequestsList(proposal.value("pd_hash").toString());
        return;
      case 8:
        return;
      case 9:
        return;

      default:
        return;
      }
      break;

    case 2:
      switch(mod_)
      {
        case 6:
          signandpayProposalCosts(proposal);
          break;

        case 7:
          CLog::log("Delete proposal " + proposal.value("pd_hash").toString());
          ProposalHandler::deleteDraftProposal(proposal.value("pd_hash").toString());
          CGUI::signalUpdateDraftProposals();
          return;

        case 8:
          return;
        case 9:
          return;
      }
      break;
  }

}
