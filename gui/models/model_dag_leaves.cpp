#include "model_dag_leaves.h"

#include "lib/k_v_handler.h"
#include "lib/dag/leaves_handler.h"

#include "gui/c_gui.h"


ModelDAGLeaves::ModelDAGLeaves(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelDAGLeaves::rowCount(const QModelIndex & /*parent*/) const
{
  switch(m_action_type)
  {
    case ActionType::LEAVES_BY_KVALUE:
      return m_leaves_by_kv.keys().size();
      break;

    case ActionType::LEAVES_BY_DAG:
      return m_leaves_by_dag.keys().size();
      break;

    case ActionType::FRESH_LEAVES:
      return m_fresh_leaves.keys().size();
      break;
  }

  return 0;
}

int ModelDAGLeaves::columnCount(const QModelIndex & /*parent*/) const
{
  return 3;
}

QVariant ModelDAGLeaves::data(const QModelIndex &index, int role) const
{
  switch(m_action_type)
  {
    case ActionType::LEAVES_BY_KVALUE:
      return getLeavesByKValue(index, role);
      break;

    case ActionType::LEAVES_BY_DAG:
      return getLeavesByDAGTracking(index, role);
      break;

    case ActionType::FRESH_LEAVES:
      return getFreshLeavesData(index, role);
      break;

  }


  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelDAGLeaves::getFreshLeavesData(const QModelIndex &index, int role) const
{
  QStringList keys = m_fresh_leaves.keys();
  keys.sort();

  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;
        case 1:
          return CUtils::hash8c(keys[index.row()]);
          break;
        case 2:
          return m_fresh_leaves[keys[index.row()]].toObject().value("bCDate").toString();
          break;
      }
      break;

    case Qt::ToolTipRole:
      return m_fresh_leaves[keys[index.row()]].toObject().value("bType").toString() + " " + keys[index.row()] + " " + m_fresh_leaves[keys[index.row()]].toObject().value("bCDate").toString();


    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(keys[index.row()]);
        case 1:
        case 2:
          return m_brushes[m_fresh_leaves[keys[index.row()]].toObject().value("bType").toString()];

      }
      break;


  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelDAGLeaves::getLeavesByKValue(const QModelIndex &index, int role) const
{
  QStringList keys = m_leaves_by_kv.keys();
  keys.sort();

  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;
        case 1:
          return CUtils::hash8c(keys[index.row()]);
          break;
        case 2:
          return m_leaves_by_kv[keys[index.row()]].toObject().value("bCDate").toString();
          break;
      }
      break;

    case Qt::ToolTipRole:
      return m_leaves_by_kv[keys[index.row()]].toObject().value("bType").toString() + " " + keys[index.row()] + " " + m_leaves_by_kv[keys[index.row()]].toObject().value("bCDate").toString();


    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(keys[index.row()]);
        case 1:
        case 2:
          return m_brushes[m_leaves_by_kv[keys[index.row()]].toObject().value("bType").toString()];

      }
      break;


  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelDAGLeaves::getLeavesByDAGTracking(const QModelIndex &index, int role) const
{
  QStringList keys = m_leaves_by_dag.keys();
  keys.sort();

  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;
        case 1:
          return CUtils::hash8c(keys[index.row()]);
          break;
        case 2:
          return m_leaves_by_dag[keys[index.row()]].value("b_creation_date");
          break;
      }
      break;

    case Qt::ToolTipRole:
      return m_leaves_by_dag[keys[index.row()]].value("b_type") + " " + keys[index.row()] + " " + m_leaves_by_dag[keys[index.row()]].value("b_creation_date");


    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(keys[index.row()]);
        case 1:
        case 2:
          return m_brushes[m_leaves_by_dag[keys[index.row()]].value("b_type")];

      }
      break;


  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelDAGLeaves::headerData(int section, Qt::Orientation orientation, int role) const
{
  switch(m_action_type)
  {
    case ActionType::LEAVES_BY_KVALUE:
    case ActionType::LEAVES_BY_DAG:
    case ActionType::FRESH_LEAVES:
      if (role == Qt::DisplayRole)
      {
        if (orientation == Qt::Horizontal)
        {
          switch (section)
          {
          case 0:
            return QString("#");
          case 1:
            return QString("Hash");
          case 2:
            return QString("Create Date");
          }
        }
      }
      break;
  }

  return QVariant();
}

void ModelDAGLeaves::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_leaves_by_kv = LeavesHandler::getLeaveBlocks();

  QSDRecordsT leaves_by_dag = DAG::analyzeDAGHealth();
  m_leaves_by_dag = {};
  for (QSDicT a_leave: leaves_by_dag)
    m_leaves_by_dag[a_leave.value("b_ash")] = a_leave;

  m_fresh_leaves = LeavesHandler::getFreshLeaves();
}


void ModelDAGLeaves::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelDAGLeaves::setCustomAction(int action_type)
{
  m_action_type = action_type;
}
