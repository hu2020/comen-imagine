#ifndef MODELCOINSVISBILITY_H
#define MODELCOINSVISBILITY_H


#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelCoinsVisbility: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelCoinsVisbility(QObject *parent);
  virtual ~ModelCoinsVisbility(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_coins_visibility = {};
  QHash<QString, QBrush> m_brushes;

  void populateData();
  void signalUpdate();
};
#endif // MODELCOINSVISBILITY_H
