#ifndef MODELMISSEDBLOCKS_H
#define MODELMISSEDBLOCKS_H
#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelMissedBlocks : public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelMissedBlocks(QObject *parent);
  virtual ~ModelMissedBlocks(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_missed_blocks = {};
  QHash<QString, QBrush> m_brushes;

  void populateData();
  void signalUpdate();
};

#endif // MODELMISSEDBLOCKS_H
