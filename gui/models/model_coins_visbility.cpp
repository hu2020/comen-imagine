#include "model_coins_visbility.h"

#include "gui/c_gui.h"

ModelCoinsVisbility::ModelCoinsVisbility(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelCoinsVisbility::rowCount(const QModelIndex & /*parent*/) const
{
  return m_coins_visibility.size();
}

int ModelCoinsVisbility::columnCount(const QModelIndex & /*parent*/) const
{
  return 6;
}

QVariant ModelCoinsVisbility::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return (QString::number(index.row() + 1) + " (" + CUtils::hash6c(m_coins_visibility[index.row()].value("coins_hash").toString()) + ")");

        case 1:
          return CUtils::hash8c(m_coins_visibility[index.row()].value("visible_by").toString());

        case 2:
          return m_coins_visibility[index.row()].value("coins_count").toInt();

        case 3:
          return CUtils::sepNum(m_coins_visibility[index.row()].value("coins_value").toDouble());

        case 4:
          return m_coins_visibility[index.row()].value("owners_count").toInt();

        case 5:
          return m_coins_visibility[index.row()].value("block_creation_date");
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(m_coins_visibility[index.row()].value("coins_hash").toString().midRef(16, 6).toString()); // used the after 6 chars to make difference in color to easy ditinguish similarity and discriminatioins
        case 1:
          return m_brushes[m_coins_visibility[index.row()].value("block_type").toString()];
        case 2:
        case 3:
          return QBrush {};
        case 4:
          return CGUI::getBrushByColorCode(m_coins_visibility[index.row()].value("owners_hash").toString().midRef(16, 6).toString());
      }
      break;

    case Qt::ToolTipRole:
    switch(index.column())
    {
      case 0:
      case 1:
        m_coins_visibility[index.row()].value("block_type").toString();
        break;


    default:
      return "";
      break;
    }


  }

  if (role == Qt::DisplayRole)
  {
    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelCoinsVisbility::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Number");
      case 1:
        return QString("Block");
      case 2:
        return QString("Visible Coins count");
      case 3:
        return QString("Visible Value");
      case 4:
        return QString("Owners count");
      case 5:
        return QString("Block Creation date");

      }
    }
  } else if (role == Qt::ToolTipRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Number");
      case 1:
        return QString("Block");
      case 2:
        return QString("Visible Coins count");
      case 3:
        return QString("Visible Value");
      case 4:
        return QString("Owners count");
      }
    }
  }
  return QVariant();
}

void ModelCoinsVisbility::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_coins_visibility = UTXOHandler::generateCoinsVisibilityReport();
}


void ModelCoinsVisbility::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}
