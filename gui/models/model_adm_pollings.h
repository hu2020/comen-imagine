#ifndef MODELADMPOLLINGS_H
#define MODELADMPOLLINGS_H




#include <QObject>
#include <QFileDialog>
#include <QApplication>
#include <QTableView>
#include <QItemDelegate>
#include <QIdentityProxyModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QAbstractTableModel>

#include "stable.h"

class ModelAdmPollings: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelAdmPollings(QObject *parent);
  virtual ~ModelAdmPollings(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QJsonArray m_adm_pollings = {};
  QString m_selected_adm_polling_type = "";

  void populateData();
  void signalUpdate();
};

#endif // MODELADMPOLLINGS_H
