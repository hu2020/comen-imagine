#include "lib/sending_q_handler/sending_q_handler.h"

#include "model_sending_q.h"

ModelSendingQ::ModelSendingQ(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelSendingQ::rowCount(const QModelIndex & /*parent*/) const
{
  return m_sending_q.size();
}

int ModelSendingQ::columnCount(const QModelIndex & /*parent*/) const
{
  return 3;
}

QVariant ModelSendingQ::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return m_sending_q[index.row()].value("sq_type").toString();
        case 1:
          return CUtils::hash8c(m_sending_q[index.row()].value("sq_code").toString());
        case 2:
          return m_sending_q[index.row()].value("sq_creation_date").toString();
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return QBrush();
        case 1:
        case 2:
          return m_brushes[m_sending_q[index.row()].value("sq_type").toString()];

      }
      break;


  }

  if (role == Qt::DisplayRole)
  {


    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelSendingQ::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Type");
      case 1:
        return QString("Hash");
      case 2:
        return QString("Create Date");
      }
    }
  }
  return QVariant();
}

void ModelSendingQ::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_sending_q = SendingQHandler::fetchFromSendingQ();
}


void ModelSendingQ::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

