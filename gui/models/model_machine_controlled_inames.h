#ifndef MODELMACHINECONTROLLEDINAMES_H
#define MODELMACHINECONTROLLEDINAMES_H



#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"
#include "lib/services/contracts/flens/iname_handler.h"

class ModelMachineControlledINames: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelMachineControlledINames(QObject *parent);
  virtual ~ModelMachineControlledINames(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;
  QVariant getCellToolTip(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_machine_controlled_inames {};
  BindingInfo m_bindings_info {};

  void populateData();
  void signalUpdate();

  void machineControlledINameDblClicked(const QModelIndex& idx);

};

#endif // MODELMACHINECONTROLLEDINAMES_H
