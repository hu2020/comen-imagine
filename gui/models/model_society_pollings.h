#ifndef MODELREFINEPOLLINGS_H
#define MODELREFINEPOLLINGS_H



#include <QObject>
#include <QFileDialog>
#include <QApplication>
#include <QTableView>
#include <QItemDelegate>
#include <QIdentityProxyModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QAbstractTableModel>

#include "stable.h"

class ModelSocietyPollings: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelSocietyPollings(QObject *parent);
  virtual ~ModelSocietyPollings(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QHash<uint32_t, QVDicT> m_society_pollings = {};
  int8_t m_adm_polling_score = 0;
  CDocHashT m_selected_adm_polling = "";

  void societyPollingsDblClicked(const QModelIndex& index);
  bool m_is_visible_adm_vote_form = false;

  void populateData();
  void signalUpdate();
};

#endif // MODELREFINEPOLLINGS_H
