#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/wallet/wallet.h"
#include "gui/c_gui.h"

#include "model_wallet_coins.h"

ModelWalletCoins::ModelWalletCoins(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelWalletCoins::rowCount(const QModelIndex & /*parent*/) const
{
  return m_wallet_controlled_coins.size();
}

int ModelWalletCoins::columnCount(const QModelIndex & /*parent*/) const
{
  return 5;
}

QVariant ModelWalletCoins::data(const QModelIndex &index, int role) const
{
  QVDicT the_coin = m_wallet_controlled_coins[index.row()];
  QString even_color = "e1d7ca";
  QString odd_color = "81d7ca";

  switch(role)
  {
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return index.row() + 1;

        case 1:
          return CUtils::packCoinCode(CUtils::hash16c(the_coin.value("wf_trx_hash").toString()), the_coin.value("wf_o_index").toUInt());

        case 2:
          return the_coin.value("wf_mature_date").toString();

        case 3:
          return CUtils::microPAIToPAI6(the_coin.value("wf_o_value").toDouble());

        case 4:
          return ((the_coin.value("wf_mature_date").toString() < CUtils::getNow()) ? "" : "Unspendable");
      }
      break;

    case Qt::CheckStateRole:
      if (index.column() == 4)
      {
        QVDicT the_coin = m_wallet_controlled_coins[index.row()];
        CCoinCodeT the_coin_ref = CUtils::packCoinCode(the_coin.value("wf_trx_hash").toString(), the_coin.value("wf_o_index").toUInt());
        if (m_trx_selected_coins_to_spend.keys().contains(the_coin_ref) && m_trx_selected_coins_to_spend[the_coin_ref] )
          return true;
        return false;  //wf_mature_date
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return CGUI::getBrushByColorCode(CCrypto::keccak256(the_coin.value("wf_address").toString()));
        case 1:
        case 2:
        case 3:
        case 4:
          return CGUI::getBrushByColorCode(((index.row()%2) == 0 ? even_color: odd_color ));

      }
      break;

    case Qt::ToolTipRole:
      switch(index.column())
      {
        case 0:
          return the_coin.value("wf_address").toString();
        case 1:
          return CUtils::packCoinCode(the_coin.value("wf_trx_hash").toString(), the_coin.value("wf_o_index").toUInt());


        default:
          return "";
          break;
      }
      break;

    case Qt::TextAlignmentRole:
      switch(index.column())
      {
        case 0:
        case 1:
        case 2:
          return QVariant();

        case 3:
          return Qt::AlignRight;

        default:
          return QVariant();
      }
      break;



    }

    if (role == Qt::DisplayRole)
    {
      return QString("Row%1, Column%2")
      .arg(index.row() + 1)
      .arg(index.column() +1);
    }
  return QVariant();
}

QVariant ModelWalletCoins::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("#");
      case 1:
        return QString("Coin");
      case 2:
        return QString("Spendable after");
      case 3:
        return QString("Value(PAIs)");
      case 4:
        return QString("Select");
      }
    }

  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Number");
      case 1:
        return QString("Prerequisities block");
      case 2:
        return QString("Parsing attemps");
      case 3:
        return QString("Block hash");
      case 4:
        return QString("Create Date");
      }
    }
  }
  return QVariant();
}

Qt::ItemFlags ModelWalletCoins::flags(const QModelIndex & index) const
{
  QVDicT the_coin = m_wallet_controlled_coins[index.row()];
  CDateT mature_date = the_coin.value("wf_mature_date").toString();

  if ((index.column() == 4) && (mature_date < CUtils::getNow()))
    return Qt::ItemIsUserCheckable | QAbstractTableModel::flags(index);;

  return QAbstractTableModel::flags(index);
}

bool ModelWalletCoins::setData(const QModelIndex & index, const QVariant & value, int role)
{
  if(index.column() == 4 && role == Qt::CheckStateRole)
  {
    QVDicT the_coin = m_wallet_controlled_coins[index.row()];
    CCoinCodeT the_coin_ref = CUtils::packCoinCode(the_coin.value("wf_trx_hash").toString(), the_coin.value("wf_o_index").toUInt());
    if (!m_trx_selected_coins_to_spend.keys().contains(the_coin_ref))
    {
      m_trx_selected_coins_to_spend[the_coin_ref] = true;
    }else{
      m_trx_selected_coins_to_spend[the_coin_ref] = !m_trx_selected_coins_to_spend[the_coin_ref];
    }

    if (m_trx_selected_coins_to_spend[the_coin_ref])
    {
      m_trx_spendable_amount += the_coin.value("wf_o_value").toDouble();
    }else{
      m_trx_spendable_amount -= the_coin.value("wf_o_value").toDouble();
    }

    CGUI::getModelWalletCoins()->recalcTransactionForm();
    CGUI::refreshTransactionFormPresentation();
  }
  else
  {
    QAbstractTableModel::setData(index, value, role);
  }
  return true;
}

void ModelWalletCoins::populateData()
{
  if (!CMachine::canStartLazyLoadings())
    return;

  m_wallet_controlled_coins = Wallet::getCoinsList(false);
}


void ModelWalletCoins::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

void ModelWalletCoins::resetSelectedWalletCoins()
{
  beginResetModel();
  for (CCoinCodeT a_coin_code: m_trx_selected_coins_to_spend.keys())
    m_trx_selected_coins_to_spend[a_coin_code] = false;
  endResetModel();
}

void ModelWalletCoins::recalcTransactionForm()
{
  m_trx_change_back = m_trx_spendable_amount - (m_trx_sending_amount * 1000000) - (m_trx_fee_amount * 1000000);
}
