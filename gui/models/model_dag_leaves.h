#ifndef MODELDAGLEAVES_H
#define MODELDAGLEAVES_H


#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"

class ModelDAGLeaves : public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelDAGLeaves(QObject *parent);
  virtual ~ModelDAGLeaves(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QJsonObject m_leaves_by_kv = {};
  QS2DicT m_leaves_by_dag = {};
  QJsonObject m_fresh_leaves = {};

  QHash<QString, QBrush> m_brushes;

  void populateData();
  void signalUpdate();


  enum ActionType
  {
    LEAVES_BY_KVALUE,
    LEAVES_BY_DAG,
    FRESH_LEAVES
  };

  int m_action_type = ActionType::LEAVES_BY_KVALUE;
  void setCustomAction(int action_type = ActionType::LEAVES_BY_KVALUE);
  QVariant getFreshLeavesData(const QModelIndex &index, int role) const;
  QVariant getLeavesByKValue(const QModelIndex &index, int role) const;
  QVariant getLeavesByDAGTracking(const QModelIndex &index, int role) const;

};

#endif // MODELDAGLEAVES_H
