#ifndef MODELPROPOSALPLEDGETRANSACTIONS_H
#define MODELPROPOSALPLEDGETRANSACTIONS_H



#include <QObject>
#include <QApplication>
#include <QAbstractTableModel>

#include "stable.h"



class ModelProposalPledgeTransactions: public QAbstractTableModel
{
  Q_OBJECT
public:
  ModelProposalPledgeTransactions(QObject *parent);
  virtual ~ModelProposalPledgeTransactions(){};
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant getCellData(const QModelIndex &index) const;

  QVariant headerData(
    int section,
    Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  QVDRecordsT m_ppt_bundles {};

  void populateData();
  void signalUpdate();

  void proposalPledgeTransactionsDblClicked(const QModelIndex& idx);

};

#endif // MODELPROPOSALPLEDGETRANSACTIONS_H
